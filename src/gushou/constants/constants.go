package constants

import (
	"os"
)

var STRIPE_ENV_SECRET_KEY = ""
var STRIPE_ENV_PUBLIC_KEY = ""
var STRIPE_CONNET_CLIENT_KEY = ""

func init() {
	STRIPE_ENV_SECRET_KEY = os.Getenv("STRIPE_ENV_SECRET_KEY")
	STRIPE_ENV_PUBLIC_KEY = os.Getenv("STRIPE_ENV_PUBLIC_KEY")
	STRIPE_CONNET_CLIENT_KEY = "ca_D9hwNthPyq2jzdvYci1GLozqrBMwPe3v"
}

const (
	FROMEMAIL = "Gushou Dragon Boat <dragonboat@gogushou.com>"
	SITE_URL  = "#siteUrl"
	SHOW_PAGE = "#showPage"

	DEFAULT_SMS_AVAILABLE_FOR_TEAM = "400"

	/* Twilio Access Details for SMS

	// Chrissy Acc
	ACCOUNT_SID = "AC6c6d752dd85d71f685c32c3f85374fa5"
	AUTH_TOKEN  = "94aa631f042ed87b1726cfa062f1aa8b"
	FROM       = "+14803729651"

	//  Ajan Acc
	ACCOUNT_SID   = "ACaa4493c4b49e9159b0bf0db07c84783f"
	AUTH_TOKEN    = "dad482592e1c6bfc5d1f161c72fa69cb"
	FROM         = "+15175806173"
	*/

	ACCOUNT_SID                = "AC6c6d752dd85d71f685c32c3f85374fa5"
	AUTH_TOKEN                 = "94aa631f042ed87b1726cfa062f1aa8b"
	FROM                       = "+14803729651" // %2B is equal to '+'
	MESSAGE_SERVICE_LIST_URL   = "https://messaging.twilio.com/v1/Services"
	CREATE_MESSAGE_URL         = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json"
	CREATE_MESSAGE_SERVICE_URL = "https://messaging.twilio.com/v1/Services"
	VIEW_MESSAGE_LIST          = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json"
	MSG_SERVICE_FRIENDLY_NAME  = "FriendlyName=gushouMsgServicePractice-"

	P_ICON       = "<p>"
	ICON_SITE    = "<a  href='" + SITE_URL + "'>Gushou</a>"
	ICON_FB      = "<a href='https://www.facebook.com/gogushou' target='_blank'>Facebook</a>"
	ICON_TWITTER = "<a href='https://twitter.com/gogushou' target='_blank'>Twitter</a>"
	ICON_BLOG    = "<a target='_blank' href='http://www.gogushou.wordpress.com'>Blog</a>"

	VERIFY_EMAIL_SUBJECT = "Confirm Your Email, Gushou Dragon Boat"
	VERIFY_EMAIL_CONTENT = "FINAL STEP! <br><br>Confirm your email address to complete your Gushou account. It's easy, just click on this #verifyLink (or copy & paste the following URL into your browser): <br><br>"

	REGISTERATION_CONTENT = "<p>Welcome to Gushou, the pulse of the dragon boat community.</p><p>Get ready to experience all the time-saving benefits Gushou has to offer:</p><ul style='list-style-type: none;'><li>Discover new teams and paddlers</li><li>Manage practices & schedules</li>    <li>Email your entire team</li>    <li>Streamline your dragon boat life</li>    <li>Process waivers</li></ul><p>You will receive digest emails once per day. Keep that inbox lean and clean! </p><p>Wishing you health, friendship & speed on the water!<br>The Gushou Team</p><p>Did you know \"Gushou\" means drummer in Chinese? They're the pulse of dragon boating - just like us."

	RESET_PASSWORD_SUBJECT = "Reset Gushou Password"
	RESET_PASSWORD_CONTENT = "Please reset your Gushou password #resetLink or copy and paste the url below in your internet browser:<br><br>"

	/* Event Created */
	EVENT_CREATED_EMAIL_SUBJECT = "Your Dragon Boat Event Is On Gushou!"
	EVENT_CREATED_EMAIL_CONTENT = "Congratulations, #eventName, #eventDate in #eventLocation is now on Gushou! Local and international paddlers will now have access to the information on your event page.<br><br>We will continue to build features to help ease your administrative work as an event organizer. Keep checking in and please feel free to contact us with questions or feedback."
	EVENT_CREATED_DIGEST        = "#eventName, #eventDate in #eventLocation is now on Gushou!"

	REHOST_EVENT_DIGEST        = "#eventName is now on Gushou for another season!"
	REHOST_EVENT_EMAIL_SUBJECT = "Event Successfully Rehosted on Gushou"
	REHOST_EVENT_EMAIL_CONTENT = "Congratulations, #eventName is now on Gushou for another season!<br><br>We wish you all the best for your event on #eventDate in #eventLocation.<br><br>We will continue to build features to help ease your administrative work as an event organizer. Keep checking in and please feel free to contact us with questions or feedback."

	CLAIM_ACCEPTED_DIGEST       = "Great news, you're ready to go! The past event organizer of #eventName has handed you the reins. Feel free to sign in and update your event information! "
	CLAIM_ACCEPTED_NOTIFICATION = "Your request to claim #eventName has been accepted. You can now use your super powers and edit #eventName."
	CLAIM_REJECTED_DIGEST       = "Event organizer #eventOrganizer, #eventOrganizerEmail has declined your request to claim #eventName. Please contact the Gushou team at dragonboat@gogushou.com for support."
	CLAIM_REJECTED_NOTIFICATION = "Event organizer, #eventOrganizer, has declined your request to claim #eventName."
	CLAIM_EVENT                 = "This is a request from #eventOrganizer for administration access to #eventName. Please accept or decline the request below. <br><br>Please sign in and check the notifiction on your dashboard <a  href='" + SITE_URL + "''>here</a> to accept or decline the request."

	EVENT_VOLUNTEER_REQUEST = "Event Volunteer Request: You have a request from #userName, (#userEmail), to volunteer at, #eventName:<br> <span style='padding-left: 4em; font-style:italic;'> #message </span>"

	EVENT_VOLUNTEER_NOTIFICATION = "You have sent a volunteer request for #eventName: <br> <span style='padding-left: 4em; font-style:italic;'> #message </span>"

	EVENT_DELETED_DIGEST = "#eventName has been taken down from Gushou's online platform. We're sorry to see you go. "

	TEAM_INVITE_REGISTERATION = "<p>Welcome to <a href='" + SITE_URL + "'>Gushou</a>, on an online dragon boat tool used for managing teams and events.</p><p>You have been invited to the dragon boat team named #teamName by #teamCaptainFirstName #teamCaptainLastName on Gushou. Please take a few minutes to create your Gusou account <a href='" + SHOW_PAGE + "''>here</a>, using #useremail. You and your captain will spend less time on dragon boat administration and more time doing what you love! </p><p style='display: #attributeValue'>Message from your team captain:<br><span style='padding-left: 4em; font-style:italic;'> #teamInviteMessage </span></p><p>Get ready to experience all the time-saving benefits Gushou has to offer:</p><ul style='list-style-type: none;'><li>Streamline communication</li><li> Manage schedule </li><li>Set availability</li><li> Process waivers</li><li>Set rosters & line-ups</li><li>Register for events</li></ul><p>Wishing you health, friendship & speed on the water!<br>Chrissy & The Gushou Team</p><p>Did you know \"Gushou\" means drummer in Chinese?... the pulse of dragon boat team.</p>"

	TEAM_INVITE_NOTIFICATION  = "You have been added to #teamName! <p>If you would like to remove yourself from this team, please select, Remove.</p>"
	TEAM_INVITE_EMAIL_SUBJECT = "Dragon Boat Team Invitation on Gushou"

	TEAM_INVITE_INSTANT_EMAIL = "<p>You have been added to the dragon boat team #teamName by #teamCaptainFirstName #teamCaptainLastName on Gushou. You can see your updates by logging into Gushou using #useremail.</p><p style='display: #attributeValue'>Message from your team captain:<br> <span style='padding-left: 4em; font-style:italic;'> #teamInviteMessage </span> </p><p>If this was a mistake or you would like to be removed from this team please login to see the notifications and click the remove button.</p><p>Wishing you great paddling!</p><p>- Message sent from Gushou, on behalf of your Team Captain.</p>"

	TEAM_ACCEPT_TEAM_NOTIFICATION = "You have been added to #teamName! <p>If you would like to remove yourself from this team, please select, Remove.</p>"

	TEAM_INVITE_ACCEPTED_DIGEST_NOTIFICATION = "#userName has accepted your request to join #teamName."
	TEAM_INVITE_REJECTED_NOTIFICATION        = "Paddler #userName have been not interested in your team #teamName."
	TEAM_ACCEPT_REJECTED_NOTIFICATION        = "You have been successfully removed from #teamName."

	TEAM_INVITATION_REMINDER_SUBJECT                = "Dragon Boat Team Invitation on Gushou"
	TEAM_INVITATION_REMINDER_DIGEST                 = "Reminder: You have been invited to #teamName and can accept your team invitation by logging in to your gushou account <a href='" + SITE_URL + "login'>here</a> and accepting your team invitation under notifications.</p><p>Already logged in? Just click <a href='" + SITE_URL + "notification-all'>here</a>"
	TEAM_INVITATION_REMINDER_INSTANT_EMAIL          = "Reminder: You have been invited to #teamName and can accept your team invitation by logging in to your gushou account <a href='" + SITE_URL + "login'>here</a> and accepting your team invitation under notifications.</p><p>Already logged in? Just click <a href='" + SITE_URL + "notification-all'>here</a>.</p><p style='display: #attributeValue'>Message from your team captain:<br><span style='padding-left: 4em; font-style:italic;'> #teamReminderMessage </span></p>"
	TEAM_INVITATION_NEW_USER_REMINDER_INSTANT_EMAIL = "Reminder: You have been invited to #teamName and can accept your team invitation on your dashboard once you register with Gushou <a href='" + SHOW_PAGE + "''>here</a> and accepting your team invitation under notifications.</p><p>Already logged in? Just click <a href='" + SITE_URL + "notification-all'>here</a>.</p><p style='display: #attributeValue'>Message from your team captain:<br><span style='padding-left: 4em; font-style:italic;'> #teamReminderMessage </span></p>"
	TEAM_INVITATION_REMINDER_NOTIFICATION           = "Reminder: You have been invited to #teamName"

	TEAM_JOIN_REQUEST_DIGEST                     = "Gushou Team Recruitiment: You have a request from #userName to join your #teamName: <br><span style='padding-left: 4em; font-style:italic;'> #teamJoinMessage </span>"
	TEAM_JOIN_REQUEST_NOTIFICATION               = "You have a request from #userName to join your dragon boat team, #teamName: <span style='padding-left: 4em; font-style:italic;'> #teamJoinMessage </span>"
	TEAM_JOIN_REQUEST_ACCEPT_DIGEST_NOTIFICATION = "Congratulations. Your request to join #teamName has been accepted."
	TEAM_JOIN_REQUEST_DECLINE_NOTIFICATION       = "Sorry, your request to join #teamName has been turned down."

	TEAM_DISCUSSION_DIGEST_NOTIFICATION       = "New post on discussion board, #teamName."
	TEAM_DISCUSSION_REPLY_DIGEST_NOTIFICATION = "New reply posted on discussion board, #teamName."
	TEAM_DISCUSSION_INSTANT_NOTIFICATION      = "Message posted on Gushou Team Page for #teamName. Posted by #senderName:<br/>"

	TEAM_PRACTICE_ADDED_DIGEST_NOTIFICATION   = "#teamName: practice added on #practiceDate at #practiceTime. (#practiceRepeats)"
	TEAM_ACTIVITY_ADDED_DIGEST_NOTIFICATION   = "#teamName: activity added on #practiceDate to #practiceEndDate  at #practiceTime. (#practiceRepeats)"
	TEAM_PRACTICE_REMOVED_DIGEST_NOTIFICATION = "#teamName: practice cancelled on #practiceDate at #practiceTime. (#practiceRepeats)"
	TEAM_PRACTICE_REMINDER_SUBJECT            = "Practice Reminder"
	TEAM_PRACTICE_REMINDER_SUBJECT_NEW        = "Gushou Dragon Boat Practice Reminder"
	TEAM_PRACTICE_REMINDER_CONTENT            = "<p>This is a practice reminder for #teamName | #practiceDate | #practiceTime | #practiceLocation </p><p>Will you be attending this practice: #Yes, #No, #Maybe.</p><p>Enjoy your paddle!</p><p>- message sent from your captain</p>"

	TEAM_PRACTICE_REMINDER_YES_MAYBE_CONTENT = "<p>This is a practice reminder for #teamName | #practiceDate | #practiceTime | #practiceLocation </p><p>Your attendance has been set but if things change you can update your availability on your #Gushou_dashboard until the time of your practice.</p><p>Enjoy your paddle!</p><p>- message sent from your captain</p>"

	TEAM_MEMBER_REMOVED_SUBJECT = "Team Change on Gushou"
	TEAM_MEMBER_REMOVED         = "You have been removed from #teamName."

	TEAM_MEMBER_INSTANT_EMAIL_SUBJECT        = "Instant Team Message, Gushou"
	TEAM_MEMBER_INSTANT_MESSAGE_NOTIFICATION = "The captain of #teamName, #captain, has sent an instant message:<br> <span style='padding-left: 4em; font-style:italic;'> #captainMessage </span>"
	TEAM_MEMBER_INSTANT_MESSAGE_DIGEST       = "#teamName captain, #captain, has sent an instant message:<br> <span style='padding-left: 4em; font-style:italic;'> #captainMessage </span>"

	TEAM_MEMBER_NEW_INSTANT_EMAIL_SUBJECT        = "#teamName, Message sent from Gushou"
	TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION = "#captain_name, captain of #teamName, has sent you the following message:<br> <span style='padding-left: 0em; font-style:italic;'> #captainMessage </span>"

	TEAM_CAPTAIN_REMOVED = "You have been removed as a captain from #teamName."

	EMAILS_SOCIAL_FOOTER   = "<br><br><p style='font-size:85%; color: #ccc' >" + ICON_SITE + " | " + ICON_FB + " | " + ICON_TWITTER + " | " + ICON_BLOG + "</p>"
	EMAILS_NO_REPLY_FOOTER = "<p  style='font-size:85%; color: #ccc' >Please note that this message is being sent on behalf of the sender from Gushou. To respond, please direct your response to sender. If you would like to contact us with questions or feedback, please email <a href='mailto:dragonboat@gogushou.com' target='_blank'>dragonboat@gogushou.com</a>.</p>"

	EMAILS_UNSUBSCRIBE_FOOTER = "<p  style='font-size:85%; color: #ccc' >If you do not want to receive email notification service, <a href='" + SITE_URL + "profile-setting/#subscribe'>unsubscribe</a></p>"

	SEND_TEAM_WAIVER_INSTANT_EMAIL          = "<p>This is a request to e-sign #waiverName. Please <a href='" + SITE_URL + "login'>login to gushou</a> and accept the waiver on your dashboard under notifications.<br><br>Or if you have already logged in, follow this #waiverLink to accept the waiver!<br><br>Thank you!</p>"
	SEND_TEAM_WAIVER_NOTIFICATION           = "Please accept electronic waiver for #eventName."
	SEND_TEAM_WAIVER_REMINDER_INSTANT_EMAIL = "<p>This is a reminder for the request to e-sign the #waiverName. Please <a href='" + SITE_URL + "login'>login to gushou</a> and accept the waiver on your dashboard under notifications.<br><br>Or if you have already logged in, follow this #waiverLink to accept the waiver!<br><br>Thank you!</p>"
	SMS_FROM_PHONENO                        = "+14803729651,+16474928808,+16479526629,+16479526682"

	DATE_TIME_FORMAT = "2006-01-02 01:00 AM"

	DATE_TIME_ZONE_FORMAT = "2006-01-02T15:04:05Z07:00"

	FREE_TRIAL_SUBCRIPTION = "This great feature is accessible through the premium team plan. You are currently on the basic plan but can upgrade anytime!"

	SMS_COUNT = 400

	SMS_PACKAGE_AMOUNT = 1500

	GUSHOUFEE = 10

	GUSHOUFEESTRING = "10"

	STRIPEFEE1 = 2.9

	STRIPEFEE2 = 0.30

	GUSHOUCOMISSIONFEE = "5"

	PAYEVENTEMAIL_SUBJECT = "Payment Received: Invoice #InvoiceId (#teamname)"

	PAYEVENTEMAIL_CONTENT1 = "A payment has been received for Invoice #InvoiceId.<br><br>"

	PAYEVENTEMAIL_CONTENT2 = "Here are your payment details: <br><br>"

	PAYEVENTEMAIL_CONTENT3 = "Your transction ID for this payment is #transactionId<br><br>"

	PAYEVENTEMAIL_CONTENT4 = "Funds will be deposited in your account in 7 business days."

	TABLE_CONTENT = "<tr><td><b>#title<b></td><td>#content</td></tr>"

	TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION_BY_SENDER = "#sender_name, has sent you the following message:<br> <span style='padding-left: 0em; font-style:italic;'> #captainMessage </span>"

	SEND_EVENT_TEAM_WAIVER_REMINDER_INSTANT_EMAIL = "<p>Please take a moment to accept your #waiverName waiver online, #waiverLink<br><br>Thank you!<br><br>-Sent from your event organizer through Gushou</p>"

	STATICUSERIMAGE = "/static/images/user-default.png"
)
