package common

import (
	"fmt"
	"gushou/constants"
	"math/big"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/leekchan/accounting"
	"golang.org/x/text/currency"
)

func IsFeaturePracticeOrNot(practiceDateTime string, user_time_zone string) bool {

	IsFeaturePractice := false
	if user_time_zone != "" {
		layout := "2006-01-02T15:04:05Z07:00"
		then, err := time.Parse(layout, practiceDateTime)
		if err == nil {
			localLoc, err := time.LoadLocation(user_time_zone)
			practiceDate := then
			if err == nil {
				practiceDate = practiceDate.In(localLoc)
				dte := time.Now()
				localDateTime := dte.In(localLoc)
				// fmt.Println("practiceDate1", practiceDate.Format(layout))
				// fmt.Println("localDateTime1", localDateTime.Format(layout))
				if practiceDate.After(localDateTime) || practiceDate.Equal(localDateTime) {
					IsFeaturePractice = true
				}
			}

		}
	}

	return IsFeaturePractice
}

func IsFeaturePracticeOrNotTwilio(practiceDateTime string, user_time_zone string) bool {

	IsFeaturePractice := false
	if user_time_zone != "" {
		layout := "2006-01-02T15:04:05Z07:00"
		then, err := time.Parse(layout, practiceDateTime)
		if err == nil {
			localLoc, err := time.LoadLocation(user_time_zone)
			practiceDate := then
			if err == nil {
				practiceDate = practiceDate.In(localLoc)
				dte := time.Now()
				localDateTime := dte.In(localLoc)
				diff := localDateTime.Sub(practiceDate)
				noDays := int(diff.Hours() / 24)
				// fmt.Println("practiceDate2", practiceDate.Format(layout))
				// fmt.Println("localDateTime2", localDateTime.Format(layout))

				if (practiceDate.After(localDateTime) || practiceDate.Equal(localDateTime)) || noDays <= 2 {
					IsFeaturePractice = true
				}
			}

		}
	}

	return IsFeaturePractice
}

func IsValidSmsOrNot(msgReceivedDate string, practiceDate string, user_time_zone string) bool {

	IsValidPractice := false
	layout := "Mon, 02 Jan 2006 15:04:05 -0700"
	layout1 := "2006-01-02T15:04:05Z07:00"
	msgReceivedDateData, err := time.Parse(layout, msgReceivedDate)

	if err == nil {
		practiceDataDate, err := time.Parse(layout1, practiceDate)
		if err == nil {
			if msgReceivedDateData.Before(practiceDataDate) || msgReceivedDateData.Equal(practiceDataDate) {
				IsValidPractice = true
			}
		}
	}

	return IsValidPractice
}

func ConvertDateToLocalTimeZone(practiceDateTime string, user_time_zone string) string {

	IsFeaturePractice := ""
	if user_time_zone != "" {
		layout := "2006-01-02T15:04:05Z07:00"
		then, err := time.Parse(layout, practiceDateTime)
		if err == nil {
			localLoc, err := time.LoadLocation(user_time_zone)
			practiceDate := then
			if err == nil {
				practiceDateData := practiceDate.In(localLoc)
				IsFeaturePractice = practiceDateData.Format(layout)
			}

		}
	}

	return IsFeaturePractice
}

func CovertToUtcTimeZone(PracticeDateTimeZone string) string {

	local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, PracticeDateTimeZone)
	if err == nil {
		t := local.UTC()
		return t.Format(constants.DATE_TIME_ZONE_FORMAT)
	}
	return ""
}

func CheckUserRole(user_profileActivities string) (string, string, string) {

	isValidTeamMan := "false"
	isValidEventOrgani := "false"
	isValidPaddling := "false"

	if strings.Contains(user_profileActivities, "Team Management") {
		isValidTeamMan = "true"
	}

	if strings.Contains(user_profileActivities, "Event Organising") {
		isValidEventOrgani = "true"
	}

	if strings.Contains(user_profileActivities, "Paddling") {
		isValidPaddling = "true"
	}

	return isValidTeamMan, isValidEventOrgani, isValidPaddling
}

func GetFeatureDateFromNumberOfdays(noOfDays int) time.Time {
	var tempDate time.Time
	local := time.Now().UTC()
	if noOfDays == 365 {
		tempDate = local.Add(time.Hour * 24 * 365)
	} else if noOfDays == 1 {
		local := time.Now().UTC()
		tempDate = local.Add(time.Hour * 24 * 1)
	}
	return tempDate
}

func IsFeatureDate(DateTime string) string {

	IsFeatureDate := "false"
	layout := "2006-01-02T15:04:05Z07:00"
	receivedDateData, err := time.Parse(layout, DateTime)

	if err == nil {
		timeNow := time.Now().UTC()
		if receivedDateData.After(timeNow) || receivedDateData.Equal(timeNow) {
			IsFeatureDate = "true"
		}
	}

	return IsFeatureDate
}

func IsOldDate(DateTime string) string {

	IsOldDate := "false"
	layout := "2006-01-02T15:04:05Z07:00"
	receivedDateData, err := time.Parse(layout, DateTime)

	if err == nil {
		timeNow := time.Now().UTC()
		if timeNow.After(receivedDateData) || timeNow.Equal(receivedDateData) {
			IsOldDate = "true"
		}
	}

	return IsOldDate
}

func DayDiffrence(dateTime string) int {

	noDays := 0
	layout := "2006-01-02T15:04:05Z07:00"
	then, err := time.Parse(layout, dateTime)
	if err == nil {
		currentDate := time.Now().UTC()
		diff := then.Sub(currentDate)
		noDays = int(diff.Hours() / 24)
	}
	return noDays
}

func SubractDate(DateTime string) string {

	IsValidDate := "false"
	layout := "2006-01-02T15:04:05Z07:00"
	receivedDateData, err := time.Parse(layout, DateTime)
	if err == nil {
		timeNow := time.Now().UTC()
		tempDate := receivedDateData.Add(time.Hour * 24 * -21)
		if tempDate.Before(timeNow) || tempDate.Equal(timeNow) {
			IsValidDate = "true"
		}
	}

	return IsValidDate
}

func PracticeDateIsOldDateOrNot(trial_period_end_date string, practiceStartDate string) bool {
	IsValidDate := false
	layout := "2006-01-02T15:04:05Z07:00"
	trial_period_end_dateData, err := time.Parse(layout, trial_period_end_date)
	practiceStartDateData, err1 := time.Parse(layout, practiceStartDate)

	if err == nil && err1 == nil {
		tempDate := trial_period_end_dateData.Add(time.Hour * 24 * -21)
		if practiceStartDateData.Before(tempDate) {
			IsValidDate = true
		}
	}
	return IsValidDate
}

func LastDayOfMonth(year int, month time.Month) int {
	if month == time.February {
		if year%4 == 0 && (year%100 != 0 || year%400 == 0) { // leap year
			return 29
		}
		return 28
	}

	if month <= 7 {
		month++
	}
	if month&0x0001 == 0 {
		return 31
	}
	return 30
}

func AddMonth(tempsms_pack_enddateValue string, monthValue int) time.Time {
	layout := "2006-01-02T15:04:05Z07:00"
	receivedDateData, err := time.Parse(layout, tempsms_pack_enddateValue)
	fmt.Println(err)
	timeData := receivedDateData.UTC()
	t := timeData.AddDate(0, monthValue, 0)
	return t
}

func FloatToString(input_num float64) string {

	// to convert a float number to a string
	return strconv.FormatFloat(input_num, 'f', 3, 64)
}

func RemoveStrinInArray(stringArray []string, slice string) []string {
	for i, v := range stringArray {
		if v == slice {
			return append(stringArray[:i], stringArray[i+1:]...)
		}
	}
	return stringArray
}

func IsFeatureDateWithoutUtc(eventStartyDateTime string) string {

	IsFeatureEvent := "false"

	layout := "2006-01-02T15:04:05Z07:00"
	then, err := time.Parse(layout, eventStartyDateTime)
	if err == nil {
		tempFormatDate := then.Format("2006-01-02")
		then, _ = time.Parse(layout, tempFormatDate+"T00:00:00Z")
		dte := time.Now()
		tempCurrentFormatDate := dte.Format("2006-01-02")
		dte, _ = time.Parse(layout, tempCurrentFormatDate+"T00:00:00Z")
		if then.After(dte) || then.Equal(dte) {
			IsFeatureEvent = "true"
		}
	}
	return IsFeatureEvent
}

func UniqueDivisionappend(tempAllEventRegisterDivision []string, v string) []string {
	result := false
	for _, data := range tempAllEventRegisterDivision {
		fmt.Println(v)
		if v == data {
			result = true
		}
	}

	if !result {
		tempAllEventRegisterDivision = append(tempAllEventRegisterDivision, v)
	}
	return tempAllEventRegisterDivision
}

func ConvertAmountIntoFormat(amountValue string) string {

	ac := accounting.Accounting{Symbol: "", Precision: 2}
	additionalQuantityTotal := new(big.Float)
	additionalQuantityTotal.SetString(amountValue)

	return ac.FormatMoneyBigFloat(additionalQuantityTotal)
}

func ConvertUnitToCurrencySynbol(currencycode string) string {
	result := ""
	unit, err_unit := currency.ParseISO(currencycode)
	if err_unit == nil {
		currencySymbol := currency.Symbol(unit)
		var ty interface{}
		ty = currencySymbol
		result = fmt.Sprintf("%v", ty)
	}
	return result
}
func RemoveDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func StringInArray(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}

func Checkeventoradd(indexnumer int) bool {
	flag := false
	if indexnumer%2 == 0 {
		flag = true
	}
	return flag
}

func CheckImageExistOrNotS3Bucket(image string) string {

	downloadfile := "false"

	response, e := http.Get(image)

	fmt.Println(image)

	if response != nil {
		if response.StatusCode == 200 {
			downloadfile = "true"
		}
	}

	if e == nil {
		defer response.Body.Close()
	}

	return downloadfile

}

func AllSameStrings(a []string) bool {
	for i := 1; i < len(a); i++ {
		if a[i] != a[0] {
			return false
		}
	}
	return true
}
