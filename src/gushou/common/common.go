package common

import (
	"encoding/json"
	"fmt"
	"gushou/constants"
	"gushou/smscommon"
	"os"
	"os/exec"
	"strings"

	"github.com/mailgun/mailgun-go"
)

type StripeConnectAccountSucessResponse struct {
	Access_token           string `json:"access_token"`
	Livemode               bool   `json:"livemode"`
	Refresh_token          string `json:"refresh_token"`
	Token_type             string `json:"token_type"`
	Stripe_publishable_key string `json:"stripe_publishable_key"`
	Stripe_user_id         string `json:"stripe_user_id"`
	Scope                  string `json:"scope"`
}

type StripeConnectAccountErrorResponse struct {
	Error             string `json:"error"`
	Error_Description string `json:"error_description"`
}

func SendEmail(toEmail, subject, emailMessage string) (string, error) {
	//send mail
	fmt.Printf("\n In common send email function")
	host_name := os.Getenv("HOST_NAME")
	var send_email_to string
	if host_name == "gogushoutesting" {
		send_email_to = "silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com, johnsonp@ihorsetechnologies.com,venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
	} else if host_name == "gogushoulive" {
		if constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT == subject {
			receiver_email := strings.Split(toEmail, "-")
			send_email_to = receiver_email[0]
		} else {
			send_email_to = toEmail
		}

	} else {
		//send_email_to = "faiz@dccper.com";
		send_email_to = "venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
		//send_email_to = toEmail
	}

	domain := "gogushou.com"
	api_key := os.Getenv("MAILGUN_API_KEY")
	//fmt.Println(send_email_to)
	mg := mailgun.NewMailgun(domain, api_key)
	email_message := mailgun.NewMessage(
		"Gushou Dragon Boat <dragonboat@gogushou.com>",
		subject,
		"",
		send_email_to)

	email_message.SetHtml(emailMessage)

	if constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT == subject {

		toCC := strings.Split(toEmail, "-")

		if host_name == "gogushoutesting" {
			email_message.AddCC("silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com, venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
		} else if host_name == "gogushoulive" {
			if len(toCC[1]) > 0 {
				email_message.AddCC(toCC[1])
			}
		} else {
			email_message.AddCC("venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
		}

	}

	resp, id, err := mg.Send(email_message)
	if err != nil {
		fmt.Println("ERROR : ", err)
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)

	return resp, err
}

func SendDiscussionEmail(toEmail, subject, emailMessage string) (string, error) {
	//send mail
	fmt.Printf("\n In common send discussion email function")
	host_name := os.Getenv("HOST_NAME")
	var send_email_to string
	if host_name == "gogushoutesting" {
		send_email_to = "silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com, johnsonp@ihorsetechnologies.com"
	} else if host_name == "gogushoulive" {

		send_email_to = toEmail

	} else {

		send_email_to = "venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"

	}

	domain := "gogushou.com"
	api_key := os.Getenv("MAILGUN_API_KEY")
	//fmt.Println(send_email_to)
	mg := mailgun.NewMailgun(domain, api_key)
	email_message := mailgun.NewMessage(
		"Gushou Dragon Boat <dragonboat@gogushou.com>",
		subject,
		"",
		send_email_to)

	email_message.SetHtml(emailMessage)

	resp, id, err := mg.Send(email_message)
	if err != nil {
		fmt.Println("ERROR : ", err)
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)

	return resp, err
}

func InArray(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}

func SendEmailWithCC(toEmail, subject, emailMessage string) (string, error) {
	//send mail
	fmt.Printf("\n In common send email function")
	host_name := os.Getenv("HOST_NAME")
	var send_email_to string
	if host_name == "gogushoutesting" {
		send_email_to = "silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com,venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
	} else if host_name == "gogushoulive" {
		receiver_email := strings.Split(toEmail, "-")
		send_email_to = receiver_email[0]
	} else {
		//send_email_to = "faiz@dccper.com";
		send_email_to = "venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
	}

	domain := "gogushou.com"
	api_key := os.Getenv("MAILGUN_API_KEY")
	//fmt.Println(send_email_to)
	mg := mailgun.NewMailgun(domain, api_key)
	email_message := mailgun.NewMessage(
		"Gushou Dragon Boat <dragonboat@gogushou.com>",
		subject,
		"",
		send_email_to)

	email_message.SetHtml(emailMessage)

	toCC := strings.Split(toEmail, "-")

	if host_name == "gogushoutesting" {
		email_message.AddCC("silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com,venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
	} else if host_name == "gogushoulive" {
		if len(toCC[1]) > 0 {
			email_message.AddCC(toCC[1])
		}
	} else {
		email_message.AddCC("venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
	}

	resp, id, err := mg.Send(email_message)
	if err != nil {
		fmt.Println("ERROR : ", err)
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)

	return resp, err
}

func SendEmailWithBCC(toEmail, subject, emailMessage string) (string, error) {
	//send mail
	fmt.Printf("\n In common send email function")
	host_name := os.Getenv("HOST_NAME")
	var send_email_to string
	if host_name == "gogushoutesting" {
		send_email_to = "silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com,venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
	} else if host_name == "gogushoulive" {
		receiver_email := strings.Split(toEmail, "-")
		send_email_to = receiver_email[0]
	} else {
		//send_email_to = "faiz@dccper.com";
		receiver_email := strings.Split(toEmail, "-")
		send_email_to = receiver_email[0]
		send_email_to = "venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com"
	}

	domain := "gogushou.com"
	api_key := os.Getenv("MAILGUN_API_KEY")
	//fmt.Println(send_email_to)
	mg := mailgun.NewMailgun(domain, api_key)
	email_message := mailgun.NewMessage(
		"Gushou Dragon Boat <dragonboat@gogushou.com>",
		subject,
		"",
		send_email_to)

	email_message.SetHtml(emailMessage)

	toCC := strings.Split(toEmail, "-")

	if host_name == "gogushoutesting" {
		email_message.AddBCC("silverio@dccper.com, faiz@dccper.com, chrissy@gogushou.com, tyler@gogushou.com, venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
	} else if host_name == "gogushoulive" {
		if len(toCC[1]) > 0 {
			email_message.AddBCC(toCC[1])
		}
	} else {
		email_message.AddBCC("venkatesang@ihorsetechnologies.com,saranyat@ihorsetechnologies.com,nandhinidevib@ihorsetechnologies.com")
	}

	resp, id, err := mg.Send(email_message)
	if err != nil {
		fmt.Println("ERROR : ", err)
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)

	return resp, err
}

func GetConnetStripeUserAccountIdUrl(accesscode string) (StripeConnectAccountSucessResponse, StripeConnectAccountErrorResponse) {

	fmt.Printf("\n In GetConnetStripeUserAccountIdUrl")
	createMsgcurl := exec.Command("curl", "https://connect.stripe.com/oauth/token", "-X", "POST", "--data-urlencode", "client_secret="+constants.STRIPE_ENV_SECRET_KEY, "--data-urlencode", "code="+accesscode, "--data-urlencode", "grant_type=authorization_code")
	var successResponse StripeConnectAccountSucessResponse
	var errorResponse StripeConnectAccountErrorResponse

	msgOut, err := createMsgcurl.Output()
	if err != nil {
		fmt.Println("error while sending the sms", err)
	} else {
		msgStr := smscommon.BytesToString(msgOut)

		if msgStr != "" {
			err1 := json.Unmarshal([]byte(msgStr), &successResponse)
			if err1 == nil {
				fmt.Println(successResponse)
			}
			err2 := json.Unmarshal([]byte(msgStr), &errorResponse)
			if err2 == nil {
				fmt.Println(errorResponse)
			}
		}
		fmt.Println(msgStr)
	}

	fmt.Printf("\n Out GetConnetStripeUserAccountIdUrl")
	return successResponse, errorResponse
}
