package main

import (
	_ "crypto/sha256"
	_ "encoding/base64"
	"fmt"
	"gushou/common"
	"gushou/controllers"
	"gushou/models"
	_ "gushou/routers"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/orm"
	_ "github.com/astaxie/beego/session"
	"github.com/beego/i18n"
	_ "github.com/lib/pq"
	_ "golang.org/x/crypto/bcrypt"
)

/*const (
    DB_USER     = "gushouuser"
    DB_PASSWORD = "gogushou19"
    DB_NAME     = "ebdb"
    DB_HOST 	= "aa1lnhl0vjbm11a.c91q2ffmdwqv.us-west-2.rds.amazonaws.com"
    DB_PORT 	= "5432"
)*/

var DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT string

func init() {
	host_name := os.Getenv("HOST_NAME")

	if host_name == "gogushoutesting" || host_name == "gogushoulive" || host_name == "gogushouihorsetesting" {

		fmt.Printf(host_name)

		DB_USER = os.Getenv("DB_USER")
		DB_PASSWORD = os.Getenv("DB_PASSWORD")
		DB_NAME = os.Getenv("DB_NAME")
		DB_HOST = os.Getenv("DB_HOST")
		DB_PORT = os.Getenv("DB_PORT")

	} else {
		//localhost
		fmt.Printf("IN localhost ")

		DB_USER = "postgres"
		DB_PASSWORD = "gogushou19"
		DB_NAME = "ebdb"
		DB_HOST = "127.0.0.1"
		DB_PORT = "5432"

		const SITE_URL = "localhost:5000/"

	}
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s port=%s host=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME, DB_PORT, DB_HOST)

	orm.RegisterDriver("pq", orm.DRPostgres)
	orm.RegisterDataBase("default", "postgres", dbinfo)
	orm.DefaultTimeLoc = time.UTC
	layout := "01/02/2006"
	str := "01/23/2017"
	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(t.String())
	}
}

func format_name(in string) (out string) {
	tmp := strings.Split(in, "_")
	name := strings.Join(tmp[0:len(tmp)-1], "")

	return name
}
func format_divisions(in string) (out map[string]string) {
	s := strings.Replace(in, "{", "", -1)
	s = strings.Replace(s, "}", "", -1)
	array := strings.Split(s, ",")

	return_array := make(map[string]string)
	for _, v := range array {
		key := models.GetFullDivName(v)
		return_array[key] = v
	}
	return return_array
}
func convertToString(in int) (out string) {
	//id, _ = strconv.Itoa(in)
	return strconv.Itoa(in)
}
func convertToInt(in string) (out int) {
	num, err := strconv.Atoi(in)
	out = 0
	if err == nil {
		out = num
	}
	return out
}
func convertToFloat(in string) (out float64) {
	num, err := strconv.ParseFloat(in, 64)
	out = 0
	if err == nil {
		out = num
	}
	return out
}
func increment(in string) (out string) {
	val, err := strconv.Atoi(in)
	if err == nil {
		val = val + 1
	}
	return strconv.Itoa(val)
}
func in_array(str string, list []string) bool {
	for _, v := range list {
		v = strings.Replace(v, `"`, "", -1)
		v = strings.Replace(v, `"`, "", -1)
		if v == str {
			return true
		}
	}
	return false
}

func InterfaceToString(value interface{}) string {
	switch v := value.(type) {
	case string:
		return v
	case int:
		return strconv.Itoa(v)
		// Add whatever other types you need
	default:
		return ""
	}
}

func CheckDate(start_date string, end_date string) bool {
	result := false

	layout := "2006-01-02T15:04:05Z07:00"
	str := ""
	if end_date != "" {
		str = end_date
	} else {
		str = start_date
	}

	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {

		if t.Before(time.Now()) {
			result = true
		} else {
			result = false

		}

	}
	return result
}

func FileTypeImg(fileName string) string {
	ext := filepath.Ext(fileName)
	ext = strings.ToLower(ext)
	switch ext {
	case ".png":
		return "/static/images/png.png"
	case ".jpg":
		return "/static/images/jpg.png"
	case ".docx":
		return "/static/images/docx.png"
	case ".doc":
		return "/static/images/doc.png"
	case ".pdf":
		return "/static/images/adobe.png"
	case ".xls":
		return "/static/images/xls.png"
	case ".xlsx":
		return "/static/images/xlsx.png"
	case ".ppt":
		return "/static/images/ppt.png"
	}
	return ""
}
func linkWithoutPrefix(url string) string {
	output := ""
	if strings.Contains(url, "https://www.twitter.com/") {
		output = strings.Replace(url, "https://www.twitter.com/", "@", -1)
	} else if strings.Contains(url, "https://") {
		output = strings.Replace(url, "https://", "", -1)
	} else {
		output = strings.Replace(url, "http://", "", -1)
	}
	return strings.TrimSpace(output)
}
func linkWithoutDomain(url string) string {
	output := ""
	if strings.Contains(url, "https://www.facebook.com/") {
		output = strings.Replace(url, "https://www.facebook.com/", "", -1)
	} else if strings.Contains(url, "https://www.youtube.com/") {
		output = strings.Replace(url, "https://www.youtube.com/", "", -1)
	} else if strings.Contains(url, "https://www.twitter.com/") {
		output = strings.Replace(url, "https://www.twitter.com/", "", -1)
	}

	return strings.TrimSpace(output)
}

func IsFeaturePractice(practiceDateTime string, user_time_zone string) bool {

	IsFeaturePractice := false
	if user_time_zone != "" {
		layout := "2006-01-02T15:04:05Z07:00"
		then, err := time.Parse(layout, practiceDateTime)
		if err == nil {
			localLoc, err := time.LoadLocation(user_time_zone)
			practiceDate := then
			if err == nil {
				practiceDate = practiceDate.In(localLoc)
				dte := time.Now()
				localDateTime := dte.In(localLoc)
				if practiceDate.After(localDateTime) || practiceDate.Equal(localDateTime) {
					IsFeaturePractice = true
				}
			}

		}
	}

	return IsFeaturePractice
}

func CovertToInt(tempweek_number interface{}) int {

	switch v := tempweek_number.(type) {
	case string:
		i, _ := strconv.Atoi(v)
		return i
	case int:
		return v
		// Add whatever other types you need
	default:
		return 0
	}
}

func CovertToMonth(tempmonth interface{}) int {

	switch v := tempmonth.(type) {
	case string:
		i, _ := strconv.Atoi(v)
		return i - 1
	case int:
		return v - 1
		// Add whatever other types you need
	default:
		return 0
	}

}

func IsValidUrlLink(link string) bool {
	isValid := false
	if strings.Contains(link, "http://") || strings.Contains(link, "https://") {
		isValid = true
	}
	return isValid
}

func In_arrayCheckRosterRegisterInEvent(rosterId string, datass []orm.Params) bool {
	isValid := false
	for _, registerData := range datass {
		if registerData["eventregisterrosterid"] == rosterId {
			isValid = true
			break
		}
	}
	return isValid
}

func In_arrayCheckEventInRegisterList(eventId string, datass []orm.Params, rosterId string) bool {
	isValid := false
	if eventId != "0" {
		for _, registerData := range datass {
			if registerData["eventfk_event_id"] == eventId {
				isValid = true
				break
			}
		}
		return isValid
	}
	return isValid
}

func In_arrayCheckEventRosterInRegisterList(eventId string, datass []orm.Params, rosterId string) bool {
	isValid := false
	if eventId != "0" {
		for _, registerData := range datass {
			if registerData["eventfk_event_id"] == eventId && registerData["eventregisterrosterid"] == rosterId {
				isValid = true
				break
			}
		}
		return isValid
	}
	return isValid
}

func IsFeatureEvent(eventStartyDateTime string) bool {

	IsFeatureEvent := false
	if eventStartyDateTime != "" {
		layout := "2006-01-02T15:04:05Z07:00"
		then, err := time.Parse(layout, eventStartyDateTime)
		if err == nil {
			tempFormatDate := then.Format("2006-01-02")
			then, _ = time.Parse(layout, tempFormatDate+"T00:00:00Z")
			dte := time.Now()
			tempCurrentFormatDate := dte.Format("2006-01-02")
			dte, _ = time.Parse(layout, tempCurrentFormatDate+"T00:00:00Z")
			if then.After(dte) || then.Equal(dte) {
				IsFeatureEvent = true
			}
		}
	}

	return IsFeatureEvent
}

func IsFutureDateTask(dateTime string) bool {

	IsFutureDateTask := false
	layout := "2006-01-02T15:04:05Z07:00"
	then, err := time.Parse(layout, dateTime)
	if err == nil {
		futureDateTime := then.UTC()
		currentTime := time.Now().UTC()
		if futureDateTime.After(currentTime) || futureDateTime.Equal(currentTime) {
			IsFutureDateTask = true
		}
	}
	return IsFutureDateTask
}

func GetWaiverRequestStatus(waiverId string) bool {
	result := false
	result = models.GetWaiverRequestSendOrNot(waiverId)
	return result
}

func IsFutureDateWithSubractDays(dateTime string, subratingDays int) bool {
	IsFutureDate := false
	practiceStartDate := common.SubractDate(dateTime)
	IsFutureDate = IsFutureDateTask(practiceStartDate)
	return IsFutureDate
}

func CheckDefaultCardDetail(subscriptionCardDetail []orm.Params) string {
	defaultCardId := ""
	for _, subscriptionCardDetailData := range subscriptionCardDetail {
		if subscriptionCardDetailData["default_source"] != "nil" {
			if subscriptionCardDetailData["default_source"].(string) == "true" && subscriptionCardDetailData["card_id"] != nil {
				defaultCardId = subscriptionCardDetailData["card_id"].(string)
				break
			}
		}
	}
	return defaultCardId
}

func GetOrderCount(otherPricingId string, registerPricingOther []orm.Params) string {
	orderCount := "0"
	for _, registerPricingOtherData := range registerPricingOther {
		if registerPricingOtherData["iteam_id"] != "nil" {
			if registerPricingOtherData["iteam_id"].(string) == otherPricingId {
				orderCount = registerPricingOtherData["iteam_id_quantity"].(string)
				break
			}
		}
	}
	return orderCount
}

func GetEventDistanceType(eventrosterRegisterType []orm.Params, class string, div string) []string {
	var distanceType []string
	for _, eventrosterRegisterData := range eventrosterRegisterType {
		temp_divisionValue := ""
		temp_classValue := ""
		if eventrosterRegisterData["div"] != nil {
			temp_divisionValue = eventrosterRegisterData["div"].(string)
		} else if eventrosterRegisterData["division"] != nil {
			temp_divisionValue = eventrosterRegisterData["division"].(string)
		}

		if eventrosterRegisterData["clases"] != nil {
			temp_classValue = eventrosterRegisterData["clases"].(string)
		} else if eventrosterRegisterData["class"] != nil {
			temp_classValue = eventrosterRegisterData["class"].(string)
		}

		temps := strings.Replace(temp_divisionValue, "{", "", -1)
		temps = strings.Replace(temps, "}", "", -1)

		if temps == div && temp_classValue == class {
			if eventrosterRegisterData["distance"] != nil && eventrosterRegisterData["boat_type"] != nil {
				tempeventdistance := eventrosterRegisterData["distance"].(string)
				tempeventdistanceunit := "m"

				if tempeventdistance == "1000" {
					tempeventdistance = "1"
					tempeventdistanceunit = "km"
				} else if tempeventdistance == "2000" {
					tempeventdistance = "2"
					tempeventdistanceunit = "km"
				}

				distanceType = append(distanceType, tempeventdistance+tempeventdistanceunit)
				distanceType = append(distanceType, eventrosterRegisterData["boat_type"].(string))
			}
			break
		}
	}
	return distanceType
}

func GetSplitString(div string) []string {
	s := strings.Split(div, "_")
	return s
}

func CheckListContain(arrayList []string, tempValue string) bool {
	result := false
	for _, arrayListData := range arrayList {
		if arrayListData == tempValue {
			result = true
			break
		}
	}
	return result
}

func Chararrayreplace(str string) string {
	if str != "" {
		str = strings.Replace(str, `"`, "", -1)
		str = strings.Replace(str, `}`, "", -1)
		str = strings.Replace(str, `{`, "", -1)
		str = strings.Replace(str, `,`, ", ", -1)
	}
	return str
}

func GetPricingQuantityCount(otherPricingId string, registerPricingOther []orm.Params) string {
	orderCount := "0"
	for _, registerPricingOtherData := range registerPricingOther {
		if registerPricingOtherData["role_iteam_id"] != "nil" {
			if registerPricingOtherData["role_iteam_id"].(string) == otherPricingId {
				orderCount = registerPricingOtherData["role_iteam_id_quantity"].(string)
				break
			}
		}
	}
	return orderCount
}

func GetCurrencySymbol(currencytext string) string {

	if currencytext != "" {
		currencytext = common.ConvertUnitToCurrencySynbol(currencytext)
	}
	return currencytext
}

func main() {
	//orm.Debug = true
	//filter to see that user is logged in. executes only if configuration variable is true
	if beego.AppConfig.String("SessionOn") == "true" {
		var FilterUser = func(ctx *context.Context) {
			if strings.HasPrefix(ctx.Input.URL(), "/login") {
				_, ok := ctx.Input.Session("GushouSession").(int)
				if ok {
					ctx.Redirect(302, "/dashboard")
				} else {
					return
				}

			}
			if strings.HasPrefix(ctx.Input.URL(), "/logout") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/features") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/pricing") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/register") {
				_, ok := ctx.Input.Session("GushouSession").(int)
				if ok {
					ctx.Redirect(302, "/dashboard")
				} else {
					return
				}
			}
			if strings.HasPrefix(ctx.Input.URL(), "/complete-register") {
				_, ok := ctx.Input.Session("GushouSession").(int)
				if ok {
					ctx.Redirect(302, "/dashboard")
				} else {
					return
				}
			}
			if strings.HasPrefix(ctx.Input.URL(), "/teamList") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/eventList") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/forgot-password") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/team-view") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/event-view") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/verify-email") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/reset-password") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/subscribe-newsletter") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/co-organizer-invite") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/resend-verification-link") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/local-timezone-session") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/twiliorequest") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/twiliofailrequest") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/subscription_update") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/subscription_canceled") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/invoice_canceled") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/invoice_failed_success_update") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/invoice_payment_success") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/invoice_created") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/invoice_update") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/activate-free-trial-for-existing-users") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/team-attendence") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/webhook/accept_stripe_connect_account") {
				return
			}

			if strings.HasPrefix(ctx.Input.URL(), "/stripe/downloadeventregistrationinvoice") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/event/download_accrediation_pdf") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/emailwaiver") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/under-18-upload-signed-team-waiver") {
				return
			}

			if ctx.Input.URL() == "/" {
				_, ok := ctx.Input.Session("GushouSession").(int)
				if ok {
					ctx.Redirect(302, "/dashboard")
				} else {
					return
				}
			}

			_, ok := ctx.Input.Session("GushouSession").(int)
			IsValidUser := false

			if ctx.Input.Session("UserId") != nil {
				id := ctx.Input.Session("UserId").(int)
				user_id := strconv.Itoa(id)
				IsValidUser = models.IsValidUser(user_id)
			}

			if !ok {
				fmt.Println("No session")

				if strings.HasPrefix(ctx.Input.URL(), "/dashboard") || strings.HasPrefix(ctx.Input.URL(), "/waiver-all") || strings.HasPrefix(ctx.Input.URL(), "/notification-all") || strings.HasPrefix(ctx.Input.URL(), "/profile-setting") {
					ctx.Redirect(302, "/login")
				} else {
					ctx.Redirect(302, "/")
				}

			}

			if !IsValidUser {
				fmt.Println("Invalid user")
				if strings.HasPrefix(ctx.Input.URL(), "/dashboard") || strings.HasPrefix(ctx.Input.URL(), "/waiver-all") || strings.HasPrefix(ctx.Input.URL(), "/notification-all") || strings.HasPrefix(ctx.Input.URL(), "/profile-setting") {
					ctx.Redirect(302, "/login")
				} else {
					ctx.Redirect(302, "/logout")
				}
			}

			beego.ErrorController(&controllers.ErrorController{})

		}
		beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	}

	beego.AddFuncMap("format_name", format_name)
	beego.AddFuncMap("increment", increment)
	beego.AddFuncMap("format_divisions", format_divisions)
	beego.AddFuncMap("convertToString", convertToString)
	beego.AddFuncMap("convertToInt", convertToInt)
	beego.AddFuncMap("convertToFloat", convertToFloat)
	beego.AddFuncMap("GetDiscussionReply", models.GetDiscussionReply)
	beego.AddFuncMap("GetTotalDiscussionReply", models.GetTotalDiscussionReply)
	beego.AddFuncMap("GetFullDivName", models.GetFullDivName)
	beego.AddFuncMap("GetEventDivision", models.GetEventDivision)
	beego.AddFuncMap("GetTeamDivision", models.GetTeamDivision)
	beego.AddFuncMap("GetWaiverMembers", models.GetWaiverMembers)
	beego.AddFuncMap("GetEventSlug", models.GetEventSlug)
	beego.AddFuncMap("GetTeamSlug", models.GetTeamSlug)
	beego.AddFuncMap("CheckDate", CheckDate)
	beego.AddFuncMap("InterfaceToString", InterfaceToString)
	beego.AddFuncMap("in_array", in_array)
	beego.AddFuncMap("FileTypeImg", FileTypeImg)
	beego.AddFuncMap("linkWithoutPrefix", linkWithoutPrefix)
	beego.AddFuncMap("linkWithoutDomain", linkWithoutDomain)
	beego.AddFuncMap("GetUserDetails", models.GetUserDetails)
	beego.AddFuncMap("GetPracticeResponse", models.GetPracticeResponse)
	beego.AddFuncMap("IsFeaturePractice", IsFeaturePractice)
	beego.AddFuncMap("CovertToInt", CovertToInt)
	beego.AddFuncMap("CovertToMonth", CovertToMonth)
	beego.AddFuncMap("IsValidUrlLink", IsValidUrlLink)
	beego.AddFuncMap("i18n", i18n.Tr)
	beego.AddFuncMap("in_arrayCheckRosterRegisterInEvent", In_arrayCheckRosterRegisterInEvent)
	beego.AddFuncMap("in_arrayCheckEventRegisterInEvent", In_arrayCheckEventInRegisterList)
	beego.AddFuncMap("in_arrayCheckEventRosterInRegisterList", In_arrayCheckEventRosterInRegisterList)
	beego.AddFuncMap("isFeatureEvent", IsFeatureEvent)
	beego.AddFuncMap("isFutureDate", IsFutureDateTask)
	beego.AddFuncMap("GetWaiverRequestStatus", GetWaiverRequestStatus)
	beego.AddFuncMap("isFutureDateWithSubractDays", IsFutureDateWithSubractDays)
	beego.AddFuncMap("checkDefaultCardDetail", CheckDefaultCardDetail)
	beego.AddFuncMap("getOrderCount", GetOrderCount)
	beego.AddFuncMap("getEventDistanceType", GetEventDistanceType)
	beego.AddFuncMap("getSplitString", GetSplitString)
	beego.AddFuncMap("checkListContain", CheckListContain)
	beego.AddFuncMap("chararrayreplace", Chararrayreplace)
	beego.AddFuncMap("getPricingQuantityCount", GetPricingQuantityCount)
	beego.AddFuncMap("getCurrencySymbol", GetCurrencySymbol)
	beego.SetStaticPath("/static", "static")

	beego.Run()
}
