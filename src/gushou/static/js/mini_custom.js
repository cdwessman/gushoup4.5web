//Team comment reply toggle
//$(".reply").click(function(){
//$(this).parents(".team-discussions-comment").first().find(".post-reply").first().slideToggle();
//});

var team_mem = [];
var team_members_update = [];
var team_message_members = [];
var SEC_NUM = 200;
var ONE_SEC = 1000;
var TWO_SEC = 2000;
var THREE_SEC = 3000;	
var FOUR_SEC = 4000;
var FIVE_SEC = 5000;
var EIGHT_SEC = 8000;
var TEN_SEC = 10000;
var ONE_MIN = 60000;
var waiverDetailWaiverName = ""
var smsSendwithDissucOrReminder = ""
var freetrialPopup = false
var subscriptionPopup = false
var createeventdiv_calss_dis_boattypeVals = []

function detectIE() {
	var ua = window.navigator.userAgent;
	var ie = ua.search(/(MSIE|Trident|Edge)/);

	return ie > -1;
}

function reloadAsGet() {
	var loc = window.location;
	var slash = loc.pathname.substr(loc.pathname.length - 1)
	var pathname = loc.pathname

	if (slash == "/") {
		//console.log(loc.protocol + '//' + loc.host + loc.pathname + loc.search + "#add-discussion")
		//window.location = loc.protocol + '//' + loc.host + loc.pathname + loc.search + loc.hash;
		location.reload()
	}
	else {
		window.location = loc.protocol + '//' + loc.host + loc.pathname + loc.search + "/#add-discussion";
	}

}
$(window).on('load', function () {
	// PAGE IS FULLY LOADED

	// save last url path in session used during login to redirect concern page.
	if ($("#header-login-url").length > 0) {
		var locationPathName = window.location.pathname
		if (locationPathName != "/login" && locationPathName != "/features" && locationPathName != "/") {
			sessionStorage.setItem('ridirecturl', locationPathName)
		}
	} else {
		sessionStorage.removeItem('ridirecturl')
	}

	if ($("#redirecturlvalue").length > 0 && sessionStorage.getItem("ridirecturl")) {
		$("#redirecturlvalue").val(sessionStorage.getItem("ridirecturl"))
	}
	// end 

	$('#general-acc-tab').attr('style', 'display: none;');
	$('#general-accreditation-li').removeClass('active');
	$('#manual-accreditation-li').removeClass('active');
	$('#manual-acc-tab').attr('style', 'display: none;');
	$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);

	sessionStorage.setItem('requesttostripe', "false")
	sessionStorage.getItem('orderlogstatus')
	if(sessionStorage.getItem('orderlogstatus') == "show") {
		$(".changeOrderLog").css('display', '');		
		$("#orderarrowicon").attr('src',"/static/images/arrow_down.png")
		sessionStorage.removeItem('orderlogstatus')
	}
	$(".changeAdditionalQuestionLog").hasClass('hide')
	$(".changeOrderLog").hasClass('hide')
	localStorage.getItem("str_from")
	console.log("str-fromm==",localStorage.getItem("str_from"))

	if(localStorage.getItem("str_from") == "eventRegister"){
		$('#eventname').hide();
		$('#eventteamname').hide();
		$('#eventpayment').hide();
		$('#teampayment').hide();
	}
	else if (localStorage.getItem("str_from") =="eventpayment") {
		
		$('#eventteamname').hide();
		$('#eventregister').hide();
		$('#teampayment').hide();
		$('#teamnameforteamtab').hide();
	}
	else if (localStorage.getItem("str_from") =="teampayment") {
		$('#eventname').hide();
		$('#eventteamname').hide();
		$('#eventregister').hide();
		$('#eventpayment').hide();
	}
	else if (localStorage.getItem("str_from") =="eventteam"){
		$('#eventregister').hide();
		$('#eventpayment').hide();
		$('#teampayment').hide();
		$('#teamnameforteamtab').hide();
	}
	sessionStorage.removeItem("changecardmissing")

	$('#header-login-url').attr('href', url);
	$('#subscriptioncardText').text("")
	$('#eventMainPage').removeClass('hide');
	$('#eventRegisterDetailList').addClass('hide');
	$('[data-toggle="tooltip"]').tooltip();
	$('#waiverhed').addClass('hide')
	$('#waiverback').addClass('hide')
	$('[data-toggle="popover"]').popover()
	$('#createLineupHide').addClass('hide')
	$('#clickfreeTrialPopuphide').addClass('hide');
	$('#bypricepersontablehideshow').addClass('hide')
	$('#byotherperson').addClass('hide')
	$('#whatwaspricehideshow').addClass('hide')
	//$('#uploadnewaccimage').addClass('hide')
	
	var teampDivisionClassChangetruefalse = false

  if(show_TeamPayment_Page_Status){
		sessionStorage.setItem('activeTab',"#team_event_payment")
	}else if(show_Event_Register_Page_Status){
		sessionStorage.setItem('activeTab',"#event_register")
	}

	if (typeof (Storage) !== "undefined") {
		activeTab = sessionStorage.getItem('activeTab')
		if (activeTab && loginScriptStatus) {
			if (activeTab == "#members" && $('#messageTabClick').length) {
				$('#messageTabClick').trigger('click');
				if (sessionStorage.getItem("#activeTabmembers")) {
					teampPage = sessionStorage.getItem("#activeTabmembers")
					$('#' + teampPage).trigger('click');
				}
			} else if (activeTab == "#waiver" && $('#waiverTabClick').length) {
				$('#waiverTabClick').trigger('click');
			} else if (activeTab == "#roaster" && $('#rosterTabClick').length) {
				$('#rosterTabClick').trigger('click');
			} else if (activeTab == "#team_payments" && $('#paymentTabClick').length) {
				$('#paymentTabClick').trigger('click');
			}
			else if (activeTab == "#lineup" && $('#lineupTabClick').length) {
				$('#lineupTabClick').trigger('click');
				lineUpCreate = localStorage.getItem('lineUpCreate')
				if (lineUpCreate == "true") {
					localStorage.removeItem('lineUpCreate')
					setTimeout(function () {
						$('#createLineup').trigger('click');
					}, 300);
				}
			} else if (activeTab == "#billing" && $('#cardBillingTabClick').length) {
				$('#cardBillingTabClick').trigger('click');
				$('#cardBillingPage').removeClass('hide');
				$('#personalPage').addClass('hide');
			} else if (activeTab == "#home" && $('#homeTabClick').length) {
				$('#homeTabClick').trigger('click');
			} else if (activeTab == "#event_register" && $('#eventRegisterTabClick').length && show_Event_Register_Page_Status) {
				$('#eventRegisterTabClick').trigger('click');
			}	else if (activeTab == "#team_event_payment" && $('#paymentTabClick').length && show_TeamPayment_Page_Status) {
					$('#paymentTabClick').trigger('click');
			} else {
				$('#homeTabClick').trigger('click');
			}
		} else {
			$('#homeTabClick').trigger('click');
		}
	} else {
		$('#homeTabClick').trigger('click');
	}

	/** View Event Page mannul activate the tab  */
	eventactiveTab = sessionStorage.getItem('eventactiveTab')
	if (eventactiveTab == "#eventTeamList" && $('#eventTeamListTabClick').length) {
		$('#eventTeamListTabClick').tab('show');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventMainPage').addClass('hide');
		$('#eventTeamList').removeClass('hide');
		$('#eventPaymentList').addClass('hide');
		$('#eventaccreditation').addClass('hide');
		$('#eventRosterRacingList').addClass('hide');
		$('#eventWaiverList').addClass('hide');
		$('#eventaccreditationList').addClass('hide');
	}
	
	else if (eventactiveTab == "#eventPaymentList" && $('#eventPaymentListTabClick').length) {
		$('#eventPaymentListTabClick').tab('show');
		$('#eventTeamList').addClass('hide');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventMainPage').addClass('hide');
		$('#eventPaymentList').removeClass('hide');
		$('#eventaccreditation').addClass('hide');
		$('#eventRosterRacingList').addClass('hide');
		$('#eventWaiverList').addClass('hide');
		$('#eventaccreditationList').addClass('hide');

	} else if (eventactiveTab == "#eventaccreditation" && $('#eventaccreditationTabClick').length) {
		$('#eventaccreditationTabClick').tab('show');
		$('#eventPaymentList').addClass('hide');
		$('#eventTeamList').addClass('hide');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventMainPage').addClass('hide');
		$('#eventPaymentList').addClass('hide');
		$('#eventRosterRacingList').addClass('hide');
		$('#eventWaiverList').addClass('hide');
		activeaccretab = sessionStorage.getItem('activeaccreditation')
		if (activeaccretab == "#General" && $('#general-accreditation').length) {
			$('#general-accreditation-li').addClass('active');
			$('#general-acc-tab').attr('style', 'display: block;');
			$('#manual-accreditation-li').removeClass('active');
			$('#manual-acc-tab').attr('style', 'display: none;');
		}else if (activeaccretab == "#Manual" && $('#manual-accreditation').length) {
			$('#manual-accreditation-li').addClass('active');
			$('#manual-acc-tab').attr('style', 'display: block;');
			$('#general-accreditation-li').removeClass('active');
			$('#general-acc-tab').attr('style', 'display: none;');

		}
		
	}else if (eventactiveTab == "#eventRosterRacingList" && $('#eventRosterRacingListTabClick').length) {
	
		$('#eventRosterRacingListTabClick').tab('show');
		$('#eventPaymentList').addClass('hide');
		$('#eventTeamList').addClass('hide');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventMainPage').addClass('hide');
		$('#eventPaymentList').addClass('hide');
		$('#eventRosterRacingList').removeClass('hide');
		$('#eventaccreditationList').addClass('hide');
		$('#eventWaiverList').addClass('hide');
		
		
	} else if (eventactiveTab == "#eventWaiverList" && $('#eventWaiversListTabClick').length) { 
   
		$('#eventWaiversListTabClick').tab('show');  
		$('#eventPaymentList').addClass('hide'); 
		$('#eventTeamList').addClass('hide'); 
		$('#eventRegisterDetailList').addClass('hide'); 
		$('#eventMainPage').addClass('hide'); 
		$('#eventPaymentList').addClass('hide'); 
		$('#eventRosterRacingList').addClass('hide'); 
		$('#eventaccreditationList').addClass('hide'); 
		$('#eventWaiverList').removeClass('hide'); 
		 
		 
	  } else {
		
		$('#eventHomeListTabClick').tab('show');
		$('#eventMainPage').removeClass('hide');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventTeamList').addClass('hide');
		$('#eventPaymentList').addClass('hide');
		$('#eventRosterRacingList').addClass('hide');
		$('#eventaccreditationList').addClass('hide');
		$('#eventWaiverList').addClass('hide');
		sessionStorage.setItem('eventactiveTab', 'eventhomeactivetab')
	}

	/** end */

	$(document).on('click', ".teamTabClick", function (e) {
		e.preventDefault()
		sessionStorage.setItem('activeTab', $(this).attr('value'))
		if ($(this).attr('value') == "#event_register") {
			window.location.href = "/team-view/" + $('#current_team_slug').val() + "/event_register";
		}else if ($(this).attr('value') == "#team_event_payment") {
			window.location.href = "/team-view/" + $('#current_team_slug').val() + "/team_event_payment";
		}
	});

	if (loginScriptStatus && isTeamViewPage && teamCaptainStatus && team_view_popupnotification == "true" && !superAdminStatus && teamFreeTrialPeriod == "false" && teamSubscriptionPeriod == "false" && teamSubscriptionEndDate == "") {
		$('#teamsubscriptionmodel').modal('show')
		subscriptionPopup = true
		$('#teammodelheaderParttext').text("Option to Upgrade")
		$('#appendHeaderText').html("<p>We hope you are enjoying Gushou's premium features.</p> <p>Your free-trial has expired. </p> <p>You can continue using Gushou's premium features by upgrading now or you can skip this step and continue on Gushou's basic plan!</p>")
		$('#clicktosubscriptionhide').removeClass('hide')
		$('#activeTeamCaptainhideshow').removeClass('hide')
		$('#activeBasicUserhideshow').addClass('hide')
	} else if (loginScriptStatus && isTeamViewPage && teamCoCaptainStatus && team_view_popupnotification == "true" && teamFreeTrialPeriod == "false" && teamSubscriptionPeriod == "false" && !superAdminStatus) {
		$('#subscriptionteammodel').modal('show')
		freetrialPopup = false
		$('#subcribeteammodelheaderParttext').text("Option to Upgrade")
		$('#clicktosubscriptionhide').addClass('hide')
		$('#clickfreeTrialPopuphide').addClass('hide');
		$('#subcribeteammodelheaderParthide').addClass('hide');
		$('#subcribeteammodelheading').text("Please ask your team captain to upgrade to Premium Package to access text messages.")
	} else if (loginScriptStatus && isTeamViewPage && teamCaptainStatus && !superAdminStatus && freetrialNotificationPopupStatus == "true" && isFreeTrialAvailable == "true") {
		$('#subcribeteammodelheaderParttext').text("Option to Upgrade")
		$('#subscriptionteammodel').modal('show')
		freetrialPopup = true
		$('#subcribeteammodelheaderParttext').text("Reminder")
		$('#subcribeteammodelheaderParthide').removeClass('hide');
		$('#clickfreeTrialPopuphide').removeClass('hide');
		if (noOfDaysRemaining == 0) {
			$('#subcribeteammodelheading').html("<p>We hope you are enjoying Gushou's premium features.</p> <p>This is a reminder that your free-trial for <b style=color:#bf5523>" + teamnametext + "</b> is expiring <b> Today </b>.</p>")
		} else {
			$('#subcribeteammodelheading').html("<p>We hope you are enjoying Gushou's premium features.</p> <p>This is a reminder that your free-trial for <b style=color:#bf5523>" + teamnametext + "</b> is expiring in <b>" + noOfDaysRemaining + " days</b>.</p>")
		}
	}

	if (loginScriptStatus == 1) {
		activeArchiveTab = sessionStorage.getItem('activeEvent')
		if (activeArchiveTab) {
			sessionStorage.removeItem('activeEvent')
			if (activeArchiveTab == "#recent") {
				$("#recent").trigger("click");
			} else if (activeArchiveTab == "#archive") {
				$("#archive").trigger("click");
			} else if (activeArchiveTab == "#upcoming") {
				$("#upcoming").trigger("click");
			}
		}
	}
	$('#loading-spinner').removeClass('hide');


	if ($(".rostercountchange").length > 0) {
		var temprostercount = 0
		$(".temprostercount").each(function () {
			temprostercount = temprostercount + 1
		})
		if (temprostercount != 0) {
			$(".rostercountchange").text(Number(temprostercount))
		}
	}

	if ($(".lineupcountchange").length > 0) {
		var templineupcount = 0
		$(".templineupcount").each(function () {
			templineupcount = templineupcount + 1
		})
		if (templineupcount != 0) {
			$(".lineupcountchange").text(Number(templineupcount))
		}
	}

	if ($(".event_registeredcountchange").length > 0) {
		var tempregisteredcount = 0
		$(".tempregisteredcount").each(function () {
			tempregisteredcount = tempregisteredcount + 1
		})
		if (tempregisteredcount != 0) {
			$(".event_registeredcountchange").text(Number(tempregisteredcount))
		}
	}

	if ($(".event_registeredpaymentcounts").length > 0) {
		var tempregisteredcount = 0
		$(".teamregisteredpaymentcounts").each(function () {
			tempregisteredcount = tempregisteredcount + 1
		})
		if (tempregisteredcount != 0) {
			$(".event_registeredpaymentcounts").text(Number(tempregisteredcount))
		}
	}

	$(".isPhoneNumValid").each(function () {
		if ($.trim($(this).text()) == "-") {
			$(this).empty();
			$.trim($(this).append("<i class='fa fa-phone'></i>"));
		}
	})

	if ($(".dashboard-notifications").find("p.local-date-time").length > 0) {
		$(".dashboard-notifications").find("p.local-date-time").each(function () {
			var utcTime = $.trim($(this).text());
			var utcDateTime = moment.utc(utcTime);
			var localDateTime = utcDateTime.local().format('ddd MMM DD YYYY, h:mm A');
			$(this).text(localDateTime);
		});
	}

	if ($(".notifications-all").find("p.local-date-time").length > 0) {
		$(".notifications-all").find("p.local-date-time").each(function () {
			var utcTime = $.trim($(this).text());
			var utcDateTime = moment.utc(utcTime);
			var localDateTime = utcDateTime.local().format('ddd MMM DD YYYY, h:mm A');
			$(this).text(localDateTime);
		});
	}

	$(".team-discussions-team_id").each(function () {
		var spanAttr = $(this).find("span.comment-timestamp").attr("lang");
		if (spanAttr != "" && spanAttr != undefined) {
			var utcDateTime = moment.utc(spanAttr);
			var localDateTime = utcDateTime.local().format('ddd MMM DD YYYY, h:mm A');
			$(this).find("span.comment-timestamp").text(localDateTime);
		}
	});
});
$(window).bind("load", function () {

	if (window.location.pathname.search("/waiver/") != 0) {
		$('#loading-spinner').addClass('hide');
	}

});
$(window).bind("pageshow", function () {
	var teamForm = $("#createTeamForm")
	var eventForm = $("#create_event_form")
	if (teamForm.length) {
		teamForm[0].reset();
	}
	if (eventForm.length) {
		eventForm[0].reset();
	}

});
if (!!window.performance && window.performance.navigation.type == 2) {
	//console("BacK button")
	window.location.reload();
}

//common function
$(function () {

	timeZone = moment.tz.guess()
	$('#localUserTimeZone').val(timeZone)

	$.ajax({
		type: "POST",
		url: "/local-timezone-session",
		data: { "timeZone": timeZone }
	}).done(function (msg) {
		console.log("momentguess", msg)
	});


	var IEversion = detectIE();
	if (IEversion && IEversion != false) {
		$('#check_browser').css('display', 'block');
	} else {
		$('#check_browser').css('display', 'none');
	}

	var opts = {
		lines: 13, // The number of lines to draw
		length: 10, // The length of each line
		width: 4, // The line thickness
		radius: 12, // The radius of the inner circle
		color: '#000', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false // Whether to use hardware acceleration
	};

	$('input[name="send_as"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="send_as_sms"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="response"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="edit_options"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="delete_options"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	$('input[name="reminder_email"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="reminder_sms"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
	$('input[name="subscription_amount"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
		uncheckedClass: '',
	});

	$('#subscriptionplan_Yearly').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
		uncheckedClass: '',
	});

	$('#subscriptionplan_Monthly').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
		uncheckedClass: '',
	});

	$('input[name="additional_practice_bool"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	$('input[name="submitrosterlater"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	$('input[name="waiverreminderstatus"]:radio').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});


	var trget = document.getElementById('loading-spinner');
	var spnr = new Spinner(opts).spin(trget);

	var week_cal_default = moment().startOf('isoweek').format("MMM D") + '-' + moment().endOf('isoweek').format("ll")
	$('#week_cal').text(week_cal_default)

	$('#password').strength({
		strengthClass: 'strength',
		strengthMeterClass: 'strength_meter',
		strengthButtonClass: 'button_strength',
		strengthButtonText: 'Show Password',
		strengthButtonTextToggle: 'Hide Password'
	});
	$('#new_password').strength({
		strengthClass: 'strength',
		strengthMeterClass: 'strength_meter',
		strengthButtonClass: 'button_strength',
		strengthButtonText: 'Show Password',
		strengthButtonTextToggle: 'Hide Password'
	});
	$('#resetPasswordPassword').strength({
		strengthClass: 'strength',
		strengthMeterClass: 'strength_meter',
		strengthButtonClass: 'button_strength',
		strengthButtonText: 'Show Password',
		strengthButtonTextToggle: 'Hide Password'
	});

	$.fn.focusWithoutScroll = function () {
		var x = window.scrollX, y = window.scrollY;
		this.focus();
		window.scrollTo(x, y);
		return this;
	}

	isNotEmpty = function (value) {
		value = jQuery.trim(value);
		if (value && value !== '') {
			return true;
		} else {
			return false;
		}
	};

	isUrlValid = function (url) {
		var webregex = /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
		//var webregex = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/
		//var webregex = /^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
		return webregex.test(url);
	};

	checkUrlPrefix = function (url, url_prefix) {
		if (url.match(/^http/) || url.match(/^https/)) {
			return url;
		}
		else if (url == "") {
			return url;
		}
		else {
			return url_prefix + url;
		}
	};

	$(".tooltips").click(function (e) {
		e.preventDefault();
	});
	$("#password_info").click(function (e) {
		e.preventDefault();
	})

	dateValidate = function (date1, date2) {
		if (new Date(date1) <= new Date(date2))
			return 1;
		else if (new Date(date1) > new Date(date2))
			return 0;
	};

	birdDateValidate = function (date1, date2, date3) {
		if (date1 && date2 && date3) {
			if ((new Date(date1) < new Date(date2)) && (new Date(date2) < new Date(date3))) {
				return 1;
			} else {
				return 0;
			}
		}
		else if (date1 && date2) {
			if (new Date(date1) < new Date(date2)) {
				return 1;
			} else {
				return 0;
			}

		} else if (date1 && date3) {
			if (new Date(date1) < new Date(date3)) {
				return 1;
			} else {
				return 0;
			}
		} else if (date2 && date3) {
			if (new Date(date2) < new Date(date3)) {
				return 1;
			} else {
				return 0;
			}
		} else if (date1 || date2 || date3) {
			return 1
		}
	};

	getFileExtension = function (filename) {
		var ext = /^.+\.([^.]+)$/.exec(filename);
		return ext == null ? "" : ext[1];
	};

	validateExt = function (ext) {
		ext_array = ["jpg", "png", "jpeg"]
		ext = ext.toLowerCase();
		if ($.inArray(ext, ext_array) != -1) {
			return true;
		} else {
			return false;
		}
	};

	validateExtTeamNotes = function (ext) {
		ext_array = ["jpg", "png", "jpeg", "doc", "docx", "ppt", "pptx", "svg", "txt", "pdf"]
		ext = ext.toLowerCase();
		if ($.inArray(ext, ext_array) != -1) {
			return true;
		} else {
			return false;
		}
	};

	$("#phone").on("keypress keyup blur", function (event) {
		if ((event.which < 45 || event.which > 57)) {
			if (event.which != 45) {
				event.preventDefault();
			}
		} else if (event.which == 47) {
			event.preventDefault();
		}
	});
});

// js for news-letter 
$(function () {
	$("#SponsorsPageScroll").click(function (e) {
		var elmnt = document.getElementById("SponsorsPageScrollView");
		if (elmnt != null) {
			$('html,body').animate({ scrollTop: $("#SponsorsPageScrollView").offset().top - 50 }, 'slow');
		} else {
			$('#defaultteammodel').modal("show")
			$('#defaultteammodelheading').html("<p>Currently, there are no sponsors set up on team page.</p>")
		}
	});

	$("#DiscussionsPageScroll").click(function (e) {
		var elmnt = document.getElementById("DiscussionsPageScrollView");
		if (elmnt != null) {
			//elmnt.scrollIntoView(true);
			$('html,body').animate({ scrollTop: $("#DiscussionsPageScrollView").offset().top - 50 }, 'slow');
		} else {
			$('#defaultteammodel').modal("show")
			$('#defaultteammodelheading').html("<p>Currently, there are no discussion set up on team page.</p>")
		}
	});

	$("#DocumentsPageScroll").click(function (e) {
		var elmnt = document.getElementById("Documents_Links_Contact_PageScroll");
		if (elmnt != null && $("#Documents_Links_Contact_PageScroll").parent().find('.dashboard-members-notes-border').length > 0) {
			// elmnt.scrollIntoView({behavior: "instant", block: "end", inline: "end"});
			$('html,body').animate({ scrollTop: $("#Documents_Links_Contact_PageScroll").offset().top - 50 }, 'slow');
		} else {
			$('#defaultteammodel').modal("show")
			$('#defaultteammodelheading').html("<p>Currently, there are no documents, links or contacts set up on this team page.</p>")
		}
	});

	$("#SchedulePageScroll").click(function (e) {
		var elmnt = document.getElementById("SchedulePageScrollView");
		if (elmnt != null) {
			//elmnt.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
			$('html,body').animate({ scrollTop: $("#SchedulePageScrollView").offset().top - 70 }, 'slow');
		} else {
			$('#defaultteammodel').modal("show")
			$('#defaultteammodelheading').html("<p>Currently, there are no schedule set up on team page.</p>")
		}
	});

	$("#send-newsletter button[type='submit']").click(function (e) {
		//console("here")
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('#sub_err_msgs').addClass('hide');
		$("#sub_succ_msgs").addClass('hide');
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		var email = $('#subscribe_email').val();

		var emailId = email.toLowerCase();

		if (isNotEmpty(emailId) && email_regex.test(emailId)) {
			$.ajax({
				type: "POST",
				url: "/subscribe-newsletter",
				data: { "email": emailId }
			}).done(function (msg) {
				if (msg == "true") {
					$('#sub_succ_msgs').removeClass('hide');
					$('#loading-spinner').addClass('hide');
				}
				else if (msg == "already subscribed") {
					$('#sub_err_msgs').removeClass('hide');
					$('#sub_err_msgs').text('You have already subscribed to gushou newletters');
					$('#loading-spinner').addClass('hide');
				}
				else {
					$('#sub_err_msgs').removeClass('hide');
					$('#sub_err_msgs').text(msg);
					$('#loading-spinner').addClass('hide');
				}

			});
		}
		else {
			$('#sub_err_msgs').removeClass('hide');
			$('#loading-spinner').addClass('hide');
			return false;
		}

	});
});

// js for login 
$(function () {
	$("#login-form input[type='submit']").click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('#err_msgs').removeClass('error');
		$("#err_msgs").text("");
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		var email = $('#login-username').val()
			, password = $('#login-password').val();

		var emailId

		if (email) {
			emailId = email.toLowerCase();
		}

		if (isNotEmpty(emailId) && isNotEmpty(password)) {

			$('#login-form').submit();
		}
		else if (!email_regex.test(emailId)) {

			$('#err_msgs').addClass('error');
			$("#err_msgs").text("Enter a valid email");
			$('#loading-spinner').addClass('hide');
			return false;

		}
		else if (emailId == "" && password == "") {
			$('#err_msgs').addClass('error');
			$("#err_msgs").text("Enter your email address and password");
			$('#loading-spinner').addClass('hide');
			return false;
		} else if (emailId == "") {
			$('#err_msgs').addClass('error');
			$("#err_msgs").text("Enter your email address");
			$('#loading-spinner').addClass('hide');
			return false;
		} else if (password == "") {
			$('#err_msgs').addClass('error');
			$("#err_msgs").text("Enter your password");
			$('#loading-spinner').addClass('hide');
			return false;
		}


	});
});

// js for reset password 
$(function () {
	$("#resetPasswordForm input[type='submit']").click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('.error-message').addClass('hide');
		$('.error-message').text('');
		$('#fp_succ_msgs').addClass('hide');

		var password = $('#resetPasswordPassword').val()
			, passwordConfirm = $('#resetPasswordPasswordConfirm').val();

		if (password.length < 8) {
			$('#rs_err_msgs').addClass('error');
			$("#rs_err_msgs").text('Invalid password length');
			$('#loading-spinner').addClass('hide');
			return false;
		}

		if (password == passwordConfirm) {
			$('#resetPasswordForm').submit();
		}
		else {
			$('#rs_err_msgs').addClass('error');
			$("#rs_err_msgs").text('Passwords do not match');
			$('#loading-spinner').addClass('hide');
			return false;
		}

	});
});

// js for change password 
$(function () {
	$('#change_password_submit').click(function (e) {
		//e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('#change_pswd_err').removeClass('error');
		$("#change_pswd_err").text("");

		$('#change_pswd_err2').removeClass('error');
		$("#change_pswd_err2").text("");

		var password = $('#new_password').val()
			, passwordConfirm = $('#re_password').val();


		if (password.length < 8 || passwordConfirm.length < 8) {
			$('#change_pswd_err').removeClass('hide');
			$("#change_pswd_err").text("Invalid password length");
			$('#loading-spinner').addClass('hide');
			return false;
		}

		if (password == passwordConfirm) {
			$('#change-password').submit();
		}
		else {
			$('#change_pswd_err').removeClass('hide');
			$("#change_pswd_err").text("New password and re-entered password do not match");
			$('#loading-spinner').addClass('hide');
			return false;
		}

	});
});

// js for resend verification link
$(function () {
	$('#resend_verify').click(function (e) {
		e.preventDefault()
		$(this).css('color', '#3270a7')

		$.ajax({
			type: "POST",
			url: "/resend-verification-link",
		}).done(function (msg) {
			if (msg == "true") {
				//location.reload();
			}
			else {
				//console("Error")
			}

		});

	});
});


$(function () {

	$("#submit_step1").click(function () {
		var isFormValid = true;
		var form = ""
		var name_regex = /^[a-zA-Z\d\-_.,\s]+$/;

		$("#create-team-1 input.required").each(function () {
			$('#team_name_err').addClass('hide');
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
			}
			else {
				$(this).removeClass("empty-err");
				form = "team"
			}
		});

		$("#create-event-1 input.required").each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
			}
			else {
				$('#end_date_err').addClass('hide');
				$('#event_name_err').addClass('hide');
				$(this).removeClass("empty-err");
				form = "event"
			}
		});

		if (!isFormValid) {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false
		}

		if (form == "event") {
			var event_name = $('#event_name').val();
			var location = $('#location').val();
			var start_date = $('#start_date').val();
			start_date = moment(start_date).format('YYYY-MM-DD[T]12:00:00.SSS');

			var end_date = $('#end_date').val();
			if (end_date) {
				end_date = moment(end_date).format('YYYY-MM-DD[T]12:00:00.SSS');
			}
			if (end_date) {
				var date_flag = dateValidate(start_date, end_date);
			} else {
				var date_flag = 1;
			}

			if (date_flag == 0) {
				$('#end_date_err').removeClass('hide');
				$("#end_date_err").text("The end date you entered occurs before the start date");
				return false;
			}

			/*if(!name_regex.test(event_name)){
				$('#event_name_err').removeClass('hide');
				$("#event_name_err").text("Please remove the special characters from the event name");
				$('#loading-spinner').addClass('hide');
				$("html, body").animate({ scrollTop: 0 }, "slow");
				return false
			}*/

			$.ajax({
				type: "POST",
				url: "/check-event-name",
				data: {
					"event_name": event_name,
					"location": location,
					"caller": "create"
				}
			}).done(function (msg) {
				//console(msg)
				if (msg == "exists") {
					$('#event_name_err').removeClass('hide');
					$("#event_name_err").text(event_name + " already exists in the same location");
					return false;
				}
				else {

					$("#create-event-2").toggleClass("hide");
					$("#create-event-1").toggleClass("hide");
				}

			});
		}
		else if (form == "team") {
			var location = jQuery.trim($('#teamlocation').val());
			var team_name = jQuery.trim($('#teamname').val());

			/*if(!name_regex.test(team_name)){
				$('#team_name_err').removeClass('hide');
				$("#team_name_err").text("Please remove the special characters from the team name");
				$('#loading-spinner').addClass('hide');
				$("html, body").animate({ scrollTop: 0 }, "slow");
				return false
			}*/

			$.ajax({
				type: "POST",
				url: "/check-team-name",
				data: {
					"team_name": team_name,
					"location": location,
					"caller": "create"
				}
			}).done(function (msg) {
				//console("team " + msg)
				if (msg == "exists") {
					//console(isFormValid)
					$('#team_name_err').removeClass('hide');
					//$('#team_name_err').addClass('empty-err');
					$("#team_name_err").text("This team already exists. If you are the new captain, please contact the prior captain and request to be made a captain. This will keep the team's history all in one place and make your job a heck of a lot easier! If you need assistance, please contact Gushou Support at dragonboat@gogushou.com");
					return false;
				}
				else {
					$("#create-team-2").toggleClass("hide");
					$("#create-team-1").toggleClass("hide");
				}

			});

		}

		$("html, body").animate({ scrollTop: 0 }, "slow");
		//return false;
	});

	$("#submit_step2").click(function () {
		$('#table tr').addClass("hide")
		createeventdiv_calss_dis_boattypeVals = []
		var mdivisions = [];
		var wdivisions = [];
		var mxdivisions = [];
		var classes = [];

		$('#event_matrix_classes_err').addClass('hide');
		$('#event_matrix_classes_err').text("");

		var $all_divisions = $("input[type=checkbox][name='All_divisions[]']:checked");

		var $all_boattype = $("input[type=checkbox][name='All_boattype[]']:checked");

		var $all_boatmeter = $("input[type=checkbox][name='All_boatmeter[]']:checked");


		if ($all_divisions.length > 0 || $all_boattype.length > 0 || $all_boatmeter.length > 0) {
			$('#event_classes_err').addClass('hide');
			$('#event_classes_err').text("");

			$('.error-message').addClass('hide');
			$(".error-message").text("");

			isFormValid2 = true
			$('.allboattypeselectedornot').each(function (e) {
				var $chkbox = $(this).find($('input[type="checkbox"]:checked'));
				if ($chkbox.length == 0) {
					isFormValid2 = false
				}
			});
			if (isFormValid2) {

				$('input:checkbox[name="all_div_cls_botty_meter[]"]').each(function () {
					if (this.checked) {
						createeventdiv_calss_dis_boattypeVals.push(this.value);
					}
				})

				$('#reg_price_err').addClass('hide');
				$("#reg_price_err").text("");
				$("#create-team-3").toggleClass("hide");
				$("#create-team-2").toggleClass("hide");

				$("#create-event-3").toggleClass("hide");
				$("#create-event-2").toggleClass("hide");
				$("html, body").animate({ scrollTop: 0 }, "slow");
				if (teampDivisionClassChangetruefalse) {
					teampDivisionClassChangetruefalse = false
					$('#pricing_based_selection').val(null).trigger('change');
					$("#appendPricingBasedPricingDetail").empty()
					$("#bypricepersontablehideshow").addClass('hide')
					$("#byotherperson").addClass('hide')
					$('#pricingbasedotherclear').val('')
				}

			} else {

				$('#event_matrix_classes_err').removeClass('hide');
				$('#event_matrix_classes_err').text("Please select racing for each classes");
				$("html, body").animate({ scrollTop: $('#event_matrix_classes_err').offset().top - 130 }, "slow");

			}

		}
		else {
			$('#class_div_err').removeClass('hide');
			$("#class_div_err").text("Please select the class(es) that apply to your team");
			$('#event_matrix_classes_err').addClass('hide');
			$('#event_matrix_classes_err').text("");
			$('#event_classes_err').removeClass('hide');
			$('#event_classes_err').text("Please select the class(es) that apply to your event");
		}
	});


	$("#submit_team_step2").click(function () {
		$('#table tr').addClass("hide")

		var mdivisions = [];
		var wdivisions = [];
		var mxdivisions = [];
		var classes = [];
		var $m_divisions = $("input[type=checkbox][name='M_team_divisions[]']:checked");
		if ($m_divisions.length == 0) {
			$m_divisions = $("input[type=checkbox][name='M_divisions[]']:checked");
		}
		if ($m_divisions.length) {
			mdivisions = $m_divisions.map(function () {
				return this.value;
			}).get();
			classes.push('M');
		}
		var $w_divisions = $("input[type=checkbox][name='W_team_divisions[]']:checked");
		if ($w_divisions.length == 0) {
			$w_divisions = $("input[type=checkbox][name='W_divisions[]']:checked");
		}
		if ($w_divisions.length) {
			wdivisions = $w_divisions.map(function () {
				return this.value;
			}).get();
			classes.push('W');
		}
		var $mx_divisions = $("input[type=checkbox][name='MX_team_divisions[]']:checked");
		if ($mx_divisions.length == 0) {
			$mx_divisions = $("input[type=checkbox][name='MX_divisions[]']:checked");
		}
		if ($mx_divisions.length) {
			mxdivisions = $mx_divisions.map(function () {
				return this.value;
			}).get();
			classes.push('MX');
		}

		if (classes.length) {
			var classes_divs = $.map(classes, function (val) {
				if (val == "M" && mdivisions.length > 0) {
					return divs = {
						"class": val,
						"div": mdivisions
					}
				}
				if (val == "W" && wdivisions.length > 0) {
					return divs = {
						"class": val,
						"div": wdivisions
					}
				}
				if (val == "MX" && mxdivisions.length > 0) {
					return divs = {
						"class": val,
						"div": mxdivisions
					}
				}
			});
		}


		if ($m_divisions.length > 0 || $w_divisions.length > 0 || $mx_divisions.length > 0) {
			$('#event_classes_err').addClass('hide');
			$('#event_classes_err').text("");

			$('.error-message').addClass('hide');
			$(".error-message").text("");


			//console(classes)
			//console(classes_divs)

			for (var i = 0; i < classes_divs.length; i++) {
				if (classes_divs[i].class == "M") {
					for (var j = 0; j < classes_divs[i].div.length; j++) {
						ele_id = classes_divs[i].div[j];
						$('#table tr[id="M_' + ele_id + '"]').removeClass("hide");
						$('#m_label').removeClass("hide");
					};

				}
				else if (classes_divs[i].class == "W") {
					for (var j = 0; j < classes_divs[i].div.length; j++) {
						ele_id = classes_divs[i].div[j];
						$('#table tr[id="W_' + ele_id + '"]').removeClass("hide");
						$('#w_label').removeClass("hide");
					};

				}
				else if (classes_divs[i].class == "MX") {
					for (var j = 0; j < classes_divs[i].div.length; j++) {
						ele_id = classes_divs[i].div[j];
						$('#table tr[id="MX_' + ele_id + '"]').removeClass("hide");
						$('#mx_label').removeClass("hide");
					};

				}
			};

			$("#create-team-3").toggleClass("hide");
			$("#create-team-2").toggleClass("hide");

			$("#create-event-3").toggleClass("hide");
			$("#create-event-2").toggleClass("hide");

		}
		else {
			$('#class_div_err').removeClass('hide');
			$("#class_div_err").text("Please select the class(es) that apply to your team");

			$('#event_classes_err').removeClass('hide');
			$('#event_classes_err').text("Please select the class(es) that apply to your event");
			//return false;
		}
		//$("#create-team-3").removeClass("hide");
		//$("#create-team-2").addClass("hide");



		$("html, body").animate({ scrollTop: 0 }, "slow");
		//return false;
	});

	$("#submitCreateTeam").click(function () {

		$('#twitter_err').addClass('hide');
		$('#phone_err').addClass('hide');
		$('#website_err').addClass('hide');
		$('#fb_err').addClass('hide');
		$('#youtube_err').addClass('hide');
		$('#email_err').addClass('hide');

		$('#team_email').removeClass('empty-err');
		$('#team_twitter').removeClass('empty-err');
		$('#team_website').removeClass('empty-err');
		$('#team_phone_number').removeClass('empty-err');
		$('#team_facebook').removeClass('empty-err');
		$('#team_youtube').removeClass('empty-err');

		$('#loading-spinner').removeClass('hide');

		$('input').val(function (_, value) {
			return $.trim(value);
		});

		var isFormValid = true
		var prefix = 'http://';
		var prefix2 = 'https://';
		var phone_regex = /^[0-9\s-]*$/;
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		var team_phone = $('#team_phone_number').val().trim();
		var team_email = $('#team_email').val().trim();
		var team_facebook = $('#team_facebook').val().trim();
		var team_twitter = $('#team_twitter').val().trim();
		var team_website = $('#team_website').val().trim();
		var team_youtube = $('#team_youtube').val().trim();

		var facebook_url = checkUrlPrefix(team_facebook, prefix2);
		var twitter_url = checkUrlPrefix(team_twitter, prefix2);
		var website_url = checkUrlPrefix(team_website, prefix);
		var youtube_url = checkUrlPrefix(team_youtube, prefix2);

		if (team_twitter != "") {
			if (isUrlValid(twitter_url) || team_twitter.substring(0, 1) == "@") {
				$('#twitter_err').removeClass('hide');
				$("#twitter_err").text("Invalid. Please enter only your username");
				$('#team_twitter').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false
			}
			team_twitter = "https://www.twitter.com/" + team_twitter;
		}

		if (team_facebook != "") {
			if (isUrlValid(facebook_url)) {
				$('#fb_err').removeClass('hide');
				$("#fb_err").text("Invalid. Please enter only your username");
				$('#team_facebook').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
			}
			team_facebook = "https://www.facebook.com/" + team_facebook;
		}

		if (team_youtube != "") {
			if (isUrlValid(youtube_url)) {
				$('#youtube_err').removeClass('hide');
				$("#youtube_err").text("Invalid. Please enter only your username");
				$('#team_youtube').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
			}
			team_youtube = "http://www.youtube.com/" + team_youtube;
		}

		if (team_email) {
			if (!email_regex.test(team_email)) {
				$('#email_err').removeClass('hide');
				$('#team_email').addClass('empty-err');
				$("#email_err").text("Incorrect email address. Please enter a proper email ID");
				isFormValid = false;
			}
		}
		if (!phone_regex.test(team_phone)) {
			$('#phone_err').removeClass('hide');
			$('#team_phone_number').addClass('empty-err');
			$("#phone_err").text("Please enter a proper phone number");
			isFormValid = false
		}
		if (website_url) {
			if (!isUrlValid(website_url)) {
				$('#website_err').removeClass('hide');
				$('#team_website').addClass('empty-err');
				$("#website_err").text("Invalid. Enter a proper URL");
				isFormValid = false;
			}
		}

		$("html, body").animate({ scrollTop: 0 }, "slow");
		//return false;
		if (isFormValid) {
			$('#createTeamForm').submit();
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	$("#submit_event_step3").click(function () {

		$('#reg_dates_err').addClass('hide');
		$("#reg_dates_err").text("");

		$('#reg_price_err').addClass('hide');
		$("#reg_price_err").text("");

		$('#class_succ').addClass('hide');
		$("#class_succ").text("");

		var start_date = jQuery.trim($('#start_date').val());
		var end_date = $('#end_date').val();
		var seb_date = $('#seb_date').val();
		var eb_date = $('#eb_date').val();
		var rb_date = $('#rb_date').val();
		var isFormValid = true

		var week_number = moment(start_date).isoWeek();
		var month = moment(start_date).month();
		var year = moment(start_date).year();
		start_date = moment(new Date(start_date)).format('YYYY-MM-DD[T]12:00:00.SSS');
		var same_event_id = "";


		if (seb_date != "Invalid Date" && seb_date != "")
			seb_date = moment(new Date(seb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');

		if (eb_date != "Invalid Date" && eb_date != "")
			eb_date = moment(new Date(eb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');

		if (rb_date != "Invalid Date" && rb_date != "") {
			rb_date = moment(new Date(rb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');
		}


		var bird_flag = birdDateValidate(seb_date, eb_date, rb_date);
		if ((seb_date != '' && seb_date != 'Invalid date') || (eb_date != '' && eb_date != 'Invalid date') || (rb_date != '' && rb_date != 'Invalid date')) {
			if ((rb_date != '' && rb_date != 'Invalid date' && rb_date > start_date) || (eb_date != '' && eb_date != 'Invalid date' && eb_date > start_date) || (seb_date != '' && seb_date != 'Invalid date' && seb_date > start_date)) {
				$('#reg_dates_err').removeClass('hide');
				$("#reg_dates_err").text("Pricing dates should occur before start date of the event");
				$('#loading-spinner').addClass('hide');
				if (isEditPageScript) {
					$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
				} else {
					$("html, body").animate({ scrollTop: 0 }, "slow");
				}
				isFormValid = false;
				return false;
			}
			else if (bird_flag == 0) {
				$('#reg_dates_err').removeClass('hide');
				$("#reg_dates_err").text("Each date must fall after the previous date");
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				if (isEditPageScript) {
					$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
				} else {
					$("html, body").animate({ scrollTop: 0 }, "slow");
				}
				return false;
			}
		}

		if (isFormValid) {
			isFormValid1 = true
			$('input[name="r_price[]"]').each(function () {
				teamIndexval = $(this).attr('data-index')
				teamp1staus = false
				teamp2staus = false
				teamp3staus = false
				if ($('#' + teamIndexval + "seb_price").val().trim() != "") {
					teamp1staus = true
				}

				if ($('#' + teamIndexval + "eb_price").val().trim() != "") {
					teamp2staus = true
				}

				if ($('#' + teamIndexval + "r_price").val().trim() != "") {
					teamp3staus = true
				}

			  tempvalue =$('#pricing_based_selection').val()
			 
				if(tempvalue == "PP"){
					if (teamp1staus) {

						$('#' + teamIndexval + "seb_price").removeClass('empty-err')
	
					} else {
						$('#' + teamIndexval + "seb_price").addClass('empty-err')
						isFormValid1 = false
					}

					if (teamp2staus) {
						$('#' + teamIndexval + "eb_price").removeClass('empty-err')
	
					} else {
						$('#' + teamIndexval + "eb_price").addClass('empty-err')
						isFormValid1 = false
					}

					if (teamp3staus) {
						$('#' + teamIndexval + "r_price").removeClass('empty-err')
	
					} else {
						$('#' + teamIndexval + "r_price").addClass('empty-err')
	
						isFormValid1 = false
					}
				}else {
					if ((teamp1staus && teamp2staus && teamp3staus) || (!teamp1staus && !teamp2staus && !teamp3staus)) {

						$('#' + teamIndexval + "seb_price").removeClass('empty-err')
						$('#' + teamIndexval + "eb_price").removeClass('empty-err')
						$('#' + teamIndexval + "r_price").removeClass('empty-err')
	
					} else {
						$('#' + teamIndexval + "seb_price").addClass('empty-err')
						$('#' + teamIndexval + "eb_price").addClass('empty-err')
						$('#' + teamIndexval + "r_price").addClass('empty-err')
	
						isFormValid1 = false
					}
				}
			});
			if (isFormValid1) {
				// Event Edit using this same script
				if (isEditPageScript) {
					submitEditClassDivisionPArt()
				} else {
					$("#create-event-4").removeClass("hide");
					$("#create-event-3").addClass("hide");
				}
			} else {
				$('#reg_price_err').removeClass('hide');
				$("#reg_price_err").text("Please fill the highlighted fields");
			}

		}
		if (isEditPageScript) {
			$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
		} else {
			$("html, body").animate({ scrollTop: 0 }, "slow");
		}
		return false;
	});

	function submitEditClassDivisionPArt() {
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "POST",
			url: "/update-event-classes",
			data: $("#updateClasses").serialize()
		}).done(function (msg) {
			if (msg == "true") {
				$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
				$('#loading-spinner').addClass('hide');
				$('#class_succ').removeClass('hide');
				$("#class_succ").text("Updated successfully");
				setTimeout(function () {
					$("#classDivisionsModal").modal("hide");
					location.reload();
				}, THREE_SEC);
			}
			else {
				$('#loading-spinner').addClass('hide');
			}
		});
	}

	$('.compl_pracs').click(function () {
		var value = $('input:checkbox[name=compl_pract]:checked').val();
		if (value == 'yes')
			$("#event_compl_pracs").removeClass('hide');
		else
			$("#event_compl_pracs").addClass('hide');
	});
	$('.additonal_pracs').click(function () {
		var value = $('input:checkbox[name=additonal_pracs]:checked').val();
		//$('#singleEvent').is(":checked")
		if (value == 'yes')
			$("#event_additiona_pracs").removeClass('hide');
		else
			$("#event_additiona_pracs").addClass('hide');
	});
	$('.coach_serv').click(function () {
		var value = $('input:checkbox[name=coach_serv]:checked').val();
		if (value == 'yes')
			$("#event_coach_serv").removeClass('hide');
		else
			$("#event_coach_serv").addClass('hide');
	});
	$('.other_items').click(function () {
		var value = $('input:checkbox[name=other_items]:checked').val();
		if (value == 'yes')
			$(".event_other_items").removeClass('hide');
		else
			$(".event_other_items").addClass('hide');
	});
	
	// $("#edit-form-submit1").click(function (e) {
	// 	var item_array = [];
	// 	var item_price_array = [];
	// 	$('.get-item-index').each(function () {
	// 		var id = $(this).attr('data-index');
	// 		if (($('#item_' + id).val() != "") && ($('#item_cost_' + id).val() != "")) {
	// 			item_array.push($('#item_' + id).val())
	// 			item_price_array.push($('#item_cost_' + id).val())
	// 		}
	// 	});
	// 	console.log(item_array+"--"+item_price_array);
	// });
	$('#create-other-pricing-add-row').click(function (e) {
		e.preventDefault();
		var row = $("#other_pract_pricing tbody tr").last().clone();
		var rowCount = $('#other_pract_pricing tbody tr').length;
	
		if (rowCount > 0) {
			var oldId = rowCount;
			var id = 1 + oldId;
			row.attr('id', 'rowss_' + id);
			row.find('#item_' + oldId).attr('id', 'item_' + id);
			row.find('#item_' + id).val('');
			row.find('#item_' + id).attr("readonly", false);
			row.find('#item_cost_' + oldId).attr('id', 'item_cost_' + id);
			row.find('#item_cost_' + id).val('');
			row.find('#item_cost_' + id).attr("readonly", false);
			row.find('.delete_item').attr('id', 'delete_item_' + id);
			row.find('#delete_item_' + id).attr("disabled", false);
			row.find('#item_id_value_' + oldId).attr('id', 'item_id_value_' + id);
			row.find('#item_id_value_' + id).val('0');
			$('#other_pract_pricing').append(row);
			$("#item_" + id).removeClass("empty-err")
			$("#item_cost_" + id).removeClass("empty-err")
		}
	});

	$(document).on('click', "a.delete_item", function (e) {
		//var liId = $(this).parent("li").attr("id");
		e.preventDefault();
		var img_id = $(this).attr('id');
		if (img_id && img_id != "") {
			var idnum = img_id.replace("delete_item_", "");
			var rowCount = $('#other_pract_pricing tbody tr').length;
			$('#item_' + idnum).val('')
			$('#item_cost_' + idnum).val('')
			$('#item_' + idnum).removeClass("empty-err")
			$('#item_cost_' + idnum).removeClass('empty-err')

			if (rowCount > 1)
				$('#rowss_' + idnum).remove();
		}
	});

	$(document).on('click', "a.delete_question_item", function (e) {
		e.preventDefault();
		var img_id = $(this).attr('id');
		if (img_id && img_id != "") {
			var idnum = img_id.replace("delete_question_", "");
			var rowCount = $('#questions_event_table tbody tr').length;
			$('#question_' + idnum).val('')
			if (rowCount > 1)
				$('#rowss_question' + idnum).remove();
		}
	});

	$('#delete_item_1').click(function (e) {
		e.preventDefault();
		$('#item_1').val('')
		$('#item_cost_1').val('')

	});

	$("#submit_step4").click(function () {

		$('#event_class_question_err').addClass('hide')
		nextFlag = false
		isFormValid2 = true
		$('#questions_event_table tbody tr').each(function () {
			var oldId = $(this).attr('id');
			oldId = oldId.replace("rowss_question", "")
			if ($('#question_' + oldId).val().trim() != "") {
				nextFlag = true
			}
		});
		teamAdittionalQueston = []
		$('[name="event_item_questions[]"]').each(function () {
			idValue = $(this).attr('id')
			idValue = idValue.replace("question_", "")
			if ($("#question_" + idValue).val().trim().length > 150) {
				isFormValid2 = false
				$("#question_" + idValue).addClass("empty-err")
				teamAdittionalQueston.push($("#question_" + idValue).val())
				$(".error_question_count").removeClass('hide')
			} else {
				$("#question_" + idValue).removeClass("empty-err")
			}
		});

		isFormValid1 = true;
		$('[name="item_title[]"]').each(function () {
			idValue = $(this).attr('id')
			idValue = idValue.replace("item_", "")
			if ($("#item_" + idValue).val().trim() == "" && $("#item_cost_" + idValue).val().trim() != "") {
				isFormValid1 = false
				$("#item_" + idValue).addClass("empty-err")
			} else if ($("#item_" + idValue).val().trim() != "" && $("#item_cost_" + idValue).val().trim() == "") {
				isFormValid1 = false
				$("#item_cost_" + idValue).addClass("empty-err")
			} else {
				$("#item_" + idValue).removeClass("empty-err")
				$("#item_cost_" + idValue).removeClass("empty-err")
			}
		});

		// if(!nextFlag){
		// 	$('#event_class_question_err').removeClass('hide')
		// 	$('#event_class_question_err').text('Please enter the questionary')
		// }

		if (!isFormValid2) {
			$('#event_class_question_err').removeClass('hide')
			$('#event_class_question_err').text('No more than 150 words.')
		}

		if (isFormValid1 && isFormValid2) {

			$("#create-event-5").removeClass("hide");
			$("#create-event-4").addClass("hide");
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false;
		}
	});

	$('#create-questions-add-row').click(function (e) {
		$('#event_class_question_err').addClass('hide')
		e.preventDefault();
		var row = $("#questions_event_table tbody tr").last().clone();
		var rowCount = $('#questions_event_table tbody tr').last();

		if (rowCount.length > 0) {
			var oldId = $(rowCount).attr('id');
			oldId = oldId.replace("rowss_question", "")
			var id = 1 + parseInt(oldId);

			row.attr('id', 'rowss_question' + id);
			row.find('#question_' + oldId).attr('id', 'question_' + id);
			row.find('#question_' + id).attr('readonly', false);
			row.find('.delete_question_item').attr('id', 'delete_question_' + id);
			row.find('#delete_question_' + id).attr('disabled', false);
			$('#questions_event_table').append(row);
			$('#question_' + id).val("")
		}
	});


	$("#create_event_submit").click(function () {

		$('#twitter_err').addClass('hide');
		$('#phone_err').addClass('hide');
		$('#website_err').addClass('hide');
		$('#fb_err').addClass('hide');
		$('#youtube_err').addClass('hide');
		$('#email_err').addClass('hide');

		$('#event_email').removeClass('empty-err');
		$('#twitter_handle').removeClass('empty-err');
		$('#event_phone_number').removeClass('empty-err');
		$('#website_url').removeClass('empty-err');
		$('#facebook_page_url').removeClass('empty-err');
		$('#youtube_url').removeClass('empty-err');

		$('#loading-spinner').removeClass('hide');

		$('input').val(function (_, value) {
			return $.trim(value);
		});


		var isFormValid = true
		var prefix = 'http://';
		var prefix2 = 'https://';
		var phone_regex = /^[0-9\s-]*$/;
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		var event_phone = $('#event_phone_number').val().trim();
		var event_email = $('#event_email').val().trim();
		var event_facebook = $('#facebook_page_url').val().trim();
		var event_twitter = $('#twitter_handle').val().trim();
		var event_website = $('#website_url').val().trim();
		var event_youtube = $('#youtube_url').val().trim();

		var facebook_url = checkUrlPrefix(event_facebook, prefix2);
		var twitter_url = checkUrlPrefix(event_twitter, prefix2);
		var website_url = checkUrlPrefix(event_website, prefix);
		var youtube_url = checkUrlPrefix(event_youtube, prefix2);

		if (event_twitter != "") {
			if (isUrlValid(twitter_url) || event_twitter.substring(0, 1) == "@") {
				$('#twitter_err').removeClass('hide');
				$("#twitter_err").text("Invalid. Please enter only your username");
				$('#twitter_handle').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false
				//console('error 1')
			}
			event_twitter = "https://www.twitter.com/" + event_twitter;
		}

		if (event_facebook != "") {
			if (isUrlValid(facebook_url)) {
				$('#fb_err').removeClass('hide');
				$("#fb_err").text("Invalid. Please enter only your username");
				$('#facebook_page_url').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				//console('error 2')
			}
			event_facebook = "https://www.facebook.com/" + event_facebook;
		}

		if (event_youtube != "") {
			if (isUrlValid(youtube_url)) {
				$('#youtube_err').removeClass('hide');
				$("#youtube_err").text("Invalid. Please enter only your username");
				$('#youtube_url').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				//console('error 3')
			}
			event_youtube = "http://www.youtube.com/" + event_youtube;
		}

		if (event_email) {
			if (!email_regex.test(event_email)) {
				$('#email_err').removeClass('hide');
				$('#event_email').addClass('empty-err');
				$("#email_err").text("Incorrect email address. Please enter a proper email ID");
				isFormValid = false;
				//console('error 4')
			}
		}
		if (isNotEmpty(event_phone.trim()) == true) {
			var phoneRegex = /^\+([0-9]{1,2})\)?[\s+][-. ]?([0-9]{3})[-. ]?([0-9]{3})?[-. ]?([0-9]{4})$/;
			if (phoneRegex.test(event_phone)) {
				var phoneRegex1 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				var phoneExpressionarray = event_phone.split(' ');
				phoneExpressionarray1 = ""
				if (phoneExpressionarray[1] != "") {
					phoneExpressionarray1 = phoneExpressionarray[1].replace(phoneRegex1, "$1-$2-$3");
				} else {
					phoneExpressionarray1 = phoneExpressionarray[2].replace(phoneRegex1, "$1-$2-$3");
				}
				$('#event_phone_number').val(phoneExpressionarray[0] + ' ' + phoneExpressionarray1);
			} else {
				$('#phone_err').removeClass('hide');
				$('#event_phone_number').addClass('empty-err');
				$("#phone_err").html("Invalid phone number<br> (ex +1 443-979-8200 or +11 443.979.8200)");
				isFormValid = false
			}
		}
		if (website_url) {
			if (!isUrlValid(website_url)) {
				$('#website_err').removeClass('hide');
				$('#website_url').addClass('empty-err');
				$("#website_err").text("Invalid. Enter a proper URL");
				isFormValid = false;
			} else {
				$('#website_url').val(website_url);
			}
		}

		$("html, body").animate({ scrollTop: 0 }, "slow");
		//console('In dfdsf ' , isFormValid)
		//return false;
		if (isFormValid) {
			//console('In here')
			$('[name="M_seb_price[]"]').each(function () {
				//console($(this).val() + '-' + $(this).attr('id') );
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}

			})

			$('[name="W_seb_price[]"]').each(function () {
				//console($(this).val() + '-' + $(this).attr('id') );
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					$(this).addClass("empty-err");
					isFormValid = false
				}
			})

			$('[name="MX_seb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="M_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="W_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="MX_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})


			$('[name="M_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="W_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="MX_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			if (isFormValid) {
				$('#create_event_form').submit();
			}
			else {
				//console("Event fees error");
				return false
			}


		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	$("#backto_step2").click(function (e) {
		$("#create-team-3").addClass("hide");
		$("#create-team-2").removeClass("hide");

		$("#create-event-3").addClass("hide");
		$("#create-event-2").removeClass("hide");

		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$("#backto_step3").click(function (e) {
		$("#create-team-3").addClass("hide");
		$("#create-team-2").removeClass("hide");

		$("#create-event-4").addClass("hide");
		$("#create-event-3").removeClass("hide");

		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$("#backto_step4").click(function (e) {
		$('#event_class_question_err').addClass('hide')
		$("#create-team-3").addClass("hide");
		$("#create-team-2").removeClass("hide");

		$("#create-event-5").addClass("hide");
		$("#create-event-4").removeClass("hide");

		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	// $("#skip_step3").click(function(e){
	// 	$("#create-team-3").addClass("hide");
	// 	$("#create-team-2").removeClass("hide");

	// 	$("#create-event-3").addClass("hide");
	// 	$("#create-event-4").removeClass("hide");

	// 	$("html, body").animate({ scrollTop: 0 }, "slow");
	// 		return false;
	// });

	$("#skip_step4").click(function (e) {
		$("#create-team-3").addClass("hide");
		$("#create-team-2").removeClass("hide");

		$("#create-event-4").addClass("hide");
		$("#create-event-5").removeClass("hide");

		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
});

//advanced search
$(function () {
	$('.advanced-search-toggle').click(function () {
		$('.advanced-search-toggle').parents(".advanced-search").first().find(".advanced-search-options").first().slideToggle();
		$('.advanced-search-toggle').find("i").toggleClass("ti-angle-down ti-angle-up");
	});
});

// js for profilesetting
//date picker: profile-settings
$(function () {
	$('#datepicker-icon').click(function () {
		$('#dob').datepicker({
			autoclose: true,
			maxDate: '-13yr',
			changeMonth: true,
			changeYear: true,
			defaultDate: '-13yr',
			yearRange: "c-100:+0"
		}).focus();
	});

	$("#psUpload").change(function () {
		$('#ps_err_msg').removeClass('error');
		$("#ps_err_msg").text('');
		var ext = getFileExtension($('#psUpload').val());
		if (validateExt(ext)) {
			psUploadReadURL(this)
		}
		else {
			$('#ps_err_msg').addClass('error');
			$("#ps_err_msg").text("Image must be png, jpg, jpeg");
			return
		}
	});

	function psUploadReadURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);

			reader.onload = function () {
				var image = new Image();
				image.src = reader.result;

				image.onload = function () {
					if (image.width > 100 || image.height > 100) {
						$('#ps_err_msg').addClass('error');
						$("#ps_err_msg").text("Please upload a image, 100x100 pixels");
						return
					} else {
						$("#profile_image_upload").submit()
					}

				};
			};
		}
	}

	$("#profile_image_upload").on('submit', (function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('#ps_err_msg').removeClass('error');
		$("#ps_err_msg").text('');
		var formData = new FormData(this);

		$.ajax({
			url: "/upload-profile-img",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (uploadImagePath) {
				$(".profile-image-upload").attr('src', uploadImagePath)
				$("#projectImageModal .progress-bar").attr("aria-valuenow", 100);
				$("#projectImageModal .progress-bar").css("width", "100%");
				$("#projectImageModal .sr-only").text("100% Complete");
				setTimeout(function () {
					$("#projectImageModal").modal("hide");
					$("#projectImageModal .progress-bar").attr("aria-valuenow", 0);
					$("#projectImageModal .progress-bar").css("width", "0%");
					$("#projectImageModal .sr-only").text("");
				}, TWO_SEC);
				$('#loading-spinner').addClass('hide');
			}
		});
	}));

	$('.class_type_M').click(function () {
		if (!$(".class_type_M").hasClass('active')) {
			$(".class_type_M").addClass('active');
			$(".class_type_W").removeClass('active');
		}

		$('#classes').val('M');
	});

	$('.class_type_W').click(function () {
		if (!$("#class_type_W").hasClass('active')) {
			$(".class_type_W").addClass('active');
			$(".class_type_M").removeClass('active');
		}

		$('#classes').val('W');
	});

	$('#profileSave').click(function (eve) {
		eve.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$(".success-message").addClass("hide");
		$(window).scrollTop(0);
		var name = jQuery.trim($('#first_name').val());
		var last_name = jQuery.trim($('#last_name').val());
		var dob = $('#dob').val();
		var address = jQuery.trim($('#address').val());
		var phone = jQuery.trim($('#phone').val());
		var weight = jQuery.trim($('#weight').val());
		var tshirt_size = jQuery.trim($('#tshirt_size').val());
		var paddling_side = $('#paddling_side').val();
		var skill_level = $('#skill_level').val();
		var isFormValid = true;
		if ($('#availableRecruit').is(":checked")) {
			var status = "available";
		}
		else var status = "member";
		var personalInfo = $('#personal_info').val();

		if ($('#display_name').is(":checked")) {
			var display_name = 0;
			var member_name = name;
		}
		else {
			var display_name = 1;
			var member_name = name + " " + last_name;
		}

		if ($('#unsubscribe').is(":checked")) {
			var unsubscribe = 1;
		}
		else var unsubscribe = 0;

		if ($('#unsub_instant').is(":checked")) {
			var instant = 1;
		}
		else var instant = 0;

		if ($('#unsub_news').is(":checked")) {
			var unsub_news = 1;
		}
		else var unsub_news = 0;
		if ($('#unsub_sms').is(":checked")) {
			var unsub_sms = 1;
		}
		else var unsub_sms = 0;

		var $activities1 = $("input[type=checkbox][name='activities[]']:checked");
		if ($activities1.length) {
			var activities = $activities1.map(function () {
				return this.value;
			}).get();
		}

		var class_type = $('.class_type.active').val();
		var phone_regex = /^[0-9\s-]*$/;
		var weight_regex = /^[0-9]{1,3}$/;


    	/*$('#activities_err').addClass('hide');
		$("#activities_err").text('');*/
		$('.error-message').addClass('hide');
		$(".error-message").text('');

		$("input.required").each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
				$('#pf_errors').removeClass('hide');
				$("#pf_errors").text("Enter fields marked in *");
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		/*$('select.required').each(function(){
			var country_required_error = $(".country_dial_code").find("span.selection").find("span.select2-selection.select2-selection--single");
	        if ($.trim($(this).val()).length == 0){
	            $(this).addClass("empty-err");
	            isFormValid = false;
				$('#pf_errors').removeClass('hide');
				country_required_error.css("border","1px solid #B40000");
				$("#pf_errors").text("Enter fields marked in *");
	        }
	        else{
				country_required_error.css("border","solid 1px #cfcfcf");
	            $(this).removeClass("empty-err");
	        }
		}); */


		if (!isFormValid) {
			$('#loading-spinner').addClass('hide');
			return false;
		}

		// if(!phone_regex.test(phone)){
		// 	$('#phone').addClass('empty-err');
		// 	$('#phone_err').removeClass('hide');
		// 	$('#phone').focus();
		// 	//$("#phone_err").text('Enter phone number using numeric characters, spaces and/or "-".');
		// 	$("#phone_err").text('Please enter a proper phone number');
		// 	$('#loading-spinner').addClass('hide');
		// 	return false;
		// }


		if (isNotEmpty($('#dial_code').val().trim()) == false && isNotEmpty($('#phone').val().trim()) == true) {
			$('#dial_code_err').removeClass('hide');
			$('#phone_err').removeClass('hide');
			$("#dial_code_err").text("Country code required");
			$('#loading-spinner').addClass('hide');
			$("#dial_code_county_code").val("")
			return false;
		} else {
			if (isNotEmpty($('#dial_code').val().trim()) == true) {
				var seletedcountrycode = $("#dial_code option:selected").text();
				var indexValue = seletedcountrycode.indexOf("+");
				if (indexValue != -1) {
					$("#dial_code_county_code").val(seletedcountrycode.substring(0, indexValue).trim())
				} else {
					$('#loading-spinner').addClass('hide');
					return false;
				}
			}
		}

		if (isNotEmpty($('#phone').val().trim()) == false && isNotEmpty($('#dial_code').val().trim()) == true) {
			$('#dial_code_err').removeClass('hide');
			$('#phone_err').removeClass('hide');
			$("#phone_err").text("Phone number is required");
			$('#loading-spinner').addClass('hide');
			return false;
		}

		if (isNotEmpty($('#phone').val().trim()) == true) {
			var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

			if (phoneRegex.test(phone)) {
				if ((phone.indexOf('-') == -1 && phone.indexOf('.') == -1) || (phone.indexOf('-') > 0 && phone.indexOf('.') > 0)) {
					var formattedPhoneNumber =
						phone.replace(phoneRegex, "$1-$2-$3");
					$('#phone').val(formattedPhoneNumber);
				}
				$('#phone').removeClass('empty-err');
				$('#phone_err').addClass('hide');
			} else {
				$('#phone').addClass('empty-err');
				$('#phone_err').removeClass('hide');
				$("#phone_err").html("Invalid phone number<br> (ex 443-979-8200 or 443.979.8200)");
				$('#loading-spinner').addClass('hide');
				return false;
			}
		}

		if (weight && !weight_regex.test(weight)) {
			$('#weight').addClass('empty-err');
			$('#weight_err').removeClass('hide');
			$('#weight').focus();
			$("#weight_err").text('Enter weight using numeric characters under 1000 lbs');
			$('#loading-spinner').addClass('hide');
			return false;
		}

		var mailchimpAddress = address;
		if (dob) {
			var dob_new = moment(new Date(dob)).format('YYYY-MM-DD');
			var mailchimDOB = moment(new Date(dob)).format('MM/DD');
			var years = moment().diff(dob_new, 'years');
			var age = years;

			if (!parseInt(age)) {
				$('#dob').addClass('empty-err');
				$('#dob_err').removeClass('hide');
				$('#dob').focus();
				$("#dob_err").text('Date of birth must be a date. eg mm/dd/yyyy ');
				$('#loading-spinner').addClass('hide');
				return false;
			}
			if (age < 13) {
				$('#dob').addClass('empty-err');
				$('#dob_err').removeClass('hide');
				$('#dob').focus();
				$("#dob_err").text('User should be older than thirteen years');
				$('#loading-spinner').addClass('hide');
				return false;
			}
			dob = moment(dob_new).format('YYYY-MM-DD[T]12:00:00.SSS');
		}

		if ($activities1.length < 1) {
			$('#activities_err').removeClass('hide');
			$("#activities_err").text("Select required activity");
			$('#loading-spinner').addClass('hide');
			return false;
		}



		if (isFormValid) {
			$("#profileSettingForm").submit();
		}
		else {
			$('#loading-spinner').addClass('hide');
		}


	});
});

//date picker: create-event
$(function () {

	$('#start_datepicker').click(function () {
		$('#start_date').datepicker({
			//autoclose: true,
			minDate: new Date(),
			onSelect: function (selected_date) {
				var selectedDate = new Date(selected_date);
				var msecsInADay = 86400000;
				var endDate = new Date(selectedDate.getTime() + msecsInADay);
				$("#end_date").datepicker("option", "minDate", endDate);
			}
		}).focus();
	});

	$('#start_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#end_datepicker').click(function () {
		$('#end_date').datepicker({
			autoclose: true,
			minDate: new Date($("#start_date").val())
		}).focus();

	});

	$('#end_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#singleEvent').change(function () {
		if ($('#singleEvent').is(":checked")) {
			$('#end_date').val('');
			$('#end_date').attr("disabled", "disabled");
		} else {
			$('#end_date').removeAttr('disabled');
		}
	});

	$('#seb_date').keypress(function (e) {
		/*var max_date = new Date($("#start_date").val())
		var min_date = new Date();

		$('#seb_date').datepicker('option', 'changeMonth', true )

		$('#seb_date').datepicker('option', 'minDate', min_date )
		$('#seb_date').datepicker('option', 'maxDate', max_date )*/

		//$('#seb_date').datepicker({
		//autoclose: true,
		//minDate: new Date(),
		//maxDate: $("#start_date").datepicker("getDate")
		//});	
		/*//console(max_date)
		//console(min_date)*/

		e.preventDefault();
		return false;


	});

	$('#seb_dtpicker').click(function () {
		var max_date = new Date($("#start_date").val())
		var min_date = new Date();

		$('#seb_date').datepicker({
			autoclose: true,
			//minDate: new Date(),
			//maxDate: $("#start_date").datepicker("getDate")
		});

		//$('#seb_date').datepicker('option', 'changeMonth', true )

		$('#seb_date').datepicker('option', 'minDate', min_date)
		$('#seb_date').datepicker('option', 'maxDate', max_date)

		//console(max_date)
		//console(min_date)

		$('#seb_date').datepicker().focus();

	});

	$('#eb_date').keypress(function (e) {
		/*var min_date;
		var max_date = $("#start_date").datepicker("getDate")

		if ($("#seb_date").datepicker("getDate")) {
			var date2 = $('#seb_date').datepicker("getDate");
            date2.setDate(date2.getDate()+1);
			min_date = new Date(date2)
		}
		else {
			min_date = new Date();
		}

		$('#eb_date').datepicker({
			autoclose: true,
			//minDate: $("#seb_date").datepicker("getDate"),
			//maxDate: $("#start_date").datepicker("getDate")
		});
		$('#eb_date').datepicker('option', 'maxDate', max_date )
		$('#eb_date').datepicker('option', 'minDate', min_date )*/
		e.preventDefault();
		return false;
	});

	$('#eb_dtpicker').click(function () {
		var min_date;
		var max_date = new Date($("#start_date").val())

		if ($("#seb_date").datepicker("getDate")) {
			var date2 = $('#seb_date').datepicker("getDate");
			date2.setDate(date2.getDate() + 1);
			min_date = new Date(date2)
		}
		else {
			min_date = new Date();
		}
		$('#eb_date').datepicker({
			autoclose: true,
			//minDate: $("#seb_date").datepicker("getDate"),
			//maxDate: $("#start_date").datepicker("getDate")
		});

		//$('#eb_date').datepicker('option', 'changeMonth', true )
		$('#eb_date').datepicker('option', 'maxDate', max_date)
		$('#eb_date').datepicker('option', 'minDate', min_date)
		$('#eb_date').datepicker().focus();

		//console(max_date)
		//console(min_date)

	});

	$('#r_date').keypress(function (e) {
		/*var min_date;
		var max_date = $("#start_date").datepicker("getDate")

		if ($("#eb_date").datepicker("getDate")) {
			min_date = $("#eb_date").datepicker("getDate");
		}
		else {
			min_date = new Date();
		}
		$('#r_date').datepicker({
			autoclose: true,
			//minDate: min_date,
			//maxDate: get_max_date()		
		});
		$('#r_date').datepicker('option', 'maxDate', max_date )
		$('#r_date').datepicker('option', 'minDate', min_date )*/
		e.preventDefault();
		return false;

	});


	$('#reg_dtpicker').click(function () {
		var min_date;
		var max_date = new Date($("#start_date").val())
		if ($("#eb_date").datepicker("getDate")) {
			min_date = $("#eb_date").datepicker("getDate");
		}
		else {
			min_date = new Date();
		}
		$('#r_date').datepicker({
			autoclose: true,
			//minDate: min_date,
			//maxDate: max_date
		});

		//$('#r_date').datepicker('option', 'changeMonth', true )
		$('#r_date').datepicker('option', 'maxDate', max_date)
		$('#r_date').datepicker('option', 'minDate', min_date)
		$('#r_date').datepicker().focus();

		//console(max_date)
		//console(min_date)
	});

	$(".select-custom").select2({
		minimumResultsForSearch: Infinity
	});

})

//js for dashboard
$(function () {
	$('#readMoreDescription').click(function (e) {
		e.preventDefault();
		$('#showDescription').addClass('hide');
		$('#showFullDescription').removeClass('hide');
		$('#readMoreDescription').addClass('hide');
	});

	// To show current weeks practice
	var x = $('#week_cal').text().split('-')[1]
	end = moment(x, "ll").endOf('isoweek').format("ll")

	var week_number = moment(end, "ll").isoWeek()
	//console("Week number ", week_number)
	$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).removeClass("hide")
	$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).addClass("currentActive")


	$('#only_events').click(function (e) {

		e.preventDefault()
		$('.dashboard-schedule-nav ul li').removeClass('active');
		$('#only_events').addClass('active');
		$('.up_events').removeClass('hide');
		$('.team_Activity').addClass('hide');
		$('.team_Practice').addClass('hide');
	});

	$('#only_activity').click(function (e) {

		e.preventDefault()
		$('.dashboard-schedule-nav ul li').removeClass('active');
		$('#only_activity').addClass('active');

		$('.currentActive').each(function () {
			if ($(this).hasClass('team_Activity')) {
				$(this).removeClass('hide')
			} else {
				$(this).addClass('hide')
			}
		});

		if ($('#month_events').hasClass('active')) {
			practiceSchedulerMonthView()
		}
	});

	$('#only_practices').click(function (e) {

		e.preventDefault()
		$('.dashboard-schedule-nav ul li').removeClass('active');
		$('#only_practices').addClass('active');

		$('.currentActive').each(function () {
			if ($(this).hasClass('team_Practice')) {
				$(this).removeClass('hide')
			} else {
				$(this).addClass('hide')
			}
		});

		if ($('#month_events').hasClass('active')) {
			practiceSchedulerMonthView()
		}
	});

	$('#whole_schdule').click(function (e) {

		e.preventDefault()
		$('.dashboard-schedule-nav ul li').removeClass('active');
		$('#whole_schdule').addClass('active');
		$('.currentActive').each(function () {
			$(this).removeClass('hide');
		});

		if ($('#month_events').hasClass('active')) {
			practiceSchedulerMonthView()
		}
	});

	$('#month_events').click(function (e) {

		e.preventDefault();
		$('.dashboard-schedule-table, #show-schedule-grid').addClass('hide');
		$('.team-schedule-grid').removeClass('hide');
		$('#month_events').addClass('active');
		$('#week_events').removeClass('active');
		$('#month_cal').removeClass('hide');
		$('#week_cal').addClass('hide');
		$('#schedule_table tr').removeClass("currentActive")
		//var month_cal = moment().month(moment().month()).format('MMMM YYYY');
		var month_cal = moment($('#week_cal').text().split('-')[1], 'MMM d YYYY').format('MMMM YYYY')
		$('#month_cal').text(month_cal);

		view = 'basicWeek'
		default_date = $('#week_cal').text().split('-')[0]
		format = "ll"

		if ($('#month_events').hasClass('active')) {
			view = 'month'
			default_date = $('#month_cal').text()
			format = "MMMM YYYY"
		}

		//console("Default Date", moment(default_date, format))

		var source = new Array();
		if ($('#month_events').hasClass('active')) {
			var x = $('#month_cal').text()
			var month = moment(x, "MMMM YYYY").format("MMMM YYYY")

			var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc so add +1
			//console.log("Month number ", x)
			$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).each(function () {
				$(this).addClass("currentActive")
				var start = moment($(this).find('.schedule-date').children('h5:first').text() + ' ' + $(this).find('.schedule-date').children('p').map(function () { return $(this).text(); }).get().join(' ') + ' ' + moment(month, "MMMM YYYY").format("YYYY"), "ddd MMM D HH:mm a YYYY")

				var statusValue = false;
				if ($('#whole_schdule').hasClass('active')) {
					statusValue = true
				} else if ($('#only_practices').hasClass('active')) {
					if ($(this).hasClass('team_Practice')) {
						statusValue = true
					}
				} else if ($('#only_activity').hasClass('active')) {
					if ($(this).hasClass('team_Activity')) {
						statusValue = true
					}
				}
				if (statusValue) {
					if ($(this).find('.schedule-practice').length) {
						var title;
						var practiceType;
						practiceType = $(this).find('.schedule-practice').attr('data-practicetype')

						if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip').length != 0) {

							if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data == undefined) {
								title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip').children('a').text()
							}
							else {
								title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
							}
						}
						else {
							title = $(this).find('.schedule-practice').children('a').children('h3').text()
						}


						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
						if (title.length > 0) {
							source.push({
								title: title + ' Team ' + practiceType,
								start: start,
								backgroundColor: '#297560 !important',
								textColor: '#fff !important'
							})
						}
					}
					else {
						var title = $(this).find('.event-title').children('h3').text()
						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
						source.push({
							title: title,
							start: start,
							backgroundColor: '#297560 !important',
							textColor: '#fff !important'
						})
					}
				}
			})

		} else {
			var x = $('#week_cal').text().split('-')[1]
			end = moment(x).endOf('isoweek').format("ll")

			var week_number = moment(end, "ll").isoWeek()
			//console("Week number ", week_number)
			$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {
				$(this).addClass("currentActive")
				var start = moment($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function () { return $(this).text(); }).get().join(' ') + ' ' + moment(end, "ll").format("YYYY"), "ddd MMM D HH:mm a YYYY")
				if ($(this).find('.schedule-practice').length) {
					//var title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
					var title = ""

					if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data == undefined) {
						title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip').children('a').text()
					}
					else {
						title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
					}

					title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')

					if (title.length > 0) {
						source.push({
							title: title + ' Team Practice',
							start: start,
							backgroundColor: '#297560 !important',
							textColor: '#fff !important'
						})
					}
				}
				else {
					var title = $(this).find('.event-title').children('h3').text()
					title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
					source.push({
						title: title,
						start: start,
						backgroundColor: '#297560 !important',
						textColor: '#fff !important'
					})
				}


			})

		}

		//console(source)

		$('#calendar-schedule').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//gotoDate: moment(default_date, format),
			defaultView: view,
			firstDay: 1,
			eventRender: function (event, element) {
				element.find('.fc-time').text(element.find('.fc-time').text() + 'm')
				element.find('.fc-time').append("<br/>");
			}
		});

		$('#calendar-schedule').fullCalendar('removeEvents')

		$('#calendar-schedule').fullCalendar('gotoDate', moment(default_date, format))
		$('#calendar-schedule').fullCalendar('addEventSource', source)

		//console(view)
	});

	$('#week_events').click(function (e) {
		e.preventDefault();
		$('.team-schedule-grid, #show-schedule-list').addClass('hide');
		$('.dashboard-schedule-table').removeClass('hide');
		$('#month_events').removeClass('active');
		$('#week_events').addClass('active');
		$('#month_cal').addClass('hide');
		$('#week_cal').removeClass('hide');
		$('#schedule_table tr').removeClass("currentActive")

		if ($('#month_cal').text() == moment().month(moment().month()).format('MMMM YYYY')) {

			// If current month then go to current week
			var week_cal = moment().startOf('isoweek').format("MMM D") + '-' + moment().endOf('isoweek').format("ll")
			$('#week_cal').text(week_cal)
		} else {

			//If viewing different month, then week view will show 1st week of that month
			var week_cal = moment($('#week_cal').text(), "MMM D").startOf('isoweek').format("MMM D") + '-' + moment($('#week_cal').text(), "ll").endOf('isoweek').format("ll")
			$('#week_cal').text(week_cal)
		}



		$('#schedule_table tr').addClass("hide")

		if ($('#month_events').hasClass('active')) {
			// To show current months practice

			var x = $('#month_cal').text()
			var month = moment(x, "MMMM YYYY").format("MMMM YYYY")

			var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc so add +1
			//console("Month number ", month_num)
			$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).removeClass("hide")
			$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).addClass("currentActive")

		}
		else {
			// To show current weeks practice
			var x = $('#week_cal').text().split('-')[1]
			end = moment(x, "ll").endOf('isoweek').format("ll")

			var week_number = moment(end, "ll").isoWeek()
			//console("Week number ", week_number)
			$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {
				$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).addClass("currentActive")
				$(this).addClass("currentActive")
				if ($('#whole_schdule').hasClass('active')) {
					$(this).removeClass("hide")
				} else if ($('#only_practices').hasClass('active')) {
					if ($(this).hasClass('team_Practice')) {
						$(this).removeClass("hide")
					}
				} else if ($('#only_activity').hasClass('active')) {
					if ($(this).hasClass('team_Activity')) {
						$(this).removeClass("hide")
					}
				}

			});
		}


	});

	$('#main-spinner').addClass('hide');

	// $('#month_events').click(function(e){
	// 	e.preventDefault();
	//  		$('.dashboard-schedule-view ul li').removeClass('active');
	//  		$('#month_events').addClass('active');
	//  		$('#month_cal').removeClass('hide');
	//  		$('#week_cal').addClass('hide');

	//  		$('#calendar-schedule').fullCalendar( 'changeView', 'month' );

	//  		var month_cal = moment().month(moment().month()).format('MMMM YYYY');
	//  		$('#month_cal').text(month_cal);

	//  		$('#schedule_table tr').addClass("hide")

	//  		// To show current months practice

	//  		var x = $('#month_cal').text()
	//  		var month = moment(x).format("MMMM YYYY")

	//  		$('#calendar-schedule').fullCalendar('gotoDate', moment(month, "MMMM YYYY"))

	// 	var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc so add +1
	// 	//console("Month number ", month_num)
	// 	$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).removeClass("hide")

	// 	var source = new Array();
	// 	var x = $('#month_cal').text()
	// 	var month = moment(x).format("MMMM YYYY")

	// 	//console.log(month)

	// 	var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc so add +1
	// 	//console("Month number ", month_num)
	// 	$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).each(function(){

	// 		var start = moment($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function() { return $(this).text(); }).get().join(' ') + ' ' + moment(month, "MMMM YYYY").format("YYYY"),"ddd MMM D HH:mm a YYYY")

	// 		if ($(this).find('.schedule-practice').length) {
	// 			var title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data

	// 			title = $.map(title.split('\n'), $.trim).filter( function (v) {	return v!=='' }).join(' ')

	// 			//console.log(start)

	// 			if (title.length > 0) {
	// 				source.push({
	// 					title: title + ' Team Practice',
	// 					start: start,
	// 					backgroundColor: '#297560 !important',
	// 					textColor: '#fff !important'
	// 				})
	// 			}
	// 		}			
	// 		else {
	// 			var title = $(this).find('.event-title').children('h3').text()
	// 			title = $.map(title.split('\n'), $.trim).filter( function (v) {	return v!=='' }).join(' ')
	// 			source.push({
	// 				title: title,
	// 				start: start,
	// 				backgroundColor: '#297560 !important',
	// 				textColor: '#fff !important'
	// 			})
	// 		}

	// 	})
	// 	//console.log(source)
	// 	$('#calendar-schedule').fullCalendar( 'removeEvents' )
	// 	$('#calendar-schedule').fullCalendar('addEventSource', source)
	//  	});
	//  	$('#week_events').click(function(e){
	//  		e.preventDefault();
	//  		$('.dashboard-schedule-view ul li').removeClass('active');
	//    	$('#week_events').addClass('active');
	//    	$('#month_cal').addClass('hide');
	//    	$('#week_cal').removeClass('hide');

	// 	//$('#calendar-schedule').fullCalendar( 'changeView', 'basicWeek' );


	// 	$('#schedule_table tr').addClass("hide")

	// 	var week_cal = moment().startOf('isoweek').format("MMM D") + '-' + moment().endOf('isoweek').format("ll")
	// 	$('#week_cal').text(week_cal)

	// 	// To show current weeks practice
	// 	var x = $('#week_cal').text().split('-')[1]
	// 	end = moment(x).endOf('isoweek').format("ll")

	// 	//$('#calendar-schedule').fullCalendar('gotoDate', moment(end, "ll"))

	// 	var week_number = moment(end, "ll").isoWeek()
	// 	//console("Week number ", week_number)
	// 	$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).removeClass("hide")

	// 	var source = new Array();
	// 	var x = $('#week_cal').text().split('-')[1]
	// 	end = moment(x).endOf('isoweek').format("ll")

	// 	var week_number = moment(end, "ll").isoWeek()
	// 	//console("Week number ", week_number)
	// 	$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function(){

	// 		var start = moment($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function() { return $(this).text(); }).get().join(' ') + ' ' + moment(end, "ll").format("YYYY"),"ddd MMM D HH:mm a YYYY")

	// 		if ($(this).find('.schedule-practice').length) {
	// 			var title =$(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data

	// 			title = $.map(title.split('\n'), $.trim).filter( function (v) {	return v!=='' }).join(' ')

	// 			if (title.length > 0) {
	// 				source.push({
	// 					title: title + ' Team Practice',
	// 					start: start,
	// 					backgroundColor: '#297560 !important',
	// 					textColor: '#fff !important'
	// 				})
	// 			}
	// 		}			
	// 		else {
	// 			var title = $(this).find('.event-title').children('h3').text()
	// 			title = $.map(title.split('\n'), $.trim).filter( function (v) {	return v!=='' }).join(' ')
	// 			source.push({
	// 				title: title,
	// 				start: start,
	// 				backgroundColor: '#297560 !important',
	// 				textColor: '#fff !important'
	// 			})
	// 		}

	// 	});

	// 	//console(source)
	// 	//$('#calendar-schedule').fullCalendar( 'removeEvents' )
	// 	//$('#calendar-schedule').fullCalendar('addEventSource', source)

	// });

	$('#next_week').click(function (e) {
		e.preventDefault();

		$('#schedule_table tr').removeClass("currentActive")

		if (!$('.team-schedule-grid').hasClass('hide')) {

			$('#calendar-schedule').fullCalendar('next');
			if ($('#month_events').hasClass('active')) {
				practiceSchedulerMonthView()
			}
			else {
				//console($('#calendar-schedule').fullCalendar('getView'))
				var week_start = $('#calendar-schedule').fullCalendar('getView').intervalStart['_d'];
				week_start = moment(week_start, "MMM D").format("MMM D")
				var week_end = $('#calendar-schedule').fullCalendar('getView').intervalEnd['_d'];
				week_end = moment(week_end).add(-1, 'days').format("ll")
				$('#week_cal').text(week_start + '-' + week_end)

				var source = new Array();
				var x = $('#week_cal').text().split('-')[1]
				end = moment(x).endOf('isoweek').format("ll")

				var week_number = moment(end, "ll").isoWeek()
				//console("Week number zzz", week_number)
				$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {
					$(this).addClass("currentActive")
					var start = moment($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function () { return $(this).text(); }).get().join(' ') + ' ' + moment(end, "ll").format("YYYY"), "ddd MMM D HH:mm a YYYY")

					if ($(this).find('.schedule-practice').length) {
						//var title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
						var title = ""

						if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data == undefined) {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip').children('a').text()
						}
						else {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
						}

						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')

						if (title.length > 0) {
							source.push({
								title: title + ' Team Practice',
								start: start,
								backgroundColor: '#297560 !important',
								textColor: '#fff !important'
							})
						}
					}
					else {
						var title = $(this).find('.event-title').children('h3').text()
						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
						source.push({
							title: title,
							start: start,
							backgroundColor: '#297560 !important',
							textColor: '#fff !important'
						})
					}
				});

				//console(source)
				$('#calendar-schedule').fullCalendar('removeEvents')
				$('#calendar-schedule').fullCalendar('addEventSource', source)
			}
		}
		else {

			$('#schedule_table tr').addClass("hide")
			if ($('#month_events').hasClass('active')) {
				var x = $('#month_cal').text()
				var month = moment(x).add(1, 'months').format("MMMM YYYY")
				$('#month_cal').text(month)

				var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc so add +1
				//console("Month number ", month_num)
				$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).removeClass("hide")
			}
			else {
				var x = $('#week_cal').text().split('-')[1]
				start = moment(x, "ll").add(1, 'days').format("ll")
				end = moment(start, "ll").endOf('isoweek').format("ll")

				start = moment(start, "MMM D").format("MMM D")
				$('#week_cal').text(start + '-' + end)

				var week_number = moment(end, "ll").isoWeek()
				//console("Week number ", week_number)
				// $('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).removeClass("hide")
				// $(this).addClass("currentActive")
				$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {

					$(this).addClass("currentActive")
					if ($('#whole_schdule').hasClass('active')) {
						$(this).removeClass("hide")
					} else if ($('#only_practices').hasClass('active')) {
						if ($(this).hasClass('team_Practice')) {
							$(this).removeClass("hide")
						}
					} else if ($('#only_activity').hasClass('active')) {
						if ($(this).hasClass('team_Activity')) {
							$(this).removeClass("hide")
						}
					}

				});
			}

		}



		////console($('#calendar-schedule').fullCalendar('getView').intervalStart['_d'])
		////console($('#calendar-schedule').fullCalendar('getView').intervalEnd['_d'])
	});

	$('#prev_week').click(function (e) {
		e.preventDefault();

		if (!$('.team-schedule-grid').hasClass('hide')) {
			$('#calendar-schedule').fullCalendar('prev');
			$('#schedule_table tr').removeClass("currentActive")
			if ($('#month_events').hasClass('active')) {
				practiceSchedulerMonthView()
			}
			else {
				var week_start = $('#calendar-schedule').fullCalendar('getView').intervalStart['_d'];
				week_start = moment(week_start, "MMM D").format("MMM D")
				var week_end = $('#calendar-schedule').fullCalendar('getView').intervalEnd['_d'];
				week_end = moment(week_end).add(-1, 'days').format("ll")
				$('#week_cal').text(week_start + '-' + week_end)

				var source = new Array();
				var x = $('#week_cal').text().split('-')[1]
				end = moment(x).endOf('isoweek').format("ll")

				var week_number = moment(end, "ll").isoWeek()
				//console.log("Week number zxc", week_number)
				$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {
					$(this).addClass("currentActive")
					var start = moment($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function () { return $(this).text(); }).get().join(' ') + ' ' + moment(end, "ll").format("YYYY"), "ddd MMM D HH:mm a YYYY")
					if ($(this).find('.schedule-practice').length) {
						//var title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
						var title = ""

						if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data == undefined) {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip').children('a').text()
						}
						else {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
						}

						//console.log($(this).find('.schedule-date').children('h5').text() + ' ' + $(this).find('.schedule-date').children('p').map(function() { return $(this).text(); }).get().join(' '));
						//console.log(start)

						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')

						if (title.length > 0) {
							source.push({
								title: title + ' Team Practice',
								start: start,
								backgroundColor: '#297560 !important',
								textColor: '#fff !important'
							})
						}
					}
					else {
						var title = $(this).find('.event-title').children('h3').text()
						title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
						source.push({
							title: title,
							start: start,
							backgroundColor: '#297560 !important',
							textColor: '#fff !important'
						})
					}
				});

				//console.log(source)
				$('#calendar-schedule').fullCalendar('removeEvents')
				$('#calendar-schedule').fullCalendar('addEventSource', source)
			}
		}
		else {

			$('#schedule_table tr').addClass("hide")
			$('#schedule_table tr').removeClass("currentActive")
			if ($('#month_events').hasClass('active')) {
				var x = $('#month_cal').text()
				var month = moment(x).subtract(1, 'months').format("MMMM YYYY")
				$('#month_cal').text(month)

				var month_num = moment(month, "MMMM YYYY").month()    // by default 0-Jan, 1-Feb, etc 
				//console("Month number ", month_num)
				$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).removeClass("hide")
			}
			else {
				var x = $('#week_cal').text().split('-')[1]
				start = moment(x, "ll").startOf('isoweek').format("ll")
				end = moment(start, "ll").add(-1, 'days').format("ll")
				start = moment(end, "ll").startOf('isoweek').format("ll")

				start = moment(start, "MMM D").format("MMM D")
				$('#week_cal').text(start + '-' + end)

				var week_number = moment(end, "ll").isoWeek()
				//console.log("Week number ", week_number)
				////team_practices Weekly week_num_year_2_2018 month_year_0_2018
				$('#schedule_table tr.week_num_year_' + week_number + '_' + moment(end, "ll").format("YYYY")).each(function () {
					$(this).addClass("currentActive")
					if ($('#whole_schdule').hasClass('active')) {
						$(this).removeClass("hide")
					} else if ($('#only_practices').hasClass('active')) {
						if ($(this).hasClass('team_Practice')) {
							$(this).removeClass("hide")
						}
					} else if ($('#only_activity').hasClass('active')) {
						if ($(this).hasClass('team_Activity')) {
							$(this).removeClass("hide")
						}
					}
				});
			}
		}
	});

	function practiceSchedulerMonthView() {
		var month_start = $('#calendar-schedule').fullCalendar('getView').title;
		month_start = moment(month_start, "MMM YYYY").format("MMMM YYYY");
		$('#month_cal').text(month_start);

		var source = new Array();
		var x = $('#month_cal').text()
		var month = moment(x, "MMMM YYYY").format("MMMM YYYY")

		var month_num = moment(month, "MMMM YYYY").month();    // by default 0-Jan, 1-Feb, etc so add +1
		//console("Month number ", month_num)
		$('#schedule_table tr.month_year_' + month_num + '_' + moment(month, "MMMM YYYY").format("YYYY")).each(function () {

			$(this).addClass("currentActive")
			var statusDataValue = false
			if ($('#whole_schdule').hasClass('active')) {
				statusDataValue = true
			} else if ($('#only_practices').hasClass('active')) {
				if ($(this).hasClass('team_Practice')) {
					statusDataValue = true
				}
			} else if ($('#only_activity').hasClass('active')) {
				if ($(this).hasClass('team_Activity')) {
					statusDataValue = true
				}
			}
			if (statusDataValue) {
				var practiceType = $(this).find('.schedule-practice').attr('data-practicetype')
				var start = moment($(this).find('.schedule-date').children('h5:first').text() + ' ' + $(this).find('.schedule-date').children('p').map(function () {
					return $(this).text();
				}).get().join(' ') + ' ' + moment(month, "MMMM YYYY").format("YYYY"), "ddd MMM D HH:mm a YYYY")

				if ($(this).find('.schedule-practice').length) {
					var title;
					if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip').length != 0) {
						if ($(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data == undefined) {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip').children('a').text()
						}
						else {
							title = $(this).find('.schedule-practice').children('h3').not('.note-tooltip')[0].firstChild.data
						}
					}
					else {
						title = $(this).find('.schedule-practice').children('a').children('h3').text()
					}

					title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')

					if (title.length > 0) {
						source.push({
							title: title + ' Team ' + practiceType,
							start: start,
							backgroundColor: '#297560 !important',
							textColor: '#fff !important'
						})
					}
				}
				else {
					var title = $(this).find('.event-title').children('h3').text()
					title = $.map(title.split('\n'), $.trim).filter(function (v) { return v !== '' }).join(' ')
					source.push({
						title: title,
						start: start,
						backgroundColor: '#297560 !important',
						textColor: '#fff !important'
					})
				}
			}
		})
		//console(source)
		$('#calendar-schedule').fullCalendar('removeEvents')
		$('#calendar-schedule').fullCalendar('addEventSource', source)
	}

	$('#myteams').click(function (e, template) {
		e.preventDefault();
		$('.dashboard-nav-pills li').removeClass('active');
		$('#myteams').addClass('active')
		$('.mem_teams').addClass('hide');
		$('.my_teams').removeClass('hide');
	});

	$('#allteams').click(function (e, template) {
		e.preventDefault();
		$('.dashboard-nav-pills li').removeClass('active');
		$('#allteams').addClass('active')
		$('.my_teams').addClass('hide');
		$('.mem_teams').removeClass('hide');
	});

	$('#all_my_teams').change(function (e) {
		var val = $('#all_my_teams').val();

		if (val == "all") {
			$('.my_teams').addClass('hide');
			$('.all_my_teams').removeClass('hide');
			$('.mem_teams').addClass('hide');
		}
		else if (val == "manage") {
			$('.mem_teams').addClass('hide');
			$('.all_my_teams').addClass('hide');
			$('.my_teams').removeClass('hide');

		}
		else if (val == "part_of") {
			$('.my_teams').addClass('hide');
			$('.all_my_teams').addClass('hide');
			$('.mem_teams').removeClass('hide');
		}
	});
	//Reports

	$('#eventreportdownloadforteam').click(function () {
		

		var from_team=$("#event_report_team").is(":enabled")
		var from_race=$("#event_report_race").is(":enabled")
		var from_ind_mul=$("#str_from_ind_mul").val()
		var from_teamVal =$("#event_report_team").val()  
		var from_raceVal =$("#event_report_race").val()  

		if(from_team  && from_teamVal.length >0){
			console.log("team")
			$("#emptySelectionreport").addClass('hide')
			var val = $('#event_report_team').val();
			$('#getMulTeamName').val(val)
			$('#EventReportForm').attr('action', '/event_reports/downloadreport');
			$('#EventReportForm').submit()

		}else if(from_race  && from_raceVal.length > 0){
			console.log("race")
			$("#emptySelectionreport").addClass('hide')
			var val = $('#event_report_race').val();
			$('#getMulRaceType').val(val)
			$('#EventReportForm').attr('action', '/event_reports/downloadracereport');
			$('#EventReportForm').submit()
			
		}else if(from_ind_mul =="ind_mul"){
			$("#emptySelectionreport").addClass('hide')
			console.log("ind_mul")
			
			var selectedMemForReport = [] ;
		$('input:checkbox[name="reports_teams[]"]').each(function () {
			if(this.checked){
				if (jQuery.inArray(this.value, selectedMemForReport) == -1) {
					selectedMemForReport.push(this.value)
				}
			}	
		});
		$("#getselectedMemForReport").val(selectedMemForReport)
		var get_selectMem = $('#getselectedMemForReport').val() ;
		if (get_selectMem != ""){
			$('#EventReportForm').attr('action', '/event_reports/downloadmultipleteamreport');
			$('#EventReportForm').submit()
			$(".check_all_report_teams").prop('checked', false);
			$("#event_report_team").prop('disabled', false);
			$("#event_report_race").prop('disabled', false);
			$('#check_all_report').prop('checked', false);
		}else{
			$("#emptySelectionreport").removeClass('hide')
		}
	
		console.log("getselectedMemForReport--",get_selectMem)				
			
		}else{
			$("#emptySelectionreport").removeClass('hide')
		}
		
	});
	
	$('#event_report_team').change(function (e) {
		var val = $('#event_report_team').val();
		
		$("#emptySelectionreport").addClass('hide')
		console.log("val",val)
		if (val.length >0){
			console.log("true")
			$("#str_from_team").val("team")
			$("#str_from_race").val("")
			$("#str_from_ind_mul").val("")
				sessionStorage.setItem('eventactiveTab','#eventRosterRacingList')
				$('#loading-spinner').removeClass('hide');
				$('#EventReportForm').attr('action', '');
				$("#event_report_race").val([]).trigger('change'); 
				$('#getMulTeamName').val(val)
				$('#getMulRaceType').val('')
				console.log("newArr--",$('#getMulTeamName').val())
				
				$('#EventReportForm').submit()
			
        	
	
		}else{
			$("#emptySelectionreport").addClass('hide')
			console.log("false")
			$("#str_from_team").val("")
			$("#str_from_race").val("")
			$('#getMulRaceType').val('')
			//$("#str_from_ind_mul").val("") 
		}
		
	});
	$(".teamreport").select2({
		placeholder: "Select Team Name"
	});
	$(".eventreportrace").select2({
		placeholder: "Select Race Type"
	});

	
	$('#event_report_race').change(function (e) {
		var val = $('#event_report_race').val();
		$("#emptySelectionreport").addClass('hide')
		$('#event_report_team').prop("disabled", false); 
		console.log("val",val)
		if (val.length > 0){
			console.log("true")
			$("#str_from_team").val("")
			$("#str_from_race").val("race")
			$("#str_from_ind_mul").val("")
			sessionStorage.setItem('eventactiveTab','#eventRosterRacingList')
			$('#loading-spinner').removeClass('hide');
			$('#EventReportForm').attr('action', '');
			$('#event_report_team').val([]).trigger('change'); 
			$('#getMulRaceType').val(val)
			$('#getMulTeamName').val('')
		
			console.log("newArr--",$('#getMulRaceType').val())
			$('#EventReportForm').submit() 
        
				
	
		}else{
			$("#emptySelectionreport").addClass('hide')
			$("#str_from_team").val("")
			$("#str_from_race").val("")
			$('#getMulTeamName').val('')
			//$("#str_from_ind_mul").val("")
			console.log("false")
		}
		
	});
	
	$('#check_all_report').click(function () {
		if(this.checked){
			$("#emptySelectionreport").addClass('hide')
			$("#str_from_ind_mul").val("ind_mul")
			$('.check_all_report_teams').prop('checked', true);
			$("#event_report_team").val("default").trigger('change');  
        	$("#event_report_race").val("default").trigger('change');  
					
		}else {
			$("#emptySelectionreport").addClass('hide')
			$('.check_all_report_teams').prop('checked', false);
				//$("#str_from_ind_mul").val("")
				$("#event_report_team").prop('disabled', false);
				$("#event_report_race").prop('disabled', false);
				$("#event_report_team").val("").trigger('change');  
        $("#event_report_race").val("").trigger('change');  
		}
	});

	$('.check_all_report_teams').click(function () {
		var val = $(this).val()
			if(this.checked){
				$("#emptySelectionreport").addClass('hide')
				$("#str_from_ind_mul").val("ind_mul")
				$("#select_event_report_"+this.value).prop('checked', true);
				$("#event_report_team").val("default").trigger('change');  
        		$("#event_report_race").val("default").trigger('change');  

			}else {
				$("#emptySelectionreport").addClass('hide')
				$("#select_event_report_"+this.value).prop('checked', false);
				$("#event_report_team").prop('disabled', false);
				$("#event_report_race").prop('disabled', false);
				$("#event_report_team").val("").trigger('change');  
        		$("#event_report_race").val("").trigger('change');  
			}	
	});

	// ** END REPORT

	$('#tj_accept').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#tj_accept').attr('value').split(';');
		team_id = arr[0];
		notf_id = arr[1];
		$.ajax({
			type: "POST",
			url: "/accept-team-member-invite",
			data: {
				"team_id": team_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#tj_decline').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $(this).attr('value').split(';');
		team_id = arr[0];
		notf_id = arr[1];
		$.ajax({
			type: "POST",
			url: "/decline-team-member-invite",
			data: {
				"team_id": team_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			if(msg == "captain"){
				$('#removeTeamModel').modal('show')
				$('#loading-spinner').addClass('hide');
			}else {
				location.reload();
			}
			
		});
	});

	$('#tjr_accept').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#tjr_accept').attr('value').split(';');
		member_id = arr[0];
		team_id = arr[1];
		notf_id = arr[2];
		$.ajax({
			type: "POST",
			url: "/accept-team-join-request",
			data: {
				"member_id": member_id,
				"team_id": team_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#tjr_decline').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#tjr_decline').attr('value').split(';');
		member_id = arr[0];
		team_id = arr[1];
		notf_id = arr[2];
		$.ajax({
			type: "POST",
			url: "/decline-team-join-request",
			data: {
				"member_id": member_id,
				"team_id": team_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#eo_accept').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#eo_accept').attr('value').split(';');
		event_id = arr[0];
		notf_id = arr[1];
		$.ajax({
			type: "POST",
			url: "/accept-co-organizer-invite",
			data: {
				"event_id": event_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#eo_decline').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#eo_decline').attr('value').split(';');
		event_id = arr[0];
		notf_id = arr[1];
		$.ajax({
			type: "POST",
			url: "/decline-co-organizer-invite",
			data: {
				"event_id": event_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#claim_accept').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#claim_accept').attr('value').split(';');
		initiator_id = arr[0]
		event_id = arr[1];
		notf_id = arr[2];
		$.ajax({
			type: "POST",
			url: "/accept-claim-event-invite",
			data: {
				"event_id": event_id,
				"notf_id": notf_id,
				"initiator_id": initiator_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});

	$('#claim_decline').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var arr = $('#claim_decline').attr('value').split(';');
		org_id = arr[0];
		event_id = arr[1];
		notf_id = arr[2];
		$.ajax({
			type: "POST",
			url: "/decline-claim-request",
			data: {
				"org_id": org_id,
				"event_id": event_id,
				"notf_id": notf_id
			}
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});


	$('.iCheck-helper').click(function () {

		if ($(this).prev('.response').val() == "attending") {
			$('.availability-note').addClass("hide")
		}
		else if ($(this).prev('.response').val() == "not attending") {
			$('.availability-note').removeClass("hide")
		}
		else if ($(this).prev('.response').val() == "maybe") {
			$('.availability-note').removeClass("hide")
		}

		if ($(this).prev('.edit_options').val() == "only") {
			$('#edit_practice_option').val("only");
			$("#practice_edit_options_submit").removeAttr("disabled");
			$("#recurring-error").empty();
		}
		else if ($(this).prev('.edit_options').val() == "all_following") {
			$('#edit_practice_option').val("all_following");
			$("#practice_edit_options_submit").removeAttr("disabled");
			$("#recurring-error").empty();
		}
		else if ($(this).prev('.edit_options').val() == "all") {
			$('#edit_practice_option').val("all");
			$("#practice_edit_options_submit").removeAttr("disabled");
			$("#recurring-error").empty();
		}

		if ($(this).prev('.delete_options').val() == "delete_only") {
			$('#delete_practice_option').val("only");
		}
		else if ($(this).prev('.delete_options').val() == "delete_all_following") {
			$('#delete_practice_option').val("all_following");
		}
		else if ($(this).prev('.delete_options').val() == "delete_all") {
			$('#delete_practice_option').val("all");
		}

	});


	$('.mark-availability').click(function (e) {
		e.preventDefault();
		var isFormValid = false
		var practice_id = $(this).attr('id').split('_')[1]
		$('input[name="response"]:radio').each(function () {
			if ($(this).is(':checked')) {
				isFormValid = true
				return false;
			}
			else {
				isFormValid = false
			}
		});

		$(this).parent().parent().parent().find("input[name='currentDateTime']").val(moment().format('YYYY-MM-DD h:mm A'));

		if (isFormValid) {
			$.ajax({
				type: "POST",
				url: "/set-availability",
				data: $("#setAvalibility_" + practice_id).serialize()
			}).done(function (msg) {
				if (msg == "attending") {
					$('#practice_status_' + practice_id).text("Attending")
					$('#practice_status_' + practice_id).addClass("attending")
					$('#practice_status_' + practice_id).css('text-decoration', 'underline');
				}
				else if (msg == "not attending") {
					$('#practice_status_' + practice_id).text("Not Attending")
					$('#practice_status_' + practice_id).addClass("not-attending");
					$('#practice_status_' + practice_id).css('text-decoration', 'underline');
				}
				else if (msg == "maybe") {
					$('#practice_status_' + practice_id).text("Maybe");
					$('#practice_status_' + practice_id).css('text-decoration', 'underline');
				} else if (msg == "error_attendance") {
					$("#practiceAttStatusModal").modal("show");
				}
			});
		}

	});


})

//js for team all
$(function () {
	$('#other_teams').click(function () {
		$('.teams-all-content-filter button').removeClass('active');
		$('#other_teams').addClass('active');
		$('.my_teams').addClass('hide');
		$('.other_teams').removeClass('hide');
		$('.all_teams').addClass('hide');
	});

	$('#my_teams').click(function () {
		$('.teams-all-content-filter button').removeClass('active');
		$('#my_teams').addClass('active');
		$('.my_teams').removeClass('hide');
		$('.other_teams').addClass('hide');
		$('.all_teams').addClass('hide');
	});

	$('#all_teams').click(function () {
		$('.teams-all-content-filter button').removeClass('active');
		$('#all_teams').addClass('active');
		$('.all_teams').removeClass('hide');
		$('.other_teams').addClass('hide');
		$('.my_teams').addClass('hide');
	});

});

//js for notification all
$(function () {
	$('.remove_noti').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var notf_id = $(this).attr('id');

		$.ajax({
			type: "POST",
			url: "/remove-notification",
			data: { "notf_id": notf_id }
		}).done(function (msg) {
			//console(msg)
			location.reload();
		});
	});
});

//js for register form
$(function () {
	$("#registerForm input[type='submit']").click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');

		var password = $("#password").val();

		$("#err_msgs2").addClass("hide")
		$("#err_msgs3").addClass("hide")
		$("#password_err").addClass("hide")

		var isFormValid = true;
		$("#registerForm input.required").each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		var activityVals = [];
		$('input:checkbox[name="activities[]"]').each(function () {
			if (this.checked) {
				activityVals.push(this.value);
			}
		});

		if (password.length < 8) {
			$('#password_err').removeClass('hide');
			$("#password_err").text("Invalid password length");
			isFormValid = false;
		}

		if (activityVals.length == 0) {
			$('#loading-spinner').addClass('hide');
			$('#activity_err').removeClass('hide');
			$("#activity_err").text("Please select your role");
			isFormValid = false;
		}
		else {
			$('#activity_err').addClass('hide');
		}

		if (isFormValid) {
			$("#registerForm").submit();
		}
		else {
			$('#loading-spinner').addClass('hide');
			return false;
		}

	});

	$('#country').change(function () {
		$('#postal_err').addClass('hide');
		$("#postal_err").text("");
		var country = $('#country').val();
		////console(country);
		if (country == "Other") {
			$('#other_country').removeClass('hide');
			$('#zip_err_msgs').removeClass('error');
			$("#zip_err_msgs").text("");
			$('#postal_code').attr("placeholder", "Postal / Zip Code");
			$('#province2').attr("placeholder", "Province or State");
			$('#province2').removeClass('hide');
			$('#province').addClass('hide');
			$('#postal_code').val("");
			$('input#other_country').val("");
		} else if (country == "USA") {
			$('#postal_code').val("");
			$('#postal_code').attr("placeholder", "Zip Code");
			$('#province2').attr("placeholder", "State");
			$('#other_country').addClass('hide');
			$('input#other_country').val("");
		} else if (country == "Canada") {
			$('#postal_code').val("");
			$('#postal_code').attr("placeholder", "Postal Code");
			$('#province2').attr("placeholder", "Province");
			$('#other_country').addClass('hide');
			$('input#other_country').val("");
		} else {
			$('#postal_code').val("");
			$('#other_country').addClass('hide');
			$('#postal_code').attr("placeholder", "Postal / Zip Code");
			$('#province2').attr("placeholder", "Province or State");
			$("#location").val("");
			$("#province2").val("");
			$('#location').removeClass('empty-err');
			$('#postal_code').removeClass('empty-err');
			$('input#other_country').val("");
			//$('#province').removeClass('hide');
			//$('#province2').addClass('hide');
		}
	});

	$('#eligible_type_M').click(function () {
		if ($("#eligible_type_M").hasClass('active')) {
			//$("#eligible_type_M").removeClass('active');
			//$("#eligible_type_W").addClass('active');
		} else {
			$("#eligible_type_M").addClass('active');
			$("#eligible_type_W").removeClass('active');
		}

		$('#classes').val('M');
	});

	$('#eligible_type_W').click(function () {
		if ($("#eligible_type_W").hasClass('active')) {
			//$("#eligible_type_W").removeClass('active');
			//$("#eligible_type_M").addClass('active');
		} else {
			$("#eligible_type_W").addClass('active');
			$("#eligible_type_M").removeClass('active');
		}
		$('#classes').val('W');
	});

	$("#completeRegisterForm input[type='submit']").click(function () {

		$('#loading-spinner').removeClass('hide');
		$('#firstname').removeClass('empty-err');
		$('#lastName').removeClass('empty-err');
		$('input#other_country').removeClass('empty-err');
		$('#country').removeClass('empty-err');
		$('#province').removeClass('empty-err');
		$('#province2').removeClass('empty-err');
		$('#location').removeClass('empty-err');
		$('#postal_code').removeClass('empty-err');
		$('#dob').removeClass('empty-err');
		$('#eligible_type_M').removeClass('empty-err');
		$('#eligible_type_W').removeClass('empty-err');
		$('#err_msgs').removeClass('error');
		$('#terms_policy').parent('div').removeClass('empty-err');
		$("#err_msgs").text('');
		$('#phone').removeClass('empty-err');
		$(".country_dial_code").removeAttr('style');
		$("#phone").attr('style', 'margin-top: 0px;');

		var isFormValid = true;

		if ($("#postal_err").hasClass('error-message')) {
			if (!$("#postal_err").hasClass('hide')) {
				isFormValid = false;
				//console('postal error')
			}

		}
		if (isNotEmpty($('#firstname').val().trim()) == false) {
			$('#firstname').addClass('empty-err');
			isFormValid = false;
		}

		if (isNotEmpty($('#lastName').val().trim()) == false) {
			$('#lastName').addClass('empty-err');
			isFormValid = false;
		}
		if (isNotEmpty($('#classes').val().trim()) == false) {
			$('#eligible_type_M').addClass('empty-err');
			$('#eligible_type_W').addClass('empty-err');
			isFormValid = false;
		}
		if ($('#country').val().trim() == "other") {
			if (isNotEmpty($('input#other_country').val().trim()) == false) {
				$('input#other_country').addClass('empty-err');
				isFormValid = false;
			}
		}
		else if (isNotEmpty($('#country').val().trim()) == false) {
			$('#country').addClass('empty-err');
			isFormValid = false;
		}
		/*if (isNotEmpty($('#province2').val().trim()) == false) {
			$('#province2').addClass('empty-err');
			//console('here')
			isFormValid = false;
		}*/
		if (isNotEmpty($('#location').val().trim()) == false) {
			$('#location').addClass('empty-err');
			isFormValid = false;
		}
		if (isNotEmpty($('#postal_code').val().trim()) == false) {
			$('#postal_code').addClass('empty-err');
			isFormValid = false;
		}

		$('#postal_err').addClass('hide');
		$("#postal_err").text("");
		$('#phone_err').addClass('hide');
		$("#phone_err").text("");
		$('#dial_code_err').addClass('hide');
		$("#dial_code_err").text("");
		$("#postal_err").text("");

		if (isNotEmpty($('#dial_code').val().trim()) == false && isNotEmpty($('#phone').val().trim()) == true) {
			$('#dial_code_err').removeClass('hide');
			$('#phone_err').removeClass('hide');
			$("#dial_code_err").text("Country code required");
			isFormValid = false;
			$("#phone").attr('style', 'margin-top: 23px;');
			$("#dial_code_county_code").val('')
		} else {
			var seletedcountrycode = $("#dial_code option:selected").text();
			var indexValue = seletedcountrycode.indexOf("+");
			if (indexValue != -1) {
				$("#dial_code_county_code").val(seletedcountrycode.substring(0, indexValue).trim())
			}
		}

		if (isNotEmpty($('#phone').val().trim()) == false && isNotEmpty($('#dial_code').val().trim()) == true) {
			$('#dial_code_err').removeClass('hide');
			$('#phone_err').removeClass('hide');
			$("#phone_err").text("Phone number is required");
			isFormValid = false;
			$(".country_dial_code").attr('style', 'margin-top: 24px;');
		}

		var country = $('#country').val();
		var postal_code = $('#postal_code').val();
		var phone = $('#phone').val();
		var country_error = $(".country_required_error").find("span.selection").find("span.select2-selection.select2-selection--single");
		country_error.css("border", "solid 1px #cfcfcf");
		var dial_code_error = $(".country_dial_code").find("span.selection").find("span.select2-selection.select2-selection--single");
		dial_code_error.css("border", "solid 1px #cfcfcf");
		if (country == "USA") {
			var us_regex = /(^\d{5}$)|(^\d{5}-\d{4}$)/
			if (!us_regex.test(postal_code)) {
				$('#postal_err').removeClass('hide');
				// $("#postal_err").text("Invalid "+country+" Postal Code");
				$("#postal_err").text("Invalid zip code (ex 12345)");
				isFormValid = false;
			}
		}
		else if (country == "Canada") {
			var ca_regex = /^([a-zA-Z][0-9][a-zA-Z])\s*([0-9][a-zA-Z][0-9])$/;
			if (!ca_regex.test(postal_code)) {
				$('#postal_err').removeClass('hide');
				// $("#postal_err").text("Invalid "+country+" Postal Code");
				$("#postal_err").text("Invalid postal code (ex A1B 2C3)");
				isFormValid = false;
			}
		} else if (country == "") {
			country_error.css("border", "1px solid #B40000");
		}
		var dob = $('#dob').val();
		if (dob) {
			var dob_new = moment(new Date(dob)).format('YYYY-MM-DD');
			var mailchimDOB = moment(new Date(dob)).format('MM/DD');
			var years = moment().diff(dob_new, 'years');
			var age = years;

			if (!parseInt(age)) {
				$('#dob').addClass('empty-err');
				isFormValid = false;
			}
			if (age < 13) {
				$('#dob').addClass('empty-err');
				isFormValid = false;
			}
		}
		else {
			$('#dob').addClass('empty-err');
			isFormValid = false;
		}

		/*if (isNotEmpty($('#dial_code').val().trim()) == false) {
			dial_code_error.css("border","1px solid #B40000");
			isFormValid = false;
		}

		if (isNotEmpty($('#phone').val().trim()) == false) {
			$('#phone').addClass('empty-err');
			isFormValid = false;
		}else{
			var phone_regex = /[0-9 -()+]+$/;
			if(!phone_regex.test(phone)){
				$('#phone_err').removeClass('hide');
				$("#phone_err").text("Invalid phone number (ex +14439798200)");
				isFormValid = false;
			}
		} */

		/*if (isNotEmpty($('#dob').val().trim()) == false) {
			$('#dob').addClass('empty-err');
			isFormValid = false;
		} else if (moment($('#dob').val().trim(),'MM/DD/YYYY').isValid() == false) {
			$('#dob').addClass('empty-err');
			isFormValid = false;
		}*/


		if (isNotEmpty($('#phone').val().trim()) == true) {
			var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

			if (phoneRegex.test(phone)) {
				if ((phone.indexOf('-') == -1 && phone.indexOf('.') == -1) || (phone.indexOf('-') > 0 && phone.indexOf('.') > 0)) {
					var formattedPhoneNumber =
						phone.replace(phoneRegex, "$1-$2-$3");
					$('#phone').val(formattedPhoneNumber);
				}
				$('#phone').removeClass('empty-err');
				$('#phone_err').addClass('hide');
			} else {
				$('#phone').addClass('empty-err');
				$('#phone_err').removeClass('hide');
				$("#phone_err").html("Invalid phone number<br> (ex 443-979-8200 or 443.979.8200)");
				isFormValid = false;
				if (isNotEmpty($('#dial_code').val().trim()) == false) {
					$(".country_dial_code").attr('style', 'margin-top: 47px;');
				} else {
					$(".country_dial_code").attr('style', 'margin-top: 70px;');
				}
			}
		}

		if (!$('#terms_policy').is(":checked")) {
			$('#terms_policy').parent('div').addClass('empty-err');
			$('#terms_policy').parent('div').css({ "border-radius": "6px", "margin-left": "-10px" });
			$('.terms_policy').css("margin-left", "9px");
			$('.terms_policy').find('span.box').css("left", "9px");
			$('.terms_policy').find('span.check').css("margin-left", "9px");
			isFormValid = false;
		}

		if (isFormValid) {
			$("#completeRegisterForm").submit();
		} else {
			$('#loading-spinner').addClass('hide');
			$("html, body").animate({ scrollTop: $("#completeRegisterForm").offset().top }, "slow");
			return false;
		}
	});
	$(document).on('cut copy paste', "#phone,#event_phone_number, #venue_name, #venue_street_address, #free_practice_num , .remove-copy-paste", function (e) {
		e.preventDefault();
	});

	// $('#phone, #venue_name, #venue_street_address, #free_practice_num , edit-event-other-item-input').bind("cut copy paste",function(e) {
	// 	e.preventDefault();
	//  });
});

// js for paddler search

$(function () {
	$("#paddler_adv_search button[type='submit']").click(function () {
		$('input').val(function (_, value) {
			return $.trim(value);
		});
		$('#loading-spinner').removeClass('hide');
		$('#paddler_adv_search').submit()
	});

	if ($('#paddler_search_offset').val() == '0') {
		$('.prev-paddler-page').addClass("hide")
	}

	if ($('#paddler_search_offset').val() == $('#paddler_search_total').val()) {
		$('.next-paddler-page').addClass("hide")
	}

	$('.next-paddler-page').click(function (e) {
		var offset = parseInt($('#paddler_search_offset').val()) + parseInt($('#paddler_search_limit').val())
		var total = parseInt($('#paddler_search_total').val())

		if (offset != total && offset <= total) {
			$('#paddler_search_offset').val(offset)
			$('#paddler_search_type').val('')
			$('#paddler_adv_search').submit()
		}


	});

	$('.prev-paddler-page').click(function (e) {
		var offset = parseInt($('#paddler_search_offset').val()) - parseInt($('#paddler_search_limit').val())

		if (offset >= 0) {
			$('#paddler_search_offset').val(offset)
			$('#paddler_search_type').val('')
			$('#paddler_adv_search').submit()
		}


	});

	$('.last-paddler-page').click(function (e) {
		var offset = parseInt($('#paddler_search_total').val())
		$('#paddler_search_offset').val(offset)
		$('#paddler_search_type').val('last')
		$('#paddler_adv_search').submit()

	});

	$('.first-paddler-page').click(function (e) {
		$('#paddler_search_offset').val('0')
		$('#paddler_search_type').val('')
		$('#paddler_adv_search').submit()

	});

	$("#rows_per_page_paddler").change(function () {
		//console.log($(this).val())
		offset = parseInt($('#paddler_search_offset').val())
		total = parseInt($('#paddler_search_total').val())

		if (offset == total) {
			offset = total - parseInt($(this).val())
			$('#paddler_search_offset').val(offset)
			$('#paddler_search_limit').val($(this).val())
		}
		else {
			$('#paddler_search_limit').val($(this).val())
		}


		$('#paddler_adv_search').submit()
	});


	$(".page_num_paddler").click(function () {
		$('#paddler_search_offset').val($(this).attr('id'))
		$('#paddler_search_type').val('')
		$('#paddler_adv_search').submit()
	});
})


// js for team search
$(function () {

	$("#team_adv_search button[type='submit']").click(function (e) {
		e.preventDefault();
		$('input').val(function (_, value) {
			return $.trim(value);
		});
		$('#loading-spinner').removeClass('hide');
		$('#offset').val('0')
		$('#team_adv_search').submit()
	});

	$('#all_divisions').click(function (e) {
		$divArr = $("input[type=checkbox][name='divisions[]']");
		if (document.getElementById('all_divisions').checked === true) {
			for (var i = 0; i < $divArr.length; i++) {
				$divArr[i].checked = true;
			}
		} else {
			for (var i = 0; i < $divArr.length; i++) {
				$divArr[i].checked = false;
			}
		}
	});

	var team_id = "";

	$('.team-delete ').click(function () {
		team_id = $(this).val()
		$("#deleteTeamModal").modal("show");
	});

	$('#deleteTeamBtn').click(function (e) {
		$.ajax({
			type: "POST",
			url: "/delete-team",
			data: { "team_id": team_id }
		}).done(function (msg) {
			////console(msg)
			if (msg == "true") {
				location.reload();
			}
			else {
				//console("Error")
			}

		});
	});

	if ($('#offset').val() == '0') {
		$('.prev-team-page').addClass("hide")
	}

	if ($('#offset').val() == $('#total').val()) {
		$('.next-team-page').addClass("hide")
	}

	$('.next-team-page').click(function (e) {
		/*$.ajax({
			type: "POST",
			url: "/teamList",
			data: $('#team_adv_search').serialize()
		}).done(function(msg) {
			
			location.reload();
			
		});*/
		var offset = parseInt($('#offset').val()) + parseInt($('#limit').val())
		var total = parseInt($('#total').val())

		if (offset != total && offset <= total) {
			$('#offset').val(offset)
			$('#type').val('')
			$('#team_adv_search').submit()
		}


	});

	$('.prev-team-page').click(function (e) {
		var offset = parseInt($('#offset').val()) - parseInt($('#limit').val())

		if (offset >= 0) {
			$('#offset').val(offset)
			$('#type').val('')
			$('#team_adv_search').submit()
		}


	});

	$('.last-team-page').click(function (e) {
		var offset = parseInt($('#total').val())
		$('#offset').val(offset)
		$('#type').val('last')
		$('#team_adv_search').submit()

	});

	$('.first-team-page').click(function (e) {
		$('#offset').val('0')
		$('#type').val('')
		$('#team_adv_search').submit()

	});

	$("#rows_per_page").change(function () {
		//console.log($(this).val())
		offset = parseInt($('#offset').val())
		total = parseInt($('#total').val())

		if (offset == total) {
			offset = total - parseInt($(this).val())
			$('#offset').val(offset)
			$('#limit').val($(this).val())
		}
		else {
			$('#limit').val($(this).val())
		}


		$('#team_adv_search').submit()
	});


	$(".page_num").click(function () {
		$('#offset').val($(this).attr('id'))
		$('#type').val('')
		$('#team_adv_search').submit()
	});
});

// js for event search
$(function () {

	$("#event_adv_search button[type='submit']").click(function () {
		$('#event_adv_search').submit(function () {
			//console("show loader")
			$('input').val(function (_, value) {
				return $.trim(value);
			});
			$('#loading-spinner').removeClass('hide');
			$('#event_adv_search').submit()

		})
	});

	$('#divisions').click(function (e) {
		$divArr = $("input[type=checkbox][name='check[]']");
		if (document.getElementById('divisions').checked === true) {
			for (var i = 0; i < $divArr.length; i++) {
				$divArr[i].checked = true;
			}
		} else {
			for (var i = 0; i < $divArr.length; i++) {
				$divArr[i].checked = false;
			}
		}
	});

	$('#date_from').focus(function () {
		$('#date_from').datepicker({
			autoclose: true,
		})
	});

	$('#datepicker-start-icon').click(function () {
		$('#date_from').datepicker({
			autoclose: true,
		}).focus();
	});

	$('#date_to').focus(function () {
		$('#date_to').datepicker({
			autoclose: true,
			minDate: $("#date_from").datepicker("getDate")
		})
	});

	$('#datepicker-end-icon').click(function () {
		$('#date_to').datepicker({
			autoclose: true,
			minDate: $("#date_from").datepicker("getDate")
		}).focus();
	});

	$('#recent').click(function (e) {
		$('#archive, #upcoming').removeClass('active')
		$('#past_table, #upcoming_table').addClass('hide')

		$('#recent').addClass('active')
		$('#recent_table').removeClass('hide')
		if (loginScriptStatus != 1) {
			sessionStorage.setItem('activeEvent', "#recent")
		}
	});
	$('#archive').click(function (e) {
		$('#recent, #upcoming').removeClass('active')
		$('#recent_table, #upcoming_table').addClass('hide')

		$('#archive').addClass('active')
		$('#past_table').removeClass('hide')
		if (loginScriptStatus != 1) {
			sessionStorage.setItem('activeEvent', "#archive")
		}
	});
	$('#upcoming').click(function (e) {
		$('#archive, #recent').removeClass('active')
		$('#past_table, #recent_table').addClass('hide')

		$('#upcoming').addClass('active')
		$('#upcoming_table').removeClass('hide')
		if (loginScriptStatus != 1) {
			sessionStorage.setItem('activeEvent', "#upcoming")
		}
	});

	/*$('#past_view_table tbody tr td:nth-child(4)').each( function(){
		var arr = $(this).text().split(';');
		//console($(this).text())
		var date = ''
		if (arr[0].trim().length > 0 ) {
			if (arr[1].trim().length > 0) {
				date = moment(arr[0].trim()).format("D MMM");
			}
			else {
				date = moment(arr[0].trim()).format("D MMM YYYY");
			}
			
		}
		if (arr[1].trim().length > 0 ) {
			date = date + ' - ' + moment(arr[1].trim()).format("D MMM YYYY");
		}

		$(this).text(date)
	});
	$('#recent_view_table tbody tr td:nth-child(4)').each( function(){
		var arr = $(this).text().split(';');
		var date = ''
		if (arr[0].trim().length > 0 ) {
			if (arr[1].trim().length > 0) {
				date = moment(arr[0].trim()).format("D MMM");
			}
			else {
				date = moment(arr[0].trim()).format("D MMM YYYY");
			}
		}
		if (arr[1].trim().length > 0 ) {
			date = date + ' - ' + moment(arr[1].trim()).format("D MMM YYYY");
		}

		$(this).text(date)
	});
	$('#upcoming_view_table tbody tr td:nth-child(4)').each( function(){
		var arr = $(this).text().split(';');
		var date = ''
		if (arr[0].trim().length > 0 ) {
			if (arr[1].trim().length > 0) {
				date = moment(arr[0].trim()).format("D MMM");
			}
			else {
				date = moment(arr[0].trim()).format("D MMM YYYY");
			}
		}
		if (arr[1].trim().length > 0 ) {
			date = date + ' - ' + moment(arr[1].trim()).format("D MMM YYYY");
		}

		$(this).text(date)
	});*/

	var event_id = "";
	$('.listing-delete').click(function () {
		event_id = $(this).val();
		$("#deleteEventModal").modal("show");
	});

	$('#deleteBtn').click(function (e) {
		//console(event_id)
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "POST",
			url: "/delete-event",
			data: { "event_id": event_id }
		}).done(function (msg) {
			////console(msg)
			if (msg == "true") {
				location.reload();
			}
			else {
				$('#loading-spinner').addClass('hide');
				//console("Error")
			}

		});
	});

	// Pagination code for upcoming event

	if ($('#upcoming_offset').val() == '0') {
		$('.prev-upcoming-page').addClass("hide")
	}

	if ($('#upcoming_offset').val() == $('#upcoming_total').val()) {
		$('.next-upcoming-page').addClass("hide")
	}

	$('.next-upcoming-page').click(function (e) {
		var offset = parseInt($('#upcoming_offset').val()) + parseInt($('#upcoming_limit').val())
		var total = parseInt($('#upcoming_total').val())

		if (offset != total && offset <= total) {
			$('#upcoming_offset').val(offset)
			$('#upcoming_type').val('')
			$('#section').val('Upcoming')
			$('#event_adv_search').submit()
		}


	});

	$('.prev-upcoming-page').click(function (e) {
		var offset = parseInt($('#upcoming_offset').val()) - parseInt($('#upcoming_limit').val())

		if (offset >= 0) {
			$('#upcoming_offset').val(offset)
			$('#upcoming_type').val('')
			$('#section').val('Upcoming')
			$('#event_adv_search').submit()
		}


	});

	$('.last-upcoming-page').click(function (e) {
		var offset = parseInt($('#upcoming_total').val())
		$('#upcoming_offset').val(offset)
		$('#upcoming_type').val('last')
		$('#section').val('Upcoming')
		$('#event_adv_search').submit()

	});

	$('.first-upcoming-page').click(function (e) {
		$('#upcoming_offset').val('0')
		$('#upcoming_type').val('')
		$('#section').val('Upcoming')
		$('#event_adv_search').submit()

	});

	$("#rows_per_page_upcoming").change(function () {
		offset = parseInt($('#upcoming_offset').val())
		total = parseInt($('#upcoming_total').val())

		if (offset == total) {
			offset = total - parseInt($(this).val())
			$('#upcoming_offset').val(offset)
			$('#upcoming_limit').val($(this).val())
		}
		else {
			$('#upcoming_limit').val($(this).val())
		}

		$('#section').val('Upcoming')
		$('#event_adv_search').submit()
	});


	$(".page_num_upcoming").click(function () {
		$('#upcoming_offset').val($(this).attr('id'))
		$('#upcoming_type').val('')
		$('#section').val('Upcoming')
		$('#event_adv_search').submit()
	});

	// Pagination code for recent event

	if ($('#recent_offset').val() == '0') {
		$('.prev-recent-page').addClass("hide")
	}

	if ($('#recent_offset').val() == $('#recent_total').val()) {
		$('.next-recent-page').addClass("hide")
	}

	$('.next-recent-page').click(function (e) {
		var offset = parseInt($('#recent_offset').val()) + parseInt($('#recent_limit').val())
		var total = parseInt($('#recent_total').val())

		if (offset != total && offset <= total) {
			$('#recent_offset').val(offset)
			$('#recent_type').val('')
			$('#section').val('Recent')
			$('#event_adv_search').submit()
		}


	});

	$('.prev-recent-page').click(function (e) {
		var offset = parseInt($('#recent_offset').val()) - parseInt($('#recent_limit').val())

		if (offset >= 0) {
			$('#recent_offset').val(offset)
			$('#recent_type').val('')
			$('#section').val('Recent')
			$('#event_adv_search').submit()
		}


	});

	$('.last-recent-page').click(function (e) {
		var offset = parseInt($('#recent_total').val())
		$('#recent_offset').val(offset)
		$('#recent_type').val('last')
		$('#section').val('Recent')
		$('#event_adv_search').submit()

	});

	$('.first-recent-page').click(function (e) {
		$('#recent_offset').val('0')
		$('#recent_type').val('')
		$('#section').val('Recent')
		$('#event_adv_search').submit()

	});

	$("#rows_per_page_recent").change(function () {
		offset = parseInt($('#recent_offset').val())
		total = parseInt($('#recent_total').val())

		if (offset == total) {
			offset = total - parseInt($(this).val())
			$('#recent_offset').val(offset)
			$('#recent_limit').val($(this).val())
		}
		else {
			$('#recent_limit').val($(this).val())
		}

		$('#section').val('Recent')

		$('#event_adv_search').submit()
	});


	$(".page_num_recent").click(function () {
		$('#recent_offset').val($(this).attr('id'))
		$('#recent_type').val('')
		$('#section').val('Recent')
		$('#event_adv_search').submit()
	});


	//Pagination code for past events

	if ($('#past_offset').val() == '0') {
		$('.prev-past-page').addClass("hide")
	}

	if ($('#past_offset').val() == $('#past_total').val()) {
		$('.next-past-page').addClass("hide")
	}

	$('.next-past-page').click(function (e) {
		var offset = parseInt($('#past_offset').val()) + parseInt($('#past_limit').val())
		var total = parseInt($('#past_total').val())

		if (offset != total && offset <= total) {
			$('#past_offset').val(offset)
			$('#past_type').val('')
			$('#section').val('Past')
			$('#event_adv_search').submit()
		}


	});

	$('.prev-past-page').click(function (e) {
		var offset = parseInt($('#past_offset').val()) - parseInt($('#past_limit').val())

		if (offset >= 0) {
			$('#past_offset').val(offset)
			$('#past_type').val('')
			$('#section').val('Past')
			$('#event_adv_search').submit()
		}


	});

	$('.last-past-page').click(function (e) {
		var offset = parseInt($('#past_total').val())
		$('#past_offset').val(offset)
		$('#past_type').val('last')
		$('#section').val('Past')
		$('#event_adv_search').submit()

	});

	$('.first-past-page').click(function (e) {
		$('#past_offset').val('0')
		$('#past_type').val('')
		$('#section').val('Past')
		$('#event_adv_search').submit()

	});

	$("#rows_per_page_past").change(function () {
		offset = parseInt($('#past_offset').val())
		total = parseInt($('#past_total').val())

		if (offset == total) {
			offset = total - parseInt($(this).val())
			$('#past_offset').val(offset)
			$('#past_limit').val($(this).val())
		}
		else {
			$('#past_limit').val($(this).val())
		}

		$('#section').val('Past')

		$('#event_adv_search').submit()
	});


	$(".page_num_past").click(function () {
		$('#past_offset').val($(this).attr('id'))
		$('#past_type').val('')
		$('#section').val('Past')
		$('#event_adv_search').submit()
	});
});

//js for edit event
$(function () {

	$("#edit-form-submit").click(function (e) {
		e.preventDefault();
		$('.success-message ').addClass('hide');
		$('#loading-spinner').removeClass('hide');

		$('input').val(function (_, value) {
			return $.trim(value);
		});

		$('#twitter_err').addClass('hide');
		$('#phone_err').addClass('hide');
		$('#website_err').addClass('hide');
		$('#fb_err').addClass('hide');
		$('#youtube_err').addClass('hide');
		$('#email_err').addClass('hide');
		$("#event_name_err").addClass('hide');
		$("#date_err").addClass('hide');
		$('#events_err').addClass('hide');
		$('#reg_dates_err').addClass('hide');

		$('#event_email').removeClass('empty-err');
		$('#twitter_handle').removeClass('empty-err');
		$('#event_phone_number').removeClass('empty-err');
		$('#website_url').removeClass('empty-err');
		$('#facebook_page_url').removeClass('empty-err');
		$('#youtube_url').removeClass('empty-err');

		$("#events_err").addClass('hide');
		$("#events_err span").text("");
		$("span.error-message").addClass('hide');
		$("span.error-message").text("");

		var date_flag = 0;
		var event_name = jQuery.trim($("#event_name").val());
		var start_date = jQuery.trim($('#start_date').val());
		var end_date = $('#end_date').val();
		var event_about = $("#event_about").val();
		var seb_date = $('#seb_date').val();
		var eb_date = $('#eb_date').val();
		var rb_date = $('#rb_date').val();
		var location = jQuery.trim($('#location').val());
		var province = jQuery.trim($('#province').val());
		var country = jQuery.trim($('#country').val());
		var venue_name = jQuery.trim($('#venue_name').val());
		var venue_street_address = jQuery.trim($('#venue_street_address').val());
		var tw = jQuery.trim($('#twitter_handle').val());
		var fb = jQuery.trim($('#facebook_page_url').val());
		var yt = jQuery.trim($('#youtube_url').val());
		var web = jQuery.trim($('#website_url').val());
		var email = jQuery.trim($('#event_email').val());
		var contact = jQuery.trim($('#event_phone_number').val());
		var about_cause = jQuery.trim($('#about_event_cause').val());
		var coaching_fee = $('#coaching_service').val();
		var currency_type = $('#currency').val();

		var phone_regex = /^[0-9\s-]*$/;
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var event_name_regex = /^[a-zA-Z\d\-_.,(){}'\[\]\s]+$/;
		var prefix = 'http://';
		var prefix2 = 'https://';
		var rege = /^[a-zA-Z0-9À-ž\s-']*$/;
		var isFormValid = true;
		var isFormValid1 = true;
		var isFormValid2 = true;
		$(".error_question_count").addClass('hide')

		if (!event_name) {
			$('#events_err').removeClass('hide');
			$('#events_err span').text("Event Name is required");
			$('#loading-spinner').addClass('hide');
			isFormValid = false;
		}



		if (end_date) {
			end_date = moment(new Date(end_date)).format('YYYY-MM-DD[T]12:00:00.SSS');
			var date_flag = dateValidate(start_date, end_date);
		} else {
			end_date = null;
			var date_flag = 1;
		}
		if (!start_date) {
			$('#events_err').removeClass('hide');
			$('#events_err span').text("Event date is required");
			$('#loading-spinner').addClass('hide');
			isFormValid = false;
			//return false;
		}

		var week_number = moment(start_date).isoWeek();
		var month = moment(start_date).month();
		var year = moment(start_date).year();
		start_date = moment(new Date(start_date)).format('YYYY-MM-DD[T]12:00:00.SSS');
		var same_event_id = "";


		if (seb_date != "Invalid Date" && seb_date != "")
			seb_date = moment(new Date(seb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');

		if (eb_date != "Invalid Date" && eb_date != "")
			eb_date = moment(new Date(eb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');

		if (rb_date != "Invalid Date" && rb_date != "") {
			rb_date = moment(new Date(rb_date)).format('YYYY-MM-DD[T]12:00:00.SSS');
		}


		var bird_flag = birdDateValidate(seb_date, eb_date, rb_date);


		if (date_flag == 0) {
			$("#date_err").removeClass('hide');
			$("#date_err").text("The end date you entered occurs before the start date");
			$('#loading-spinner').addClass('hide');
			isFormValid = false;
			return false;
		}
		if (!rege.test(location) || !rege.test(province) || !rege.test(country) || !venue_name || !venue_street_address) {
			$('#events_err').removeClass('hide');
			$("#events_err span").text("Enter a valid address for your event");
			$('#loading-spinner').addClass('hide');
			isFormValid = false;
			return false;
		}
		if ((seb_date != '' && seb_date != 'Invalid date') || (eb_date != '' && eb_date != 'Invalid date') || (rb_date != '' && rb_date != 'Invalid date')) {
			if ((rb_date != '' && rb_date != 'Invalid date' && rb_date > start_date) || (eb_date != '' && eb_date != 'Invalid date' && eb_date > start_date) || (seb_date != '' && seb_date != 'Invalid date' && seb_date > start_date)) {
				$('#reg_dates_err').removeClass('hide');
				$("#reg_dates_err").text("Pricing dates should occur before start date of the event");
				$('#seb_date').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				return false;
			}
			else if (bird_flag == 0) {
				$('#reg_dates_err').removeClass('hide');
				$("#reg_dates_err").text("Each date must fall after the previous date");
				$('#seb_date').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				return false;
			}
		}

		twurl = checkUrlPrefix(tw, prefix2);
		fburl = checkUrlPrefix(fb, prefix2)
		yturl = checkUrlPrefix(yt, prefix2)
		web = checkUrlPrefix(web, prefix)
		if (tw != "") {
			if (isUrlValid(twurl) || tw.substring(0, 1) == "@") {
				$('#twitter_err').removeClass('hide');
				$("#twitter_err").text("Invalid. Please enter only your username");
				$('#twitter_handle').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				return false
			}
			tw = "https://www.twitter.com/" + tw;
		}
		if (fb != "") {
			if (isUrlValid(fburl)) {

				$('#fb_err').removeClass('hide');
				$("#fb_err").text("Invalid. Please enter only your username");
				$('#facebook_page_url').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				return false;
			}
			fb = "https://www.facebook.com/" + fb;
		}

		if (yt != "") {
			if (isUrlValid(yturl)) {
				$('#youtube_err').removeClass('hide');
				$("#youtube_err").text("Invalid. Please enter only your username");
				$('#youtube_url').focus();
				$('#loading-spinner').addClass('hide');
				isFormValid = false;
				return false;
			}
			yt = "http://www.youtube.com/" + yt;
		}

		// if(!phone_regex.test(contact)){
		// 	$('#phone_err').removeClass('hide');
		// 	$('#event_phone_number').addClass('empty-err').focus();
		// 	$("#phone_err").text("Please enter a proper phone number");
		// 	$('#loading-spinner').addClass('hide');

		// 	isFormValid = false;
		// 	return false
		// }

		if (isNotEmpty(contact.trim()) == true) {
			var phoneRegex = /^\+([0-9]{1,3})\)?[\s+][-. ]?([0-9]{3})[-. ]?([0-9]{3})?[-. ]?([0-9]{4})$/;
			if (phoneRegex.test(contact)) {
				var phoneRegex1 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				var phoneExpressionarray = contact.split(' ');
				phoneExpressionarray1 = ""
				if (phoneExpressionarray[1] != "") {
					phoneExpressionarray1 = phoneExpressionarray[1].replace(phoneRegex1, "$1-$2-$3");
				} else {
					phoneExpressionarray1 = phoneExpressionarray[2].replace(phoneRegex1, "$1-$2-$3");
				}
				$('#event_phone_number').val(phoneExpressionarray[0] + ' ' + phoneExpressionarray1);
			} else {
				$('#phone_err').removeClass('hide');
				$('#event_phone_number').addClass('empty-err').focus();
				$("#phone_err").html("Invalid phone number<br> (ex +1 443-979-8200 or +11 443.979.8200)");
				$('#loading-spinner').addClass('hide');
				isFormValid = false
				return false
			}
		}

		/*if(!event_name_regex.test(event_name) && event_name != ""){
			$('#event_name_err').removeClass('hide');
			$('#event_name').addClass('empty-err').focus();
			$("#event_name_err").text("Please remove the special characters from the event name");
			$('#loading-spinner').addClass('hide');
			return false
		}*/

		if (email) {
			if (!email_regex.test(email)) {
				$('#email_err').removeClass('hide');
				$('#event_email').addClass('empty-err').focus();
				$("#email_err").text("Incorrect email address. Please enter a proper email ID");
				$('#loading-spinner').addClass('hide');

				isFormValid = false;
				return false;
			}
		}

		if (web != "" && !isUrlValid(web)) {
			$('#website_err').removeClass('hide');
			$("#website_err").text("Invalid. Enter a proper URL");
			$('#website_url').focus();
			$('#loading-spinner').addClass('hide');
			isFormValid = false;
			return false;
		} else {
			$('#website_url').val(web)
		}

		if (isFormValid) {
			//console("Valid")
			$('[name="M_seb_price[]"]').each(function () {
				//console($(this).val() + '-' + $(this).attr('id') );
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}

			})

			$('[name="W_seb_price[]"]').each(function () {
				//console($(this).val() + '-' + $(this).attr('id') );
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					$(this).addClass("empty-err");
					isFormValid = false
				}
			})

			$('[name="MX_seb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="M_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="W_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="MX_eb_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})


			$('[name="M_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="W_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})

			$('[name="MX_r_price[]"]').each(function () {
				if (!isNaN($(this).val())) {
					$(this).attr('type', 'text');
					$(this).val($(this).val() + '-' + $(this).attr('id'));
				}
				else {
					isFormValid = false
				}
			})


			$('[name="item_title[]"]').each(function () {
				idValue = $(this).attr('id')
				idValue = idValue.replace("item_", "")
				if ($("#item_" + idValue).val().trim() == "" && $("#item_cost_" + idValue).val().trim() != "") {
					isFormValid1 = false
					$("#item_" + idValue).addClass("empty-err")
				} else if ($("#item_" + idValue).val().trim() != "" && $("#item_cost_" + idValue).val().trim() == "") {
					isFormValid1 = false
					$("#item_cost_" + idValue).addClass("empty-err")
				} else {
					$("#item_" + idValue).removeClass("empty-err")
					$("#item_cost_" + idValue).removeClass("empty-err")
				}
			});

			$('[name="event_questions[]"]').each(function () {
				idValue = $(this).attr('id')
				idValue = idValue.replace("question_", "")
				if ($("#question_" + idValue).val().trim().length > 150) {
					isFormValid2 = false
					$("#question_" + idValue).addClass("empty-err")
					$(".error_question_count").removeClass('hide')
				} else {
					$("#question_" + idValue).removeClass("empty-err")
				}
			});

			if (isFormValid && isFormValid1 && isFormValid2) {
				$.ajax({
					type: "POST",
					url: "/check-event-name",
					data: {
						"event_name": event_name,
						"location": location,
						"caller": "edit"
					}
				}).done(function (msg) {
					//console(msg)
					if (msg == "exists") {
						$('#events_err').removeClass('hide');
						$('#events_err span').text(event_name + " already exists in the same location");
						$('#loading-spinner').addClass('hide');
						return false;
					}
					else {

						all_href = $('[name="event_desc"]').find('a')
						for (var i = 0; i < all_href.length; i++) {
							//console($(all_href[i].outerHTML).attr("href"))
							old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
							old_href = old_href.replace("https", "")
							old_href = old_href.replace("http", "")
							old_href = old_href.replace(":", "")
							old_href = old_href.replace("//", "")

							new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
							//console(old_href, new_href)
							editor_id = $('[name="event_desc"]')[0].id
							$("#" + editor_id).html($("#" + editor_id).html().replace(old_href, new_href));

							$("#event_about").val($("#" + editor_id).html())
						};

						$("#editEventForm").submit();
					}

				});
			}
			else {
				//console("Invalid fees")
				$('#loading-spinner').addClass('hide');
				return false
			}
		}
		else {
			$('#loading-spinner').addClass('hide');
			return false
		}
	});

	$('#changeClassDivs').click(function (eve) {
		eve.preventDefault()
		$('#appendClassDivisionMaxTable').empty()
		$('#appendClassDivisionMaxHideShowTable').addClass('hide')
		$('#createboxsectionfooterhideshow').addClass('hide')
		$(".error-message").addClass('hide');
		$(".error-message").html("");
		$('#EditClassDivisionMaxContinueClick').trigger('click');
		$('#eventEditClassDivisionStep1').removeClass('hide')
		$('#eventEditClassDivisionStep2').addClass('hide')
		$("#classDivisionsModal").modal("show");
	});

	function editClass_Division_Boattype(idValueData) {
		thisvalue = $('#' + idValueData)
		tempClass = thisvalue.attr('data-class');
		tempDivision = thisvalue.attr('data-division');
		tempId = thisvalue.attr('id')
		tempClassCode1 = tempId
		tempDivisionCode1 = ""
		tempClassCode1 = tempClassCode1.replace("M_", "");
		tempClassCode1 = tempClassCode1.replace("W_", "");
		tempClassCode1 = tempClassCode1.replace("MX_", "");
		if (tempDivision == "Male") {
			tempDivisionCode1 = "M"
		} else if (tempDivision == "Women") {
			tempDivisionCode1 = "W"
		} else if (tempDivision == "Mixed") {
			tempDivisionCode1 = "MX"
		}
		eventBoatTypeWithJosn = $('#eventBoatTypeWithJosn').val()
		boatType = ""
		if (eventBoatTypeWithJosn != "null" && eventBoatTypeWithJosn != "") {
			$.each(JSON.parse(eventBoatTypeWithJosn), function (key, value) {
				if ((tempClassCode1 == value.class) && (tempDivisionCode1 == value.div)) {
					boatType = value.boat_type
				}
			});
			class_Division_Boattype(tempClass, tempDivision, tempId, tempClassCode1, tempDivisionCode1, boatType)
		} else {
			class_Division_Boattype(tempClass, tempDivision, tempId, tempClassCode1, tempDivisionCode1, boatType)
		}
	}

	$('#start_dtpicker').click(function () {
		$('#start_date').datepicker({
			minDate: new Date(),
			onSelect: function (selected_date) {
				var selectedDate = new Date(selected_date);
				var msecsInADay = 86400000;
				var endDate = new Date(selectedDate.getTime() + msecsInADay);
				$("#end_date").datepicker("option", "minDate", endDate);
			}
		}).focus();
	});

	$('#end_dtpicker').click(function () {
		$('#end_date').datepicker({
			autoclose: true,
			minDate: new Date($("#start_date").val())
		}).focus();
	});

	$('#reg_dtpicker').click(function () {
		var min_date;
		var max_date = new Date($("#start_date").val())
		if ($("#eb_date").datepicker("getDate")) {
			min_date = $("#eb_date").datepicker("getDate");
		}
		else {
			min_date = new Date();
		}
		$('#rb_date').datepicker({
			autoclose: true,
			//minDate: min_date,
			//maxDate: max_date
		});

		//$('#r_date').datepicker('option', 'changeMonth', true )
		$('#rb_date').datepicker('option', 'maxDate', max_date)
		$('#rb_date').datepicker('option', 'minDate', min_date)
		$('#rb_date').datepicker().focus();

		//console(max_date)
		//console(min_date)
	});

	$('#rb_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#single_day_event').change(function () {
		if ($('#single_day_event').is(":checked")) {
			$('#end_date').val('');
			$('#end_date').attr("disabled", "disabled");
		} else {
			$('#end_date').removeAttr('disabled');
		}
	});

	$('#addOrganizer').click(function (eve) {
		eve.preventDefault()
		$('#organizerEmail').val('');
		$('#organizerMessage').val('');
		$('#succ_msg').removeClass('success');
		$('#err_msg, #existing_err_msg').removeClass('error');
		$("#succ_msg").text("");
		$("#err_msg, #existing_err_msg").text("");
		$("#addEventOrganizer").modal("show");
	});

	$('.remove-organizer').click(function (e) {
		e.preventDefault();
		var eoId = this.id;
		//console(eoId)

		var total_organizers = $("#total_organizers").val();

		if (eoId && total_organizers != 1) {
			//Get the person's name 
			$.ajax({
				url: "/get-user-name",
				type: "POST",
				data: { "user_id": eoId },
				success: function (data) {
					//console("Success")
					$('#eventOrgId').val(eoId);
					$('.get-event-org-name').text(data);
					$("#confirmDeleteModal").modal("show");
					$('#deleteEventOrganizer').click(function () {
						$.ajax({
							url: "/remove-event-co-organizer",
							type: "POST",
							data: { "eo_id": eoId },
							success: function (data) {
								$('#rem_succ_msgs').addClass('success');
								$("#rem_succ_msgs").text("Event organizer has been succesfully removed");
								setTimeout(function () {
									$("#confirmDeleteModal").modal("hide");
									location.reload();
								}, FIVE_SEC);

							}
						});
					});
				}
			});

		}
		else {
			$('#addOrganizerModal').modal('show');
		}
	})

	$('#addSponsor').click(function (eve) {
		eve.preventDefault();
		$('#es_err_msg').removeClass('error');
		$("#es_err_msg").text('');
		$("#uploadSponsorModal").modal("show");
	});

	$("#eventSponsorUpload").change(function () {
		$('#es_err_msg').removeClass('error');
		$("#es_err_msg").text('');
		var ext = getFileExtension($('#eventSponsorUpload').val());
		if (validateExt(ext)) {
			$("#event_sponsor_upload").submit()
		}
		else {
			$('#es_err_msg').addClass('error');
			$("#es_err_msg").text("Image must be png, jpg, jpeg");
			return
		}
	});

	$("#event_sponsor_upload").on('submit', (function (e) {
		e.preventDefault();
		//$('#loading-spinner').removeClass('hide');
		$('#es_err_msg').removeClass('error');
		$("#es_err_msg").text('');
		var formData = new FormData(this);

		$.ajax({
			url: "/upload-event-sponsor",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data == "bad response") {
					//console(data)
					$('#es_err_msg').addClass('error');
					$("#es_err_msg").text("Bad Response. Try again");
					$('#loading-spinner').addClass('hide');
				}
				else {
					//console("Success")
					$("#uploadSponsorModal .progress-bar").attr("aria-valuenow", 100);
					$("#uploadSponsorModal .progress-bar").css("width", "100%");
					$("#uploadSponsorModal .sr-only").text("100% Complete");
					setTimeout(function () {
						$("#uploadSponsorModal").modal("hide");
						location.reload();
					}, TWO_SEC);
				}

			}
		});
	}));

	$('#removeEventSponsors').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');
		var sponsor_id = [];
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_succ_msgs").text("");
		$('input:checkbox[name="sponsors_id[]"]').each(function () {
			if (this.checked) {
				sponsor_id.push(this.value);
			}
		});
		//console(sponsor_id)
		if (sponsor_id.length > 0) {
			$.ajax({
				type: "POST",
				url: "/remove-event-sponsor",
				data: { "ids": sponsor_id }
			}).done(function (msg) {
				////console(msg)
				if (msg == "true") {
					location.reload();
				}
				else {
					$('#loading-spinner').addClass('hide');
					//console("Error")
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	$('#uploadLogo').click(function (e) {
		e.preventDefault();
		$('#ev_err_msg').removeClass('error');
		$("#ev_err_msg").text('');
		$("#uploadLogoModal").modal("show");
	});

	$("#eventLogoUpload").change(function () {
		$('#ev_err_msg').removeClass('error');
		$("#ev_err_msg").text('');
		var ext = getFileExtension($('#eventLogoUpload').val());
		if (validateExt(ext)) {
			
			eventLogoUploadReadURL(this)
		}
		else {
			$('#ev_err_msg').addClass('error');
			$("#ev_err_msg").text("Image must be png, jpg, jpeg");
			return
		}
	})

	function eventLogoUploadReadURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);

			reader.onload = function () {
				var image = new Image();
				image.src = reader.result;

				image.onload = function () {
					if (image.width > 100 || image.height > 100) {
						$('#ev_err_msg').addClass('error');
						$("#ev_err_msg").text("Please upload a image, 100x100 pixels");
						return
					} else {
						$("#event_image_upload").submit()
					}

				};
			};
		}
	}

	$("#event_image_upload").on('submit', (function (e) {
		e.preventDefault();
		var formData = new FormData(this);
		$('#loading-spinner').removeClass('hide');
		$('#ev_err_msg').removeClass('error');
		$("#ev_err_msg").text('');

		$.ajax({
			url: "/upload-event-img",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				//console("Success")
				$("#uploadLogoModal .progress-bar").attr("aria-valuenow", 100);
				$("#uploadLogoModal .progress-bar").css("width", "100%");
				$("#uploadLogoModal .sr-only").text("100% Complete");
				$('#loading-spinner').addClass('hide');
				setTimeout(function () {
					$("#uploadLogoModal").modal("hide");
					location.reload();
				}, TWO_SEC);
			}
		});
	}));

	$('#submitAddOrganizer').click(function () {
		$('#loading-spinner').removeClass('hide');
		$('#succ_msg').removeClass('success');
		$('#err_msg, #new_err_msg').removeClass('error');
		$("#succ_msg, #err_msg, #new_err_msg").text("");

		var emails = $('#organizerEmail').val();
		var email_array = emails.split(',')
		var invalid_emails = []


		for (var i = 0; i < email_array.length; i++) {
			email_array[i] = email_array[i].trim()
			var regex_email = email_array[i].match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
			if (!regex_email) {
				invalid_emails.push(email_array[i])
			}
		};

		valid_emails = email_array.filter(function (val) {
			return invalid_emails.indexOf(val) == -1;
		});

		//console(valid_emails)

		if (valid_emails.length > 0) {
			$.ajax({
				type: "POST",
				url: "/co-organizer-invite",
				data: {
					"email": valid_emails,
					"message": $("#organizerMessage").val()
				}
			}).done(function (msg) {
				var obj = $.parseJSON(msg)
				//console(obj)
				if (obj.sent != null) {
					$('#succ_msg').addClass('success');
					$("#succ_msg").text("Invitation sent to " + obj.sent.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (obj.alreadyInvited != null) {
					$('#existing_err_msg').addClass('error');
					$("#existing_err_msg").text("Email already sent to " + obj.alreadyInvited.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (obj.alreadyMember != null) {
					$('#succ_msg').addClass('error');
					$("#succ_msg").text("Invitation already sent to " + obj.alreadyMember.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (invalid_emails.length > 0) {
					$('#err_msg').addClass('error');
					$("#err_msg").text("Unable to send team invitation to " + invalid_emails.join(", "));
					$('#loading-spinner').addClass('hide');
				}

				setTimeout(function () {
					$("#addEventOrganizer").modal("hide");
				}, THREE_SEC);

			});
		}
		else {
			$('#err_msg').addClass('error');
			$("#err_msg").text("Unable to send team invitation to " + invalid_emails.join(", "));
			$('#loading-spinner').addClass('hide');
		}

	});

	$('.clear_free_pricing').click(function (eve) {
		eve.preventDefault();
		$('#free_practice_num').val('');
		$("#compl_pract_cost").val('');
	});

	$('.clear_addn_pricing').click(function (eve) {
		eve.preventDefault();
		$('#additonal_pracs').val('');
		$("#additional_practices_cost").val('');
	});

	$('.clear_coach_pricing').click(function (eve) {
		eve.preventDefault();
		$('#coaching_service').val('');
	});

	$('.clear_fees').click(function (eve) {
		eve.preventDefault();
		//console.log($(this).parent().siblings())
		$(this).parent().siblings().each(function () {
			$(this).children().val('')
		})
	});

	$('#add_faq').click(function (e) {
		e.preventDefault();
		var main_div = $(".faqs").last().clone();
		var main_div_count = $('.faqs').length;

		if (main_div_count > 0) {
			var oldId = main_div_count;
			var id = 1 + oldId;

			main_div.attr('id', 'faq_' + id);
			main_div.find('#faqQ_' + oldId).attr('id', 'faqQ_' + id).val('');
			main_div.find('#faqA_' + oldId).attr('id', 'faqA_' + id).val('');
			main_div.find('#delete_faq_' + oldId).attr('id', 'delete_faq_' + id).val('');
			//$('.faqs').insertAfter(main_div);
			$(main_div).insertAfter($(".faqs").last());
		}
	});


	$(document).on('click', "a.delete-faq", function (e) {
		e.preventDefault();
		var id = $(this).attr('id');
		//console(id)

		if ($('.faqs').length > 1) {
			var idnum = id.replace("delete_faq_", "");
			$("#faq_" + idnum).remove();
		}

	});

	$('#delete_faq_1').click(function (e) {
		e.preventDefault();
		$('#faqQ_1').val('');
		$('#faqA_1').val('')
	});

	/*$("#edit-form-submit").click(function() {
		var isFormValid = true
		
  		if(isFormValid) {
  			//console('In here')
  			$('[name="M_seb_price[]"]').each( function  () {
  				//console($(this).val() + '-' + $(this).attr('id') );
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})
  			
  			$('[name="W_seb_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})

  			$('[name="MX_seb_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})

  			$('[name="M_eb_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})
  			
  			$('[name="W_eb_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})

  			$('[name="MX_eb_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})


  			$('[name="M_r_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})
  			
  			$('[name="W_r_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})

  			$('[name="MX_r_price[]"]').each( function  () {
				$(this).val($(this).val() + '-' + $(this).attr('id') ) ; 
  			})

  			$('#editEventForm').submit();
  		}
	})*/

	var editor = new MediumEditor('.editable_event_desc', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Description'
		}
	});

	if ($('#hidden_desc').length && $('#hidden_desc').val().trim().length > 0) {
		$('.editable.editable_event_desc.form-control.medium-editor-element').focusWithoutScroll();
		//$("html, body").animate({ scrollTop: 0 }, "fast");
		$('.editable.editable_event_desc.form-control.medium-editor-element').html($('#hidden_desc').val())
		$("#event_about").val($('#hidden_desc').val())
	}

	var text = $("#showDescription").text()
	var html = new DOMParser().parseFromString(text, "text/html").body.innerHTML
	html = html.replace("&lt;/", "")
	$("#showDescription").html(html)
	$("#showDescription").removeClass("hide")

	$('.ancmnt-html').each(function () {
		var text = $(this).text()
		var html = new DOMParser().parseFromString(text, "text/html").body.innerHTML
		html = html.replace("&lt;/", "")
		$(this).html(html)
	});

	$(document).on('click', "a.readMoreAnnouncement", function (e) {
		e.preventDefault();
		var id = $(this).attr('value');

		$('#showAnnouncement_' + id).addClass('hide');
		$('#showFullAnnouncement_' + id).removeClass('hide');
		$(this).addClass('hide');
	});

});

// js for team view 
$(function () {

	$('.dashboard-discussions a').attr("target", "_blank");
	$('.team-description a').attr("target", "_blank");

	$('#joinTeam').click(function (eve) {
		eve.preventDefault();
		$("#tj_succ_msgs, #tj_err_msgs").text("");
		$('#tj_err_msgs').removeClass('error');


		$("#joinTeamModal").modal("show");

	});

	$('#join_team').click(function (eve) {

		if ($('#join_team_msg').val().trim().length > 0) {
			eve.preventDefault()
			$.ajax({
				url: "/team-join-request",
				type: "POST",
				data: { "msg": $('#join_team_msg').val().trim() },
				success: function (data) {
					if (data == "true") {
						$('#tj_succ_msgs').addClass('success');
						$("#tj_succ_msgs").text("You request was sent successfully");
					}
					else if (data == "1") {
						$('#tj_err_msgs').addClass('error');
						$("#tj_err_msgs").text("Team invite has already been sent to you by the team captain. Please check your notifications on your dashboard and accept team invite there");
					}
					else if (data == "2") {
						$('#tj_err_msgs').addClass('error');
						$("#tj_err_msgs").text("You are already a member of this team");
					}
					else if (data == "3") {
						$('#tj_err_msgs').addClass('error');
						$("#tj_err_msgs").text("You have rejected team request");
					}
					else if (data == "4") {
						$('#tj_err_msgs').addClass('error');
						$("#tj_err_msgs").text("You have previously sent a request to join this team");
					}
					else {
						$('#tj_err_msgs').addClass('error');
						$("#tj_err_msgs").text("You have previously sent a request to join this team");
					}

					setTimeout(function () {
						$("#joinTeamModal").modal("hide");
					}, THREE_SEC);

				}
			});
		}

	});

	$('#teamDiscussionForm').keyup(function () {
		var msg = $("#teamDiscussionForm").serializeArray()[0].value;
		msg = $(msg).text()  // remove html tags

		if (msg.length > 0) {
			$('#saveDisc').removeAttr("disabled");
			$('#saveDisc').removeClass("disabled_button");
		} else {
			$('#saveDisc').addClass("disabled_button");
			$('#saveDisc').attr("disabled", "disabled");
		}
	});

	$("#teamDiscussionForm").bind("keyup paste", function (e) {
		var finalContent = "";
		if (e.type == "keyup") {
			finalContent = $($("#teamDiscussionForm").serializeArray()[0].value).text(); // remove html tags			
		} else {
			finalContent = $.trim(e.originalEvent.clipboardData.getData('text'));
		}
		if (finalContent.length > 0) {
			$('#saveDisc').removeAttr("disabled");
			$('#saveDisc').removeClass("disabled_button");
		} else {
			$('#saveDisc').addClass("disabled_button");
			$('#saveDisc').attr("disabled", "disabled");
		}
	});

	var isChecked = true;
	$("#instant_sms .iCheck-helper").click(function (e) {
		if (isChecked) {
			$('#instant_sms input').iCheck('check');
			isChecked = false;
			$("#view_type option[value='everyone']").remove();
			$("#view_type").trigger('change');
			// if(teamSubscriptionPeriod == "true" || teamFreeTrialPeriod == "true"){
			// 	checkSmsCount()
			// }
		} else {
			$('#instant_sms input').iCheck('uncheck');
			isChecked = true;
			$("#view_type option").eq(0).after('<option value="everyone">Public</option>');
			$("#view_type").trigger('change');
		}

	});

	function checkSmsCount() {
		$.ajax({
			type: "GET",
			url: "/check-sms-count",
		}).done(function (responseCountData) {
			if (responseCountData != "null") {
				responseCountDataValue = JSON.parse(responseCountData)
				tempresponseCount = parseInt(responseCountDataValue.smscount)
				if (tempresponseCount <= 30 && responseCountDataValue.popupstatus == "false") {
					$('#sms_package_text').text("Currently your team is accumulating 400 text message credits per month on the premium team plan. However, it seems that you're a busy team and you may need more credits to keep up with your communication efforts. Would you like to add 1000 text messages to your team account for $15USD? These credits will be available for one year from date of purchase and there are no refunds.")
					$('#smsPackageSubscriptionModal').modal('show')
				}
			}
		});
	}

	var dis_id = ""

	$('.reply').click(function (e) {
		e.preventDefault()
		/*$('textarea#reply_msg').editable({
	      inlineMode: false,
	      countCharacters: false
	    });*/
		//$("#reply_msg_"+this._id).val('');
		dis_id = $(this).attr('id')
		$("#teamReplyForm_" + $(this).attr('id')).toggleClass('hide');
	});

	/*$('.btn-loading').click( function(e){
		e.preventDefault();
		var div=$('#refresh').html();
		$.ajax({
			type: "POST",
			url: "/load-team-discussion",
			data: {	"limit": 5
			} 
		}).done(function(msg) {
			console.log('success')
			//$("#refresh").load(location.href + " #refresh");
		});

	});*/


	/*$("span[class='comment-timestamp']").each( function() {
		$(this).text(moment($(this).text()).format("ddd MMM D YYYY, h:mm:ss a"))
	});*/

	/*$('#team_members tbody tr td:nth-child(5) p').each( function(){
		if ($(this).text().trim().length > 0 ) {
			$(this).text(moment($(this).text().trim()).format("D MMM YYYY"));
		}
	});*/

	$('#cancel_invite').click(function (e) {
		e.preventDefault()
		team_mem = []
		$('#rem_err_msgs').removeClass('error');
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_err_msgs,#rem_succ_msgs").text('');
		var pending_mem = [];
		$('input:checkbox[name="pending_members[]"]').each(function () {
			if (this.checked) {
				team_mem.push(this.value);
			}
		});
		//console(team_mem)
		if (team_mem.length > 0) {
			$("#confirmDeleteModal").modal("show");
			tempTextHeading = "<p>Do you really want to remove this member invitation?</p>"
			if (team_mem.length >= 2) {
				tempTextHeading = "<p>Do you really want to remove this member's invitation?</p>"
			}
			newhtml = ""
			newhtml += tempTextHeading;
			$("#appendTeamInviteRemove").empty().append(newhtml);
			$("#appendTeamMemberInvite").removeClass('hide')
		}
		else {
			$("#confirmDeleteModal").modal("hide");
		}
	});

	$(document).on('click', '#removeIndividualInvite', function (e) {
		e.preventDefault()
		team_mem = []
		$('#rem_err_msgs').removeClass('error');
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_err_msgs,#rem_succ_msgs").text('');
		var pending_mem = [];
		team_mem.push($(this).attr('value'));
		if (team_mem.length > 0) {
			$("#confirmDeleteModal").modal("show");
			$("#confirmDeleteModal").modal("show");
			tempTextHeading = "<p>Do you really want to remove this member invitation?</p>"
			if (team_mem.length >= 2) {
				tempTextHeading = "<p>Do you really want to remove this member's invitation?</p>"
			}
			newhtml = ""
			newhtml += tempTextHeading;
			$("#appendTeamInviteRemove").empty().append(newhtml);
			$("#appendTeamMemberInvite").removeClass('hide')
		}
		else {
			$("#confirmDeleteModal").modal("hide");
		}
	});

	$('.checkedTeams').click(function () {
		var val = $(this).val()
		console.log("val"+val)
		
			if(this.checked){
				$("#reg_"+this.value).prop('checked', true);
				$("#order_by_name_"+this.value).prop('checked', true);
				$("#order_by_roster_"+this.value).prop('checked', true);
				
			}else {
				$("#reg_"+this.value).prop('checked', false);
				$("#order_by_name_"+this.value).prop('checked', false);
				$("#order_by_roster_"+this.value).prop('checked', false);
			}	
	});

	$('#select_all_members').click(function () {

		if(this.checked){
			$('.checkedTeams').prop('checked', true);
					
		}else {
			$('.checkedTeams').prop('checked', false);
		}
		
	});
	
	$('#send_announcement').click(function () {

		if(this.checked){
			$('#send_announcement').prop('checked', true);
			$('#send_announcement').val("yes");
					
		}else {
			$('#send_announcement').prop('checked', false);
			$('#send_announcement').val("no");
		}
		
	});

	$('#emailTeam').click(function (eve) {
		eve.preventDefault();
		//var val = $(this).val()
		$("#succ_msg").text("");
		$("#succ_msg").removeClass("success");
		$("#err_msg").text("");
		$(".editable_discussion_msg_for_event_team_email").val("");
		$(".editable_discussion_msg_for_event_team_email").text("");
		$('#send_announcement').prop('checked', false);
		$('#send_announcement').val("no");
		$("#err_msg").removeClass("error");
		$("#emailCaptains").css({"background": "#fff","color": "#297560"});
		$("#emailTeamMem").css({"background": "#fff","color": "#297560"});
		$("#sendEmailFromTeamModal").modal("show");
		$("#sendEmailFromTeamModal .modal-body #getTo").val( "" );
		var selectedMemForEmail = [] ;
		$('input:checkbox[name="team_members[]"]').each(function () {
			if(this.checked){
				if (jQuery.inArray(this.value, selectedMemForEmail) == -1) {
					selectedMemForEmail.push(this.value)
				}
			}	
		});
	$("#getSelectedMemForMail").val(selectedMemForEmail)
	var eventRegIds=$("#getSelectedMemForMail").val();
	var eventId=  $("#getEventId").val()
	$("#getEventIdEmail").val(eventId)

	});

	$('#emailTeamMem').click(function (eve) {
		eve.preventDefault();
		var val = $(this).val()
		
		$("#emailTeamMem").css({"background": "#297560","color": "#fff"});
		$("#emailCaptains").css({"background": "#fff","color": "#297560"});
		$("#getTo").val("team members")
	});

	$('#emailCaptains').click(function (eve) {
		eve.preventDefault();
		var val = $(this).val()
		$("#emailTeamMem").css({"background": "#fff","color": "#297560"});
		$("#emailCaptains").css({"background": "#297560","color": "#fff"});
		$("#getTo").val("captains")
	});
	
	$('#sendEmailSubmit').click(function (eve) {
	eve.preventDefault();
	var eventRegIds=$("#getSelectedMemForMail").val();
	var email_to = $("#getTo").val();
	var email_message = ""
	var all_href = $('[name="discussion_msg"]').find('a')
	for (var i = 0; i < all_href.length; i++) {

		old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
		old_href = old_href.replace("https", "")
		old_href = old_href.replace("http", "")
		old_href = old_href.replace(":", "")
		old_href = old_href.replace("//", "")

		new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
		editor_id = $('[name="discussion_msg"]')[0].id
		$("#" + editor_id).html($("#" + editor_id).html().replace(old_href, new_href));

	 $("#discussion_msg").val($("#" + editor_id).html())
	};

	var discussion_msg = $("#discussion_msg").val();
	$("#plain_discussion_msg").val($(discussion_msg).text());

	var sendAnnouncement = $('#send_announcement').val();
	
	 isvalidform =true
	
	// console.log("email_message--",email_message)
		if($('#send_announcement').val() == "no"){
			
			if ($("#getTo").val()=="" ){
				$('#err_msg').addClass('error');
				$('#err_msg').text('Select atleast one option');
				isvalidform =false
			}
			else if($("#discussion_msg").val()==""){
		
				$('#err_msg').addClass('error');
				$('#err_msg').text('Enter Message');
				isvalidform =false
			}
			else if($("#getSelectedMemForMail").val() ==""){
				$('#err_msg').addClass('error');
				isvalidform =false
				$('#err_msg').text('Select Team Names');
			}
			
		}else{
		if($("#discussion_msg").val()==""){
		
			$('#err_msg').addClass('error');
			$('#err_msg').text('Enter Message');
			isvalidform =false
		}
	}

	if(isvalidform) {
		$("#succ_msg").text("");
			$("#succ_msg").removeClass("success");
			$('#err_msg').removeClass('error');
			$('#err_msg').text('');

			var eventRegIds=$("#getSelectedMemForMail").val();
			var email_to=$("#getTo").val();
			var email_message=$("#plain_discussion_msg").val();
			var sendAnnouncement=$("#send_announcement").val();
			var eventId=$("#getEventIdEmail").val();

			//console.log($("#getEventIdEmail").val(), $("#getSelectedMemForMail").val(), $("#getTo").val(), $('#send_announcement').val())
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/send-event-team-email",
				data: $("#sendEventTeamEmailForm").serialize()
				
			}).done(function (msg) {

				console.log(msg)
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$('#err_msg').removeClass('error');
					$('#err_msg').text('');
					
					if( eventRegIds != "" && email_to != "" && email_message!="" && sendAnnouncement == "no" && eventId != ""){
						$("#succ_msg").text("Email sent sucessfully to your recipient");
						$("#succ_msg").addClass("success");
					}
					else if( eventRegIds != "" && email_to != "" && email_message!="" && sendAnnouncement == "yes" && eventId != ""){
						$("#succ_msg").text("Information got posted sucessfully");
						$("#succ_msg").addClass("success");
					}
					else if( eventRegIds != "" && email_to == "" && email_message !="" && sendAnnouncement == "yes" && eventId != ""){
						$("#succ_msg").text("Announcement posted to event sucessfully");
						$("#succ_msg").addClass("success");
					}
				
					setTimeout(function () {
						$("#sendEmailFromTeamModal").modal("hide");
						location.reload();
					}, FIVE_SEC);
				}
				else if (msg == "false"){
					$('#loading-spinner').addClass('hide');
					$("#succ_msg").text("");
					$("#succ_msg").removeClass("success");
					if( eventRegIds != "" && email_to != "" && email_message!="" && sendAnnouncement == "no" && eventId != ""){
						$("#err_msg").text("Email not sent to your recipient");
						$("#err_msg").addClass("error");
					}
					else if( eventRegIds != "" && email_to != "" && email_message!="" && sendAnnouncement == "yes" && eventId != ""){
						$("#err_msg").text("Information not got posted ");
						$("#err_msg").addClass("error");
					}
					else if( eventRegIds != "" && email_to == "" && email_message !="" && sendAnnouncement == "yes" && eventId != ""){
						$("#err_msg").text("Announcement not posted to event");
						$("#err_msg").addClass("error");
					}
					setTimeout(function () {
						$("#sendEmailFromTeamModal").modal("hide");
						location.reload();
					}, FIVE_SEC);

				}
				else{
					$('#loading-spinner').addClass('hide');
					$("#succ_msg").text("");
					$("#succ_msg").removeClass("success");
					$("#err_msg").text(msg);
					$("#err_msg").addClass("error");
				}
			});
	}
});
	$('#select_all_roster_members').click(function () {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="team_roster_members[]"]').each(function () {
				$(this).prop('checked', true);
			});
		}
		else {
			$('input:checkbox[name="team_roster_members[]"]').each(function () {
				$(this).prop('checked', false);
			});
		}
	});

	$('#select_all_members_alpha').click(function () {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="team_members[]"]').each(function () {
				$(this).prop('checked', true);
			});
		}
		else {
			$('input:checkbox[name="team_members[]"]').each(function () {
				$(this).prop('checked', false);
			});
		}
	});

	$('#sendInvite').click(function (eve) {
		eve.preventDefault()
		$('#member_email').val('');
		$('#invite_team_msg').val('');
		$('#succ_msg').removeClass('success');
		$('#err_msg, #new_err_msg, #team_err_msg').removeClass('error');
		$("#succ_msg").text("");
		$("#err_msg, #new_err_msg, #team_err_msg").text("");
		$('#sendInviteSubmit').removeClass("disabled_button");
		$('#sendInviteSubmit').removeAttr("disabled");
		$("#sendInviteModal").modal("show");
	});

	$('#sendInviteSubmit').click(function () {
		$('#loading-spinner').removeClass('hide');
		$('#succ_msg').removeClass('success');
		$('#err_msg, #new_err_msg').removeClass('error');
		$("#succ_msg, #err_msg, #new_err_msg").text("");

		var emails = $('#member_email').val();
		var email_array = emails.split(/[ ,]+/)
		var invalid_emails = []

		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		for (var i = 0; i < email_array.length; i++) {
			email_array[i] = email_array[i].trim()
			var regex_email = email_regex.test(email_array[i]);
			if (!regex_email) {
				invalid_emails.push(email_array[i])
			}
		};

		valid_emails = email_array.filter(function (val) {
			return invalid_emails.indexOf(val) == -1;
		});

		//console.log(valid_emails)

		if (valid_emails.length > 0) {
			var currentTeamId = $('#current_teamid').val()
			$.ajax({
				type: "POST",
				url: "/team-member-invite",
				data: {
					"email": valid_emails,
					"message": $("#invite_team_msg").val(),
					"teamId": currentTeamId
				}
			}).done(function (msg) {
				var obj = $.parseJSON(msg)
				//console(obj)
				if (obj.sent != null) {
					$('#err_msg').removeClass('error');
					$("#err_msg").text("");
					$('#succ_msg').addClass('success');
					$("#succ_msg").text("Team invite sent to " + obj.sent.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (obj.alreadyInvited != null) {
					$('#err_msg').addClass('error');
					$("#err_msg").text("Team invite already sent to " + obj.alreadyInvited.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (obj.alreadyMember != null) {
					$('#team_err_msg').addClass('error');
					$("#team_err_msg").text("Team members already include " + obj.alreadyMember.join(", "));
					$('#loading-spinner').addClass('hide');
				}
				if (invalid_emails.length > 0) {
					$('#err_msg').addClass('error');
					$("#err_msg").text("Unable to send team invitation to " + invalid_emails.join(", "));
					$('#loading-spinner').addClass('hide');
				}

				setTimeout(function () {
					$("#sendInviteModal").modal("hide");
					sessionStorage.setItem("activeTab", "#members");
					location.reload();
				}, ONE_SEC);

			});
		}
		else {
			$('#err_msg').addClass('error');
			$("#err_msg").text("Enter email address");
			$('#loading-spinner').addClass('hide');
		}

	});

	$('#select_all_pending').click(function () {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="pending_members[]"]').each(function () {
				$(this).prop('checked', true);
			});
		}
		else {
			$('input:checkbox[name="pending_members[]"]').each(function () {
				$(this).prop('checked', false);
			});
		}
	});

	$('#select_all_pending_alpha').click(function () {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="pending_members[]"]').each(function () {
				$(this).prop('checked', true);
			});
		}
		else {
			$('input:checkbox[name="pending_members[]"]').each(function () {
				$(this).prop('checked', false);
			});
		}
	});

	$('#resendInvite').click(function (eve) {
		eve.preventDefault();
		team_mem = [];
		$('#resend_succ_msg').removeClass('success');
		$('#resend_succ_msg').text('');
		$('#resendInviteForm #reinvite_team_msg').val('');
		$('input:checkbox[name="pending_members[]"]').each(function () {
			if (this.checked) {
				team_mem.push(this.value);
			}
		});
		if (team_mem.length > 0) {
			$.ajax({
				url: "/get-pending-email-id",
				type: "POST",
				data: { "ids": team_mem },
				success: function (data) {
					var obj = $.parseJSON(data)
					//console(obj)
					if (obj.pending_emails.length > 0) {
						$('.resend-invite-name').text(obj.pending_emails.join(", "))
						$("#resendInviteModal").modal("show");

					}
				}
			});

		}
	});

	$('#messageTeam').click(function (eve) {
		eve.preventDefault();
		team_mem = [];
		$("#teamsMessage").val('')
		$('#msg_succ_msgs').removeClass('success');
		$('#msg_err_msgs').removeClass('error');
		$("#msg_succ_msgs, #msg_err_msgs").text("");
		$("#team_msg_succ_msgs, #team_msg_err_msgs").text("");

		$('input:checkbox[name="team_members[]"]').each(function () {
			if (this.checked) {
				team_mem.push(this.value);
			}
		});
		if (team_mem.length > 0) {
			$.ajax({
				url: "/get-user-names",
				type: "POST",
				data: {
					"ids": team_mem
				},
				success: function (data) {
					var obj = $.parseJSON(data)
					if (obj.length > 0) {
						$('.resend-invite-name').text(obj.join(", "));
						//$('#toEmailIds').val(obj.data.join(", "));
						$('#toMembesId').val(team_mem);
						$("#messageToTeamsModal").modal("show");
						if ($("#teamCaptainId").val() == $("#teamUserId").val()) {
							$("#teamEmailTeams").text("You will be cc'd on this email.");
						} else {
							$("#teamEmailTeams").text("You and the captain will be cc'd on this email.");
						}
					}
				}
			});
		} else {
			$('#defaultteammodelwithheading').modal("show")
			$('#defaultteammodelwithheadingText').text("Gushou Service Msg")
			$('#defaultteammodelwithheading_Body').text("Please select one or more members, from the left hand side of your team list, in order to send your message via email.")
		}
	});

	$(document).on('click', '#resendIndividualInvite', function (eve) {
		eve.preventDefault();
		team_mem = [];
		$('#resend_succ_msg').removeClass('success');
		$('#resend_succ_msg').text('');
		$('#resendInviteForm #reinvite_team_msg').val('');
		team_mem.push($(this).attr('value'));
		if (team_mem.length > 0) {
			$.ajax({
				url: "/get-pending-email-id",
				type: "POST",
				data: { "ids": team_mem },
				success: function (data) {
					var obj = $.parseJSON(data)
					if (obj.pending_emails.length > 0) {
						$('.resend-invite-name').text(obj.pending_emails.join(", "))
						$("#resendInviteModal").modal("show");
					}
				}
			});
		}
	});

	$('#resend_invite').click(function () {
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			url: "/resend-team-invite",
			type: "POST",
			data: { "ids": team_mem, "msg": $('#resendInviteForm #reinvite_team_msg').val().trim(), "teamid" : $('#current_teamid').val().trim() },
			success: function (data) {
				if (data == "true") {
					$('#loading-spinner').addClass('hide');
					$('#resend_succ_msg').addClass('success');
					$("#resend_succ_msg").text("New reminder sent");
					setTimeout(function () {
						$("#resendInviteModal").modal("hide");
						$('input:checkbox[name="pending_members[]"]').each(function () {
							$(this).prop('checked', false);
						});

						if ($('#select_all_pending').length) {
							$('#select_all_pending').prop('checked', false)
						}
						if ($('#select_all_pending_alpha').length) {
							$('#select_all_pending_alpha').prop('checked', false)
						}
						sessionStorage.setItem("activeTab", "#members");
						location.reload();
					}, FIVE_SEC);
				}
				else {
					$('#resend_err_msg').addClass('error');
					$("#resend_err_msg").text("Couldn't send reminder");
				}
			}
		});

	});

	$(document).on('click', '.msgMembers', function (eve) {
		eve.preventDefault()
		$('#teamMessage').val('');
		$('#msg_succ_msgs').removeClass('success');
		$('#msg_err_msgs').removeClass('error');
		$("#msg_succ_msgs, #msg_err_msgs").text("");
		$("#team_msg_succ_msgs, #team_msg_err_msgs").text("");
		//$("#messageTeamModal").modal("show");

		var member_id = this.id;
		////console(member_id)

		if (member_id) {
			//Get the person's name 
			$.ajax({
				url: "/get-user-name",
				type: "POST",
				data: { "user_id": member_id },
				success: function (data) {
					$('.resend-invite-name').text(data);
					$('#to_member').val(member_id);
					$("#messageTeamModal").modal("show");
					if ($("#teamCaptainId").val() == member_id) {
						$("#teamEmail").text("You will be cc'd on this email.");
					} else {
						$("#teamEmail").text("You and the captain will be cc'd on this email.");
					}

				}
			});

		}
	});

	$('#send_message').click(function () {
		if ($("#teamMessage").val().trim().length > 0) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				url: "/send-email-message",
				type: "POST",
				data: { "member_id": $('#to_member').val(), "msg": $("#teamMessage").val().trim() },
				success: function (data) {
					if (data == "true") {
						$('#loading-spinner').addClass('hide');
						$('#msg_succ_msgs').addClass('success');
						$("#msg_succ_msgs").text("You have successfully sent your message ");
						setTimeout(function () {
							$("#messageTeamModal").modal("hide");
						}, TWO_SEC);
					}
					else {
						$('#loading-spinner').addClass('hide');
						$('#msg_succ_msgs').addClass('error');
						$("#msg_succ_msgs").text("Couldn't send your message");
					}
				}
			});
		}
	});


	$('#send_message_to_team').click(function () {
		if ($("#teamsMessage").val().trim().length > 0) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				url: "/send-message-to-team",
				type: "POST",
				data: { "member_id": $('#toMembesId').val(), "toEmailIds": $('#toEmailIds').val(), "msg": $("#teamsMessage").val().trim() },
				success: function (data) {
					if (data == "true") {
						$('#loading-spinner').addClass('hide');
						$('#team_msg_succ_msgs').addClass('success');
						$("#team_msg_succ_msgs").text("You have successfully sent your message ");
						$('input:checkbox[name="team_members[]"]').each(function () {
							$(this).prop('checked', false);
						});
						$('#select_all_members').prop('checked', false);
						setTimeout(function () {
							$("#messageToTeamsModal").modal("hide");
						}, TWO_SEC);
					}
					else {
						$('#loading-spinner').addClass('hide');
						$('#team_msg_succ_msgs').addClass('error');
						$("#team_msg_succ_msgs").text("Couldn't send your message");
					}
				}
			});
		}
	});

	$('#saveDisc').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		smsSendwithDissucOrReminder = ""
		if ($("#instant_sms input[type='radio']").is(":checked")) {
			$.ajax({
				type: "GET",
				url: "/check-team-sms-limit",
				data: ""
			}).done(function (msg) {

				$('#loading-spinner').addClass('hide');
				var tempmsg = JSON.stringify(msg)
				var tSMSLimitObj = $.parseJSON(tempmsg);
				var sms_limit = parseInt(tSMSLimitObj.sms_limits);
				var free_trial_status = tSMSLimitObj.free_trial_status;
				var premium_Subscription = tSMSLimitObj.premium_subscription;
				var tempresponseCount = parseInt($('#activeTeamMemberCount').val())
				if (free_trial_status == "true" && !superAdminStatus) {

					if (sms_limit < tempresponseCount) {
						$('#teamsubscriptionmodel').modal('show')
						subscriptionPopup = false
						$('#teammodelheaderParttext').text("Gushou Service Msg")
						// $('#appendHeaderText').html("<p style='text-align: justify;'>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p>")
						$('#teamsubscriptiocheckboxhide').addClass('hide')

						if (!teamCaptainStatus) {
							$('#appendHeaderText').html("<p style='text-align: justify;'>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p><p>Please contact team captain.</p>")
							$('#activeTeamCaptainhideshow').addClass('hide')
							$('#activeBasicUserhideshow').removeClass('hide')
						} else {
							$('#appendHeaderText').html("<p style='text-align: justify;'>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p>")
							$('#activeTeamCaptainhideshow').removeClass('hide')
							$('#activeBasicUserhideshow').addClass('hide')
						}
					} else if (sms_limit >= tempresponseCount) {
						$("#errorMsgSmsLimit").empty();
						$("#errorMsgSmsLimit").append("<p>Your team account has " + sms_limit + " SMS msgs remaining.</p>");
						$("#confirmModalForSMSLimit").modal("show");
					}
				} else if (premium_Subscription == "true" && !superAdminStatus) {
					if (sms_limit < tempresponseCount) {
						if (teamCaptainStatus) {
							if (smsNextCreditStartingDate == "") {
								$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p>");
							} else {
								$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits that you can use to start sending text messages again.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p>");
							}
							$('.changeSmsIdValue').attr("id", "submitTeamDiscussion");
							smsSendwithDissucOrReminder = "saveDisc"
							$('#smspackageactivebasicuserhideshow').addClass('hide')
							$('#smspackageactiveteamcaptainhideshow').removeClass('hide')
						} else {
							if (smsNextCreditStartingDate == "") {
								$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p><p>Please contact team captain.</p>");
							} else {
								$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits that you can use to start sending text messages again.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p> <p>Please contact team captain.</p>");
							}
							$('#smspackageactiveteamcaptainhideshow').addClass('hide')
							$('#smspackageactivebasicuserhideshow').removeClass('hide')
						}
						$('#smsPackageSubscriptionModal').modal('show')
					} else if (sms_limit >= tempresponseCount) {
						$("#errorMsgSmsLimit").empty();
						if (smsNextCreditStartingDate == "") {
							$("#errorMsgSmsLimit").append("<p>Your team account has " + sms_limit + " SMS msgs remaining.</p>");
						} else {
							$("#errorMsgSmsLimit").append("<p>Your team account has " + sms_limit + " SMS msgs remaining. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits.</p>");
						}
						$("#confirmModalForSMSLimit").modal("show");
					}
				} else if (free_trial_status == "false" && premium_Subscription == "false" && !superAdminStatus) {
					$('#subcribeteammodelheaderParttext').text("Gushou Service Msg")
					$('#subscriptionteammodel').modal("show")
					freetrialPopup = false
					$('#clickfreeTrialPopuphide').addClass('hide');
				} else if (superAdminStatus) {
					$("#errorMsgSmsLimit").empty();
					if (sms_limit <= 0) {
						$("#errorMsgSmsLimit").append("<p>Unfortunately your team does not have enough SMS messages to send this out, upgrade to premium to continue</p>" +
							"<p> or wait until the end of the month for a reset of your SMS quota</p>")
					} else {
						$("#errorMsgSmsLimit").append("<p>Your team account has " + sms_limit + " SMS msgs remaining.</p>");
					}
					$("#confirmModalForSMSLimit").modal("show");
				}
			});
		} else {
			all_href = $('[name="discussion_msg"]').find('a')
			for (var i = 0; i < all_href.length; i++) {
				old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
				old_href = old_href.replace("https", "")
				old_href = old_href.replace("http", "")
				old_href = old_href.replace(":", "")
				old_href = old_href.replace("//", "")

				new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
				editor_id = $('[name="discussion_msg"]')[0].id
				$("#" + editor_id).html($("#" + editor_id).html().replace(old_href, new_href));

				$("#discussion_msg").val($("#" + editor_id).html())
			};

			$.ajax({
				type: "POST",
				url: "/add-team-discussion",
				data: $("#teamDiscussionForm").serialize()
			}).done(function (msg) {
				var comObj = $.parseJSON(msg);
				if (comObj.status == "true") {
					reloadAsGet()
				}
				else {
					$('#loading-spinner').addClass('hide');
				}

			});


		}

	});

	$(document).on('click', '#submitTeamDiscussion', function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');

		all_href = $('[name="discussion_msg"]').find('a')
		for (var i = 0; i < all_href.length; i++) {

			old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
			old_href = old_href.replace("https", "")
			old_href = old_href.replace("http", "")
			old_href = old_href.replace(":", "")
			old_href = old_href.replace("//", "")

			new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
			editor_id = $('[name="discussion_msg"]')[0].id
			$("#" + editor_id).html($("#" + editor_id).html().replace(old_href, new_href));

			$("#discussion_msg").val($("#" + editor_id).html())
		};

		var discussion_msg = $("#discussion_msg").val();
		$("#plain_discussion_msg").val($(discussion_msg).text());

		$.ajax({
			type: "POST",
			url: "/add-team-discussion",
			data: $("#teamDiscussionForm").serialize()
		}).done(function (msg) {
			var comObj = $.parseJSON(msg);
			if (comObj.status == "true") {
				var smsResponse = comObj.sms_ersponse_status;
				var smsSentCount = 0;
				var smsFailedMembers = "";
				for (var index in smsResponse) {
					var smsObj = smsResponse[index];
					if (smsObj.sms_status == "true") {
						smsSentCount++;
					} else {
						smsFailedMembers += smsObj.display_name + ", " + smsObj.email_address + " - " + smsObj.user_sms_error + "&"
					}
				}
				smsFailedMembers = smsFailedMembers.substr(smsFailedMembers, smsFailedMembers.length - 1)
				sessionStorage.setItem('smsSentCount', smsSentCount);
				sessionStorage.setItem('smsFailedMembers', smsFailedMembers);
				sessionStorage.setItem('isTeamDiscussionPostedForSMS', true);
				reloadAsGet()
			}
			else {
				$('#loading-spinner').addClass('hide');
			}

		});
	});

	if (sessionStorage.getItem('isTeamDiscussionPostedForSMS')) {
		$("#smsResponseMsg").empty();
		var smsSentCount = sessionStorage.getItem('smsSentCount');
		if (parseInt(smsSentCount) == 1) {
			$("#smsResponseMsg").append("<p>Text message has been sent to  ‘" + smsSentCount + "’ member.</p>");
		} else if (parseInt(smsSentCount) > 1) {
			$("#smsResponseMsg").append("<p>Text message has been sent to  ‘" + smsSentCount + "’ members.</p>");
		}
		var smsFailedMembers = sessionStorage.getItem('smsFailedMembers');
		if (smsFailedMembers != "") {
			$("#smsResponseMsg").append("<p>not sent to members below: ");
			var failedMembers = smsFailedMembers.split("&");
			for (var i in failedMembers) {
				$("#smsResponseMsg").append("<p>" + failedMembers[i] + "</p>");
			}
		}
		sessionStorage.clear();
		$("#postTeamDiscussion").modal("show");
	}

	$('.replySubmit').click(function (e) {
		e.preventDefault();
		all_href = $('[name="dis_reply_msg"]').find('a')
		for (var i = 0; i < all_href.length; i++) {
			//console($(all_href[i].outerHTML).attr("href"))
			old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
			old_href = old_href.replace("https", "")
			old_href = old_href.replace("http", "")
			old_href = old_href.replace(":", "")
			old_href = old_href.replace("//", "")

			new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
			//console(old_href, new_href)

			$("#reply_msg_" + dis_id).val($("#reply_msg_" + dis_id).val().replace(old_href, new_href))
		};


		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "POST",
			url: "/add-team-discussion-reply",
			data: $("#teamDiscussionReplyForm_" + dis_id).serialize()
		}).done(function (msg) {
			//console(msg)
			if (msg == "true") {
				//console("Success")
				//location.reload()
				reloadAsGet()
			}
			else {
				//console("Error")
			}

		});
	});

	$("#loadMore button[type='submit']").click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');

		var slash = window.location.pathname.substr(window.location.pathname.length - 1)
		if (slash == "/") {
			history.pushState("data", "Team View | Gushou", window.location.protocol + '//' + window.location.host + window.location.pathname + "#add-discussion")

		} else {
			history.pushState("data", "Team View | Gushou", window.location.protocol + '//' + window.location.host + window.location.pathname + "/#add-discussion")

		}

		$("#loadMore").submit()

	});

	$("#loadMoreReplies button[type='submit']").click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');

		var slash = window.location.pathname.substr(window.location.pathname.length - 1)
		if (slash == "/") {
			history.pushState("data", "Team View | Gushou", window.location.protocol + '//' + window.location.host + window.location.pathname + "#add-discussion")

		} else {
			history.pushState("data", "Team View | Gushou", window.location.protocol + '//' + window.location.host + window.location.pathname + "/#add-discussion")

		}

		$("#loadMoreReplies").submit()

	});

	$(document).on('click', '.add_cocaptain_role', function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var co_captain_id = $(this).attr('name');
		//console(co_captain_id)
		$.ajax({
			type: "POST",
			url: "/make-team-co-captain",
			data: { "co_captain_id": co_captain_id }
		}).done(function (msg) {
			//console(msg)
			if (msg == "true") {
				//console("Success")
				sessionStorage.setItem("activeTab", "#members");
				location.reload()
			}
			else {
				//console("Error")
			}

		});
	});

	$(document).on('click', '.remove_cocaptain', function (e) {
		$('#defaultConfirmationModal').modal('show')
		$('#defaultConfirmationTitle').text('Remove co-captain status')
		$('#defaultConfirmationHeading').text('Do you wish to remove co-captain status and permissions of this user?')
		$('#defaultConfirmation_click').removeClass().addClass('btn btn-primary btn-fullwidth remove_cocaptain_role')
		$('#defaultConfirmation_click').attr('name', $(this).attr('id'));
	});

	$(document).on('click', '.co_captain', function (e) {
		$('#defaultConfirmationModal').modal('show')
		$('#defaultConfirmationTitle').text('Add Co-Captain')
		$('#defaultConfirmationHeading').html('<p>Do you wish to make this user a co-captain?</p><p>They will have the same permissions as the captain other than the option to subscribe to or cancel the team plan.</p> ')
		$('#defaultConfirmation_click').removeClass().addClass('btn btn-primary btn-fullwidth add_cocaptain_role')
		$('#defaultConfirmation_click').attr('name', $(this).attr('id'));
	});

	$(document).on('click', '.remove_cocaptain_role', function (e) {
		e.preventDefault();
		$('#defaultConfirmationModal').modal('hide')
		$('#loading-spinner').removeClass('hide');
		var co_captain_id = $(this).attr('name');
		var rUrl = window.location.href;
		$.ajax({
			type: "POST",
			url: "/remove-team-co-captain",
			data: { "co_captain_id": co_captain_id }
		}).done(function (msg) {
			//console(msg)
			if (msg == "true") {
				sessionStorage.setItem("activeTab", "#members");
				if (window.location.href.split("/").length > 4) {
					window.location = window.location.origin + "/team-view/" + window.location.href.split("/")[4];
				} else {
					window.location = window.location.origin;
				}
				location.reload()
				$('#loading-spinner').addClass('hide');
			}
			else {
				$("#makeCocaptainModal").modal("show");
				$('#loading-spinner').addClass('hide');
				//console("Error")
			}

		})
	});

	$("#searchByUName").on("keyup", function () {
		var value = $(this).val();
		console.log($("#team_members").length)
		if ($("#team_members").length) {
			var hideCount = 0
			var showCount = 0
			$("#team_members tr").each(function (index) {
				if (index !== 0) {

					$row = $(this);

					var name = $row.find(".list-title").text().toLowerCase().trim();

					if (name.indexOf(value.toLowerCase().trim()) == -1) {
						$row.hide();
						hideCount = hideCount + 1
					}
					else {
						$row.show();
						showCount = showCount + 1
					}
				}
			});
			if (showCount == 0) {
				$("#memberusernotfount").show()
				$("#team_members").addClass('hide')
			} else {

				$("#memberusernotfount").hide()
				$("#team_members").removeClass('hide')
			}

		}
		else {
			$("#team_members_alpha tr").each(function (index) {
				if (index !== 0) {

					$row = $(this);

					var name = $row.find(".list-title").text().toLowerCase().trim();

					if (name.indexOf(value) == -1) {
						$row.hide();
					}
					else {
						$row.show();
					}
				}
			});
		}


	});

	$("#searchNotes").on("keyup", function () {
		var value = $(this).val();

		$("#team_notes tr").each(function (index) {

			$row = $(this);

			var name = $row.find(".download").text().toLowerCase().trim();

			if (name.indexOf(value) == -1) {
				$row.hide();
			}
			else {
				$row.show();
			}

		});

	});

	$('#readMoreDescription').click(function (e) {
		e.preventDefault();

	});

	$(document).on('click', "a.readMoreMsg", function (e) {
		e.preventDefault();
		var dis_id = $(this).attr('value');

		$('#showMsg_' + dis_id).addClass('hide');
		$('#showFullMsg_' + dis_id).removeClass('hide');
		$(this).addClass('hide');
	});

	$(document).on('click', "a.readMoreReplies", function (e) {
		e.preventDefault();
		var reply_id = $(this).attr('value');

		$('#showReply_' + reply_id).addClass('hide');
		$('#showFullReply_' + reply_id).removeClass('hide');
		$(this).addClass('hide');
	});

	var editor = new MediumEditor('.editable_discussion_msg', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
	        /* This example includes the default options for paste,
	           if nothing is passed this is what it used */
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Discussion'
		}
	});
	

	var editor = new MediumEditor('.editable_reply', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Reply'
		}
	});

	var editor = new MediumEditor('.editable_discussion_msg_for_event_team_email', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
	        /* This example includes the default options for paste,
	           if nothing is passed this is what it used */
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Enter your message here...'
		},
			elementsContainer: document.getElementById('sendEmailFromTeamModal') 
	});

	$('.msg-html').each(function () {
		var text = $(this).text()
		var html = new DOMParser().parseFromString(text, "text/html").body.innerHTML
		html = html.replace("&lt;/", "")
		$(this).html(html)
	});

});

// js for team schedule
$(function () {

	var editor = new MediumEditor('.practice_note', {
		elementsContainer: document.getElementById('addPracticeModal'),
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Additional details'
		}
	});

	var editor = new MediumEditor('.editable_practice_note', {
		elementsContainer: document.getElementById('editPracticeModal'),
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Additional details'
		}
	});

	$('#practice_time').datetimepicker({
		format: 'LT',
		useCurrent: false,
		//minDate: new Date($.now())
	});

	$('#edit_practice_time').datetimepicker({
		format: 'LT',
		useCurrent: false,
		//minDate: new Date($.now())
	});

	$('#edit_practice_date').datepicker({
		autoclose: true,
		minDate: new Date()
	});

	$('#add_practice').click(function (eve, temp) {
		eve.preventDefault()
		$("#addPracticeModal").modal("show");
		$('#practice_date').val('');
		$('#practice_end_date').val('');
		$('#practice_time').val('');
		$('#practice_location').val('');
		$('#practice_note').val('');
		$('#practiceDateTimeZone').val('');
		$('#practiceEndDateTimeZone').val('');
		$('#practice_repeats').siblings("span").eq(0).css("width", "150px");
		$("#practiceForm input.required").each(function () {
			$(this).removeClass("empty-err");
		});
		$("input:radio[name=scheduleTypeData]").val(["Practice"]);
		practiceChecked()
	});

	$('#practice_time').focus(function (e) {
		if ($('#practice_time').val().length == 0) {
			var d = new Date();
			$('#practice_time').data("DateTimePicker").date(new Date(d.setHours(12, 0, 0)));
		}

	});

	$("input[type=radio][name=scheduleTypeData]").change(function () {
		if (this.value == 'Practice') {
			practiceChecked()
		} else if (this.value == 'Activity') {
			activityChecked()
		}
	});

	function practiceChecked() {
		$('#practice_end_date').val('');
		$("#practice_end_date-hide").addClass('hide')
		$("#practice_repeats-hide").removeClass('hide')
		$("#practice_repeats").attr("disabled", false);
		$("#enddateerror").text("")
		$("#enddateerror").addClass("hide")
		$("#startdateerror").text("")
		$("#startdateerror").addClass("hide")
		$("#practise_submit").text("Add practice")
		$("#practice_location").removeClass("empty-err");
		$('#practice_date').removeClass("empty-err");
		$('#practice_end_date').removeClass("empty-err");
		$('#practice_time').removeClass("empty-err");
		$("#practice_single_day_event").addClass("hide")
		$('#practice_length').find('#removefullday').remove().end().find('#removefulldays').remove().end();
		$('#practice_date').datepicker({
			minDate: new Date(),
			autoclose: true,
		});

		$('#practice_end_date').datepicker({
			minDate: new Date(),
			autoclose: true,
		});

	}

	function activityChecked() {
		$('#practice_date').val('');
		$('#practice_repeats').val(" ");
		$("#practice_end_date-hide").removeClass('hide')
		$("#practice_repeats").attr("disabled", true);
		$("#practice_repeats-hide").addClass('hide')
		$("#practise_submit").text("Add activity")
		$("#startdateerror").text("")
		$("#startdateerror").addClass("hide")
		$("#enddateerror").text("")
		$("#enddateerror").addClass("hide")
		$("#practice_location").removeClass("empty-err");
		$('#practice_date').removeClass("empty-err");
		$('#practice_end_date').removeClass("empty-err");
		$('#practice_time').removeClass("empty-err");
		$("#practice_single_day_event").removeClass("hide")
		$('#practice_length').find('#removefullday').remove().end().append($("<option id='removefulldays'></option>").attr("value", "1440").text("full days"));

		$('#practice_date').datepicker({
			minDate: new Date(),
			autoclose: true,
		});

		$('#practice_end_date').datepicker({
			minDate: new Date(),
			autoclose: true,
		});

	}

	$('#singleDayPractice').change(function () {
		$('#practice_end_date').removeClass("empty-err");
		if ($('#singleDayPractice').is(":checked")) {
			$('#practice_end_date').val('');
			$('#practice_end_date').attr("disabled", "disabled");
			$('#practice_length').find('#removefulldays').remove().end().append($("<option id='removefullday'></option>").attr("value", "1440").text("full day"));
		} else {
			$('#practice_end_date').removeAttr('disabled');
			$('#practice_length').find('#removefullday').remove().end().append($("<option id='removefulldays'></option>").attr("value", "1440").text("full days"));
		}
	});

	$('#practiceCalendar').click(function () {
		$('#practice_date').datepicker({
			minDate: new Date()
		}).focus();
	});

	$('#edit_practiceCalendar').click(function () {
		$('#edit_practice_date').datepicker({
			minDate: new Date()
		}).focus();
	});

	$(document).on('change', '#practice_date', function () {
		var selectedDate = new Date(this.value);
		var msecsInADay = 86400000;
		var endDate = new Date(selectedDate.getTime() + msecsInADay);
		$("#practice_end_date").datepicker("option", "minDate", endDate);
	});

	$('#practice_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#edit_practice_end_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#edit_practice_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#practiceendCalendar').click(function () {
		$('#practice_end_date').datepicker({
			autoclose: true,
			minDate: new Date($("#practice_date").val())
		}).focus();

	});

	$(document).on('change', '#edit_practice_date', function () {
		var selectedDate = new Date(this.value);
		var msecsInADay = 86400000;
		var endDate = new Date(selectedDate.getTime() + msecsInADay);
		$("#edit_practice_end_date").datepicker("option", "minDate", endDate);
	});

	$('#edit_practiceendCalendar').click(function () {
		$('#edit_practice_end_date').datepicker({
			autoclose: true,
			minDate: new Date($("#edit_practice_date").val())
		}).focus();
	});

	$('#practice_end_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#team-practice-date').click(function () {
		$('#practice_time').focus();
	});

	$('#practise_submit').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');
		$("#enddateerror").text("")
		$("#enddateerror").addClass("hide")
		$("#startdateerror").text("")
		$("#startdateerror").addClass("hide")
		timeZone = moment.tz.guess()
		var isFormValid = true;
		var isFormValidActivity = true;
		var practiceStartDateValid = true
		$("#practiceForm input.required").each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
				$('#loading-spinner').addClass('hide');
				$("#addPracticeModal").animate({ scrollTop: 0 }, "slow");
				/*button.prop('disabled', false);*/
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		var scheduleTypeData = $("input:radio[name=scheduleTypeData]:checked").val()
		if (scheduleTypeData == "Activity" && !$('#singleDayPractice').is(':checked')) {
			if ($.trim($('#practice_end_date').val()).length == 0 && !$('#practice_end_date').is(':disabled')) {
				$('#practice_end_date').addClass("empty-err");
				isFormValidActivity = false;
				$('#loading-spinner').addClass('hide');
				$("#addPracticeModal").animate({ scrollTop: 0 }, "slow");
			}
			else {
				$('#practice_end_date').removeClass("empty-err");
				isFormValidActivity = true;
			}
		}

		if (timeZone != "") {
			$('#locationTimeZone').val(timeZone)
			practiceTime = $('#practice_time').val();
			practice_end_date = $('#practice_end_date').val();
			practice_Date = $('#practice_date').val();

			if (practiceTime != "") {
				practiceTime = hourConverstion(practiceTime);
				var dateTime = new Date(practice_Date + " " + practiceTime);
				dateTime = moment(dateTime).format("YYYY-MM-DDTHH:mm:ssZ");
				$('#practiceDateTimeZone').val(dateTime);
			}

			if (moment().format() > dateTime) {
				$('#practice_Date').addClass("empty-err");
				practiceStartDateValid = false;
				$('#loading-spinner').addClass('hide');
				$("#addPracticeModal").animate({ scrollTop: 0 }, "slow");
				$("#startdateerror").text("Date and time must be in the future")
				$("#startdateerror").removeClass("hide")
			}


			if (practice_end_date && practice_end_date != "" && practiceTime != "") {
				var dateTime1 = new Date(practice_end_date + " " + practiceTime);
				dateTime1 = moment(dateTime1).format("YYYY-MM-DDTHH:mm:ssZ");
				$('#practiceEndDateTimeZone').val(dateTime1);

				if (dateTime1 < dateTime || moment().format() > dateTime1) {
					$('#practice_end_date').addClass("empty-err");
					isFormValidActivity = false;
					$('#loading-spinner').addClass('hide');
					$("#addPracticeModal").animate({ scrollTop: 0 }, "slow");
					$("#startdateerror").text("Date and time must be in the future")
					$("#startdateerror").removeClass("hide")
				}
			}
		} else {
			isFormValid = false;
		}

		if (isFormValid && isFormValidActivity && practiceStartDateValid) {

			$.ajax({
				type: "POST",
				url: "/add-team-practice",
				data: $("#practiceForm").serialize()
			}).done(function (msg) {
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					if (scheduleTypeData == "Activity") {
						$("#practice_succ").text("Team Activity added successfully ");
					} else {
						$("#practice_succ").text("Team Practice added successfully ");
					}

					//setTimeout(function() {
					$("#addPracticeModal").modal("hide");
					$('#loading-spinner').addClass('show');
					location.reload(true);
					//}, THREE_SEC); 

				}
				else {
					$('#loading-spinner').addClass('hide');
					//console("Error")
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
			return false
		}



	});

	function hourConverstion(time) {

		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		var AMPM = time.match(/\s(.*)$/)[1];
		if (AMPM == "PM" && hours < 12) hours = hours + 12;
		if (AMPM == "AM" && hours == 12) hours = hours - 12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if (hours < 10) sHours = "0" + sHours;
		if (minutes < 10) sMinutes = "0" + sMinutes;
		return sHours + ":" + sMinutes
	}

	$('.removePractice').click(function (e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/get-team-practice-info",
			data: { "practice_id": $(this).attr("id") }
		}).done(function (info) {
			var obj = $.parseJSON(info)
			//console.log(obj)

			$('#delete_practice_id').val(obj.practice_id);
			$('#delete_practice_repeats').val(obj.repeats);

			if (obj.repeats == "") {
				$("#delete_option_2").addClass("hide");
				$("#delete_option_3").addClass("hide");
			}
			else {

				$("#delete_option_2").removeClass("hide");
				$("#delete_option_3").removeClass("hide");


				$("#edit_practice_date").datepicker("destroy");
				$("#edit_practice_date").attr("readonly", "readonly");
				$('#edit_practice_repeats').attr("readonly", "readonly");
			}


			$("#deletePracticeModalOptions").modal("show");


		});

	})

	$("input[type=radio][name=editscheduleTypeData]").change(function () {
		if (this.value == 'Practice') {
			editpracticeChecked()
		} else if (this.value == 'Activity') {
			editactivityChecked()
		}
	});

	function editpracticeChecked() {
		$('#edit_practice_end_date').val('');
		$("#edit_practice_end_date-hide").addClass('hide')
		$("#edit_practice_repeats-hide").removeClass('hide')
		$("#edit_practice_repeats").attr("disabled", false);
		$("#editenddateerror").text("")
		$("#editenddateerror").addClass("hide")
		$("#editstartdateerror").text("")
		$("#editstartdateerror").addClass("hide")
		$("#practice_edit_submit").text("Edit Practice")
		$("#edit_practice_single_day_event").addClass("hide")
	}

	function editactivityChecked() {
		$('#edit_practice_date').val('');
		$('#edit_practice_repeats').val(" ");
		$("#edit_practice_end_date-hide").removeClass('hide')
		$("#edit_practice_repeats").attr("disabled", true);
		$("#edit_practice_repeats-hide").addClass('hide')
		$("#editenddateerror").text("")
		$("#editenddateerror").addClass("hide")
		$("#editstartdateerror").text("")
		$("#editstartdateerror").addClass("hide")
		$("#practice_edit_submit").text("Edit Activity")
		$("#edit_practice_single_day_event").removeClass("hide")
	}
	var practiceType = "";
	var singleDayPracticeType = false
	var objpracticeenddate = "";
	$('#editsingleDayPractice').change(function () {
		if ($('#editsingleDayPractice').is(":checked")) {
			$('#edit_practice_end_date').val('');
			$('#edit_practice_end_date').attr("disabled", "disabled");
			$('#edit_practice_end_date').removeClass("empty-err");
			if (singleDayPracticeType && practiceType == "1440") {
				$('#edit_practice_length').find('#removefulldays').remove().end().append($("<option id='removefullday'></option>").attr("value", "1440").text("full day"));
				$('#edit_practice_length').val(practiceType).trigger('change');
			} else {
				$('#edit_practice_length').find('#removefulldays').remove().end().append($("<option id='removefullday'></option>").attr("value", "1440").text("full day"));
			}

		} else {
			$('#edit_practice_end_date').removeAttr('disabled');
			$('#edit_practice_end_date').removeClass("empty-err");
			if (!singleDayPracticeType && practiceType == "1440") {
				$('#edit_practice_length').find('#removefullday').remove().end().append($("<option id='removefulldays'></option>").attr("value", "1440").text("full days"));
				$('#edit_practice_length').val(practiceType).trigger('change');
			} else {
				$('#edit_practice_length').find('#removefullday').remove().end().append($("<option id='removefulldays'></option>").attr("value", "1440").text("full days"));
			}
			$('#edit_practice_end_date').val(objpracticeenddate);
		}
	});

	$('.editPractice').click(function (e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/get-team-practice-info",
			data: { "practice_id": $(this).attr("id") }
		}).done(function (info) {
			var obj = $.parseJSON(info)
			//console.log(obj)practiceDateTimeZone
			$("input:radio[name=editscheduleTypeData]").val([obj.practicetype]);
			practiceType = "";
			singleDayPracticeType = false
			objpracticeenddate = ""
			if (obj.practicetype == "Activity") {
				editactivityChecked()
				if (obj.practiceenddate == "") {
					$('#editsingleDayPractice').prop('checked', true);
					$('#edit_practice_end_date').attr("disabled", true);
					singleDayPracticeType = true
					$('#edit_practice_length').find('#removefulldays').remove().end().append($("<option id='removefullday'></option>").attr("value", "1440").text("full day"));
				} else {
					singleDayPracticeType = false
					$('#editsingleDayPractice').prop('checked', false);
					$('#edit_practice_end_date').attr("disabled", false);
					$('#edit_practice_end_date').removeClass("empty-err");
					$('#edit_practice_length').find('#removefullday').remove().end().append($("<option id='removefulldays'></option>").attr("value", "1440").text("full days"));
				}
				objpracticeenddate = obj.practiceenddate
				$('#edit_practice_end_date').val(obj.practiceenddate);
			} else {
				editpracticeChecked()
			}

			$("input:radio[name=editscheduleTypeData]").attr('disabled', true);

			$('#edit_practice_id').val(obj.practice_id);
			$('#edit_practice_date').val(obj.date);
			$('#edit_practice_time').val(obj.practice_time);
			practiceType = obj.practice_length;
			$('#edit_practice_length').val(obj.practice_length).trigger('change');
			$('#edit_practice_location').val(obj.location);
			$('#edit_practice_date_title').text(obj.date_2);
			$('#editLocationTimeZone').val(obj.practiceDateTimeZone);
			$('#editpracticeisOldPractice').val(obj.isOldPractice)
			optionRepeat = ""
			if (!obj.isOldPractice) {
				$('#edit_practice_date').attr('disabled', true);
				$('#edit_practice_time').attr('disabled', true);
				$('#edit_practice_length').attr('disabled', true);
				$('#edit_practice_location').attr('disabled', true);
				$('#edit_practice_repeats').attr('disabled', true);
				$('#editsingleDayPractice').attr('disabled', true);
				$('#edit_practice_end_date').attr('disabled', true);
			} else {
				$('#edit_practice_date').attr('disabled', false);
				$('#edit_practice_time').attr('disabled', false);
				$('#edit_practice_length').attr('disabled', false);
				$('#edit_practice_location').attr('disabled', false);
				$('#edit_practice_repeats').attr('disabled', false);
				$('#editsingleDayPractice').attr('disabled', false);
				optionRepeat = obj.repeats
			}

			if (obj.note != "") {
				$('div.editable_practice_note').html(obj.note);
				$('div.editable_practice_note').attr("data-medium-focused", "true");
				$('#edit_practice_note').val(obj.note);

			}

			//$('#edit_practice_repeats').val(obj.repeats);	
			$('#edit_practice_repeats').val(optionRepeat).trigger('change');
			$('#edit_practice_repeats').attr("lang", optionRepeat);
			$('#edit_notifyTeamMembers').val(obj.notify);
			if (optionRepeat == 'Weekly') {
				$("#edit_practice_repeats option[value='Monthly']").remove();
				$('#edit_practice_repeats').siblings("span").eq(0).css("width", "150px");
				//$('#edit_practice_repeats').trigger('change');
			} else if (optionRepeat == 'Monthly') {
				$("#edit_practice_repeats option[value='Weekly']").remove();
				$('#edit_practice_repeats').siblings("span").eq(0).css("width", "150px");
				$('#edit_practice_repeats').trigger('change');
			}

			$("#editPracticeForm input.required").each(function () {
				$(this).removeClass("empty-err");
			});

			if (optionRepeat == "") {
				$('#edit_practice_date').datepicker({
					autoclose: true,
					minDate: new Date()
				});
			} else {

				$("#edit_option_2").removeClass("hide");
				$("#edit_option_3").removeClass("hide");
			}

			$("#editPracticeModal").modal("show");


		});
	});

	$('#practice_edit_submit').click(function (e) {
		e.preventDefault();
		var isFormValid = false;
		var isFormValidActivity = true;
		var practiceStartDateValid = true;

		var prev_practice_repeats = $('#edit_practice_repeats').attr("lang");
		timeZone = $('#editLocationTimeZone').val() != "" ? $('#editLocationTimeZone').val() : moment.tz.guess()

		var editscheduleTypeData = $("input:radio[name=editscheduleTypeData]:checked").val()
		if (editscheduleTypeData == "Activity" && !$('#editsingleDayPractice').is(':checked')) {
			if ($.trim($('#edit_practice_end_date').val()).length == 0) {
				$('#edit_practice_end_date').addClass("empty-err");
				isFormValidActivity = false;
				$('#loading-spinner').addClass('hide');
				$("#editPracticeModal").animate({ scrollTop: 0 }, "slow");
			}
			else {
				$('#edit_practice_end_date').removeClass("empty-err");
				isFormValidActivity = true;
			}
		}

		if (timeZone != "") {


			var editpracticeisOldPracticeStatus = $('#editpracticeisOldPractice').val()

			$('#editLocationTimeZone').val(timeZone)
			practice_Date = $('#edit_practice_date').val();
			practiceTime = $('#edit_practice_time').val();
			if (practiceTime != "" && practice_Date != "") {
				practiceTime = hourConverstion(practiceTime);
				var dateTime = new Date(practice_Date + " " + practiceTime);
				dateTime = moment(dateTime).format("YYYY-MM-DDTHH:mm:ssZ");
				$('#editPracticeDateTimeZone').val(dateTime);
				isFormValid = true;
			}

			if (editpracticeisOldPracticeStatus == "true") {
				if (moment().format() > dateTime) {
					$('#edit_practice_date').addClass("empty-err");
					practiceStartDateValid = false;
					$('#loading-spinner').addClass('hide');
					$("#editPracticeModal").animate({ scrollTop: 0 }, "slow");
					$("#editstartdateerror").text("Date and time must be in the future")
					$("#editstartdateerror").removeClass("hide")
				} else {
					$("#editstartdateerror").text("")
					$("#editstartdateerror").addClass("hide")
					$('#edit_practice_date').removeClass("empty-err");
				}
			}

			practice_end_date = $('#edit_practice_end_date').val();
			if (practice_end_date && practice_end_date != "" && practiceTime != "") {
				var dateTime1 = new Date(practice_end_date + " " + practiceTime);
				dateTime1 = moment(dateTime1).format("YYYY-MM-DDTHH:mm:ssZ");
				$('#editpracticeEndDateTimeZone').val(dateTime1);

				if (editpracticeisOldPracticeStatus == "true") {
					if (dateTime1 < dateTime || moment().format() > dateTime1) {
						$('#edit_practice_end_date').addClass("empty-err");
						isFormValidActivity = false;
						$('#loading-spinner').addClass('hide');
						$("#editPracticeModal").animate({ scrollTop: 0 }, "slow");
						$("#editstartdateerror").text("Date and time must be in the future")
						$("#editstartdateerror").removeClass("hide")
					} else {
						$("#editenddateerror").text("")
						$("#editenddateerror").addClass("hide")
						$('#edit_practice_end_date').removeClass("empty-err");
					}
				}
			}
		}
		if (isFormValid == true && isFormValidActivity && practiceStartDateValid) {
			if (prev_practice_repeats == "" && $('#edit_practice_repeats').val() != "") {
				if ($('#edit_practice_repeats').val() == "Weekly" || $('#edit_practice_repeats').val() == "Monthly") {

					$("#prev_practice_repeats").val(prev_practice_repeats);
					$('#loading-spinner').removeClass('hide');

					$.ajax({
						type: "POST",
						url: "/edit-team-practice",
						data: $("#editPracticeForm").serialize()
					}).done(function (msg) {
						if (msg == "true") {

							$("#edit_practice_succ").text("Team Practice edited successfully ");
							setTimeout(function () {
								$("#editPracticeModalOptions").modal("hide");
								$('#loading-spinner').addClass('hide');
								location.reload(true);
							}, TWO_SEC);

						}
						else {
							$('#loading-spinner').addClass('hide');
						}
					});
				}
			} else {
				$("#editPracticeModal").modal("hide");
				if ($('#edit_practice_repeats').val() == "") {
					$("#edit_option_2").addClass("hide");
					$("#edit_option_3").addClass("hide");
				} else {
					$("#edit_option_2").removeClass("hide");
					$("#edit_option_3").removeClass("hide");
				}
				$('input[name="edit_options"]:radio').iCheck('uncheck');
				$("#practice_edit_options_submit").attr("disabled", true);
				$("#recurring-error").empty();
				if ($('#edit_practice_repeats').val() == "") {
					$("#recurring-error").text("Kindly select the recurring practice");
				} else {
					$("#recurring-error").text("Kindly select anyone of the recurring practice");
				}
				$("#editPracticeModalOptions").modal("show");
			}

		}
	});

	/*$('.edit_options').click(function () {
		var option = $(this).attr('value')
		$('#edit_practice_option').val(option);
	})

	$('.delete_options').click(function () {
		var option = $(this).val()
		$('#delete_practice_option').val(option);
	})*/

	$('#practice_edit_options_submit').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');

		if ($('#edit_practice_option').val().length > 0) {
			//console.log($("#editPracticeForm").serialize())
			$.ajax({
				type: "POST",
				url: "/edit-team-practice",
				data: $("#editPracticeForm").serialize()
			}).done(function (msg) {
				if (msg == "true") {

					$("#edit_practice_succ").text("Team Practice edited successfully ");
					setTimeout(function () {
						$("#editPracticeModalOptions").modal("hide");
						$('#loading-spinner').addClass('hide');
						location.reload(true);
					}, TWO_SEC);

				} else if (msg == "NoChanges") {
					location.reload(true)
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}

	});

	$('#practice_delete_options_submit').click(function (e) {
		e.preventDefault()
		//$('#loading-spinner').removeClass('hide');

		if ($('#delete_practice_option').val().length > 0) {
			//console.log($("#editPracticeForm").serialize())
			$.ajax({
				type: "POST",
				url: "/remove-team-practice",
				data: {
					"practice_id": $('#delete_practice_id').val(),
					"delete_option": $('#delete_practice_option').val(),
					"repeats": $('#delete_practice_repeats').val()
				}
			}).done(function (msg) {
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$("#edit_practice_succ").text("Team Practice deleted successfully ");
					setTimeout(function () {
						$("#editPracticeModalOptions").modal("hide");
						location.reload(true);
					}, THREE_SEC);

				}
				else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}

	});

	function compare(a, b) {
		if (a.name < b.name)
			return -1;
		if (a.name > b.name)
			return 1;
		return 0;
	}

	$('.attendees').click(function (e) {
		var practice_id = $(this).attr('id').split('_')[1]
		$("#attendee_view_all").find("tr:gt(0)").remove();
		$("#attendee_view_attending").find("tr:gt(0)").remove();
		$("#attendee_view_not_attending").find("tr:gt(0)").remove();
		$("#attendee_view_maybe").find("tr:gt(0)").remove();
		$("#attendee_view_no_response").find("tr:gt(0)").remove();

		// reset to default for email
		$('#send_practice_reminder').addClass("show");
		$('.reminder-sent').addClass("hide");
		$('.sent-on').addClass("hide");
		$('input[name="reminder_email"]:radio').iCheck('uncheck');

		// reset to default for sms
		$('#send_practice_reminder-sms').addClass("show");
		$('.sms-reminder-sent').addClass("hide");
		$('.sms-sent-on').addClass("hide");
		$('input[name="reminder_sms"]:radio').iCheck('uncheck');

		$.ajax({
			type: "POST",
			url: "/get-practice-attendees",
			data: {
				"practice_id": practice_id
			}
		}).done(function (msg) {
			var obj = $.parseJSON(msg)
			//console.log(obj)

			obj.all = obj.all.sort(compare)

			$('#attendance_practice_id').val(practice_id)
			$('#modal_title h3').text("Practice on " + obj.date)
			$('#p_time').text(moment(obj.time, ["h:mm A"]).format("HH:mm") + " (" + obj.duration + " mins" + ")")
			$('#p_location').text(obj.location)

			/*if(obj.last_reminder != "") {
				$('.sent-on').removeClass("hide")
				$('.sent-on').text("Last sent on " + obj.last_reminder)
			}

			if(obj.sms_last_reminder != "") {
				$('.sms-sent-on').removeClass("hide")
				$('.sms-sent-on').text("Last sent on " + obj.sms_last_reminder)
			} */

			$("#u_practice_" + practice_id).text("0 attendees")

			if (obj.all != null) {
				$("#view_count_all").text("[" + obj.all.length + "]")
			}
			if (obj.attending != null) {
				if (obj.attending.length > 1) {
					$("#u_practice_" + practice_id).text(obj.attending.length + " attendees")
				} else if (obj.attending.length == 1) {
					$("#u_practice_" + practice_id).text(obj.attending.length + " attendee")
				}
				$("#view_count_attending").text("[" + obj.attending.length + "]")
			}
			if (obj.not_attending != null) {
				$("#view_count_not_attending").text("[" + obj.not_attending.length + "]")
			}
			if (obj.maybe != null) {
				$("#view_count_maybe").text("[" + obj.maybe.length + "]")
			}
			if (obj.not_responded != null) {
				$("#view_count_not_responded").text("[" + obj.not_responded.length + "]")
			}

			$.each(obj.all, function (i, curr) {
				var row = $("#attendee_view_all tbody tr").first().clone();
				var rowCount = $('#attendee_view_all tbody tr').length;

				if (rowCount > 0) {
					//console.log(row)
					row.removeClass("hide");

					if (curr.files) {
						row.find('img').attr('src', curr.files).each(function (key, val) {
							//console.log(val)
						});
					} else {
						row.find('img').attr('src', '/static/images/user-default.png')
					}

					//console.log(row.find('.attendee-name'))

					row.find('.attendee-name').text(curr.name).each(function (key, val) {
						//console.log(val)
					});

					row.find('.reason').text(curr.reason).each(function (key, val) {
						//console.log(val)
					});

					if (curr.response == "attending") {
						row.find('.inactive-attending').addClass('active-attending')
					}
					else if (curr.response == "not attending") {
						row.find('.inactive-not-attending').addClass('active-not-attending')
					}
					else if (curr.response == "maybe") {
						row.find('.inactive-maybe').addClass('active-maybe')
					}

					row.find('.member-id').text(curr.fk_user_id)

					$('#attendee_view_all').append(row);
				}
			});

			$.each(obj.attending, function (i, curr) {
				var row = $("#attendee_view_attending tbody tr").last().clone();
				var rowCount = $('#attendee_view_attending tbody tr').length;

				if (rowCount > 0) {
					row.removeClass("hide");

					if (curr.files) {
						row.find('img').attr('src', curr.files)
					} else {
						row.find('img').attr('src', '/static/images/user-default.png')
					}

					row.find('.attendee-name').text(curr.name)

					row.find('.reason').text(curr.reason)

					row.find('.inactive-attending').addClass('active-attending')

					row.find('.member-id').text(curr.fk_user_id)

					$('#attendee_view_attending').append(row);
				}
			});

			$.each(obj.not_attending, function (i, curr) {
				var row = $("#attendee_view_not_attending tbody tr").last().clone();
				var rowCount = $('#attendee_view_not_attending tbody tr').length;

				if (rowCount > 0) {
					row.removeClass("hide");

					if (curr.files) {
						row.find('img').attr('src', curr.files)
					} else {
						row.find('img').attr('src', '/static/images/user-default.png')
					}

					row.find('.attendee-name').text(curr.name)

					row.find('.reason').text(curr.reason)

					row.find('.inactive-not-attending').addClass('active-not-attending')

					row.find('.member-id').text(curr.fk_user_id)

					$('#attendee_view_not_attending').append(row);
				}
			});

			$.each(obj.maybe, function (i, curr) {
				var row = $("#attendee_view_maybe tbody tr").last().clone();
				var rowCount = $('#attendee_view_maybe tbody tr').length;

				if (rowCount > 0) {
					row.removeClass("hide");

					if (curr.files) {
						row.find('img').attr('src', curr.files)
					} else {
						row.find('img').attr('src', '/static/images/user-default.png')
					}

					row.find('.attendee-name').text(curr.name)

					row.find('.reason').text(curr.reason)

					row.find('.inactive-maybe').addClass('active-maybe')

					row.find('.member-id').text(curr.fk_user_id)

					$('#attendee_view_maybe').append(row);
				}
			});

			$.each(obj.not_responded, function (i, curr) {
				var row = $("#attendee_view_no_response tbody tr").last().clone();
				var rowCount = $('#attendee_view_no_response tbody tr').length;

				if (rowCount > 0) {
					row.removeClass("hide");

					if (curr.files) {
						row.find('img').attr('src', curr.files)
					} else {
						row.find('img').attr('src', '/static/images/user-default.png')
					}

					row.find('.attendee-name').text(curr.name)

					row.find('.member-id').text(curr.fk_user_id)

					$('#attendee_view_no_response').append(row);
				}
			});

			$("#practiceAttendeesModal").modal("show");
			$('[data-toggle="tooltip"]').tooltip();
			var idElement = document.getElementById('practiceReminderEmail');
			if (idElement != null) {
				document.getElementById('practiceReminderEmail').removeEventListener('click', reminder_email);
			}

			var idElement = document.getElementById('practiceReminder_Sms');
			if (idElement != null) {
				document.getElementById('practiceReminder_Sms').removeEventListener('click', reminder_sms);
			}
			$(".practiceReminder").attr("disabled", "disabled");
			$(".practiceReminder").attr("id", "");
			$('.reminder-sent-error').addClass("hide");
			//$("#attendee_type").val("all").change();
			if (obj.isOldPractice == false) {
				$('#disabledButtonHidden').addClass("hide");
				$('#disabledButtonHidden1').addClass("hide");
				$('#disabledButtonHidden2').addClass("hide");
				$('#disabledButtonHidden3').addClass("hide");
			} else {
				$('#disabledButtonHidden').removeClass("hide");
				$('#disabledButtonHidden1').removeClass("hide");
				$('#disabledButtonHidden2').removeClass("hide");
				$('#disabledButtonHidden3').removeClass("hide");
			}

		});



	});

	$('#attendee_type').change(function (e) {
		var val = $('#attendee_type').val();
		//console.log(val)

		if (val == "all") {
			$("#attendee_view_all").removeClass("hide");
			$("#attendee_view_attending").addClass("hide")
			$("#attendee_view_not_attending").addClass("hide")
			$("#attendee_view_maybe").addClass("hide")
			$("#attendee_view_no_response").addClass("hide")

			$("#view_count_all").removeClass("hide")
			$("#view_count_attending").addClass("hide")
			$("#view_count_not_attending").addClass("hide")
			$("#view_count_maybe").addClass("hide")
			$("#view_count_not_responded").addClass("hide")

		}
		else if (val == "attending") {
			$("#attendee_view_all").addClass("hide");
			$("#attendee_view_attending").removeClass("hide")
			$("#attendee_view_not_attending").addClass("hide")
			$("#attendee_view_maybe").addClass("hide")
			$("#attendee_view_no_response").addClass("hide")

			$("#view_count_all").addClass("hide")
			$("#view_count_attending").removeClass("hide")
			$("#view_count_not_attending").addClass("hide")
			$("#view_count_maybe").addClass("hide")
			$("#view_count_not_responded").addClass("hide")

		}
		else if (val == "not_attending") {
			$("#attendee_view_all").addClass("hide");
			$("#attendee_view_attending").addClass("hide")
			$("#attendee_view_not_attending").removeClass("hide")
			$("#attendee_view_maybe").addClass("hide")
			$("#attendee_view_no_response").addClass("hide")

			$("#view_count_all").addClass("hide")
			$("#view_count_attending").addClass("hide")
			$("#view_count_not_attending").removeClass("hide")
			$("#view_count_maybe").addClass("hide")
			$("#view_count_not_responded").addClass("hide")

		}
		else if (val == "maybe") {
			$("#attendee_view_all").addClass("hide");
			$("#attendee_view_attending").addClass("hide")
			$("#attendee_view_not_attending").addClass("hide")
			$("#attendee_view_maybe").removeClass("hide")
			$("#attendee_view_no_response").addClass("hide")

			$("#view_count_all").addClass("hide")
			$("#view_count_attending").addClass("hide")
			$("#view_count_not_attending").addClass("hide")
			$("#view_count_maybe").removeClass("hide")
			$("#view_count_not_responded").addClass("hide")

		}
		else if (val == "no_response") {
			$("#attendee_view_all").addClass("hide");
			$("#attendee_view_attending").addClass("hide")
			$("#attendee_view_not_attending").addClass("hide")
			$("#attendee_view_maybe").addClass("hide")
			$("#attendee_view_no_response").removeClass("hide")

			$("#view_count_all").addClass("hide")
			$("#view_count_attending").addClass("hide")
			$("#view_count_not_attending").addClass("hide")
			$("#view_count_maybe").addClass("hide")
			$("#view_count_not_responded").removeClass("hide")

		}

	});

	var practice_attending = []
	var practice_not_attending = []
	var practice_maybe = []
	var no_response = []

	$(document).on('click', "label.inactive-attending", function (e) {
		//console.log("attending")
		$(this).toggleClass('active-attending');
		$(this).siblings().removeClass('active-not-attending');
		$(this).siblings().removeClass('active-maybe');
		//console.log($(this).closest('td').next('td').text())

		if ($(this).hasClass('active-attending')) {
			var val = $(this).closest('td').next('td').text()

			var index = practice_not_attending.indexOf(val);

			if (index > -1) {
				practice_not_attending.splice(index, 1);
			}

			index = practice_maybe.indexOf(val);

			if (index > -1) {
				practice_maybe.splice(index, 1);
			}

			if (jQuery.inArray(val, practice_attending) == -1) {
				practice_attending.push(val)
			}
		}
		else {
			var val = $(this).closest('td').next('td').text()

			var index = practice_not_attending.indexOf(val);

			if (index > -1) {
				practice_not_attending.splice(index, 1);
			}

			index = practice_maybe.indexOf(val);

			if (index > -1) {
				practice_maybe.splice(index, 1);
			}

			index = practice_attending.indexOf(val);

			if (index > -1) {
				practice_attending.splice(index, 1);
			}

			if (jQuery.inArray(val, no_response) == -1) {
				no_response.push(val)
			}

		}

	});

	$(document).on('click', "label.inactive-not-attending", function (e) {
		//console.log("not attending")
		$(this).toggleClass('active-not-attending');
		$(this).siblings().removeClass('active-attending');
		$(this).siblings().removeClass('active-maybe');
		//console.log($(this).closest('td').next('td').text())

		if ($(this).hasClass('active-not-attending')) {

			var val = $(this).closest('td').next('td').text()

			var index = practice_attending.indexOf(val);

			if (index > -1) {
				practice_attending.splice(index, 1);
			}

			index = practice_maybe.indexOf(val);

			if (index > -1) {
				practice_maybe.splice(index, 1);
			}

			if (jQuery.inArray(val, practice_not_attending) == -1) {
				practice_not_attending.push(val)
			}
		}
		else {
			var val = $(this).closest('td').next('td').text()

			var index = practice_not_attending.indexOf(val);

			if (index > -1) {
				practice_not_attending.splice(index, 1);
			}

			index = practice_maybe.indexOf(val);

			if (index > -1) {
				practice_maybe.splice(index, 1);
			}

			index = practice_attending.indexOf(val);

			if (index > -1) {
				practice_attending.splice(index, 1);
			}

			if (jQuery.inArray(val, no_response) == -1) {
				no_response.push(val)
			}
		}

	});

	$(document).on('click', "label.inactive-maybe", function (e) {
		//console.log("attending")
		$(this).toggleClass('active-maybe');
		$(this).siblings().removeClass('active-not-attending');
		$(this).siblings().removeClass('active-attending');
		//console.log($(this).closest('td').next('td').text())

		if ($(this).hasClass('active-maybe')) {
			var val = $(this).closest('td').next('td').text()

			var index = practice_attending.indexOf(val);

			if (index > -1) {
				practice_attending.splice(index, 1);
			}

			index = practice_not_attending.indexOf(val);

			if (index > -1) {
				practice_not_attending.splice(index, 1);
			}

			if (jQuery.inArray(val, practice_maybe) == -1) {
				practice_maybe.push(val)
			}
		}
		else {
			var val = $(this).closest('td').next('td').text()

			var index = practice_not_attending.indexOf(val);

			if (index > -1) {
				practice_not_attending.splice(index, 1);
			}

			index = practice_maybe.indexOf(val);

			if (index > -1) {
				practice_maybe.splice(index, 1);
			}

			index = practice_attending.indexOf(val);

			if (index > -1) {
				practice_attending.splice(index, 1);
			}

			if (jQuery.inArray(val, no_response) == -1) {
				no_response.push(val)
			}
		}

	});

	$("#attendance_submit").click(function (e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/mark-practice-attendance",
			data: {
				"attending": practice_attending,
				"not_attending": practice_not_attending,
				"maybe": practice_maybe,
				"no_response": no_response,
				"practice_id": $('#attendance_practice_id').val()
			}
		}).done(function (msg) {

			if (practice_attending.length > 1) {
				$("#u_practice_" + $('#attendance_practice_id').val()).text(practice_attending.length + " attendees")
			} else if (practice_attending.length == 1) {
				$("#u_practice_" + $('#attendance_practice_id').val()).text(practice_attending.length + " attendee")
			} else {
				$("#u_practice_" + $('#attendance_practice_id').val()).text("0 attendees")
			}
			$('.reminder-sent-error').addClass("hide");
			$('.reminder-sent-error').text("")

			$('#practicereminderattendancetext').removeClass("hide");
			$("#practiceAttendeesModal").animate({ scrollTop: 0 }, "slow");
			$('#practicereminderattendancetext').text("Attendance saved successfully")
			setTimeout(function () {
				$('#practicereminderattendancetext').addClass("hide");
				$('#practicereminderattendancetext').text("")
				$('#practiceAttendeesModal').modal("hide")
				location.reload()
			}, TWO_SEC);
		});
	});

	$('input[name="reminder_email"]').on('ifChecked', function (event) {
		$('.reminder-sent-error').addClass("hide");
		var idElement = document.getElementById('practiceReminder_Sms');
		if (idElement != null) {
			document.getElementById('practiceReminder_Sms').removeEventListener('click', reminder_sms);
		}
		event.preventDefault();
		$('input[name="reminder_sms"]:radio').iCheck('uncheck');
		$(".practiceReminder").attr("id", "practiceReminderEmail");
		$(".practiceReminder").removeAttr("disabled");
		document.getElementById('practiceReminderEmail').addEventListener('click', reminder_email);
	});

	function reminder_email() {
		reminder_emailConformationPopup()
	}

	function reminder_emailConformationPopup() {
		$("#confirmModalSmsPracticeReminder").find(".modal-body").empty();

		var appendDom = '<button type="button" class="close " data-dismiss="modal" aria-label="Close"> '
			+ '<span aria-hidden="true"><i class="ti-close"></i></span></button><h3 id="smsReminderTitle">Email Practice reminder and attendance request </h3>'
			+ '<div class="row" id="errorSmsReminder"><div class="col-md-12">Are you sure you want to send a Email Practice reminder and attendance request? </div></div>'
			+ '<div class="row delete-team-user-row" id="smsReminderConfirmBtns"><div class="col-xs-6"><button id="submitEmailPracticeReminder" type="button" class="btn btn-primary btn-fullwidth">Ok</button></div>'
			+ '<div class="col-xs-6"><button type="reset" class="btn btn-secondary btn-fullwidth" data-dismiss="modal" aria-label="Close">Cancel</button></div></div>';

		$("#confirmModalSmsPracticeReminder").find(".modal-body").append(appendDom);
		$("#confirmModalSmsPracticeReminder").modal("show");
	}

	$(document).on('click', "#submitEmailPracticeReminder", function (e) {
		$('#loading-spinner').removeClass('hide');
		$('.reminder-sent-error').addClass("hide");
		$('.reminder-sent-error').text("")
		$("#confirmModalSmsPracticeReminder").modal("hide");
		$.ajax({
			type: "POST",
			url: "/send-practice-reminder",
			data: {
				"practice_id": $('#attendance_practice_id').val(),
				"communication_channel": "email"
			}
		}).done(function (msg) {
			$('#loading-spinner').addClass('hide');

			var comObj = $.parseJSON(msg);
			if (comObj.status == "true") {
				$('.reminder-sent').removeClass("hide");
				$('.sent-on').removeClass("hide");
				$('.sent-on').text("sent" + " " + moment().format("DD MMM YYYY"));
				$('input[name="reminder_email"]:radio').iCheck('uncheck');
				$(".practiceReminder").attr("disabled", "disabled");
			} else if (comObj.status == "oldPractice") {
				$('#disabledButtonHidden').addClass("hide");
				$('#disabledButtonHidden1').addClass("hide");
				$('#disabledButtonHidden2').addClass("hide");
				$('#disabledButtonHidden3').addClass("hide");
			} else if (comObj.status == "none") {
				$("#confirmModalSmsPracticeReminder").modal("hide");
				$('.reminder-sent-error').removeClass("hide");
				$('.reminder-sent-error').addClass("error");
				$('.reminder-sent-error').text("All team members are responded; reminder has not be sent")
			} else {
				$('.reminder-sent-error').removeClass("hide");
				$('.reminder-sent-error').addClass("error");
				$('.reminder-sent-error').text("Error in sending reminder")
			}
		});
	});
	$('input[name="reminder_sms"]').on('ifChecked', function (event) {
		event.preventDefault();
		$('.reminder-sent-error').addClass("hide");
		var idElement = document.getElementById('practiceReminderEmail');
		if (idElement != null) {
			document.getElementById('practiceReminderEmail').removeEventListener('click', reminder_email);
		}
		$('input[name="reminder_email"]:radio').iCheck('uncheck');
		$(".practiceReminder").attr("id", "practiceReminder_Sms");
		$(".practiceReminder").removeAttr("disabled");

		document.getElementById('practiceReminder_Sms').addEventListener('click', reminder_sms);
	});

	function reminder_sms() {
		reminder_smsConformationPopup()
	}

	function reminder_smsConformationPopup() {
		$("#confirmModalSmsPracticeReminder").find(".modal-body").empty();
		smsSendwithDissucOrReminder = ""
		$.ajax({
			type: "GET",
			url: "/check-team-sms-limit",
			data: ""
		}).done(function (msg) {
			$('#loading-spinner').addClass('hide');
			var tempmsg = JSON.stringify(msg)
			var tSMSLimitObj = $.parseJSON(tempmsg);
			var sms_limit = parseInt(tSMSLimitObj.sms_limits);
			var free_trial_status = tSMSLimitObj.free_trial_status;
			var premium_Subscription = tSMSLimitObj.premium_subscription;
			var tempresponseCount = parseInt($('#activeTeamMemberCount').val())
			if (free_trial_status == "true" && !superAdminStatus) {
				if (sms_limit < tempresponseCount) {
					$('#teamsubscriptionmodel').modal('show')
					subscriptionPopup = false
					$('#teammodelheaderParttext').text("Gushou Service Msg")
					// $('#appendHeaderText').html("<p style='text-align: justify;' >Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p>")
					$('#teamsubscriptiocheckboxhide').addClass('hide')
					if (!teamCaptainStatus) {
						$('#appendHeaderText').html("<p style='text-align: justify;'>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p><p>Please contact team captain.</p>")
						$('#activeTeamCaptainhideshow').addClass('hide')
						$('#activeBasicUserhideshow').removeClass('hide')
					} else {
						$('#appendHeaderText').html("<p style='text-align: justify;'>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p style='text-align: justify;'>The good news; you can upgrade to Gushou’s premium plan to enjoy this feature as well as other great features!</p>")
						$('#activeTeamCaptainhideshow').removeClass('hide')
						$('#activeBasicUserhideshow').addClass('hide')
					}
				} else if (sms_limit >= tempresponseCount) {
					var appendDom = '<button type="button" class="close cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close"> '
						+ '<span aria-hidden="true"><i class="ti-close"></i></span></button><h3 id="smsReminderTitle">Gushou Service Msg</h3>'
						+ '<div class="row" id="errorSmsReminder"><div class="col-md-12"><p>Your team account has ' + sms_limit + ' SMS msgs remaining.</p></div></div>'
						+ '<div class="row delete-team-user-row" id="smsReminderConfirmBtns"><div class="col-xs-6"><button id="submitSmsPracticeReminder" type="button" class="btn btn-primary btn-fullwidth">Ok</button></div>'
						+ '<div class="col-xs-6"><button type="reset" class="btn btn-secondary btn-fullwidth cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close">Cancel</button></div></div>';

					$("#confirmModalSmsPracticeReminder").find(".modal-body").append(appendDom);
					$("#confirmModalSmsPracticeReminder").modal("show");

				}
			} else if (premium_Subscription == "true" && !superAdminStatus) {
				if (sms_limit < tempresponseCount) {

					if (teamCaptainStatus) {
						if (smsNextCreditStartingDate == "") {
							$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p>");
						} else {
							$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits that you can use to start sending text messages again.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p>");
						}
						$('.changeSmsIdValue').attr("id", "submitSmsPracticeReminder");
						smsSendwithDissucOrReminder = "smsremider"
						$('#smspackageactivebasicuserhideshow').addClass('hide')
						$('#smspackageactiveteamcaptainhideshow').removeClass('hide')
					} else {
						if (smsNextCreditStartingDate == "") {
							$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p><p>Please contact team captain.</p>");
						} else {
							$("#sms_package_text").html("<p>Your team account has " + sms_limit + " text messages remaining - unfortunately not enough to send this message via text. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits that you can use to start sending text messages again.</p><p>Or... because it seems that you're a busy team, and may need more credits today to keep up with your communication efforts, there is an option to buy an additional 1,000 text messages for your team account for $15 USD? These credits will be available for one year from date of purchase and there are no refunds.</p> <p>Please contact team captain.</p>");
						}
						$('#smspackageactiveteamcaptainhideshow').addClass('hide')
						$('#smspackageactivebasicuserhideshow').removeClass('hide')
					}
					$('#smsPackageSubscriptionModal').modal('show')

				} else if (sms_limit >= tempresponseCount) {
					$("#errorMsgSmsLimit").empty();
					var headingText = ""
					if (smsNextCreditStartingDate == "") {
						headingText = "<p>Your team account has " + sms_limit + " SMS msgs remaining.</p>";
					} else {
						headingText = "<p>Your team account has " + sms_limit + " SMS msgs remaining. On " + smsNextCreditStartingDate + " you will receive your monthly top-up of 400 text message credits.</p>";
					}
					var appendDom = '<button type="button" class="close cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close"> '
						+ '<span aria-hidden="true"><i class="ti-close"></i></span></button><h3 id="smsReminderTitle">Gushou Service Msg</h3>'
						+ '<div class="row" id="errorSmsReminder"><div class="col-md-12">' + headingText + '</div></div>'
						+ '<div class="row delete-team-user-row" id="smsReminderConfirmBtns"><div class="col-xs-6"><button id="submitSmsPracticeReminder" type="button" class="btn btn-primary btn-fullwidth">Ok</button></div>'
						+ '<div class="col-xs-6"><button type="reset" class="btn btn-secondary btn-fullwidth cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close">Cancel</button></div></div>';

					$("#confirmModalSmsPracticeReminder").find(".modal-body").append(appendDom);
					$("#confirmModalSmsPracticeReminder").modal("show");

				}
			} else if (free_trial_status == "false" && premium_Subscription == "false" && !superAdminStatus) {
				$('#subscriptionteammodel').modal("show")
				freetrialPopup = false
				$('#subcribeteammodelheaderParttext').text("Gushou Service Msg")
				$('#clickfreeTrialPopuphide').addClass('hide');
			} else if (superAdminStatus) {
				var appendDom = '<button type="button" class="close cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close"> '
					+ '<span aria-hidden="true"><i class="ti-close"></i></span></button><h3 id="smsReminderTitle">SMS Practice reminder and attendance request </h3>'
					+ '<div class="row" id="errorSmsReminder"><div class="col-md-12">Are you sure you want to send a SMS Practice reminder and attendance request? </div></div>'
					+ '<div class="row delete-team-user-row" id="smsReminderConfirmBtns"><div class="col-xs-6"><button id="submitSmsPracticeReminder" type="button" class="btn btn-primary btn-fullwidth">Ok</button></div>'
					+ '<div class="col-xs-6"><button type="reset" class="btn btn-secondary btn-fullwidth cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close">Cancel</button></div></div>';

				$("#confirmModalSmsPracticeReminder").find(".modal-body").append(appendDom);
				$("#confirmModalSmsPracticeReminder").modal("show");
			}
		});
	}

	$(document).on('click', ".cancelSmsPracticeReminder", function (e) {
		$('input[name="reminder_sms"]:radio').iCheck('uncheck');
		$(".practiceReminder").attr("disabled", "disabled");
	});

	$(document).keyup(function (e) {
		if (e.keyCode == 27) {
			if ($("#confirmModalSmsPracticeReminder").is(":visible")) {
				if ($("#confirmModalSmsPracticeReminder").find("#smsLimitTitle").length > 0) {
					$('input[name="reminder_sms"]:radio').iCheck('uncheck');
					$(".practiceReminder").attr("disabled", "disabled");
				}
			}
		}
	});

	$('#confirmModalSmsPracticeReminder').on('hidden.bs.modal', function (event) {
		$('input[name="reminder_sms"]:radio').iCheck('uncheck');
	});


	$(document).on('click', "#submitSmsPracticeReminder", function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$("#smsPackageSubscriptionModal").modal('hide')
		$.ajax({
			type: "POST",
			url: "/send-practice-reminder",
			data: {
				"practice_id": $('#attendance_practice_id').val(),
				"communication_channel": "sms"
			}
		}).done(function (msg) {

			$('#loading-spinner').addClass('hide');
			var comObj = $.parseJSON(msg);
			if (comObj.status == "true") {
				$("#confirmModalSmsPracticeReminder").modal("show");
				$("#confirmModalSmsPracticeReminder").find(".modal-body").empty();
				$("#smsReminderTitle").empty();
				$("#errorSmsReminder > div").empty();
				$("#smsReminderConfirmBtns").empty();
				$('.sms-sent-on').removeClass("hide");
				$('.sms-sent-on').text("sent" + " " + moment().format("DD MMM YYYY"));

				var smsResponse = comObj.sms_ersponse_status;
				var smsSentCount = 0;
				var smsFailedMembers = "";
				//var smsSuccessMembers = "";

				for (var index in smsResponse) {
					var smsObj = smsResponse[index];
					if (smsObj.sms_status == "true") {
						smsSentCount++;
						//	smsSuccessMembers += smsObj.display_name + "&"
					} else {
						smsFailedMembers += smsObj.display_name + ", " + smsObj.email_address + " - " + smsObj.user_sms_error + "&"
					}
				}
				smsFailedMembers = smsFailedMembers.substr(smsFailedMembers, smsFailedMembers.length - 1)
				var appendInner = '<button type="button" class="close cancelSmsPracticeReminder" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span></button><h3 id="smsLimitTitle">Gushou Service Msg</h3>'

					+ '<div class="row"><div class="col-md-12" id="errSmsReminderLimits" >Your team account has ' + comObj.available_sms_count + ' SMS msgs remaining.</div></div><h3 id="smsReminderTitle"></h3><div class="row"><div class="col-md-12" id="errorSmsReminder" ></div></div><div class="row delete-team-user-row" id="smsReminderConfirmBtns"></div>';

				$("#confirmModalSmsPracticeReminder").find(".modal-body").append(appendInner);

				if (parseInt(smsSentCount) == 1 || parseInt(smsSentCount) > 1) {
					$("#smsReminderTitle").append("SMS Reminder Status");
					$("#smsReminderTitle").css({ "margin-bottom": "20px", "margin-top": "16px" });
				} else if (smsResponse.length == 0) {
					$("#smsReminderTitle").append("Already reply message got from team members!");
					$("#smsReminderTitle").css({ "margin-bottom": "20px", "margin-top": "16px" });
				}

				if (parseInt(smsSentCount) == 1) {
					$("#errorSmsReminder").append("<p>Text message has been sent to  ‘" + smsSentCount + "’ member.</p>");
				} else if (parseInt(smsSentCount) > 1) {
					$("#errorSmsReminder").append("<p>Text message has been sent to  ‘" + smsSentCount + "’ members.</p>");
				}

				if (smsFailedMembers != "") {
					$("#errorSmsReminder").append("<p>not sent to members below: ");
					var failedMembers = smsFailedMembers.split("&");
					for (var i in failedMembers) {
						$("#errorSmsReminder").append("<p>" + failedMembers[i] + "</p>");
					}
				}
				$("#smsReminderConfirmBtns").append("<div class='col-xs-12'><button type='reset' class='btn btn-primary btn-fullwidth cancelPracticeReminder' data-dismiss='modal' aria-label='Close'>Ok</button></div>")
				$("#confirmModalSmsPracticeReminder").modal("show");
			}
			else if (comObj.status == "none") {
				$("#confirmModalSmsPracticeReminder").modal("hide");
				$('.reminder-sent-error').removeClass("hide");
				$('.reminder-sent-error').addClass("error");
				$('.reminder-sent-error').text("All team members unavailable; reminder has not be sent")
			} else if (comObj.status == "oldPractice") {
				$("#confirmModalSmsPracticeReminder").modal("hide");
				$('.reminder-sent-error').addClass("hide");
				$('#disabledButtonHidden').addClass("hide");
				$('#disabledButtonHidden1').addClass("hide");
				$('#disabledButtonHidden2').addClass("hide");
				$('#disabledButtonHidden3').addClass("hide");
			} else if (comObj.status == "subscription") {
				$("#confirmModalSmsPracticeReminder").modal("hide");
				$('#subcribeteammodelheaderParttext').text("Option to Upgrade")
				$("#subscriptionteammodel").modal('show')
				freetrialPopup = false
				$('#clickfreeTrialPopuphide').addClass('hide');
			} else {
				$("#confirmModalSmsPracticeReminder").modal("hide");
				$('.reminder-sent-error').removeClass("hide");
				$('.reminder-sent-error').addClass("error");
				$('.reminder-sent-error').text("Error in sending reminder")
			}

		});

	});


});

// js for edit team
$(function () {

	$('#edit_team_submit').click(function (e) {
		e.preventDefault();
		$('.success-message ').addClass('hide');
		$('#loading-spinner').removeClass('hide');


		$('input').val(function (_, value) {
			return $.trim(value);
		});

		$('#twitter_err').addClass('hide');
		$('#phone_err').addClass('hide');
		$('#website_err').addClass('hide');
		$('#fb_err').addClass('hide');
		$('#youtube_err').addClass('hide');
		$('#email_err').addClass('hide');
		$('#team_name_err').addClass('hide');
		$('#team_errors').addClass('hide');

		$('#team_name').removeClass('empty-err');
		$('#team_email').removeClass('empty-err');
		$('#team_twitter').removeClass('empty-err');
		$('#team_website').removeClass('empty-err');
		$('#team_phone_number').removeClass('empty-err');
		$('#team_facebook').removeClass('empty-err');
		$('#team_youtube').removeClass('empty-err');
		$("#loading-spinner").removeClass('hide');



		var phone_regex = /^[0-9\s-]*$/;
		var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var rege = /^[a-zA-Z0-9\s-']*$/;
		var team_name_regex = /^[a-zA-Z\d\-_.,\s]+$/;
		var prefix = 'http://';
		var prefix2 = 'https://';

		var team_phone = $('#team_phone_number').val().trim();

		var team_name = $('#team_name').val().trim();
		var location = $('#teamlocation').val().trim();
		var team_email = $('#team_email').val().trim();
		var team_facebook = $('#team_facebook').val().trim();
		var team_twitter = $('#team_twitter').val().trim();
		var team_website = $('#team_website').val().trim();
		var team_youtube = $('#team_youtube').val().trim();

		var isFormValid = true;

		var facebook_url = checkUrlPrefix(team_facebook, prefix2)
		var twitter_url = checkUrlPrefix(team_twitter, prefix2)
		var youtube_url = checkUrlPrefix(team_youtube, prefix2)
		team_website = checkUrlPrefix(team_website, prefix)

		$("#editTeamForm input.required").each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		if (!isFormValid) {
			$('#loading-spinner').addClass('hide');
			return false;
		}
		$('#team_errors').addClass('hide');
		$('#team_errors').text('');

		if (team_twitter != "") {
			if (isUrlValid(twitter_url) || team_twitter.substring(0, 1) == "@") {
				$('#twitter_err').removeClass('hide');
				$("#twitter_err").text("Invalid. Please enter only your username");
				$('#team_twitter').focus();
				$('#loading-spinner').addClass('hide');
				return false
			}
			team_twitter = "https://www.twitter.com/" + team_twitter;
		}
		if (team_facebook != "") {
			if (isUrlValid(facebook_url)) {
				$('#fb_err').removeClass('hide');
				$("#fb_err").text("Invalid. Please enter only your username");
				$('#team_facebook').focus();
				$('#loading-spinner').addClass('hide');
				return false;
			}
			team_facebook = "https://www.facebook.com/" + team_facebook;
		}

		if (team_youtube != "") {
			if (isUrlValid(youtube_url)) {
				$('#youtube_err').removeClass('hide');
				$("#youtube_err").text("Invalid. Please enter only your username");
				$('#team_youtube').focus();
				$('#loading-spinner').addClass('hide');
				return false;
			}
			team_youtube = "http://www.youtube.com/" + team_youtube;
		}

		if (!phone_regex.test(team_phone)) {
			$('#phone_err').removeClass('hide');
			$('#team_phone_number').addClass('empty-err').focus();
			$("#phone_err").text("Please enter a proper phone number");
			$('#loading-spinner').addClass('hide');
			return false
		}

		/*if(!team_name_regex.test(team_name)){
			$('#team_name_err').removeClass('hide');
			$('#team_name').addClass('empty-err').focus();
			$("#team_name_err").text("Please remove the special characters from the team name");
			$('#loading-spinner').addClass('hide');
			return false
		}*/

		if (team_email) {
			if (!email_regex.test(team_email)) {
				$('#email_err').removeClass('hide');
				$('#team_email').addClass('empty-err').focus();
				$("#email_err").text("Incorrect email address. Please enter a proper email ID");
				$('#loading-spinner').addClass('hide');
				return false;
			}
		}

		if (team_website) {
			//console(team_website)
			if (!isUrlValid(team_website)) {
				$('#website_err').removeClass('hide');
				$('#team_website').addClass('empty-err').focus();
				$("#website_err").text("Invalid. Enter a proper URL");
				$('#loading-spinner').addClass('hide');
				return false;
			}
		}

		if (isFormValid) {

			//console(team_name)

			$.ajax({
				type: "POST",
				url: "/check-team-name",
				data: {
					"team_name": team_name,
					"location": location,
					"caller": "edit"
				}
			}).done(function (msg) {
				//console(msg)
				if (msg == "exists") {
					$('#loading-spinner').addClass('hide');
					$('#team_name_err').removeClass('hide');
					$('#team_name').addClass('empty-err').focus();
					$("#team_name_err").text("This team already exists. If you are the new captain, please contact the prior captain and request to be made a captain. This will keep the team's history all in one place and make your job a heck of a lot easier! If you need assistance, please contact Gushou Support at dragonboat@gogushou.com");
					return false;
				}
				else {

					all_href = $('[name="team_description"]').find('a')
					for (var i = 0; i < all_href.length; i++) {
						//console($(all_href[i].outerHTML).attr("href"))
						old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
						old_href = old_href.replace("https", "")
						old_href = old_href.replace("http", "")
						old_href = old_href.replace(":", "")
						old_href = old_href.replace("//", "")

						new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
						//console(old_href, new_href)
						editor_id = $('[name="team_description"]')[0].id
						$("#" + editor_id).html($("#" + editor_id).html().replace(old_href, new_href));

						$("#team_description").val($("#" + editor_id).html())
					};

					//console.log($('#editTeamForm').serializeArray())
					$('#editTeamForm').submit();
					$('.error-message').addClass('hide');
					$(".error-message span").text("");
					$('.success-message').removeClass('hide');
				}
			});

		}
		else {
			$('#loading-spinner').addClass('hide');
		}

	});

	$('#changeClassDivsTeam').click(function (eve) {
		eve.preventDefault()
		$('#class_err').text('')
		$('#class_err').addClass('hide')

		$(".mdivision").find("span").each(function () {
			var id = "M_" + $(this).text().split('\n')[0]
			$('#' + id).prop('checked', true)
		});
		$(".wdivision").find("span").each(function () {
			var id = "W_" + $(this).text().split('\n')[0]
			$('#' + id).prop('checked', true)
		});
		$(".mxdivision").find("span").each(function () {
			var id = "MX_" + $(this).text().split('\n')[0]
			$('#' + id).prop('checked', true)
		});

		$(".error-message").addClass('hide');
		$(".error-message").html("");
		$("#classDivisionsModal").modal("show");
	});

	$('#saveClassesTeam').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$('#cls_div_err').text('')
		$('#cls_div_err').addClass('hide')
		var $m_event_divisions = $('input[name="M_event_divisions[]"]:checked');
		var $w_event_divisions = $('input[name="W_event_divisions[]"]:checked');
		var $mx_event_divisions = $('input[name="MX_event_divisions[]"]:checked');

		if ($m_event_divisions.length > 0 || $w_event_divisions.length > 0 || $mx_event_divisions.length > 0) {
			$.ajax({
				type: "POST",
				url: "/update-team-classes",
				data: $("#updateClasses").serialize()
			}).done(function (msg) {
				//console('updated classes' , msg)
				if (msg == "true") {
					setTimeout(function () {
						$("#classDivisionsModal").modal("hide");
						location.reload();
					}, FIVE_SEC);
				}
			});
		} else {
			$('#loading-spinner').addClass('hide');
			$('#cls_div_err').text('Please select at least one class')
			$('#cls_div_err').removeClass('hide')
			$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
		}
	});

	$("#teamLogoUpload").change(function () {
		$('#lerr_msg').removeClass('error');
		$("#lerr_msg").text("");
		var ext = getFileExtension($('#teamLogoUpload').val());
		if (validateExt(ext)) {
			teamLogoUploadReadURL(this)
		}
		else {
			$('#lerr_msg').addClass('error');
			$("#lerr_msg").text("Image must be png, jpg, jpeg");
			return
		}

	})

	function teamLogoUploadReadURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);

			reader.onload = function () {
				var image = new Image();
				image.src = reader.result;

				image.onload = function () {
					if (image.width > 100 || image.height > 100) {
						$("#imgaccInp").val(null);
						$('#lerr_msg').addClass('error');
						$("#lerr_msg").text("Please upload a image, 100x100 pixels");
						return
					} else {
						$("#team_image_upload").submit()
					}

				};
			};
		}
	}

	$("#team_image_upload").on('submit', (function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		$('#lerr_msg').removeClass('error');
		$("#lerr_msg").text("");

		$.ajax({
			url: "/upload-team-img",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (imagePathValue) {
				$('.teamLogoModalChange').attr('src', imagePathValue)
				$("#teamLogoModal .progress-bar").attr("aria-valuenow", 100);
				$("#teamLogoModal .progress-bar").css("width", "100%");
				$("#teamLogoModal .sr-only").text("100% Complete");
				document.getElementById("teamLogoUpload").value = "";
				setTimeout(function () {
					$("#teamLogoModal .progress-bar").attr("aria-valuenow", 0);
					$("#teamLogoModal .progress-bar").css("width", "0%");
					$("#teamLogoModal .sr-only").text("");
					$("#teamLogoModal").modal("hide");
					location.reload()
				}, TWO_SEC);

				$('#loading-spinner').addClass('hide');
			}
		});
	}));

	$('#remove_team_member').click(function (e) {
		e.preventDefault()
		team_mem = []
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_succ_msgs").text("");
		$('#rem_err_msgs').removeClass('error');
		$("#rem_err_msgs").text("");

		$('input:checkbox[name="team_members[]"]').each(function () {
			if (this.checked) {
				team_mem.push(this.value);
			}
		});
		if (team_mem.length > 0) {
			$("#confirmDeleteModal").modal("show");
		}
		else {
			$("#confirmDeleteModal").modal("hide");
		}
	});

	$(document).on('click', '#removeIndividualMember', function (e) {
		e.preventDefault()
		team_mem = []
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_succ_msgs").text("");
		$('#rem_err_msgs').removeClass('error');
		$("#rem_err_msgs").text("");
		team_mem.push($(this).attr("value"));
		console.log(team_mem)
		if (team_mem.length > 0) {
			tempTextHeading = "<p>Do you really want to remove this member from team?</p>"
			if (team_mem.length >= 2) {
				tempTextHeading = "<p>Do you really want to remove this members from team?</p>"
			}
			newhtml = ""
			newhtml += tempTextHeading;
			newhtml += "<p>Once removed, member will be removed from all rosters.</p>";
			$("#appendTeamInviteRemove").empty().append(newhtml);
			$("#appendTeamMemberInvite").addClass('hide')

			$("#confirmDeleteModal").modal("show");
		}
		else {
			$("#confirmDeleteModal").modal("hide");
		}
	});

	$('#deleteMember').click(function (e) {
		$('#loading-spinner').removeClass('hide');
		//console(team_mem)
		$.ajax({
			type: "POST",
			url: "/remove-team-member",
			data: { "ids": team_mem }
		}).done(function (msg) {
			var tempmsgData = JSON.stringify(msg)
			var obj = $.parseJSON(tempmsgData)
			//console(obj)
			if (obj.status == "true") {
				$('#loading-spinner').addClass('hide');
				$("#rem_succ_msgs").addClass("success");
				$("#rem_succ_msgs").text("Team Member Removed");

				if (obj.fail) {
					$("#rem_err_msgs").addClass("error")
					$("#rem_err_msgs").text("Cannot remove the last captain from this team. Please assign another captain to the team before deleting")
				}
				setTimeout(function () {
					$("#confirmDeleteModal").modal("hide");
					sessionStorage.setItem("activeTab", "#members");
					location.reload();
				}, FIVE_SEC);

			}
			else {
				if (obj.fail) {
					$("#rem_err_msgs").addClass("error")
					$("#rem_err_msgs").text("Cannot remove the last captain from this team. Please assign another captain to the team before deleting")
				}
				$('#loading-spinner').addClass('hide');
				//console("Error")
			}

		});
	});

	$('#edit_team_member').click(function (e) {
		e.preventDefault();
		$(".dasboard-team-table").find("th:last").attr("colspan", "2");
		$(".tm-display").hide();
		$(".tm-editable").show();
		$(".weight-editable").css("display", "table");
		$(".phone-editable").css("display", "block");
		$(".country-code-editable").css("display", "block");
		$(".country-dial-code").next().css("width", "135px");
		$("#saveCancelDiv").show();
	});

	$('#cancel_team_member').click(function (e) {
		e.preventDefault();
		$(".tm-display").show();
		$(".tm-editable").hide();
		$(".weight-editable").css("display", "none");
		$(".phone-editable").css("display", "none");
		$(".country-code-editable").css("display", "none");
		$(".dasboard-team-table").find("th:last").removeAttr("colspan");
		$("#saveCancelDiv").hide();
		$(".dasboard-team-table > tbody > tr").each(function () {
			$(this).find($('.select2-selection--single')).removeClass('empty-err');
			$(this).find("input[name='country_phone_number']").removeClass('empty-err');
		});
	});

	$('#save_team_member').click(function (e) {
		e.preventDefault();
		var phoneNumberValid = true;
		var dialcodeValid = true;
		$(".dasboard-team-table > tbody > tr").each(function () {

			if (isNotEmpty($(this).find('select[name="dial_code"] option:selected').val()) == false) {
				$(this).find($('.select2-selection--single')).addClass('empty-err');
				$(".tm-editable").find($('.select2-selection--single')).removeClass('empty-err');
				dialcodeValid = false
			} else {
				$(this).find($('.select2-selection--single')).removeClass('empty-err');
			}

			if (isNotEmpty($(this).find("input[name='country_phone_number']").val().trim()) == false) {
				$(this).find($('.country_phone_number')).addClass('empty-err');
				phoneNumberValid = false
			} else if (isNotEmpty($(this).find("input[name='country_phone_number']").val().trim()) == true) {
				var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				var phone = $(this).find("input[name='country_phone_number']").val().trim();
				if (phoneRegex.test(phone)) {
					if ((phone.indexOf('-') == -1 && phone.indexOf('.') == -1) || (phone.indexOf('-') > 0 && phone.indexOf('.') > 0)) {
						var formattedPhoneNumber =
							phone.replace(phoneRegex, "$1-$2-$3");
						$(this).find("input[name='country_phone_number']").val(formattedPhoneNumber);
					}
					$(this).find("input[name='country_phone_number']").removeClass('empty-err');
				} else {
					$(this).find("input[name='country_phone_number']").addClass('empty-err');
					phoneNumberValid = false
				}
			}

			team_members_update.push(+ $(this).find("input[type='checkbox']").val() + "#"
				+ $(this).find('select[name="paddling_side"] option:selected').val() + "#"
				+ $(this).find('select[name="skill_level"] option:selected').val() + "#"
				+ $(this).find("input[name='weight']").val() + "#"
				+ $(this).find('select[name="dial_code"] option:selected').val() + "#"
				+ $(this).find("input[name='country_phone_number']").val());
		});

		if (dialcodeValid && phoneNumberValid) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/save-team-member",
				data: { "team_members_update": team_members_update }
			}).done(function (msg) {
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$("#saveCancelDiv").hide();
					location.reload();
				}
				else {
					$('#loading-spinner').addClass('hide');
					$(".tm-display").show();
					$(".tm-editable").hide();
					$(".weight-editable").css("display", "none");
					$(".phone-editable").css("display", "none");
					$(".country-code-editable").css("display", "none");
					$("#saveCancelDiv").hide();
				}
			});

		}
	});


	$('#uploadWaiverFiles').click(function (eve) {
		eve.preventDefault()
		$("#uploadWaiverFilesModal .progress-bar").attr("aria-valuenow", 0);
		$("#uploadWaiverFilesModal .progress-bar").css("width", 0);
		$("#uploadWaiverFilesModal .sr-only").text("");
		$('#waiver_err_msg').removeClass('error');
		$('#waiver_succ_msg').removeClass('success');
		$("#waiver_err_msg, #waiver_succ_msg").text('');
		$("#select2-event_name-container, #waiver_name").val('');
		$(".select-custom").select2('val', '');
		$("#waiverUpload").val('')
		$("#uploadWaiverFilesModal").modal("show");
	});

	$("#waiverUpload").change(function () {
		$('#waiver_err_msg').removeClass('error');
		$("#waiver_err_msg").text("");
		var ext = $('#waiverUpload').val().split(".")[1];
		if ((ext == "pdf" || ext == "PDF")) {
			$("#uploadWaiverFilesModal .progress-bar").attr("aria-valuenow", 100);
			$("#uploadWaiverFilesModal .progress-bar").css("width", "100%");
			$("#uploadWaiverFilesModal .sr-only").text("100% Complete");
		}
		else {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Please upload a pdf file");
			return
		}

	});

	$("#waiverIndividualUpload").change(function () {
		$('#waiver_err_msg_upload').removeClass('error');
		$("#waiver_err_msg_upload").text("");
		var ext = $('#waiverIndividualUpload').val().split(".")[1];
		if ((ext == "pdf" || ext == "PDF")) {
			$("#uploadSignedWaiverFilesModal .progress-bar").attr("aria-valuenow", 100);
			$("#uploadSignedWaiverFilesModal .progress-bar").css("width", "100%");
			$("#uploadSignedWaiverFilesModal .sr-only").text("100% Complete");
		}
		else {
			$('#waiver_err_msg_upload').addClass('error');
			$("#waiver_err_msg_upload").text("Please upload a pdf file");
			return
		}

	});
	$("#waiverEventUpload").change(function () {
		$('#waiver_err_msg').removeClass('error');
		$("#waiver_err_msg").text("");
		var ext = $('#waiverEventUpload').val().split(".")[1];
		if ((ext == "pdf" || ext == "PDF")) {
			$("#uploadEventWaiverFilesModal .progress-bar").attr("aria-valuenow", 100);
			$("#uploadEventWaiverFilesModal .progress-bar").css("width", "100%");
			$("#uploadEventWaiverFilesModal .sr-only").text("100% Complete");
		}
		else {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Please upload a pdf file");
			return
		}

	});
	
	$(document).on('click', '#addEventWaiver', function (e) {
		e.preventDefault()
		console.log("hi")
		$('#waiver_err_msg').removeClass('error');
		$("#waiver_err_msg").text("");
		$('#waiver_succ_msg').removeClass('success');
		$("#waiver_succ_msg").text("");
		$("#waiver_name_add_waiver_event").val("");
		

	var eventName= $("#getEventName").val()
	var eventid = $("#getEventId").val()
	
	
	$("#event_name_add_waiver_event").val(eventName)
	$("#event_id_add_waiver_event").val(eventid)

	$("#uploadEventWaiverFilesModal .progress-bar").attr("aria-valuenow", 0);
	$("#uploadEventWaiverFilesModal .progress-bar").css("width", 0);
	$("#uploadEventWaiverFilesModal .sr-only").text("");
	$("#uploadEventWaiverFilesModal").val('')
	$('#uploadEventWaiverFilesModal').modal('show')
	
	});

	

	$("#event_waiver_upload").on('submit', (function (e) {
		e.preventDefault();
		console.log("helloOO")
		$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		var event_id = $('#event_id_add_waiver_event').val();
		var waiver_name = $('#waiver_name_add_waiver_event').val()
		console.log("--",event_id)
		var ext = $('#waiverEventUpload').val().split(".")[1];
		if (!waiver_name) {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Enter waiver name");
			$('#waiver_succ_msg').removeClass('success');
			$("#waiver_succ_msg").text("");
			$('#loading-spinner').addClass('hide');
			return false;
		}
		if ((ext == "pdf" || ext == "PDF")) {
			console.log("successsss")
			$.ajax({
				url: "/add-waiver-from-event",
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
			}).done(function (msg) {
				
				if(msg=="true"){

					$('#waiver_err_msg').removeClass('error');
					$("#waiver_err_msg").text("");
					$('#waiver_succ_msg').addClass('success');
					$('#waiver_succ_msg').text('Waiver saved successfully');
					
					$('#loading-spinner').addClass('hide');
					setTimeout(function () {
						location.reload()
					}, FIVE_SEC);
				}else if(msg=="No Teams Registered For Event"){
					$('#loading-spinner').addClass('hide');
						$('#waiver_err_msg').addClass('error');
						$("#waiver_err_msg").text("No Teams Registered For Event");

				}
				else if(msg=="flagfalse"){
					$('#loading-spinner').addClass('hide');
						$('#waiver_err_msg').addClass('error');
						$("#waiver_err_msg").text("Waiver already exists for registered teams");
					
				}
			});

		}else {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Please upload a pdf file");
			$('#loading-spinner').addClass('hide');
			return
		}
	}));

	$("#team_waiver_upload").on('submit', (function (e) {
		e.preventDefault();
		$('#waiver_err_msg').removeClass('error');
		$("#waiver_err_msg").text("");
		$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		console.log(formData)
		var ext = $('#waiverUpload').val().split(".")[1];
		//console(ext)

		var event_id = $('#event_name').val();
		var waiver_name = jQuery.trim($('#waiver_name').val());
		if (!event_id) {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Enter event name");
			$('#loading-spinner').addClass('hide');
			return false;
		}
		if (!waiver_name) {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Enter waiver name");
			$('#loading-spinner').addClass('hide');
			return false;
		}

		if ((ext == "pdf" || ext == "PDF")) {
			$.ajax({
				url: "/upload-team-waiver",
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					if (data == "true") {
						//console("Success")
						$('#loading-spinner').addClass('hide');
						$('#waiver_succ_msg').addClass('success');
						$("#waiver_succ_msg").text("Waiver saved successfully");
						setTimeout(function () {
							$("#uploadWaiverFilesModal").modal("hide");
							sessionStorage.setItem('activeTab', '#waiver')
							location.reload();
						}, TWO_SEC)
					}
					else if (data == "3") {
						$('#loading-spinner').addClass('hide');
						$('#waiver_err_msg').addClass('error');
						$("#waiver_err_msg").text("Selected event already has a waiver");
					}

				}
			});
		}
		else {
			$('#waiver_err_msg').addClass('error');
			$("#waiver_err_msg").text("Please upload a pdf file");
			$('#loading-spinner').addClass('hide');
			return
		}

	}));
	$("#team_signed_waiver_upload").on('submit', (function (e) {
		e.preventDefault();
		console.log("hi")
		$('#waiver_err_msg_upload').removeClass('error');
		$("#waiver_err_msg_upload").text("");
		$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		console.log(formData)
		var ext = $('#waiverIndividualUpload').val().split(".")[1];
		
	
		if ((ext == "pdf" || ext == "PDF")) {
			$.ajax({
				url: "/upload-signed-team-waiver",
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					if (data == "true") {
						//console("Success")
						$('#loading-spinner').addClass('hide');
						$('#waiver_succ_msg_upload').addClass('success');
						$("#waiver_succ_msg_upload").text("Waiver saved successfully");
						$("#uploadSignedWaiverFilesModal").animate({ scrollTop: 0 }, "slow");
						
						setTimeout(function () {
							$("#uploadSignedWaiverFilesModal").modal("hide");
						}, FIVE_SEC)
						if ($("#waiver_event_reg_id_for_upload").val() != ""){
							console.log("eve")
						
							getTeamMemberDetails($("#waiver_event_reg_id_for_upload").val())

						}else{
							console.log("team")
							individualWaiver($("#waiver_id_for_upload").val(), $("#waiver_team_id_for_upload").val(), $("#waiver_event_id_for_upload").val(), "")
							
						}
				
					}
					else if (data == "3") {
						$('#loading-spinner').addClass('hide');
						//$('#waiver_err_msg').addClass('error');
						//$("#waiver_err_msg").text("Selected event already has a waiver");
					}
	
				}
			});
		}
		else {
			$('#waiver_err_msg_upload').addClass('error');
			$("#waiver_err_msg_upload").text("Please upload a pdf file");
			$('#loading-spinner').addClass('hide');
			return
		}
	
	}));
	
	$(document).on('click', '.view_uploaded_waiver', function (e) {
	e.preventDefault()
	$('#loading-spinner').removeClass('hide');
	var waiverUrlSlug = $(this).attr('value');
	localStorage.setItem('teamUploadWaiverPart', 'true')
	window.location.href = waiverUrlSlug;

	$('#loading-spinner').addClass('hide');
});

$(document).on('click', '.view_uploaded_waiver_event', function (e) {
	e.preventDefault()
	$('#loading-spinner').removeClass('hide');
	var waiverUrlSlug = $(this).attr('value');
	localStorage.setItem('teamUploadWaiverPart', 'false')
	window.location.href = waiverUrlSlug;

	$('#loading-spinner').addClass('hide');
});

	// $('.select_all_waiver_members').click(function(){
	// 	var val = $(this).val()
	// 	if(this.checked) {
	// 		$('input:checkbox[name="waiver_'+val+'_members"]').each(function() {
	// 			if ($(this).closest('tr')[0].children[3].innerText.length == 0) {

	// 				$(this).prop('checked',true);
	// 			}

	// 		});
	// 	}
	// 	else {
	// 		$('input:checkbox[name="waiver_'+val+'_members"]').each(function() {
	// 			$(this).prop('checked',false);
	// 		});
	// 	}
	// });

	$(document).on('click', '.select_all_waiver_members', function (even) {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="waiver_' + val + '_members"]').each(function () {
				if ($(this).closest('tr')[0].children[3].innerText.length == 0) {
					$(this).prop('checked', true);
					$("#tr_background" + $(this).val()).addClass('waiver-checked');
				}

			});
		}
		else {
			$('input:checkbox[name="waiver_' + val + '_members"]').each(function () {
				$(this).prop('checked', false);
				$("#tr_background" + $(this).val()).removeClass('waiver-checked');
			});
		}
	});

	$(document).on('click', '.select_individual_waiver_members', function () {
		var val = $(this).val()
		if (this.checked) {
			$("#tr_background" + $(this).val()).addClass('waiver-checked');
		}
		else {
			$("#tr_background" + $(this).val()).removeClass('waiver-checked');
		}
	});

	var selected_waiver_members = [];
	var waiver_id

	backWaiverList = function () {
		selected_waiver_members = []
		$('#waivermemberappend').addClass('hide')
		$('#waivertab').removeClass('hide')
		$('#waiverback').addClass('hide')
		$('#waiverhed').addClass('hide')
		$('#uploadWaiverFiles').removeClass('hide')
		sessionStorage.setItem("activeTab", "#waiver");
		location.reload();
	};


	$('#sendWaiverBtn').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');

		console.log(selected_waiver_members)
		if (selected_waiver_members.length > 0) {
			$.ajax({
				type: "POST",
				url: "/send-team-waiver",
				data: { "member_ids": selected_waiver_members, "waiver_id": waiver_id }
			}).done(function (msg) {
				if (msg == "true") {
					$("#sendWaiverModal").modal("hide");
					$('#loading-spinner').addClass('hide');
					$('input:checkbox[name="waiver_' + waiver_id + '_members"]').each(function () {
						$(this).prop('checked', false);
						$("#tr_background" + $(this).val()).removeClass('waiver-checked');
					});
					$('.select_all_waiver_members').prop('checked', false)
					teammemberwaiverId = $('#teammemberwaiverId').val();
					teammemberteamId = $('#teammemberteamId').val();
					teammembereventId = $('#teammembereventId').val();
					teammemberwaivername = $('#teammemberwaivername').val();
					individualWaiver(teammemberwaiverId, teammemberteamId, teammembereventId, teammemberwaivername)
				}
				else {
				
					$('#loading-spinner').addClass('hide');
					setTimeout(function () {
						$("#sendWaiverModal").modal("hide");
						$('#loading-spinner').addClass('hide');
						$('input:checkbox[name="waiver_' + waiver_id + '_members"]').each(function () {
							$(this).prop('checked', false);
							$("#tr_background" + $(this).val()).removeClass('waiver-checked');
						});
						$('.select_all_waiver_members').prop('checked', false)
						location.reload();
					}, TWO_SEC)
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	sendWaiver = function (waiverId, teamId, eventId, e) {
		e.preventDefault();
		selected_waiver_members = []
		$('#waiver_sent_succ, #waiver_reminder_succ').removeClass('success');
		$('#waiver_sent_succ, #waiver_reminder_succ').text("");

		$('input:checkbox[name="waiver_' + waiver_id + '_members"]').each(function () {
			if (this.checked) {
				selected_waiver_members.push(this.value);
			}
		});

		if (selected_waiver_members.length > 0)
			$("#sendWaiverModal").modal("show");

		$('#teammemberwaiverId').val(waiverId);
		$('#teammemberteamId').val(teamId);
		$('#teammembereventId').val(eventId);
		$('#teammemberwaivername').val(waiverDetailWaiverName);
	};


	sendIndividualSendWaiver = function (memberId, waiver_id, teamId, eventId, e) {
		e.preventDefault();
		selected_waiver_members = []
		selected_waiver_members.push(memberId);
		if (selected_waiver_members.length > 0)
			$("#sendWaiverModal").modal("show");
		$('#teammemberwaiverId').val(waiver_id);
		$('#teammemberteamId').val(teamId);
		$('#teammembereventId').val(eventId);
		$('#teammemberwaivername').val(waiverDetailWaiverName);
	};

	var waiver_id;

	$('.waiverMembers').click(function (e) {
		//console("waivers")

		waiver_id = $(this).attr('id')

		$target = $('.waiver-info-memebers');

		$("#waiver_" + waiver_id).toggleClass('hide');
		//$("#arrow_"+waiver_id).toggleClass('ti-angle-down ti-angle-up');

		if ($("#arrow_" + waiver_id).hasClass('ti-angle-down')) {
			$(".arrow").removeClass('ti-angle-up')
			$(".arrow").addClass('ti-angle-down')

			$("#arrow_" + waiver_id).removeClass('ti-angle-down')
			$("#arrow_" + waiver_id).addClass('ti-angle-up')
		}
		else {
			$("#arrow_" + waiver_id).removeClass('ti-angle-up')
			$("#arrow_" + waiver_id).addClass('ti-angle-down')

			$(".arrow").removeClass('ti-angle-up')
			$(".arrow").addClass('ti-angle-down')

		}

		if (!$target.hasClass('hide')) {
			$("#waiver_" + waiver_id).toggleClass('hide').slideUp();
			$target.toggleClass('hide').slideDown();
		}

		/*if(!$target.hasClass('hide').parent()) {*/
		//$("#arrow_"+waiver_id).toggleClass('ti-angle-down ti-angle-up');	
		//}

		return false;
	});

	var team_waiver_id;

	$('.removeWaiver').click(function (e) {
		e.preventDefault();
		team_waiver_id = $(this).attr('id');
		//console(team_waiver_id)
		$('#waiver_removed_succ').removeClass('success');
		$('#waiver_removed_err').removeClass('error');
		$('#waiver_removed_succ, #waiver_removed_err').text('');
		$("#removeWaiverModal").modal("show");

	});

	$('#removeWaiverBtn').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');
		$('#waiver_removed_succ').removeClass('error');
		$('#waiver_removed_succ').removeClass('success');
		$("#waiver_removed_succ").text("");

		//console(team_waiver_id)
		if (team_waiver_id) {
			$.ajax({
				type: "POST",
				url: "/remove-team-waiver",
				data: { "team_waiver_id": team_waiver_id }
			}).done(function (msg) {
				////console(msg)
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$('#waiver_removed_succ').addClass('success');
					$('#waiver_removed_succ').text('Waiver removed successfully');
					setTimeout(function () {
						$("#removeWaiverModal").modal("hide");
						sessionStorage.setItem('activeTab', '#waiver')
						location.reload();
					}, THREE_SEC);

				}
				else {
					//console("Error")
					$('#loading-spinner').removeClass('hide');
				}

			});
		}
	});

	$('#addSponsor').click(function (eve) {
		eve.preventDefault()
		$('#ts_err_msg').removeClass('error');
		$("#ts_err_msg").text('');
		$("#uploadSponsorModalT").modal("show");
	});

	$("#teamSponsorUpload").change(function () {
		$('#ts_err_msg').removeClass('error');
		$("#ts_err_msg").text("");
		var ext = getFileExtension($('#teamSponsorUpload').val());
		if (validateExt(ext)) {
			$("#team_sponsor_upload").submit()
		}
		else {
			$('#ts_err_msg').addClass('error');
			$("#ts_err_msg").text("Image must be png, jpg, jpeg");
			return
		}

	});

	$("#team_sponsor_upload").on('submit', (function (e) {
		e.preventDefault();
		//$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		$('#ts_err_msg').removeClass('error');
		$("#ts_err_msg").text("");

		$.ajax({
			url: "/upload-team-sponsor",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data == "bad response") {
					//console(data)
					$('#ts_err_msg').addClass('error');
					$("#ts_err_msg").text("Bad Response. Try again");
					$('#loading-spinner').addClass('hide');
				}
				else {

					$("#uploadSponsorModalT .progress-bar").attr("aria-valuenow", 100);
					$("#uploadSponsorModalT .progress-bar").css("width", "100%");
					$("#uploadSponsorModalT .sr-only").text("100% Complete");
					setTimeout(function () {
						$("#uploadSponsorModalT").modal("hide");
						location.reload();
					}, TWO_SEC);
				}

			}
		});
	}));

	$('#removeSponsors').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');
		var sponsor_id = [];
		$('#rem_succ_msgs').removeClass('success');
		$("#rem_succ_msgs").text("");
		$('input:checkbox[name="sponsors_id[]"]').each(function () {
			if (this.checked) {
				sponsor_id.push(this.value);
			}
		});
		//console(sponsor_id)
		//console($("#remove_sponsors").serialize())
		if (sponsor_id.length > 0) {
			$.ajax({
				type: "POST",
				url: "/remove-team-sponsor",
				data: { "ids": sponsor_id }
			}).done(function (msg) {
				////console(msg)
				if (msg == "true") {
					location.reload();
				}
				else {
					//console("Error")
					$('#loading-spinner').removeClass('hide');
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	$('#select_all_sponsors').click(function () {
		var val = $(this).val()
		if (this.checked) {
			$('input:checkbox[name="sponsors_id[]"]').each(function () {
				$(this).prop('checked', true);
			});
		}
		else {
			$('input:checkbox[name="sponsors_id[]"]').each(function () {
				$(this).prop('checked', false);
			});
		}
	});

	$('#uploadTeamFiles').click(function (e) {
		e.preventDefault();
		$('#up_err_msg').removeClass('error');
		$("#up_err_msg").text('');
		$("#uploadTeamFilesModal").modal("show");
	});

	$('#teamUpload').change(function (event, template) {
		$('#up_err_msg').removeClass('error');
		$("#up_err_msg").text("");

		var ext = getFileExtension($('#teamUpload').val());
		//console.log($('#teamUpload'))
		if (validateExtTeamNotes(ext)) {
			$("#profile_image_upload").submit()
		}
		else {
			$('#up_err_msg').addClass('error');
			$("#up_err_msg").text("Please upload any of the following: pdf, jpg, png, doc, ppt, txt or svg");
			return
		}

		$("#team_notes_upload").submit()
	});

	$("#team_notes_upload").on('submit', (function (e) {
		e.preventDefault();
		//$('#loading-spinner').removeClass('hide');
		$('#up_err_msg').removeClass('error');
		$("#up_err_msg").text("");

		var formData = new FormData(this);

		$.ajax({
			url: "/upload-team-note",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data == "bad response") {
					//console(data)
					$('#up_err_msg').addClass('error');
					$("#up_err_msg").text("Bad Response. Try again");
					$('#loading-spinner').addClass('hide');
				}
				else {

					//console("Success")
					$("#uploadTeamFilesModal .progress-bar").attr("aria-valuenow", 100);
					$("#uploadTeamFilesModal .progress-bar").css("width", "100%");
					$("#uploadTeamFilesModal .sr-only").text("100% Complete");
					setTimeout(function () {
						$("#uploadTeamFilesModal").modal("hide");
						location.reload();
					}, TWO_SEC);
				}

			}
		});
	}));


	$('#delete_file').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var files = [];
		$('input:checkbox[name="team_files[]"]').each(function () {
			if (this.checked) {
				files.push(this.value);
			}
		});
		if (files.length > 0) {
			$.ajax({
				type: "POST",
				url: "/remove-team-note",
				data: { "ids": files }
			}).done(function (msg) {
				////console(msg)
				if (msg == "true") {
					location.reload();
				}
				else {
					$('#loading-spinner').addClass('hide');
					//console("Error")
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	$('#uploadTeamLinks').click(function (e) {
		e.preventDefault();
		$('#up_err_msg').removeClass('error');
		$("#up_err_msg").text('');
		$("#uploadTeamLinksModal").modal("show");
	});

	$('#add_links_submit').click(function (e) {
		e.preventDefault()
		$('#loading-spinner').removeClass('hide');

		var isFormValid = true;
		$("#team_links_upload input.required").each(function () {
			$(this).removeClass("empty-err");

			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				isFormValid = false;
				$('#loading-spinner').addClass('hide');
				$("#uploadTeamLinksModal").animate({ scrollTop: 0 }, "slow");
			} else if ($.trim($(this).val()).length > 0) {

				isUrlValidData = isUrlValid($.trim($(this).val()));

				if (isUrlValidData) {
					isFormValid = true;
				} else {
					$("#link").addClass("empty-err");
					$("#link_succ").text("Invalid Link");
					isFormValid = false;
					$('#loading-spinner').addClass('hide');
					$("#uploadTeamLinksModal").animate({ scrollTop: 0 }, "slow");
				}
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		if (isFormValid) {
			$.ajax({
				type: "POST",
				url: "/add-team-links",
				data: $("#team_links_upload").serialize()
			}).done(function (msg) {
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$("#link_succ").text("Links added successfully ");
					setTimeout(function () {
						//$("#uploadTeamLinksModal").modal("hide");
						location.reload(true);
					}, TWO_SEC);

				}
				else {
					$('#loading-spinner').addClass('hide');

				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
			return false
		}

	});

	$('#delete_link').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var links = [];
		$('input:checkbox[name="team_links[]"]').each(function () {
			if (this.checked) {
				links.push(this.value);
			}
		});

		if (links.length > 0) {
			$.ajax({
				type: "POST",
				url: "/remove-team-links",
				data: { "ids": links }
			}).done(function (msg) {

				if (msg == "true") {
					location.reload();
				}
				else {
					$('#loading-spinner').addClass('hide');

				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});

	var editor = new MediumEditor('.editable_team_description', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Description'
		}
	});

	if ($('#hidden_desc').length && $('#hidden_desc').val().trim().length > 0) {
		$('.editable.editable_team_description.form-control.medium-editor-element').focus();
		$('.editable.editable_team_description.form-control.medium-editor-element').html($('#hidden_desc').val())
		$("#team_description").val($('#hidden_desc').val())
	}


});

// js for event view
$(function () {

	$('#volunteer_request').click(function (eve) {

		if ($('#volunteer_msg').val().trim().length > 0) {
			eve.preventDefault()
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				url: "/volunteer-request",
				type: "POST",
				data: { "msg": $('#volunteer_msg').val().trim() },
				success: function (data) {
					if (data == "true") {
						$('#volunteerForm #rh_succ_msgs').addClass('success');
						$('#volunteerForm #rh_succ_msgs').text('You have successfully sent volunteer request to the event organizer(s)');
						$('#loading-spinner').addClass('hide');
					}
					else {
						$('#volunteerForm #rh_err_msgs').addClass('error');
						$('#volunteerForm #rh_err_msgs').text('Error sending request to the event organizer(s). Please try again later');
						$('#loading-spinner').addClass('hide');
					}

					setTimeout(function () {
						$("#VolunteerModal").modal("hide");
					}, THREE_SEC);

				}
			});
		}


	});

	$('.event-announcements a').attr("target", "_blank");
	$('.event-description a').attr("target", "_blank");


	$('#post-announcement').click(function (e) {
		$('#loading-spinner').removeClass('hide');
		var msg = $("#insertAnnouncement").serializeArray()[0].value;
		all_href = $(msg).find('a')
		for (var i = 0; i < all_href.length; i++) {
			//console($(all_href[i].outerHTML).attr("href"))
			old_href = 'href="' + $(all_href[i].outerHTML).attr("href") + '"'
			old_href = old_href.replace("https", "")
			old_href = old_href.replace("http", "")
			old_href = old_href.replace(":", "")
			old_href = old_href.replace("//", "")

			new_href = 'href="' + '//' + $(all_href[i].outerHTML).attr("href") + '"'
			//console(old_href, new_href)
			msg = msg.replace(old_href, new_href);
		};
		//console(msg)

		$.ajax({
			type: "POST",
			url: "/add-announcement",
			data: { "msg": msg }
		}).done(function (msg) {
			////console(msg)
			if (msg == "true") {
				location.reload();
			}
			else {
				$('#loading-spinner').addClass('hide');
				//console("Error")
			}

		});
	});

	$('#rehost').click(function (e) {
		e.preventDefault();
		$('#rh_err_msgs2').removeClass('error');
		$('#rh_succ_msgs2').removeClass('success');
		$("#rh_err_msgs2, rh_succ_msgs2").text("");
		$('#rehosting-view-button').removeAttr('disabled');
		$('#rehostingModal').modal('show');
	});

	$('#claim').click(function (e) {
		e.preventDefault();
		$('#claim_succ_msgs').removeClass('success');
		$('#claim_err_msgs').removeClass('error');
		$("#claim_succ_msgs, #claim_err_msgs").text("");
		$('#claimEventModal').modal('show');
	});

	$('#claim_event').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "POST",
			url: "/claim-event-request",
		}).done(function (msg) {
			if (msg == "true") {
				//location.reload();
				$('#claim_succ_msgs').addClass('success');
				$("#claim_succ_msgs").text("Your request has been sent to event organizer");
				$('#loading-spinner').addClass('hide');
			}
			else if (msg == "pending") {
				$('#claim_err_msgs').addClass('error');
				$("#claim_err_msgs").text("Your request is pending with event organizer");
				$('#loading-spinner').addClass('hide');
			}
			else if (msg == "declined") {
				$('#claim_err_msgs').addClass('error');
				$("#claim_err_msgs").text("Your request is declined by the event organizer");
				$('#loading-spinner').addClass('hide');
			}
			else {
				$('#claim_err_msgs').addClass('error');
				$("#claim_err_msgs").text("No event organizer for the event");
				$('#loading-spinner').addClass('hide');
			}

			setTimeout(function () {
				$("#claimEventModal").modal("hide");
			}, THREE_SEC);

		});

	});

	$('#single_day_event_rehost').change(function () {
		if ($('#single_day_event_rehost').is(":checked")) {
			$('#rh_end_date').val('');
			$('#rh_end_date').attr("disabled", "disabled");
		} else {
			$('#rh_end_date').removeAttr('disabled');
		}
	});

	$('#srh_dtpicker').click(function () {
		$('#rh_start_date').datepicker({
			autoclose: true,
			minDate: new Date(),
			onSelect: function (selected_date) {
				var selectedDate = new Date(selected_date);
				var msecsInADay = 86400000;
				var endDate = new Date(selectedDate.getTime() + msecsInADay);
				$("#rh_end_date").datepicker("option", "minDate", endDate);
			}
		}).focus();
	});

	$('#rh_start_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});

	$('#rh_start_date').focus(function () {
		$('#rh_start_date').datepicker({
			autoclose: true,
			minDate: new Date(),
			onSelect: function (selected_date) {
				var selectedDate = new Date(selected_date);
				var msecsInADay = 86400000;
				var endDate = new Date(selectedDate.getTime() + msecsInADay);
				$("#rh_end_date").datepicker("option", "minDate", endDate);
			}
		});
	});



	$('#erh_dtpicker').click(function () {
		$('#rh_end_date').datepicker({
			autoclose: true,
			startDate: '-0m',
			minDate: $("#rh_start_date").datepicker("getDate")
		}).focus();
	});

	$('#rh_end_date').focus(function () {
		$('#rh_end_date').datepicker({
			autoclose: true,
			startDate: '-0m',
			minDate: $("#rh_start_date").datepicker("getDate")
		});
	});

	$('#rh_end_date').keypress(function (e) {
		e.preventDefault();
		return false;
	});


	$('#event_version').change(function (e) {
		var event_slug = $('#event_version').val();
		//console(event_slug)

		window.location = window.location.origin + "/event-view/" + event_slug

	});

	$('#become_a_volunteer').click(function (eve) {

		$('#volunteer_msg').val('');
		$('#rh_succ_msgs').removeClass('success');
		$("#rh_succ_msgs").text("");

		$('#rh_err_msgs').removeClass('error');
		$("#rh_err_msgs").text("");

		$('#VolunteerModal').modal('show');

	});

	var editor = new MediumEditor('.editable_announcement_message', {
		toolbar: {
			buttons: [
				'bold',
				'italic',
				'underline',
				'justifyLeft',
				'justifyCenter',
				'justifyRight',
				'anchor',
				'orderedlist',
				'unorderedlist',
				'fontsize'
			]
		},
		paste: {
			forcePlainText: false,
			cleanPastedHTML: false,
			cleanReplacements: [],
			cleanAttrs: [],
			cleanTags: ['meta'],
			unwrapTags: []
		},
		placeholder: {
			text: 'Make an announcement'
		}
	});
})

// js for rehosting
$(function () {
	$("#rehostingDates button[type='submit']").click(function (e) {
		$('#rh_err_msgs2').removeClass('error');
		$("#rh_err_msgs2").text("");
		var rh_start_date = $('#rh_start_date').val();
		var rh_end_date = $('#rh_end_date').val();
		if (rh_start_date == "" || !rh_start_date) {
			$('#rh_err_msgs2').addClass('error');
			$("#rh_err_msgs2").text("Select the new event date");
			$('#rehosting-view-button').removeAttr('disabled');
			return false;
		}
		else {
			$("#rehostingDates").submit();
		}
	})
})

//js for waiver
$(function () {
	$('#medical_condition').click(function () {
		if ($('#medical_condition').is(":checked")) {
			$("#medical-condition-box").removeClass("waiver-hidden-box");
		} else {
			$("#medical-condition-box").addClass("waiver-hidden-box");
		}
	});

	$('#medical_condition_yes').click(function () {
		$("#medical-condition-box").removeClass("waiver-hidden-box");
	});

	$('#medical_condition_no').click(function () {
		$("#medical-condition-box").addClass("waiver-hidden-box");
	});

	$("#under18_signed_waiver_upload").on('submit', (function (e) {
		e.preventDefault();
		console.log("hi")
		$('#waiver_err_msg_upload').removeClass('error');
		$("#waiver_err_msg_upload").text("");
		$('#loading-spinner').removeClass('hide');
		var formData = new FormData(this);
		console.log(formData)
		var ext = $('#under18SignedWaiverUpload').val().split(".")[1];

		var medical_con = $("#medical_cond_under18").val()
		var medical_con_desc =$("#medical_cond_desc_under18").val()
		var contact_name = $("#contact_name_under18").val()
		var contact_number =$("#contact_number_under18").val()
		var waiver_id = $("#waiver_id_under18").val()
		var team_id =	$("#team_id_under18").val()
		var event_id =	$("#event_id_under18").val()
		var member_id = $("#member_id_under18").val()
		
	
		if ((ext == "pdf" || ext == "PDF")) {
			$.ajax({
				url: "/under-18-upload-signed-team-waiver",
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					if (data == "true") {
						//console("Success")
						setTimeout(function () {
						$('#loading-spinner').addClass('hide');
						$('#waiver_succ_msg_upload').addClass('success');
						$("#waiver_succ_msg_upload").text("Waiver saved successfully");
						$("#under18SignedWaiverUpload").modal("hide");
							location.reload();
						}, FIVE_SEC)
					}
				}
			});
		}
		else {
			$('#waiver_err_msg_upload').addClass('error');
			$("#waiver_err_msg_upload").text("Please upload a pdf file");
			$('#loading-spinner').addClass('hide');
			return
		}
	
	}));

	$("#under18SignedWaiverUpload").change(function () {
		$('#waiver_err_msg_upload').removeClass('error');
		$("#waiver_err_msg_upload").text("");
		var ext = $('#under18SignedWaiverUpload').val().split(".")[1];
		if ((ext == "pdf" || ext == "PDF")) {
			$("#uploadSignedWaiverUnder18FilesModal .progress-bar").attr("aria-valuenow", 100);
			$("#uploadSignedWaiverUnder18FilesModal .progress-bar").css("width", "100%");
			$("#uploadSignedWaiverUnder18FilesModal .sr-only").text("100% Complete");
			
		}
		else {
			$('#waiver_err_msg_upload').addClass('error');
			$("#waiver_err_msg_upload").text("Please upload a pdf file");
			return
		}

	});

	$("#accpetWaiverForm button[type='submit']").click(function (e) {
		e.preventDefault();
		var isFormValid = true
		$('#contact_number_err').addClass('hide');
		$('#contact_number_err').text('');
		$('#waiver_errors span').text('');
		$('#waiver_err_msg_upload').removeClass('error');
		$("#waiver_err_msg_upload").text("");

		$('#contact_name, #contact_number, #agree_checkbox').removeClass('empty-err');
		var agree = $('input[name="agree"]:checked').val();
		if ($('#medical_condition').is(":checked")) {
			var medical_condition_option = "yes";
		} else {
			var medical_condition_option = "no";
		}

		var medical_condition_desc = $('#medical_condition_desc').val();
		var contact_name = $('#contact_name').val();
		var contact_number = $('#contact_number').val();

		if (isNotEmpty(contact_name) == false) {
			$('#contact_name').addClass('empty-err');
			$('#waiver_errors').removeClass('hide');
			$('#waiver_errors span').text('Enter all the fields marked in *');
			isFormValid = false;
		}
		if (isNotEmpty(contact_number) == false) {
			$('#contact_number').addClass('empty-err');
			$('#waiver_errors').removeClass('hide');
			$('#waiver_errors span').text('Enter all the fields marked in *');
			isFormValid = false;
		}
		// if (isNotEmpty(agree) == false) {
		// 	$('#agree_checkbox').addClass('empty-err');
		// 	$('#waiver_errors').removeClass('hide');
		// 	$('#waiver_errors span').text('Enter all the fields marked in *');
		// 	isFormValid = false;
		// }

		var phone_regex = /^[0-9\s-]*$/;
		if (!phone_regex.test(contact_number)) {
			$('#contact_number_err').removeClass('hide');
			$('#contact_number_err').text('Enter contact number with numerics ');
			$('#contact_number_err').focus();
			isFormValid = false
		}

		if (isFormValid) {
			
			var checkUnder18 = $("#checkunder18").val()
		if (checkUnder18 == "true"){
			console.log("checkUnder18--",checkUnder18)
			$("#uploadSignedWaiverUnder18FilesModal .progress-bar").attr("aria-valuenow", 0);
			$("#uploadSignedWaiverUnder18FilesModal .progress-bar").css("width", 0);
			$("#uploadSignedWaiverUnder18FilesModal .sr-only").text("");
			$("#under18SignedWaiverUpload").val('')

	
			var med_con = $("#medical_condition").prop('checked')
			console.log(med_con)
			var med_desc = $("#medical_condition_desc").val()
			var con_name = $("#contact_name").val()
			var con_num = $("#contact_number").val() 
			var mem_id = $("#memberidunder18").val()

			$("#uploadSignedWaiverUnder18FilesModal").modal("show")

			$("#medical_cond_under18").val(med_con)
			$("#medical_cond_desc_under18").val(med_desc)
			$("#contact_name_under18").val(con_name)
			$("#contact_number_under18").val(con_num)
			$("#member_id_under18").val(mem_id)
			

			$("#waiver_id_under18").val($("#waiveridunder18").val())
			$("#team_id_under18").val($("#teamidunder18").val())
			$("#event_id_under18").val($("#eventidunder18").val())
			$("#event_id_under18").val($("#eventidunder18").val())
	
		}else{
			console.log("checkUnder18--",checkUnder18)
			$("#accpetWaiverForm").submit();
		
		}
	}
		

		
		/*$(window).scrollTop(0);
		$('.success-message').removeClass('hide');
		$(".success-message span").text("You have successfully accepted your waiver.");*/


	});

});

// js for paddlers
$(function () {
	$('.delete_user').click(function (e) {
		e.preventDefault();
		var user_id = this.id;
		var user_name = "";
		//console(user_id)

		if (user_id) {
			$.ajax({
				url: "/get-user-name",
				type: "POST",
				data: { "user_id": user_id },
				success: function (data) {
					//console("Success")
					user_name = data
					$('.deleteUsername').text(user_name);
					$("#deleteUserModal").modal("show");
					$('#deleteUserBtn').click(function () {
						$.ajax({
							url: "/delete-user",
							type: "POST",
							data: { "user_id": user_id },
							success: function (data) {
								if (data == "true") {
									/*$('#rem_succ_msgs').addClass('success');
									$("#rem_succ_msgs").text("Event organizer has been succesfully removed.");
									setTimeout(function(){
										$("#confirmDeleteModal").modal("hide");
									}, 5000);*/
									location.reload()
								}
								else {
									$("#deleteUserModal").modal("hide");
									$('.deleteUsername').text(user_name);
									$("#deleteConfirmationModal").modal("show");
								}

							}
						});
					});
				}
			});

		}
		else {
			//console("Select a user")
		}
	})
});

function initMap(lat, lng) {
	//console(lat, lng)
	var uluru = { lat: lat, lng: lng };
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: uluru,
		disableDefaultUI: true
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}
$(function () {
	displayPracticeStatus = function (practice_id, status, obj, practice_date, practice_time) {

		if (status != "NoResponse" || $.trim(obj.text()) != "Set Availability") {
			$.ajax({
				type: "GET",
				url: "/get-practice-status",
				data: { "practice_id": practice_id }
			}).done(function (pstatus) {
				var tpStatus = $.parseJSON(pstatus)
				obj.parent().find('input[name="response"]:radio').each(function () {
					if ($(this).val() == tpStatus.response) {
						$(this).iCheck('check');
					}
				});
				if (tpStatus.response == "not attending" || tpStatus.response == "maybe") {
					obj.parent().find('div.availability-note').removeClass('hide');
					obj.parent().find('textarea[name="reason"]').val(tpStatus.reason)
				} else {
					obj.parent().find('div.availability-note').addClass('hide');
				}
			});
		} else {
			obj.parent().find('div.availability-note').addClass('hide');
			obj.parent().find('input[name="response"][value="attending"]:radio').iCheck('check');
		}
	};


	function waivernamecompare(a, b) {
		if (a.user_name && b.user_name)
		{
		if (a.user_name.toLowerCase() < b.user_name.toLowerCase())
			return -1;
		if (a.user_name.toLowerCase() > b.user_name.toLowerCase())
			return 1;
		}
		return 0;
	}


	individualWaiver = function (waiverId, teamId, eventId, waiver_name) {
		waiverDetailWaiverName = waiver_name
		//.var tempwaiver_name= rooster_Data[0].rosterid +',"duplicate",event'
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "GET",
			url: "/view-individual-Wavier-Details",
			data: { "waiverId": waiverId, "teamId": teamId, "eventId": eventId }
		}).done(function (waiverData) {
			var tempWaiverData = JSON.stringify(waiverData)
			var tempWaiverJsonData = JSON.parse(tempWaiverData)
			if (!tempWaiverJsonData.hasOwnProperty("status")) {
				var html = '';
				waiverCoCaptains = $("#waiverCoCaptains").val()
				var coCaptainData = waiverCoCaptains.split(',');
				waiverTeamCaptainId = $("#waiverTeamCaptainId").val()
				var event_reg_id = ""
				html += "<td class='waiver-btns-col' colspan=4>" +
					"<div class='row'>" +
					"<div class='col-md-12'>" +
					"<a href='#' onClick=sendWaiver(" + waiverId + "," + teamId + "," + eventId + ",event) class='btn waiver-info-btn sendWaiver pull-right' id=" + waiverId + ">Send Waiver Request/Reminder</a>" +
					"</div></div>" +
					"	<table id='waivertab' class='table inner-table dashboard-table dasboard-team-waiver-info-table responsive-table'>" +
					"<thead>" +
					"<tr>" +
					"<th style='width: 30px;'>" +
					"<div class='md-checkbox'>" +
					"<input id='selectAllWaiverMembers_" + waiverId + "' class='md-check select_all_waiver_members' type='checkbox' value=" + waiverId + ">" +
					"<label for='selectAllWaiverMembers_" + waiverId + "'>" +
					"<span class='check'></span><span class='box'></span>" +
					"</label></div></th>" +
					"<th class='col-md-4'>NAME</th>" +
					"<th class='col-md-2'>LAST REQUESTED <a class='advanced-waiver-toggle waiverMembersInfoSent'></a></th>" +
					"<th class='col-md-2' style='text-align: center;'>ACCEPTED ONLINE <a class='advanced-waiver-toggle waiverMembersInfoSigned'></a></th>" +
					"<th class='col-md-2'>CONTACT</th>" +
					"<th class='col-md-2'>PHONE</th>" +
					"<th class='col-md-2'>MEDICAL CONDITION</th>" +
					"<th class='col-md-2'>HARD COPY<br> <span style='font-size: 11px;text-transform: capitalize;'class='color'>(Under 18)</span></th>" +
					"</tr></thead><tbody>";
				var arr = Object.keys(tempWaiverJsonData).map(function (k) { return tempWaiverJsonData[k] });
				arr.sort(waivernamecompare);
				
				$.each(arr, function (idx, obj) {
					console.log(obj)
					console.log(idx)
					if(obj.signed_date){
						html += "<tr class = 'grayout' id='tr_background" + obj.fk_member_id + "'><td><div class='md-checkbox'>";

					}else if (obj.waiver_upload_date ){
						html += "<tr class = 'grayout' id='tr_background" + obj.fk_member_id + "'><td><div class='md-checkbox'>";

					}else{
						html += "<tr id='tr_background" + obj.fk_member_id + "'><td><div class='md-checkbox'>";

					}

					if (obj.signed_date) {
						html += "<input id=" + obj.fk_member_id + "_" + waiverId + " name='waiver_" + waiverId + "_members' value=" + obj.fk_member_id + " class='md-check manage-team-checkbox waiver_members_../_id' type='checkbox' disabled>";
					} else {
						html += "<input id=" + obj.fk_member_id + "_" + waiverId + " name='waiver_" + waiverId + "_members' value=" + obj.fk_member_id + " class='select_individual_waiver_members md-check manage-team-checkbox waiver_members_../_id' type='checkbox'>";
					}

					if(obj.signed_date){
						html += "</td>";
					}else if (obj.waiver_upload_date ){
						html += "</td>";
					}else{
						html += "<label for=" + obj.fk_member_id + "_" + waiverId + ">" +
						"<span class='check'></span>" +
						"<span class='box'></span>&nbsp;" +
						"</label></div></td>";
					}
				
					if (obj.user_name) {
						html += "<td><div class='dasboard-team-member-image team-waiver-name'>";
						if (obj.files != "" && obj.files) {
							html += "<img src='" + obj.files + "' class='img-circle'></div>";
						} else {
							html += "<img src='/static/images/user-default.png' class='img-circle'></div>";
						}
						html += "<div class='dasboard-team-member-content'><div class='team-member-name'><p class='tm-list-title tm-list-title-overwrite'>" + obj.user_name + "</p><p>" + obj.classes + "," + obj.age;

						var index = jQuery.inArray(obj.fk_member_id, coCaptainData)
						if (index != -1) {
							if (obj.fk_member_id == waiverTeamCaptainId) {
								html += "<span class='label-orange label-orange-overwrite'>Captain</span></p></div></div></td>";
							} else {
								html += "<span class='label-gray label-orange-overwrite'>Captain</span></p></div></div></td>";
							}
						} else {
							html += "</p></div></div></td>";
						}
					} else {
						html += "<td></td>";
					}
					if (obj.send_date) {
						html += "<td>" + obj.send_date + "</td>";
					} else {
						html += "<td></td>";
					}
					if (obj.signed_date) {
						html += "<td>" + obj.signed_date + "</td>";
					} else {
						html += "<td></td>";
					}
					if (obj.contact_name) {
						html += "<td>" + obj.contact_name + "</td>";
					} else {
						html += "<td></td>";
					}
					if (obj.contact_number) {
						html += "<td>" + obj.contact_number + "</td>";
					} else {
						html += "<td></td>";
					}
					html += "<td class='text-center'>";
					if (obj.medical_condition_desc) {
						html += "<div class='note-tooltip'><i class='ti-notepad'></i><div class='class-tooltip waiver-medicalcondition'>" + obj.medical_condition_desc + "</div></div></td>";
					}
					html += "</td>";

					if(obj.signed_date){
						html += "<td></td>"
					}
					else if (obj.waiver_upload_date ){

						
						html +="<td>"+"<a value='/view-uploaded-signed-team-waiver/"+obj.fk_member_id+"/"+waiverId+"/"+teamId+"/"+eventId+"' name='waivers-team-paris' href='#/' target='' class='btn btn-thin paybutton view_uploaded_waiver' style='min-width: 20px;font-size: 13px;'>View Waiver</a>"+
						"<p style='margin: 0px;font-size: 13px;'>"+obj.waiver_upload_date+"</p>"+"</td>"
						"</td>"
						// 	html +="<td>"+"<button class='view_uploaded_waiver' type='button' style='font-size:13px;' value='/view-uploaded-signed-team-waiver/"+obj.fk_member_id+","+waiverId+","+teamId+","+eventId+"'>View Waiver</button>"+
					// "<p style='margin: 0px;font-size: 13px;padding-left:5px;'>"+obj.waiver_upload_date+"</p>"+
					// "</td>"
					
				}
					else{
						if (obj.send_date) {
							html +="<td>"+
							"<div class='upload uploadWaiverForTeamMembers' style='padding: 5px;' value='"+obj.fk_member_id+","+waiverId+","+teamId+","+eventId+","+event_reg_id+"'>"+
							"<img src='/static/images/upload.png' width='19px' height='18px'>"+
							"<a class='upload_gray'>Upload </a>"+
							  "</div>"
							"</td>"
						}else{
							html +="<td></td>"
						}
						
					}
				
					// if (!obj.signed_date){
					// html +=	"<td><button type='button' class='btn btn-default buttonpopover' data-container='body' data-html='true' data-toggle='popover'"+
					// " data-trigger='focus' data-placement='left' data-content='<ul class=popover-waiver><li><a style=color:black; href=# onclick=sendIndividualSendWaiver("+obj.fk_member_id+","+waiverId+","+teamId+","+eventId+",event)>Send Waiver Reminder</a></li></ul>'>"+
					// "<span class='glyphicon glyphicon-option-horizontal' aria-hidden='true'></span></button>"+
					// "	</td></tr>";
					// }else {
					html += "</tr>";
					// }
				});
				html += "</tbody></table></td>";
				$('#waivermemberappend').empty().append(html);
				$('#waivermemberappend').removeClass('hide')
				$('#waivertab').addClass('hide')
				$('#uploadWaiverFiles').addClass('hide')
				$('#waiverback').removeClass('hide')
				$('#waiverhed').removeClass('hide')
				$('#waiverhed').text(waiver_name)
				$('[data-toggle="popover"]').popover()
			}


			$('#loading-spinner').addClass('hide');
		});
	};
	
	$(document).on('click', ".editIndividualUser", function (e) {
		e.preventDefault();
		var memberId = $(this).attr('id');
		$('.editDialNumber-err').addClass('hide');
		$('.dialSelected').removeClass('empty-err');
		$('#editMemberWeight').removeClass('empty-err');
		$('.editPhone_err').addClass('hide');
		$('#editPhoneNumber').removeClass('empty-err');
		if (memberId) {
			//Get the individual user detail
			$.ajax({
				url: "/get-user-detail",
				type: "POST",
				data: { "user_id": memberId },
				success: function (data) {
					if (data) {
						userData = JSON.parse(data)
						if (userData.length > 0) {
							console.log(userData[0])
							$("#userDetailEditableModel").modal("show");
							$('#editMemberName').val(userData[0].name);
							$('#editPhoneNumber').val(userData[0].phone);
							$('#editMemberWeight').val(userData[0].weight);
							if (userData[0].paddling_side) {
								$('.sideSelected').val(userData[0].paddling_side);
							} else {
								$('.sideSelected').val("");
							}

							if (userData[0].skill_level) {
								$('.skillSelected').val(userData[0].skill_level);
							} else {
								$('.skillSelected').val("");
							}

							if (userData[0].country_dial_code && userData[0].dial_county_code) {

								//$('.dialSelected').val(userData[0].country_dial_code);

								var $select = $('.dialSelected');
								$select.children().filter(function () {
									return this.text == userData[0].dial_county_code + " " + userData[0].country_dial_code;
								}).prop('selected', true);
								$("#dial_code_county_code").val(userData[0].dial_county_code)
							} else {
								$('.dialSelected').val("");
								$("#dial_code_county_code").val("")
							}
							$('#editMemberId').val(memberId)
						}
					}
				}
			});
		}
	});
	$('#editUserSave').click(function (e) {
		e.preventDefault()
		var validDialCode = false;
		var validPhoneCode = false;
		var validUserWeight = false;

		$('.editPhone_err').addClass('hide');
		$('#editPhoneNumber').removeClass('empty-err');
		$('.editDialNumber-err').addClass('hide');
		$('.dialSelected').removeClass('empty-err');

		if (isNotEmpty($(".dialSelected option:selected").val()) == false && isNotEmpty($("#editPhoneNumber").val()) == true) {
			$('.editDialNumber-err').removeClass('hide');
			$('.dialSelected').addClass('empty-err');
			validDialCode = false;
		} else {
			if (isNotEmpty($(".dialSelected option:selected").val()) == true && isNotEmpty($("#editPhoneNumber").val()) == true) {
				validDialCode = true;
				$('.editDialNumber-err').addClass('hide');
				$('.dialSelected').removeClass('empty-err');

				var seletedcountrycode = $(".dialSelected option:selected").text();
				var indexValue = seletedcountrycode.indexOf("+");
				if (indexValue != -1) {
					$("#dial_code_county_code").val(seletedcountrycode.substring(0, indexValue).trim())
				} else {
					validDialCode = false;
				}
			} else {
				validDialCode = true;
			}
		}

		if (isNotEmpty($(".dialSelected").val()) == true && isNotEmpty($("#editPhoneNumber").val()) == false) {
			validPhoneCode = false;
			emptyOrWroungPhoneNumber()
		} else {
			if (isNotEmpty($("#editPhoneNumber").val()) == true && isNotEmpty($(".dialSelected").val()) == true) {
				var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				var phone = $("#editPhoneNumber").val()
				if (phoneRegex.test(phone)) {
					if (phone.indexOf('-') == -1 && phone.indexOf('.') == -1 || phone.indexOf('-') > 0 && phone.indexOf('.') > 0) {
						var formattedPhoneNumber =
							phone.replace(phoneRegex, "$1-$2-$3");
						$('#editPhoneNumber').val(formattedPhoneNumber);
					}
					validPhoneCode = true;
					$('.editPhone_err').addClass('hide');
					$('#editPhoneNumber').removeClass('empty-err');
				} else {
					validPhoneCode = false;
					emptyOrWroungPhoneNumber()
				}
			} else {
				validPhoneCode = true;
			}
		}

		if ($("#editMemberWeight").val() >= 0) {
			validUserWeight = true
			$('#editMemberWeight').removeClass('empty-err');
		} else {
			$('#editMemberWeight').addClass('empty-err');
		}

		if (validDialCode && validPhoneCode && validUserWeight) {
			var memberId = $('#editMemberId').val()
			team_members_update.push(+memberId + "#"
				+ $(".sideSelected option:selected").val() + "#"
				+ $(".skillSelected option:selected").val() + "#"
				+ $("#editMemberWeight").val() + "#"
				+ $(".dialSelected option:selected").val() + "#"
				+ $("#editPhoneNumber").val() + "#"
				+ $("#dial_code_county_code").val());

			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/save-team-member",
				data: { "team_members_update": team_members_update }
			}).done(function (msg) {
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$('#rem_succ_msgs_status').removeClass('hide');
					$('#rem_err_msgs_status').addClass('hide');
					$(".sideAppend_" + memberId).text($(".sideSelected option:selected").val())
					$(".levelAppend_" + memberId).text($(".skillSelected option:selected").val())
					if ($("#editMemberWeight").val() != 0) {
						$(".weightAppend_" + memberId).text($("#editMemberWeight").val() + " lbs")
					} else {
						$(".weightAppend_" + memberId).text("")
					}
					$("#userDetailEditableModel").animate({ scrollTop: 0 }, "slow");
					setTimeout(function () {
						$("#userDetailEditableModel").modal("hide");
						$('#rem_succ_msgs_status').addClass('hide');
						$('#rem_err_msgs_status').addClass('hide');
						location.reload()
						sessionStorage.setItem("activeTab", "#members");
					}, TWO_SEC);
				}
				else {
					$('#rem_succ_msgs_status').addClass('hide');
					$('#rem_err_msgs_status').removeClass('hide');
					$('#loading-spinner').addClass('hide');
				}
			});
		}
	});

	emptyOrWroungPhoneNumber = function () {
		$('#editPhoneNumber').addClass('empty-err');
		$('.editPhone_err').removeClass('hide');
		$(".editPhone_err").html("Invalid phone number<br> (ex 443-979-8200 or 443.979.8200)");
		return false;
	};

	// roster tab new roster button action script
	$(document).on('click', "#newroster", function (eve) {
		eve.preventDefault();
		$(".set_default_role").val('paddler').trigger('change');
-		//$(".show_roster_comment").css("display", "");
-		//$("#togglerostercommenticon").attr('src',"/static/images/arrow_up.png")	

		rosterCreateIntialize("rosterpage")
		// $('.img-roster-mem-role').addClass('hide')
		// $(".resetupdate_select").removeClass('empty-err');
		// $('.resetupdate_select').prop('selectedIndex', 0);

	
		$(".img-roster-mem-roles").css("border", "solid 1px #ced1d0");
		$(".img-roster-mem-roles").removeClass("isSelected");
		$(".img_paddler").css("border", "solid 3px #2a7560");
		$(".img_paddler").addClass("isSelected");

		$("#rosterRoles").select2({width: 'select2_roster_width'}); 
  	$(".js-example-basic-multiple").select2({
  			placeholder: "Select a role"
  	});

	});

	$(document).on('click', "#new_lineup_roster", function (eve) {
		eve.preventDefault();
		$(".set_default_role").val('paddler').trigger('change');
		rosterCreateIntialize("lineuppage")
		//$(".show_roster_comment").css("display", "");
  //	$("#togglerostercommenticon").attr('src',"/static/images/arrow_up.png")	
		// $('.img-roster-mem-role').addClass('hide')
		// $(".resetupdate_select").removeClass('empty-err');
		// $('.resetupdate_select').prop('selectedIndex', 0);

	
		$(".img-roster-mem-roles").css("border", "solid 1px #ced1d0");
		$(".img-roster-mem-roles").removeClass("isSelected");
		$(".img_paddler").css("border", "solid 3px #2a7560");
		$(".img_paddler").addClass("isSelected");
	});

	function rosterCreateIntialize(pageOption) {
		localStorage.removeItem("rosteroption")
		localStorage.removeItem("rosterpracticeid")
		localStorage.removeItem("rostereventname")
		localStorage.removeItem("rostereventid")
		localStorage.removeItem("rostertype")
		localStorage.removeItem("rosterComment")
		localStorage.removeItem("team_save_draft_roster")
		$('#rosteraddeditForm').find('input, textarea, button, select').attr('disabled', false);

		$('#viewRosterEdit').addClass('hide')
		$("#roaster_msg_err_msgs").text("")
		$("#roaster_msg_succ_msgs").text("")
		$("#newRosterModel").modal("show");
		$("#defaultHeading").removeClass('hide');
		$("#dynamicHeading").addClass('hide');
		$("#rooster_Event").addClass('hide')
		$("#rooster_Practice").addClass('hide')
		$("#roster_members_list").addClass('hide');
		$("#save-roster-button").addClass('hide');
		$("#edit-save-roster-button").addClass('hide');
		$("#rostercomment").addClass('hide');
		$("#team_save_draft_roster_hide").addClass('hide');
		$('input[name="rosterType"]').prop('checked', false);
		$('#rosterName').val('')
		$('#rostercommentDetail').val('')
		$("#rosterPracticeId").val("0");
		$("#rosterEventId").val("0");
		$("#searchrosterEventId").val("")
		$("#roasterUpdateId").val('')
		$('input:checkbox[name="team_roster_members[]"]').each(function () {
			if (this.checked) {
				$(this).prop('checked', false);
			}
		});
		$('#select_all_roster_members').prop('checked', false);
		$(".iconhide").each(function () {
			$(this).addClass('hide');
		})

		if (pageOption == "lineuppage") {
			$('.changebuttonnameroster').removeClass("save-roster-detail").addClass("save-lineup-roster-detail")
		} else {
			$('.changebuttonnameroster').addClass("save-roster-detail").removeClass("save-lineup-roster-detail")
		}
	}

	//create roster radio button change event
	$('input[type=radio][name=rosterType]').change(function () {
		$("#rostercommentDetail").val('');
		$('#team_save_draft_roster').prop('checked', false);
		if (this.value == 'Event') {
			$("#rooster_Event").removeClass('hide')
			$("#rooster_Practice").addClass('hide')
			//$("#save-roster-button").removeClass('hide');
			$("#rostercomment").removeClass('hide');
			$("#team_save_draft_roster_hide").removeClass('hide');
			$("#rosterPracticeId").val("0");
			$("#roster_members_list").removeClass('hide')
			if (localStorage.getItem("rosteroption")) {
				$('#searchrosterEventId').val(localStorage.getItem("rostereventname"));
				$('#rosterEventId').val(localStorage.getItem("rostereventid"));
			}

			if (!localStorage.getItem("rosteroption")) {
				resetTeamMemberCheckBox()
				$("#save-roster-button").removeClass('hide');
			} else {
				if (localStorage.getItem("rosteroption") == "duplicate") {
					$("#save-roster-button").removeClass('hide');
				} else {
					$("#save-roster-button").addClass('hide');
				}
			}

			$('.roster-member-li').each(function () {
				$(this).removeAttr("style")
			});
			$('#loading-spinner').addClass('hide');
		}
		else if (this.value == 'Practice') {
			$("#rooster_Practice").removeClass('hide')
			$("#rooster_Event").addClass('hide')
			$("#rostercomment").removeClass('hide');
			$("#team_save_draft_roster_hide").removeClass('hide');
			$("#rosterEventId").val("0");
			$("#searchrosterEventId").val("")
			if (!localStorage.getItem("rosteroption")) {
				resetTeamMemberCheckBox()
				$("#save-roster-button").removeClass('hide');
				getUpComingEventOrPracticeList('practice', "rosterPracticeId", "")
			} else {
				$('#loading-spinner').removeClass('hide');
				if (localStorage.getItem("rosteroption") == "duplicate") {
					$("#save-roster-button").removeClass('hide');
				} else {
					$("#save-roster-button").addClass('hide');
				}
				rosterpracticeiddata = localStorage.getItem("rosterpracticeid")
				//localStorage.setItem("rostereventid",rooster_Data[0].fk_event_id)
				if (localStorage.getItem("rostertype") != "Practice") {
					getUpComingEventOrPracticeList('practice', "rosterPracticeId", "")
					$("#rosterPracticeId").val(Number(rosterpracticeiddata));
				} else {
					getUpComingEventOrPracticeList('practice', "rosterPracticeId", "")
				}
			}
			$("#roster_members_list").removeClass('hide')
			$('#loading-spinner').addClass('hide');
		} else if (this.value == 'N/A') {
			$('#loading-spinner').removeClass('hide');
			$("#rooster_Event").addClass('hide')
			$("#rooster_Practice").addClass('hide')
			$("#rostercomment").removeClass('hide');
			$("#team_save_draft_roster_hide").removeClass('hide');
			$("#roster_members_list").removeClass('hide')
			if (!localStorage.getItem("rosteroption")) {
				resetTeamMemberCheckBox()
				$("#save-roster-button").removeClass('hide');
			} else {
				if (localStorage.getItem("rosteroption") == "duplicate") {
					$("#save-roster-button").removeClass('hide');
				} else {
					$("#save-roster-button").addClass('hide');
				}
			}
			$("#rosterPracticeId").val("0");
			$("#rosterEventId").val("0");
			$("#searchrosterEventId").val("")
			$('.roster-member-li').each(function () {
				$(this).removeAttr("style")
			});
			$('#loading-spinner').addClass('hide');
		}
		if (localStorage.getItem("rostertype") == this.value) {
			$("#rostercommentDetail").val(localStorage.getItem("rosterComment"))
			if (localStorage.getItem("team_save_draft_roster") == "0") {
				$('#team_save_draft_roster').prop('checked', false);
			} else if (localStorage.getItem("team_save_draft_roster") == "1") {
				$('#team_save_draft_roster').prop('checked', true);
			}
		}
	});

	function resetTeamMemberCheckBox() {
		$('input:checkbox[name="team_roster_members[]"]').each(function () {
			if (this.checked) {
				$(this).prop('checked', false);
			}
		});
	}

	// get the upcoming practice are register future practice of each team
	getUpComingEventOrPracticeList = function (optionType, optionId, rosteractionOption) {
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			url: "/get-register-event-created-practice",
			type: "GET",
			data: { "optionType": optionType },
			success: function (data) {
				tempdata = JSON.stringify(data)
				if (tempdata != "null") {
					practice_Event_Data = JSON.parse(tempdata)
					if (practice_Event_Data.length > 0) {
						var options = '';
						options += '<option value="0">Please Select Practice</option>';
						for (var i = 0; i < practice_Event_Data.length; i++) {
							options += '<option value="' + practice_Event_Data[i].id + '">' + practice_Event_Data[i].practicedate_location + '</option>';
						}
						$("select#" + optionId).html(options);
					}
				} else {
					var options = '';
					options += '<option value="0">Please Select Practice</option>';
					$("select#" + optionId).html(options);
				}

				if (localStorage.getItem("rostertype") == "Practice" || rosteractionOption == "editroster") {
					rosterpracticeiddata = localStorage.getItem("rosterpracticeid")
					$.ajax({
						url: "/get-practice-attedence-detail",
						type: "GET",
						data: { "practice_id": Number(rosterpracticeiddata) },
						success: function (attendencedata) {
							attendendeMarking(attendencedata)
							$("#rosterPracticeId").val(Number(rosterpracticeiddata));
							$('#loading-spinner').addClass('hide');
						}
					});
				} else {
					$('#loading-spinner').addClass('hide');
				}
			}
		});
	}

	// This script get the all the attendence response of single practiceid
	$("#rosterPracticeId").change(function () {
		practice_id = this.value
		$('#loading-spinner').removeClass('hide');
		resetTeamMemberCheckBox()
		$.ajax({
			url: "/get-practice-attedence-detail",
			type: "GET",
			data: { "practice_id": practice_id },
			success: function (attendencedata) {
				attendendeMarking(attendencedata)
			}
		});
		$("#roster_members_list").removeClass('hide')
	});


	function attendendeMarking(attendencedata) {
		$(".iconhide").each(function () {
			$(this).addClass('hide');
		})
		tempattendencedata = JSON.stringify(attendencedata)
		if (tempattendencedata != "null") {
			attendence_DataValue = JSON.parse(tempattendencedata)
			if (attendence_DataValue.all) {
				if (attendence_DataValue.all.length > 0) {

					for (var i = 0; i < attendence_DataValue.all.length; i++) {
						console.log(attendence_DataValue.all[i])
						if (attendence_DataValue.all[i].response == "maybe") {
							$("#bg_mayat" + attendence_DataValue.all[i].fk_user_id).removeClass('hide');
						} else if (attendence_DataValue.all[i].response == "not attending") {
							$("#bg_notat" + attendence_DataValue.all[i].fk_user_id).removeClass('hide');
						} else if (attendence_DataValue.all[i].response == "attending") {
							$("#bg_at" + attendence_DataValue.all[i].fk_user_id).removeClass('hide');
							$("#mem_ros_" + attendence_DataValue.all[i].fk_user_id).prop('checked', true);
						}
					}
					$('#loading-spinner').addClass('hide');
				}
			} else {
				$('#loading-spinner').addClass('hide');
			}
		}
	}

	// save new roster detail.
	$(document).on('click', ".save-lineup-roster-detail", function (eve) {
		$("#roaster_msg_err_msgs").text("")
		$("#roaster_msg_succ_msgs").text("")

		eve.preventDefault()
		rosterName = "false"
		isFormValid = true;
		if ($("#rosterName").val().trim().length === 0) {
			$("#rosterName").addClass('empty-err');
			isFormValid = false;
		} else {
			isFormValid = true;
			$("#rosterName").removeClass('empty-err');
		}

		var team_roster_members_role=[];
		var selectedValues
		var flag="true";

		var rosterSelectedPeople = [];
		var rosterSelectedPeopleRole=[];

		$('input:checkbox[name="team_roster_members[]"]').each(function () {
				if (this.checked) {
					selectedValues = $('.select_role_'+ this.value).val().join("_");
					console.log("selectedValues--"+selectedValues)
				
					if (selectedValues == '') {
						flag="false";
					} 
				if ( flag == 'true') {
					$("#roaster_msg_err_msgs").text("")
						console.log("checked--"+this.value)
						 team_roster_members_role.push(selectedValues);	
					}else{
						$('#loading-spinner').addClass('hide');
					$("#roaster_msg_err_msgs").text("Please select role(s) for roster members")
					$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
						isFormValid = false;
					}
				}
		});
		$('#rosterMemberRoles').val(team_roster_members_role)
		if (isFormValid) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "GET",
				url: "/check-roster-name",
				data: { "rosterName": $("#rosterName").val().trim() },
			}).done(function (msg) {
				if (msg == "true") {
					rosterName = msg;
					$("#roaster_msg_err_msgs").text("Roster name already exists")
					$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
					$('#loading-spinner').addClass('hide');
					return
				} else {
					$.ajax({
						type: "POST",
						url: "/add-edit-roster",
						data: $("#rosteraddeditForm").serialize()
					}).done(function (msg) {
						if (msg) {
							$('#loading-spinner').addClass('hide');
							$("#roaster_msg_succ_msgs").text("Roster saved successfully")
							$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
							setTimeout(function () {
								sessionStorage.setItem('activeTab', '#lineup')
								localStorage.setItem('lineUpCreate', 'true')
								location.reload()
							}, TWO_SEC);
						}
					});
				}
			});
		} else {
			$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
		}
	});

	$(document).on('click', ".img-roster-mem-roles", function (eve) {


		var value = $(this).attr('value');
		console.log("if this-", value)
		var title = $(this).attr('data-original-title');

		if (title === "paddler") {
			console.log("title* -", title)
				
				$("#paddler_id_" + value).css("border", "solid 3px #2a7560");
				$("#paddler_id_" + value).addClass("isSelected");
				
		}
		else {

				$("#paddler_id_" + value).css("border", "solid 1px #ced1d0");
				$("#paddler_id_" + value).removeClass("isSelected");
		}
		if (title === "drummer") {
			console.log("title* -", title)

				$("#drummer_id_" + value).css("border", "solid 3px #2a7560");
				$("#drummer_id_" + value).addClass("isSelected");
			
		}
		else {
			$("#drummer_id_" + value).css("border", "solid 1px #ced1d0");
			$("#drummer_id_" + value).removeClass("isSelected");
		}
		 if (title === "captain") {
			console.log("title* -", title)

				$("#captain_id_" + value).css("border", "solid 3px #2a7560");
				$("#captain_id_" + value).addClass("isSelected");
			
		}
		else {
			$("#captain_id_" + value).css("border", "solid 1px #ced1d0");
				$("#captain_id_" + value).removeClass("isSelected");
				
		}
		 if (title === "coach") {
			console.log("title* -", title)

			
				$("#coach_id_" + value).css("border", "solid  3px #2a7560");
				$("#coach_id_" + value).addClass("isSelected");
			
		}
		else {
			$("#coach_id_" + value).css("border", "solid  1px #ced1d0");
				$("#coach_id_" + value).removeClass("isSelected");
		}

		if (title === "official") {
			console.log("title* -", title)
			
				$("#official_id_" + value).css("border", "solid 3px #2a7560");
				$("#official_id_" + value).addClass("isSelected");
		}
		else {
				$("#official_id_" + value).css("border", "solid 1px #ced1d0");
				$("#official_id_" + value).removeClass("isSelected");
		}
		if (title === "other") {
			console.log("title* -", title)
			
				$("#other_id_" + value).css("border", "solid 3px #2a7560");
				$("#other_id_" + value).addClass("isSelected");
		}
		else {
				$("#other_id_" + value).css("border", "solid 1px #ced1d0");
				$("#other_id_" + value).removeClass("isSelected");
		}

	});

	$(document).on('click', "#eventteamname", function (e) {
		e.preventDefault()
		eventactiveTab = sessionStorage.getItem('eventactiveTab')
		
		if ($(this).attr('value') == "#eventTeamName") {
			sessionStorage.setItem('eventactiveTab', '#eventTeamList')
			window.location.href = "/event-view/" + $('#event_slug_hidden').attr('value') + "/event-teams-view";
		}
		
	});
	$(document).on('click', "#eventname", function (e) {
		e.preventDefault()
		
		if ($(this).attr('value') == "#eventName") {
			
			window.location.href = "/event-view/" + $('#event_slug_hidden').attr('value') + "/event-teams-view";
		}
	});
	$(document).on('click', "#eventregister", function (e) {
		e.preventDefault()
		
		if ($(this).attr('value') == "#eventRegister") {
			
		window.location.href = "/team-view/" + $('#team_slug_hidden').attr('value') + "/event_register";
		//console.log("/event-view/" + $('#slug_hidden').attr('value') + "/event_register")
		}
	});

	$(document).on('click', "#toggleRosterComment", function (e) {
			e.preventDefault();
				tempId1 = document.getElementsByClassName("show_roster_comment")
		
				if(tempId1[0].style.display == "none"){
					$(".show_roster_comment").toggle();		
					$("#togglerostercommenticon").attr('src',"/static/images/arrow_up.png")
				}
				else {
					$(".show_roster_comment").toggle();		
					$("#togglerostercommenticon").attr('src',"/static/images/arrow_down.png")		
				} 
	});


	$(document).on('click', "#fromeventRegister", function (e) {
		//e.preventDefault()
		if ($(this).attr('value') == "fromeventRegister") {
			localStorage.setItem("str_from", "eventRegister")
		}
		
	  });
	
	$(document).on('click', ".save-roster-detail,#edit-roster-detail", function (eve) {
		$("#roaster_msg_err_msgs").text("")
		$("#roaster_msg_succ_msgs").text("")
		$('[data-toggle="tooltip"]').tooltip();

		eve.preventDefault()
		rosterName = "false"
		isFormValid = true;
	
		if ($("#rosterName").val().trim().length === 0) {
			$("#rosterName").addClass('empty-err');
			isFormValid = false;
		}

		else {
			isFormValid = true;
			$("#rosterName").removeClass('empty-err');
		}

		var team_roster_members_role=[];
	  var selectedValues
		var flag = "true"
		
		var rosterSelectedPeopleRole=[];
		var rosterSelectedPeople = [];

		$('input:checkbox[name="team_roster_members[]"]').each(function () {
			if (this.checked) {
				selectedValues = $('.select_role_'+ this.value).val().join("_");
				console.log("selectedValues--"+selectedValues)
			
				if (selectedValues == '') {
					flag="false";
				} 
			if ( flag == 'true') {
				$("#roaster_msg_err_msgs").text("")
					console.log("checked--"+this.value)
				 	team_roster_members_role.push(selectedValues);	
				}else{
					$('#loading-spinner').addClass('hide');
				$("#roaster_msg_err_msgs").text("Please select role(s) for roster members")
				$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
					isFormValid = false;
				}
			}
		});

		$('#rosterMemberRoles').val(team_roster_members_role)
		if (isFormValid) {
			$('#loading-spinner').removeClass('hide');

			if ($("#roasterUpdateId").val() == "") {
				$.ajax({
					type: "GET",
					url: "/check-roster-name",
					data: { "rosterName": $("#rosterName").val().trim() },
				}).done(function (msg) {
					if (msg == "true") {
						rosterName = msg;
						$("#roaster_msg_err_msgs").text("Roster name already exists")
						$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
						$('#loading-spinner').addClass('hide');
						return
					} else {
						myFunction()
					}
				});
			} else {
				myFunction()
			}

			function myFunction() {
				$.ajax({
					type: "POST",
					url: "/add-edit-roster",
					data: $("#rosteraddeditForm").serialize()
				}).done(function (msg) {
					tempmsgdata = JSON.stringify(msg)
					if (localStorage.getItem("rosteroption") != "edit") {
						if (tempmsgdata != "null") {
							rooster_DataValue = JSON.parse(tempmsgdata)
							rooster_Data = rooster_DataValue.rosterdata
							rooster_Submit_Status_Data = rooster_DataValue.rostersubmitstatus
							if (rooster_Data) {
								rosterCoCaptains = $("#rosterCoCaptains").val()
								var coCaptainData = rosterCoCaptains.split(',');
								rosterTeamCaptainId = $("#rosterTeamCaptainId").val()
								rosterUserId = $("#rosterUserId").val()
								rosterUserId = $("#rosterUserId").val()
								rosterSuperAdmin = $("#rosterSuperAdmin").val()
								var html = ""
								html += "<tr>" +
									"<td>" +
									"<div class='dasboard-team-member-content'>" +
									"<div class='team-member-name'>" +
									"<a href='#'onclick=editOrViewOrDuplicateRooster(" + rooster_Data[0].rosterid + ",'view',event) class='list-title tm-list-title-overwrite'>" +
									rooster_Data[0].name + "</a></div></div>" +
									"</td>" +
									"<td>" +
									"<div class='dasboard-team-member-content'>" +
									"<div class='team-member-name'>" +
									"<span class='list-title list-title-overwrite'>";
								if (rooster_Data[0].memberlength) {
									html += rooster_Data[0].memberlength;
								}
								html += "</span></div></div>" +
									"</td>" +
									"<td>" +
									"<div class='dasboard-team-member-content'>" +
									"<div class='team-member-name'>" +
									"<span class='list-title list-title-overwrite'>";
								html += rooster_Data[0].rostertype
								html += "</span></div></div>" +
									"</td>" +
									"<td>" +
									"<div class='dasboard-team-member-content'>" +
									"<div class='team-member-name'>";
								if (rooster_Data[0].rostertype == "Practice" && rooster_Data[0].fk_practice_id != "0") {
									html += "<span class='list-title list-title-overwrite'>";
									html += rooster_Data[0].formatedpracticedate + "|" + rooster_Data[0].practice_time + "|" + rooster_Data[0].practice_location
									html += "</span></div></div>";
								} else if (rooster_Data[0].rostertype == "Event" && rooster_Data[0].fk_event_id != "0") {
									html += "<span class='list-title list-title-overwrite'>";
									html += rooster_Data[0].eventname
									html += "</span></div></div>";
								} else {
									html += "<span class='list-title list-title-overwrite'>" +
										"</span></div></div>";
								}
								html += "</td>";
								var index = jQuery.inArray(rosterUserId, coCaptainData)
								if ((index != -1) || (rosterUserId == rosterTeamCaptainId) || (rosterSuperAdmin == "true")) {
									var editroaster = rooster_Data[0].rosterid + ',"edit",event'
									var duplicateroaster = rooster_Data[0].rosterid + ',"duplicate",event'
									if (rooster_Submit_Status_Data == "false") {
										html += "<td><a tabindex='0' role='button'  class='btn btn-default buttonpopover' data-container='body' data-html='true' data-toggle='popover'" +
											"data-placement='left'  data-trigger='focus' data-content='<ul class=popover-waiver><li><a href=# onclick=editOrViewOrDuplicateRooster(" + editroaster + ")>Edit</a></li><li><a href=# onclick=editOrViewOrDuplicateRooster(" + duplicateroaster + ")>Duplicate</a></li><li><a id=deleteRoster value=" + rooster_Data[0].rosterid + " href=# >Delete</a></li></ul>'" +
											"><span class='glyphicon glyphicon-option-horizontal' aria-hidden='true'></span></a></td>;"
									} else {
										html += "<td><a tabindex='0' role='button'  class='btn btn-default buttonpopover' data-container='body' data-html='true' data-toggle='popover'" +
											"data-placement='left'  data-trigger='focus' data-content='<ul class=popover-waiver><li><a href=# onclick=editOrViewOrDuplicateRooster(" + editroaster + ")>Edit</a></li><li><a href=# onclick=editOrViewOrDuplicateRooster(" + duplicateroaster + ")>Duplicate</a></li><li><a id=deleteRoster value=" + rooster_Data[0].rosterid + " href=# >Delete</a></li></ul>'" +
											"><span class='glyphicon glyphicon-option-horizontal' aria-hidden='true'></span></a></td>;"
									}

								}
								$('#appendrooster').prepend(html)
								$('[data-toggle="popover"]').popover()
								$('#loading-spinner').addClass('hide');
								$("#roaster_msg_succ_msgs").text("Roster saved successfully")
								$("#newRosterModel").animate({ scrollTop: 0 }, "slow");

								rosterCount = $("#rosterCount").val()
								$("#rosterCount").val(Number(rosterCount) + 1)
								$(".rostercountchange").text(Number(rosterCount) + 1)
								setTimeout(function () {
									sessionStorage.setItem('activeTab', '#roaster')
									location.reload()
									$("#newRosterModel").modal("hide");
								}, TWO_SEC);
							} else {
								$("#roaster_msg_err_msgs").text("Roster not saved successfully")
								$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
								$('#loading-spinner').addClass('hide');
							}
						}
					} else {
						localStorage.removeItem("rosteroption")
						localStorage.removeItem("rosterpracticeid")
						localStorage.removeItem("rostereventname")
						localStorage.removeItem("rostereventid")
						localStorage.removeItem("rostertype")
						localStorage.removeItem("rosterComment")
						localStorage.removeItem("team_save_draft_roster")
						if (msg == "true") {
							$("#roaster_msg_succ_msgs").text("Roster updated")
							$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
							setTimeout(function () {
								$("#newRosterModel").modal("hide");
								sessionStorage.setItem('activeTab', '#roaster')
								location.reload()
							}, TWO_SEC);
						} else {
							$("#roaster_msg_err_msgs").text("Roster not updated")
							$("#newRosterModel").animate({ scrollTop: 0 }, "slow");
						}
						$('#loading-spinner').addClass('hide');
					}
				});
			}
		} else {
			$("#newRosterModel").animate({ scrollTop: 0 }, "slow");

		}

	});

	editSubmittedRooster = function (temprosterId, tempoptionType, e) {
		e.preventDefault();
		$("input[type=radio][value='Practice']").prop("disabled", true);
		$("input[type=radio][value='N/A']").prop("disabled", true);

		editOrViewOrDuplicateRooster(temprosterId, tempoptionType, e)
	}

	$('#newRosterModel').on('hidden.bs.modal', function (e) {
		$("input[type=radio][value='Practice']").prop("disabled", false);
		$("input[type=radio][value='N/A']").prop("disabled", false);
	})

	editOrViewOrDuplicateRooster = function (rosterId, optionType, e) {
		e.preventDefault();
		//$('.img-roster-mem-role').addClass('hide')
		$(".set_default_role").val('paddler').trigger('change');
		//$(".show_roster_comment").css("display", "");
  	//$("#togglerostercommenticon").attr('src',"/static/images/arrow_up.png")	
		$('#loading-spinner').removeClass('hide');
		$('#select_all_roster_members').prop('checked', false);
		$("#roaster_msg_err_msgs").text("")
		$("#roaster_msg_succ_msgs").text("")
		$(".resetupdate_select").removeClass('empty-err');
		$("#rosterName").removeClass('empty-err');

		$(".img-roster-mem-roles").css("border", "solid 1px #ced1d0");
		$(".img-roster-mem-roles").removeClass("isSelected");

		$(".img_paddler").css("border", "solid 3px #2a7560");
		$(".img_paddler").addClass("isSelected");

		localStorage.removeItem("rosteroption")
		localStorage.removeItem("rosterpracticeid")
		localStorage.removeItem("rostereventid")
		localStorage.removeItem("rostereventname")
		localStorage.removeItem("rostertype")
		localStorage.removeItem("rosterComment")
		localStorage.removeItem("team_save_draft_roster")

		$(".iconhide").each(function () {
			$(this).addClass('hide');
		})

		$.ajax({
			type: "GET",
			url: "/get-individual-roster-detail",
			data: { "rosterId": rosterId },
		}).done(function (msg) {
			tempmsg = JSON.stringify(msg)
			if (tempmsg != "null") {
				rooster_Data1 = JSON.parse(tempmsg)
				rooster_Data = rooster_Data1.rosterdetaildata
				roosterSubmitStatus = rooster_Data1.rostersubmitstatus[0].case
				$("#newRosterModel").modal("show");
				$("#rooster_Event").addClass('hide')
				$("#rosterEventId").val("0")
				$("#searchrosterEventId").val("")
				$("#rosterPracticeId").val("0")
				$("#rostercommentDetail").val('')
				$('#team_save_draft_roster').prop('checked', false);
				$('input[name="rosterType"]').prop('checked', false);

				$('.resetupdate_select').prop('selectedIndex', 0);
				$('input:checkbox[name="team_roster_members[]"]').each(function () {
					if (this.checked) {
						$(this).prop('checked', false);
					}
					$('.roster-member-li').each(function () {
						$(this).removeAttr("style")
					});
				});

				if (rooster_Data[0].type == "Practice") {
					$("#rooster_Practice").removeClass('hide')
					$("#rooster_Event").addClass('hide')
					getUpComingEventOrPracticeList('practice', "rosterPracticeId", "editroster")
				} else if (rooster_Data[0].type == "N/A") {
					$("#rooster_Practice").addClass('hide')
					$("#rooster_Event").addClass('hide')
					$('#loading-spinner').addClass('hide');
				} else if (rooster_Data[0].type == "Event") {
					$("#rooster_Practice").addClass('hide')
					$("#rooster_Event").removeClass('hide')
					$('#loading-spinner').addClass('hide');
					if (rooster_Data[0].event_name && rooster_Data[0].event_name != "") {
						$('#searchrosterEventId').val(rooster_Data[0].event_name);
						localStorage.setItem("rostereventname", rooster_Data[0].event_name)
					} else {
						$('#searchrosterEventId').val("");
						localStorage.setItem("rostereventname", "")
					}
					$('#rosterEventId').val(rooster_Data[0].fk_event_id);
				}
				$("#rosterName").val(rooster_Data[0].name)

				$('input:radio[name=rosterType]').val([rooster_Data[0].type]);
				$("#rostercomment").removeClass('hide')
				$("#team_save_draft_roster_hide").removeClass('hide');

				$("#rostercommentDetail").val(rooster_Data[0].comments)
				if (rooster_Data[0].save_draft == "0") {
					$('#team_save_draft_roster').prop('checked', false);
				} else if (rooster_Data[0].save_draft == "1") {
					$('#team_save_draft_roster').prop('checked', true);
				}

				$("#roster_members_list").removeClass('hide')
				var rooster_DataArray = rooster_Data[0].members.replace("{", "").replace("}", "").split(',');
				if (rooster_DataArray[0] != "") {
					for (var i = 0; i < rooster_DataArray.length; i++) {
						$("#mem_ros_" + rooster_DataArray[i]).prop('checked', true);

					}

				}
				var rooster_RoleArray = rooster_Data1.rostermemberroledata
				if (rooster_RoleArray != "" && rooster_RoleArray) {
					for (var i = 0; i < rooster_RoleArray.length; i++) {
						var memId = rooster_RoleArray[i].roster_member_id
						var rooster_RoleMemArray = rooster_RoleArray[i].member_role.replace("{", "").replace("}", "").split(',');
										
						if (rooster_RoleMemArray != "") {
							$(".select_role_"+memId).val(rooster_RoleMemArray).trigger('change');
						}

					}
				}

				$('.changebuttonnameroster').removeClass("save-roster-detail").removeClass("save-lineup-roster-detail")
				$('#rosteraddeditForm').find('input, textarea, button, select').attr('disabled', false);
				if (optionType == "edit") {
					$('#viewRosterEdit').addClass('hide')
					$("#defaultHeading").addClass('hide');
					$("#dynamicHeading").removeClass('hide');
					$("#save-roster-button").addClass('hide');
					$("#roasterUpdateId").val(rooster_Data[0].rosteridvalue)
					$("#dynamicHeading").text("Edit Roster");
					$("#edit-save-roster-button").removeClass('hide');
					localStorage.setItem("rosteroption", "edit")
					$('#loading-spinner').addClass('hide');
				} else if (optionType == "duplicate") {
					$('#viewRosterEdit').addClass('hide')
					$("#defaultHeading").removeClass('hide');
					$("#dynamicHeading").addClass('hide');
					$("#roasterUpdateId").val('')
					$("#save-roster-button").removeClass('hide');
					$("#edit-save-roster-button").addClass('hide');
					localStorage.setItem("rosteroption", "duplicate")
					$('.changebuttonnameroster').addClass("save-roster-detail").removeClass("save-lineup-roster-detail")
					$('#loading-spinner').addClass('hide');
				} else if (optionType == "view") {
					$('#viewRosterEdit').addClass('hide')
					$("#defaultHeading").addClass('hide');
					$("#dynamicHeading").removeClass('hide');
					$("#dynamicHeading").text("View Roster")
					$("#save-roster-button").addClass('hide');
					$("#edit-save-roster-button").addClass('hide');
					$("#roasterUpdateId").val(rooster_Data[0].rosteridvalue)
					localStorage.setItem("rosteroption", "edit")
					$('#loading-spinner').addClass('hide');
					$('#rosteraddeditForm').find('input, textarea, button, select').attr('disabled', true);
				}

				if(roosterSubmitStatus == "true"){
					$('#roster_practice').attr('disabled', true);
					$('#roster_n_a').attr('disabled', true);
					$('#searchrosterEventId').attr('readonly', true);
				}else {
					$('#searchrosterEventId').attr('readonly', false);
					if (optionType != "view"){
						$('#roster_practice').attr('disabled', false);
						$('#roster_n_a').attr('disabled', false);
					}
				}

				localStorage.setItem("rosterpracticeid", rooster_Data[0].fk_practice_id)
				localStorage.setItem("rostereventid", rooster_Data[0].fk_event_id)
				localStorage.setItem("rostertype", rooster_Data[0].type)
				localStorage.setItem("rosterComment", rooster_Data[0].comments)
				if (rooster_Data[0].save_draft == "0") {
					localStorage.setItem("team_save_draft_roster", rooster_Data[0].save_draft)
				} else if (rooster_Data[0].save_draft == "1") {
					localStorage.setItem("team_save_draft_roster", rooster_Data[0].save_draft)
				}
				$('[data-toggle="tooltip"]').tooltip();
				$('#loading-spinner').addClass('hide');
			}
		});
	}

	$('#viewRosterEdit').click(function (eve) {
		eve.preventDefault();
		$("#dynamicHeading").text("Edit Roster");
		$("#edit-save-roster-button").removeClass('hide');
		localStorage.setItem("rosteroption", "edit")
		$('#viewRosterEdit').addClass('hide')
	});

	$(document).on('click', "#removeRosterBtn", function (e) {
		e.preventDefault();
		var rosterId = $(this).attr('value');
		if (rosterId) {
			//Get the individual user detail
			$.ajax({
				url: "/delete-roster",
				type: "POST",
				data: { "rosterid": rosterId },
				success: function (data) {
					$("#removeRosterModal").modal("hide");
					sessionStorage.setItem('activeTab', '#roaster')
					location.reload()
				}
			});
		}
	});

	$(document).on('click', "#deleteRoster", function (e) {
		e.preventDefault();
		var rosterId = $(this).attr('value');
		if (rosterId) {
			$.ajax({
				url: "/event-check-roster-register-with-event",
				type: "GET",
				data: { "rosterid": rosterId },
				success: function (data) {
					if (data == "true") {
						$("#removeRosterModal").modal("show");
						$("#erroremoveroster").text("This Roster is linked with an registered event. Are you sure want to delete this Roster ? Deleting it will remove it from the event!")
						$("#removeRosterBtn").val(rosterId)
					} else {
						$("#removeRosterModal").modal("show");
						$("#erroremoveroster").text("Are you sure you want to remove this roster?")
						$("#removeRosterBtn").val(rosterId)
					}

				}
			});
		}
	});

	$(document).on('click', "#deleteRegisterRoster", function (e) {
		e.preventDefault();
		var rosterId = $(this).attr('value');
		if (rosterId) {
			$("#removeRosterModal").modal("show");
			$("#erroremoveroster").text("This Roster is linked with an registered event. Are you sure want to delete this Roster ? Deleting it will remove it from the event!")
			$("#removeRosterBtn").val(rosterId)
		}
	});


	/**In roster view click newlineup button this id will trigger */
	$(document).on('click', "#createLineup", function (e) {
		e.preventDefault();
		$('#viewLineupHide').addClass('hide')
		$('#createLineupHide').removeClass('hide')
		$.ajax({
			url: "/get-roster-details-list",
			type: "GET",
			success: function (data) {
				createNewOrEditLineup(data)
			}
		});
	});

	$(document).on('click', ".vieweditLineup", function (e) {
		e.preventDefault();
		$('#viewLineupHide').addClass('hide')
		$('#createLineupHide').removeClass('hide')
		$('#leftSidePaddlersHide').addClass('hide')
		$('#rightidePaddlersHide').addClass('hide')
		$('#othersidePaddlersHide').addClass('hide')
		$('#bothsidePaddlersHide').addClass('hide')
		$('#dropArea').addClass('hide')
		$('#lineup-alert').addClass('hide')
		$("#lineupname").val('')
		$("#team_save_draft_lineup").prop("checked", false);
		var lineup_idValue = $(this).attr('value');
		var roster_idValue = $(this).attr('name');
		var lineupoption = $(this).attr('id');
		$.ajax({
			url: "/get-roster-details-list",
			type: "GET",
			success: function (data) {
				createNewOrEditLineup(data)
				editLineUpData(lineup_idValue, roster_idValue, lineupoption)
				$("#lineup_id").val(roster_idValue)
			}
		});
	});

	$(document).on('click', "#lineupTabClick", function (e) {
		e.preventDefault();
		$('#viewLineupHide').removeClass('hide')
		$('#createLineupHide').addClass('hide')
		resetLineup()
	});
	
	

	
	function reIntializeLineup() {
		$('#leftSidePaddlers').empty()
		$('#rightSidePaddlers').empty()
		$('#othersidePaddlers').empty()
		$('#bothsidePaddlers').empty()
		$('.leftDropBox').empty();
		$('.rightDropBox').empty();
		$('.topDropBox').empty();
		$('.bottomDropBox').empty();
		$('#dropArea').addClass('hide')
		$('#lineup-alert').addClass('hide')
		$('#leftSidePaddlersHide').addClass('hide')
		$('#rightidePaddlersHide').addClass('hide')
		$('#othersidePaddlersHide').addClass('hide')
		$('#bothsidePaddlersHide').addClass('hide')
		$('#submitCreateRoster').addClass('hide')
		$('#editCreateRoster').addClass('hide')
		$('#team_save_draft_lineup').attr("disabled", false);
		$('#team_save_draft_lineup_hide').addClass('hide')
	}
	function editLineUpData(lineup_idData, roster_idData, lineupoptionStatus) {
		reIntializeLineup()
		$.ajax({
			url: "/get-lineup-individual-detail",
			type: "GET",
			data: { "lineup_id": lineup_idData, "roster_id": roster_idData },
			success: function (lineupdata) {
				var templineupdata = JSON.stringify(lineupdata)
				if (templineupdata) {
					lineupdataValue = JSON.parse(templineupdata)
					if (lineupdataValue.lineupdata && lineupdataValue.rosteruserdata) {
						leftsidehtml = "";
						rightsidehtml = "";
						othersidehtml = "";
						bothsidehtml = "";
						var membersDataIndex = [];
						if (lineupdataValue.lineupdata.length > 0 && lineupdataValue.rosteruserdata.length > 0) {

							var left_padller_membersData = lineupdataValue.lineupdata[0].left_padller_members.replace("{", "").replace("}", "").split(',');
							var right_padller_membersData = lineupdataValue.lineupdata[0].right_padller_members.replace("{", "").replace("}", "").split(',');
							var other_membersData = lineupdataValue.lineupdata[0].other_members.replace("{", "").replace("}", "").split(',');

							var selected_left_paddlersData = lineupdataValue.lineupdata[0].selected_left_paddlers.replace("{", "").replace("}", "").split(',');
							var selected_right_paddlersData = lineupdataValue.lineupdata[0].selected_right_paddlers.replace("{", "").replace("}", "").split(',');
							var selected_drummersData = lineupdataValue.lineupdata[0].selected_drummers.replace("{", "").replace("}", "").split(',');
							var selected_sparessData = lineupdataValue.lineupdata[0].selected_sparess.replace("{", "").replace("}", "").split(',');

							$("#lineupname").val(lineupdataValue.lineupdata[0].lineup_name)
							if (lineupdataValue.lineupdata[0].save_draft == "0") {
								$("#team_save_draft_lineup").prop("checked", false);
							} else if (lineupdataValue.lineupdata[0].save_draft == "1") {
								$("#team_save_draft_lineup").prop("checked", true);
							}

							for (var i = 0; i < lineupdataValue.rosteruserdata.length; i++) {

								membersDataIndex.push(lineupdataValue.rosteruserdata[i].fk_user_id)

								var memberweight = ""
								var memberpaddilingside = ""

								if (lineupdataValue.rosteruserdata[i].weight && lineupdataValue.rosteruserdata[i].weight != "" && lineupdataValue.rosteruserdata[i].weight != "0" && !active_User_Status) {
									memberweight = lineupdataValue.rosteruserdata[i].weight
								}

								if (lineupdataValue.rosteruserdata[i].paddling_side && lineupdataValue.rosteruserdata[i].paddling_side != "") {
									memberpaddilingside = lineupdataValue.rosteruserdata[i].paddling_side
								}

								if (memberweight != "" && memberpaddilingside != "") {
									memberpaddilingside = ", " + memberpaddilingside
								}
								//&& $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, left_padller_membersData) != -1
								if (lineupdataValue.rosteruserdata[i].paddling_side == "left" && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_left_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_right_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_drummersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_sparessData) == -1) {

									leftsidehtml += "<div id= " + lineupdataValue.rosteruserdata[i].fk_user_id + " class='dragBox dragDrop leftteammembers' data-position='leftside'>" +
										"<span class='dropDefaultTxt'>SK</span>" +
										"<div class='dragItem'>";
									if (lineupdataValue.rosteruserdata[i].files) {
										leftsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + " data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + lineupdataValue.rosteruserdata[i].files + "' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									} else {
										leftsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									}

									leftsidehtml += "</div></div>";
								} else if (lineupdataValue.rosteruserdata[i].paddling_side == "right" && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_left_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_right_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_drummersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_sparessData) == -1) {
									rightsidehtml += "<div id= " + lineupdataValue.rosteruserdata[i].fk_user_id + " class='dragBox dragDrop rightteammembers' data-position='rightside'>" +
										"<span class='dropDefaultTxt'>SK</span>" +
										"<div class='dragItem'>";
									if (lineupdataValue.rosteruserdata[i].files) {
										rightsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='" + lineupdataValue.rosteruserdata[i].files + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									} else {
										rightsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									}

									rightsidehtml += "</div></div>";
								} else if (lineupdataValue.rosteruserdata[i].paddling_side == "both" && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_left_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_right_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_drummersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_sparessData) == -1) {
									bothsidehtml += "<div id= " + lineupdataValue.rosteruserdata[i].fk_user_id + " class='dragBox dragDrop bothteammembers' data-position='bothside'>" +
										"<span class='dropDefaultTxt'>SK</span>" +
										"<div class='dragItem'>";
									if (lineupdataValue.rosteruserdata[i].files) {
										bothsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='" + lineupdataValue.rosteruserdata[i].files + "' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									} else {
										bothsidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									}
									bothsidehtml += "</div></div>";
								} else if ($.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_left_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_right_paddlersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_drummersData) == -1 && $.inArray(lineupdataValue.rosteruserdata[i].fk_user_id, selected_sparessData) == -1) {
									othersidehtml += "<div id= " + lineupdataValue.rosteruserdata[i].fk_user_id + " class='dragBox dragDrop otherteammembers' data-position='otherside'>" +
										"<span class='dropDefaultTxt'>SK</span>" +
										"<div class='dragItem'>";
									if (lineupdataValue.rosteruserdata[i].files) {
										othersidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='" + lineupdataValue.rosteruserdata[i].files + "' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									} else {
										othersidehtml += "<img data-weight= " + lineupdataValue.rosteruserdata[i].weight + "  src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + lineupdataValue.rosteruserdata[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + lineupdataValue.rosteruserdata[i].fk_user_id + "' class='dragImg'>";
									}
									othersidehtml += "</div></div>";
								}
							}

							topDropBoxHtml = ""
							leftDropBoxHtml = ""
							rightDropBoxHtml = ""
							bottomDropBoxHtml = ""


							for (var i = 0; i < selected_left_paddlersData.length; i++) {
								if (selected_left_paddlersData[i] != "0") {
									indexValue = $.inArray(selected_left_paddlersData[i], membersDataIndex)
									if (indexValue != -1) {
										sideData = lineupdataValue.rosteruserdata[indexValue].paddling_side
										var datapostionDetail = "otherside"
										if (sideData == "right") {
											datapostionDetail = "rightside"
										} else if (sideData == "left") {
											datapostionDetail = "leftside"
										} else if (sideData == "both") {
											datapostionDetail = "bothside"
										}

										var memberweight = ""
										var memberpaddilingside = ""

										if (lineupdataValue.rosteruserdata[indexValue].weight && lineupdataValue.rosteruserdata[indexValue].weight != "" && lineupdataValue.rosteruserdata[indexValue].weight != "0" && !active_User_Status) {
											memberweight = lineupdataValue.rosteruserdata[indexValue].weight
										}

										if (lineupdataValue.rosteruserdata[indexValue].paddling_side && lineupdataValue.rosteruserdata[indexValue].paddling_side != "") {
											memberpaddilingside = lineupdataValue.rosteruserdata[indexValue].paddling_side
										}

										if (memberweight != "" && memberpaddilingside != "") {
											memberpaddilingside = ", " + memberpaddilingside
										}

										leftDropBoxHtml += "<div class='dropBox ui-droppable droppedItem ui-draggable ui-draggable-handle' data-position=" + datapostionDetail + ">" +
											"<span class='dropDefaultTxt'>SK</span>" +
											"<div class='dragItem' style='z-index: 98;'>";
										if (lineupdataValue.rosteruserdata[indexValue].files) {
											leftDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + " data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + lineupdataValue.rosteruserdata[indexValue].files + "' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										} else {
											leftDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + "  data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										}
										leftDropBoxHtml += "</div></div>";
									}
								} else {
									leftDropBoxHtml += "<div class='dropBox'>" +
										"<div class='dragDrop'>" +
										"<span class='dropDefaultTxt'>P</span>" +
										"</div></div>";
								}
							}

							for (var i = 0; i < selected_right_paddlersData.length; i++) {
								if (selected_right_paddlersData[i] != "0") {
									indexValue = $.inArray(selected_right_paddlersData[i], membersDataIndex)
									if (indexValue != -1) {
										sideData = lineupdataValue.rosteruserdata[indexValue].paddling_side
										var datapostionDetail = "otherside"
										if (sideData == "right") {
											datapostionDetail = "rightside"
										} else if (sideData == "left") {
											datapostionDetail = "leftside"
										} else if (sideData == "both") {
											datapostionDetail = "bothside"
										}

										var memberweight = ""
										var memberpaddilingside = ""

										if (lineupdataValue.rosteruserdata[indexValue].weight && lineupdataValue.rosteruserdata[indexValue].weight != "" && lineupdataValue.rosteruserdata[indexValue].weight != "0" && !active_User_Status) {
											memberweight = lineupdataValue.rosteruserdata[indexValue].weight
										}

										if (lineupdataValue.rosteruserdata[indexValue].paddling_side && lineupdataValue.rosteruserdata[indexValue].paddling_side != "") {
											memberpaddilingside = lineupdataValue.rosteruserdata[indexValue].paddling_side
										}

										if (memberweight != "" && memberpaddilingside != "") {
											memberpaddilingside = ", " + memberpaddilingside
										}

										rightDropBoxHtml += "<div class='dropBox ui-droppable droppedItem ui-draggable ui-draggable-handle' data-position=" + datapostionDetail + ">" +
											"<span class='dropDefaultTxt'>SK</span>" +
											"<div class='dragItem' style='z-index: 98;'>";
										if (lineupdataValue.rosteruserdata[indexValue].files) {
											rightDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + "  data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + lineupdataValue.rosteruserdata[indexValue].files + "' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										} else {
											rightDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + "  data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										}
										rightDropBoxHtml += "</div></div>";
									}
								} else {
									rightDropBoxHtml += "<div class='dropBox'>" +
										"<div class='dragDrop'>" +
										"<span class='dropDefaultTxt'>P</span>" +
										"</div></div>";
								}
							}

							for (var i = 0; i < selected_drummersData.length; i++) {
								if (selected_drummersData[i] != "0") {
									indexValue = $.inArray(selected_drummersData[i], membersDataIndex)
									if (indexValue != -1) {
										sideData = lineupdataValue.rosteruserdata[indexValue].paddling_side
										var datapostionDetail = "otherside"
										if (sideData == "right") {
											datapostionDetail = "rightside"
										} else if (sideData == "left") {
											datapostionDetail = "leftside"
										} else if (sideData == "both") {
											datapostionDetail = "bothside"
										}

										var memberweight = ""
										var memberpaddilingside = ""

										if (lineupdataValue.rosteruserdata[indexValue].weight && lineupdataValue.rosteruserdata[indexValue].weight != "" && lineupdataValue.rosteruserdata[indexValue].weight != "0" && !active_User_Status) {
											memberweight = lineupdataValue.rosteruserdata[indexValue].weight
										}

										if (lineupdataValue.rosteruserdata[indexValue].paddling_side && lineupdataValue.rosteruserdata[indexValue].paddling_side != "") {
											memberpaddilingside = lineupdataValue.rosteruserdata[indexValue].paddling_side
										}

										if (memberweight != "" && memberpaddilingside != "") {
											memberpaddilingside = ", " + memberpaddilingside
										}

										topDropBoxHtml += "<div class='dropBox ui-droppable droppedItem ui-draggable ui-draggable-handle' data-position=" + datapostionDetail + ">" +
											"<span class='dropDefaultTxt'>SK</span>" +
											"<div class='dragItem' style='z-index: 98;'>";
										if (lineupdataValue.rosteruserdata[indexValue].files) {
											topDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + " data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + lineupdataValue.rosteruserdata[indexValue].files + "' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										} else {
											topDropBoxHtml += "<img data-toggle='tooltip' data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + " data-html= 'true'  data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										}
										topDropBoxHtml += "</div></div>";
									}
								} else {
									topDropBoxHtml += "<div class='dropBox'>" +
										"<div class='dragDrop'>" +
										"<span class='dropDefaultTxt'>D</span>" +
										"</div></div>";
								}
							}

							for (var i = 0; i < selected_sparessData.length; i++) {
								if (selected_sparessData[i] != "0") {
									indexValue = $.inArray(selected_sparessData[i], membersDataIndex)
									if (indexValue != -1) {
										sideData = lineupdataValue.rosteruserdata[indexValue].paddling_side
										var datapostionDetail = "otherside"
										if (sideData == "right") {
											datapostionDetail = "rightside"
										} else if (sideData == "left") {
											datapostionDetail = "leftside"
										} else if (sideData == "both") {
											datapostionDetail = "bothside"
										}

										var memberweight = ""
										var memberpaddilingside = ""

										if (lineupdataValue.rosteruserdata[indexValue].weight && lineupdataValue.rosteruserdata[indexValue].weight != "" && lineupdataValue.rosteruserdata[indexValue].weight != "0" && !active_User_Status) {
											memberweight = lineupdataValue.rosteruserdata[indexValue].weight
										}

										if (lineupdataValue.rosteruserdata[indexValue].paddling_side && lineupdataValue.rosteruserdata[indexValue].paddling_side != "") {
											memberpaddilingside = lineupdataValue.rosteruserdata[indexValue].paddling_side
										}

										if (memberweight != "" && memberpaddilingside != "") {
											memberpaddilingside = ", " + memberpaddilingside
										}

										bottomDropBoxHtml += "<div class='dropBox ui-droppable droppedItem ui-draggable ui-draggable-handle' data-position=" + datapostionDetail + ">" +
											"<span class='dropDefaultTxt'>SK</span>" +
											"<div class='dragItem' style='z-index: 98;'>";
										if (lineupdataValue.rosteruserdata[indexValue].files) {
											bottomDropBoxHtml += "<img data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + "  src='" + lineupdataValue.rosteruserdata[indexValue].files + "' data-html= 'true'  data-toggle='tooltip' data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' class='dragImg'>";
										} else {
											bottomDropBoxHtml += "<img data-weight= " + lineupdataValue.rosteruserdata[indexValue].weight + " src='/static/images/user-default.png' data-classes='" + lineupdataValue.rosteruserdata[indexValue].classes + "' value='" + lineupdataValue.rosteruserdata[indexValue].fk_user_id + "' data-html= 'true'  data-toggle='tooltip' data-original-title='<p class=mb-1>" + lineupdataValue.rosteruserdata[indexValue].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' class='dragImg'>";
										}
										bottomDropBoxHtml += "</div></div>";
									}
								} else {
									bottomDropBoxHtml += "<div class='dropBox'>" +
										"<div class='dragDrop'>" +
										"<span class='dropDefaultTxt'>S</span>" +
										"</div></div>";
								}
							}

							$('#leftSidePaddlersHide').removeClass('hide')
							$('#rightidePaddlersHide').removeClass('hide')
							$('#othersidePaddlersHide').removeClass('hide')
							$('#bothsidePaddlersHide').removeClass('hide')
							$('#dropArea').removeClass('hide')
							$('#lineup-alert').removeClass('hide')
							$('.leftDropBox').empty().append(leftDropBoxHtml);
							$('.rightDropBox').empty().append(rightDropBoxHtml);
							$('.topDropBox').empty().append(topDropBoxHtml);
							$('.bottomDropBox').empty().append(bottomDropBoxHtml);

							$('#leftSidePaddlers').empty().append(leftsidehtml);
							$('#rightSidePaddlers').empty().append(rightsidehtml);
							$('#othersidePaddlers').empty().append(othersidehtml);
							$('#bothsidePaddlers').empty().append(bothsidehtml);


							localStorage.setItem("lineupoption", lineupoptionStatus)
							localStorage.setItem("lineup_idvalue", lineup_idData)
							localStorage.setItem("roster_idvalue", roster_idData)
							$('[data-toggle="tooltip"]').tooltip();
							$('#team_save_draft_lineup_hide').removeClass('hide')
							if (lineupoptionStatus != "view") {
								$('#team_save_draft_lineup').attr("disabled", false);
								$("#lineupname").attr("disabled", false);
								$("#lineup_id").attr("disabled", false);
								$('#rostersavehide').removeClass('hide')
								if (lineupoptionStatus == "edit") {
									$("#createlineup").text("")
									$('#submitCreateRoster').addClass('hide')
									$('#editCreateRoster').removeClass('hide')
									$('#lineupUpdateId').val(lineup_idData)
									$('#lineupname').attr("disabled", true);

								} else {
									$('#submitCreateRoster').removeClass('hide')
									$('#editCreateRoster').addClass('hide')
									$("#createlineup").text("Create Lineup")
								}
								dragDropPaddlers();
							} else {
								$('#team_save_draft_lineup').attr("disabled", true);
								$("#createlineup").text("")
								$("#lineupname").attr("disabled", true);
								$("#lineup_id").attr("disabled", true);
								$('#submitCreateRoster').addClass('hide')
								$('#editCreateRoster').addClass('hide')
								$('#rostersavehide').addClass('hide')
								lineupCount()
							}
							$('[data-toggle="tooltip"]').tooltip();
						}
					} else {
						$("#rostermembereditemptymodel").modal("show")
					}
				}
			}
		});
	}

	$(document).on('click', "#backeditlineuprooster", function (e) {
		e.preventDefault();
		$("#rostermembereditemptymodel").modal("hide")
		resetLineup();
		$('#viewLineupHide').removeClass('hide')
		$('#createLineupHide').addClass('hide')

	});


	function createNewOrEditLineup(lineupStatus) {
		var lineupStatusData = JSON.stringify(lineupStatus)
		if (lineupStatusData != "null") {
			roster_Data = JSON.parse(lineupStatusData)
			if (roster_Data.length > 0) {
				var options = '';
				options += '<option value="">Select Roster name</option>';
				for (var i = 0; i < roster_Data.length; i++) {
					if (roster_Data[i].type == "Practice") {
						if (roster_Data[i].activestatus == "1" || !roster_Data[i].activestatus) {
							options += '<option value="' + roster_Data[i].id + '">' + roster_Data[i].name + '</option>';
						}
					} else {
						options += '<option value="' + roster_Data[i].id + '">' + roster_Data[i].name + '</option>';
					}

				}
				$("select#lineup_id").empty().html(options);
			}
		} else {
			var options = '';
			options += '<option value="">Select Roster name</option>';
			$("select#lineup_id").empty().html(options);
		}

		resetLineup()
	}

	function resetLineup() {
		$('#lineupUpdateId').val('')
		localStorage.removeItem("lineupoption")
		localStorage.removeItem("lineup_idvalue")
		localStorage.removeItem("roster_idvalue")
		$('#leftSidePaddlersHide').addClass('hide')
		$('#rightidePaddlersHide').addClass('hide')
		$('#othersidePaddlersHide').addClass('hide')
		$('#bothsidePaddlersHide').addClass('hide')
		$('#rostersavehide').addClass('hide')
		$('#team_save_draft_lineup_hide').addClass('hide')
		$('#team_save_draft_lineup').attr("disabled", false);
		$('#dropArea').addClass('hide')
		$('#lineup-alert').addClass('hide')
		$('#loading-spinner').addClass('hide');
		$('#submitCreateRoster').removeClass('hide')
		$('#editCreateRoster').addClass('hide')
		$("#lineupname").attr("disabled", false);
		$("#lineup_id").attr("disabled", false);
		var right_padller_members = [];
		var left_padller_members = [];
		var other_members = [];

		var leftDropBox_members = [];
		var rightDropBox_members = [];
		var buttomDropBox_members = [];
		var topDropBox_members = [];


		$("#left_padller_membersValue").val(left_padller_members)
		$("#right_padller_membersValue").val(right_padller_members)
		$("#other_padller_membersValue").val(other_members)

		$("#selectedleft_padller_membersValue").val(leftDropBox_members)
		$("#selectedright_padller_membersValue").val(rightDropBox_members)
		$("#selecteddrummer_padller_membersValue").val(topDropBox_members)
		$("#selectedspare_padller_membersValue").val(buttomDropBox_members)
		$("#lineupname").removeClass('empty-err');
		$("#lineup_msg_err_msgs").text("")
		$("#lineup_msg_succ_msgs").text("")
		$("#lineupname").val('')
		$("#team_save_draft_lineup").prop("checked", false);
		$("#lineup_id").val("")
		$("#createlineup").text("Create Lineup")

		$('.leftDropBox').empty();
		$('.rightDropBox').empty();
		$('.topDropBox').empty();
		$('.bottomDropBox').empty();

		$('#leftSidePaddlers').empty();
		$('#rightSidePaddlers').empty();
		$('#othersidePaddlers').empty();
		$('#bothsidePaddlers').empty();

	}

	$("#lineup_id").change(function () {
		$("#lineup_msg_err_msgs").text("")
		$("#lineupname").removeClass('empty-err');
		rosterId = this.value
		/** For edit view duplicate roster using same code. So during edit view,duplicate store in localstorage otherwise else condition is process. */
		if (localStorage.getItem("lineupoption") && localStorage.getItem("lineup_idvalue") && localStorage.getItem("roster_idvalue")) {
			if (localStorage.getItem("roster_idvalue") == rosterId || rosterId == "") {
				$("#lineup_id").val(localStorage.getItem("roster_idvalue"))
				editLineUpData(localStorage.getItem("lineup_idvalue"), localStorage.getItem("roster_idvalue"), localStorage.getItem("lineupoption"))
			} else {
				paddlerTeamView(rosterId, localStorage.getItem("lineupoption"))
			}
		} else {
			paddlerTeamView(rosterId, "")
		}
	});

	function paddlerTeamView(rosterIdValues, OptionType) {
		reIntializeLineup()
		rosterId = rosterIdValues
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			url: "/get-roster-user-detail",
			type: "GET",
			data: { "rosterId": rosterId },
			success: function (membersdata) {
				var tempmembersData = JSON.stringify(membersdata)
				if (tempmembersData != "null") {
					membersdataValue = JSON.parse(tempmembersData)
					if (membersdataValue.length > 0) {
						leftsidehtml = "";
						rightsidehtml = "";
						othersidehtml = "";
						bothsidehtml = "";
						for (var i = 0; i < membersdataValue.length; i++) {
							var memberweight = ""
							var memberpaddilingside = ""

							if (membersdataValue[i].weight && membersdataValue[i].weight != "" && membersdataValue[i].weight != "0" && !active_User_Status) {
								memberweight = membersdataValue[i].weight
							}

							if (membersdataValue[i].paddling_side && membersdataValue[i].paddling_side != "") {
								memberpaddilingside = membersdataValue[i].paddling_side
							}

							if (memberweight != "" && memberpaddilingside != "") {
								memberpaddilingside = ", " + memberpaddilingside
							}

							if (membersdataValue[i].paddling_side == "left") {
								leftsidehtml += "<div id= " + membersdataValue[i].fk_user_id + " class='dragBox dragDrop leftteammembers' data-position='leftside'>" +
									"<span class='dropDefaultTxt'>SK</span>" +
									"<div class='dragItem'>";
								if (membersdataValue[i].files) {
									leftsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + " tabindex='0' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + membersdataValue[i].files + "' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								} else {
									leftsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "   tabindex='0' data-html= 'true' src='/static/images/user-default.png' data-toggle='tooltip'  data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								}

								leftsidehtml += "</div></div>";
							} else if (membersdataValue[i].paddling_side == "right") {
								rightsidehtml += "<div id= " + membersdataValue[i].fk_user_id + " class='dragBox dragDrop rightteammembers' data-position='rightside'>" +
									"<span class='dropDefaultTxt'>SK</span>" +
									"<div class='dragItem'>";
								if (membersdataValue[i].files) {
									rightsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  data-html= 'true' data-toggle='tooltip' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + membersdataValue[i].files + "' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								} else {
									rightsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  src='/static/images/user-default.png' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								}

								rightsidehtml += "</div></div>";
							} else if (membersdataValue[i].paddling_side == "both") {
								bothsidehtml += "<div id= " + membersdataValue[i].fk_user_id + " class='dragBox dragDrop bothteammembers' data-position='bothside'>" +
									"<span class='dropDefaultTxt'>SK</span>" +
									"<div class='dragItem'>";
								if (membersdataValue[i].files) {
									bothsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  data-html= 'true' data-toggle='tooltip' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' src='" + membersdataValue[i].files + "' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								} else {
									bothsidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  src='/static/images/user-default.png' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' data-classes='" + membersdataValue[i].classes + "' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								}

								bothsidehtml += "</div></div>";
							} else {
								othersidehtml += "<div id= " + membersdataValue[i].fk_user_id + " class='dragBox dragDrop otherteammembers' data-position='otherside'>" +
									"<span class='dropDefaultTxt'>SK</span>" +
									"<div class='dragItem'>";
								if (membersdataValue[i].files) {
									othersidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  src='" + membersdataValue[i].files + "' data-classes='" + membersdataValue[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								} else {
									othersidehtml += "<img data-weight= " + membersdataValue[i].weight + " data-paddlingside= " + membersdataValue[i].paddling_side + "  data-name= " + membersdataValue[i].name + "  src='/static/images/user-default.png' data-classes='" + membersdataValue[i].classes + "' data-html= 'true' data-toggle='tooltip' title='<p class=mb-1>" + membersdataValue[i].name + "</p> <p class=mb-1>" + memberweight + "" + memberpaddilingside + "</p>' value='" + membersdataValue[i].fk_user_id + "' class='dragImg'>";
								}

								othersidehtml += "</div></div>";
							}

						}
						lineupeditcreateview(leftsidehtml, rightsidehtml, othersidehtml, bothsidehtml)
						if (leftsidehtml == "" && rightsidehtml == "" && othersidehtml == "" && bothsidehtml == "") {
							$('#rostersavehide').addClass('hide')
							$('#team_save_draft_lineup_hide').addClass('hide')
							$('#dropArea').addClass('hide')
							$('#lineup-alert').addClass('hide')
						} else {
							$('#team_save_draft_lineup_hide').removeClass('hide')
							$('#rostersavehide').removeClass('hide')
							if (OptionType == "edit") {
								$('#editCreateRoster').removeClass('hide')
								$('#submitCreateRoster').addClass('hide')
							} else {
								$('#submitCreateRoster').removeClass('hide')
								$('#editCreateRoster').addClass('hide')
							}
							$('#lineup-alert').removeClass('hide')
							$('#dropArea').removeClass('hide')
						}


						/** Reset the boot placing images */
						topDropBoxHtml = ""
						leftDropBoxHtml = ""
						rightDropBoxHtml = ""
						bottomDropBoxHtml = ""
						for (var i = 0; i < 10; i++) {

							leftDropBoxHtml += "<div class='dropBox'>" +
								"<div class='dragDrop'>" +
								"<span class='dropDefaultTxt'>P</span>" +
								"</div></div>";

							rightDropBoxHtml += "<div class='dropBox'>" +
								"<div class='dragDrop'>" +
								"<span class='dropDefaultTxt'>P</span>" +
								"</div></div>";
						}

						for (var i = 0; i < 1; i++) {
							topDropBoxHtml += "<div class='dropBox'>" +
								"<div class='dragDrop'>" +
								"<span class='dropDefaultTxt'>D</span>" +
								"</div></div>";

							bottomDropBoxHtml += "<div class='dropBox'>" +
								"<div class='dragDrop'>" +
								"<span class='dropDefaultTxt'>S</span>" +
								"</div></div>";
						}

						$('.leftDropBox').empty().append(leftDropBoxHtml);
						$('.rightDropBox').empty().append(rightDropBoxHtml);
						$('.topDropBox').empty().append(topDropBoxHtml);
						$('.bottomDropBox').empty().append(bottomDropBoxHtml);
						/**End */

						dragDropPaddlers();
						$('[data-toggle="tooltip"]').tooltip();
						//$('.dragImg').tooltip('show')
					}
				} else {
					$("#rostermembercreateemptymodel").modal("show")
				}
				$('#loading-spinner').addClass('hide');
			}
		});
	}

	$(document).on('click', "#backcreatelineuprooster", function (e) {
		e.preventDefault();
		$("#rostermembercreateemptymodel").modal("hide")
	});

	function lineupeditcreateview(leftsidehtml, rightsidehtml, othersidehtml, bothsidehtml) {
		if (leftsidehtml != "") {
			$('#leftSidePaddlers').empty().append(leftsidehtml);
			$('#leftSidePaddlersHide').removeClass('hide')
		} else {
			$('#leftSidePaddlers').empty()
			$('#leftSidePaddlersHide').addClass('hide')
		}
		if (rightsidehtml != "") {
			$('#rightSidePaddlers').empty().append(rightsidehtml);
			$('#rightidePaddlersHide').removeClass('hide')
		} else {
			$('#rightSidePaddlers').empty()
			$('#rightidePaddlersHide').addClass('hide')
		}
		if (othersidehtml != "") {
			$('#othersidePaddlers').empty().append(othersidehtml);
			$('#othersidePaddlersHide').removeClass('hide')
		} else {
			$('#othersidePaddlers').empty()
			$('#othersidePaddlersHide').addClass('hide')
		}
		if (bothsidehtml != "") {
			$('#bothsidePaddlers').empty().append(bothsidehtml);
			$('#bothsidePaddlersHide').removeClass('hide')
		} else {
			$('#bothsidePaddlers').empty()
			$('#bothsidePaddlersHide').addClass('hide')
		}
		dragDropPaddlers();
	}

	$(document).on('click', "#backlineuplist", function (e) {
		e.preventDefault();
		resetLineup();
		$('#viewLineupHide').removeClass('hide')
		$('#createLineupHide').addClass('hide')
	});


	/** Create Or Duplicate Lineup Call Trigger this event */
	$('#submitCreateRoster').click(function (e) {
		e.preventDefault();
		$("#lineupname").removeClass('empty-err');
		$("#lineup_msg_err_msgs").text("")
		$("#lineup_msg_succ_msgs").text("")

		isValidForm = false;
		//$('#loading-spinner').removeClass('hide');
		if ($("#lineupname").val().trim().length > 0) {
			isValidForm = true;
			$("#lineup_msg_err_msgs").text("")
			$("#lineupname").removeClass('empty-err');
		} else {
			$("#lineup_msg_err_msgs").text("Lineup name is missing")
			$("#lineupname").addClass('empty-err');
			isValidForm = false;
		}
		if (isValidForm) {
			$.ajax({
				type: "GET",
				url: "/check-lineup-name",
				data: { "lineupname": $("#lineupname").val().trim() },
			}).done(function (msg) {
				if (msg == "true") {
					$("html, body").animate({ scrollTop: 0 }, "slow");
					$("#lineup_msg_err_msgs").text("Lineup name already exists")
					$('#loading-spinner').addClass('hide');
					return
				} else {
					lineUpCreateFunction("create")
				}
			});
		} else {
			$("html, body").animate({ scrollTop: 0 }, "slow");
		}
	});

	/** Edit Lineup Call this click event */
	$('#editCreateRoster').click(function (e) {
		e.preventDefault();
		$("#lineupname").removeClass('empty-err');
		$("#lineup_msg_err_msgs").text("")
		$("#lineup_msg_succ_msgs").text("")
		lineUpCreateFunction("update")
	});

	/** Create Or Edit LineUp */
	function lineUpCreateFunction(optionType) {
		var right_padller_members = [];
		$(".rightSidePaddlers .rightteammembers").each(function () {
			right_padller_members.push($(this).find('.dragImg').attr('value'));
		})

		var left_padller_members = [];
		$(".leftSidePaddlers .leftteammembers").each(function () {
			left_padller_members.push($(this).find('.dragImg').attr('value'));
		})

		var other_members = [];
		$(".othersidePaddlers .otherteammembers").each(function () {
			other_members.push($(this).find('.dragImg').attr('value'));
		})

		$(".bothsidePaddlers .bothteammembers").each(function () {
			other_members.push($(this).find('.dragImg').attr('value'));
		})

		$("#left_padller_membersValue").val(left_padller_members)
		$("#right_padller_membersValue").val(right_padller_members)
		$("#other_padller_membersValue").val(other_members)

		var leftDropBox_members = [];
		$(".leftDropBox .dropBox").each(function () {
			if ($(this).find('.dragImg').length) {
				leftDropBox_members.push($(this).find('.dragImg').attr('value'));
			} else {
				leftDropBox_members.push(0);
			}
		})

		var rightDropBox_members = [];
		$(".rightDropBox .dropBox").each(function () {
			if ($(this).find('.dragImg').length) {
				rightDropBox_members.push($(this).find('.dragImg').attr('value'));
			} else {
				rightDropBox_members.push(0);
			}
		})

		var buttomDropBox_members = [];
		$(".bottomDropBox .dropBox").each(function () {
			if ($(this).find('.dragImg').length) {
				buttomDropBox_members.push($(this).find('.dragImg').attr('value'));
			} else {
				buttomDropBox_members.push(0);
			}
		})

		var topDropBox_members = [];
		$(".topDropBox .dropBox").each(function () {
			if ($(this).find('.dragImg').length) {
				topDropBox_members.push($(this).find('.dragImg').attr('value'));
			} else {
				topDropBox_members.push(0);
			}
		})

		$("#selectedleft_padller_membersValue").val(leftDropBox_members)
		$("#selectedright_padller_membersValue").val(rightDropBox_members)
		$("#selecteddrummer_padller_membersValue").val(topDropBox_members)
		$("#selectedspare_padller_membersValue").val(buttomDropBox_members)

		$.ajax({
			type: "POST",
			url: "/create-lineup",
			data: $("#saverosterdetail").serialize()
		}).done(function (msg) {
			if (msg == "true") {
				$('#loading-spinner').addClass('hide');
				if (optionType == "create") {
					$("#lineup_msg_succ_msgs").text("LineUp Created")
				} else {
					$("#lineup_msg_succ_msgs").text("LineUp Updated")
				}

				$("html, body").animate({ scrollTop: 0 }, "slow");
				setTimeout(function () {
					sessionStorage.setItem("activeTab", "#lineup");
					location.reload()
				}, ONE_SEC);
			}
			else {
				if (optionType == "create") {
					$("#lineup_msg_err_msgs").text("LineUp Not Created")
				} else {
					$("#lineup_msg_err_msgs").text("LineUp Not Updated")
				}

				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('#loading-spinner').addClass('hide');
			}
		});
	}

});

$(document).on('click', "#removeLineupBtn", function (e) {
	e.preventDefault();
	var lineupId = $(this).attr('value');
	if (lineupId) {
		//Get the individual user detail
		$.ajax({
			url: "/delete-lineup",
			type: "POST",
			data: { "lineupid": lineupId },
			success: function (data) {
				$("#removeLineupModal").modal("hide");
				sessionStorage.setItem('activeTab', '#lineup')
				location.reload()
			}
		});
	}
});

$(document).on('click', "#deleteLineup", function (e) {
	e.preventDefault();
	var rosterId = $(this).attr('value');
	if (rosterId) {
		$("#removeLineupModal").modal("show");
		$("#removeLineupBtn").val(rosterId)
	}
});

$(document).ready(function () {
	$('#accselectmultiple').select2();
	dragDropPaddlers();
	$('.js-example-basic-multiple').select2();
});

// $(window).load(function () {
//     dragDropPaddlers();

// });

/** Dynamically update the LineUp slot calculation */
function lineupCount() {
	var rightDropBox_membersWeight = 0;
	var leftDropBox_membersWeight = 0;
	var buttomDropBox_membersWeight = 0;
	var topDropBox_membersWeight = 0;
	var maleCount = 0
	var femalecount = 0
	var filledcount = 0
	$(".bottomDropBox .dropBox").each(function () {
		if ($(this).find('.dragImg').length) {
			weight = $(this).find('.dragImg').attr('data-weight')
			gender = $(this).find('.dragImg').attr('data-classes')
			filledcount = filledcount + 1
			if (weight != "null" && weight != "0" && weight != "") {
				buttomDropBox_membersWeight = buttomDropBox_membersWeight + Number(weight)
			}

			if (gender == "W") {
				femalecount = femalecount + 1
			} else if (gender == "M") {
				maleCount = maleCount + 1
			}
		}
	})

	$(".topDropBox .dropBox").each(function () {
		if ($(this).find('.dragImg').length) {
			weight = $(this).find('.dragImg').attr('data-weight')
			gender = $(this).find('.dragImg').attr('data-classes')
			filledcount = filledcount + 1
			if (weight != "null" && weight != "0" && weight != "") {
				topDropBox_membersWeight = topDropBox_membersWeight + Number(weight)
			}

			if (gender == "W") {
				femalecount = femalecount + 1
			} else if (gender == "M") {
				maleCount = maleCount + 1
			}
		}
	})


	$(".leftDropBox .dropBox").each(function () {
		if ($(this).find('.dragImg').length) {
			weight = $(this).find('.dragImg').attr('data-weight')
			gender = $(this).find('.dragImg').attr('data-classes')
			filledcount = filledcount + 1
			if (weight != "null" && weight != "0" && weight != "") {
				leftDropBox_membersWeight = leftDropBox_membersWeight + Number(weight)
			}

			if (gender == "W") {
				femalecount = femalecount + 1
			} else if (gender == "M") {
				maleCount = maleCount + 1
			}
		}
	})

	$(".rightDropBox .dropBox").each(function () {
		if ($(this).find('.dragImg').length) {
			weight = $(this).find('.dragImg').attr('data-weight')
			gender = $(this).find('.dragImg').attr('data-classes')
			filledcount = filledcount + 1
			if (weight != "null" && weight != "0" && weight != "") {
				rightDropBox_membersWeight = rightDropBox_membersWeight + Number(weight)
			}

			if (gender == "W") {
				femalecount = femalecount + 1
			} else if (gender == "M") {
				maleCount = maleCount + 1
			}
		}
	})


	$("#rightsideweight").text(rightDropBox_membersWeight + " lbs")
	$("#leftsideweight").text(leftDropBox_membersWeight + " lbs")
	$("#frontsideweight").text(topDropBox_membersWeight + " lbs")
	$("#rearsideweight").text(buttomDropBox_membersWeight + " lbs")
	$("#lineupmencount").text(maleCount)
	$("#lineupwomencount").text(femalecount)
	$("#slotFilledCount").text(filledcount + " of 22")
}
//setTimeout(dragDropPaddlers,10);

function dragDropPaddlers() {
	lineupCount()
	var dragItem, dropItem, dragclass, dragAgainItem;
	var $dragArea = $("#dragArea"),
		$dropArea = $("#dropArea");
	$(".dragDrop, .dropBox").draggable({
		scroll: true,
		helper: "clone",
		//   /appendTo: '.dropContainer',
		containment: "#dragdropcontainer",
		revert: true,

		create: function (event, ui) {
			// console.log("hhhhh")

		},
		start: function (event, ui) {
			event.stopPropagation();
			dragItem = $(this).attr("data-position")
			//$(this).removeAttr("data-toggle");
			console.log("dragffff" + dragItem)
			$(".dragItem").css("z-index", "98");
			$(this).css("z-index", "1000");
		},

		drag: function (event, ui) {
			//console.log("aaa")

		},
		revert: function (is_valid_drop) {

			if (is_valid_drop) {
				lineupCount()
			}
			$('[data-toggle="tooltip"]').tooltip();
		},
		stop: function (event, ui) {
		}


	});

	$(".dropBox").droppable(
		{
			accept: ".dragBox",
			drop: function (event, ui) {
				$(this).html(ui.draggable.html())
				userId = ui.draggable.attr('id')
				ui.draggable.detach();

				$(this).addClass('droppedItem');
				//$(this).find('.dragImg').removeAttr("data-toggle");
				if (dragItem === "leftside") {
					$(this).attr('data-position', "leftside");
				} else if (dragItem === "rightside") {
					$(this).attr('data-position', "rightside");
				} else if (dragItem === 'otherside') {
					$(this).attr('data-position', "otherside");
				} else if (dragItem === 'bothside') {
					$(this).attr('data-position', "bothside");
				}
				console.log(dragItem);

				$(".droppedItem,.dragDrop").draggable({
					helper: "clone",
					revert: true,
					create: function (event, ui) {
						console.log("eaaa")
					},
					start: function (event, ui) {
						console.log("c")
						// dragAgainItem = this.getAttribute("data-position");
						// dragItem = dragAgainItem;
					},
					stop: function (event, ui) {
					},
					drag: function (event, ui) {

						dragItem1 = this.getAttribute("data-position");
						dragItem = dragItem1;
						console.log(dragItem)
					},
					revert: function (is_valid_drop) {
						//console.log(is_valid_drop);
						if (is_valid_drop) {
							lineupCount()
							$(this).find('.dragItem').remove();
							$(".drag-profle-img").removeClass("ui-droppable");
							$(".drag-profle-img .droppedItem").removeClass("dropBox");
							$(".drag-profle-img .droppedItem").addClass("dragBox");
							$(".drag-profle-img .droppedItem").addClass("dragDrop");
							$(".droppedItem .dropDefaultTxt").remove();
							$(".drag-profle-img .droppedItem").removeClass("ui-droppable");
							$("#dropArea .droppedItem").append("<span class='dropDefaultTxt'>P</span>");
							$(".dragBox").removeClass("droppedItem");
							$(this).removeAttr("data-position");
							$(this).removeClass("ui-draggable ui-draggable-handle");
							$(".drag-profle-img .dragBox").addClass("ui-draggable ui-draggable-handle");
							$('[data-toggle="tooltip"]').tooltip();
						}

					},
				});

			}
		});

	$('#leftSidePaddlers,#rightSidePaddlers,#othersidePaddlers,#bothsidePaddlers').droppable(
		{
			accept: ".droppedItem",
			drop: function (event, ui) {
				$(ui.draggable).find('.dragItem .dragImg').attr("data-toggle", "tooltip")
				if (dragItem == 'leftside') {
					$(ui.draggable).addClass('leftteammembers')
					$(ui.draggable).clone().appendTo("#leftSidePaddlers");
				} else if (dragItem == 'rightside') {
					$(ui.draggable).addClass('rightteammembers')
					$(ui.draggable).clone().appendTo("#rightSidePaddlers");
				} else if (dragItem == 'otherside') {
					$(ui.draggable).addClass('otherteammembers')
					$(ui.draggable).clone().appendTo("#othersidePaddlers");
				} else if (dragItem == 'bothside') {
					$(ui.draggable).addClass('bothteammembers')
					$(ui.draggable).clone().appendTo("#bothsidePaddlers");
				}

				$(".dragBox,.droppedItem").draggable({
					helper: "clone",
					revert: true,
					start: function (event, ui) {
						var dragItem111 = this.getAttribute("data-position");
						dragItem = dragItem111;
						console.log("e")
						console.log(dragItem)

					},
					create: function (event, ui) {
					},
					drag: function (event, ui) {
					},
					revert: function (is_valid_drop) {

						if (is_valid_drop) {

							$(this).find('.dragItem').remove();
							$(".drag-profle-img").removeClass("ui-droppable");
							$(".drag-profle-img .droppedItem").removeClass("dropBox");
							$(".drag-profle-img .droppedItem").addClass("dragBox");
							$(".drag-profle-img .droppedItem").addClass("dragDrop");
							$(".droppedItem .dropDefaultTxt").remove();
							$(".drag-profle-img .droppedItem").removeClass("ui-droppable");
							$("#dropArea .droppedItem").append("<span class='dropDefaultTxt'>P</span>");
							$(".dragBox").removeClass("droppedItem");
							$(this).removeAttr("data-position");
							$(this).removeClass("ui-draggable ui-draggable-handle");
							$(".drag-profle-img .dragBox").addClass("ui-draggable ui-draggable-handle");



							if (dragItem == 'leftside') {
								$(".drag-profle-img .droppedItem").addClass("leftteammembers");
							} else if (dragItem == 'rightside') {
								$(".drag-profle-img .droppedItem").addClass("rightteammembers");
							} else if (dragItem == 'otherside') {
								$(".drag-profle-img .droppedItem").addClass("otherteammembers");
							} else if (dragItem == 'bothside') {
								$(".drag-profle-img .droppedItem").addClass("bothteammembers");
							}

							$('[data-toggle="tooltip"]').tooltip();
							setTimeout(function () {
								lineupCount()

							}, SEC_NUM);
						}
						console.log("eDDDD")

					},
					stop: function (event, ui) {
					}

				})
			}
		});


}

$(function () {
	$(document).on('click', "#teameventregister", function (e) {
		e.preventDefault();
		//var rosterId = $(this).attr('value');
		$("#teameventregistermodel").modal("show");
		$('.add_more_roster_event').attr('data-index', 1)
		$("#selectedteamname").val("")
		$("#searchteamname").val("")
		$("#searchteamname").removeClass('empty-err');
		$("#selectedteamname").removeClass('empty-err');
		resetRosterClassDivision()
		$('#roste_hide_show').addClass('hide')
		$('#roste_list_hide_show').addClass('hide')
		$('#nonroste_hide_show').addClass('hide')
		$('#nondivision_hide_show').addClass('hide')
		$('#additional_practice_quantity_hide').addClass('hide')
		$("#event_register_message").val("")
		$("#additional_practice_quantity").val("0")
		$('input[name="waiverreminderstatus"]:radio').iCheck('uncheck');
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text("")
		$(".event-quality-size").each(function () {
			$(this).val(0)
		})
	});

	$("#searchteamname").autocomplete({
		appendTo: ".searchteamappend",
		delay: 500,
		source: function (request, response) {
			$("#eventregisterteam").val("")
			$("#searchrostername").val("")
			$("#eventregisterroster").val("")
			$('#searchrostername').attr("disabled", true)
			$('#loading-spinner').removeClass('hide');
			resetRosterClassDivision()
			$('#roste_hide_show').addClass('hide')
			$('#roste_list_hide_show').addClass('hide')
			$('#nonroste_hide_show').addClass('hide')
			$('#nondivision_hide_show').addClass('hide')
			$.ajax({
				url: "/get-Teamlist-autocomplete",
				type: "GET",
				data: { "searchText": request.term.trim() },
				success: function (teamListdata) {
					tempteamListdata = JSON.stringify(teamListdata)
					if (tempteamListdata != "null") {
						teamListdataValue = JSON.parse(tempteamListdata)
						if (teamListdataValue.length > 0) {
							$('#searchrostername').attr("disabled", false)
							$('#loading-spinner').addClass('hide');
							response($.map(teamListdataValue, function (item) {
								return {
									label: item.team_name,
									value: item.team_name,
									temp: item.id
								}
							}));
						} else {
							var data = [{
								label: "No Result Found",
								value: "No Result Found",
								temp: 0
							}];
							$('#loading-spinner').addClass('hide');
							response(data)
						}
					} else {
						var data = [{
							label: "No Result Found",
							value: "No Result Found",
							temp: 0
						}];
						$('#loading-spinner').addClass('hide');
						response(data)
					}
					//response( data );
				}
			});
		},
		minLength: 3,
		select: function (event, ui) {
			$("#searchteamname").removeClass('empty-err');
			$("#selectedteamname").removeClass('empty-err');
			if (ui.item.temp != 0) {
				$("#event_div_clas_err_msgs").text('');
				$("#event_reg_succ_msgs").text("")
				$("#event_reg_err_msgs").text("")
				selectedTeamValue = ui.item.temp
				selectedTeamName = ui.item.label
				$("#eventregisterteam").val(selectedTeamValue)
				$("#tempeventregisterteamid").val(selectedTeamValue)
				$("#registerteamSeletedName").val(selectedTeamName)
				$('#loading-spinner').removeClass('hide');
				$('#selectregisterteam').val("true")
				$("#currentregistratioStep").val("")
				$('#create_team_event_form').submit();
			} else {
				setTimeout(function () {
					$("#searchteamname").val("")
				}, 200);
				$("#tempeventregisterteamid").val("")
				$("#eventregisterteam").val("")
				$('#selectregisterteam').val("false")
			}
		},
		error: function (xhr) {
			console.log(xhr)
		}
	});

	$("#searchteamname").focusout(function () {
		if ($("#searchteamname").val().length <= 2 || $("#eventregisterteam").val() == "") {
			clearautocomplete()
		}
	});

	function clearautocomplete() {
		$("#searchteamname").val("")
		$("#eventregisterteam").val("")
		$('#searchrostername').attr("disabled", true)
		$("#searchrostername").val("")
		$("#eventregisterroster").val("")
	}

	$("#searchrostername").autocomplete({
		appendTo: ".searchrosterappend",
		delay: 500,
		source: function (request, response) {
			$("#eventregisterroster").val("")
			var teamId = $("#eventregisterteam").val()
			if (teamId != "" && teamId) {
				$('#loading-spinner').removeClass('hide');
				$.ajax({
					url: "/get-rosterlist-teamid",
					type: "GET",
					data: { "searchText": request.term.trim(), "team_id": teamId },
					success: function (rosterListdata) {
						if (rosterListdata != "null") {
							rosterListdataValue = JSON.parse(rosterListdata)
							if (rosterListdataValue.length > 0) {
								$('#loading-spinner').addClass('hide');
								response($.map(rosterListdataValue, function (item) {
									return {
										label: item.name,
										value: item.name,
										temp: item.id
									}
								}));
							} else {
								var data = [{
									label: "No Result Found",
									value: "No Result Found",
									temp: 0
								}];
								$('#loading-spinner').addClass('hide');
								response(data)
							}
						} else {
							var data = [{
								label: "No Result Found",
								value: "No Result Found",
								temp: 0
							}];
							$('#loading-spinner').addClass('hide');
							response(data)
						}
					}
				});
			} else {
				clearautocomplete()
			}
		},
		minLength: 3,
		select: function (event, ui) {
			if (ui.item.temp != 0) {
				$("#eventregisterroster").val(ui.item.temp)
			} else {
				setTimeout(function () {
					$("#searchrostername").val("")
				}, 200);
				$("#eventregisterroster").val("")
			}
		},
		error: function (xhr) {
			console.log(xhr)
		}
	});

	$("#searchrostername").focusout(function () {
		if ($("#searchrostername").val().length <= 2) {
			$("#searchrostername").val("")
			$("#eventregisterroster").val("")
		}
	});

	$(document).on('click', "#teamotherregister", function (e) {
		e.preventDefault();
		$("#teamOtherRegisterModal").modal("show");
	});

	$(document).on("keypress keyup blur scroll", '.event-quality-size', function (event) {
		this.value = this.value.replace(/[^0-9\.]/g, '');
		if (this.value.split('.').length > 1) {
			this.value = this.value.replace(/\.+$/, "");
		}
		var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;

		if (!valid) {
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});

	$(document).on("keypress keyup blur scroll", '.event-user-count-size', function (event) {
		this.value = this.value.replace(/[^0-9\.]/g, '');
		if (this.value.length > 5) {
			this.value = this.value.substring(0, this.value.length - 1);
		}
	});

	$(document).on("keypress keyup blur scroll", '.event-payment-size', function (event) {
		this.value = this.value.replace(/[^0-9\.]/g, '');
		if (this.value.split('.').length > 1) {
			this.value = this.value.replace(/\.+$/, "");
		}
		var valid = /^\d{0,5}(\.\d{0,2})?$/.test(this.value),
			val = this.value;

		if (!valid) {
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});


	$(document).on("keypress keyup blur scroll", '.event-quality-size-range', function (event) {
		this.value = this.value.replace(/\D/g, '');
		this.value = this.value.replace(/\.+$/, "");
		var valid = /^\d{0,4}?$/.test(this.value),
			val = this.value;

		if (!valid) {
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});

	$('.eventviewregister .iCheck-helper').click(function () {
		$("#additional_practice_quantity").val("0")
		if ($('input[type=radio][name=additional_practice_bool]:checked').val() == 'No') {
			$("#additional_practice_quantity_hide").addClass('hide')
		} else if ($('input[type=radio][name=additional_practice_bool]:checked').val() == 'Yes') {
			$("#additional_practice_quantity_hide").removeClass('hide')
		}
	});


	$('.eventrostetsubmitornot .iCheck-helper').click(function () {
		$("#event_div_clas_err_msgs").text("")
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'No') {
			$(".hideOrShowRosterSelectTag").attr('disabled', true);
		} else if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'Yes') {
			if ($('#teamRosterDetail').val() == "null") {
				$("#rosternotineventregmodel").modal("show");
				$('input[id="submitrosterlateryes"]:radio').iCheck('uncheck');
				$('input[id="submitrosterlaterno"]:radio').iCheck('check');
			} else {
				$(".hideOrShowRosterSelectTag").attr('disabled', false);
			}
		}
	});

	$('#submit_event_register_step1').click(function (e) {
		$('#loading-spinner').removeClass('hide')
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text("")
		isFormValid = true;
		isSelectedTeamValid = true
		isSeletedDivisionClass = true
		isSeletedRosterStatus = true

		if ($("#searchteamname").val() == "") {
			$("#searchteamname").addClass('empty-err');
			isFormValid = false;
		} else {
			isFormValid = true;
			$("#searchteamname").removeClass('empty-err');
		}

		if ($("#selectedteamname").val() == "") {
			$("#selectedteamname").addClass('empty-err');
			isSelectedTeamValid = false;
		} else {
			isSelectedTeamValid = true;
			$("#selectedteamname").removeClass('empty-err');
		}

		var selectedDivisionArray = [];
		var selectedClassArray = [];
		var selectedRosterIdArray = [];
		var selectedBoatTypeArray = [];

		var onlyselectedDivisionArray = [];
		var onlyselectedClassArray = [];
		var onlyselectedDistanceArray = [];

		var completeselectedDistanceClassDistanceboattypeArray = [];

		$(".table_tr_selected_divition_classes_roster").each(function () {

			tempselectedDivisionValue = $(this).find("td:eq(0)").attr('data-index')
			tempselectedClassesValue = $(this).find("td:eq(1)").attr('data-index')
			tempselectedBoatTypeValue = $(this).find("td:eq(2)").attr('data-index')
			tempselectedrosterValue = $(this).find('#eventregisterroster').val()
			if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'Yes') {
				if (tempselectedDivisionValue && tempselectedDivisionValue != "" && tempselectedClassesValue && tempselectedClassesValue != "" && tempselectedrosterValue && tempselectedrosterValue != "0") {
					selectedDivisionArray.push(tempselectedDivisionValue.trim())
					selectedClassArray.push(tempselectedClassesValue.trim())
					selectedRosterIdArray.push(tempselectedrosterValue.trim())
					if (tempselectedBoatTypeValue != "" && tempselectedBoatTypeValue) {
						selectedBoatTypeArray.push(tempselectedBoatTypeValue.trim())
					}
				}
			}

			if (tempselectedBoatTypeValue && tempselectedBoatTypeValue != "" && tempselectedDivisionValue && tempselectedDivisionValue != "" && tempselectedClassesValue && tempselectedClassesValue != "") {

				tempValue = tempselectedDivisionValue.trim() + "_" + tempselectedClassesValue.trim() + "_" + tempselectedBoatTypeValue.trim()

				if (jQuery.inArray(tempValue, completeselectedDistanceClassDistanceboattypeArray) == -1) {

					onlyselectedDivisionArray.push(tempselectedDivisionValue.trim())
					onlyselectedClassArray.push(tempselectedClassesValue.trim())
					onlyselectedDistanceArray.push(tempselectedBoatTypeValue.trim())
					completeselectedDistanceClassDistanceboattypeArray.push(tempValue)
				}
			} else if (tempselectedDivisionValue && tempselectedDivisionValue != "" && tempselectedClassesValue && tempselectedClassesValue != "") {
				tempValue = tempselectedDivisionValue.trim() + "_" + tempselectedClassesValue.trim()
				if (jQuery.inArray(tempValue, completeselectedDistanceClassDistanceboattypeArray) == -1) {
					onlyselectedDivisionArray.push(tempselectedDivisionValue.trim())
					onlyselectedClassArray.push(tempselectedClassesValue.trim())
					completeselectedDistanceClassDistanceboattypeArray.push(tempValue)
				}
			}
		})

		$('#SelectedDivisionArray').val(selectedDivisionArray)
		$('#SelectedClassArray').val(selectedClassArray)
		$('#SelectedRosterIdArray').val(selectedRosterIdArray)
		$('#OnlySelectedDivisionArray').val(onlyselectedDivisionArray)
		$('#OnlySelectedClassArray').val(onlyselectedClassArray)
		$('#OnlySelectedDistanceArray').val(onlyselectedDistanceArray)
		$('#SelectedBoatTypeArray').val(selectedBoatTypeArray)

		if (isFormValid && isSelectedTeamValid) {
			if (onlyselectedDivisionArray.length == 0 || onlyselectedClassArray.length == 0) {
				isSeletedDivisionClass = false
				$("#event_reg_err_msgs").text("Please select division & class")
			} else if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'Yes') {
				//|| selectedBoatTypeArray.length == 0
				if (selectedDivisionArray.length == 0 || selectedClassArray.length == 0 || selectedRosterIdArray.length == 0) {
					isSeletedRosterStatus = false
					if (selectedRosterIdArray.length == 0) {
						$("#event_reg_err_msgs").text("Please select the roster")
					}
				}
			}
		}

		if (isFormValid && isSelectedTeamValid && isSeletedDivisionClass && isSeletedRosterStatus) {
			$("#currentregistratioStep").val("1")
			$('#selectregisterteam').val("true")
			$("#cancelEventRegistrationstatus").val("false")
			$('#create_team_event_form').submit();
		} else {
			$('#loading-spinner').addClass('hide')
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false
		}

	});

	$('#submit_event_register_step2').click(function (e) {
		$('#loading-spinner').removeClass('hide')
		$('#participantscount').removeClass("empty-err")
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$('#event_reg_err_msgs4').text("")
		$("#event_div_clas_err_msgs").text("")
		$("#cancelEventRegistrationstatus").val("false")
		$("#currentregistratioStep").val("2")
		$('#selectregisterteam').val("true")

		isvalidStatus = true

		$('[name="per_person_quantity[]"]').each(function () {
			idValue = this.value.trim()
			if (idValue == "") {
				isvalidStatus = false
				$(this).addClass("empty-err")
			}else {
				$(this).removeClass("empty-err")
			}
		});

		if ($("#participantscount").length) {
			if($("#participantscount").val().trim() == "" ){
				isvalidStatus = false
			}
		}

		if (isvalidStatus) {
			$('#create_team_event_form').submit();
		} else {
			$('#loading-spinner').addClass('hide')
			$('#event_reg_err_msgs4').text("Please add the number of participants in the field below.")
			$('#participantscount').addClass("empty-err")
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false
		}
	});

	$(document).on('click', "#cancelEventRegistration", function (e) {
		$('#loading-spinner').removeClass('hide')
		$("#currentregistratioStep").val("0")
		$('#selectregisterteam').val("false")
		$("#cancelEventRegistrationstatus").val("true")
		sessionStorage.setItem('eventactiveTab', 'eventhomeactivetab')
		$('#create_team_event_form').submit();
	});

	if (completeRegistrationEventStatus == "true" || sessionStorage.getItem('teameventregisterstatus')) {
		completeRegistrationEventStatus = false
		sessionStorage.removeItem('teameventregisterstatus')
		$('#defaulteventmodelwithheading').modal("show")
		$('#defaulteventmodelwithheadingText').text("THANK YOU FOR YOUR REGISTRATION")
		if(completeEventRegistrationPaymentDoneStatus == "true")
		{
			completeEventRegistrationPaymentDoneStatus ="false"
			$('#defaulteventmodelwithheading_Body').html("<p>A registration message has been sent to the club organizer who will follow up with you shortly. Thank you kindly for your registration request.</p><br><p> Payment success.</p>")

		}else {
			$('#defaulteventmodelwithheading_Body').html("<p>A registration message has been sent to the club organizer who will follow up with you shortly. Thank you kindly for your registration request.</p>")

		}
		setTimeout(function () {
			$("#defaulteventmodelwithheading").modal('hide')
		}, EIGHT_SEC);
	}

	$('#submit_event_register_step3').click(function (e) {
		$('#loading-spinner').removeClass('hide')
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs3").text("")
		$("#event_div_clas_err_msgs").text("")
		$("#event_reg_err_msgs3").addClass('hide');
		isFormValid = true
		$('[name="event_question_answer[]"]').each(function () {
			idValue = this.value.trim()
			if (idValue.length > 200 || idValue == "") {
				isFormValid = false
				$(this).addClass("empty-err")
				$("#event_reg_err_msgs3").removeClass('hide');
				$("#event_reg_err_msgs3").text("Maximum character length is 150.")
			} else {
				$(this).removeClass("empty-err")
			}
		});


		if (isFormValid) {
			$("#currentregistratioStep").val("3")
			$('#selectregisterteam').val("true")
			$("#cancelEventRegistrationstatus").val("false")
			$('#create_team_event_form').submit();
		} else {
			$('#loading-spinner').addClass('hide')
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false
		}
	});

	$('#submit_event_register_step3_with_payment').click(function (e) {
		
		$('#eventpaymentstatusClick').val("true")
		$('#submit_event_register_step3').trigger('click');
	});
	

	$('#submit_event_register_step4_with_payment').click(function (e) {
		$('#loading-spinner').removeClass('hide')
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs3").text("")
		$("#event_div_clas_err_msgs").text("")
		$("#event_reg_err_msgs3").addClass('hide');

			$("#currentregistratioStep").val("4")
			$('#selectregisterteam').val("true")
			$("#cancelEventRegistrationstatus").val("false")
			$('#create_team_event_form').submit();
	});

	$('#cancelWithoutPayment').click(function (e) {
		e.preventDefault()
		sessionStorage.setItem("teameventregisterstatus","true")
		window.location.href = "/event-view/" + $('#registereventslug').val();
	});

	$('#register_team_event_button').click(function (e) {
		e.preventDefault()
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text("")
		isFormValid = true;
		isFormRosterValid = true;
		isSelectedTeamValid = true
		isSeletedQuantity = true;
		isSeletedDivisionClass = true
		isSeletedRosterStatus = true

		if ($("#searchteamname").val() == "") {
			$("#searchteamname").addClass('empty-err');
			isFormValid = false;
		} else {
			isFormValid = true;
			$("#searchteamname").removeClass('empty-err');
		}

		if ($("#selectedteamname").val() == "") {
			$("#selectedteamname").addClass('empty-err');
			isSelectedTeamValid = false;
		} else {
			isSelectedTeamValid = true;
			$("#selectedteamname").removeClass('empty-err');
		}

		var selectedDivisionArray = [];
		var selectedClassArray = [];
		var selectedRosterIdArray = [];

		var onlyselectedDivisionArray = [];
		var onlyselectedClassArray = [];

		$(".table_tr_selected_divition_classes_roster").each(function () {

			tempselectedDivisionValue = $(this).find("td:eq(0)").attr('data-index')
			tempselectedClassesValue = $(this).find("td:eq(1)").attr('data-index')
			tempselectedrosterValue = $(this).find('#eventregisterroster').val()
			if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'Yes') {
				if (tempselectedDivisionValue && tempselectedDivisionValue != "" && tempselectedClassesValue && tempselectedClassesValue != "" && tempselectedrosterValue && tempselectedrosterValue != "0") {
					selectedDivisionArray.push(tempselectedDivisionValue)
					selectedClassArray.push(tempselectedClassesValue)
					selectedRosterIdArray.push(tempselectedrosterValue)
				}
			}

			if (tempselectedDivisionValue && tempselectedDivisionValue != "" && tempselectedClassesValue && tempselectedClassesValue != "") {
				if (jQuery.inArray(tempselectedDivisionValue, onlyselectedDivisionArray) == -1 && jQuery.inArray(tempselectedClassesValue, onlyselectedClassArray) == -1) {
					onlyselectedDivisionArray.push(tempselectedDivisionValue)
					onlyselectedClassArray.push(tempselectedClassesValue)
				}
			}
		})

		$('#SelectedDivisionArray').val(selectedDivisionArray)
		$('#SelectedClassArray').val(selectedClassArray)
		$('#SelectedRosterIdArray').val(selectedRosterIdArray)
		$('#OnlySelectedDivisionArray').val(onlyselectedDivisionArray)
		$('#OnlySelectedClassArray').val(onlyselectedClassArray)

		if (onlyselectedDivisionArray.length == 0 || onlyselectedClassArray.length == 0) {
			isSeletedDivisionClass = false
			$("#event_reg_err_msgs").text("Please select division & class")
		} else if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'Yes') {
			if (selectedDivisionArray.length == 0 || selectedClassArray.length == 0 || selectedRosterIdArray.length == 0) {
				isSeletedRosterStatus = false
				$("#event_reg_err_msgs").text("Please select the roster")
			}
		}

		$(".event-quality-size").each(function () {
			if (!$(this).val() && $(this).val() == "") {
				$(this).addClass('empty-err');
				isSeletedQuantity = false
			} else {
				$(this).removeClass('empty-err');
			}
		})
		if (isFormValid && isSelectedTeamValid && isSeletedQuantity && isSeletedDivisionClass && isSeletedRosterStatus) {
			$('#loading-spinner').removeClass('hide')
			$.ajax({
				type: "GET",
				url: "/team-check-team-already-register-in-event",
				data: { "team_Id": $('#eventregisterteam').val() },
			}).done(function (resultData) {
				if (resultData == "false") {
					submitTeamEventRegister()
				} else {
					$("#event_reg_err_msgs").text("Already Registered")
					$('#loading-spinner').addClass('hide')
					$("#teameventregistermodel").animate({ scrollTop: 0 }, "slow");
				}
			});

		} else {
			$('#loading-spinner').addClass('hide')
			$("#teameventregistermodel").animate({ scrollTop: 0 }, "slow");
		}
	});


	function submitTeamEventRegister() {
		$.ajax({
			type: "POST",
			url: "/team-event-register",
			data: $("#eventregisterFormData").serialize()
		}).done(function (ajaxValueData) {
			if (ajaxValueData == "true") {
				$('#defaulteventmodelwithheading').modal("show")
				$('#defaulteventmodelwithheadingText').text("THANK YOU FOR YOUR REGISTRATION")
				$('#defaulteventmodelwithheading_Body').text("A registration message has been sent to the club organizer who will follow up with you shortly. Thank you kindly for your registration request.")

				setTimeout(function () {
					$("#defaulteventmodelwithheading").modal('hide')
					location.reload()
				}, EIGHT_SEC);
				$('#loading-spinner').addClass('hide');
			} else {
				$("#event_reg_err_msgs").text("Team Not Register with Event")
				$('#loading-spinner').addClass('hide')
				$("#teameventregistermodel").animate({ scrollTop: 0 }, "slow");
				$('#loading-spinner').addClass('hide')
			}
		});
	}
	$("#searchrosterEventId").autocomplete({
		appendTo: ".searchrostereventIdappend",
		delay: 500,
		source: function (request, response) {
			$("#rosterEventId").val("0")
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				url: "/get-eventlist-autocomplete",
				type: "GET",
				data: { "searchText": request.term.trim() },
				success: function (eventListdata) {
					tempeventListdata = JSON.stringify(eventListdata)
					if (tempeventListdata != "null") {
						eventListdataValue = JSON.parse(tempeventListdata)
						if (eventListdataValue.length > 0) {
							$('#loading-spinner').addClass('hide');
							response($.map(eventListdataValue, function (item) {
								return {
									label: item.event_name,
									value: item.event_name,
									temp: item.id
								}
							}));
						} else {
							var data = [{
								label: "No Result Found",
								value: "No Result Found",
								temp: 0
							}];
							$('#loading-spinner').addClass('hide');
							response(data)
						}
					} else {
						var data = [{
							label: "No Result Found",
							value: "No Result Found",
							temp: 0
						}];
						$('#loading-spinner').addClass('hide');
						response(data)
					}
					//response( data );
				}
			});
		},
		minLength: 3,
		select: function (event, ui) {
			if (ui.item.temp != 0) {
				$("#rosterEventId").val(ui.item.temp)
			} else {
				setTimeout(function () {
					$("#searchrosterEventId").val("")
				}, 200);
				$("#rosterEventId").val("0")
			}
		},
		error: function (xhr) {
			console.log(xhr)
		}
	});

	$("#searchrosterEventId").focusout(function () {
		if ($("#searchrosterEventId").val().length <= 2 || $("#rosterEventId").val() == "0") {
			$("#searchrosterEventId").val("")
			$("#rosterEventId").val("0")
		}
	});

	$("#selectedteamname").change(function () {

		$("#event_div_clas_err_msgs").text('');
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		selectedTeamValue = this.value
		$("#eventregisterteam").val(selectedTeamValue)
		$("#tempeventregisterteamid").val(selectedTeamValue)
		$('#loading-spinner').removeClass('hide');
		$('#selectregisterteam').val("true")
		$("#currentregistratioStep").val("")
		$('#create_team_event_form').submit();
	});

	var event_register_team_roster_options = '';
	if ($("#teamRosterDetail").length) {
		tempValue = $("#teamRosterDetail").val()
		if (tempValue != "null" && tempValue != "") {
			event_register_team_roster_options += '<option value="0">Select Roster</option>';
			//tempselectedTeamRosterData =JSON.stringify(tempValue)
			tempselectedTeamRosterData = JSON.parse(tempValue)
			for (var i = 0; i < tempselectedTeamRosterData.length; i++) {
				event_register_team_roster_options += '<option value="' + tempselectedTeamRosterData[i].id + '">' + tempselectedTeamRosterData[i].name + '</option>';
			}
		}
	}

	function addRosterInEventRegisterOnlySingleClassAndDivision() {
		maleList = $("#maleDivision").val();
		femaleList = $("#womenDivision").val();
		mixedList = $("#mixedDivision").val();

		var tempmaleList = maleList.replace("[", "").replace("]", "").split(' ');
		var tempfemaleList = femaleList.replace("[", "").replace("]", "").split(' ');
		var tempmixedList = mixedList.replace("[", "").replace("]", "").split(' ');

		tempmaleListCount = 0
		tempfemaleListCount = 0
		tempmixedListCount = 0

		if (tempmaleList.length > 0 && tempmaleList[0] != "") {
			tempmaleListCount = tempmaleList.length
		}

		if (tempfemaleList.length > 0 && tempfemaleList[0] != "") {
			tempfemaleListCount = tempfemaleList.length
		}

		if (tempmixedList.length > 0 && tempmixedList[0] != "") {
			tempmixedListCount = tempmixedList.length
		}

		tempCount = tempmaleListCount + tempfemaleListCount + tempmixedListCount

		if ($('#eventDivisionChange option').length == 2 && tempCount == 1) {
			$('.div_class_roster_1').addClass('hide')
			$("#remove_margin_buttom_eve_reg").attr('style', 'margin-bottom: 0px;');
			var singleeventClassesChange = ""
			var singleeventClassesChangeText = ""
			var singleeventDivisionChange = $("#eventDivisionChange option:eq(1)").val()
			var singleeventDivisionChangeText = $("#eventDivisionChange option:eq(1)").text()
			if (jQuery.inArray(singleeventDivisionChange, tempmaleList) != -1) {
				singleeventClassesChange = "M"
				singleeventClassesChangeText = "Male"
			}
			if (jQuery.inArray(singleeventDivisionChange, tempfemaleList) != -1) {
				singleeventClassesChange = "W"
				singleeventClassesChangeText = "Women"
			}

			if (jQuery.inArray(singleeventDivisionChange, tempmixedList) != -1) {
				singleeventClassesChange = "MX"
				singleeventClassesChangeText = "Mixed"
			}
			addMoreClassAndDivisitionEventReg(singleeventClassesChange, singleeventDivisionChange, singleeventClassesChangeText, singleeventDivisionChangeText, false)
		} else {
			$("#remove_margin_buttom_eve_reg").attr('style', 'margin-bottom: 20px;');
			$("#removeEventRegisterTableHeading").addClass('hide');
			$('.div_class_roster_1').removeClass('hide')
		}
	}

	function checkClass_Division(onlyselectedDivisionValue, onlyselectedClassesValue, onlyselectedrosterValue, onlyselecteddistanceValue) {
		var divisionStatus = true;
		var tempdivisionCount = 0;
		$(".table_tr_selected_divition_classes_roster").each(function () {
			tempselectedDivisionValue = $(this).find("td:eq(0)").attr('data-index')
			tempselectedClassesValue = $(this).find("td:eq(1)").attr('data-index')
			tempselectedDistanceValue = $(this).find("td:eq(2)").attr('data-index')
			tempselectedrosterValue = $(this).find('#eventregisterroster').val()

			if (tempselectedDistanceValue != "" && tempselectedDistanceValue) {
				if (tempselectedDivisionValue.trim() == onlyselectedDivisionValue.trim() && onlyselectedClassesValue.trim() == tempselectedClassesValue.trim() && onlyselectedrosterValue.trim() == tempselectedrosterValue.trim() && onlyselecteddistanceValue.trim() == tempselectedDistanceValue.trim()) {
					tempdivisionCount = tempdivisionCount + 1
					if (tempdivisionCount > 1) {
						$("#event_reg_err_msgs").html('This roster has already been selected for this class,division,distance and boattype and  </br>(Note, the same roster can be used for diffrent class/division/distance/boattype combination)');
						divisionStatus = false
						$("html, body").animate({ scrollTop: 0 }, "slow");
						return false
					}
				}
			} else {
				if (tempselectedDivisionValue.trim() == onlyselectedDivisionValue.trim() && onlyselectedClassesValue.trim() == tempselectedClassesValue.trim() && onlyselectedrosterValue.trim() == tempselectedrosterValue.trim()) {
					tempdivisionCount = tempdivisionCount + 1
					if (tempdivisionCount > 1) {
						$("#event_reg_err_msgs").html('This roster has already been selected for this class,division,distance and boattype and  </br>(Note, the same roster can be used for diffrent class/division/distance/boattype combination)');
						divisionStatus = false
						$("html, body").animate({ scrollTop: 0 }, "slow");
						return false
					}
				}
			}
		})

		return divisionStatus
	}

	function resetRosterClassDivision() {
		$("#append_roster_division_class").find('.table_tr_selected_divition_classes_roster').remove()
		$('input[name="submitrosterlater"]:radio').iCheck('uncheck');
		$(".append_div_class_roster").each(function () {
			tempselectedIndexValue = $(this).find('#eventDivisionChange').attr('data-index')
			if (tempselectedIndexValue == 1) {
				tempselectedDivisionValue = $(this).find('#eventDivisionChange').val("")
				tempselectedClassesValue = $(this).find('#eventClassesChange').val("")
			} else {
				$(this).remove()
			}
		})
	}

	// Create Event Class Division Script
	$(document).on('change', "#eventDivisionChange", function (e) {
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text('');
		$('#addMoreRosterButton_hide').addClass('hide')

		selectedDivValue = this.value
		selectedIndexValue = $(this).attr('data-index')
		divisionArrayWithJosn = $("#divisionArrayWithJosn").val();
		if (selectedDivValue != "" && divisionArrayWithJosn != "null" && divisionArrayWithJosn != "") {
			selectedDivValue = selectedDivValue.split('_')


			var options = '';
			options += '<option value="">Select Class</option>';
			alreadMaleadd = false
			alreadWomenadd = false
			alreadMixedadd = false
			$.each(JSON.parse(divisionArrayWithJosn), function (key, value) {
				tempdivreplace = value.div.replace("{", "")
				tempdivreplace = tempdivreplace.replace("}", "")
				if (value.boat_type) {
					if ((selectedDivValue[1] == value.boat_type) && (selectedDivValue[0] == tempdivreplace)) {
						if (value.class == "M" && !alreadMaleadd) {
							alreadMaleadd = true
							options += '<option value="M">Men</option>';
						} else if (value.class == "W" && !alreadWomenadd) {
							alreadWomenadd = true
							options += '<option value="W">Women</option>';
						} else if (value.class == "MX" && !alreadMixedadd) {
							alreadMixedadd = true
							options += '<option value="MX">Mixed</option>';
						}

					}
				} else {
					if (selectedDivValue[0] == tempdivreplace) {
						if (value.class == "M" && !alreadMaleadd) {
							alreadMaleadd = true
							options += '<option value="M">Men</option>';
						} else if (value.class == "W" && !alreadWomenadd) {
							alreadWomenadd = true
							options += '<option value="W">Women</option>';
						} else if (value.class == "MX" && !alreadMixedadd) {
							alreadMixedadd = true
							options += '<option value="MX">Mixed</option>';
						}
					}
				}
			});

			$('.div_class_roster_' + selectedIndexValue).find('#event_clases_Hide').removeClass('hide')
			$('.div_class_roster_' + selectedIndexValue).find('#event_boat_type_Hide').addClass('hide')
			$('.div_class_roster_' + selectedIndexValue).find("select#eventClassesChange").empty().html(options);

		} else {
			$(this).val("")
			var options = '';
			options += '<option value="">Select Class</option>';
			$('.div_class_roster_' + selectedIndexValue).find('#event_clases_Hide').addClass('hide')
			$('.div_class_roster_' + selectedIndexValue).find('#event_boat_type_Hide').addClass('hide')
			$('.div_class_roster_' + selectedIndexValue).find("select#eventClassesChange").empty().html(options);
		}
	});

	$(document).on('change', "#eventClassesChange", function (e) {
		selectedClassesValue = this.value
		selectedDivisionValue = $('#eventDivisionChange').val()
		if (selectedClassesValue == "" || selectedDivisionValue == "") {
			$('#event_boat_type_Hide').addClass('hide')
			$('#addMoreRosterButton_hide').addClass('hide')
		} else {
			$('#event_boat_type_Hide').removeClass('hide')
			divisionArrayWithJosn = $("#divisionArrayWithJosn").val();
			//boatType =""
			if (divisionArrayWithJosn != "null" && divisionArrayWithJosn != "") {
				var options = '';
				var distanceValue = '';

				var temp200status = true
				var temp500status = true
				var temp1000status = true
				var temp2000status = true

				$.each(JSON.parse(divisionArrayWithJosn), function (key, value) {
					tempdivreplace = value.div.replace("{", "")
					tempdivreplace = tempdivreplace.replace("}", "")
					if (value.boat_type) {
						if ((selectedDivisionValue == tempdivreplace + "_" + value.boat_type) && (selectedClassesValue == value.class)) {
							distanceValue = value.distance
						}
					}
					if (distanceValue == "200" && temp200status) {
						options += "<option selected  value='200'>" + distanceValue + "</option>"
						temp200status = false
					} else if (distanceValue == "500" && temp500status) {
						options += "<option value='500'>" + distanceValue + "</option>"
						temp500status = false
					} else if (distanceValue == "1000" && temp1000status) {
						options += "<option value='1000'> 1km </option>"
						temp1000status = false
					} else if (distanceValue == "2000" && temp2000status) {
						options += "<option value='2000'> 2km </option>"
						temp2000status = false
					}
				});

				if (options != '') {
					$('#event_boat_type_Hide').removeClass('hide')
					$('.div_class_roster_' + selectedIndexValue).find("select#eventBoatTypeChange").empty().html(options);
				} else {
					$('#event_boat_type_Hide').addClass('hide')
				}
			} else {
				$('#event_boat_type_Hide').addClass('hide')
			}
			$('#addMoreRosterButton_hide').removeClass('hide')
		}
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text('')

	});

	$(document).on('change', "#eventregisterroster", function (e) {
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text('')
		var rosterSelectedId = this.value
		var tempRosterCount = 0
		tempselectedIndexValue = $(this).attr('data-index')
		tempselectedDivisionValue = $('.selected_divition_classes_roster' + tempselectedIndexValue).find("td:eq(0)").attr('data-index')
		tempselectedClassesValue = $('.selected_divition_classes_roster' + tempselectedIndexValue).find("td:eq(1)").attr('data-index')
		tempselectedDistanceValue = $('.selected_divition_classes_roster' + tempselectedIndexValue).find("td:eq(2)").attr('data-index')
		tempselectedrosterValue = $('.selected_divition_classes_roster' + tempselectedIndexValue).find('#eventregisterroster').val()
		classdivisionStatus = checkClass_Division(tempselectedDivisionValue, tempselectedClassesValue, tempselectedrosterValue, tempselectedDistanceValue)
		if (!classdivisionStatus) {
			$('.selected_divition_classes_roster' + tempselectedIndexValue).find('#eventregisterroster').val("0")
		}
	});

	var index_event_roster_add = 0
	$(document).on('click', ".add_more_roster_event", function (e) {
		e.preventDefault()
		index_event_roster_add = $(".table_tr_selected_divition_classes_roster").length
		$("#event_div_clas_err_msgs").text('')
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		tempeventClassesChange = $('#eventClassesChange').val()
		tempeventDivisionChange = $('#eventDivisionChange').val()
		tempeventBoatTypeChange = $('#eventBoatTypeChange').val()
		tempeventClassesChangeText = $('#eventClassesChange option:selected').text()
		tempeventDivisionChangeText = $('#eventDivisionChange option:selected').text()
		tempeventBoatTypeChangeText = $('#eventBoatTypeChange option:selected').text()
		$('#addMoreRosterButton_hide').addClass('hide')
		if (tempeventClassesChange && tempeventClassesChange != "" && tempeventDivisionChange && tempeventDivisionChange != "") {
			teampStatusFalse = true
			addMoreClassAndDivisitionEventReg(tempeventClassesChange, tempeventDivisionChange, tempeventClassesChangeText, tempeventDivisionChangeText, true, tempeventBoatTypeChange, tempeventBoatTypeChangeText)
		} else {
			$("#event_reg_err_msgs").text('Please select class & division')
			$("#teameventregistermodel").animate({ scrollTop: 0 }, "slow");
		}
	});

	function addMoreClassAndDivisitionEventReg(eventClassesChange, eventDivisionChange, eventClassesChangeText, eventDivisionChangeText, deleteOpionFlag, eventBoatTypeChange, eventBoatTypeChangeText) {
		$("#removeEventRegisterTableHeading").removeClass('hide');
		index_event_roster_add = index_event_roster_add + 1
		newhtml = ""
		newhtml += "<tr class='table_tr_selected_divition_classes_roster selected_divition_classes_roster" + index_event_roster_add + "'>";
		newhtml += "<td style='color:#bf5523' class='col-md-2' data-index='" + eventDivisionChange + "' >" + eventDivisionChangeText + "</td>";
		newhtml += "<td style='color:#bf5523' class='col-md-2' data-index='" + eventClassesChange + "' >" + eventClassesChangeText + "</td>";
		if (eventBoatTypeChangeText != "" && eventBoatTypeChange != "") {
			newhtml += "<td style='color:#bf5523' class='col-md-2' data-index='" + eventBoatTypeChange + "' >" + eventBoatTypeChangeText + "</td>";
		} else {
			newhtml += "<td class='col-md-2'></td>"
		}
		if (event_register_team_roster_options != "") {
			newhtml += "<td class='col-md-5' style='display: contents;'>";
			if ($('input[type=radio][name=submitrosterlater]:checked').val() == 'No') {
				newhtml += "<select disabled class='form-control hideOrShowRosterSelectTag' id='eventregisterroster'  data-index='" + index_event_roster_add + "'  name='eventregisterroster'></select></td>";
			} else {
				newhtml += "<select class='form-control hideOrShowRosterSelectTag' id='eventregisterroster'  data-index='" + index_event_roster_add + "'  name='eventregisterroster'></select></td>";
			}
		} else {
			newhtml += "<td class='col-md-2'>";
			newhtml += "</td>"
		}
		if (deleteOpionFlag) {
			newhtml += "<td class='col-md-1'><a id='table_tr_selected_divition_classes_roster_delete' class='btn-lg' style='color: black;' data-index='" + index_event_roster_add + "' href='#' role='button'><span class='glyphicon glyphicon-trash'></span></a></td>"
		} else {
			newhtml += "<td class='col-md-1'></td>"
		}

		newhtml += "</tr>";
		$("#roste_list_hide_show").removeClass('hide');
		$("#append_roster_division_class").append(newhtml);
		$(".selected_divition_classes_roster" + index_event_roster_add).find("select#eventregisterroster").html(event_register_team_roster_options);
		$('#eventClassesChange').val("")
		$('#eventDivisionChange').val("")
		$('#event_boat_type_Hide').addClass('hide')
	}

	$(document).on('click', "#table_tr_selected_divition_classes_roster_delete", function (e) {
		e.preventDefault();
		$("#event_reg_succ_msgs").text("")
		$("#event_reg_err_msgs").text("")
		$("#event_div_clas_err_msgs").text("")

		selectedIndexValue = $(this).attr('data-index')
		$(".selected_divition_classes_roster" + selectedIndexValue).remove()
		strCount = 0
		$("#table_tr_selected_divition_classes_roster_delete").each(function () {
			strCount = strCount + 1
		});

		if (strCount == 0) {
			$("#roste_list_hide_show").addClass('hide');
		} else {
			$("#roste_list_hide_show").removeClass('hide');
		}
	});


	$('.nav-tabs a[href="#eventMainPage"]').click(function () {
		$('#eventMainPage').removeClass('hide');
		$('#eventRegisterDetailList').addClass('hide');
		$('#eventTeamList').addClass('hide');
		$('#eventPaymentList').addClass('hide');
		$('#eventaccreditation').addClass('hide');
		$('#eventRosterRacingList').addClass('hide');
		$('#eventaccreditationList').addClass('hide'); 
		$('#eventWaiverList').addClass('hide');
		sessionStorage.setItem('eventactiveTab', 'eventhomeactivetab')
	});


	var maleRoster_EventDivision = [];
	var womenRoster_EventDivision = [];
	var mixedRoster_EventDivision = [];
	var eventclassdivisionJsonObject = "";
	// addRoosterDetailInEvent = function (rosterId, eventid, teamId, e) {
	// 	e.preventDefault();
	// 	$('#loading-spinner').removeClass('hide');
	// 	$('#eventClasesList_hide').addClass('hide');
	// 	$('#eventBoatList_hide').addClass('hide');
	// 	$("#submit_roster_event_succ").text("")
	// 	$("#submit_roster_event_err").text("")
	// 	$("#register_roster_id").val("")
	// 	$("#register_event_id").val("")
	// 	$("#eventDivisionListChange").removeClass('empty-err');
	// 	$("#eventClasesListChange").removeClass('empty-err');

	// 	eventclassdivisionJsonObject = "";
	// 	$.ajax({
	// 		url: "/get_event_detail_eventid",
	// 		data: { "eventid": eventid, "teamid": teamId },
	// 		type: "GET",
	// 		success: function (selectedEventClassDivisionData) {
	// 			tempselectedEventClassDivisionData = JSON.stringify(selectedEventClassDivisionData)
	// 			if (tempselectedEventClassDivisionData != "null") {
	// 				selectedEventClassDivisionDataValue = JSON.parse(tempselectedEventClassDivisionData)
	// 				eventclassdivisionJsonObject = selectedEventClassDivisionDataValue.eventclassdivisiondata
	// 				if (selectedEventClassDivisionDataValue.eventclassdivisiondata && selectedEventClassDivisionDataValue.alldivisiondata) {
	// 					var options = '';
	// 					var optionsStatus = false;
	// 					options += '<option value="">Please Select Division</option>';

	// 					teampClassDivision = []
	// 					$.each(selectedEventClassDivisionDataValue.eventclassdivisiondata, function (key, values) {
	// 						$.each(selectedEventClassDivisionDataValue.alldivisiondata, function (key1, value1) {
	// 							if (values.div == value1.value) {
	// 								tempselecteddivision = value1.value
	// 								optionsStatus = true;
	// 								tempselecteddivisionname = value1.div_name
	// 								if (values.boat_type != "" && values.boat_type) {
	// 									tempselecteddivision = tempselecteddivision + "_" + values.boat_type
	// 									tempselecteddivisionname = tempselecteddivisionname + " " + values.boat_type + " Person"
	// 								}
	// 								if (teampClassDivision.indexOf(tempselecteddivision) == -1) {
	// 									options += '<option value="' + tempselecteddivision + '">' + tempselecteddivisionname + '</option>';
	// 									teampClassDivision.push(tempselecteddivision)
	// 								}
	// 								return false
	// 							}
	// 						});
	// 					});


	// 					$("select#eventDivisionListChange").html(options);
	// 					$("#register_roster_id").val(rosterId)
	// 					$("#register_event_id").val(eventid)
	// 					if (optionsStatus) {
	// 						$("#submit_roster_eventModal").modal('show')
	// 					} else {
	// 						$("#defaultteammodelheading").text("Please Contact Event Organizer Division and Class not given. ")
	// 						$("#defaultteammodel").modal('show')
	// 					}
	// 				}
	// 			}
	// 			$('#loading-spinner').addClass('hide');
	// 		}
	// 	});
	// }

	$(document).on('change', "#eventDivisionListChange", function (e) {
		$("#submit_roster_event_succ").text("")
		$("#submit_roster_event_err").text("")

		selectedDivisionValue = this.value

		if (selectedDivisionValue != "") {
			var options = '';
			options += '<option value="">Select Class</option>';
			tempMalestatus = true
			tempWomenstatus = true
			tempMixedstatus = true
			$.each(eventclassdivisionJsonObject, function (key, values) {
				tempselecteddivision = values.div
				if (values.boat_type != "" && values.boat_type) {
					tempselecteddivision = tempselecteddivision + "_" + values.boat_type
				}
				if (tempselecteddivision == selectedDivisionValue) {
					if (values.class == "M" && tempMalestatus) {
						tempMalestatus = false
						options += '<option value="M">Men</option>';
					} else if (values.class == "W" && tempWomenstatus) {
						tempWomenstatus = false
						options += '<option value="W">Women</option>';
					} else if (values.class == "MX" && tempMixedstatus) {
						tempMixedstatus = false
						options += '<option value="MX">Mixed</option>';
					}
				}
			});

			$("select#eventClasesListChange").empty().html(options);
			$('#eventClasesList_hide').removeClass('hide');
			$('#eventBoatList_hide').addClass('hide');
			$("select#eventBoatListChange").empty()
		} else {
			$('#eventClasesList_hide').addClass('hide');
			$('#eventBoatList_hide').addClass('hide');
			$("select#eventBoatListChange").empty()
			$("select#eventClasesListChange").val("")
		}
	});

	$(document).on('change', "#eventClasesListChange", function (e) {
		$("#submit_roster_event_succ").text("")
		$("#submit_roster_event_err").text("")
		selectedClassValue = this.value
		selectedDivisionValue = $("#eventDivisionListChange").val()
		if (selectedClassValue != "" && selectedDivisionValue != "") {
			var options = '';
			$.each(eventclassdivisionJsonObject, function (i, value1) {
				tempselecteddivision = value1.div
				if (value1.boat_type != "" && value1.boat_type) {
					tempselecteddivision = tempselecteddivision + "_" + value1.boat_type
				}
				if (selectedClassValue == value1.class && selectedDivisionValue == tempselecteddivision) {
					if (value1.distance != "0" && value1.distance && value1.distance != "") {
						options += "<option value='" + value1.distance + "'>" + value1.distance + "</option>";
					}
				}

			});

			if (options != '') {

				$('#eventBoatList_hide').removeClass('hide');
				$("select#eventBoatListChange").empty().html(options);
			} else {
				$('#eventBoatList_hide').addClass('hide');
				$("select#eventBoatListChange").empty()
			}
		} else {
			$('#eventBoatList_hide').addClass('hide');
			$("select#eventBoatListChange").empty()
		}
	});


	$('#submit_roster_event').click(function (e) {
		e.preventDefault()
		$("#submit_roster_event_succ").text("")
		$("#submit_roster_event_err").text("")
		isFormValid = true;
		isSelectedClassValid = true

		if ($("#eventDivisionListChange").val() == "") {
			$("#eventDivisionListChange").addClass('empty-err');
			isFormValid = false;
		} else {
			isFormValid = true;
			$("#eventDivisionListChange").removeClass('empty-err');
		}

		if ($("#eventClasesListChange").val() == "") {
			$("#eventClasesListChange").addClass('empty-err');
			isSelectedClassValid = false;
		} else {
			isSelectedClassValid = true;
			$("#eventClasesListChange").removeClass('empty-err');
		}


		if (isFormValid && isSelectedClassValid) {
			$('#loading-spinner').removeClass('hide')
			$.ajax({
				type: "POST",
				url: "/event-register-roster-to-event",
				data: $("#submit_roster_eventForm").serialize()
			}).done(function (submitValueData) {
				if (submitValueData == "true") {
					$("#submit_roster_event_succ").text("Roster Register with concern Event")
					$("#submit_roster_eventModal").animate({ scrollTop: 0 }, "slow");
					sessionStorage.setItem('activeTab', '#roaster')
					setTimeout(function () {
						location.reload()
					}, TWO_SEC);
					$('#loading-spinner').addClass('hide');
				} else if (submitValueData == "alreadyexist") {
					$("#submit_roster_event_err").text("Selected class and division already associate with this roster")
					$("#submit_roster_eventModal").animate({ scrollTop: 0 }, "slow");
					$('#loading-spinner').addClass('hide');
				} else {
					$("#submit_roster_event_err").text("Roste Not Register with concern Event")
					$('#loading-spinner').addClass('hide')
					$("#submit_roster_eventModal").animate({ scrollTop: 0 }, "slow");
					$('#loading-spinner').addClass('hide')
				}
			});
		} else {
			$('#loading-spinner').addClass('hide')
			$("#submit_roster_eventModal").animate({ scrollTop: 0 }, "slow");
		}
	});

	$('.nav-tabs a[href="#personalPage"]').click(function () {
		$('#personalPage').removeClass('hide');
		$('#cardBillingPage').addClass('hide');
	});

	$('.nav-tabs a[href="#cardBillingPage"]').click(function () {
		$('#cardBillingPage').removeClass('hide');
		$('#personalPage').addClass('hide');
	});

	$('input[name="subscription_amount"]:radio').on('ifClicked', function (event) {
		event.preventDefault()
		setTimeout(function () {
			$('input[name="subscription_amount"]:radio').parent().closest('div').attr('class', 'iradio_square-green')
		}, 300);

		if (teamCoCaptainStatus || active_User_Status) {
			$('#clicktosubscriptionhide').addClass('hide');
			$('#clickfreeTrialPopuphide').addClass('hide');
		}

		$('#subcribeteammodelheaderParttext').text("Gushou Service Msg")
		$('#subscriptionteammodel').modal("show")
		freetrialPopup = false
	});

	$(document).on('click', ".subscriptionteamClass", function (e) {
		e.preventDefault()
		if (teamCoCaptainStatus || active_User_Status) {
			$('#clicktosubscriptionhide').addClass('hide')
			$('#clickfreeTrialPopuphide').addClass('hide');
		}
		$('#subcribeteammodelheaderParthide').removeClass('hide');
		$('#subcribeteammodelheaderParttext').text("Option to Upgrade")
		$('#subscriptionteammodel').modal("show")
		freetrialPopup = false
		$('#clickfreeTrialPopuphide').addClass('hide');
	});

	$(document).on('change', "#subscriptioncardId", function (e) {
		selectedDivisionValue = this.value
		if (selectedDivisionValue == "Change") {
			$('#subscriptioncardId').val($('#subscriptioncardselected').val())
			$('#subscriptDefaultCardModal').modal('show')
		}
	});

	$(document).on('click', "#submitDefaultCard", function (e) {
		defaultCardSelectChange = $('#defaultCardSelectChange').val()
		if (defaultCardSelectChange != "" && defaultCardSelectChange) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/stripe/delete_or_make_default_card",
				data: { "cardId": defaultCardSelectChange, "optionType": "make_default" },
			}).done(function (defaultresultDataValue) {
				if (defaultresultDataValue != "null" && defaultresultDataValue) {
					submitValueDataValue = JSON.parse(defaultresultDataValue)
					cardValueData = submitValueDataValue.status
					if (cardValueData == "true") {
						if (sessionStorage.getItem("changecardmissing") && submitValueDataValue.cardid != "") {
							sessionStorage.removeItem("changecardmissing")
							$("#subscriptioncardId").val(submitValueDataValue.cardid)
							$('#subscriptDefaultCardModal').modal("hide")
							$('#subscriptionPaySubmit').trigger('click');
							$('#loading-spinner').addClass('hide');
						} else {
							location.reload()
							$('#loading-spinner').addClass('hide');
						}

					} else {
						$('#loading-spinner').addClass('hide');
					}
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}
	});

	$(document).on('click', "#goBackPreviousPage", function (e) {
		e.preventDefault()
		window.history.back();
	});

	$(document).on('click', "#team-member-toggle", function (e) {
		e.preventDefault()
		window.history.back();
	});

	// $('.profile-card-member-toggle').click( function(){
	// $('.profile-card-member-toggle').parents(".dashboard-members").first().find(".card-list_profile").first().slideToggle(500,'linear');
	// $('.profile-card-profile-member-option').toggleClass("hide")
	// $('.profile-card-member-toggle').find("i").toggleClass("ti-angle-down ti-angle-up");
	// });

	// $('.profile-subscription-member-toggle').click( function(){
	// 	$('.profile-subscription-member-toggle').parents(".dashboard-members").first().find(".subscription-list_profile").first().slideToggle(500,'linear');
	// 	$('.profile-subscription-member-option').toggleClass("hide")
	// 	$('.profile-subscription-member-toggle').find("i").toggleClass("ti-angle-down ti-angle-up");
	// 	});

	var cancelSubscriptionFromPage = ""
	var cancelSubscriptionEndCancelStatus = ""
	var cancelSubscriptionUrl = ""
	$(document).on('click', "#cancel_subscription_detail", function (e) {
		e.preventDefault()
		$('#cancelSubscriptionModal').modal('show');
		$('#errorTextAppend').html("<p>We're sorry to see you downgrade but hope you enjoy the basic features to help you run a slick operation for <b style=color:#bf5523>" + $(this).attr('data-teamname') + "</b>.</p><p>You will still have premium features until the end of your billing month. Rosters and line-ups will be saved if you choose to return.</p>")
		$('#cancel_subscription_detail_submit').val($(this).attr('value'));
		$('#cancel_subscription_detail_submit').attr('name', this.name)
		cancelSubscriptionFromPage = "profile"
		cancelSubscriptionEndCancelStatus = "true"
		cancelSubscriptionUrl = "/stripe/cancel_subscription"
	});

	$(document).on('click', "#cancel_trial_subscription_detail", function (e) {
		e.preventDefault()
		$('#cancelSubscriptionModal').modal('show');
		$('#errorTextAppend').html("<p>We're sorry to see you downgrade but hope you enjoy the basic features to help you run a slick operation for <b style=color:#bf5523>" + $(this).attr('data-teamname') + "</b>.</p><p>You will still have premium features until the end of your billing month. Rosters and line-ups will be saved if you choose to return.</p>")
		$('#cancel_subscription_detail_submit').val($(this).attr('value'));
		$('#cancel_subscription_detail_submit').attr('name', this.name)
		cancelSubscriptionFromPage = ""
		cancelSubscriptionEndCancelStatus = "false"
		cancelSubscriptionUrl = "/stripe/cancel_trial_subscription"
	});

	$(document).on('click', "#cancel_subscription_detail_submit", function (e) {
		e.preventDefault()
		subscriptionValue = $(this).attr('value');
		teamId = this.name
		if (subscriptionValue != "" && subscriptionValue && teamId && teamId != "") {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: cancelSubscriptionUrl,
				data: { "subscriptionId": subscriptionValue, "teamId": teamId, "fromPage": cancelSubscriptionFromPage, "endCancelStatus": cancelSubscriptionEndCancelStatus },
			}).done(function (subscription_detailData) {
				if (subscription_detailData == "true") {
					cancelSubscriptionFromPage = ""
					cancelSubscriptionEndCancelStatus = ""
					cancelSubscriptionUrl = ""
					sessionStorage.setItem('activeTab', '#billing')
					location.reload()
					$('#loading-spinner').addClass('hide');
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}
	});

	$(document).on('click', "#reactivate_subscription_detail", function (e) {
		e.preventDefault()
		subscriptionValue = $(this).attr('value');
		teamId = this.name
		if (subscriptionValue != "" && subscriptionValue && teamId && teamId != "") {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/stripe/reactivate_subscription",
				data: { "subscriptionId": subscriptionValue, "teamId": teamId },
			}).done(function (subscription_detailData) {
				if (subscription_detailData == "true") {
					sessionStorage.setItem('activeTab', '#billing')
					location.reload()
					$('#loading-spinner').addClass('hide');
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}
	});

	$(document).on('click', ".remove_captain", function (e) {
		e.preventDefault()
		removeCaptainId = $(this).attr('id');
		if (removeCaptainId != "" && removeCaptainId) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/check_captain_subscribe_or_not",
				data: { "captainId": removeCaptainId },
			}).done(function (subscription_detailData) {
				$('#remove_captain_submit').attr('value', removeCaptainId)
				if (subscription_detailData == "true") {
					$('#removeCaptainModal').modal('show')
					$('#remove_captain_heading').html("<p>Are you sure you want to switch roles with the captain? You will be captain and the current captain will be a co-captain.</p><p>As the new captain, you will have control over your team subscription plan. The team's current plan will continue for the remaining paid period.</p><p>To continue the subscription with no interruption, it is recommended to enter your credit card details in the next step. (or you can skip this step and do it later)</p>")
					$('#remove_captain_submit').remove("removeCaptainFromteam").addClass("addCaptainSubscribeCard")
					$('#subscriptionOption').removeClass('hide');
					$('#nonsubscriptionOption').addClass('hide');
					$('#subscriptionremoveCaptainId').val(removeCaptainId);
					$('#loading-spinner').addClass('hide');
				} else {
					$('#removeCaptainModal').modal('show')
					$('#remove_captain_heading').html('<p>Are you sure you want to switch roles with the captain? You will be captain and the current captain will be a co-captain.</p>')
					$('#remove_captain_submit').remove("addCaptainSubscribeCard").addClass("removeCaptainFromteam")
					$('#subscriptionOption').addClass('hide');
					$('#nonsubscriptionOption').removeClass('hide');
					$('#loading-spinner').addClass('hide');
				}
			});
		}

	});

	$(document).on('click', ".removeCaptainFromteam", function (e) {
		e.preventDefault()
		removeCaptainId = $(this).attr('value');
		if (removeCaptainId != "" && removeCaptainId) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/team_remove_captain",
				data: { "captainId": removeCaptainId },
			}).done(function (subscription_detailData) {
				if (subscription_detailData == "true") {
					sessionStorage.setItem('activeTab', '#members')
					location.reload()
					$('#loading-spinner').addClass('hide');
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}

	});

	$(document).on('click', "#subscriptionSkipSubmit", function (e) {
		e.preventDefault()
		$('#subscriptionskipstatus').val(true)
		$('#loading-spinner').removeClass('hide')
		$.ajax({
			type: "POST",
			url: "/stripe/add-team-subscription",
			data: $("#subscriptionPay").serialize()
		}).done(function (submitValueData) {
			if (submitValueData != "null") {
				submitValueDataValue = JSON.parse(submitValueData)
				if (submitValueDataValue.redirecturl != "") {
					$('#subscription_err_msg').removeClass().addClass("success")
					$('#subscription_err_msg').text(submitValueDataValue.status)
					$('#loading-spinner').addClass('hide')
					setTimeout(function () {
						sessionStorage.setItem('activeTab', '#members')
						window.location.href = submitValueDataValue.redirecturl;
					}, TWO_SEC);
				} else {
					$('#subscription_err_msg').text(submitValueDataValue.status)
					$('#loading-spinner').addClass('hide')
				}
			}
		});
	});

	$(document).on('click', ".add_captain_admin_submit", function (e) {
		e.preventDefault()
		removeCaptainId = $(this).attr('id');
		$('#remove_captain_admin_submit').attr('value', removeCaptainId);
		if (removeCaptainId != "" && removeCaptainId) {
			$('#loading-spinner').removeClass('hide');
			$('#removeCaptainAdminModal').modal('show')
			$('#loading-spinner').addClass('hide');
		}

	});

	$(document).on('click', "#remove_captain_admin_submit", function (e) {
		e.preventDefault()
		removeCaptainId = $(this).attr('value');
		if (removeCaptainId != "" && removeCaptainId) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/team_add_captain_by_admin",
				data: { "captainId": removeCaptainId },
			}).done(function (subscription_detailData) {
				if (subscription_detailData == "true") {
					sessionStorage.setItem('activeTab', '#members')
					location.reload()
					$('#loading-spinner').addClass('hide');
				} else {
					$('#loading-spinner').addClass('hide');
				}
			});
		}

	});

	$(document).on('click', ".cancelSmsSubscriptionReminder", function (e) {
		$('#smsPackageSubscriptionModal').modal('hide')
		$('#loading-spinner').removeClass('hide');
		$.ajax({
			type: "POST",
			url: "/inactive-sms-subscription-popup",
		}).done(function (subscription_detailData) {
			$('#loading-spinner').addClass('hide');
			if (smsSendwithDissucOrReminder == "smsremider") {
				$('#submitSmsPracticeReminder').trigger('click');
			} else if (smsSendwithDissucOrReminder == "saveDisc") {
				$('#submitTeamDiscussion').trigger('click');
			}
		});
	});

	$('#clickfreeTrialPopup_checkbox').click(function () {
		if ($('#clickfreeTrialPopup_checkbox').is(':checked')) {
			var currentTeamId = $('#current_teamid').val()
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/inactive-free-trial-subscription-popup",
				data: { "currentTeamId": currentTeamId, "status": 'Not Show' },
			}).done(function (subscription_detailData) {
				freetrialPopup = false
				$('#loading-spinner').addClass('hide');
				$('#clickfreeTrialPopup_checkbox').attr('disabled', true);
				$('#subscriptionteammodel').modal('hide')
			});
		}
	});

	$(document).on('click', ".subscriptionOnOff", function (e) {
		$('#free_subscription_radiobox').val($(this).attr('value'))
		if (subscriptionPopup) {
			click_basic_TeamPopup_checkbox();
		}
		$('#free_Subscription_Form').submit();
	});

	$('.viewWaiverPage').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		var waiverUrlSlug = $(this).attr('value');
		var teamUrlSlug = $(this).attr('name');
		if (waiverUrlSlug && teamUrlSlug && waiverUrlSlug != "" && teamUrlSlug != "") {
			localStorage.setItem('teamWaiverPart', 'true')
			localStorage.setItem('teamWaiverTeamSlug', teamUrlSlug)
			window.location.href = waiverUrlSlug;
		}
		$('#loading-spinner').addClass('hide');
	});

	$('#backTowaiverallPage').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		teamWaiverPart = localStorage.getItem('teamWaiverPart')
		if (teamWaiverPart) {
			if (teamWaiverPart == "true") {
				tempSlug = localStorage.getItem('teamWaiverTeamSlug')
				localStorage.removeItem('teamWaiverTeamSlug')
				localStorage.removeItem('teamWaiverPart')
				sessionStorage.setItem("activeTab", "#waiver");
				window.location.href = "/team-view/" + tempSlug
			}
		} else {
			window.location.href = "/waiver-all";
		}
		$('#loading-spinner').addClass('hide');
	});

	$('#backFromViewUploadWaiver').click(function (e) {
		e.preventDefault();
		$('#loading-spinner').removeClass('hide');
		teamUploadWaiverPart = localStorage.getItem('teamUploadWaiverPart')
		var team_slug = $("#teamSlugUpload").val();
		var event_slug = $("#eventSlugUpload").val();
		if (teamUploadWaiverPart) {
			if (teamUploadWaiverPart == "true") {
			 
				localStorage.removeItem('teamUploadWaiverPart')
				sessionStorage.setItem("activeTab", "#waiver");
				window.location.href = "/team-view/" + team_slug
			}
			else {
			
				sessionStorage.setItem('eventactiveTab', '#eventWaiverList') 
				window.location.href = "/event-view/" + event_slug + "/event-waivers"; 
				
			}
		} 
		$('#loading-spinner').addClass('hide');
	});

	$('body').on('hidden.bs.modal', function (e) {
		if ($('.modal').hasClass('in')) {
			$('body').addClass('modal-open');
		}
	});

	$(document).on('click', "#submit_to_subscription_page", function (e) {
		e.preventDefault()
		$('#submitSubscriptionform').submit();
	});

	$(document).on('click', ".teamMemberActiveId", function (e) {
		e.preventDefault()
		var tempActiveClickId = $(this).attr('value');
		sessionStorage.setItem("#activeTabmembers", tempActiveClickId)
	});

	$(document).on('click', ".canceladdcarderror", function (e) {
		if (sessionStorage.getItem('addcardmissing')) {
			location.reload()
		} else {
			return true
		}

	});

	$('#teamsubscriptionmodel').on('hidden.bs.modal', function (e) {
		if (subscriptionPopup) {
			click_basic_TeamPopup_checkbox();
		}
	})

	$('#subscriptionteammodel').on('hidden.bs.modal', function (e) {
		if (freetrialPopup) {
			clickfreeTrialPopup_checkbox_off();
		}
	})

	$(document).on('click', "#hidefreetrialpopup", function (e) {
		if (freetrialPopup) {
			clickfreeTrialPopup_checkbox_off();
		}
	})

	function click_basic_TeamPopup_checkbox() {
		var currentTeamId = $('#current_teamid').val()
		$.ajax({
			type: "POST",
			url: "/inactive-basic-subscription-popup",
			data: { "currentTeamId": currentTeamId, "status": 'Not Show' },
		}).done(function (subscription_detailData) {
			$('#clickfreeTrialPopup_checkbox').attr('disabled', true);
		});
	}

	function clickfreeTrialPopup_checkbox_off() {
		var currentTeamId = $('#current_teamid').val()
		$.ajax({
			type: "POST",
			url: "/inactive-free-trial-subscription-popup",
			data: { "currentTeamId": currentTeamId, "status": 'false' },
		}).done(function (subscription_detailData) {
			$('#clickfreeTrialPopup_checkbox').attr('disabled', true);
		});
	}

	$("input[name='MX_divisions[]'],input[name='W_divisions[]'],input[name='M_divisions[]'],input[name='M_event_divisions[]'],input[name='W_event_divisions[]'],input[name='MX_event_divisions[]']").change(function () {
		tempClass = $(this).attr('data-class');
		tempDivision = $(this).attr('data-division');
		tempId = $(this).attr('id')
		tempClassCode1 = tempId
		tempDivisionCode1 = ""
		tempClassCode1 = tempClassCode1.replace("M_", "");
		tempClassCode1 = tempClassCode1.replace("W_", "");
		tempClassCode1 = tempClassCode1.replace("MX_", "");
		if (tempDivision == "Male") {
			tempDivisionCode1 = "M"
		} else if (tempDivision == "Women") {
			tempDivisionCode1 = "W"
		} else if (tempDivision == "Mixed") {
			tempDivisionCode1 = "MX"
		}
		if ($(this).is(':checked')) {
			class_Division_Boattype(tempClass, tempDivision, tempId, tempClassCode1, tempDivisionCode1, "")
		} else {
			$("#" + tempDivision + tempId).remove()
		}
	});

	/** Event View Page Tab click events */
	$(document).on('click', ".eventTabClick", function (e) {
		e.preventDefault()
		eventactiveTab = sessionStorage.getItem('eventactiveTab')
		if ($(this).attr('value') == "#eventTeamList" && eventactiveTab != "#eventTeamList") {
			sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
			window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-teams-view";
		}
		else if ($(this).attr('value') == "#eventPaymentList" && eventactiveTab != "#eventPaymentList") {
			sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
			
			window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-payment-view";
			
		}else if ($(this).attr('value') == "#eventaccreditation" && eventactiveTab != "#eventaccreditation") {
			sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
			sessionStorage.setItem('activeaccreditation', "#General")
			window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-accreditation-general-view";
		}
		else if ($(this).attr('value') == "#eventRosterRacingList" && eventactiveTab != "#eventRosterRacingList") {
			sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
			
			window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-reports";
			
		}
		else if ($(this).attr('value') == "#eventWaiverList" && eventactiveTab != "#eventWaiversListTabClick") { 
			sessionStorage.setItem('eventactiveTab', $(this).attr('value')) 
			 
			window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-waivers"; 
			 
		  }
	});

	//Rosters & Racing

	$(document).on('click', '#show_teamsmember_rosterracing', function (eve) {
		eve.stopPropagation()
		console.log("hh")
		tempId1 = document.getElementsByClassName("rosterracingTableData")
		if(tempId1[0].style.display == "none"){
			$(".rosterracingTableData").toggle();
			$(".rosterracingIndividualTeamList").css('display','none')		
		}
		else {
			$(".rosterracingTableData").toggle();	
			$(".rosterracingIndividualTeamList").css('display','none')			
		} 
	});

	$(document).on('click', '#getRosterListByTeam', function (eve) {
		eve.stopPropagation()
		console.log("hi")
		tempId1 = document.getElementsByClassName("rosterracingIndividualTeamList")
		tempId2 = document.getElementsByClassName("rosterracingTableData")
		if(tempId1[0].style.display == "none"){
			$(".rosterracingIndividualTeamList").toggle();		
		}
		else {
			$(".rosterracingIndividualTeamList").toggle();			
		} 
	});

	/** End */

	/** Event View (Teams Tab) */
	$(document).on('click', "#updateTeamEventRegisterDetail", function (e) {
		$('#loading-spinner').removeClass('hide');
		var id = $(this).attr('value');
		$("#reg_event_captain_name").val("")
		$("#reg_event_captain_email").val("")
		$("#reg_event_captain_phone").val("")
		$("#reg_event_captain_note").val("")
		$("#reg_event_edit_msg_err_msgs").text("")
		$("#reg_event_edit_msg_succ_msgs").text("")
		$.ajax({
			type: "GET",
			url: "/get-event-register-detail-by-id",
			data: { "eventRegisterId": id },
		}).done(function (eventRegisterDetails) {
			tempmsgdata = JSON.stringify(eventRegisterDetails)
			if (tempmsgdata != "null") {
				tempEventRegisterDetails = JSON.parse(tempmsgdata)
				$("#reg_event_captain_name").val(tempEventRegisterDetails[0].reg_captain_name)
				$("#reg_event_captain_email").val(tempEventRegisterDetails[0].reg_email_id)
				$("#reg_event_captain_phone").val(tempEventRegisterDetails[0].reg_phone_number)
				$("#reg_event_captain_note").val(tempEventRegisterDetails[0].message)
				$("#current_reg_event_id").val(id)
				$("#editTeamRegisterModel").modal('show')
				$('#loading-spinner').addClass('hide');
			} else {
				$('#loading-spinner').addClass('hide');
			}
		});
	});

	$(document).on('click', "#save-reg-event-team-detail", function (e) {
		$("#reg_event_edit_msg_err_msgs").text("")
		$("#reg_event_edit_msg_succ_msgs").text("")
		isValid = true
		isValid1 = true
		var reg_event_captain_phone = $('#reg_event_captain_phone').val()
		var reg_event_captain_email = $('#reg_event_captain_email').val()
		if (isNotEmpty(reg_event_captain_phone.trim()) == true) {
			var phoneRegex = /^\+([0-9]{1,2})\)?[\s+][-. ]?([0-9]{3})[-. ]?([0-9]{3})?[-. ]?([0-9]{4})$/;
			if (phoneRegex.test(reg_event_captain_phone)) {
				var phoneRegex1 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				var phoneExpressionarray = reg_event_captain_phone.split(' ');
				phoneExpressionarray1 = ""
				if (phoneExpressionarray[1] != "") {
					if(phoneExpressionarray.length > 2){

						phoneExpressionarrayN = phoneExpressionarray[1]+"-"+phoneExpressionarray[2]+"-"+phoneExpressionarray[3]
						console.log(phoneExpressionarrayN.replace(phoneRegex1, "$1-$2-$3"))
						phoneExpressionarray1 = phoneExpressionarrayN.replace(phoneRegex1, "$1-$2-$3");
					
					}else if(phoneExpressionarray[1].indexOf('-') == -1){

						console.log(phoneExpressionarray[1].replace(phoneRegex1, "$1-$2-$3"))
						phoneExpressionarray1 = phoneExpressionarray[1].replace(phoneRegex1, "$1-$2-$3");
					
					}
					else{
						phoneExpressionarray1 = phoneExpressionarray[1].replace(phoneRegex1, "$1-$2-$3");

					}
					
				} else {
					phoneExpressionarray1 = phoneExpressionarray[2].replace(phoneRegex1, "$1-$2-$3");
				}
				$('#reg_event_captain_phone').val(phoneExpressionarray[0] + ' ' + phoneExpressionarray1);
			
			} else {
				isValid = false
				$("#reg_event_edit_msg_err_msgs").html("Invalid phone number<br> (ex +1 443-979-8200 or +11 443.979.8200)")
				$("#editTeamRegisterModel").animate({ scrollTop: 0 }, "slow");
			}
		}
		if (isValid) {
			var email_regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
			if (!email_regex.test(reg_event_captain_email)) {
				$("#reg_event_edit_msg_err_msgs").text("Please enter a valid email address")
				$("#editTeamRegisterModel").animate({ scrollTop: 0 }, "slow");
				isValid1 = false
			}
		}

		if (isValid && isValid1) {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/edit-event-register-detail",
				data: $("#eventregistereditForm").serialize()
			}).done(function (resultData) {
				if (resultData == "true") {
					$('#loading-spinner').addClass('hide');
					$("#reg_event_edit_msg_succ_msgs").text("Updated successfully")
					$("#editTeamRegisterModel").animate({ scrollTop: 0 }, "slow");
					setTimeout(function () {
						location.reload()
						$("#editTeamRegisterModel").modal("hide");
					}, TWO_SEC);
				} else {
					$("#reg_event_edit_msg_err_msgs").text("Not updated successfully")
					$("#editTeamRegisterModel").animate({ scrollTop: 0 }, "slow");
					$('#loading-spinner').addClass('hide');
				}

			});
		}
		else {
			$('#loading-spinner').addClass('hide');
		}
	});
	$(document).on('click', "#cancelTeamEventRegisterDetail", function (e) {
		var id = $(this).attr('value');
		$('#registerTeamEventDeleteModel').modal("show")
			$('#registerTeamEventDeleteModelText').text("Remove Team")
			$('#registerTeamEventDeleteModel_Body').text("Are you sure you would like to remove this team from the list of registered teams? If they have not made a payment yet, their information will be deleted. If they have already made a payment, their information will be deactivated.")
		$('#deleteEventRegisterTeam').val(id);
	});
	
	$(document).on('click', "#deleteEventRegisterTeam", function (e) {
		$('#loading-spinner').removeClass('hide');
		var id = $(this).attr('value');
		$.ajax({
			type: "POST",
			url: "/cancel-event-register-detail-by-id",
			data: { "eventRegisterId": id },
		}).done(function (eventRegisterDetailsResult) {
			$('#loading-spinner').addClass('hide');
			if (eventRegisterDetailsResult == "true") {
				location.reload()
			}
		});
	});
	
	$('#filter_all-waivers').change(function () {
		if (this.checked) {
			var allCheck = $('#filter_all-waivers').prop('checked')
			var teamNameVal = $("#searchByTeamNameWaiverFilter").val();
			if ($("#waiverTeamsEvent").length) {
				var hideCount = 0
				var showCount = 0
				$(".filerbyspanclass").each(function (index) {
					if (index >= 0) {
						if (teamNameVal && allCheck) {

							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
							console.log("teamName-", teamName)
							var completestatus = $row.find(".getcompletestatus").text().toLowerCase().trim();
							var incompletestatus = $row.find(".getincompletestatus").text().toLowerCase().trim();
							var cbval_comp = "complete"
							var cbval_incomp = "incomplete"

							console.log("V1*-", teamName.indexOf(teamNameVal.toLowerCase().trim()))
							if (teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1 && (incompletestatus.toLowerCase().trim().indexOf(cbval_incomp.toLowerCase().trim()) == -1 || completestatus.toLowerCase().trim().indexOf(cbval_comp.toLowerCase().trim()) == -1)) {
								$row.hide();
								hideCount = hideCount + 1
							}

							else if (teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) != -1 && (incompletestatus.toLowerCase().trim().indexOf(cbval_incomp.toLowerCase().trim()) != -1 || completestatus.toLowerCase().trim().indexOf(cbval_comp.toLowerCase().trim()) != -1)) {
								$row.show();
								showCount = showCount + 1
							}


						}
						else {
							$row = $(this);
							$row.show();
							$("#waiverTeamsEvent").removeClass('hide')
							$(".filerbyspanclass").removeClass('hide')
						}


					}


				});
				if (showCount == 0) {
					if (teamNameVal != "") {
						$("#waiverfilternoteamfound").removeClass('hide')
						$(".filerbyspanclass").addClass('hide')
					}
				} else {
					$("#waiverfilternoteamfound").addClass('hide')
					$(".filerbyspanclass").removeClass('hide')
				}
			}


		} else {
			var allCheck = $('#filter_all-waivers').prop('checked')
			var teamNameVal = $("#searchByTeamNameWaiverFilter").val();
			if (teamNameVal && !allCheck) {

				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {

							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
							console.log("teamName-", teamName)

							console.log("V1*-", teamName.indexOf(teamNameVal.toLowerCase().trim()))
							if (teamName.indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (teamName.indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (teamNameVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}

			}
			else {
				$(".filerbyspanclass").each(function (index) {
					if (index >= 0) {
						$row = $(this);
						$row.show();
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				});
			}

		}

	});

	$('#filter_waiver_complete').change(function(){
		if(this.checked){
			$('#filter_waiver_incomplete').prop("checked",false)
			//$("#searchByTeamNameWaiverFilter").val("")
		}
		
	});
	$('#filter_waiver_incomplete').change(function(){
		if(this.checked){
		$('#filter_waiver_complete').prop("checked",false)
		//$("#searchByTeamNameWaiverFilter").val("")
		}
	});
	$('.check_all_waiver').change(function () {

		if (this.checked) {

			var returnVal = this.value;
			var allCheck = $('#filter_all-waivers').prop('checked')
			var teamNameVal = $("#searchByTeamNameWaiverFilter").val();
			console.log("nn-", returnVal)
			console.log("len-", $("#waiverTeamsEvent").length)
			if ($("#waiverTeamsEvent").length) {
				var hideCount = 0
				var showCount = 0

				$(".filerbyspanclass").each(function (index) {
					if (index >= 0) {



						if (teamNameVal && returnVal == "complete") {
							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();

							var completestatus = $row.find(".getcompletestatus").text().toLowerCase().trim();

							var cbval = returnVal



							if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
						else if (teamNameVal && returnVal == "incomplete") {
							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();

							var incompletestatus = $row.find(".getincompletestatus").text().toLowerCase().trim();

							var cbval = returnVal



							if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}


						else if (returnVal == "complete") {

							$row = $(this);
							//var val1 = $row.find(".getincompletestatus").text();
							var val2 = $row.find(".getcompletestatus").text();

							//console.log("val1-",val1)
							console.log("val2-", val2)

							if (val2.indexOf(returnVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (val2.indexOf(returnVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}
						}
						else if (returnVal == "incomplete") {

							$row = $(this);
							var val1 = $row.find(".getincompletestatus").text();
							//var val2 = $row.find(".getcompletestatus").text();

							console.log("val1-", val1)
							//console.log("val2-",val2)

							if (val1.indexOf(returnVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (val1.indexOf(returnVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}



					}
				});
				if (showCount == 0) {
					if (returnVal != "") {
						$("#waiverfilternoteamfound").removeClass('hide')
						$(".filerbyspanclass").addClass('hide')
					}
				} else {
					$("#waiverfilternoteamfound").addClass('hide')
					$(".filerbyspanclass").removeClass('hide')
				}
			}


		} else {
			var returnVal = $(this).val()
			console.log("nn*-", returnVal)
			var teamNameVal = $("#searchByTeamNameWaiverFilter").val();
			console.log("teamNameVal*-", teamNameVal)
			var comp = $('#filter_waiver_complete').prop("checked");
			console.log("com*-", comp)
			var incomp = $('#filter_waiver_incomplete').prop("checked");
			console.log("incom*-", incomp)
			if (teamNameVal && incomp && returnVal == "complete") {
				console.log("incomplete")
				var currentVal = "incomplete"
				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {
							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();

							var incompletestatus = $row.find(".getincompletestatus").text().toLowerCase().trim();

							var cbval = currentVal

							if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (currentVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}
			}
			else if (teamNameVal && comp && returnVal == "incomplete") {
				console.log("complete")
				var currentVal = "complete"
				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {

							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();

							var completestatus = $row.find(".getcompletestatus").text().toLowerCase().trim();

							var cbval = currentVal



							if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (currentVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}
			}
			else if (teamNameVal && !incomp && !comp) {

				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {

							$row = $(this);
							var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
							console.log("teamName-", teamName)

							console.log("V1*-", teamName.indexOf(teamNameVal.toLowerCase().trim()))
							if (teamName.indexOf(teamNameVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (teamName.indexOf(teamNameVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (teamNameVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}

			}
			else if (returnVal == "incomplete" && comp) {
				var currentVal = "complete"
				console.log("complete")
				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {


							$row = $(this);
							//var val1 = $row.find(".getincompletestatus").text();
							var val2 = $row.find(".getcompletestatus").text();

							//console.log("val1-",val1)
							console.log("val2-", val2)

							if (val2.indexOf(currentVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (val2.indexOf(currentVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (currentVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}
			} else if (returnVal == "complete" && incomp) {
				console.log("incomplete")
				var currentVal = "incomplete"
				if ($("#waiverTeamsEvent").length) {
					var hideCount = 0
					var showCount = 0

					$(".filerbyspanclass").each(function (index) {
						if (index >= 0) {


							$row = $(this);
							var val1 = $row.find(".getincompletestatus").text();
							//var val2 = $row.find(".getcompletestatus").text();

							console.log("val1-", val1)
							//console.log("val2-",val2)

							if (val1.indexOf(currentVal.toLowerCase().trim()) == -1) {
								$row.hide();
								hideCount = hideCount + 1
							}
							else if (val1.indexOf(currentVal.toLowerCase().trim()) != -1) {
								$row.show();
								showCount = showCount + 1
							}

						}
					});
					if (showCount == 0) {
						if (currentVal != "") {
							$("#waiverfilternoteamfound").removeClass('hide')
							$(".filerbyspanclass").addClass('hide')
						}
					} else {
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				}
			}

			else {
				console.log("all")
				$(".filerbyspanclass").each(function (index) {
					if (index >= 0) {
						$row = $(this);
						$row.show();
						$("#waiverfilternoteamfound").addClass('hide')
						$(".filerbyspanclass").removeClass('hide')
					}
				});
			}

		}

	});
	$("#searchByTeamNameWaiverFilter").on("keyup", function () {
		var value = $(this).val();
		console.log(value)
		//$('.check_all_waiver').prop("checked",false)
		var cb_comp = $('#filter_waiver_complete').prop('checked');
		var cbv_comp = $('#filter_waiver_complete')
		var cb_incomp = $('#filter_waiver_incomplete').prop('checked');
		var cbv_incomp = $('#filter_waiver_incomplete')
		var cba = $('#filter_all-waivers').prop('checked');

		if ($("#waiverTeamsEvent").length) {
			var hideCount = 0
			var showCount = 0

			$(".filerbyspanclass").each(function (index) {
				if (index >= 0) {
					if (cba && cbv_comp.val() == "complete" && cbv_incomp.val() == "incomplete") {

						$row = $(this);
						var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
						console.log("teamName-", teamName)

						console.log("V1*-", teamName.indexOf(value.toLowerCase().trim()))
						if (teamName.indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (teamName.indexOf(value.toLowerCase().trim()) != -1) {
							$row.show();
							showCount = showCount + 1
						}


					}
					else if (cb_comp && cbv_comp.val() == "complete") {

						$row = $(this);
						var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
						console.log("teamName-", teamName)
						var completestatus = $row.find(".getcompletestatus").text().toLowerCase().trim();
						console.log("completestatus-", completestatus)
						console.log("cbval--", cbv_comp.val().toLowerCase().trim())
						console.log("input-", value.toLowerCase().trim())
						console.log("V-", completestatus.toLowerCase().trim().indexOf(cbv_comp))
						console.log("V1-", teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()))
						var cbval = cbv_comp.val().toLowerCase().trim()



						if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (completestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) != -1) {
							$row.show();
							showCount = showCount + 1
						}

					} else if (cb_incomp && cbv_incomp.val() == "incomplete") {

						$row = $(this);
						var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
						console.log("teamName-", teamName)
						var incompletestatus = $row.find(".getincompletestatus").text().toLowerCase().trim();
						console.log("incompletestatus-", incompletestatus)
						console.log("cbval--", cbv_incomp.val().toLowerCase().trim())
						console.log("input-", value.toLowerCase().trim())
						console.log("V-", incompletestatus.toLowerCase().trim().indexOf(cbv_incomp))
						console.log("V1-", teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()))
						var cbval = cbv_incomp.val().toLowerCase().trim()



						if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 && teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) == -1 || teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (incompletestatus.toLowerCase().trim().indexOf(cbval.toLowerCase().trim()) != -1 && teamName.toLowerCase().trim().indexOf(value.toLowerCase().trim()) != -1) {
							$row.show();
							showCount = showCount + 1
						}

					}
					else if (!cb_comp || !cb_incomp) {
						$row = $(this);
						var teamName = $row.find(".getfilterwaiverteamname").text().toLowerCase().trim();
						console.log("teamName-", teamName)

						console.log("V1*-", teamName.indexOf(value.toLowerCase().trim()))
						if (teamName.indexOf(value.toLowerCase().trim()) == -1) {
							$row.hide();
							hideCount = hideCount + 1
						}
						else if (teamName.indexOf(value.toLowerCase().trim()) != -1) {
							$row.show();
							showCount = showCount + 1
						}
					}


				}
			});
			if (showCount == 0) {
				if (value != "") {
					$("#waiverfilternoteamfound").removeClass('hide')
					$(".filerbyspanclass").addClass('hide')
				}
			} else {

				$("#waiverfilternoteamfound").addClass('hide')
				$(".filerbyspanclass").removeClass('hide')

			}
		}
	});
	
	$("#searchByTeamCaptainName").on("keyup", function () {
		var value = $(this).val();

		if ($("#event_team_members_team").length) {
			var hideCount = 0
			var showCount = 0
			$("#event_team_members_team tr").each(function (index) {
				if (index !== 0) {

					$row = $(this);

					var teamname = $row.find("td:nth-child(2) a.event-list-title").text().toLowerCase().trim();
					var captainname = $row.find("td:nth-child(3) a.event-list-title").text().toLowerCase().trim();

					if (teamname.indexOf(value.toLowerCase().trim()) == -1 && captainname.indexOf(value.toLowerCase().trim()) == -1) {
						$row.hide();
						hideCount = hideCount + 1
					}
					else if (teamname.indexOf(value.toLowerCase().trim()) != -1 || captainname.indexOf(value.toLowerCase().trim()) != -1) {
						$row.show();
						showCount = showCount + 1
					}
				}
			});

			if (showCount == 0) {
				if (value != "") {
					$("#eventregmemberusernotfount").show()
					$("#event_team_members_team tr").addClass('hide')
				}
			} else {
				$("#eventregmemberusernotfount").hide()
				$("#event_team_members_team tr").removeClass('hide')
			}
		}
	});

	$(document).on('click', "#teamsortacending_eventreg", function (e) {
		e.preventDefault()
		$("#eventregmemberusernotfount").hide()
		$("#event_team_members tr").show()
		$("#searchByTeamCaptainName").val("")

		tempId = document.getElementById("eventteamregisterdetailorderbyTeamname")
		tempId1 = document.getElementById("eventteamregisterdetail")
		if (tempId.style.display == "" || (tempId.style.display == "none" && tempId1.style.display == "none")) {
			$("#eventteamregisterdetail").css("display", "");
			$("#eventteamregisterdetailorderbyTeamname").css("display", "none");
			$("#eventteamregisterdetailorderbyTeamRostername").css("display", "none");

		} else if (tempId1.style.display == "") {
			$("#eventteamregisterdetail").css("display", "none");
			$("#eventteamregisterdetailorderbyTeamname").css("display", "");
			$("#eventteamregisterdetailorderbyTeamRostername").css("display", "none");
		}
	});

	$(document).on('click', "#rostersortacending_eventreg", function (e) {
		e.preventDefault()
		$("#eventregmemberusernotfount").hide()
		$("#event_team_members tr").show()
		$("#searchByTeamCaptainName").val("")

		tempId = document.getElementById("eventteamregisterdetailorderbyTeamRostername")
		tempId1 = document.getElementById("eventteamregisterdetail")

		if (tempId.style.display == "") {
			$("#eventteamregisterdetail").css("display", "");
			$("#eventteamregisterdetailorderbyTeamname").css("display", "none");
			$("#eventteamregisterdetailorderbyTeamRostername").css("display", "none");

		} else if (tempId1.style.display == "" || (tempId.style.display == "none" && tempId1.style.display == "none")) {
			$("#eventteamregisterdetail").css("display", "none");
			$("#eventteamregisterdetailorderbyTeamname").css("display", "none");
			$("#eventteamregisterdetailorderbyTeamRostername").css("display", "");
		}

	});

	$("input[name='All_divisions[]'],input[name='All_boattype[]'],input[name='All_boatmeter[]']").change(function () {
		$('#event_matrix_classes_err').addClass('hide');
		$('#event_matrix_classes_err').text("");

		$('#event_classes_err').addClass('hide');
		$('#event_classes_err').text("");

		if ($('input[name="All_divisions[]"]:checked').length > 0 && $('input[name="All_boattype[]"]:checked').length > 0 && $('input[name="All_boatmeter[]"]:checked').length > 0) {
			$('#ClassDivisionMaxHideShowContinue').removeClass('hide')
			$("#appendClassDivisionMaxTable").empty()
			$("#appendClassDivisionMaxHideShowTable").addClass('hide')
			$("#createboxsectionfooterhideshow").addClass('hide')
		} else {
			$('#ClassDivisionMaxHideShowContinue').addClass('hide')
			$("#appendClassDivisionMaxTable").empty()
			$("#appendClassDivisionMaxHideShowTable").addClass('hide')
			$("#createboxsectionfooterhideshow").addClass('hide')
		}
	});

	$(document).on('click', "#ClassDivisionMaxContinueClick", function (e) {
		teampDivisionClassChangetruefalse = true
		var all_divisions = [];
		e.preventDefault()
		$('input:checkbox[name="All_divisions[]"]').each(function () {
			if (this.checked) {
				all_divisions.push(this.value);
			}
		});

		var all_boattype = [];
		$('input:checkbox[name="All_boattype[]"]').each(function () {
			if (this.checked) {
				all_boattype.push(this.value);
			}
		});

		var all_boatmeter = [];
		$('input:checkbox[name="All_boatmeter[]"]').each(function () {
			if (this.checked) {
				all_boatmeter.push(this.value);
			}
		});

		create_Event_class_Division_distance(all_divisions, all_boattype, all_boatmeter)
	});

	$(document).on('click', "#EditClassDivisionMaxContinueClick", function (e) {
		teampDivisionClassChangetruefalse = true
		var all_divisions = [];
		e.preventDefault()
		$('input:checkbox[name="All_divisions[]"]').each(function () {
			if (this.checked) {
				all_divisions.push(this.value);
			}
		});

		var all_boattype = [];
		$('input:checkbox[name="All_boattype[]"]').each(function () {
			if (this.checked) {
				all_boattype.push(this.value);
			}
		});

		var all_boatmeter = [];
		$('input:checkbox[name="All_boatmeter[]"]').each(function () {
			if (this.checked) {
				all_boatmeter.push(this.value);
			}
		});

		if (all_divisions.length > 0 && all_boattype.length > 0 && all_boatmeter.length > 0) {
			create_Event_class_Division_distance(all_divisions, all_boattype, all_boatmeter)
			checkedEventClassDivisionOldDetail()
		}
	});

	function checkedEventClassDivisionOldDetail() {
		var registerClassDivisionDetailWithJosn = $("#registerClassDivisionDetailWithJosn").val();
		if (registerClassDivisionDetailWithJosn != "null" && registerClassDivisionDetailWithJosn != "") {
			$.each(JSON.parse(registerClassDivisionDetailWithJosn), function (key, value) {
				var div = value.div.replace("{", "");
				div = div.replace("}", "");
				$("#" + value.class + "_" + value.distance + "_" + div + "_" + value.boat_type).attr('checked', true)
			});
		}
	}

	$("#submit_edit_step2").click(function () {
		$('#table tr').addClass("hide")
		createeventdiv_calss_dis_boattypeVals = []
		var mdivisions = [];
		var wdivisions = [];
		var mxdivisions = [];
		var classes = [];

		$('#event_matrix_classes_err').addClass('hide');
		$('#event_matrix_classes_err').text("");

		var $all_divisions = $("input[type=checkbox][name='All_divisions[]']:checked");

		var $all_boattype = $("input[type=checkbox][name='All_boattype[]']:checked");

		var $all_boatmeter = $("input[type=checkbox][name='All_boatmeter[]']:checked");


		if ($all_divisions.length > 0 || $all_boattype.length > 0 || $all_boatmeter.length > 0) {
			$('#event_classes_err').addClass('hide');
			$('#event_classes_err').text("");

			$('.error-message').addClass('hide');
			$(".error-message").text("");

			isFormValid2 = true
			$('.allboattypeselectedornot').each(function (e) {
				var $chkbox = $(this).find($('input[type="checkbox"]:checked'));
				if ($chkbox.length == 0) {
					isFormValid2 = false
				}
			});
			if (isFormValid2) {

				$('input:checkbox[name="all_div_cls_botty_meter[]"]').each(function () {
					if (this.checked) {
						createeventdiv_calss_dis_boattypeVals.push(this.value);
					}
				})

				$('#reg_price_err').addClass('hide');
				$("#reg_price_err").text("");

				$('#eventEditClassDivisionStep1').addClass('hide')
				$('#eventEditClassDivisionStep2').removeClass('hide')
				$('#pricing_based_selection').trigger('change')
				EditClassDivisionStep2()
			} else {

				$('#event_matrix_classes_err').removeClass('hide');
				$('#event_matrix_classes_err').text("Please select racing for each classes");
				$("#classDivisionsModal").animate({ scrollTop: $('#event_matrix_classes_err').offset().top - 130 }, "slow");
			}

		}
		else {
			$('#class_div_err').removeClass('hide');
			$("#class_div_err").text("Please select the class(es) that apply to your team");
			$('#event_matrix_classes_err').addClass('hide');
			$('#event_matrix_classes_err').text("");
			$('#event_classes_err').removeClass('hide');
			$('#event_classes_err').text("Please select the class(es) that apply to your event");
			$("#classDivisionsModal").animate({ scrollTop: 0 }, "slow");
		}

	});


	function EditClassDivisionStep2() {
		var eventPricingDetailWithJosnData = $('#eventPricingDetailWithJosn').val()
		if (eventPricingDetailWithJosnData != "null" && eventPricingDetailWithJosnData != "") {
			if (eventPrice_basedDetailScript == "BRC") {
				$.each(JSON.parse(eventPricingDetailWithJosnData), function (key, value) {
					tempValue = $("#" + value.pricing_class + "_" + value.pricing_division + "_" + value.boat_type).attr('data-index')
					$('#' + tempValue + 'seb_price').val(value.super_early_bird_cost)
					$('#' + tempValue + 'eb_price').val(value.early_bird_cost)
					$('#' + tempValue + 'r_price').val(value.regular_cost)
				});
			} else if (eventPrice_basedDetailScript == "PP") {
				tempkey =0
				$.each(JSON.parse(eventPricingDetailWithJosnData), function (key, value) {
					tempkey = tempkey + 1
					$('#'+tempkey+'seb_price').val(value.super_early_bird_cost)
					$('#'+tempkey+'eb_price').val(value.early_bird_cost)
					$('#'+tempkey+'r_price').val(value.regular_cost)
				});
			} else if (eventPrice_basedDetailScript == "OT") {
				$.each(JSON.parse(eventPricingDetailWithJosnData), function (key, value) {
					$('#pricingbasedotherclear').val(value.other)
				});
			}
		}
	}
	$(document).on('click', "#back_event_step1", function (e) {
		$('#eventEditClassDivisionStep1').removeClass('hide')
		$('#eventEditClassDivisionStep2').addClass('hide')
	});

	$(document).on('click', ".closeClassChanges", function (e) {
		location.reload()
	});

	$(document).on('click', ".collapseextendclick", function (e) {
		e.stopPropagation();
		e.preventDefault()
	});

	$("#pricing_based_selection").change(function () {
		tempValue = $('#pricing_based_selection').val()
		$("#appendPricingBasedPricingDetail").empty()
		$('#pricingbasedotherclear').val('')
		$('#byotherperson').addClass('hide')
		$("#bypricepersontablehideshow").removeClass('hide')
		$("#byracingperpersonhideshow").addClass('hide')
		if (tempValue == "BRC") {
			$("#byracingperpersonhideshow").removeClass('hide')
			generateclass_Division_distance()
		} else if (tempValue == "PP") {
			$("#byracingperpersonhideshow").removeClass('hide')
			generatePerpersonPriceBased()
		} else if (tempValue == "OT") {
			$('#byotherperson').removeClass('hide')
		} else {
			$('#byotherperson').addClass('hide')
			$("#bypricepersontablehideshow").addClass('hide')
		}
		if (eventPrice_basedDetailScript != "") {
			EditClassDivisionStep2()
		}
	})

	$(document).on('click', "#submitrosterlatermodel", function (e) {
		e.stopPropagation();
		e.preventDefault()
		$('#loading-spinner').removeClass('hide')
		$("#roster_reg_err_msgs").text("")
		teampEvent_EveRegId = $(this).attr("value")
		teampEvent_EveRegId =teampEvent_EveRegId.split(',')
		var teamSlug =""
	
		if ($('#getTeamSlug').val() != ""){
			teamSlug = $('#getTeamSlug').val();
		}
		console.log(eventOrganizerStatus , "" , eventCoOrganizerStatus)
		console.log("--",$('#getTeamSlug').val())

		$.ajax({
			type: "GET",
			url: "/get_later_submit_roster_detail",
			data: {"eventid": teampEvent_EveRegId[0],
			       "regeventid": teampEvent_EveRegId[1] }
		}).done(function(tempmsgDetailList) {
			tempmsg = JSON.stringify(tempmsgDetailList)
			if(tempmsg != "null"){
				rooster_Data1 =JSON.parse(tempmsg)
				if(rooster_Data1.teamroosterlist && rooster_Data1.registerlaterclassdivisionlist){
					
				 newhtml=""
				 for(var i=0; i< rooster_Data1.registerlaterclassdivisionlist.length; i++){
				 newhtml += '<tr style="border-bottom: 1px solid #ddd;" class="table_tr_submitlaterrosterDetail">'
				 var boat_type ="" 
				 var boat_type1 ="" 
				 var classtype ="" 

				 if(rooster_Data1.registerlaterclassdivisionlist[i].boat_type != "") {
					boat_type = "_"+rooster_Data1.registerlaterclassdivisionlist[i].boat_type
					boat_type1 = rooster_Data1.registerlaterclassdivisionlist[i].boat_type+" Person "
				 }

				 if(rooster_Data1.registerlaterclassdivisionlist[i].class == "M") {
					classtype ="Men"
				 }else if(rooster_Data1.registerlaterclassdivisionlist[i].class == "W") {
					classtype ="Women"
				 }else if(rooster_Data1.registerlaterclassdivisionlist[i].class == "MX") {
					classtype ="Mixed"
				 }

         valuetemp= rooster_Data1.registerlaterclassdivisionlist[i].div+'_'+rooster_Data1.registerlaterclassdivisionlist[i].class+boat_type
				 newhtml +='<td colspan="2" data-value='+rooster_Data1.registerlaterclassdivisionlist[i].div+boat_type+'>'+boat_type1+'</td>'
				 newhtml +='<td colspan="2">'+rooster_Data1.registerlaterclassdivisionlist[i].div_name+'</td>'
				 newhtml +='<td colspan="2" data-value='+rooster_Data1.registerlaterclassdivisionlist[i].class+'>'+classtype+'</td>'
				 newhtml +='<td colspan="2" data-value='+rooster_Data1.registerlaterclassdivisionlist[i].distanceValue+'>'+rooster_Data1.registerlaterclassdivisionlist[i].distance+'</td>'
				 newhtml +='<td colspan="4" >'
				 newhtml +='<div class="form-group">'
				 newhtml +='<select id="selectedrosterid" name="" class="select-custom form-control reset_roster_list '+valuetemp+'" style="width: 100%;">'
				 newhtml +='<option value="">- Select Roster -</option>'
				 for(var j=0; j< rooster_Data1.teamroosterlist.length; j++){
					 if(rooster_Data1.teamroosterlist[j].id == rooster_Data1.registerlaterclassdivisionlist[i].lastrosterid){
						newhtml +='<option selected value='+rooster_Data1.teamroosterlist[j].id+'>'+rooster_Data1.teamroosterlist[j].name+'</option>'
					 }else {
						newhtml +='<option value='+rooster_Data1.teamroosterlist[j].id+'>'+rooster_Data1.teamroosterlist[j].name+'</option>'
					 }
				 
				 }
				 newhtml +='</select>'
				 newhtml +='</div>'
				 newhtml +='</td>'
				 newhtml +='</tr>'
				 }
				 $("#table_submit_later_roosterappend").empty().append(newhtml);
		 
			   $("#submit_roster_model").modal("show");
				 $("#RegisterEventId").val(teampEvent_EveRegId[1])
				 $('#loading-spinner').addClass('hide')
				}else {
					if (!eventOrganizerStatus && !eventCoOrganizerStatus){
						$("#defaulterrorHeading").html("You will need to create rosters for each racing category before submitting. To do this, please navigate to your <a id='navigateToRosterTab' href='#'>Rosters Tab.</a>");

					}
					else{
						$("#defaulterrorHeading").html("You will need to create rosters for each racing category before submitting. To do this, please navigate to your Rosters Tab");

					}
					
					$("#defaulterrorTitle").text("Create Roster(s) First!");
					$("#defaulterrormodel").modal("show");
					$('#loading-spinner').addClass('hide')
				}
			}else {
				$('#loading-spinner').addClass('hide')
			}
		});
	});
	
	$(document).on('click', "#navigateToRosterTab", function (e) {
		e.preventDefault()
		console.log($('#getTeamSlug').val())
		$("#defaulterrormodel").modal("hide");
		sessionStorage.setItem('activeTab', '#roaster')
		window.location.href = $('#getTeamSlug').val();

	});
	$(document).on('click', "#later_roster_submit", function (e) {
		e.stopPropagation();
		e.preventDefault()
		$('#loading-spinner').removeClass('hide')
		if($('#eventpayment').is(":visible")){
			localStorage.setItem("str_from", "eventpayment")
		} else if($('#teampayment').is(":visible")){
			localStorage.setItem("str_from", "teampayment")
		}else if($('#eventregister').is(":visible")){
			localStorage.setItem("str_from", "eventRegister")
		}
		console.log("str_from-",localStorage.getItem("str_from"))
		var selectedDivisionArray = [];
		var selectedClassArray = [];
		var selectedDistanceArray = [];
		var selectedRosterIdArray = [];
		var isSeletedRosterStatus = true
		$("#roster_reg_err_msgs").text("")

		$(".table_tr_submitlaterrosterDetail").each(function () {
			tempselectedDivision_BoatValue = $(this).find("td:eq(0)").attr('data-value')
			tempselectedClassesValue = $(this).find("td:eq(2)").attr('data-value')
			tempselectedDistanceValue = $(this).find("td:eq(3)").attr('data-value')
			tempselectedrosterValue = $(this).find('#selectedrosterid').val()
			if (tempselectedDivision_BoatValue != "" && tempselectedDivision_BoatValue && tempselectedClassesValue != "" && tempselectedClassesValue && tempselectedrosterValue != "" && tempselectedrosterValue) {
				selectedDivisionArray.push(tempselectedDivision_BoatValue.trim())
				selectedClassArray.push(tempselectedClassesValue.trim())
				selectedDistanceArray.push(tempselectedDistanceValue.trim())
				selectedRosterIdArray.push(tempselectedrosterValue.trim())
			}
		});

		$('#SelectedDivisionArray').val(selectedDivisionArray)
		$('#SelectedClassArray').val(selectedClassArray)
		$('#SelectedDistanceArray').val(selectedDistanceArray)
		$('#SelectedRosterIdArray').val(selectedRosterIdArray)

		if (selectedDivisionArray.length == 0 || selectedClassArray.length == 0) {
			isSeletedRosterStatus = false
			if (selectedRosterIdArray.length == 0) {
				$("#roster_reg_err_msgs").text("Please select the roster")
			}
		}

		if (isSeletedRosterStatus) {
			$('#submitlaterrosterform').submit();
		} else {
			$('#loading-spinner').addClass('hide');
			$("#submit_roster_model").animate({ scrollTop: 0 }, "slow");
			return false
		}
	});

	$(document).on('click', "#ordereditpopup", function (e) {
		e.preventDefault()
		if($('#eventpayment').is(":visible")){
					localStorage.setItem("str_from", "eventpayment")
			} else if($('#teampayment').is(":visible")){
					localStorage.setItem("str_from", "teampayment")
			}else if($('#eventregister').is(":visible")){
					localStorage.setItem("str_from", "eventRegister")
			}
		$('#ordereditmodelpopup').modal('show')
		
	});
	$('#payTeam').click(function (eve) {
		eve.preventDefault()
		$('#teampaymentpopup').modal('show')
	});
	
	$('.event_payment_btn').click(function (eve) {
		eve.preventDefault()
		console.log("getstripeconnectstatus--",$('#getstripeconnectstatus').val())

		$('#showOwingBalance').val("")
		$('#tran_note').val("")
		$('#OfflinePay').val("")
		$('#OfflineRefund').val("")
		$('#paypopup_err').text("")

		var getTeamName,getBalanceAmt,getBalanceAmtFormatted,getEventRegPayId1,payStatus,getTotalEventFee,getEventReceive,getDiscount

		if($('#getstripeconnectstatus').val() == "false"){
			$('#paytermsallowsuccesmodal').modal('show')
			$('#paytermsallowsuccesmodalText').html("Receive Payments Online")
			$('#paytermsallowsuccesmodal_Body').html("<p>Please, set up your payment information to collect payments through Gushou. All payments through Gushou are processed using <a target='_blank' href='https://stripe.com/ca/customers?&utm_campaign=paid_brand&utm_medium=cpc&utm_source=google&ad_content=275961462204&utm_term=stripe&utm_matchtype=e&utm_adposition1t1&utm_device=c&gclid=EAIaIQobChMIxqi_odHX3AIVAzJpCh0RLAzqEAAYASAEEgKW4PD_BwE'>Stripe</a>.</p><p>To get started, please select the "+'"'+"Set Up Payment Info"+'"'+" button on your Payments Tab of your Event Page.</p>")
			
		}else{

			var row = $(this).closest("tr"); 
			var getEventRegPayId = row.find(".getEventRegPayId").text();
			if (getEventRegPayId == ""){
				getEventRegPayId = $("#getEventRegPayId_order").val();
			}
			var getCurrency =  $(".getCurrency").text();
			console.log("getEventRegPayId",getEventRegPayId,"getCurrency",getCurrency)

			 $.ajax({
				url: "/get_event_team_payment_detail",
				type: "POST",
				data: {
				  "event_reg_pay_id": getEventRegPayId,
				  },
				success: function (data) {

				  data1 = JSON.stringify(data)
				  if(data1 != null){
					console.log(data1)
					getTeamName = data.Result[0].team_name;
					getBalanceAmt = data.Result[0].balance_amount;
					getBalanceAmtFormatted = data.Result[0].balance_formatted;
					getDiscount = data.Result[0].discount;
					getEventRegPayId1 = data.Result[0].eventpayid;
					payStatus = data.Result[0].paymentstatus;
					getTotalEventFee = data.Result[0].total_event_fee;
					getEventReceive= data.Result[0].total_event_receive;
					changeefeedetail = data.Result[0].changeefeedetail;
					
			$('#eventpaymentpopup').modal('show')
					
			if(getDiscount == "N/A"){
				$("input[type=radio][value = owing]").prop("disabled",true);
				$('#showOwingBalance').addClass('hide')
				$("input[type=radio][value = owing]").attr("checked",false);
				$("input[type=radio][value = fee]").prop("disabled",false);
				$('.offlinefeelabel').text("Apply Fees");
				$("input[type=radio][value = offlinepay]").prop("disabled",true);
				$("input[type=radio][value = offlinerefund]").prop("disabled",true);
			}else if(changeefeedetail == "true"){
				$("input[type=radio][value = owing]").prop("disabled",false);
				$('#showOwingBalance').removeClass('hide')
				$("input[type=radio][value = owing]").attr("checked",true);
				$("input[type=radio][value = fee]").prop("disabled",true);
				$('.offlinefeelabel').text("Apply Fees");
				$("input[type=radio][value = offlinepay]").prop("disabled",true);
				$("input[type=radio][value = offlinerefund]").prop("disabled",true);
			}else {
				$("input[type=radio][value = owing]").prop("disabled",false);
				$('#showOwingBalance').removeClass('hide')
				$("input[type=radio][value = owing]").attr("checked",true);
				$("input[type=radio][value = fee]").prop("disabled",false);
				$('.offlinefeelabel').text("Fee does not apply");
				$("input[type=radio][value = offlinepay]").prop("disabled",false);
				$("input[type=radio][value = offlinerefund]").prop("disabled",false);
			}
			$('#eventpaymentpopup_Body #getPayTeamName').text(getTeamName)
			$('#eventpaymentpopup_Body #getOweAmount').text(getBalanceAmtFormatted)
			$('#eventpaymentpopup_Body #getOweAmount').val(getBalanceAmt)
			$('#eventpaymentpopup_Body #getOweAmount1').val(getBalanceAmt)
			$('#eventpaymentpopup_Body #eventpayregsid').val(getEventRegPayId)
			$('#eventpaymentpopup_Body #getcurrenytype').val(getCurrency)
			$('#eventpaymentpopup_Body #geteventfee').val(getEventReceive)
			$('#eventpaymentpopup_Body #gettotaleventfee').val(getTotalEventFee)
			}
			}
		});
		}	
	});

	$(document).on('click', "#event_track_payments", function (e) {
		console.log("----",$(this).attr('value'))
		$('#loading-spinner').removeClass('hide');
		var row = $(this).closest("tr");
		var getEventPayId = $(".getEventIdPay").text();
		var getTeamPayId = $(".getTeamIdPay").text();
		console.log(getEventPayId +"----" + getTeamPayId)
		var id = $(this).attr('value');
		window.location.href = "/event-view/event-payment-track/" + id +"/" +getEventPayId +"/"+getTeamPayId;
	});

	$(document).on('click', "#paytrackbackbread", function (e) {
		e.preventDefault()
		
		if ($(this).attr('value') == "#paytrackbackbread") {
			sessionStorage.setItem('eventactiveTab', '#eventPaymentList')
			window.location.href = "/event-view/" + $('#event_slug_hidden_log').attr('value') + "/event-payment-view";
		}
	});

	$(document).on('click', "#eventpayment", function (e) {
		e.preventDefault()
		if ($(this).attr('value') == "#eventpayment") {	
		window.location.href = "/event-view/" + $('#event_slug_hidden').attr('value') + "/event-payment-view";
		sessionStorage.setItem('eventactiveTab', '#eventPaymentList')
		}
	});
	
	$(document).on('click', "#fromeventpayment", function (e) {
		//e.preventDefault()
		if ($(this).attr('value') == "eventpayment") {
			localStorage.setItem("str_from", "eventpayment")
		}	
	});
	$(document).on('click', "#fromeventteam", function (e) {
		//e.preventDefault()
		if ($(this).attr('value') == "eventTeamName") {
			localStorage.setItem("str_from", "eventteam")
		}	
	});
	  $(document).on('click', "#teampayment", function (e) {
		e.preventDefault()
		if ($(this).attr('value') == "#teampayment") {	
		window.location.href = "/team-view/" + $('#team_slug_hidden').attr('value') + "/team_event_payment";
		sessionStorage.setItem('activeTab',"#team_event_payment")
		}
	});
	$(document).on('click', "#teamnameforteamtab", function (e) {
		e.preventDefault()
		if ($(this).attr('value') == "#teamNameForTeamTab") {	
		window.location.href = "/team-view/" + $('#team_slug_hidden').attr('value');
		sessionStorage.setItem('activeTab',"#home")
		}
	});
	
	$(document).on('click', "#fromteampayment", function (e) {
		//e.preventDefault()
		if ($(this).attr('value') == "teampayment") {
			localStorage.setItem("str_from", "teampayment")
		}
	});

	$('#savePayDetails').click(function (eve) {
		eve.preventDefault()
		var radioValue = $("input[name='payoptions']:checked").val();
		localStorage.setItem("str_from", "eventpayment")
		var amount="",discount="",event_reg_pay_id,transaction_note="",currency="";
		event_reg_pay_id=$('#eventpayregsid').val();
		transaction_note = $('#tran_note').val()
		currency = $('#getcurrenytype').val()
		var currentEventFee =  parseFloat($('#geteventfee').val());
		var totalEventFee =  parseFloat($('#gettotaleventfee').val());
		var balanceEventFee = totalEventFee - currentEventFee
		var main_balance_to_pay = parseFloat($('#getOweAmount').val());

		
		if(radioValue == "owing"){
			
			var minimumowingamount = main_balance_to_pay - balanceEventFee

			if($('#showOwingBalance').val() == ""){
				$('#paypopup_err').text("Enter value for choosen option")
			}
			else if (parseFloat($('#showOwingBalance').val()) < minimumowingamount ){
				$('#paypopup_err').text("Owing amount not below this amount " + minimumowingamount)
			}
			else{
				amount = $('#showOwingBalance').val()
				$('#paypopup_err').text("")
				console.log(transaction_note,"--",amount)
				sendEventPayment(amount,discount,event_reg_pay_id,transaction_note,currency,radioValue)
			}

		}
		else if(radioValue == "fee"){
			discount = "N/A"
			console.log(discount)
			sendEventPayment(amount,discount,event_reg_pay_id,transaction_note,currency,radioValue)
		}
		else if(radioValue == "offlinepay"){
			
			if($('#OfflinePay').val() == ""){
				$('#paypopup_err').text("Enter value for choosen option")
			}else if (parseFloat($('#OfflinePay').val()) <= 0){
				$('#paypopup_err').text("Amount should be greater than 0")
			}
			else if (parseFloat($('#OfflinePay').val()) > main_balance_to_pay){
					$('#paypopup_err').text("Amount is greater than Owe")
			}
			else{
				amount = $('#OfflinePay').val()
				$('#paypopup_err').text("")
				console.log(transaction_note,"--",amount,"--",radioValue)
				console.log("radioValue---",radioValue)
				sendEventPayment(amount,discount,event_reg_pay_id,transaction_note,currency,radioValue)
			}
			
		}
		else if(radioValue == "offlinerefund"){
			
			if($('#OfflineRefund').val() == ""){
				$('#paypopup_err').text("Enter value for choosen option")
			}
			else if (parseFloat($('#OfflineRefund').val()) <= 0){
				$('#paypopup_err').text("Amount should be greater than 0")
			}
			else if(parseFloat($('#OfflineRefund').val()) > parseFloat($('#geteventfee').val())){
				console.log($('#OfflineRefund').val() ,"--",$('#geteventfee').val())
				$('#paypopup_err').text("Refund amount is greater than Net Fee; amount deposited into your account after Gushou Fee.")
			}
			else{
				amount = $('#OfflineRefund').val()
				$('#paypopup_err').text("")
				console.log(transaction_note,"--",amount)
				sendEventPayment(amount,discount,event_reg_pay_id,transaction_note,currency,radioValue)
			}	
		}
		else if(radioValue == "recordnote"){
			sendEventPayment(amount,discount,event_reg_pay_id,transaction_note,currency,radioValue)
		}else {
			$('#paypopup_err').text("Please select atleast one option.")
		}
		
	
	});

	$(document).on('click', "#sortby_teamname", function (e) {
		e.preventDefault()

		tempId1 = document.getElementById("eventteamregisterdetail_sortbyteamname")
		
		if (tempId1.style.display == ""){
			$("#eventteamregisterpaymentdetail").css("display", "");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "none");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "none");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "none");
		}
		else {
			$("#eventteamregisterpaymentdetail").css("display", "none");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "none");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "none");
		}

	});
	
	$(document).on('click', "#sortby_paystatus", function (e) {
		e.preventDefault()
	
		tempId1 = document.getElementById("eventteamregisterdetail_sortbypaystatus")
		
		if (tempId1.style.display == ""){
			$("#eventteamregisterpaymentdetail").css("display", "");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "none");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "none");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "none");
		}
		else {
			$("#eventteamregisterpaymentdetail").css("display", "none");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "none");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "none");
		}


	});
	$(document).on('click', "#sortby_balance", function (e) {
		e.preventDefault()

		tempId1 = document.getElementById("eventteamregisterdetail_sortbybalanceamt")

		if (tempId1.style.display == ""){
			$("#eventteamregisterpaymentdetail").css("display", "");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "none");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "none");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "none");
		}
		else {
			$("#eventteamregisterpaymentdetail").css("display", "none");
			$("#eventteamregisterdetail_sortbyteamname").css("display", "none");
			$("#eventteamregisterdetail_sortbypaystatus").css("display", "none");
			$("#eventteamregisterdetail_sortbybalanceamt").css("display", "");
		}



	});
	
	function sendEventPayment(amount,discount,eventregpayid,note,currency,radioValue){
		$('#loading-spinner').removeClass('hide')
		$.ajax({
			url: "/event_payment_manual",
			type: "POST",
			data: {"event_reg_pay_id": eventregpayid,
				   "amount_paid": amount,
					"apply_discount" : discount,
					"transaction_note":note,
					"currency":currency,
					"choosen_type":radioValue
				},
			success: function (data) {
				tempmsgdata = JSON.stringify(data)
				console.log("aloow----",tempmsgdata)
				var result = JSON.parse(tempmsgdata)

				console.log("pay_status--",result.PayStatus)
				console.log("choosen_type--",result.ChoosenType)
		
				if(result.PayStatus == "true"){
					$('#paypopup_err').text("")
					$('#paypopup_succ').text("Successfully Saved")
					setTimeout(function () {
					$('#eventpaymentpopup').modal('hide')
					$('#loading-spinner').addClass('hide')
					location.reload()
					}, THREE_SEC);	
					
				}else if(result.PayStatus != ""){
					$('#paypopup_succ').text("")
					$('#paypopup_err').text(result.PayStatus)
					$('#loading-spinner').addClass('hide')
					// //setTimeout(function () {
					// $('#eventpaymentpopup').modal('hide')
					// $('#loading-spinner').addClass('hide')
					// location.reload()
					// }, THREE_SEC);	
					
				}else{
					$('#paypopup_succ').text("")
					$('#paypopup_err').text("Some Problem occur.. try after some time")
					location.reload()
					$('#eventpaymentpopup').modal('hide')
					$('#loading-spinner').addClass('hide')
				}
				
			}
		});
	}
	
	$('.paytermsdeny').click(function (e) {
		e.preventDefault();
		console.log("deny")
		$('#paymentTermsConditionModal').modal('hide');
		$('#paydenymodal').modal('show');
		$('#paydenymodal_Body').text("Connect to Stripe to accept or proceed for further payments")
	});
	

	
	$('.requesttoconnectaccount').click(function (e) {
		e.preventDefault();
		console.log("click to connect")
		$('#loading-spinner').removeClass('hide')
		$.ajax({
			url: "/request_to_connet_stripe",
			type: "POST",
			data: {"currenteventId": $("#currenteventId").val()},
			success: function (data) {
				console.log("aloow----",data)
				if (data == "true") {
					sessionStorage.setItem('requesttostripe', "true")
					$('#loading-spinner').addClass('hide')
					$('#paymentTermsConditionModal').modal('hide');
					$('#paydenymodal').modal('hide');
					
					$('#paytermsallowsuccesmodal').modal('show');
					$('#paytermsallowsuccesmodal_Body').html("An email with instructions to set-up and connect your Stripe account has been sent to your inbox. Once you complete this step, you are ready to start collecting race fees.")
				
				} 
				else {
					sessionStorage.setItem('requesttostripe', "false")
					$('#loading-spinner').addClass('hide')
					$('#paymentTermsConditionModal').modal('hide');
					$('#paytermsallowsuccesmodal').modal('show');
					$('#paydenymodal').modal('hide');
					$('#paytermsallowsuccesmodal_Body').text("The is some problem please try after sometime!!")
	
				  }

			}
		});
	});


	$(document).on('click', "#teamtabpaystatus", function (e) {
		e.preventDefault();
		sessionStorage.setItem('eventactiveTab', '#eventPaymentList')
		window.location.href = "/event-view/" + $('#getEventSlug').val() + "/event-payment-view";

	});

	
	$('#filter_all').click(function () {
		if(this.checked){
			$('.check_all').prop('checked', true);
					
		}else {
			$('.check_all').prop('checked', false);
		}
	});

	$('.check_all').click(function () {
		if(!this.checked){
			$('#filter_all').prop('checked', false);		
		}
	});

	$('#filter_all-waivers').click(function () {
		if(this.checked){
			//$("#searchByTeamNameWaiverFilter").val("")
			$('.check_all_waiver').prop('checked', true);
					
		}else {
			$('.check_all_waiver').prop('checked', false);
		}
	});

	$('.check_all_waiver').click(function () {
		if(!this.checked){
			$('#filter_all-waivers').prop('checked', false);		
		}
	});
	
	
	$('#select_all_members_for_payment_mail').click(function () {
		if(this.checked){
			$('.checkedTeamsPay').prop('checked', true);
					
		}else {
			$('.checkedTeamsPay').prop('checked', false);
		}
	});

	$('#select_all_teams_waiver').click(function () {
		if(this.checked){
			console.log("hii")
			$('.check_teams_waivers').prop('checked', true);
					
		}else {
			console.log("bye")
			$('.check_teams_waivers').prop('checked', false);
		}
	});


	$(document).on('click', '.check_teams_individual_waivers', function () {
		var val = $(this).val()
		console.log("hi",val)
		var val_arr = val.split("_")
		var event_reg_id = val_arr[4]
		console.log("val-",val)
		console.log("val_arr-",val_arr)
		console.log("event_reg_id-",event_reg_id)
		if (!this.checked){
			$("#check_teams_waiver_"+event_reg_id).prop('checked', false);
		}
	});
	$(document).on('click', '.check_team_name_waiver', function () {
		var val = $(this).val()
		console.log("hi",val)
		var val_arr = val.split("_")
		var event_reg_id = val_arr[1]
		console.log("val-",val)
		console.log("val_arr-",val_arr)
		console.log("event_reg_id-",event_reg_id)
		if (this.checked){
			$(".tm_"+event_reg_id).prop('checked', true);
		}else{
			$(".tm_"+event_reg_id).prop('checked', false);
		}
	});

	


	$('.checkedTeamsPay').click(function () {
		var val = $(this).val()
			if(this.checked){
				$("#regpay_"+this.value).prop('checked', true);
				$("#regpay_sortbyteamname_"+this.value).prop('checked', true);
				$("#regpay_sortbypaystatus_"+this.value).prop('checked', true);
				$("#regpay_sortbybalanceamt_"+this.value).prop('checked', true);

			}else {
				$("#regpay_"+this.value).prop('checked', false);
				$("#regpay_sortbyteamname_"+this.value).prop('checked', false);
				$("#regpay_sortbypaystatus_"+this.value).prop('checked', false);
				$("#regpay_sortbybalanceamt_"+this.value).prop('checked', false);
	
			}	
	});

	$(document).on("keypress keyup blur scroll", '.event-user-input-val', function (event) {
		this.value = this.value.replace(/[^0-9\.]/g, '');
	});

	$(document).on('click', "#viewtermscondition", function (e) {
		e.preventDefault();
		
		$('#termsConditionEventPaymentModal').modal('show');
	});
	$('#emailTeamAboutPayment').click(function (eve) {
		eve.preventDefault();
		var selectedMemForEmail = [] ;
		$('input:checkbox[name="team_members_pay[]"]').each(function () {
			if(this.checked){
				if (jQuery.inArray(this.value, selectedMemForEmail) == -1) {
					selectedMemForEmail.push(this.value)
				}
			}	
		});
		$("#getEventRegIds").val(selectedMemForEmail)
		var eventRegIds = $('#getEventRegIds').val() ;
		console.log("selectedMemForPayEmail--",eventRegIds)
		if(eventRegIds == "" ){
			$("#paytermsallowsuccesmodal").modal("show");
			$("#paytermsallowsuccesmodalText").text("")
			$("#paytermsallowsuccesmodal_Body").text("Choose atleast one team to send mail ")
			
		}
		else {
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/send-event-team-payment-email",
				data: {
					"event_reg_ids": eventRegIds,	
				}
			}).done(function (msg) {
	
				if (msg == "true") {
					$('#loading-spinner').addClass('hide');
					$("#paytermsallowsuccesmodal").modal("show");
					$("#paytermsallowsuccesmodalText").text("Payment Remainder")
					$("#paytermsallowsuccesmodal_Body").text("A payment reminder has been emailed to selected team contact(s). You have been cc'd.")
					setTimeout(function () {
					location.reload()
					}, FIVE_SEC);
				}
				else {
					$('#loading-spinner').addClass('hide');
					$("#paytermsallowsuccesmodal").modal("show");
					$("#paytermsallowsuccesmodalText").text("Payment Remainder")
					$("#paytermsallowsuccesmodal_Body").text("Mail not send. Try after some time")
					setTimeout(function () {
					location.reload()
					}, FIVE_SEC);
				}
			});
	}
	});
	
	$("#eventpaymentfilter button[type='submit']").click(function () {
		$('#eventpaymentfilter').submit(function () {
			sessionStorage.setItem('eventactiveTab','#eventPaymentList')
		$('#loading-spinner').removeClass('hide');
		$('input').val(function (_, value) {
			return $.trim(value);
		});
		$('#eventpaymentfilter').submit()
	})
	});

	$(document).on('click', "#togglePaymentTrackingByInvoiceId", function (e) {
		e.preventDefault();
			if($("#payment_tracking_by_invoice_id").hasClass('hide')){
			$("#payment_tracking_by_invoice_id").removeClass('hide')
		}
		else {
			$("#payment_tracking_by_invoice_id").toggle();		
		} 
	});
	$(document).on('click', '.trackIndividualPayment', function (eve) {
		eve.preventDefault()
		$('#payment_tracking').removeClass('hide')
		$('#payment').addClass('hide')
		
	});
	$(document).on('click', '#payment_breadcrumb', function (eve) {
		eve.preventDefault()
		$('#payment_tracking').addClass('hide')
		$('#payment').removeClass('hide')
		
	});
	$(document).on('click', "#toggleOrderLog", function (e) {
		e.preventDefault();
		tempId1 = document.getElementsByClassName("changeOrderLog")
		if(tempId1[0].style.display == "none"){
			$(".changeOrderLog").toggle();		
			$("#orderarrowicon").attr('src','/static/images/arrow_down.png')
		}
		else {
			$(".changeOrderLog").toggle();		
			$("#orderarrowicon").attr('src','/static/images/arrow_up.png')		
		} 
	});
	$(document).on('click', "#toggleAdditionalQuestion", function (e) {
		e.preventDefault();
		tempId1 = document.getElementsByClassName("changeAdditionalQuestionLog")

		if(tempId1[0].style.display == "none"){
			$(".changeAdditionalQuestionLog").toggle();		
			$("#orderquestionaryicon").attr('src',"/static/images/arrow_down.png")
		}
		else {
			$(".changeAdditionalQuestionLog").toggle();		
			$("#orderquestionaryicon").attr('src',"/static/images/arrow_up.png")		
		} 
	});
	$('input[type=radio][name=payoptions]').change(function () {
		console.log("radio--",this.value)
		
		if(this.value == "owing") {
			$('.payPopupTextBox').val("")
			$('#showOwingBalance').removeClass('hide')
			//$('#FeeNotApply').addClass('hide')
			$('#OfflinePay').addClass('hide')
			$('#OfflineRefund').addClass('hide')
			//$('#RecordNote').addClass('hide')
		}
		else if(this.value == "fee") {
			$('.payPopupTextBox').val("")
			$('#showOwingBalance').addClass('hide')
			//$('#FeeNotApply').removeClass('hide')
			$('#OfflinePay').addClass('hide')
			$('#OfflineRefund').addClass('hide')
			//$('#RecordNote').addClass('hide')
		}
		else if(this.value == "offlinepay") {
			$('.payPopupTextBox').val("") 
			$('#showOwingBalance').addClass('hide')
			//$('#FeeNotApply').addClass('hide')
			$('#OfflinePay').removeClass('hide')
			$('#OfflineRefund').addClass('hide')
			//$('#RecordNote').addClass('hide')
		}
		else if(this.value == "offlinerefund") {
			$('.payPopupTextBox').val("")
			$('#showOwingBalance').addClass('hide')
			//$('#FeeNotApply').addClass('hide')
			$('#OfflinePay').addClass('hide')
			$('#OfflineRefund').removeClass('hide')
			//$('#RecordNote').addClass('hide')
		}
		else if(this.value == "recordnote") {
			
			$('#showOwingBalance').addClass('hide')
			//$('#FeeNotApply').addClass('hide')
			$('#OfflinePay').addClass('hide')
			$('#OfflineRefund').addClass('hide')
			//$('#RecordNote').removeClass('hide')
		}
		else{
			console.log("no match")
		}
	});

	$(document).on('change, keyup', ".edit_order_item_change", function (e) {
	 totalValueOfOrder = 0
	 $('input[name="per_person_quantity[]"]').each(function () {
		tempvalue =0
		tempquantity = this.value;
		tempindexId = $(this).attr('data-index')
		tempcost =$('#edit_per_person_cost'+tempindexId).html().trim()
		tempvalue =((tempcost * tempquantity).valueOf()).toFixed(0);
		$('#edit_per_person_total_cost'+tempindexId).html(tempvalue)
		totalValueOfOrder =totalValueOfOrder + Number(tempvalue)
	 });

	 $('input[name="iteam_id_quantity[]"]').each(function () {
		tempvalue =0
		tempquantity = this.value;
		tempindexId = $(this).attr('data-index')
		tempcost =$('#edit_order_iteam_cost'+tempindexId).html().trim()
		tempvalue =((tempcost * tempquantity).valueOf()).toFixed(0);
		$('#edit_order_iteam_total_cost'+tempindexId).html(tempvalue)
		totalValueOfOrder =totalValueOfOrder + Number(tempvalue)
	 });

	 if($('#additional_quantity').length >0){
		 tempvalue =0
		 addquantityValue = $('#additional_quantity').val();
		 addquantitycostValue = $('#additional_quantity_cost').html().trim();
		 tempvalue =((addquantitycostValue * addquantityValue).valueOf()).toFixed(0);
		 $('#additional_quantity_cost_total').html(tempvalue)
		 totalValueOfOrder =totalValueOfOrder + Number(tempvalue)	
	 }
	 var overallamount = orderEditOverallAmount
	 overallamount = overallamount.split(',').join('');
	 var diff;
	 var finalTotalOrderVal;
	 diff = totalValueOfOrder - overallamount
	 if(overallamount < totalValueOfOrder){
		 finalTotalOrderVal = $('#getCurrencySymbol').val() +" "+overallamount.toLocaleString() +"(+"+diff+")";
	 }
	 else if(overallamount == totalValueOfOrder){
		finalTotalOrderVal = $('#getCurrencySymbol').val() +" "+overallamount.toLocaleString();
	 }
	 else {
		finalTotalOrderVal = $('#getCurrencySymbol').val() +" "+overallamount.toLocaleString() +"("+diff+")";
	 }
	 $('#overallOrderDetailCount').html(finalTotalOrderVal)
	});

	$("#edit_order_detail_submit").click(function () {
		$('#loading-spinner').removeClass('hide')
		
		isFormValid = true
		$("#order_edit_err_msgs").text('')
		$('[name="per_person_quantity[]"]').each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				$("#order_edit_err_msgs").text("Please enter a quantity of at least 0 for each item")
				isFormValid = false;
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		$('[name="iteam_id_quantity[]"]').each(function () {
			if ($.trim($(this).val()).length == 0) {
				$(this).addClass("empty-err");
				$("#order_edit_err_msgs").text("Please enter a quantity of at least 0 for each item")
				isFormValid = false;
			}
			else {
				$(this).removeClass("empty-err");
			}
		});

		if($('#additional_quantity').length){
			if($.trim($('#additional_quantity').val()).length == 0){
				$('#additional_quantity').addClass("empty-err");
				$("#order_edit_err_msgs").text("Please enter a quantity of at least 0 for each item")
				isFormValid = false;
			}else {
				$('#additional_quantity').removeClass("empty-err");
			}
		}
		
		if($.trim($('#changeordernotetext').val()).length == 0){
			$('#changeordernotetext').addClass("empty-err");
			$("#order_edit_err_msgs").text("Please add a note")
			isFormValid = false;
		}else {
			$('#changeordernotetext').removeClass("empty-err");
		}
    if (isFormValid) {
		
		sessionStorage.setItem('orderlogstatus', 'show')
				$('#allordereditemForm').submit();	
				
		}else {
			  $('#loading-spinner').addClass('hide')
				$("#ordereditmodelpopup").animate({ scrollTop: 0 }, "slow");
				return false;
		}

	});
	
	$('#paymentTermsConditionId').click(function (e) {
		e.preventDefault();
		var requestToStripeStatus = sessionStorage.getItem('requesttostripe')
		console.log("requestToStripeStatus--",requestToStripeStatus)
		if(requestToStripeStatus == "false"){
			$('#paymentTermsConditionModal').modal('show');
		}
		else{
			$('#paytermsallowsuccesmodal').modal('show');
			$('#paytermsallowsuccesmodal_Body').html("An email with instructions to set-up and connect your Stripe account has been sent to your inbox. Once you complete this step, you are ready to start collecting race fees.")
		}
	});

	$('.paymentTermsConditionId').click(function (e) {
		e.preventDefault();
		var requestToStripeStatus = sessionStorage.getItem('requesttostripe')
		console.log("requestToStripeStatus--",requestToStripeStatus)
		if(requestToStripeStatus == "false"){
			$('#paymentTermsConditionModal').modal('show');
		}
		else{
			$('#paytermsallowsuccesmodal').modal('show');
			$('#paytermsallowsuccesmodal_Body').html("An email with instructions to set-up and connect your Stripe account has been sent to your inbox. Once you complete this step, you are ready to start collecting race fees.")
		}
	});
	
	$(".onoffswitch-checkbox").change(function() {
		console.log("checked---",$(this).prop("checked"))
		var requestToStripeStatus = sessionStorage.getItem('requesttostripe')
		console.log("requestToStripeStatus--",requestToStripeStatus)
		if (this.checked){
			console.log("getstripeconnectstatus--",$('#getstripeconnectstatus').val())
			if($('#getstripeconnectstatus').val() == "false"){
				if(requestToStripeStatus == "false"){
					$('#paymentTermsConditionModal').modal('show');
				}
				else{
					$('#paytermsallowsuccesmodal').modal('show');
					$('#paytermsallowsuccesmodal_Body').html("An email with instructions to set-up and connect your Stripe account has been sent to your inbox. Once you complete this step, you are ready to start collecting race fees.")
				}
			}
	
		}
		
	});
	
	$(document).on('click', "#paymentpageredirectclick", function (e) {
		sessionStorage.setItem('activeTab', "#team_event_payment")
		window.location.href = "/team-view/" + $('#current_team_slug').val() + "/team_event_payment";
	});

	$(document).on('click', "#nonteamEventPaymentOption", function (e) {
		e.preventDefault()
		$('#nonteamEventPaymentOptionModel').modal('show');
	});

	//  add jquery changes by madhan
$('#manual-acc-tab').show();

$(document).on('click', ".general-accreditation-tab", function () {
	getactivevalue = sessionStorage.getItem('activeaccreditation')
	if(getactivevalue != "#General" ) {
	sessionStorage.setItem('activeaccreditation', "#General")
	sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
	window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-accreditation-general-view";
	}
});
$(document).on('click', ".manual-accreditation-tab", function () {

	getactivevalue = sessionStorage.getItem('activeaccreditation')
	if(getactivevalue != "#Manual" ) {
		sessionStorage.setItem('activeaccreditation', "#Manual")
		sessionStorage.setItem('eventactiveTab', $(this).attr('value'))
		window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-accreditation-manual-view";
	}
});

$(document).on('click', "#general_update_accreditation", function () {
	teamusereventid = $(this).attr('value')
	$('#acc_err_msgs').addClass('');
	$("#acc_succ_msgs").text('');
	$('#acc_err_msgs').text('');
	$("#acc_succ_msgs").addClass('');

	$('#accusername').val('')
	$('#accselectmultiple').val(0).trigger('change');
	$(".genaccessrolevalue").removeClass('roleeditactive')
	$('#even_manualtid').val('')
	$('#accaccessrole').val('')
	$('#accroledetail').val('')
	$('#accfilepath').val('')
	$("#imgaccInp").val(null);
	$('#accgenimageUpload').val('')
	$('#accmanualuserid').val('0')
	$('#accmanualupdateid').val('0')

	$.ajax({
		type: "POST",
		url: "/event/accgeneralupdatedetail",
		data: { "teamusereventid": teamusereventid }
	}).done(function (msg) {
		data1 = JSON.stringify(msg)
		if(data1 != null){
			data2 = JSON.parse(data1)
			console.log(data2.userdetails)
			 filepath = data2.userdetails[0]["filepath"]
			 name = data2.userdetails[0]["firstname"] +" "+ data2.userdetails[0]["lastname"]
			 role = data2.userdetails[0]["member_role"]
			 access = data2.userdetails[0]["access_role"]
			 $('#accmanualuserid').val(data2.userdetails[0]["userid"])

			 $('#accselectmultiple').val('').trigger('change');
			 var val_arr = access.split(",")
			 $(".genaccessrolevalue").removeClass('roleeditactive')
			 for(var i=0; i<val_arr.length;i++){
          if(val_arr[i] == "athletevillage" ){
						$("#acc_av_value").addClass('roleeditactive')
					}else if(val_arr[i] == "vip" ){
						$("#acc_vip_value").addClass('roleeditactive')
					}else if(val_arr[i] == "toweraccess" ){
						$("#acc_ta_value").addClass('roleeditactive')
					}else if(val_arr[i] == "mediacentre" ){
						$("#acc_mc_value").addClass('roleeditactive')
					}
			 }

			 var role_val_arr = role.split(",")
			 $("#img-acc-upload").attr('src',filepath)		
			 $('#accusername').val(name)
			 $('#accselectmultiple').val(role_val_arr).trigger('change');
			 $('#accfilepath').val(filepath)
			$('#acredationheadertext').text('Edit Accreditation')
			$("#general-update-model").modal("show");
		}
		
	});
});

$(document).on('click', "#manual_update_accreditation", function () {
	manualautoid = $(this).attr('value')
	$('#acc_err_msgs').addClass('');
	$("#acc_succ_msgs").text('');
	$('#acc_err_msgs').text('');
	$("#acc_succ_msgs").addClass('');

	$('#accusername').val('')
	$('#accselectmultiple').val(0).trigger('change');
	$(".genaccessrolevalue").removeClass('roleeditactive')
	$('#even_manualtid').val('')
	$('#accaccessrole').val('')
	$('#accroledetail').val('')
	$('#accfilepath').val('')
	$("#imgaccInp").val(null);
	$('#accgenimageUpload').val('')
	$('#accmanualuserid').val('0')
	$('#accmanualupdateid').val('0')
	$('.checkedManualUserTeam').prop('checked', false);
	$('.checkedManualUserNameTeam').prop('checked', false);
	$('#select_all_manual_accredation').prop('checked', false);
	$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	
	$.ajax({
		type: "POST",
		url: "/event/accmanualupdatedetail",
		data: { "manualautoid": manualautoid }
	}).done(function (msg) {
		data1 = JSON.stringify(msg)
		if(data1 != null){
			data2 = JSON.parse(data1)
			console.log(data2.userdetails)
			 filepath = data2.userdetails[0]["filepath"]
			 name = data2.userdetails[0]["firstname"] +" "+ data2.userdetails[0]["lastname"]
			 role = data2.userdetails[0]["member_role"]
			 access = data2.userdetails[0]["access_role"]
			 $('#accmanualuserid').val(data2.userdetails[0]["userid"])

			 if(data2.userdetails[0]["manualid"]) {
        $('#accmanualupdateid').val(data2.userdetails[0]["manualid"])
			 }else {
				$('#accmanualupdateid').val(0)
			 }

			 if(data2.userdetails[0]["userid"]) {
        $('#accmanualuserid').val(data2.userdetails[0]["userid"])
			 }else {
				$('#accmanualuserid').val(0)
			 }

			 $('#accselectmultiple').val('').trigger('change');
			 var val_arr = access.split(",")
			 $(".genaccessrolevalue").removeClass('roleeditactive')
			 for(var i=0; i<val_arr.length;i++){
          if(val_arr[i] == "athletevillage" ){
						$("#acc_av_value").addClass('roleeditactive')
					}else if(val_arr[i] == "vip" ){
						$("#acc_vip_value").addClass('roleeditactive')
					}else if(val_arr[i] == "toweraccess" ){
						$("#acc_ta_value").addClass('roleeditactive')
					}else if(val_arr[i] == "mediacentre" ){
						$("#acc_mc_value").addClass('roleeditactive')
					}
			 }

			 var role_val_arr = role.split(",")
			 $("#img-acc-upload").attr('src',filepath)		
			 $('#accusername').val(name)
			 $('#accselectmultiple').val(role_val_arr).trigger('change');
			 $('#accfilepath').val(filepath)
			 $('#acredationheadertext').text('Edit Accreditation')
			 
			$("#general-update-model").modal("show");
		}
		
	});
});

$(document).on('click', "#manual-model-acc", function () {
	$("#manual-model").modal("show");
});

$(document).on('click', "#accreditationusernamesortacending", function (e) {
	e.preventDefault()
	
	$('.checkedGeneralTeamSort').prop('checked', false);
	$('.checkedGeneralUsernameSort').prop('checked', false);
	$('#select_all_accredation').prop('checked', false);
	$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);

	tempId1 = document.getElementById("accreditationGeneralNameAcendingList")
	
	if (tempId1.style.display == ""){
		$("#accreditationGeneralNameAcendingList").css("display", "none");
		$("#accreditationGeneralTeamAcendingList").css("display", "");
	}
	else {
		$("#accreditationGeneralNameAcendingList").css("display", "");
		$("#accreditationGeneralTeamAcendingList").css("display", "none");
	}

});


$(document).on('click', "#manualaccsortacending_username", function (e) {
	e.preventDefault()
	
	$('.checkedManualUserTeam').prop('checked', false);
	$('.checkedManualUserNameTeam').prop('checked', false);
	$('#select_all_manual_accredation').prop('checked', false);
	$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);

	tempId1 = document.getElementById("accreditationManualTeamAcendingList")
	
	if (tempId1.style.display == ""){
		$("#accreditationManualNameAcendingList").css("display", "");
		$("#accreditationManualTeamAcendingList").css("display", "none");
	}
	else {
		$("#accreditationManualNameAcendingList").css("display", "none");
		$("#accreditationManualTeamAcendingList").css("display", "");
	}

});

$('#select_all_accredation').click(function () {

	tempId1 = document.getElementById("accreditationGeneralNameAcendingList")
	
	if (tempId1.style.display == ""){

		if(this.checked){
			$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
			$('.checkedGeneralUsernameSort').prop('checked', true);
		}else {
			$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
			$('.checkedGeneralUsernameSort').prop('checked', false);
		}

		$('.checkedGeneralTeamSort').prop('checked', false);

	}else {

		if(this.checked){
			$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
			$('.checkedGeneralTeamSort').prop('checked', true);
		}else {
			$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
			$('.checkedGeneralTeamSort').prop('checked', false);
		}

		$('.checkedGeneralUsernameSort').prop('checked', false);
	}
});

$('.checkedGeneralUsernameSort').click(function () {
	if(this.checked){
		$(this).prop('checked', true);
	}else {
		$(this).prop('checked', false);
	}
	
	if($('#accreditationGeneralNameAcendingList input:checked').length == 0) {
		$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	}else {
		$('#select_all_accredation').prop('checked', false);
		$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
	}
});

$('.checkedGeneralTeamSort').click(function () {
	if(this.checked){
		$(this).prop('checked', true);
	}else {
		$(this).prop('checked', false);
	}

	if($('#accreditationGeneralTeamAcendingList input:checked').length == 0) {
		$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	}else {
		$('#select_all_accredation').prop('checked', false);
		$('#general-print-acc').removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
	}
});

$('#general-print-acc').click(function () {

	tempId1 = document.getElementById("accreditationGeneralNameAcendingList")

	if (tempId1.style.display == ""){
		$('#accreditationGeneralNameAcendingForm').submit();
	}else {
		$('#accreditationGeneralTeamAcendingForm').submit();
	}
});


$('#manual-print-acc').click(function () {
	tempId1 = document.getElementById("accreditationManualTeamAcendingList")
	if (tempId1.style.display == ""){
		$('#accreditationManualTeamAcendingListForm').submit();
	}else {
		$('#accreditationManualNameAcendingListForm').submit();
	}
});

$('.genaccessrolevalue').click(function () {
	if ($(this).hasClass('roleeditactive')) {
    $(this).removeClass("roleeditactive")
	 }else {
		$(this).addClass("roleeditactive")
	 }
});

	$("#accupdateroleform").on('submit', (function (e) {	
		e.preventDefault()
	$('#acc_err_msgs').addClass('');
	$("#acc_succ_msgs").text('');
	$('#acc_err_msgs').text('');
	$("#acc_succ_msgs").addClass('');
  var updatemanualid = $('#accmanualupdateid').val()
	isvalidform =true
	var errortext =""
	var ext = getFileExtension($('#imgaccInp').val());
	if($('#accusername').val() == ""){
		isvalidform =false
		errortext ="Name field required " 
	}else if($('#accselectmultiple').val() == ""){
		isvalidform =false
		errortext ="Please select the role(s)" 
		
	}else if(!$('#acc_av_value').hasClass('roleeditactive') && !$('#acc_vip_value').hasClass('roleeditactive') && !$('#acc_ta_value').hasClass('roleeditactive') && !$('#acc_mc_value').hasClass('roleeditactive')){
		isvalidform =false
		errortext ="Please select the access" 
	}else if(ext !=""){
		if(!validateExt(ext)){
			isvalidform =false
			errortext ="Please upload a jpg or jpeg or png image of maximum 10MB, 100x100" 
		}
	}

	if(isvalidform){
		$('#loading-spinner').removeClass('hide');
		$('#acc_even_manualtid').val($('#currenteventId').val())
		selected_access_role = []

		$('.genaccessrolevalue').each(function () {
			if ($(this).hasClass('roleeditactive')) {
				selected_access_role.push(this.value);
			}
		});

		$('#accroledetail').val($('#accselectmultiple').val())	

		$("#accaccessrole").val(selected_access_role)
		var formData = new FormData(this);
		$.ajax({
			url: "/add-update-manual-acc-data",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data == true) {
					if(updatemanualid == 0) {
						$("#acc_succ_msgs").text('Added Successfully');
					}else {
						$("#acc_succ_msgs").text('Updated Successfully');
					}
					
	        $("#acc_succ_msgs").addClass('success');
					setTimeout(function() {
						sessionStorage.setItem('activeaccreditation', "#Manual")
						sessionStorage.setItem('eventactiveTab', '#eventaccreditation')
						window.location.href = "/event-view/" + $('#current_event_slug').val() + "/event-accreditation-manual-view";
					}, FOUR_SEC);
					$('#loading-spinner').addClass('hide');
					
				}
				else {
					$('#loading-spinner').addClass('hide');
					$('#acc_err_msgs').addClass('error');
		      $('#acc_err_msgs').text('error');
		      $("#general-update-model").animate({ scrollTop: 0 }, "slow");
				}
			}
		});
		
	}else {
		$('#loading-spinner').addClass('hide');
		$('#acc_err_msgs').addClass('error');
		$('#acc_err_msgs').text(errortext);
		$("#general-update-model").animate({ scrollTop: 0 }, "slow");
	}
}));

$('#manual-add-model-acc').click(function () {
	
	$('#accusername').val('')
	$('#accselectmultiple').val(0).trigger('change');
	$('#accusername').val('')
	$(".genaccessrolevalue").removeClass('roleeditactive')
	$("#img-acc-upload").attr('src','/static/images/user-default.png')
	$('.checkedManualUserTeam').prop('checked', false);
	$('.checkedManualUserNameTeam').prop('checked', false);
	$('#select_all_manual_accredation').prop('checked', false);
	$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);	
	$('#even_manualtid').val('')
	$('#accaccessrole').val('')
	$('#accroledetail').val('')
	$('#accfilepath').val('')
	$("#imgaccInp").val(null);
	$('#accgenimageUpload').val('')
	$('#accmanualuserid').val('0')
	$('#accmanualupdateid').val('0')
	$('#acc_err_msgs').addClass('');
	$("#acc_succ_msgs").text('');
	$('#acc_err_msgs').text('');
	$("#acc_succ_msgs").addClass('');
	$('#acredationheadertext').text('Add Accreditation')
	$("#general-update-model").modal("show");
});

$('#select_all_manual_accredation').click(function () {

	tempId1 = document.getElementById("accreditationManualTeamAcendingList")
	
	if (tempId1.style.display == ""){

	if(this.checked){
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
		$('.checkedManualUserTeam').prop('checked', true);
	}else {
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
		$('.checkedManualUserTeam').prop('checked', false);
	}
	$('.checkedManualUserNameTeam').prop('checked', false);
}else {

	if(this.checked){
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
		$('.checkedManualUserNameTeam').prop('checked', true);
	}else {
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
		$('.checkedManualUserNameTeam').prop('checked', false);
	}

	$('.checkedManualUserTeam').prop('checked', false);
}

});

$('.checkedManualUserTeam').click(function () {
	if(this.checked){
		$(this).prop('checked', true);
	}else {
		$(this).prop('checked', false);
	}

	if($('#accreditationManualTeamAcendingList input:checked').length == 0) {
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	}else {
		$('#select_all_manual_accredation').prop('checked', false);
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
	}
});

$('.checkedManualUserNameTeam').click(function () {
	if(this.checked){
		$(this).prop('checked', true);
	}else {
		$(this).prop('checked', false);
	}

	if($('#accreditationManualNameAcendingList input:checked').length == 0) {
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'none'}).attr("disabled", true);
	}else {
		$('#select_all_manual_accredation').prop('checked', false);
		$("#manual-print-acc").removeAttr("href").css({'cursor': 'pointer', 'pointer-events' : 'visible'}).attr("disabled", false);
	}
});

});

// stripe payment related script
$(document).ready(function(){
	sessionStorage.removeItem("addcardmissing")
	var stripekey = Stripe(stripeKeyData);
	var elementskeys = stripekey.elements();
	
	// Custom styling can be passed to options when creating an Element.
	var style = {
		base: {
			// Add your base input styles here. For example:
			fontSize: '16px',
			color: "#32325d",
		}
	};
	// Create an instance of the card Element.
	var stripecardinstance = elementskeys.create('card', { style: style });
	
	function eventregisterteamaddcarddetail() {
		stripecardinstance.unmount('#card-element');
		stripecardinstance.mount('#event-card-element');
		stripecardinstance.addEventListener('change', function (event) {
			var displayError = document.getElementById('event-card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});
	

		var form = document.getElementById('registereventpayment');
		form.addEventListener('submit', function (event) {
					$('#loading-spinner').removeClass('hide')
					event.preventDefault();
					stripekey.createToken(stripecardinstance).then(function (result) {
						if (result.error) {
							// Inform the customer that there was an error.
							var errorElement = document.getElementById('event-card-errors');
							errorElement.textContent = result.error.message;
							$('#loading-spinner').addClass('hide')
						} else {
							// Send the token to your server.
								var form = document.getElementById('registereventpayment');
								var hiddenInput = document.createElement('input');
								hiddenInput.setAttribute('type', 'hidden');
								hiddenInput.setAttribute('name', 'stripeToken');
								hiddenInput.setAttribute('id', 'stripeTokenIdValue');
								hiddenInput.setAttribute('value', result.token.id);
								form.appendChild(hiddenInput);
								//$('#loading-spinner').addClass('hide')
								//$('.registerteampayclick').trigger('click')

								stripekey.createToken(stripecardinstance).then(function (result) {
									if (result.error) {
										// Inform the customer that there was an error.
										var errorElement = document.getElementById('event-card-errors');
										errorElement.textContent = result.error.message;
										$('#loading-spinner').addClass('hide')
									} else {
										// Send the token to your server.
											var form = document.getElementById('registereventpayment');
											var hiddenInput = document.createElement('input');
											hiddenInput.setAttribute('type', 'hidden');
											hiddenInput.setAttribute('name', 'stripeToken1');
											hiddenInput.setAttribute('id', 'stripeTokenIdValue1');
											hiddenInput.setAttribute('value', result.token.id);
											form.appendChild(hiddenInput);
											$('#loading-spinner').addClass('hide')
											$('.registerteampayclick').trigger('click')
									}
								});
						}
					});
		});
	}
	
	function subscriptionaddcarddetail() {
				// Add an instance of the card Element into the `card-element` <div>.
				stripecardinstance.unmount('#event-card-element');
				stripecardinstance.mount('#card-element');
				stripecardinstance.addEventListener('change', function (event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});
	
		// Create a token or display an error when the form is submitted.
		var form = document.getElementById('payment-form');
		form.addEventListener('submit', function (event) {
			$('#loading-spinner').removeClass('hide')
			event.preventDefault();
	
			stripekey.createToken(stripecardinstance).then(function (result) {
				if (result.error) {
					// Inform the customer that there was an error.
					var errorElement = document.getElementById('card-errors');
					errorElement.textContent = result.error.message;
					$('#loading-spinner').addClass('hide')
				} else {
					// Send the token to your server.
						stripeTokenHandler(result.token);
				}
			});
		});
	}

	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);
		$('#loading-spinner').removeClass('hide')
	
		$.ajax({
			type: "POST",
			url: "/stripe/createCard",
			data: $("#payment-form").serialize()
		}).done(function(addresultDataValue) {
			if(addresultDataValue != "null" && addresultDataValue){
				submitValueDataValue =JSON.parse(addresultDataValue)
				resultData = submitValueDataValue.status
				if(resultData == "true"){
					$("#setting_succ_msgs").text("Card added successfully")
					$("#addCardDetailModal").animate({ scrollTop: 0 }, "slow");
					if(sessionStorage.getItem("addcardmissing") && submitValueDataValue.cardid !=""){
						sessionStorage.removeItem("addcardmissing")
						$("#subscriptioncardId").val(submitValueDataValue.cardid)
						$('#addCardDetailModal').modal("hide")
						var form = document.getElementById('subscriptionPay');
						var hiddenInput = document.createElement('input');
						hiddenInput.setAttribute('type', 'hidden');
						hiddenInput.setAttribute('name', 'subscriptioncard');
						hiddenInput.setAttribute('value', submitValueDataValue.cardid);
						hiddenInput.setAttribute('id', 'subscriptioncardId');
						form.appendChild(hiddenInput);
						$('#subscriptionPaySubmit').trigger('click');
					}else {
						sessionStorage.setItem('activeTab','#billing')
						setTimeout(function() {
						location.reload()
					}, FOUR_SEC);
					}
					
					$('#loading-spinner').addClass('hide');
				}else if(resultData == "false"){
					$("#setting_err_msgs").text("Card Not Added Successfully.")
					setTimeout(function() {
						location.reload()
					}, FOUR_SEC);
					$('#loading-spinner').addClass('hide');
				}else {
					if(resultData != "null"){
					err_Event_Data =JSON.parse(resultData)
					$("#setting_err_msgs").text(err_Event_Data.message)
					setTimeout(function() {
						location.reload()
					}, FOUR_SEC);
					$('#loading-spinner').addClass('hide');
				}
			}
			} 
		});
	}

	$(document).on('click', "#defaultPaymentCardOption", function (e) {
		e.preventDefault();
		$('#userpaymentconfirmationmodel').modal('hide');
		teamEventPaymentOption($(this).attr("value"), "default");
		$('#defautcardeventpay').removeClass('hide')
		$('#newcardeventpay').addClass('hide')
		$('.newcardpaymentdataonoff').addClass('hide')
	});

	$(document).on('click', "#newPaymentCardOption", function (e) {
		e.preventDefault();
		$('#userpaymentconfirmationmodel').modal('hide');
		$('.newcardpaymentdataonoff').removeClass('hide')
		teamEventPaymentOption($(this).attr("value"),"newcard");
		$('#defautcardeventpay').addClass('hide')
		$('#newcardeventpay').removeClass('hide')
	});

	$(document).on('click', "#addBillingCard", function (e) {
		e.preventDefault()
		subscriptionaddcarddetail()
		$("#setting_succ_msgs").text("");
		$("#setting_err_msgs").text("");
		$(".InputElement").val("");
		$('#addCardDetailModal').modal("show")
	});

	$(document).on('click', "#free_subscription_Btn", function (e) {
		e.preventDefault()
		subscriptionaddcarddetail()
		$("#setting_succ_msgs").text("");
		$("#setting_err_msgs").text("");
		$('#addCardDetailModal').modal("show")
	});

	$('#subscriptionPaySubmit').click(function (e) {
		e.preventDefault()
		$('#subscription_err_msg').text("")
		$('#subscriptioncardText').text("")

		if (!removeSmsStatus) {
			if (!$('input:radio[name=subscriptionplan]').is(':checked')) {
				$('#subscription_err_msg').text("Please Select a Plan")
				return
			}
		} else if (removeSmsStatus) {
			if (!$("input:radio[name='smssubscriptionplan']").is(":checked")) {
				$('#subscription_err_msg').text("Please Select the SMS Plan ")
				return
			}
		}
		if ($("#subscriptioncardId").length) {
			var tempCardCount = $("#subscription_Card_Length").val()
			if ($("#subscriptioncardId").val().trim() == "" && tempCardCount > 0) {
				sessionStorage.setItem("changecardmissing", "true")
				$('#subscriptDefaultCardModal').modal('show')
				return
			}
		}
		else {
			subscriptionaddcarddetail()
			sessionStorage.setItem("addcardmissing", "true")
			$('#addCardDetailModal').modal('show')
			return
		}
		subscriptionAddCode()
	});

	$(document).on('click', "#teamEventPaymentOptionConformation", function (e) {
		e.preventDefault()
		regPayId = $(this).attr("value")
		$('#defaultPaymentCardOption').val(regPayId)
		$('#newPaymentCardOption').val(regPayId)
		if(stripeSourceCardStatus == false) {
			$('#userpaymentconfirmationmodel').modal('hide');
		$('.newcardpaymentdataonoff').removeClass('hide')
		teamEventPaymentOption($(this).attr("value"),"newcard");
		$('#defautcardeventpay').addClass('hide')
		$('#newcardeventpay').removeClass('hide')
		}else {
			$('#userpaymentconfirmationmodel').modal('show');
		}
		$('#currenctRegPaymentId').val(regPayId)
	});

	if(readyToPaymentStatus) {
		regPayId = eventRegisterPaymentId
		$('#defaultPaymentCardOption').val(regPayId)
		$('#newPaymentCardOption').val(regPayId)
		if(stripeSourceCardStatus == false) {
		$('#userpaymentconfirmationmodel').modal('hide');
		$('.newcardpaymentdataonoff').removeClass('hide')
		teamEventPaymentOption(regPayId,"newcard");
		$('#defautcardeventpay').addClass('hide')
		$('#newcardeventpay').removeClass('hide')
		}else {
			$('#userpaymentconfirmationmodel').modal('show');
		}
		$('#currenctRegPaymentId').val(regPayId)
	}
  var remaamountwithformat = 0
	$('.registerteampayclick').click(function (e) {

		e.preventDefault()
		$("#event_payment_err").text("")
		$("#event_payment_succ").text("")

		$("#event_payment_err").addClass("hide")
		$("#event_payment_succ").addClass("hide")

		$('#currentregistratioStep').val("")
		//$('#tempeventregisterteamid').val("0")
	  localStorage.setItem("str_from", "teampayment")		
		$('#pay_amount').removeClass("empty-err")
		var pay_amount = $('#pay_amount').val()

		var numberDatatype =remaamountwithformat
		
		var numberpay_amount =Number(pay_amount)

		if(pay_amount.trim() != "" && numberpay_amount <= numberDatatype && numberpay_amount >0){
		
			$('#loading-spinner').removeClass('hide');
			$.ajax({
				type: "POST",
				url: "/stripe/event_registrationpayment",
				data: $("#registereventpayment").serialize()
			}).done(function (msg) {
				tempmsgdata = JSON.stringify(msg)
			if (tempmsgdata != "null") {
				tempEventRegisterDetails = JSON.parse(tempmsgdata)
				msg = tempEventRegisterDetails.status
				redirecturl = tempEventRegisterDetails.redirecturl
				if (msg == "true") {
					$("#event_payment_succ").text("Payment Success")
					$("#event_payment_succ").removeClass("hide")
					$("#registereventteampaymentpopup").animate({ scrollTop: 0 }, "slow");
					if(redirecturl != "") {
						setTimeout(function () {
							$("#registereventteampaymentpopup").modal("hide")
							window.location.href = redirecturl
						}, 300);
					}else {
						setTimeout(function () {
							$("#registereventteampaymentpopup").modal("hide")
							location.reload()
						}, FIVE_SEC);
					}
					
				}else if(msg == "false"){
					$("#event_payment_err").text("Payment failed.")
					$("#event_payment_err").removeClass("hide")
				}else {
					$("#event_payment_err").text(msg)
					$("#event_payment_err").removeClass("hide")
				}
			}
				$('#loading-spinner').addClass('hide');
			});
		}else {
			$("#event_payment_err").text("Please enter a valid amount and not exceeding amount owed")
			$("#event_payment_err").removeClass("hide")
			$('#pay_amount').addClass("empty-err")
			$("#registereventteampaymentpopup").animate({ scrollTop: 0 }, "slow");
		}
	});

	function subscriptionAddCode() {
		$('#subscriptionskipstatus').val(false)
		$('#loading-spinner').removeClass('hide')
		$.ajax({
			type: "POST",
			url: "/stripe/add-team-subscription",
			data: $("#subscriptionPay").serialize()
		}).done(function (submitValueData) {
			if (submitValueData != "null") {
				submitValueDataValue = JSON.parse(submitValueData)
				if (submitValueDataValue.redirecturl != "") {
					$('#defaultsubscriptionmodelheading').modal("show")
					$('#defaulteventmodelwithheadingText').text("Premium Plan Upgrade - Success")
					if (submitValueDataValue.status == "Payment Succeed") {
						$('#defaulteventmodelwithheading_Body').html("<p>Congrats, you have successfully subscribed to the Team Premium Plan. We want you to take advantage of Gushou's advanced features and are here to help if needed.  If you have any quesitons email dragonboat@gogushou.com or check out Gushou's <a href='/pricing#faq' target='_blank'>FAQ</a> and/or <a href='https://gushou.freshdesk.com/support/home' target='_blank'>tutorial</a> videos.</p>")
					} else {
						$('#defaulteventmodelwithheading_Body').text(submitValueDataValue.status)
					}
					$('#loading-spinner').addClass('hide')
					setTimeout(function () {
						$("#defaultsubscriptionmodelheading").modal('hide')
						location.replace(submitValueDataValue.redirecturl)
						if (submitValueDataValue.redirecturl == "/profile-setting") {
							sessionStorage.setItem('activeTab', '#billing')
						}
					}, EIGHT_SEC);
				} else {
					$('#subscription_err_msg').text(submitValueDataValue.status)
					$('#loading-spinner').addClass('hide')
				}
			}
		});
	}
  function teamEventPaymentOption(regPayId, requesttype) {
		$("#event_payment_succ").text("")
		$("#event_payment_err").text("")
		$("#event-card-errors").text("")
		
		$("#event_payment_err").addClass("hide")
		$("#event_payment_succ").addClass("hide")
		$('#pay_amount').removeClass("empty-err")
		localStorage.setItem("str_from", "teampayment")
	
		 $.ajax({
			type: "GET",
			url: "/stripe/GetTeamPaymentDetail",
			data: { "regPayId": regPayId }
		}).done(function (data) {
			tempdata = JSON.stringify(data)
			if (tempdata != "null") {
				teamPaymentData = JSON.parse(tempdata)
				temptext =teamPaymentData.team_name+" owes"+" " + teamPaymentData.currencySymbol+" "+ teamPaymentData.remainingAmount +" on this fees."
				$('#paymentreplaceheading').html(temptext)
				$('#paymentreplacecurrency').text(teamPaymentData.currencySymbol)
				$('#eventregistercurrentEventRegisterTeamId').val(teamPaymentData.teamid)
				$('#eventregistercurrentEventSlug').val(teamPaymentData.eventslug)
				$('#eventregistercurrentEventRegisterId').val(teamPaymentData.eventregisterId)
				remaamountwithformat = Number(teamPaymentData.remaamountwithformat)
	
				$('#teampaymnetinvoice').attr("href",'/stripe/downloadorderinvoice/'+regPayId)
				$('#registereventteampaymentpopup').modal("show")
				$('#pay_amount').val("")
				$('#team_event_transaction_note').val("")
				$('#newcardpaymentnameid').val("")
				$("#stripeTokenIdValue").remove();
				$("#stripeTokenIdValue1").remove();
				if(requesttype == "newcard"){
					eventregisterteamaddcarddetail()
				}
			}
		});
	}
});
// End

function generateclass_Division_distance() {
	newhtml = ""

	checkduplicateclass_Division = []

	for (var i = 0; i < createeventdiv_calss_dis_boattypeVals.length; i++) {
		teampsliptData = createeventdiv_calss_dis_boattypeVals[i].split('_')
		tempyyy = teampsliptData[2] + "_" + teampsliptData[1] + "_" + teampsliptData[3]

		if (checkduplicateclass_Division.indexOf(tempyyy) == -1) {
			checkduplicateclass_Division.push(tempyyy)
			tempclass = ""
			if (teampsliptData[2] == "M") {
				tempclass = "Men"
			} else if (teampsliptData[2] == "W") {
				tempclass = "Women"
			} else if (teampsliptData[2] == "MX") {
				tempclass = "Mixed"
			}

			newhtml += '<tr>'
			newhtml += '<td class="label-bold"  colspan="4">' + tempclass + '</td>'
			newhtml += '<input type="hidden" data-index= ' + i + ' id="' + tempyyy + '" name="seb_eb_r_price[]" value="' + tempyyy + '"/>'
			newhtml += '</tr>'
			newhtml += '<tr class="checkemptyprice">'
			newhtml += "<td>" + teampsliptData[0] + ' ' + teampsliptData[3] + " person</td>"
			newhtml += '<td><input id=' + i + 'seb_price data-index= ' + i + ' type="Number" class="form-control event-quality-size" placeholder="Price" name="seb_price[]" value="" autocomplete="off"></td>'
			newhtml += '<td><input  id=' + i + 'eb_price data-index= ' + i + ' type="Number" class="form-control event-quality-size" placeholder="Price" name="eb_price[]" value="" autocomplete="off"></td>'
			newhtml += '<td style="padding-right:0px !important"><input  id=' + i + 'r_price data-index= ' + i + ' type="Number" class="form-control event-quality-size" placeholder="Price" name="r_price[]" value="" autocomplete="off"></td>'
			newhtml += '</tr>'
		}
	}

	$("#appendPricingBasedPricingDetail").empty().append(newhtml);

}

function generatePerpersonPriceBased() {

	newhtml = ""
	newhtml += '<tr class="checkemptyprice">'
	newhtml += "<td> <span style='color: #2a745f;'>Per Person</span>(Competitors, Team Leaders, Coaches) </td>"
	newhtml += '<td><input  id=1seb_price data-index=1 type="Number" class="form-control event-quality-size" placeholder="Price" name="seb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td><input  id=1eb_price data-index=1 type="Number" class="form-control event-quality-size" placeholder="Price" name="eb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td style="padding-right:0px" ><input  id=1r_price data-index=1 type="Number" class="form-control event-quality-size" placeholder="Price" name="r_price[]" value="" autocomplete="off"></td>'
	newhtml += '</tr>'

	newhtml += '<tr class="checkemptyprice">'
	newhtml += "<td> <span style='color: #2a745f;'>Per Person</span>(NF Presidents, Secretary Generals, Officials)</td>"
	newhtml += '<td><input  id=2seb_price data-index=2 type="Number" class="form-control event-quality-size" placeholder="Price" name="seb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td><input  id=2eb_price data-index=2 type="Number" class="form-control event-quality-size" placeholder="Price" name="eb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td style="padding-right:0px" ><input  id=2r_price data-index=2 type="Number" class="form-control event-quality-size" placeholder="Price" name="r_price[]" value="" autocomplete="off"></td>'
	newhtml += '</tr>'

	newhtml += '<tr class="checkemptyprice">'
	newhtml += "<td> <span style='color: #2a745f;'>Per Person</span>(Other, including Media)</td>"
	newhtml += '<td><input  id=3seb_price data-index=3 type="Number" class="form-control event-quality-size" placeholder="Price" name="seb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td><input  id=3eb_price data-index=3 type="Number" class="form-control event-quality-size" placeholder="Price" name="eb_price[]" value="" autocomplete="off"></td>'
	newhtml += '<td style="padding-right:0px" ><input  id=3r_price data-index=3 type="Number" class="form-control event-quality-size" placeholder="Price" name="r_price[]" value="" autocomplete="off"></td>'
	newhtml += '</tr>'

	$("#appendPricingBasedPricingDetail").empty().append(newhtml);
}

function create_Event_class_Division_distance(tempall_divisions, tempall_boattype, tempall_boatmeter) {
	tempall_boatmeterlength = tempall_boatmeter.length
	temp_200 = false
	temp_500 = false
	temp_1000 = false
	temp_2000 = false

	if (tempall_boatmeter.indexOf("200") != -1) {
		temp_200 = true
	}

	if (tempall_boatmeter.indexOf("500") != -1) {
		temp_500 = true
	}

	if (tempall_boatmeter.indexOf("1000") != -1) {
		temp_1000 = true
	}

	if (tempall_boatmeter.indexOf("2000") != -1) {
		temp_2000 = true
	}

	tempSizecolspan = 6 + tempall_boatmeterlength * 3

	newhtml = ""
	newhtml += '<table  class="table table-bordered table-dark ">'
	newhtml += '<thead>'
	newhtml += '<tr class="thead-back-color">'
	newhtml += '<td colspan="6"></td>'
	newhtml += '<th colspan=' + tempall_boatmeterlength + ' scope="">Men</th>'
	newhtml += '<th colspan=' + tempall_boatmeterlength + ' scope="">Women</th>'
	newhtml += '<th colspan=' + tempall_boatmeterlength + ' scope="">Mixed</th>'
	newhtml += '</tr>'
	newhtml += '<tr class="thead-back-color">'
	newhtml += '<td colspan="6"></td>'
	if (temp_200) {
		newhtml += '<th scope="col">200m</th>'
	}

	if (temp_500) {
		newhtml += '<th scope="col">500m</th>'
	}

	if (temp_1000) {
		newhtml += '<th scope="col">1km</th>'
	}

	if (temp_2000) {
		newhtml += '<th scope="col">2km</th>'
	}

	if (temp_200) {
		newhtml += '<th scope="col">200m</th>'
	}

	if (temp_500) {
		newhtml += '<th scope="col">500m</th>'
	}

	if (temp_1000) {
		newhtml += '<th scope="col">1km</th>'
	}

	if (temp_2000) {
		newhtml += '<th scope="col">2km</th>'
	}

	if (temp_200) {
		newhtml += '<th scope="col">200m</th>'
	}

	if (temp_500) {
		newhtml += '<th scope="col">500m</th>'
	}

	if (temp_1000) {
		newhtml += '<th scope="col">1km</th>'
	}

	if (temp_2000) {
		newhtml += '<th scope="col">2km</th>'
	}

	newhtml += '</tr>'
	newhtml += '</thead>'
	newhtml += '<tbody>'
	for (var i = 0; i < tempall_divisions.length; i++) {
		tempDivisionName = $('#All_divisions' + tempall_divisions[i]).attr('data-divname')
		newhtml += '<tr class="table-tr-display">'
		newhtml += '<tr>'
		newhtml += '<td class="td-font-size" colspan=' + tempSizecolspan + '>' + tempDivisionName + '</td>'
		newhtml += '</tr>'

		for (var j = 0; j < tempall_boattype.length; j++) {
			newhtml += '<tr class="allboattypeselectedornot">'
			newhtml += '<td class="td-font-size-child" colspan="6" >' + tempall_boattype[j] + ' person</td>'

			if (temp_200) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_M_" + tempall_boattype[j] + "_200' class='md-check all-divisions' name='all_div_cls_botty_meter[]' id='M_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='M_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span class='check' style='left: unset !important;' ></span><span class='box' style='left: unset !important;'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_500) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_M_" + tempall_boattype[j] + "_500' class='md-check all-divisions' name='all_div_cls_botty_meter[]' id='M_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='M_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_1000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_M_" + tempall_boattype[j] + "_1000' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='M_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='M_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_2000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_M_" + tempall_boattype[j] + "_2000' class='md-check all-divisions' name='all_div_cls_botty_meter[]' id='M_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='M_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_200) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_W_" + tempall_boattype[j] + "_200' class='md-check all-divisions' name='all_div_cls_botty_meter[]' id='W_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='W_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_500) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_W_" + tempall_boattype[j] + "_500' class='md-check all-divisions' name='all_div_cls_botty_meter[]' id='W_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='W_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_1000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_W_" + tempall_boattype[j] + "_1000' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='W_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='W_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_2000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_W_" + tempall_boattype[j] + "_2000' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='W_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='W_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_200) {
				newhtml += '<td align="center" >'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_MX_" + tempall_boattype[j] + "_200' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='MX_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='MX_200_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_500) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_MX_" + tempall_boattype[j] + "_500' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='MX_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='MX_500_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_1000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_MX_" + tempall_boattype[j] + "_1000' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='MX_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='MX_1000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}

			if (temp_2000) {
				newhtml += '<td align="center">'
				newhtml += '<div class="md-checkbox">'
				newhtml += "<input value='" + tempDivisionName + "_" + tempall_divisions[i] + "_MX_" + tempall_boattype[j] + "_2000' class='md-check all-divisions'  name='all_div_cls_botty_meter[]' id='MX_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "' type='checkbox'>"
				newhtml += "<label style='padding-left: unset !important;' for='MX_2000_" + tempall_divisions[i] + "_" + tempall_boattype[j] + "'><span style='left: unset !important;' class='check'></span><span style='left: unset !important;' class='box'></span>  </label>"
				newhtml += '</div>'
				newhtml += '</td>'
			}
			newhtml += '</tr>'
		}

		newhtml += '</tr>'
	}
	newhtml += '</tbody>'
	newhtml += '</table>'

	$("#appendClassDivisionMaxHideShowTable").removeClass('hide')
	$("#createboxsectionfooterhideshow").removeClass('hide')
	$("#ClassDivisionMaxHideShowContinue").addClass('hide')
	$("#appendClassDivisionMaxTable").empty().append(newhtml);
}


$(document).on('click', '.getEventRegIdForTeam', function (eve) {
	eve.preventDefault()
	var event_reg_id= $(this).attr("value")
	tempId1 = document.getElementsByClassName("rosterracingTableData1_"+event_reg_id)
	if(tempId1[0].style.display == "none"){
		$("#table_team_member_rosterracing1").addClass("inlinedisplay")
		console.log("hi")
		$(".rosterracingTableData1_"+event_reg_id).toggle();
		console.log("display",event_reg_id)	
		getTeamMemberDetails(event_reg_id)
	}
	else {
		console.log("bye")
		$("#table_team_member_rosterracing1").removeClass("inlinedisplay")
		$(".rosterracingTableData1_"+event_reg_id).toggle();	
		
	} 
});

$(document).on('click', '#show_teamsmember_rosterracing2', function (eve) {
	eve.stopPropagation()
	console.log("hh")
	tempId1 = document.getElementsByClassName("rosterracingTableData2")
	if(tempId1[0].style.display == "none"){
		$(".rosterracingTableData2").toggle();
		$(".rosterracingIndividualTeamList2").css('display','none')		
	}
	else {
		$(".rosterracingTableData2").toggle();	
		$(".rosterracingIndividualTeamList2").css('display','none')			
	} 
});
$(document).on('click', '#sendWaiverFromEventBtn', function (eve) {
	eve.preventDefault();
	console.log("wai eve")
	$('#loading-spinner').removeClass('hide');
	var waiver_members_event = $("#waiver_members_event").val()
	$('#sendWaiverFromEventBtn').removeClass('hide')
	$('#waiver_send_content').text("The selected paddler(s) will receive an email request to sign their waiver.")
	console.log(waiver_members_event)
	if (waiver_members_event.length > 0) {
		$.ajax({
			type: "POST",
			url: "/send-event-waiver",
			data: { "waiver_members_event": waiver_members_event }
		}).done(function (msg) {
			if(msg=="true"){
				setTimeout(function () {
					$('#loading-spinner').addClass('hide');
					$("#sendEventWaiverModal").modal("hide");
					$('#waiver_sent_succ').addClass('success');
					$('#waiver_sent_succ').text("Waiver sent successfully")
					
					location.reload();
				}, TWO_SEC)
			}else if(msg=="add waiver"){
				$('#loading-spinner').addClass('hide');
				$('#waiver_sent_err').addClass('error');
				$('#waiver_sent_err').text("Please add waiver to this event to send waiver")
				$('#sendWaiverFromEventBtn').addClass('hide')
				$('#waiver_send_content').text("")
				
			}else{
				setTimeout(function () {
					$('#loading-spinner').addClass('hide');
					$("#sendEventWaiverModal").modal("hide");
					location.reload();
				}, TWO_SEC)
			}

		
		});
	}
	else {
		$('#loading-spinner').addClass('hide');
	}
});

$(document).on('click', '#event_waiver_remainder', function (eve) {
	eve.preventDefault()
	console.log("hhwaiver")
	$("#waiver_sent_err").removeClass('error')
	$("#waiver_sent_err").text("")
	$("#waiver_sent_succ").removeClass('success')
	$("#waiver_sent_succ").text("")
	$("#waiver_reminder_succ").removeClass('success')
	$("#waiver_reminder_succ").text("")

	selected_waiver_members = []
		$('#waiver_sent_succ, #waiver_reminder_succ').removeClass('success');
		$('#waiver_sent_succ, #waiver_reminder_succ').text("");
		var event_reg_id = $("#getEventRegIdForTeam").val()
		
		
		$('input:checkbox.check_teams_waivers').each(function () {
			if (this.checked) {
				selected_waiver_members.push(this.value);
			}
		});

		console.log(selected_waiver_members)
		if (selected_waiver_members.length > 0)
		$("#sendEventWaiverModal").modal("show");
		$("#waiver_members_event").val(selected_waiver_members)
	
});



$(document).on('click', '#show_teamsmember_waivers', function (eve) {
	eve.stopPropagation()
	console.log("hh")
	tempId1 = document.getElementsByClassName("rosterracingTableData")
	if(tempId1[0].style.display == "none"){
		$(".rosterracingTableData").toggle();
		$(".rosterracingIndividualTeamList").css('display','none')		
	}
	else {
		$(".rosterracingTableData").toggle();	
		$(".rosterracingIndividualTeamList").css('display','none')			
	} 
});

getTeamMemberDetails = function (event_reg_id) {

	$('#loading-spinner').removeClass('hide');
	$.ajax({
		type: "GET",
		url: "/get_team_member_waiver_detail",
		data: { "event_reg_id": event_reg_id }
	}).done(function (waiverData) {
		var tempWaiverData = JSON.stringify(waiverData)
		var tempWaiverJsonData = JSON.parse(tempWaiverData)
		var team_check = $("#check_teams_waiver_"+event_reg_id).prop("checked")
		var eventRegIdUpload = $("#waiver_event_reg_id_for_upload").val(event_reg_id)
		console.log("team_check-",team_check)
		console.log(tempWaiverJsonData)
		var waiver_sumitted_status = $("#waiver_sumbitted_team_"+event_reg_id).val()
		var html = '';
		waiverCoCaptains = $("#getCoCaptainId_"+event_reg_id).val()
		var coCaptainData = waiverCoCaptains.split(',');
		waiverTeamCaptainId = $("#getCaptainId_"+event_reg_id).val()
		html +="<td> <table id='event_team_members_payment' class='table rosterracing-table'style='width:95%'>"+
		"<thead class='text_style'>"+
						  "<tr>"+
						  "<th style='width:0px'>"+
			// 			  "<div class='md-checkbox custom-checkbox'>"+
			// 	"<input id='check_all_tem_mem_"+event_reg_id+"'class='md-check check_all_tem_mem' type='checkbox'>"+
			//    " <label for='all' class='marginZero'>"+
			// 	   "<span class='check'></span>"+
			// 	   "<span class='box'></span>"+
			// 	" </label>"+
			//    "</div>
							"</th>"+
							 
							  "<th class='lowercase mnu_padding1' style='text-align: center;width: 200px;'>Name"+

							 " </th>"+
							 " <th class='lowercase' style='padding-left: 35px !important;'>Last"+
							  "  <br> Requested</th>"+
							 " <th class='lowercase mnu_padding'>Accepted Online</th>"+
							 " <th class='lowercase mnu_padding' style='text-align: center;'>Contact</th>"+
							 " <th class='lowercase' style='width:120px'>Medical"+
							  "  <br> Condition</th>"+
							 " <th class='lowercase'>Hard Copy"+
							   " <br>"+
							   " <span class='color'>(Under 18)</span>"+
							"  </th>"+
							 " <th class='lowercase'>Waiver Status (Individuals)</th>"+
							"</tr>"+
						  "</thead><tbody>";
		for(var i=0; i<tempWaiverJsonData.length-1;i++){

			console.log('index: ' + i + ', name: ' + tempWaiverJsonData[i].username);
			if (tempWaiverJsonData[i].formatted_waiver_signed_date) {
				html += "<tr class='grayout'>"
			}else if (tempWaiverJsonData[i].signed_upload_date) {
				html += "<tr class='grayout'>"
			}else{
				html += "<tr>"
			}
			html += "<td>"
			
			 if (tempWaiverJsonData[i].formatted_waiver_signed_date) {
				html += "<div></div></td>"
			}else if (tempWaiverJsonData[i].signed_upload_date) {
				html += "<div></div></td>"
			}else{
				console.log(waiver_sumitted_status)
				if (waiver_sumitted_status == "not submitted"){
					html += "<div  data-toggle='tooltip' title='Waiver not added for team' class='md-checkbox class-tooltip_pay hide' style='top:-10px'>"+
					" <input disabled id='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='md-check check_teams_individual_waivers check_teams_waivers tm_"+event_reg_id+"' value='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"_"+event_reg_id+"' type='checkbox'>"+
				   " <label for='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='marginZero'>"+
					   "<span class='check'></span>"+
					   "<span class='box'></span>"+
					" </label>"+
				   "</div></td>"
				}
				 else if(team_check){
					html += "<div class='md-checkbox' style='top:-10px'>"+
					" <input id='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='md-check check_teams_individual_waivers check_teams_waivers tm_"+event_reg_id+"' value='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"_"+event_reg_id+"' type='checkbox' checked>"+
				   " <label for='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='marginZero'>"+
					   "<span class='check'></span>"+
					   "<span class='box'></span>"+
					" </label>"+
				   "</div></td>"
				}else{
					html += "<div class='md-checkbox' style='top:-10px'>"+
					" <input id='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='md-check check_teams_individual_waivers check_teams_waivers tm_"+event_reg_id+"' value='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"_"+event_reg_id+"' type='checkbox'>"+
				   " <label for='tm_"+tempWaiverJsonData[i].fk_event_id+"_"+tempWaiverJsonData[i].fk_team_id+"_"+tempWaiverJsonData[i].fk_member_id+"' class='marginZero'>"+
					   "<span class='check'></span>"+
					   "<span class='box'></span>"+
					" </label>"+
				   "</div></td>"
				}
			
			}
				
				
				html +="<td><div class='name-section'><div class='name'>"+
				 " <div class='name-icon'>"
				 if (tempWaiverJsonData[i].files){
					html +=  "<img src='"+tempWaiverJsonData[i].files+"' class='img-circle event_waiver_team_mem_icon'>"
				 }else{
					html +=  "<img src='/static/images/user-default.png' class='img-circle event_waiver_team_mem_icon'>"
				 }
				html +=  "</div>"

				 if (tempWaiverJsonData[i].isUnder_18 == "true"){
						html += "<div class='info'>"+
						"<p class='color-orange' style='font-weight: 600;font-size: 14px;width: 150px !important;word-wrap: break-word;;margin:0px'>"+tempWaiverJsonData[i].username+"</p>"+
						"<p class='color-orange' style='font-size: 13px;margin:0px'>"+tempWaiverJsonData[i].user_class+","+tempWaiverJsonData[i].user_age
						
						var index = jQuery.inArray(tempWaiverJsonData[i].fk_member_id, coCaptainData)
						if (index != -1) {
							if (tempWaiverJsonData[i].fk_member_id == waiverTeamCaptainId) {
								html += "<span class='label-orange label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";
							} else {
								html += "<span class='label-gray label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";
							}
						} else {
							console.log("nnnn",waiverTeamCaptainId,coCaptainData)
							if(waiverTeamCaptainId === coCaptainData ){
								html += "<span class='label-orange label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";

							}else{
								html += "</div></div>";
							}
							
						}						
						html +="</span>"+
						
					  "</div>"
				 }else{
					html += "<div class='info'>"+
					"<p style='font-weight: 600;color: #2a7560;font-size: 14px;margin:0px;width: 150px !important;word-wrap: break-word;'>"+tempWaiverJsonData[i].username+"</p>"+
					"<p class='para_grey_color' style='font-size: 13px;margin:0px'>"+tempWaiverJsonData[i].user_class+","+tempWaiverJsonData[i].user_age
					console.log("mem_id",tempWaiverJsonData[i].fk_member_id)
					console.log("coCaptainData",coCaptainData)
					var index = jQuery.inArray(tempWaiverJsonData[i].fk_member_id, coCaptainData)
						if (index != -1) {
							if (tempWaiverJsonData[i].fk_member_id == waiverTeamCaptainId) {
								html += "<span class='label-orange label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";
							} else {
								html += "<span class='label-gray label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";
							}
						} else {
							console.log("nnnn",waiverTeamCaptainId,coCaptainData)
							if(waiverTeamCaptainId === coCaptainData ){
								html += "<span class='label-orange label-orange-overwrite waiver_capt_co_cap'>Captain</span></p>";

							}else{
								html += "</div></div>";
							}
						}
					
						html +="</span>"+
				  "</div>"
				 }
				 
				 html +="</div>"+
			  "</div>"+
			"</td>"+
			"<td>"+
			  "<div class='last_request'>"
			  console.log("tempWaiverJsonData[i].medical_condition_desc--",tempWaiverJsonData[i].medical_condition_desc)
			  if(tempWaiverJsonData[i].formatted_waiver_send_date === undefined){
			
				html +="<a class='para_grey_color'></a>"
				 
			  }else{
				
				if (tempWaiverJsonData[i].isUnder_18 == "true"){
					html +="<a class='color-orange font14' style='padding-left: 30px;'>"+tempWaiverJsonData[i].formatted_waiver_send_date+"</a>"
				  }else{
					html +="<a class='para_grey_color font14' style='padding-left: 30px;'>"+tempWaiverJsonData[i].formatted_waiver_send_date+"</a>"
				  }

			  }
			 
				html += "</div>"+
			"</td>"+
			"<td style='text-align: center;'>"+
			  "<div class='rosterracing-table-th'>ROSTER NAME</div>"+
			 " <div class=''>"
			 if(tempWaiverJsonData[i].formatted_waiver_signed_date === undefined){
			
				html +="<a class='para_grey_color'></a>"
				 
			  }else{
			 if (tempWaiverJsonData[i].isUnder_18 == "true"){
				html +="<a style='text-align: center;' class='color-orange font14'>"+tempWaiverJsonData[i].formatted_waiver_signed_date+"</a>"
			 }else{
				html +="<a style='text-align: center;' class='para_grey_color font14'>"+tempWaiverJsonData[i].formatted_waiver_signed_date+"</a>"
			 }
			}
			 html += "</div>"+
			"</td>"+
			"<td style='text-align: center;'>"
			 "<div class='rosterracing-table-th'>CAPTAIN NAME</div>"+
			  "<div class=''>"
			  if(tempWaiverJsonData[i].contact_name === undefined){
			
				html +="<a class='para_grey_color'></a>"
				 
			  }else{
				if (tempWaiverJsonData[i].isUnder_18 == "true"){
					html +="<a style='text-align: center;' class='color-orange font14'>"+tempWaiverJsonData[i].contact_name+"<br>"+tempWaiverJsonData[i].contact_number+"</a>"
				}else{
					html +="<a style='text-align: center;' class='para_grey_color font14'>"+tempWaiverJsonData[i].contact_name+"<br>"+tempWaiverJsonData[i].contact_number+"</a>"

				}
			  }
				
			  html += "</div>"+
			"</td>"+
			"<td>"+
			  "<div>"
			  if(tempWaiverJsonData[i].medical_condition_desc === ''||tempWaiverJsonData[i].medical_condition_desc === undefined){
			
				html +="<a class='para_grey_color'></a>"
				 
			  }else{
				html +="<div class='dasboard-team-member-content text-align-left' style='text-align: center;'>"+
				" <div class='note-tooltip icon_ti-notepad' style='font-size: 10px;'>"+
				   "<i class='ti-notepad' style='text-align: center; margin-left:5px'></i>"+
				   "<div class='class-tooltip waiver-medicalcondition'>"+tempWaiverJsonData[i].medical_condition_desc

			  }
				console.log("**",tempWaiverJsonData[i].formatted_waiver_signed_date,tempWaiverJsonData[i].signed_upload_waiver_path,tempWaiverJsonData[i].signed_upload_date)
				console.log("**",tempWaiverJsonData[i].fk_team_waiver_id,tempWaiverJsonData[i].fk_team_id,tempWaiverJsonData[i].fk_event_id,tempWaiverJsonData[i].fk_member_id)
				html +="</div>"+
				 "</div>"+

				"</div>"+
				"</span>"+
			  "</div>"+
			"</td>"

			if(tempWaiverJsonData[i].formatted_waiver_signed_date){
				html += "<td></td>"
			}
			else if (tempWaiverJsonData[i].signed_upload_date){
				html +="<td style='text-align: center;'>"+"<a value='/view-uploaded-signed-team-waiver/"+tempWaiverJsonData[i].fk_member_id+"/"+tempWaiverJsonData[i].fk_team_waiver_id+"/"+tempWaiverJsonData[i].fk_team_id+"/"+tempWaiverJsonData[i].fk_event_id+"' name='waivers-team-paris' href='#/' target='' class='btn btn-thin paybutton view_uploaded_waiver_event' style='min-width: 20px;font-size: 13px;'>View Waiver</a>"+
						"<p style='margin: 0px;font-size: 13px;'>"+tempWaiverJsonData[i].signed_upload_date+"</p>"+"</td>"
						"</td>"
			}else{
				if(tempWaiverJsonData[i].formatted_waiver_send_date){
					html += "<td style='text-align: center;'>"+
				
					"<div class='upload uploadWaiverForTeamMembers' style='padding: 5px;text-align: center;' value='"+tempWaiverJsonData[i].fk_member_id+","+tempWaiverJsonData[i].fk_team_waiver_id+","+tempWaiverJsonData[i].fk_team_id+","+tempWaiverJsonData[i].fk_event_id+","+event_reg_id+"'>"+
					
					  "<img src='/static/images/upload.png' width='19px' height='18px'>"+
					  "<a class='upload_gray'>Upload </a>"+
					"</div>"+
				  "</td>"
				}else{
					html += "<td></td>"
				}
				
			}
			
			if(tempWaiverJsonData[i].formatted_waiver_send_date){
				if(tempWaiverJsonData[i].formatted_waiver_signed_date){
						html +="<td><div data-toggle='tooltip' title= 'Online Signed' class='class-tooltip_pay md-checkbox text-ailgn-center'>"+
						"<img src='/static/images/waiver_tick.png' class=''>"+
							"</div>"+
							"</td>"+
						"</tr>"

				}else if(tempWaiverJsonData[i].signed_upload_date) {
					html +="<td><div data-toggle='tooltip' title= 'Manually Signed' class='class-tooltip_pay md-checkbox text-ailgn-center'>"+
					"<img src='/static/images/waiver_tick.png' class=''>"+
						"</div>"+
						"</td>"+
					"</tr>"
				}
				
				else{
					
					html +="<td></td></tr>"
				}
				
			}else{
				html +="<td><div></div></td></tr>"
			}
			  
		}
		html += "</tbody></table></td>";
		$('#event_team_member_waiverappend_'+event_reg_id).empty().append(html);

		$('#loading-spinner').addClass('hide');
	});
};

$(document).on('click', ".eventTeamMemberWaiverValidate", function () {
	console.log("hi",$(this).attr("value"))
	var result = $(this).attr("value").split(',');
	var member_id = result[0]
	var waiver_id = result[1]
	var team_id = result[2]
	var event_id = result[3]
	var event_waiver_uploaded_path = $(".event_waiver_uploaded_path_"+member_id+"_"+team_id+"_"+event_id).val()
	console.log(result)
	console.log("memid",result[0])
	console.log("waiver_id",result[1])
	console.log("teamid",result[2])
	console.log("eventid",result[3])
	if(event_waiver_uploaded_path){

		console.log("path",event_waiver_uploaded_path)
		$.ajax({
			type: "POST",
			url: "/validate-signed-team-waiver",
			data: { "member_id": member_id,"waiver_id" :waiver_id,"team_id":team_id,"event_id":event_id }
		}).done(function (msg) {
			
			if(msg=="true"){
				$("#paytermsallowsuccesmodal").modal("show");
				$("#paytermsallowsuccesmodalText").text("Waiver Validated")
				$("#paytermsallowsuccesmodal_Body").text("Your Waiver has been validated successfully")
				setTimeout(function () {
					location.reload()
				}, FIVE_SEC);
			}
		});
	}
	else{
		console.log("path empty")
		$(".eventTeamMemberWaiverValidate").prop("checked", false);
		$("#paytermsallowsuccesmodal").modal("show");
		$("#paytermsallowsuccesmodalText").text("")
		$("#paytermsallowsuccesmodal_Body").text("Upload Signed Waiver or Sign Waiver online")

	}
	
});


$(document).on('click', ".uploadWaiverForTeamMembers", function () {
	console.log("hi",$(this).attr("value"))
	var result = $(this).attr("value").split(',');
	console.log(result)
	console.log("memid",result[0])
	console.log("waiver_id",result[1])
	console.log("teamid",result[2])
	console.log("eventid",result[3])
	console.log("event_reg_id",result[4])

	$("#waiver_err_msg_upload").text("")
	$("#waiver_err_msg_upload").removeClass('error')

	$("#waiver_succ_msg_upload").text("")
	$("#waiver_succ_msg_upload").removeClass('success')

	$("#contact_name").val("")
	$("#contact_number").val("")
	$("#medical_condition_desc").val("")
	
	
	$("#waiver_id_for_upload").val(result[1])
	$("#waiver_member_id_for_upload").val(result[0])
	$("#waiver_team_id_for_upload").val(result[2])
	$("#waiver_event_id_for_upload").val(result[3])
	$("#waiver_event_reg_id_for_upload").val(result[4])
	
	
	
	$("#uploadSignedWaiverFilesModal .progress-bar").attr("aria-valuenow", 0);
	$("#uploadSignedWaiverFilesModal .progress-bar").css("width", 0);
	$("#uploadSignedWaiverFilesModal .sr-only").text("");
	$("#waiverIndividualUpload").val('')
	$('#uploadSignedWaiverFilesModal').modal('show')
	
	});
		
$(document).on('click', "#model-accreditation", function () {
	$("#manual-model").modal("show");
});
$(document).on('click', "#manual-model-acc", function () {
	$("#manual-model").modal("show");
});

$(document).ready( function() {
	$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [label]);
	});

	$('.btn-file :file').on('fileselect', function(event, label) {
		
		var input = $(this).parents('.input-group').find(':text'),
			log = label;
		
		if( input.length ) {
			input.val(log);
		} else {
			if( log ) alert(log);
		}
	
	});


	function readURL(input) {
		if (input.files && input.files[0]) {
					var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);

					reader.onload = function() {
						var image = new Image();
						image.src = reader.result;

						image.onload = function() {
							if (image.width >100 || image.height >100) {
								
								var output = document.getElementById('img-acc-upload');
								output.src ="/static/images/user-default.png"
								$('#accgenimageUpload').val('')
								$("#imgaccInp").val(null);
								$('#acc_err_msgs').addClass('error');
								$("#acc_err_msgs").text("Please upload a image, 100x100 pixels");
								$("#general-update-model").animate({ scrollTop: 0 }, "slow");
							}else {
								var output = document.getElementById('img-acc-upload');
								output.src =reader.result
							}
							
					  };
					};
			
		}
	}
	
	$("#imgaccInp").change(function(){
		$('#acc_err_msgs').addClass('');
		$("#acc_succ_msgs").text('');
		$('#acc_err_msgs').text('');
		$("#acc_succ_msgs").addClass('');
		var ext = getFileExtension($('#imgaccInp').val());
		if (validateExt(ext)) {
		readURL(this);
		}else {
			$('#acc_err_msgs').addClass('error');
			$("#acc_err_msgs").text("Image must be png, jpg, jpeg");
			return
		}
	}); 	
});

