$(function () {
	/*PDFJS.getDocument($("#pdf").attr('data')).then(function(pdf) {
		console.log('this document has ' + pdf.numPages);
		$("#pdf").attr('height', 1200*(pdf.numPages));
	});*/
	
	// URL of PDF document
	//var url = "https://gushou-image.s3-us-west-2.amazonaws.com/testingImages/20170424102033_image.pdf";

	//renderPDF(url, document.getElementById('the-canvas'));
	console.log($("#pdf").attr('data'))
	var url = $("#pdf").attr('data');
	getBinaryData(url)

	/*var container = document.getElementById('the-canvas');
	// Load document
	PDFJS.getDocument(url).then(function (doc) {
		var promise = Promise.resolve();
		for (var i = 0; i < doc.numPages; i++) {
	    // One-by-one load pages
	    promise = promise.then(function (id) {
	    	return doc.getPage(id + 1).then(function (pdfPage) {
	// Add div with page view.
	var SCALE = 1.0; 
	var pdfPageView = new PDFJS.PDFPageView({
		container: container,
		id: id,
		scale: SCALE,
		defaultViewport: pdfPage.getViewport(SCALE),
	      // We can enable text/annotations layers, if needed
	      textLayerFactory: new PDFJS.DefaultTextLayerFactory(),
	      annotationLayerFactory: new PDFJS.DefaultAnnotationLayerFactory()
	  });
	    // Associates the actual page with the view, and drawing it
	    pdfPageView.setPdfPage(pdfPage);
	    return pdfPageView.draw();        
	});
	    }.bind(null, i));
	}
	return promise;
	});*/


});

function callGetDocment (response) {
  // body...

  PDFJS.getDocument(response).then(function(pdf) {
  	console.log('this document has ' + pdf.numPages);
  	

  	pdf.getPage(1).then(function(page) {
  		console.log(page)
  		var viewport = page.getViewport( 1 );
  		var h;

	    //Here's the width and height
	    console.log( "Width: " + viewport.width + ", Height: " + viewport.height )
	    //var h = Math.ceil(viewport.height / 100) * 100

	    if( (viewport.height < 700) && (pdf.numPages <= 10) ) {
	    	h = viewport.height + 250
	    } 
	    else if( (viewport.height > 700) && (pdf.numPages <= 10) ) {
	    	h = viewport.height + 400
	    } 
	    else {
	    	h = viewport.height
	    }
	    
	    
	    $("#pdf").attr('height', (h * (pdf.numPages) ));
	    $('#loading-spinner').addClass('hide');
  	});

  });
}

function getBinaryData (url) {
    // body...
  	console.log("show loader")

    var xhr = new XMLHttpRequest();

	xhr.open('GET', url, true);
	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	xhr.responseType = 'arraybuffer';
	xhr.onreadystatechange = function(e) {
		if (this.readyState == 4 && this.status == 200) {
			//binary form of ajax response,
			$('#loading-spinner').removeClass('hide');  
			callGetDocment(e.currentTarget.response);
			$('#loading-spinner').addClass('hide');
		}
	};  

    xhr.onerror = function  () {
        // body...
        location.reload()
    }

    xhr.send();
}

function renderPDF(url, canvasContainer, options) {
	var options = options || { scale: 1 };

	function renderPage(page) {
		var viewport = page.getViewport(options.scale);
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		var renderContext = {
			canvasContext: ctx,
			viewport: viewport
		};

		canvas.height = viewport.height;
		canvas.width = viewport.width;
		canvasContainer.appendChild(canvas);

		page.render(renderContext);
	}

	function renderPages(pdfDoc) {
		console.log(pdfDoc.numPages)
		for(var num = 1; num <= pdfDoc.numPages; num++)
			pdfDoc.getPage(num).then(renderPage);
	}

	PDFJS.disableWorker = true;
	PDFJS.getDocument(url).then(renderPages);
}