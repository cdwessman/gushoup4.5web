package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"io"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/utils/pagination"
	"github.com/astaxie/beego/validation"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/bamzi/jobrunner"
	NContext "golang.org/x/net/context"
	"googlemaps.github.io/maps"
)

type TeamController struct {
	beego.Controller
}

func (c *TeamController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Free_Subcription_Text"] = constants.FREE_TRIAL_SUBCRIPTION
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		//c.Data["TeamCaptainId"] = team_view["team_0"]["fk_captain_id"].(string)

		//user_id := c.GetSession("UserId").(int)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		c.Data["CurrentUserId"] = user_id

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

type teamValidate struct {
	TeamName       string   `form:"teamname" valid:"Required"`
	SponsorName    string   `form:"teamsponsor" `
	TeamLocation   string   `form:"teamlocation" valid:"Required"`
	TeamProvince   string   `form:"teamprovince" valid:"Required"`
	TeamCountry    string   `form:"teamcountry" valid:"Required"`
	MXTeamdivision []string `form:"MX_team_divisions[]" `
	MTeamdivision  []string `form:"M_team_divisions[]" `
	WTeamdivision  []string `form:"W_team_divisions[]" `
	TeamTwitter    string   `form:"team_twitter" `
	TeamFacebook   string   `form:"team_facebook" `
	TeamYouTube    string   `form:"team_youtube" `
	TeamPh         string   `form:"team_phone_number" `
	TeamEmail      string   `form:"team_email"`
	TeamWebsite    string   `form:"team_website" `
	Description    string   `form:"team_description"`
	Recruit        string   `form:"availableRecruit"`
	TeamMakePublic string   `form:"teammakepublic"`
}

type teamSearchValidate struct {
	TeamName  string   `form:"search_team_name"`
	City      string   `form:"city"`
	Classes   string   `form:"classes" `
	Captain   string   `form:"captain"`
	Recruit   string   `form:"recruit"`
	Divisions []string `form:"divisions[]" `
	Limit     string   `form:"limit"`
	Offset    string   `form:"offset"`
	Type      string   `form:"type"`
}

type teamPracticeValidate struct {
	PracticeDate            string `form:"practice_date"`
	PracticeTime            string `form:"practice_time"`
	PracticeLength          string `form:"practice_length" `
	PracticeLocation        string `form:"practice_location"`
	PracticeNote            string `form:"practice_note"`
	PracticeRepeats         string `form:"practice_repeats" `
	Type                    string `form:"notifyTeamMembers"`
	Id                      string `form:"practice_id"`
	EditOption              string `form:"edit_practice_option"`
	PrevPracticeRepeats     string `form:"prev_practice_repeats"`
	LocationTimeZone        string `form:"locationTimeZone"`
	PracticeDateTimeZone    string `form:"practiceDateTimeZone"`
	PracticeEndDate         string `form:"practice_end_date"`
	PracticeType            string `form:"scheduleTypeData"`
	PracticeEndDateTimeZone string `form:"practiceEndDateTimeZone"`
	EditPracticeType        string `form:"editscheduleTypeData"`
}

type availabilityPracticeValidate struct {
	PracticeId      string `form:"practice_id"`
	Response        string `form:"response"`
	Reason          string `form:"reason" `
	CurrentDateTime string `form:"currentDateTime" `
}

type teamDiscussionValidate struct {
	DiscussionMsg      string `form:"discussion_msg"`
	Type               string `form:"view_type"`
	SendAs             string `form:"send_as"`
	SendAsSms          string `form:"send_as_sms"`
	PlainDiscussionMsg string `form:"plain_discussion_msg"`
}

type teamDiscussionReplyValidate struct {
	ReplyMsg string `form:"dis_reply_msg" valid:"Required"`
	Type     string `form:"dis_notify_type"`
	DisId    string `form:"dis_id"`
}

type teamRemoveMembersValidate struct {
	MemberIds []string `form:"team_members[]" `
}

type mystruct struct {
	Sent           []string `json:"sent"`
	AlreadyInvited []string `json:"alreadyInvited"`
	AlreadyMember  []string `json:"alreadyMember"`
	Status         string   `json:"status"`
}

type attendee_struct struct {
	All             []orm.Params `json:"all"`
	Attending       []orm.Params `json:"attending"`
	NotAttending    []orm.Params `json:"not_attending"`
	Maybe           []orm.Params `json:"maybe"`
	NotResponded    []orm.Params `json:"not_responded"`
	Status          string       `json:"status"`
	Date            string       `json:"date"`
	Time            string       `json:"time"`
	Location        string       `json:"location"`
	Duration        string       `json:"duration"`
	LastReminder    string       `json:"last_reminder"`
	SmsLastReminder string       `json:"sms_last_reminder"`
	IsOldPractice   bool         `json:"isOldPractice"`
}

type email_ids struct {
	Pending_emails []string `json:"pending_emails"`
}

type practice_info struct {
	Id                   string `json:"practice_id"`
	Date                 string `json:"date"`
	Time                 string `json:"practice_time"`
	Length               string `json:"practice_length"`
	Location             string `json:"location"`
	Note                 string `json:"note"`
	Repeats              string `json:"repeats"`
	Notify               string `json:"notify"`
	Date_2               string `json:"date_2"`
	PracticeDateTimeZone string `json:"practiceDateTimeZone"`
	PracticeEndDate      string `json:"practiceenddate"`
	PracticeType         string `json:"practicetype"`
	IsOldPractice        bool   `json:"isOldPractice"`
}

type removed_member_ids struct {
	Fail   string `json:"fail"`
	Status string `json:"status"`
}

type loadMoreValidate struct {
	CommentLimit string `form:"comments_limit"`
	RepliesLimit string `form:"replies_limit"`
	Type         string `form:"type"`
}

type teamLinksValidate struct {
	Link      string `form:"link"`
	LinkTitle string `form:"link_title"`
}

type teamSMSLimit struct {
	MembershipLevel            string `json:"membership_level"`
	SMSLimit                   string `json:"sms_limits"`
	DefaultSmsAvailableForTeam string `json:"default_sms_available_for_team"`
	Free_Trial_Status          string `json:"free_trial_status"`
	Premium_Subscription       string `json:"premium_subscription"`
}

type rosterValidate struct {
	NewRosterName         string   `form:"new_RosterName" valid:"Required"`
	RosterType            string   `form:"rosterType" valid:"Required"`
	RosterEventId         string   `form:"rosterEventId"`
	RosterPracticeId      string   `form:"rosterPracticeId"`
	RosterComment         string   `form:"rostercomment"`
	TeamRosterMembers     []string `form:"team_roster_members[]"`
	TeamRosterMembersRole string   `form:"team_roster_members_role"`
	SaveDraft             string   `form:"team_save_draft_roster"`
	CurrentTeamId         string   `form:"current_teamid"`
}

type lineUpValidate struct {
	LineupName            string   `form:"lineupname"`
	RosterId              string   `form:"roster_id"`
	LeftPadllerMembers    []string `form:"left_padller_members[]"`
	RightPadllerMembers   []string `form:"right_padller_members[]"`
	OtherMembers          []string `form:"other_members[]"`
	SelectedLeftPaddlers  []string `form:"selected_left_paddlers[]"`
	SelectedRightPaddlers []string `form:"selected_right_paddlers[]"`
	SelectedDrummers      []string `form:"selected_drummers[]"`
	SelectedSpares        []string `form:"selected_spares[]"`
	SaveDraft             string   `form:"team_save_draft_lineup"`
	CurrentTeamId         string   `form:"current_teamid"`
}

type rosterattendee_struct struct {
	All          []orm.Params `json:"all"`
	NotResponded []orm.Params `json:"not_responded"`
}

type lineUpDetail_struct struct {
	LineupData     []orm.Params `json:"lineupdata"`
	RosterUserData []orm.Params `json:"rosteruserdata"`
}

type create_roster_struct struct {
	RosterData         []orm.Params `json:"rosterdata"`
	RosterSubmitStatus string       `json:"rostersubmitstatus"`
}

type sms_subscription_count struct {
	PopUpStatus string `json:"popupstatus"`
	SMSCount    string `json:"smscount"`
}

type get_roster_detail_struct struct {
	RosterDetailData     []orm.Params `json:"rosterdetaildata"`
	RosterMemberRoleData []orm.Params `json:"rostermemberroledata"`
	RosterSubmitStatus   []orm.Params `json:"rostersubmitstatus"`
}

type submit_later_roster_detail struct {
	RegisterLaterClassDivisionList []map[string]string `json:"registerlaterclassdivisionlist"`
	TeamRoosterList                []orm.Params        `json:"teamroosterlist"`
	RegisterRosterDetailList       []orm.Params        `json:"registerrosterdetaillist"`
}

/*type TeamMember struct {
	Name string
	//AcceptedDate  int
}

type ByName []TeamMember

func (a ByName) Len() int           { return len(a) }
func (a ByName) Less(i, j int) bool {
    return slice[i].Name < slice[j].Name;
}

func (a ByName) Swap(i, j int) {
    slice[i], slice[j] = slice[j], slice[i]
}*/

func (c *TeamController) CreateTeam() {

	//models.GetAllStockItems()
	c.TplName = "team/createTeam.html"
	c.Data["PageTitle"] = "Create Team"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	c.Data["create_team_1"] = true
	c.Data["create_team_2"] = false
	c.Data["create_team_3"] = false

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		user_id := c.GetSession("UserId").(int)
		isValid := models.CanCreateTeam(user_id)

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

		if isValid {
			c.Data["userActivity"] = true
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	fmt.Println(c.Ctx.Input.Method())

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//c.Redirect("/dashboard", 302)
		fmt.Println(items)

		result := make(map[string]string)
		user_id := c.GetSession("UserId").(int)
		fmt.Println(user_id)

		user_details := models.GetUserDetails(strconv.Itoa(user_id))

		data := make(map[string]string)
		data["TeamName"] = items.TeamName
		data["SponsorName"] = items.SponsorName
		data["TeamLocation"] = items.TeamLocation
		data["TeamMakePublic"] = items.TeamMakePublic

		classes := ""
		div_string := ""
		if len(items.MTeamdivision) == 0 {
			data["Divisions"] = ""
			classes = classes
			data["MTeamdivision"] = ""
		} else {
			div_string = strings.Join(items.MTeamdivision[:], ",")
			data["Divisions"] = div_string + data["Divisions"]
			data["MTeamdivision"] = div_string + data["MTeamdivision"]
			classes = classes + "M"
		}

		if len(items.WTeamdivision) == 0 {
			data["Divisions"] = data["Divisions"]
			classes = classes
			data["WTeamdivision"] = ""
		} else {
			div_string = strings.Join(items.WTeamdivision[:], ",")
			data["Divisions"] = div_string + "," + data["Divisions"]
			data["WTeamdivision"] = div_string + data["WTeamdivision"]

			if classes == "" {
				classes = classes + "W"
			} else {
				classes = classes + ",W"
			}

		}

		if len(items.MXTeamdivision) == 0 {
			data["Divisions"] = data["Divisions"]
			classes = classes
			data["MXTeamdivision"] = ""
		} else {
			div_string = strings.Join(items.MXTeamdivision[:], ",")
			data["Divisions"] = div_string + "," + data["Divisions"]
			data["MXTeamdivision"] = div_string + data["MXTeamdivision"]

			if classes == "" {
				classes = classes + "MX"
			} else {
				classes = classes + ",MX"
			}
		}

		tmp := strings.Split(data["Divisions"], ",")
		fmt.Println(tmp)
		tmp = models.RemoveDuplicates(tmp)
		data["Divisions"] = strings.Join(tmp[:], ",")

		tmp = strings.Split(data["MTeamdivision"], ",")
		data["MTeamdivision"] = strings.Join(tmp[:], ",")

		tmp = strings.Split(data["WTeamdivision"], ",")
		data["WTeamdivision"] = strings.Join(tmp[:], ",")

		tmp = strings.Split(data["MXTeamdivision"], ",")
		data["MXTeamdivision"] = strings.Join(tmp[:], ",")

		fmt.Println(data["Divisions"])
		fmt.Println(classes)

		data["Divisions"] = "{" + data["Divisions"] + "}"
		data["Classes"] = "{" + classes + "}"

		data["MTeamdivision"] = "{" + data["MTeamdivision"] + "}"
		data["WTeamdivision"] = "{" + data["WTeamdivision"] + "}"
		data["MXTeamdivision"] = "{" + data["MXTeamdivision"] + "}"

		data["TeamProvince"] = items.TeamProvince
		data["TeamCountry"] = items.TeamCountry
		data["TeamPh"] = items.TeamPh

		if items.TeamTwitter != "" {
			data["TeamTwitter"] = "https://www.twitter.com/" + items.TeamTwitter
		} else {
			data["TeamTwitter"] = items.TeamTwitter
		}

		if items.TeamFacebook != "" {
			data["TeamFacebook"] = "https://www.facebook.com/" + items.TeamFacebook
		} else {
			data["TeamFacebook"] = items.TeamFacebook
		}

		if items.TeamYouTube != "" {
			data["TeamYouTube"] = "https://www.youtube.com/" + items.TeamYouTube
		} else {
			data["TeamYouTube"] = items.TeamYouTube
		}

		data["TeamEmail"] = items.TeamEmail

		if items.TeamWebsite != "" {
			data["TeamWebsite"] = "http://" + items.TeamWebsite
		} else {
			data["TeamWebsite"] = items.TeamWebsite
		}

		x, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
		if err != nil {
			fmt.Println("fatal error: %s", err)
		}

		r := &maps.GeocodingRequest{
			Address: items.TeamLocation + ", " + items.TeamProvince + ", " + items.TeamCountry,
		}

		resp, err := x.Geocode(NContext.Background(), r)
		fmt.Printf("%+v\n", resp)
		if resp != nil {
			if len(resp) > 0 {
				data["Latitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lat, 'f', -1, 64)
				data["Longitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lng, 'f', -1, 64)
			}
		}

		reg, err := regexp.Compile("[^A-Za-z0-9À-ž]+")
		if err != nil {
			fmt.Println(err)
		}

		name_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.TeamName), " "))
		loc_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.TeamLocation), " "))

		data["Slug"] = strings.Replace(name_slug, " ", "-", -1) + "-" + strings.Replace(loc_slug, " ", "-", -1)
		data["SmsLimits"] = constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM

		result = models.CreateTeam(data, user_id, user_details["user"]["email_address"].(string))

		if result["status"] == "1" {

			c.Data["completeRegister"] = true
			c.Redirect("/team-view/"+data["Slug"], 302)
		}

	}

}

func (c *TeamController) TeamList() {
	c.TplName = "team/teamList.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	var all_divs []orm.Params
	all_divs = models.GetAllDivisions()
	if all_divs[0] != nil {
		c.Data["AllDivs"] = all_divs
	}

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := teamSearchValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//c.Redirect("/dashboard", 302)

		fmt.Println(items)
		result := make(map[string]orm.Params)

		data := make(map[string]string)

		data["TeamName"] = items.TeamName
		data["City"] = items.City
		data["Captain"] = items.Captain

		if items.Recruit == "recruit" {
			data["Recruit"] = "1"
		} else {
			data["Recruit"] = ""
		}

		/*if items.Classes == "any" {
			data["Classes"] =
		} else if items.Classes == "all" {
			data["Classes"] =
		} else {
			data["Classes"] =
		}*/

		data["Classes"] = items.Classes
		div_string := strings.Join(items.Divisions[:], ",")
		data["Divisions"] = div_string

		data["Limit"] = items.Limit
		data["Offset"] = items.Offset
		data["Type"] = items.Type

		var offsetInt, limitInt = 0, 0
		num, err := strconv.Atoi(items.Limit)
		if err == nil {
			limitInt = num
		}
		num, err = strconv.Atoi(items.Offset)
		if err == nil {
			offsetInt = num
		}
		c.Data["NewOffset"] = items.Offset
		c.Data["NewLimit"] = items.Limit
		c.Data["Type"] = items.Type
		c.Data["AdvSearch"] = false

		for k, v := range data {

			if k == "Classes" {
				if v != "any" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Divisions" {
				if len(v) > 0 {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "City" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Captain" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Recruit" {
				if v == "1" {
					c.Data["AdvSearch"] = true
					break
				}
			}

		}

		result, totalCount := models.SearchTeam(data)

		if _, ok := result["status"]; !ok {

			if len(result) > 0 {
				c.Data["TeamList"] = true
				c.Data["AllTeams"] = result
				c.Data["TotalTeams"] = totalCount

				i, _ := strconv.ParseInt(items.Limit, 10, 64)

				totalPages := int(math.Ceil(float64(totalCount) / float64(i)))
				fmt.Println("Pages ", totalPages)

				pages := make(map[int]int)
				offset := 0
				for i := 1; i <= totalPages; i++ {
					pages[i] = offset

					offset = offset + limitInt
				}
				c.Data["TotalPages"] = pages
				fmt.Println(pages)

				tmp := make(map[int]int)
				var currPage int

				if items.Type != "last" {
					for k, v := range pages {

						if v == offsetInt {
							c.Data["CurrPage"] = k
							currPage = k
							break
						}

					}
				} else {
					c.Data["CurrPage"] = totalPages
					currPage = totalPages
				}

				if currPage == 0 {
					for k, _ := range pages {
						if k+1 <= len(pages) {
							fmt.Println(pages[k+1], " > ", offsetInt)
							if pages[k+1] > offsetInt {
								c.Data["CurrPage"] = k
								currPage = k
								break
							}
						}
					}
				}

				fmt.Println(" CurrPage ", currPage)

				if currPage < 4 {

					if len(pages) >= 5 {
						for i := 1; i <= 5; i++ {
							tmp[i] = pages[i]
						}
					} else {
						for i := 1; i <= len(pages); i++ {
							tmp[i] = pages[i]
						}
					}

				} else {

					if currPage+2 <= totalPages {
						for i := currPage - 2; i <= currPage+2; i++ {
							tmp[i] = pages[i]
						}
					} else {
						for i := currPage - 3; i <= totalPages; i++ {
							tmp[i] = pages[i]
						}
					}

				}

				c.Data["FivePages"] = tmp

			}

		} else {
			c.Data["TeamList"] = false
		}

		//fmt.Println(result)

	} else {
		result := make(map[string]orm.Params)
		result, totalCount := models.SearchTeam(nil)

		if _, ok := result["status"]; !ok {
			c.Data["TeamList"] = true
			c.Data["AllTeams"] = result
			c.Data["TotalTeams"] = totalCount

			totalPages := int(math.Ceil(float64(totalCount) / float64(10)))
			fmt.Println("Pages ", totalPages)
			fmt.Println("TotalTeams ", totalCount)

			pages := make(map[int]int)
			offset := 0

			for i := 1; i <= totalPages; i++ {
				pages[i] = offset

				offset = offset + 10
			}
			c.Data["TotalPages"] = pages

			tmp := make(map[int]int)

			for i := 1; i <= 5; i++ {
				tmp[i] = pages[i]
			}

			c.Data["FivePages"] = tmp
			c.Data["CurrPage"] = 1

			c.Data["AdvSearch"] = false

		}
	}

	posts := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"}
	postsPerPage := 5
	paginator := pagination.SetPaginator(c.Ctx, postsPerPage, 10)

	// fetch the next 20 posts
	fmt.Println(paginator.Offset())
	fmt.Println(paginator.PageLinkFirst)
	c.Data["posts"] = posts[paginator.Offset():5]

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	// all_divs :=make(map[string]string)
	// all_divs["1"] = "A"
	// all_divs["2"] = "B"
	// all_divs["3v"] = "C"
	// //all_divs :=[]string{"A","B","C","D","E"}

	// c.Data["all_divs"] = all_divs
	// for index,element := range all_divs {
	// 	fmt.Println(index, element)

	// }
}

func (c *TeamController) TeamView() {
	c.TplName = "team/teamView.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	var all_captains []string
	var mem_ids []string

	fmt.Println(c.Ctx.Input.Param(":slug"))
	slug := c.Ctx.Input.Param(":slug")
	c.Data["Slug"] = slug
	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	if c.GetSession("PracticeDate") != nil {
		c.Data["PracticeDate"] = c.GetSession("PracticeDate")
		c.DelSession("PracticeDate")
	}
	if c.GetSession("PracticeTime") != nil {
		c.Data["PracticeTime"] = c.GetSession("PracticeTime")
		c.DelSession("PracticeTime")
	}

	team_view := make(map[string]orm.Params)
	userID := "0"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		id := c.GetSession("UserId").(int)
		userID = strconv.Itoa(id)
	}

	team_view = models.GetTeamBySlug(slug, models.IsLoggedIn(c.GetSession("GushouSession")), userID)

	if _, ok := team_view["status"]; !ok {
		c.Data["Teammakepublic"] = team_view["team_0"]["team_make_public"].(string)
		c.Data["Teamnametext"] = team_view["team_0"]["team_name"].(string)
		c.SetSession("TeamId", team_view["team_0"]["id"].(string))

		if team_view["team_0"]["divisions"] != nil {
			s := strings.Replace(team_view["team_0"]["divisions"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Divisions"] = strings.Split(s, ",")
		}

		if team_view["team_0"]["classes"] != nil {
			s := strings.Replace(team_view["team_0"]["classes"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Classes"] = strings.Split(s, ",")
		}

		if team_view["team_0"]["fk_co_captains"] != nil {
			s := strings.Replace(team_view["team_0"]["fk_co_captains"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["CoCaptainsScript"] = s
			c.Data["CoCaptains"] = strings.Split(s, ",")
			all_captains = strings.Split(s, ",")

		}

		for _, v := range team_view {
			//key := "class_" + strconv.Itoa(k)
			if v["class"] == "M" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MDivisions"] = strings.Split(s, ",")
					c.Data["MDiv"] = true
				}

			} else if v["class"] == "W" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["WDivisions"] = strings.Split(s, ",")
				}

			} else if v["class"] == "MX" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MXDivisions"] = strings.Split(s, ",")
				}

			}

		}
		c.Data["TeamView"] = team_view

		var contact []int

		if team_view["team_0"]["twitter_handle"] != nil {
			if len(team_view["team_0"]["twitter_handle"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}
		} else {
			contact = append(contact, 0)
		}

		if team_view["team_0"]["facebook_page_url"] != nil {
			if len(team_view["team_0"]["facebook_page_url"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if team_view["team_0"]["youtube_link"] != nil {
			if len(team_view["team_0"]["youtube_link"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if team_view["team_0"]["phone_number"] != nil {
			if len(team_view["team_0"]["phone_number"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if team_view["team_0"]["email"] != nil {
			if len(team_view["team_0"]["email"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if team_view["team_0"]["website_url"] != nil {
			if len(team_view["team_0"]["website_url"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		for _, val := range contact {
			if val == 1 {
				c.Data["ContactSection"] = true
				break
			} else {
				c.Data["ContactSection"] = false
			}
		}

	} else {
		fmt.Println(" Wrong slug ")
		c.Redirect("/", 302)
		return
	}
	//fmt.Println(team_view)

	//var count int = 0
	team_captain := make(map[string]orm.Params)
	team_captain = models.GetUserDetails(team_view["team_0"]["fk_captain_id"].(string))
	if _, ok := team_captain["status"]; !ok {
		c.Data["TeamCaptain"] = team_captain

		c.Data["TeamCaptainId"] = team_view["team_0"]["fk_captain_id"].(string)

		if team_view["team_0"]["fk_co_captains"] != nil {
			s := strings.Replace(team_view["team_0"]["fk_co_captains"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["CoCaptainsScript"] = s
			c.Data["CoCaptains"] = strings.Split(s, ",")
			fmt.Println(strings.Split(s, ","))
			all_captains = strings.Split(s, ",")
		}
	}

	teamCaptainStatus := false
	teamCoCaptainStatus := false
	remainingDays := 0
	popupstatus := "true"
	isFreeTrialAvailable := "false"
	teamSubscriptiondayDiffrence := ""
	teamSubscriptionplan_end_date := ""
	c.Data["Change_Captain_By_Admin"] = false

	activeUserBoolStatus := false
	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		var team_Roaster []orm.Params
		localUserTimeZone := GetTimezoneInSessionTeam(c)
		tempid := c.GetSession("UserId").(int)
		tempuserID := strconv.Itoa(tempid)
		if c.GetSession("SuperAdmin") != "true" {
			activeUserBoolStatus = models.GetActiveUserStatus(tempuserID, team_view["team_0"]["id"].(string))
		}

		c.Data["Active_User_Status"] = activeUserBoolStatus

		team_Roaster = models.GetRoosterDetails(team_view["team_0"]["id"].(string), localUserTimeZone, activeUserBoolStatus)
		c.Data["TeamRoaster"] = team_Roaster

		var team_LineUp []orm.Params
		team_LineUp = models.GetLineUpDetails(team_view["team_0"]["id"].(string), localUserTimeZone, activeUserBoolStatus)
		c.Data["TeamLineUp"] = team_LineUp

		var team_EventRegistration []orm.Params
		team_EventRegistration = models.GetEventRegistrationDetail_Roster(team_view["team_0"]["id"].(string))
		c.Data["TeamEventRegistrationDetails"] = team_EventRegistration

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		if team_view["team_0"]["fk_captain_id"].(string) == user_id {
			teamCaptainStatus = true
		}

		if team_view["team_0"]["change_captain_status_by_admin"].(string) == "true" {
			c.Data["Change_Captain_By_Admin"] = true
			// models.UpdateChange_Captain_Status(team_view["team_0"]["id"].(string), false)
		}

		teamSubscriptiondayDiffrence, teamSubscriptionplan_end_date = models.Check_The_Team_SubscriptionValid_Time(team_view["team_0"]["id"].(string))
		if !teamCaptainStatus {
			for _, captain := range all_captains {

				if user_id == captain {
					teamCoCaptainStatus = true
					break
				}
			}
		}

		c.Data["SmsNextCreditStartingDate"] = models.Check_The_SmsCreditStartingDate(team_view["team_0"]["id"].(string))
		c.Data["TeamFreeTrialPeriod"] = team_view["team_0"]["free_trial"].(string)
		c.Data["TeamSubscriptionPeriod"] = team_view["team_0"]["subscription_on_off"].(string)

		models.ResetSubscriptionSmsPackageCount(team_view["team_0"]["id"].(string))
		remainingDays, isFreeTrialAvailable, popupstatus = models.CheckShowPopupTrialPeriodNotification(team_view)

		c.Data["UserId"] = c.GetSession("UserId")
		if c.GetSession("UserId") != nil {
			teamManagmentStatus, eventorganisingStatus, paddlingStatus := models.GetLatestUserActivitiesDetail(c.GetSession("UserId").(int))
			c.SetSession("Paddling_Admin", paddlingStatus)
			c.SetSession("Event_Manage_Admin", eventorganisingStatus)
			c.SetSession("Team_Manage_Admin", teamManagmentStatus)
		}
		c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
		c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
		c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")

		clickedtabname := c.Ctx.Input.Param(":tabname")
		c.Data["Show_Event_Register_Page"] = false
		c.Data["Event_Register_Count"] = "0"
		if clickedtabname == "event_register" {
			c.Data["Show_Event_Register_Page"] = true
			c.Data["Event_Register_Detail_List"] = models.GetRegisterEventTeamDetail(team_view["team_0"]["id"].(string))
		} else {
			c.Data["Event_Register_Count"] = models.GetRegisterEventCount(team_view["team_0"]["id"].(string))
		}

		if clickedtabname == "team_event_payment" {
			if c.GetSession("SuperAdmin") == "true" {
				c.Data["SuperAdmin"] = "true"
			} else {
				c.Data["SuperAdmin"] = "false"
			}
			c.Data["Show_TeamPayment_Page"] = true
			stripeSourceCardStatus, last4digit := models.CheckSubscription_Card_Detail_By_UserId(user_id)
			c.Data["StripeSourceCardLast4"] = last4digit
			c.Data["StripeSourceCardStatus"] = stripeSourceCardStatus

			c.Data["Team_Payment_Detail_List"] = models.GetRegisterEventTeamPaymentDetail(team_view["team_0"]["id"].(string))
		} else {
			c.Data["Team_Payment_Detail_Count"] = models.GetRegisterEventTeamPaymentCount(team_view["team_0"]["id"].(string))
		}

		c.Data["AllRolesList"] = models.GetAllRolesList()

	}

	c.Data["TeamSubscriptionDayDiffrence"] = teamSubscriptiondayDiffrence
	c.Data["TeamSubscriptionEndDate"] = teamSubscriptionplan_end_date

	c.Data["RemainingDays"] = remainingDays
	c.Data["PopupStatus"] = popupstatus
	c.Data["IsFreeTrialAvailable"] = isFreeTrialAvailable

	c.Data["TeamCaptainStatus"] = teamCaptainStatus
	c.Data["TeamCoCaptainStatus"] = teamCoCaptainStatus
	c.Data["IsTeamViewPage"] = true

	var team_practice []orm.Params
	loginUserTimeZone := GetTimezoneInSessionTeam(c)
	team_practice = models.GetTeamPractices(team_view["team_0"]["id"].(string), loginUserTimeZone)
	if team_practice[0] != nil {
		c.Data["TeamPractice"] = team_practice
	}

	//fmt.Println(c.Data["TeamCaptain"])

	// to fetch active team members
	var team_members []orm.Params
	team_members = models.GetTeamMembers(team_view["team_0"]["id"].(string))
	mem_keys := make([]int, len(team_members), len(team_members))
	mem_index := 0
	//fmt.Println(team_members)
	for k, _ := range team_members {
		fmt.Println("k ", k)
		fmt.Println("index ", mem_index)

		mem_keys[mem_index] = mem_index
		mem_index++
	}

	sort.Ints(mem_keys)

	//fmt.Println(mem_keys)
	c.Data["Mem_Keys"] = mem_keys
	//fmt.Println(c.Data["Mem_Keys"])
	fmt.Println("team_members")
	//fmt.Println(team_members)
	if team_members[0] != nil {
		c.Data["TeamMembers"] = team_members
		c.Data["MemberCount"] = len(team_members)
		fmt.Println("COUNT ", len(team_members), len(team_captain))

		if models.IsLoggedIn(c.GetSession("GushouSession")) {

			for _, v := range team_members {
				mem_ids = append(mem_ids, v["fk_user_id"].(string))
			}

			c.Data["MemIds"] = mem_ids

		}
	}

	//fmt.Println(c.Data["TeamMembers"])

	var team_members_alpha []orm.Params
	team_members_alpha = models.GetTeamMembersAlpha(team_view["team_0"]["id"].(string))
	fmt.Printf("Team Members List \n")

	if team_members_alpha[0] != nil {
		c.Data["TeamMembersAlpha"] = team_members_alpha
	}

	team_logo := models.GetTeamLogo(team_view["team_0"]["id"].(string))
	if team_logo != "" {
		c.Data["TeamLogo"] = team_logo
	}

	team_discussion := make(map[string]orm.Params)
	var notify_type []string

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		if c.GetSession("SuperAdmin") != "true" {
			if common.InArray(user_id, all_captains) {

				fmt.Println(" All discussion")
				notify_type = []string{"team", "captain", "everyone"}

			} else if common.InArray(user_id, mem_ids) {

				fmt.Println("Team and public")
				notify_type = []string{"team", "everyone", ""}

			} else {

				fmt.Println("Only public")
				notify_type = []string{"everyone", "", ""}

			}
		} else {

			fmt.Println(" All discussion")
			notify_type = []string{"team", "captain", "everyone"}

		}
	} else {

		fmt.Println("Only public")
		notify_type = []string{"everyone", "", ""}
	}

	fmt.Println(notify_type)

	if c.Ctx.Input.Method() == "POST" {

		//c.LayoutSections = make(map[string]string)
		//c.LayoutSections["Scripts"] = "team/content.tpl"
		fmt.Println("In POST ")

		flash := beego.NewFlash()
		items := loadMoreValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)

		if items.Type == "comments" {
			limitInt := 0
			num, err := strconv.Atoi(items.CommentLimit)
			if err == nil {
				limitInt = num
			}

			team_discussion = models.GetTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, limitInt+3)
			if _, ok := team_discussion["status"]; !ok {
				c.Data["TeamDiscussion"] = team_discussion
				c.Data["TeamDiscussionCount"] = len(team_discussion)

				moreResults := models.GetTotalTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, limitInt+3)
				if moreResults {
					c.Data["MoreResult"] = true
				}

			}

			c.Data["RepliesLimit"] = items.RepliesLimit

		} else {

			limitInt := 0
			num, err := strconv.Atoi(items.CommentLimit)
			if err == nil {
				limitInt = num
			}

			team_discussion = models.GetTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, limitInt)
			if _, ok := team_discussion["status"]; !ok {
				c.Data["TeamDiscussion"] = team_discussion
				c.Data["TeamDiscussionCount"] = len(team_discussion)

				moreResults := models.GetTotalTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, limitInt)
				if moreResults {
					c.Data["MoreResult"] = true
				}

			}

			num, err = strconv.Atoi(items.RepliesLimit)
			if err == nil {
				limitInt = num
			}

			c.Data["RepliesLimit"] = strconv.Itoa(limitInt + 3)

		}

	} else {

		team_discussion = models.GetTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, 3)
		if _, ok := team_discussion["status"]; !ok {
			c.Data["TeamDiscussion"] = team_discussion
			c.Data["TeamDiscussionCount"] = len(team_discussion)

			moreResults := models.GetTotalTeamDiscussion(team_view["team_0"]["id"].(string), notify_type, 3)
			if moreResults {
				c.Data["MoreResult"] = true
			}

		}

		c.Data["RepliesLimit"] = "3"

	}

	keys := make([]int, len(team_discussion), len(team_discussion))
	index := 0
	//fmt.Println(c.Data["TeamDiscussion"])
	for _, _ = range team_discussion {

		keys[index] = index
		index++
	}

	sort.Ints(keys)
	//keys = models.ReverseArrayInt(keys)
	//fmt.Println(keys)
	c.Data["Keys"] = keys
	//fmt.Println(c.Data["Keys"])

	var team_pending_req []orm.Params
	team_pending_req = models.GetTeamPendingReq(team_view["team_0"]["id"].(string))
	if team_pending_req[0] != nil {
		c.Data["TeamPendingReq"] = team_pending_req
		c.Data["TeamPendingReqCount"] = len(team_pending_req)
	}

	var team_pending_req_alpha []orm.Params
	team_pending_req_alpha = models.GetTeamPendingReqAlpha(team_view["team_0"]["id"].(string))
	if team_pending_req_alpha[0] != nil {
		c.Data["TeamPendingReqAlpha"] = team_pending_req_alpha
	}

	team_sponsor := make(map[string]orm.Params)
	team_sponsor = models.GetTeamSponsor(team_view["team_0"]["id"].(string))
	if _, ok := team_sponsor["status"]; !ok {
		c.Data["TeamSponsor"] = team_sponsor
	}

	team_waivers := make(map[string]orm.Params)
	team_waivers = models.GetTeamWaiver(team_view["team_0"]["id"].(string))
	if _, ok := team_waivers["status"]; !ok {
		c.Data["TeamWaiver"] = team_waivers
	}

	team_notes := make(map[string]orm.Params)
	team_notes = models.GetTeamNote(team_view["team_0"]["id"].(string))
	if _, ok := team_notes["status"]; !ok {
		c.Data["TeamNote"] = team_notes
	}

	team_links := make(map[string]orm.Params)
	team_links = models.GetTeamLinks(team_view["team_0"]["id"].(string))
	if _, ok := team_links["status"]; !ok {
		c.Data["TeamLink"] = team_links
	}

	// to fetch upcoming events
	upcoming_event := make(map[string]orm.Params)
	upcoming_event = models.GetAllUpcomingEvent()
	if _, ok := upcoming_event["status"]; !ok {
		c.Data["UpcomingEvent"] = upcoming_event
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		//c.Data["TeamCaptainId"] = team_view["team_0"]["fk_captain_id"].(string)

		//user_id := c.GetSession("UserId").(int)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		c.Data["CurrentUserId"] = user_id

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *TeamController) TeamAll() {
	c.TplName = "team/teamsAll.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	user_id := c.GetSession("UserId").(int)

	team_manage := make(map[string]orm.Params)
	team_manage = models.GetTeam(user_id, false)
	if _, ok := team_manage["status"]; !ok {

		c.Data["TeamManage"] = team_manage
		c.Data["TeamManageCount"] = len(team_manage)
	}

	team_part_of := make(map[string]orm.Params)
	team_part_of = models.GetTeamPartOf(user_id, false)
	if _, ok := team_part_of["status"]; !ok {
		c.Data["TeamPartOf"] = team_part_of
		c.Data["TeamPartOfCount"] = len(team_part_of)
	}

	c.Data["AllTeamsCount"] = 0
	all_my_teams := make(map[string]orm.Params)
	all_my_teams = models.GetAllMyTeams(user_id, false)
	if _, ok := all_my_teams["status"]; !ok {
		c.Data["AllMyTeams"] = all_my_teams
		c.Data["AllTeamsCount"] = len(all_my_teams)
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *TeamController) EditTeam() {
	c.TplName = "team/teamEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.GetSession("UpdateSuccess") == "true" {
		c.Data["UpdateSuccess"] = true
		c.DelSession("UpdateSuccess")
	}

	fmt.Println(c.Ctx.Input.Param(":slug"))
	slug := c.Ctx.Input.Param(":slug")

	var all_captains []string

	team_view := make(map[string]orm.Params)

	userID := "0"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		id := c.GetSession("UserId").(int)
		userID = strconv.Itoa(id)
	}

	team_view = models.GetTeamBySlug(slug, models.IsLoggedIn(c.GetSession("GushouSession")), userID)
	if _, ok := team_view["status"]; !ok {

		if team_view["team_0"]["divisions"] != nil {
			s := strings.Replace(team_view["team_0"]["divisions"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Divisions"] = strings.Split(s, ",")
		}

		if team_view["team_0"]["classes"] != nil {
			s := strings.Replace(team_view["team_0"]["classes"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Classes"] = strings.Split(s, ",")
		}

		if team_view["team_0"]["fk_co_captains"] != nil {
			s := strings.Replace(team_view["team_0"]["fk_co_captains"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["CoCaptainsScript"] = s
			c.Data["CoCaptains"] = strings.Split(s, ",")
			all_captains = strings.Split(s, ",")
		}

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		if c.GetSession("SuperAdmin") != "true" {
			if !common.InArray(user_id, all_captains) && (user_id != team_view["team_0"]["fk_captain_id"].(string)) {
				fmt.Println(" You cannot edit the team")
				fmt.Println("User id ", user_id, " Captain ids ", all_captains)
				c.Redirect("/", 302)
			}
		}

		for _, v := range team_view {
			//key := "class_" + strconv.Itoa(k)
			if v["class"] == "M" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MDivisions"] = strings.Split(s, ",")
					c.Data["MDiv"] = true
				}

			} else if v["class"] == "W" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["WDivisions"] = strings.Split(s, ",")
				}

			} else if v["class"] == "MX" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MXDivisions"] = strings.Split(s, ",")
				}

			}

		}

		c.Data["TeamView"] = team_view
	} else {
		fmt.Println(" Wrong slug ")
		c.Redirect("/", 302)
		return
	}

	var count int = 0

	team_captain := make(map[string]orm.Params)
	team_captain = models.GetUserDetails(team_view["team_0"]["fk_captain_id"].(string))
	if _, ok := team_view["status"]; !ok {
		c.Data["TeamCaptain"] = team_captain
		count = 1
		c.Data["MemberCount"] = 1
	}

	fmt.Println(c.Data["TeamCaptain"])

	// to fetch Team Active Members list
	var team_members []orm.Params
	team_members = models.GetTeamMembersAlpha(team_view["team_0"]["id"].(string))

	if team_members[0] != nil {
		c.Data["TeamMembers"] = team_members
		c.Data["MemberCount"] = len(team_members) + count
		fmt.Println("COUNT ", len(team_members), len(team_captain))
	}

	// to fetch Team logo
	team_logo := models.GetTeamLogo(team_view["team_0"]["id"].(string))
	if team_logo != "" {
		c.Data["TeamLogo"] = team_logo
	}

	// to fetch Team Sponsors
	team_sponsor := make(map[string]orm.Params)
	team_sponsor = models.GetTeamSponsor(team_view["team_0"]["id"].(string))
	if _, ok := team_sponsor["status"]; !ok {
		c.Data["TeamSponsor"] = team_sponsor
	}

	//Code is comment add in team view controller
	// // to fetch upcoming events
	// upcoming_event := make(map[string]orm.Params)
	// upcoming_event = models.GetAllUpcomingEvent()
	// if _, ok := upcoming_event["status"]; !ok {
	// 	c.Data["UpcomingEvent"] = upcoming_event
	// }

	team_waivers := make(map[string]orm.Params)
	team_waivers = models.GetTeamWaiver(team_view["team_0"]["id"].(string))
	if _, ok := team_waivers["status"]; !ok {
		c.Data["TeamWaiver"] = team_waivers
	}

	team_notes := make(map[string]orm.Params)
	team_notes = models.GetTeamNote(team_view["team_0"]["id"].(string))
	if _, ok := team_notes["status"]; !ok {
		c.Data["TeamNote"] = team_notes
	}

	team_links := make(map[string]orm.Params)
	team_links = models.GetTeamLinks(team_view["team_0"]["id"].(string))
	if _, ok := team_links["status"]; !ok {
		c.Data["TeamLink"] = team_links
	}

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := teamValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)
		result := make(map[string]string)

		data["TeamName"] = items.TeamName
		data["TeamLocation"] = items.TeamLocation
		data["TeamProvince"] = items.TeamProvince
		data["TeamCountry"] = items.TeamCountry
		data["TeamLocation"] = items.TeamLocation
		data["Description"] = items.Description
		data["TeamMakePublic"] = items.TeamMakePublic
		if items.Recruit == "on" {
			data["Recruit"] = "1"
		} else {
			data["Recruit"] = "0"
		}

		data["TeamPh"] = items.TeamPh

		if items.TeamTwitter != "" {
			data["TeamTwitter"] = "https://www.twitter.com/" + items.TeamTwitter
		} else {
			data["TeamTwitter"] = items.TeamTwitter
		}

		if items.TeamFacebook != "" {
			data["TeamFacebook"] = "https://www.facebook.com/" + items.TeamFacebook
		} else {
			data["TeamFacebook"] = items.TeamFacebook
		}

		if items.TeamYouTube != "" {
			data["TeamYouTube"] = "https://www.youtube.com/" + items.TeamYouTube
		} else {
			data["TeamYouTube"] = items.TeamYouTube
		}

		data["TeamEmail"] = items.TeamEmail

		if items.TeamWebsite != "" {
			data["TeamWebsite"] = "http://" + items.TeamWebsite
		} else {
			data["TeamWebsite"] = items.TeamWebsite
		}

		x, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
		if err != nil {
			fmt.Println("fatal error: %s", err)
		}

		r := &maps.GeocodingRequest{
			Address: items.TeamLocation + ", " + items.TeamProvince + ", " + items.TeamCountry,
		}

		resp, err := x.Geocode(NContext.Background(), r)
		if resp != nil {
			if len(resp) > 0 {
				data["Latitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lat, 'f', -1, 64)
				data["Longitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lng, 'f', -1, 64)
			}
		}

		reg, err := regexp.Compile("[^A-Za-z0-9À-ž]+")
		if err != nil {
			fmt.Println(err)
		}

		name_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.TeamName), " "))
		loc_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.TeamLocation), " "))

		data["Slug"] = strings.Replace(name_slug, " ", "-", -1) + "-" + strings.Replace(loc_slug, " ", "-", -1)

		result = models.EditTeam(data, team_view["team_0"]["id"].(string))

		if result["status"] == "1" {

			c.SetSession("UpdateSuccess", "true")
			c.Redirect("/team-edit/"+data["Slug"], 302)
		}
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		activeUserBoolStatus := false
		tempid := c.GetSession("UserId").(int)
		tempuserID := strconv.Itoa(tempid)
		if c.GetSession("SuperAdmin") != "true" {
			activeUserBoolStatus = models.GetActiveUserStatus(tempuserID, team_view["team_0"]["id"].(string))
		}

		c.Data["Active_User_Status"] = activeUserBoolStatus

		teamCoCaptainStatus := false
		teamCaptainStatus := false

		if team_view["team_0"]["fk_captain_id"].(string) == tempuserID {
			teamCaptainStatus = true
		}

		if !teamCaptainStatus {
			for _, captain := range all_captains {

				if tempuserID == captain {
					teamCoCaptainStatus = true
					break
				}
			}
		}

		c.Data["TeamCoCaptainStatus"] = teamCoCaptainStatus

		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		c.Data["TeamCaptainId"] = team_view["team_0"]["fk_captain_id"].(string)

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *TeamController) TeamMemberInvite() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In TeamMemberInvite")

	if c.Ctx.Input.Method() == "POST" {
		/*var ctx *context.Context
		x := c.Ctx.Request
		fmt.Println(ctx.BeegoInput.IsPost())
		fmt.Println(x)
		c.Data["ajax"] = true
		fmt.Println(c.GetString("data"))
		c.TplName = "event/createEvent.html"*/
		fmt.Println("In post")

		var email_ids []string
		c.Ctx.Input.Bind(&email_ids, "email")

		message := c.GetString("message")
		fmt.Println(email_ids, message)

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetString("teamId")
		//result := "false"
		sent, alreadyInvited, alreadyMember, status := models.AddTeamMember(email_ids, message, user_id, team_id, false)

		outputObj := &mystruct{
			Sent:           sent,
			AlreadyInvited: alreadyInvited,
			AlreadyMember:  alreadyMember,
			Status:         status,
		}
		/*outputObj.Sent = sent
		outputObj.AlreadyInvited = alreadyInvited
		outputObj.AlreadyMember = alreadyMember
		outputObj.Atatus = status*/

		fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		fmt.Println(string(b))

		//fmt.Println(c.Ctx.Output.JSON(result, false, false))
		//c.Ctx.Output.JSON(outputObj, true, true)
		c.Ctx.Output.Body([]byte(string(b)))
	}
	//return &ClassDivs{"test", "ajax"}
	//c.AjaxResponse()

}

func (c *TeamController) RemoveTeamMembers() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In RemoveTeamMembers")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		var MemberIds []string
		c.Ctx.Input.Bind(&MemberIds, "ids")

		data := make(map[string][]string)

		data["MemberIds"] = MemberIds
		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result, last_captain := models.RemoveTeamMembers(data, team_id, user_id, removed_by)

		outputObj := &removed_member_ids{
			Fail:   last_captain,
			Status: result,
		}

		c.Ctx.Output.JSON(outputObj, false, false)

	}

}

func (c *TeamController) SaveTeamMembers() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In SaveTeamMembers")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		var Team_members_update []string
		c.Ctx.Input.Bind(&Team_members_update, "team_members_update")

		data := make(map[string][]string)

		data["Team_members_update"] = Team_members_update
		fmt.Println(Team_members_update)

		result := models.SaveTeamMembers(data)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) AcceptTeamMemberInvite() {
	c.TplName = "dashboard.html"
	fmt.Println("In AcceptTeamMemberInvite")

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetString("team_id")
		notf_id := c.GetString("notf_id")

		id := c.GetSession("UserId").(int)
		member_id := strconv.Itoa(id)

		result := models.AcceptTeamMember(team_id, member_id, notf_id)
		jobrunner.Now(WaiverSendOrResendForIndividualMember{member_id, team_id})
		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *TeamController) DeclineTeamMemberInvite() {
	c.TplName = "dashboard.html"
	fmt.Println("In DeclineTeamMemberInvite")

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetString("team_id")
		notf_id := c.GetString("notf_id")

		id := c.GetSession("UserId").(int)
		member_id := strconv.Itoa(id)

		result := models.DeclineTeamMember(team_id, member_id, notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) AddTeamPractice() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddTeamPractice")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamPracticeValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["PracticeDate"] = items.PracticeDate
		fmt.Println(items.PracticeDate)
		fmt.Println(items.PracticeTime)
		data["PracticeLength"] = items.PracticeLength
		data["PracticeTime"] = items.PracticeTime
		data["PracticeLocation"] = items.PracticeLocation
		data["PracticeRepeats"] = items.PracticeRepeats
		data["LocationTimeZone"] = items.LocationTimeZone
		data["PracticeDateTimeZone"] = items.PracticeDateTimeZone
		data["PracticeEndDateTimeZone"] = items.PracticeEndDateTimeZone
		if items.Type == "1" {
			data["Type"] = "team"
		} else {
			data["Type"] = items.Type
		}

		data["PracticeNote"] = items.PracticeNote

		data["PracticeEndDate"] = items.PracticeEndDate
		data["PracticeType"] = items.PracticeType

		c.SetSession("PracticeDate", items.PracticeDate)
		c.SetSession("PracticeTime", items.PracticeTime)

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AddTeamPracticeSchedule(data, user_id, team_id)

		c.Ctx.Output.Body([]byte(result))

	}
	//return &ClassDivs{"test", "ajax"}
	//c.AjaxResponse()

}

func (c *TeamController) EditTeamPractice() {
	c.TplName = "team/teamView.html"
	fmt.Println("In EditTeamPractice")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamPracticeValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["PracticeDate"] = items.PracticeDate
		data["PracticeLength"] = items.PracticeLength
		data["PracticeTime"] = items.PracticeTime
		data["PracticeLocation"] = items.PracticeLocation
		data["PracticeRepeats"] = items.PracticeRepeats
		data["PracticeDateTimeZone"] = items.PracticeDateTimeZone
		data["LocationTimeZone"] = items.LocationTimeZone

		if items.Type == "1" {
			data["Type"] = "team"
		} else {
			data["Type"] = items.Type
		}

		data["PracticeNote"] = items.PracticeNote

		data["PracticeId"] = items.Id
		data["EditOption"] = items.EditOption
		data["PrevPracticeRepeats"] = items.PrevPracticeRepeats

		data["PracticeEndDate"] = items.PracticeEndDateTimeZone
		data["PracticeType"] = items.EditPracticeType

		data["PracticeType_During_Old_Practice"] = c.GetString("editscheduleTypeHidden")

		c.SetSession("PracticeDate", items.PracticeDate)
		c.SetSession("PracticeTime", items.PracticeTime)

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		localUserTimeZone := GetTimezoneInSessionTeam(c)
		result := models.EditTeamPracticeSchedule(data, user_id, team_id, localUserTimeZone)

		c.Ctx.Output.Body([]byte(result))

	}
	//return &ClassDivs{"test", "ajax"}
	//c.AjaxResponse()

}

func (c *TeamController) SetPracticeAvailability() {
	c.TplName = "team/teamView.html"
	fmt.Println("In SetPracticeAvailability")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := availabilityPracticeValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["PracticeId"] = items.PracticeId
		data["Response"] = items.Response
		data["Reason"] = items.Reason
		data["CurrentDateTime"] = items.CurrentDateTime

		id := c.GetSession("UserId").(int)
		userID := strconv.Itoa(id)

		result := models.SetPracticeAvailability(data, userID)

		c.Ctx.Output.Body([]byte(result))

	}
	//return &ClassDivs{"test", "ajax"}
	//c.AjaxResponse()

}

func (c *TeamController) GetPracticeAttendees() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetPracticeAttendees")

	if c.Ctx.Input.Method() == "POST" {

		practice_id := c.GetString("practice_id")

		all, attending, not_attending, maybe, not_responded, status, p_date, p_time, p_duration, p_location, p_last_reminder, p_sms_last_reminder, isOldPractice := models.GetPracticeAttendees(practice_id)

		outputObj := &attendee_struct{
			All:             all,
			Attending:       attending,
			NotAttending:    not_attending,
			Maybe:           maybe,
			NotResponded:    not_responded,
			Status:          status,
			Date:            p_date,
			Time:            p_time,
			Duration:        p_duration,
			Location:        p_location,
			LastReminder:    p_last_reminder,
			SmsLastReminder: p_sms_last_reminder,
			IsOldPractice:   isOldPractice,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println(string(b))
		c.Ctx.Output.Body([]byte(string(b)))
	}

}

func (c *TeamController) MarkPracticeAttendance() {
	c.TplName = "team/teamView.html"
	fmt.Println("In MarkPracticeAttendance")

	if c.Ctx.Input.Method() == "POST" {

		var Attending []string
		var NotAttending []string
		var Maybe []string
		var NoResponse []string

		c.Ctx.Input.Bind(&Attending, "attending")
		c.Ctx.Input.Bind(&NotAttending, "not_attending")
		c.Ctx.Input.Bind(&Maybe, "maybe")
		c.Ctx.Input.Bind(&NoResponse, "no_response")

		practice_id := c.GetString("practice_id")

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.MarkPracticeAttendance(Attending, NotAttending, Maybe, NoResponse, practice_id, team_id, user_id)

		//fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}
}

func (c *TeamController) SendPracticeReminder() {
	c.TplName = "team/teamView.html"
	fmt.Println("In SendPracticeReminder")

	if c.Ctx.Input.Method() == "POST" {

		practice_id := c.GetString("practice_id")

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		communicationChannel := c.GetString("communication_channel")
		fmt.Println(communicationChannel)
		superdmin := "true"
		if c.GetSession("SuperAdmin") != "true" {
			superdmin = "false"
		}
		result := models.SendPracticeReminder(practice_id, team_id, user_id, communicationChannel, superdmin)

		b, _ := json.Marshal(result)
		fmt.Println(string(b))

		c.Ctx.Output.Body([]byte(string(b)))

	}
}

func (c *TeamController) UploadTeamImg() {
	c.TplName = "user/profileSettings.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetSession("TeamId").(string)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			result := models.SaveTeamUrl(team_id, path, header.Filename)
			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *TeamController) AddTeamDiscussion() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddTeamDiscussion")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamDiscussionValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["DiscussionMsg"] = items.DiscussionMsg
		data["PlainDiscussionMsg"] = items.PlainDiscussionMsg
		fmt.Println(items.PlainDiscussionMsg)
		data["Type"] = items.Type
		data["SendAs"] = items.SendAs
		data["SendAsSms"] = items.SendAsSms

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AddTeamDiscussion(data, team_id, user_id)

		b, _ := json.Marshal(result)
		fmt.Println(string(b))

		c.Ctx.Output.Body([]byte(string(b)))
	}

}

func (c *TeamController) CheckTeamSMSLimit() {
	fmt.Println("In CheckTeamSMSLimit")
	team_id := c.GetSession("TeamId").(string)
	result, sms_limits_subscription := models.CheckTeamSMSLimit(team_id)

	fmt.Println(result)
	smsLimt := ""
	free_trial := "false"
	premium_subscription := "false"

	if result["0"]["sms_limits"] != nil {
		tempteam_sms_limits, _ := strconv.Atoi(result["0"]["sms_limits"].(string))
		tempteam_sms_limits1, _ := strconv.Atoi(sms_limits_subscription)
		smsLimt = strconv.Itoa(tempteam_sms_limits + tempteam_sms_limits1)
	}

	if result["0"]["free_trial"] != nil {
		free_trial = result["0"]["free_trial"].(string)
	}

	if result["0"]["subscription_on_off"] != nil {
		premium_subscription = result["0"]["subscription_on_off"].(string)
	}
	outputObj := &teamSMSLimit{
		MembershipLevel:            result["0"]["membership_level"].(string),
		SMSLimit:                   smsLimt,
		DefaultSmsAvailableForTeam: constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM,
		Free_Trial_Status:          free_trial,
		Premium_Subscription:       premium_subscription,
	}

	// fmt.Println(outputObj)
	// b, _ := json.Marshal(outputObj)
	// fmt.Println(string(b))

	// c.Ctx.Output.Body([]byte(string(b)))

	c.Ctx.Output.JSON(outputObj, false, false)
}

func (c *TeamController) AddTeamDiscussionReply() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddTeamDiscussionReply")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamDiscussionReplyValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			c.Ctx.Output.Body([]byte("false"))
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["ReplyMsg"] = items.ReplyMsg
		data["Type"] = items.Type
		data["DisId"] = items.DisId

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AddTeamDiscussionReply(data, team_id, user_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) DeleteTeam() {
	c.TplName = "event/eventList.html"
	fmt.Println("In DeleteEvent")

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetString("team_id")
		fmt.Println(team_id)

		result := models.DeleteTeam(team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) UploadTeamSponsor() {
	c.TplName = "team/teamEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetSession("TeamId").(string)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
			c.Ctx.Output.Body([]byte("bad response"))
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			result := models.SaveTeamSponsorUrl(team_id, path, header.Filename)
			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *TeamController) RemoveTeamSponsor() {
	c.TplName = "team/teamEdit.html"
	fmt.Println("In RemoveTeamSponsor")

	if c.Ctx.Input.Method() == "POST" {

		var SponsorIds []string
		c.Ctx.Input.Bind(&SponsorIds, "ids")

		fmt.Println(SponsorIds)

		data := make(map[string][]string)

		data["SponsorIds"] = SponsorIds
		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result := models.RemoveTeamSponsor(data, team_id, user_id, removed_by)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) MakeCoCaptain() {
	c.TplName = "event/eventView.html"
	fmt.Println("In MakeCoCaptain")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		co_captain_id := c.GetString("co_captain_id")

		//id := c.GetSession("UserId").(int)
		//user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)
		result := models.AddTeamCoCaptain(co_captain_id, team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) RemoveCoCaptain() {
	c.TplName = "event/eventView.html"
	fmt.Println("In MakeCoCaptain")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		co_captain_id := c.GetString("co_captain_id")

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)
		result := models.RemoveTeamCoCaptain(co_captain_id, user_id, team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) UpdateTeamClasses() {
	c.TplName = "team/teamEdit.html"
	fmt.Println("In UpdateEventClasses")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		flash := beego.NewFlash()
		items := updateClassesValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)
		classes := ""
		div_string := ""
		if len(items.Mdivision) == 0 {
			classes = classes
			data["Mdivision"] = ""
		} else {
			div_string = strings.Join(items.Mdivision[:], ",")
			data["Mdivision"] = div_string + data["Mdivision"]
			classes = classes + "M"
		}

		if len(items.Wdivision) == 0 {
			classes = classes
			data["Wdivision"] = ""
		} else {
			div_string = strings.Join(items.Wdivision[:], ",")
			data["Wdivision"] = div_string + data["Wdivision"]
			classes = classes + ",W"
		}

		if len(items.MXdivision) == 0 {
			classes = classes
			data["MXdivision"] = ""
		} else {
			div_string = strings.Join(items.MXdivision[:], ",")
			data["MXdivision"] = div_string + data["MXdivision"]
			classes = classes + ",MX"
		}

		fmt.Println(classes)

		data["Mdivision"] = "{" + data["Mdivision"] + "}"
		data["Wdivision"] = "{" + data["Wdivision"] + "}"
		data["MXdivision"] = "{" + data["MXdivision"] + "}"

		fmt.Println(data)

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)

		result := models.EditTeamClasses(data, user_id, team_id, items.Mdivision, items.Wdivision, items.MXdivision)

		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) UploadTeamNote() {
	c.TplName = "team/teamEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetSession("TeamId").(string)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/team_files/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
			c.Ctx.Output.Body([]byte("bad response"))
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			id := c.GetSession("UserId").(int)
			user_id := strconv.Itoa(id)

			user_details := models.GetUserDetails(user_id)
			user_name := ""

			if user_details["user"]["name"] != nil {
				user_name = user_details["user"]["name"].(string)
			}
			if user_details["user"]["last_name"] != nil {
				user_name = user_name + " " + user_details["user"]["last_name"].(string)
			}

			result := models.SaveTeamNoteUrl(team_id, user_id, user_name, path, header.Filename)
			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *TeamController) RemoveTeamNote() {
	c.TplName = "team/teamEdit.html"
	fmt.Println("In RemoveTeamNote")

	if c.Ctx.Input.Method() == "POST" {

		var FileIds []string
		c.Ctx.Input.Bind(&FileIds, "ids")

		data := make(map[string][]string)

		data["FileIds"] = FileIds
		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result := models.RemoveTeamNote(data, team_id, user_id, removed_by)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) RemoveTeamPractice() {
	c.TplName = "team/teamView.html"
	fmt.Println("In RemoveTeamPractice")

	if c.Ctx.Input.Method() == "POST" {

		practice_id := c.GetString("practice_id")
		delete_option := c.GetString("delete_option")
		repeats := c.GetString("repeats")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)

		result := models.RemoveTeamPractice(practice_id, user_id, delete_option, repeats, team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) SendEmailMessage() {
	c.TplName = "team/teamView.html"
	fmt.Println("In SendEmailMessage")

	if c.Ctx.Input.Method() == "POST" {

		member_id := c.GetString("member_id")
		msg := c.GetString("msg")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)

		result := models.SendEmailMessage(member_id, msg, user_id, team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) TeamJoinRequest() {
	c.TplName = "team/teamView.html"
	fmt.Println("In TeamJoinRequest")

	if c.Ctx.Input.Method() == "POST" {

		msg := c.GetString("msg")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)

		result := models.SendTeamJoinRequest(msg, user_id, team_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) AcceptTeamJoinRequest() {
	c.TplName = "team/teamView.html"
	fmt.Println("In TeamJoinRequest")

	if c.Ctx.Input.Method() == "POST" {

		team_id := c.GetString("team_id")
		notf_id := c.GetString("notf_id")
		member_id := c.GetString("member_id")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AcceptTeamJoinRequest(user_id, team_id, notf_id, member_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) CheckTeamName() {
	c.TplName = "dashboard.html"
	fmt.Println("In CheckTeamName")

	if c.Ctx.Input.Method() == "POST" {

		team_name := c.GetString("team_name")
		//location := c.GetString("location")
		caller := c.GetString("caller")

		//slug := strings.Replace(strings.ToLower(team_name), " ", "-", -1) + "-" + strings.Replace(strings.ToLower(location), " ", "-", -1)

		result := models.CheckTeamName(team_name)

		fmt.Println(result)

		if result != "" {

			if caller == "edit" {
				team_id := c.GetSession("TeamId").(string)
				if result != team_id {
					c.Ctx.Output.Body([]byte("exists"))
				} else {
					c.Ctx.Output.Body([]byte(""))
				}

			} else {
				c.Ctx.Output.Body([]byte("exists"))
			}

		} else {
			c.Ctx.Output.Body([]byte(""))
		}

	}
}

func (c *TeamController) GetPendingReqEmail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetPendingReqEmail")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		var member_ids []string
		c.Ctx.Input.Bind(&member_ids, "ids")

		team_id := c.GetSession("TeamId").(string)
		//result := "false"
		result := models.GetPendingReqEmail(member_ids, team_id)

		outputObj := &email_ids{
			Pending_emails: result,
		}

		fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		fmt.Println(string(b))

		c.Ctx.Output.Body([]byte(string(b)))
	}

}

func (c *TeamController) TeamPracticeInfo() {
	c.TplName = "team/teamView.html"
	fmt.Println("In TeamPracticeInfo")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		practice_id := c.GetString("practice_id")
		/*id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)*/

		loginUserTimeZone := GetTimezoneInSessionTeam(c)
		result, isOldPractice := models.TeamPracticeInfo(practice_id, loginUserTimeZone)
		note := ""
		if result[0]["note"] != nil {
			note = result[0]["note"].(string)
		}

		repeats := ""
		if result[0]["repeats"] != nil {
			repeats = result[0]["repeats"].(string)
		}

		notify_type := ""
		if result[0]["type"] != nil {
			notify_type = result[0]["type"].(string)
		}

		user_time_zone := ""

		if result[0]["user_time_zone"] != nil {
			user_time_zone = result[0]["user_time_zone"].(string)
		}

		practice_end_date_format := ""

		if result[0]["practice_end_date_format"] != nil {
			practice_end_date_format = result[0]["practice_end_date_format"].(string)
		}

		outputObj := &practice_info{
			Id:                   result[0]["id"].(string),
			Date:                 result[0]["practice_date_format"].(string),
			Date_2:               result[0]["practice_date_format_2"].(string),
			Time:                 result[0]["practice_time"].(string),
			Length:               result[0]["practice_length"].(string),
			Location:             result[0]["practice_location"].(string),
			Note:                 note,
			Repeats:              repeats,
			Notify:               notify_type,
			PracticeDateTimeZone: user_time_zone,
			PracticeEndDate:      practice_end_date_format,
			PracticeType:         result[0]["team_practices_type"].(string),
			IsOldPractice:        isOldPractice,
		}

		fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		fmt.Println(string(b))

		c.Ctx.Output.Body([]byte(string(b)))
	}

}

func (c *TeamController) RemoveTeamWaiver() {
	c.TplName = "team/teamView.html"
	fmt.Println("In RemoveTeamWaiver")

	if c.Ctx.Input.Method() == "POST" {

		team_waiver_id := c.GetString("team_waiver_id")

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result := models.RemoveTeamWaiver(team_waiver_id, removed_by)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) ResendTeamInvite() {
	c.TplName = "team/teamView.html"
	fmt.Println("In ResendTeamInvite")

	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		var member_ids []string
		c.Ctx.Input.Bind(&member_ids, "ids")

		msg := c.GetString("msg")

		team_id := c.GetSession("TeamId").(string)

		if c.GetString("teamid") != "" {
			team_id = c.GetString("teamid")
		}
		//result := "false"
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.ResendTeamInvite(member_ids, team_id, user_id, msg)

		/*outputObj := &email_ids{
			Pending_emails: result,
		}

		fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		fmt.Println(string(b))*/

		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *TeamController) DeclineTeamJoinRequest() {
	c.TplName = "team/teamView.html"
	fmt.Println("In DeclineTeamJoinRequest")

	if c.Ctx.Input.Method() == "POST" {

		member_id := c.GetString("member_id")
		team_id := c.GetString("team_id")
		notf_id := c.GetString("notf_id")

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.DeclineTeamJoinRequest(user_id, member_id, team_id, notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) AddTeamLinks() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddTeamLinks")

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := teamLinksValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		data["Link"] = items.Link
		data["LinkTitle"] = items.LinkTitle

		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AddTeamLinks(data, user_id, team_id)

		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *TeamController) RemoveTeamLinks() {
	c.TplName = "team/teamEdit.html"
	fmt.Println("In RemoveTeamLinks")

	if c.Ctx.Input.Method() == "POST" {

		var LinkIds []string
		c.Ctx.Input.Bind(&LinkIds, "ids")

		data := make(map[string][]string)

		data["LinkIds"] = LinkIds
		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result := models.RemoveTeamLinks(data, team_id, user_id, removed_by)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func GetTimezoneInSessionTeam(c *TeamController) string {

	if c.GetSession("localUserTimeZone") != nil {
		return c.GetSession("localUserTimeZone").(string)
	} else {
		t := time.Now()
		name, _ := t.Zone()
		return name
	}

	return ""
}

func (c *TeamController) GetTwilioWebhookRequest() {
	fmt.Println("In getTwilioWebhookRequest")
	fromNumber := c.GetString("From")
	toNumber := c.GetString("To")
	bodyData := c.GetString("Body")

	logs.SetLogger("file", `{"filename":"test.log"}`)

	logs.Info(time.Now())
	logs.Trace(c.Input())

	models.AttendenceMarkUpCodeTemp(fromNumber, toNumber, bodyData)
	fmt.Println("Out getTwilioWebhookRequest")

	c.Ctx.Output.Body([]byte(""))

}

func (c *TeamController) SendMessagesToTeam() {
	c.TplName = "team/teamView.html"
	fmt.Println("In SendMessagesToTeam")

	if c.Ctx.Input.Method() == "POST" {
		var member_idsMapData []string

		member_ids := c.GetString("member_id")

		member_idsMapData = strings.Split(member_ids, ",")

		msg := c.GetString("msg")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		team_id := c.GetSession("TeamId").(string)
		toEmailIds := models.GetUserEmailList(member_idsMapData)
		result := "false"
		if len(toEmailIds) > 0 {
			//justString := strings.Join(toEmailIds, ",")
			result = models.SendMessagesToTeam(member_ids, toEmailIds, msg, user_id, team_id)
		}
		c.Ctx.Output.Body([]byte(result))

	}

}

/*This controller get the upcomming Practice Or Event depends on team Id*/
func (c *TeamController) GetRegisterEventOrCreatedUpcommingPractice() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetRegisterEventOrCreatedUpcommingPractice")

	if c.Ctx.Input.Method() == "GET" {
		//optionType := c.GetString("optionType")
		teamId := c.GetSession("TeamId").(string)
		localUserTimeZone := GetTimezoneInSessionTeam(c)
		result := models.GetCreatedUpcommingPractice(localUserTimeZone, teamId)
		c.Ctx.Output.JSON(result, false, false)
		fmt.Println("Out GetRegisterEventOrCreatedUpcommingPractice")
	}

}

/*This controller get the practice attendence detail of the individual practice id with all response*/
func (c *TeamController) GetPracticeAttedenceDetail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetPracticeAttedenceDetail")

	if c.Ctx.Input.Method() == "GET" {
		//var allAttendence []orm.Params

		practice_id := c.GetString("practice_id")
		teamId := c.GetSession("TeamId").(string)

		all, not_responded := models.GetIndividualPracticeAttendeesDetail(practice_id, teamId)

		outputObj := &rosterattendee_struct{
			All:          all,
			NotResponded: not_responded,
		}
		c.Ctx.Output.JSON(outputObj, false, false)
		fmt.Println("Out GetPracticeAttedenceDetail")
	}

}

// This controller save the roaster detail
func (c *TeamController) AddEditRoster() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddRoster")
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := rosterValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		data := make(map[string]string)
		data["NewRosterName"] = items.NewRosterName
		data["RosterType"] = items.RosterType
		data["RosterEventId"] = items.RosterEventId
		data["RosterPracticeId"] = items.RosterPracticeId
		data["RosterComment"] = items.RosterComment
		if items.SaveDraft == "on" {
			data["SaveDraft"] = "1"
		} else {
			data["SaveDraft"] = "0"
		}

		teamRosterMembersstring := strings.Join(items.TeamRosterMembers[:], ",")
		teamRosterMembersArray := items.TeamRosterMembers
		teamRosterMembersRoleArray := strings.Split(items.TeamRosterMembersRole, ",")
		teamId := items.CurrentTeamId
		rosterupdateid := c.GetString("rosterupdateid")
		if rosterupdateid == "" {
			localUserTimeZone := GetTimezoneInSessionTeam(c)
			result, rosterSubmitStatus := models.CreateRoster(data, teamId, teamRosterMembersstring, localUserTimeZone, teamRosterMembersArray, teamRosterMembersRoleArray)
			outputObj := &create_roster_struct{
				RosterData:         result,
				RosterSubmitStatus: rosterSubmitStatus,
			}
			c.Ctx.Output.JSON(outputObj, false, false)
		} else {
			if rosterupdateid != "" {
				data["RosterUpdateId"] = rosterupdateid
				result := models.UpdateRoster(data, teamId, teamRosterMembersstring, teamRosterMembersArray, teamRosterMembersRoleArray)
				b, _ := json.Marshal(result)
				c.Ctx.Output.Body([]byte(string(b)))
			}
		}

		fmt.Println("Out AddRoster")
	}
}

func (c *TeamController) GetIndividualRosterDetail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetIndividualRosterDetail")
	if c.Ctx.Input.Method() == "GET" {
		rosterId := c.GetString("rosterId")

		result, rosterMemberRoleData, rosterSubmitstatus := models.GetIndividualRosterDetail(rosterId)
		outputObj := &get_roster_detail_struct{
			RosterDetailData:     result,
			RosterMemberRoleData: rosterMemberRoleData,
			RosterSubmitStatus:   rosterSubmitstatus,
		}

		c.Ctx.Output.JSON(outputObj, false, false)
		fmt.Println("Out GetIndividualRosterDetail")
	}
}

func (c *TeamController) CheckRosterName() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CheckRosterName")
	if c.Ctx.Input.Method() == "GET" {
		rosterName := c.GetString("rosterName")
		teamId := c.GetSession("TeamId").(string)
		result := models.CheckRosterName(rosterName, teamId)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		fmt.Println("Out CheckRosterName")
	}
}

func (c *TeamController) DeleteRosterName() {
	c.TplName = "team/teamView.html"
	fmt.Println("In DeleteRosterName")
	if c.Ctx.Input.Method() == "POST" {
		rosterid := c.GetString("rosterid")
		result := models.DeleteRosterName(rosterid)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		fmt.Println("Out DeleteRosterName")
	}
}

func (c *TeamController) GetRosterUserDetail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetRosterUserDetail")
	if c.Ctx.Input.Method() == "GET" {
		rosterId := c.GetString("rosterId")
		result := models.GetRosterUserDetail(rosterId)
		c.Ctx.Output.JSON(result, false, false)
		fmt.Println("Out GetRosterUserDetail")
	}
}

func (c *TeamController) GetRosterDetailList() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetRosterDetailList")
	if c.Ctx.Input.Method() == "GET" {
		teamId := c.GetSession("TeamId").(string)
		team_Roaster := models.GetRosterDetailList(teamId)
		c.Ctx.Output.JSON(team_Roaster, false, false)
		fmt.Println("Out GetRosterDetailList")
	}
}

func (c *TeamController) CreateLineup() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CreateRoster")
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := lineUpValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		data := make(map[string]string)
		data["LineupName"] = items.LineupName
		data["RosterId"] = items.RosterId
		if items.SaveDraft == "on" {
			data["SaveDraft"] = "1"
		} else {
			data["SaveDraft"] = "0"
		}
		leftPadllerMembers := strings.Join(items.LeftPadllerMembers[:], ",")
		rightPadllerMembers := strings.Join(items.RightPadllerMembers[:], ",")
		otherMembers := strings.Join(items.OtherMembers[:], ",")
		selectedLeftPaddlers := strings.Join(items.SelectedLeftPaddlers[:], ",")
		selectedRightPaddlers := strings.Join(items.SelectedRightPaddlers[:], ",")
		selectedDrummers := strings.Join(items.SelectedDrummers[:], ",")
		selectedSpares := strings.Join(items.SelectedSpares[:], ",")

		teamId := items.CurrentTeamId
		lineupUpdateId := c.GetString("lineupUpdateId")
		if lineupUpdateId == "" {
			result := models.CreateLineup(data, teamId, leftPadllerMembers, rightPadllerMembers, otherMembers, selectedLeftPaddlers, selectedRightPaddlers, selectedDrummers, selectedSpares)
			c.Ctx.Output.Body([]byte(result))
		} else if lineupUpdateId != "" {
			data["LineupUpdateId"] = lineupUpdateId
			result := models.UpdateLineup(data, teamId, leftPadllerMembers, rightPadllerMembers, otherMembers, selectedLeftPaddlers, selectedRightPaddlers, selectedDrummers, selectedSpares)
			b, _ := json.Marshal(result)
			c.Ctx.Output.Body([]byte(string(b)))
		}
	}

	fmt.Println("Out CreateRoster")
}

func (c *TeamController) CheckLineUpName() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CheckLineUpName")
	if c.Ctx.Input.Method() == "GET" {
		lineupName := c.GetString("lineupname")
		teamId := c.GetSession("TeamId").(string)
		result := models.CheckLineUpName(lineupName, teamId)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		fmt.Println("Out CheckLineUpName")
	}
}

func (c *TeamController) GetLineUpIndividualDetail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetLineUpIndividualDetail")
	if c.Ctx.Input.Method() == "GET" {
		lineup_id := c.GetString("lineup_id")
		roster_id := c.GetString("roster_id")
		lineupData, roosterUserData := models.GetLineUpIndividualDetail(lineup_id, roster_id)

		outputObj := &lineUpDetail_struct{
			LineupData:     lineupData,
			RosterUserData: roosterUserData,
		}
		c.Ctx.Output.JSON(outputObj, false, false)
		fmt.Println("Out GetLineUpIndividualDetail")
	}
}

func (c *TeamController) DeleteLineUp() {
	c.TplName = "team/teamView.html"
	fmt.Println("In DeleteLineUp")
	if c.Ctx.Input.Method() == "POST" {
		lineupid := c.GetString("lineupid")
		result := models.DeleteLineUp(lineupid)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		fmt.Println("Out DeleteLineUp")
	}
}

func (c *TeamController) GetEventListAutoComplete() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetEventListAutoComplete")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		searchText := c.GetString("searchText")
		result := models.GetEventListAutoComplete(searchText)
		c.Ctx.Output.JSON(result, false, false)
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetEventListAutoComplete")
	}
}

func (c *TeamController) CheckRosterRegisterWithEvent() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CheckRosterRegisterWithEvent")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		rosterid := c.GetString("rosterid")
		result := models.CheckRosterRegisterWithEvent(rosterid)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out CheckRosterRegisterWithEvent")
	}
}

func (c *TeamController) CheckCaptainSubscriptionOrNot() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CheckCaptainSubscriptionOrNot")
	if c.Ctx.Input.Method() == "POST" {
		captainID := c.GetString("captainId")
		teamID := c.GetSession("TeamId").(string)
		result := models.CheckCaptainSubscriptionOrNot(captainID, teamID)
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out CheckCaptainSubscriptionOrNot")
	}
}

func (c *TeamController) TeamRemoveCaptain() {
	c.TplName = "team/teamView.html"
	fmt.Println("In TeamRemoveCaptain")
	if c.Ctx.Input.Method() == "POST" {
		captainID := c.GetString("captainId")
		teamID := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		userID := strconv.Itoa(id)
		result := models.TeamRemoveCaptain(captainID, teamID, userID)
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out TeamRemoveCaptain")
	}
}

func (c *TeamController) AddTeamCaptainByAdmin() {
	c.TplName = "team/teamView.html"
	fmt.Println("In AddTeamCaptainByAdmin")
	if c.Ctx.Input.Method() == "POST" {
		result := "false"
		selectedcaptainID := c.GetString("captainId")
		teamID := c.GetSession("TeamId").(string)
		resultAdmin := models.CancelOldCaptainSubscription(teamID)
		if resultAdmin == "true" {
			result = models.TeamRemoveCaptain("0", teamID, selectedcaptainID)
		}
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out AddTeamCaptainByAdmin")
	}
}

func (c *TeamController) CheckSmsCount() {
	c.TplName = "team/teamView.html"
	fmt.Println("In CheckSmsCount")
	if c.Ctx.Input.Method() == "GET" {
		teamID := c.GetSession("TeamId").(string)
		result, popupstatus := models.GetSmsCount(teamID)
		smsCountTemp := strconv.Itoa(result)

		outputObj := &sms_subscription_count{
			PopUpStatus: popupstatus,
			SMSCount:    smsCountTemp,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(string(b)))
		fmt.Println("Out CheckSmsCount")
	}
}

func (c *TeamController) InactiveSmsSubscriptionPopup() {
	c.TplName = "team/teamView.html"
	fmt.Println("In InactiveSmsSubscriptionPopup")
	if c.Ctx.Input.Method() == "POST" {
		teamID := c.GetSession("TeamId").(string)
		result := models.InactiveSmsSubscriptionPopup(teamID)
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out InactiveSmsSubscriptionPopup")
	}
}

func (c *TeamController) InactiveFreeTrialSubscriptionPopup() {
	c.TplName = "team/teamView.html"
	fmt.Println("In InactiveFreeTrialSubscriptionPopup")
	if c.Ctx.Input.Method() == "POST" {
		teamID := c.GetString("currentTeamId")
		status := c.GetString("status")
		result := models.InactiveFreeTrialSubscriptionPopup(teamID, status)
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out InactiveFreeTrialSubscriptionPopup")
	}
}

func (c *TeamController) ActiveFreeTrialForExistingUsers() {
	result := models.ActiveFreeTrialForExistingUsers()
	c.Ctx.Output.Body([]byte(result))
}

func (c *TeamController) MarkPracticeAttendanceViaEmail() {
	teamID := c.Ctx.Input.Param(":team_id")
	practiceID := c.Ctx.Input.Param(":practice_id")
	user_Id := c.Ctx.Input.Param(":user_id")
	responseText := c.Ctx.Input.Param(":response_text")

	result := models.MarkPracticeAttendanceViaEmail(teamID, practiceID, user_Id, responseText)
	if result == "success" {
		result = "Thank you for submitting your availability, it has been updated in Gushou."
	}
	c.Ctx.Output.JSON(result, false, false)
}

func (c *TeamController) InactiveBasicSubscriptionPopup() {
	c.TplName = "team/teamView.html"
	fmt.Println("In InactiveBasicSubscriptionPopup")
	if c.Ctx.Input.Method() == "POST" {
		teamID := c.GetString("currentTeamId")
		status := c.GetString("status")
		result := models.InactiveBasicSubscriptionPopup(teamID, status)
		c.Ctx.Output.Body([]byte(result))
		fmt.Println("Out InactiveBasicSubscriptionPopup")
	}
}

func (c *TeamController) GetLaterSubmitRosterDetail() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetLaterSubmitRosterDetail")
	if c.Ctx.Input.Method() == "GET" {
		regeventid := c.GetString("regeventid")

		registerLaterClassDivisionList := models.GetEventRegisterClassDivisionByRegisterEventId(regeventid)

		regteamDetail := models.GetRegisterEventDetailByRegEventId(regeventid)

		teamroosterlist := models.GetTeamRoosterListByTeamId(regteamDetail[0]["fk_team_id"].(string), regeventid)

		//registerRosterDetailList := models.GetRegisterRosterDetails(regeventid)

		outputObj := &submit_later_roster_detail{
			RegisterLaterClassDivisionList: registerLaterClassDivisionList,
			TeamRoosterList:                teamroosterlist,
		}

		c.Ctx.Output.JSON(outputObj, false, false)
		fmt.Println("Out GetLaterSubmitRosterDetail")
	}
}

func (c *TeamController) GetEachTeamPaymentTrackingDetailData() {
	c.TplName = "team/teamPaymentTracking.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Team Payment tracking"

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	regpayid := c.Ctx.Input.Param(":regpayid")

	if regpayid != "" {

		fmt.Println(regpayid)

		regpaymentdetail := models.GetEventRegisterTeamPaymentDetail(regpayid)

		if regpaymentdetail != nil {
			tempTeamId := ""
			if regpaymentdetail[0]["fk_team_id"] != nil {
				tempTeamId = regpaymentdetail[0]["fk_team_id"].(string)
			}

			slug := models.GetTeamSlugNameByTeamId(tempTeamId)
			c.Data["teamSlug"] = slug

			registerTeamDetail := models.GetRegisterTeamDetailByTeamId(tempTeamId)
			c.Data["RegisterTeamDetail"] = registerTeamDetail

			tempPaymentTrackingDetail := models.GetPaymentTrackingDetails(regpayid)

			if tempPaymentTrackingDetail != nil {
				if tempPaymentTrackingDetail[0]["currency_unit"] != nil {
					c.Data["PaymentCurrencySymbol"] = common.ConvertUnitToCurrencySynbol(tempPaymentTrackingDetail[0]["currency_unit"].(string))
				}
			}
			c.Data["PaymentTrackingDetails"] = tempPaymentTrackingDetail

		} else {
			fmt.Println(" No TeamPaymentTracking event by the slug")
			c.Redirect("/", 302)
			return
		}

		if models.IsLoggedIn(c.GetSession("GushouSession")) {
			c.Data["IsLoggedIn"] = 1
			c.Data["FirstName"] = c.GetSession("FirstName")
			c.Data["LastName"] = c.GetSession("LastName")
			c.Data["UserId"] = c.GetSession("UserId")
		} else {
			c.Data["IsLoggedIn"] = 0
		}
	} else {
		fmt.Println(" No TeamPaymentTracking event by the slug")
		c.Redirect("/", 302)
		return
	}
}
