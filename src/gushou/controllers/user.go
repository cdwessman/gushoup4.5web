package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type UserController struct {
	beego.Controller
}

func (c *UserController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY
}

type infoValidate struct {
	Email      string   `form:"email" valid:"Email"`
	Password   string   `form:"password" valid:"Required"`
	Activity   []string `form:"activities[]" valid:"Required"`
	LoginEmail string   `form:"email" valid:"Email"`
}

type registerValidate struct {
	FirstName       string `form:"firstname" valid:"Required"`
	LastName        string `form:"lastName" valid:"Required"`
	DisplayName     string `form:"displayname"`
	DOB             string `form:"dob" valid:"Required"`
	Classes         string `form:"classes" valid:"Required"`
	Phone           string `form:"phone"`
	Country         string `form:"country" valid:"Required"`
	OtherCountry    string `form:"other_country"`
	Province        string `form:"province2"`
	Location        string `form:"location" valid:"Required"`
	PostalCode      string `form:"postal_code" valid:"Required"`
	Newsletters     string `form:"resgisterSubscribe"`
	CountryDialCode string `form:"dial_code"`
	DialCountryCode string `form:"dial_code_county"`
}

type editInfoValidate struct {
	FirstName        string   `form:"first_name" valid:"Required"`
	ShowFullName     string   `form:"showFullName" `
	LastName         string   `form:"last_name" valid:"Required"`
	PaddlingSide     string   `form:"paddling_side"`
	SkillLevel       string   `form:"skill_level" `
	Recruit          string   `form:"availableRecruit"`
	Activities       []string `form:"activities[]" valid:"Required"`
	About            string   `form:"personal_info" `
	Location         string   `form:"address" valid:"Required"`
	TshirtSize       string   `form:"tshirt_size"`
	DOB              string   `form:"dob" valid:"Required"`
	Weight           string   `form:"weight" `
	Phone            string   `form:"phone"`
	Classes          string   `form:"classes"`
	SubscribeDigest  string   `form:"unsubscribe"`
	SubscribeInstant string   `form:"unsub_instant"`
	Newsletters      string   `form:"unsub_news"`
	SubscribeSms     string   `form:"unsub_sms"`
	CountryDialCode  string   `form:"dial_code"`
	DialCountryCode  string   `form:"dial_code_county"`
}

type changePasswordValidate struct {
	CurrPassword string `form:"curr_password" valid:"Required"`
	NewPassword  string `form:"new_password" valid:"Required"`
	RePassword   string `form:"re_password" valid:"Required"`
}

func (c *UserController) GetUserName() {

	fmt.Println("SMS Test for Gushou...")
	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		if c.Ctx.Input.Method() == "POST" {

			name := "undefined"
			user_id := c.GetString("user_id")

			result := models.GetUserDetails(user_id)
			if result["user"]["name"] != nil {
				name = result["user"]["name"].(string)
			}

			fmt.Println(name)
			c.Ctx.Output.Body([]byte(name))
		}

	} else {
		c.Ctx.Output.Body([]byte("You must log in"))
	}
}

func (c *UserController) ProfileSetting() {
	c.TplName = "user/profileSettings.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.GetSession("UpdateSuccess") == "true" {
		c.Data["UpdateSuccess"] = true
		c.DelSession("UpdateSuccess")
	}

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}
	fmt.Println(c.Ctx.Input.Param(":paddleruserid"))
	paddleruserid := c.Ctx.Input.Param(":paddleruserid")
	user_id_string := ""
	c.Data["IsNotPaddlerUser"] = true
	if paddleruserid != "" {
		user_id_string = paddleruserid
		c.Data["Paddleruserid"] = paddleruserid
		c.Data["IsNotPaddlerUser"] = false
	} else {
		user_id := c.GetSession("UserId").(int)
		user_id_string = strconv.Itoa(user_id)
		c.Data["Paddleruserid"] = ""
	}

	var user_Subscription_Detail []orm.Params
	user_Subscription_Detail = models.GetUserSubscriptionDetail(user_id_string)
	c.Data["User_Subscription_Detail"] = user_Subscription_Detail

	result := make(map[string]orm.Params)
	result = models.GetUserDetails(user_id_string)

	var card_Detail []orm.Params
	card_Detail = models.GetCardDetails(user_id_string)
	c.Data["User_Card_Detail"] = card_Detail

	s := strings.Replace(result["user"]["activities"].(string), "{", "", -1)
	s = strings.Replace(s, "}", "", -1)
	fmt.Println(result)

	newsletter := models.IsSubscribed(result["user"]["email_address"].(string))

	if newsletter {
		c.Data["Newsletter"] = true
	}

	var array []string
	array = strings.Split(s, ",")
	//array :=[]string{"A","B","C","D","E"}
	c.Data["Activities"] = array

	if _, ok := result["status"]; !ok {
		c.Data["UserInfo"] = result
	}

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := editInfoValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		//result :=make(map[string]string)
		data := make(map[string]string)

		activity_string := strings.Join(items.Activities[:], ",")

		if c.GetSession("SuperAdmin") == "true" && paddleruserid == "" {
			activity_string = activity_string + ", Super Admin"
		} else {
			if strings.Contains(result["user"]["activities"].(string), "Super Admin") {
				activity_string = activity_string + ", Super Admin"
			}
		}

		data["Activities"] = "{" + activity_string + "}"

		data["FirstName"] = items.FirstName
		data["LastName"] = items.LastName

		if items.ShowFullName == "on" {
			data["ShowFullName"] = "1"
		} else {
			data["ShowFullName"] = "0"
		}

		data["PaddlingSide"] = items.PaddlingSide
		data["SkillLevel"] = items.SkillLevel

		if items.Recruit == "yes" {
			data["Recruit"] = "available"
		}

		data["About"] = items.About
		data["Location"] = items.Location
		data["TshirtSize"] = items.TshirtSize
		data["DOB"] = items.DOB
		data["Weight"] = items.Weight
		data["Phone"] = items.Phone
		data["CountryDialCode"] = items.CountryDialCode
		data["Classes"] = items.Classes
		data["DialCountryCode"] = items.DialCountryCode

		if items.SubscribeDigest == "1" {
			data["SubscribeDigest"] = "1"
		} else {
			data["SubscribeDigest"] = "0"
		}

		if items.SubscribeInstant == "1" {
			data["SubscribeInstant"] = "1"
		} else {
			data["SubscribeInstant"] = "0"
		}

		if items.Newsletters == "1" {
			data["Newsletters"] = "1"
		} else {
			data["Newsletters"] = "0"
		}

		if items.SubscribeSms == "1" {
			data["SubscribeSms"] = "1"
		} else {
			data["SubscribeSms"] = "0"
		}

		fmt.Printf("%+v\n", items)

		paddleruserid := c.Ctx.Input.Param(":paddleruserid")
		user_id := ""
		if paddleruserid != "" {
			user_id = paddleruserid
		} else {
			id := c.GetSession("UserId").(int)
			user_id = strconv.Itoa(id)
		}

		result := models.EditUserDetails(data, user_id)
		if result["status"] == "1" {
			editmember_details := models.GetUserDetails(user_id)
			teamManagmentStatus, eventorganisingStatus, paddlingStatus := checkUserRole(editmember_details["user"]["activities"].(string))
			c.SetSession("Team_Manage_Admin", teamManagmentStatus)
			c.SetSession("Event_Manage_Admin", eventorganisingStatus)
			c.SetSession("Paddling_Admin", paddlingStatus)
		}
		if paddleruserid == "" {
			if result["status"] == "1" {
				c.SetSession("UpdateSuccess", "true")
				c.SetSession("FirstName", items.FirstName)
				c.SetSession("LastName", items.LastName)
				c.Redirect("/profile-setting", 302)
			}
		} else {
			if result["status"] == "1" {
				c.SetSession("UpdateSuccess", "true")
				c.SetSession("FirstName", c.GetSession("FirstName"))
				c.SetSession("LastName", c.GetSession("LastName"))
				c.Redirect("/profile-setting/"+paddleruserid, 302)
			}
		}

		fmt.Println(result)

	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *UserController) ChangePassword() {
	c.TplName = "user/changePassword.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := changePasswordValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		fmt.Println(items.NewPassword, items.RePassword)

		if items.NewPassword == items.RePassword {
			id := c.GetSession("UserId").(int)
			user_id := strconv.Itoa(id)

			result := models.ChangePassword(user_id, items.CurrPassword, items.NewPassword)

			if result == "true" {
				c.Data["Success"] = true
			} else {
				c.Data["Fail"] = true
			}

		} else {
			fmt.Println("Your new password does not match your re-entered password")
		}

	}
	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *UserController) Register() {
	c.TplName = "user/register.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"
	teamJoinID := c.Ctx.Input.Param(":teamjoinid")
	token := c.Ctx.Input.Param(":token")

	if teamJoinID != "" && token != "" {
		// verify the link by token
		isVerified := models.VerifyTeamInvitationToken(token)
		fmt.Println("isVerified ", isVerified)

		if isVerified["is_verified"] == "true" {
			// get the user mail by team-join-id
			userEmail := models.GetEmailByTeamJoinId(teamJoinID)
			// check whether the user is existing user or not
			isRegisteredUser := models.IsRegisteredUserByEmail(userEmail)
			if isRegisteredUser {
				c.Redirect("/login", 302)
			} else {
				items := infoValidate{}
				items.Email = userEmail
				c.Data["FormItems"] = items
			}
		} else {
			fmt.Println(c.Ctx.Input.Method())
			if c.Ctx.Input.Method() != "POST" {
				c.TplName = "verifyTeamInvitationLink.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

				if isVerified["expired"] == "true" {
					c.Data["Expired"] = true
				} else if isVerified["invalid"] == "true" {
					c.Data["Invalid"] = true
				}
			}
		}
	}

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := infoValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}
		if teamJoinID != "" {
			items.LoginEmail = models.GetEmailByTeamJoinId(teamJoinID)
		}
		result := make(map[string]string)

		activity_string := strings.Join(items.Activity[:], ",")
		activity_string = "{" + activity_string + "}"
		result = models.AddUser(items.Email, items.Password, activity_string)

		if result["status"] == "1" {

			id, err := strconv.Atoi(result["user_id"])
			if err == nil {
				c.SetSession("UserId", id)
			}

			id, err = strconv.Atoi(result["profile_id"])
			if err == nil {
				c.SetSession("ProfileId", id)
			}

			fmt.Println(c.GetSession("UserId"))
			fmt.Println(c.GetSession("ProfileId"))

			//c.SetSession("verificationEmailSent", "true")

			// update the email id when the new user used different email
			if token != "" {
				isEmailUpdated := models.UpdateEmailForNewUser(items.Email, teamJoinID, items.LoginEmail)
				if isEmailUpdated {
					fmt.Println("user email updated successfully")
				} else {
					fmt.Println("user email not updated successfully")
				}
			}
			c.Redirect("/complete-register", 302)
		} else {
			c.Data["EmailExists"] = true
		}

	}
}

func (c *UserController) CompleteRegister() {
	c.TplName = "user/completeRegister.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	/* site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	} */

	if c.GetSession("UserId") != nil {
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		// if c.GetSession("verificationEmailSent") == "true" {
		// 	c.Data["verificationEmailSent"] = true
		// 	c.DelSession("verificationEmailSent")
		// }

		if c.Ctx.Input.Method() == "POST" {

			//c.Data["completeRegister"] = true
			//c.Redirect("/complete-register", 302)
			flash := beego.NewFlash()
			items := registerValidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				fmt.Println("Cannot parse form")
				return
			}

			//c.Data["FormItems"] = items

			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				fmt.Println(c.Data["Errors"])
				return
			}

			fmt.Println(items.Classes)
			result := make(map[string]string)
			profile_id := c.GetSession("ProfileId").(int)
			fmt.Println(profile_id)
			data := make(map[string]string)
			data["FirstName"] = items.FirstName
			data["LastName"] = items.LastName
			data["Classes"] = items.Classes
			data["Phone"] = items.Phone
			if len(items.DisplayName) == 0 {
				data["DisplayName"] = "0"
			} else {
				data["DisplayName"] = "1"
			}

			data["DOB"] = items.DOB

			if items.Country == "Other" {
				data["Country"] = items.OtherCountry
			} else {
				data["Country"] = items.Country
			}

			data["Location"] = items.Location
			data["PostalCode"] = items.PostalCode

			if items.Newsletters == "1" {
				data["Newsletters"] = "1"
			} else {
				data["Newsletters"] = "0"
			}

			data["CountryDialCode"] = items.CountryDialCode
			data["DialCountryCode"] = items.DialCountryCode

			result = models.CompleteUserProfile(data, profile_id)

			if result["status"] == "1" {

				c.Data["FormItems"] = items

				c.Data["completeRegister"] = true
				//user_details := models.GetUserDetails(user_id)
				//email_verified := user_details["user"]["email_verified"].(string)
				//email_address := user_details["user"]["email_address"].(string)
				//name := user_details["user"]["name"].(string)
				//footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

				//email_html_message := "Hi there, " + name + constants.REGISTERATION_CONTENT + footer_msg

				result := models.ResendVerificationLink(user_id)
				fmt.Println(result)
				//common.SendEmail(email_address, "Do You Gushou? Yeah, You Do.", email_html_message)
			} else {
				c.Data["DbError"] = result["status"]
			}

			if _, ok := result["error"]; ok {
				c.Data["DbError"] = result["error"]
			}

		}
	} else {
		c.Redirect("/", 302)
	}

}

func (c *UserController) ResendVerificationLink() {
	c.TplName = "user/completeRegister.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	fmt.Println(c.Ctx.Input.Method())

	if c.Ctx.Input.Method() == "POST" {

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.ResendVerificationLink(user_id)
		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
		//c.Data["completeRegister"] = true

	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *UserController) AllNotification() {
	c.TplName = "user/notificationsAll.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	user_id := c.GetSession("UserId").(int)

	var notification []orm.Params
	notification = models.GetNotification(user_id, false)
	if notification[0] != nil {
		c.Data["MyNotification"] = notification
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	if c.Ctx.Input.Method() == "POST" {

		//c.Redirect("/complete-register", 302)
		c.Data["completeRegister"] = true

	}
}

func (c *UserController) UploadProfileImg() {
	c.TplName = "user/profileSettings.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {
		user_id := ""
		if c.GetString("paddleruserid") != "" {
			user_id = c.GetString("paddleruserid")
		} else {
			id := c.GetSession("UserId").(int)
			user_id = strconv.Itoa(id)
		}

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			result := models.SaveProfileUrl(user_id, path)
			c.Ctx.Output.Body([]byte(result))
		}

		//str, err := resp.Presign()

		/*svc = s3.New(nil)
		req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
			Bucket: aws.String("gushou-image"),
			Key:    aws.String(path),
			//Body: fileBytes,
			})

		urlStr, err := req.Presign(15 * time.Minute)
		fmt.Println("The URL is", urlStr)

		//fmt.Println("The URL is:", resp)*/

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *UserController) DeleteUser() {
	c.TplName = "paddler/paddlersList.html"
	fmt.Println("In DeleteUser")

	if c.Ctx.Input.Method() == "POST" {

		user_id := c.GetString("user_id")
		fmt.Println(user_id)

		result := models.DeleteUser(user_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *UserController) RemoveNotification() {
	c.TplName = "user/notificationsAll.html"
	fmt.Println("In RemoveNotification")

	if c.Ctx.Input.Method() == "POST" {

		notf_id := c.GetString("notf_id")

		result := models.RemoveNotification(notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func GetTempLoc(filename string) string {
	return strings.TrimRight(os.TempDir(), "/") + "/" + filename
}

/** This code get the individual user details depend on userId .*/
func (c *UserController) GetUserDetail() {

	fmt.Println("In GetIndividualWavierMemberDetail")
	c.TplName = "team/teamView.html"
	user_id := c.GetString("user_id")
	result := models.GetUserDetail(user_id)
	b, _ := json.Marshal(result)
	fmt.Println(string(b))

	c.Ctx.Output.Body([]byte(string(b)))
	fmt.Println("Out GetIndividualWavierMemberDetail")
}

/** This code get the list name detail depends on  userIds .*/
func (c *UserController) GetUserNameList() {
	c.TplName = "team/teamView.html"
	fmt.Println("In GetUserNameList")

	if c.Ctx.Input.Method() == "POST" {
		var member_ids []string
		c.Ctx.Input.Bind(&member_ids, "ids")
		result := models.GetUserNameListData(member_ids)

		b, _ := json.Marshal(result)
		fmt.Println(string(b))

		c.Ctx.Output.Body([]byte(string(b)))
	}
}

func checkUserRole(user_profileActivities string) (string, string, string) {

	isValidTeamMan := "false"
	isValidEventOrgani := "false"
	isValidPaddling := "false"

	if strings.Contains(user_profileActivities, "Team Management") {
		isValidTeamMan = "true"
	}

	if strings.Contains(user_profileActivities, "Event Organising") {
		isValidEventOrgani = "true"
	}

	if strings.Contains(user_profileActivities, "Paddling") {
		isValidPaddling = "true"
	}

	return isValidTeamMan, isValidEventOrgani, isValidPaddling
}
