package controllers

import (
	"fmt"
	"html/template"
	"strings"

	"bytes"
	"gushou/common"
	"gushou/constants"
	"gushou/models"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/bamzi/jobrunner"
)

type WaiverController struct {
	beego.Controller
}

func (c *WaiverController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY
}

type waiverAcceptValidate struct {
	MedicalCondition     string `form:"medical_condition" `
	MedicalConditionDesc string `form:"medical_condition_desc" `
	ContactName          string `form:"contact_name" valid:"Required"`
	ContactNum           string `form:"contact_number" valid:"Required"`
	// Agree                string `form:"agree" valid:"Required"`
}

func (c *WaiverController) Waiver() {

	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	//models.GetAllStockItems()
	c.TplName = "waiver/waiver.html"
	c.Data["PageTitle"] = "Create Team"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	user_id := ""

	if models.IsLoggedIn(c.GetSession("GushouSession")) {

		c.Data["str_from"] = "login"
		fmt.Println("Logged in")
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		id := c.GetSession("UserId").(int)
		user_id = strconv.Itoa(id)

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
		c.Data["str_from"] = "withoutlogin"
	}

	userid := c.Ctx.Input.Param(":userid")
	strFrom := c.Ctx.Input.Param(":from")
	event_slug := c.Ctx.Input.Param(":event_slug")
	team_slug := c.Ctx.Input.Param(":team_slug")
	if userid != "" {
		user_id = userid
	}
	str_from := ""
	if strFrom != "" {
		str_from = strFrom

	}

	fmt.Println(user_id)
	// if c.GetSession("UpdateSuccess") == "true" {
	// 	c.Data["UpdateSuccess"] = true
	// 	c.DelSession("UpdateSuccess")
	// }

	event_id := models.GetEventId(event_slug)
	team_id := models.GetTeamId(team_slug)

	fk_team_waiver_id := ""
	fk_member_id := ""
	without_login_member_id := ""

	fmt.Println(event_id, team_id)

	team_waiver := make(map[string]orm.Params)

	if c.GetSession("SuperAdmin") != nil {
		team_waiver = models.GetWaiver(event_id, team_id, user_id, "true")
	} else {
		team_waiver = models.GetWaiver(event_id, team_id, user_id, "false")
	}

	if _, ok := team_waiver["status"]; !ok {
		if _, ok := team_waiver["team_waiver_0"]; ok {

			c.Data["TeamWaiver"] = team_waiver["team_waiver_0"]
			fmt.Println("TeamWaiver present")

			if team_waiver["team_waiver_0"]["fk_team_waiver_id"] != nil {
				fk_team_waiver_id = team_waiver["team_waiver_0"]["fk_team_waiver_id"].(string)
			}
			if team_waiver["team_waiver_0"]["fk_member_id"] != nil {
				fk_member_id = team_waiver["team_waiver_0"]["fk_member_id"].(string)
				UserAge := models.GetUserAgeByUserId(fk_member_id)
				IsUnder18 := "false"
				if UserAge < "18" {
					IsUnder18 = "true"
				}
				c.Data["IsUnder18"] = IsUnder18
				c.Data["event_id"] = event_id
				c.Data["team_id"] = team_id
				c.Data["fk_team_waiver_id"] = fk_team_waiver_id
				c.Data["fk_member_id"] = fk_member_id
				without_login_member_id = fk_member_id
				fmt.Println(without_login_member_id)

			}

			if team_waiver["team_waiver_0"]["waiver_signed_date"] != nil {
				c.Data["Signed"] = true
			} else if team_waiver["team_waiver_0"]["signed_upload_date"] != nil {
				c.Data["Signed"] = true
			}
		} else {
			c.Redirect("/dashboard", 302)
		}
	} else {
		c.Redirect("/dashboard", 302)
	}

	if c.Ctx.Input.Method() == "POST" {

		isUnder18 := c.GetString("isUnder18")
		fmt.Println("isUnder18", isUnder18)

		flash := beego.NewFlash()
		items := waiverAcceptValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			fmt.Println("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//c.Redirect("/dashboard", 302)

		fmt.Printf("%+v\n", items)

		data := make(map[string]string)
		result := make(map[string]string)

		data["MedicalConditionDesc"] = items.MedicalConditionDesc
		data["ContactName"] = items.ContactName
		data["ContactNum"] = items.ContactNum
		//data["Agree"] = items.Agree

		if items.MedicalCondition == "on" {
			data["MedicalCondition"] = "yes"
		} else {
			data["MedicalCondition"] = "no"
		}
		fmt.Printf("without-", without_login_member_id)

		if str_from == "emailwaiver" {
			fk_member_id = without_login_member_id
		} else {
			id := c.GetSession("UserId").(int)
			fk_member_id = strconv.Itoa(id)
		}

		result = models.UpdateWaiver(data, team_id, event_id, fk_team_waiver_id, fk_member_id)

		if result["status"] == "1" {

			if str_from == "emailwaiver" {
				c.Ctx.Output.JSON("Your Waiver has been uploaded successfully", false, false)
			} else {

				c.Redirect("/waiver-all", 302)
			}
			c.SetSession("UpdateSignWaiverSuccess", "true")

		}
	}

}

func (c *WaiverController) AllWaiver() {
	c.TplName = "waiver/waiversAll.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	fmt.Println("AllWaivers")
	id := c.GetSession("UserId").(int)
	user_id := strconv.Itoa(id)

	result := make(map[string]orm.Params)
	result = models.GetMyWaiver(user_id)

	if _, ok := result["status"]; !ok {
		c.Data["MyWaiver"] = true
		if c.GetSession("UpdateSignWaiverSuccess") == "true" {
			c.Data["UpdateSignWaiverSuccess"] = true
			c.DelSession("UpdateSignWaiverSuccess")
		}
		c.Data["AllWaivers"] = result
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		fmt.Println("Logged in")
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *WaiverController) UploadTeamWaiver() {
	c.TplName = "team/teamEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		// id := c.GetSession("UserId").(int)
		// user_id := strconv.Itoa(id)

		team_id := c.GetSession("TeamId").(string)
		event_id := c.GetString("event_name")
		waiver_name := c.GetString("waiver_name")

		exists := models.CheckWaiver(team_id, event_id)

		if !exists {
			token := ""
			creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
			_, err := creds.Get()
			if err != nil {
				fmt.Printf("bad credentials: %s", err)
			}
			cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
			svc := s3.New(session.New(), cfg)

			f, header, _ := c.GetFile("file")

			defer f.Close()

			out, err := os.Create("/tmp/" + header.Filename)
			if err != nil {
				fmt.Println("Unable to create the file for writing. Check your write access privilege")
				return
			}

			defer out.Close()

			// write the content from POST to the file
			_, err = io.Copy(out, f)
			if err != nil {
				fmt.Println(err)
			}

			fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

			file, err := os.Open("/tmp/" + header.Filename)
			if err != nil {
				fmt.Printf("err opening file: %s", err)
			}

			defer file.Close()
			fileInfo, _ := file.Stat()
			size := fileInfo.Size()
			buffer := make([]byte, size) // read file content to buffer

			file.Read(buffer)
			fileBytes := bytes.NewReader(buffer)
			fileType := http.DetectContentType(buffer)

			ext := filepath.Ext(file.Name())

			host_name := os.Getenv("HOST_NAME")
			path := ""
			if host_name == "gogushoulive" {
				path = "/waivers/" + time.Now().Format("20060102150405") + "_image" + ext
			} else {
				path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
			}
			fmt.Println(path)

			params := &s3.PutObjectInput{
				Bucket:        aws.String(os.Getenv("BUCKET")),
				Key:           aws.String(path),
				Body:          fileBytes,
				ACL:           aws.String("public-read"),
				ContentLength: aws.Int64(size),
				ContentType:   aws.String(fileType),
			}
			resp, err := svc.PutObject(params)
			if err != nil {
				fmt.Printf("bad response: %s", err)
			} else {
				fmt.Printf("response %s", awsutil.StringValue(resp))

				result, teampwaiverId := models.SaveWaiver(path, team_id, event_id, waiver_name)
				id := c.GetSession("UserId").(int)
				user_id := strconv.Itoa(id)
				//Run in background asynchronously Send the waiver to team members
				jobrunner.Now(WaiverSendOrResend{teampwaiverId, team_id, user_id})
				c.Ctx.Output.Body([]byte(result))
			}

			err = os.Remove("/tmp/" + header.Filename)
			if err == nil {
				fmt.Println("File removed successfully from tmp folder: " + header.Filename)
			}

		} else {
			fmt.Println("Selected event already has a waiver")
			c.Ctx.Output.Body([]byte("3"))

		}

	}
}

func (c *WaiverController) SendTeamWaiver() {
	c.TplName = "team/teamEdit.html"
	fmt.Println("In SendTeamWaiver")

	if c.Ctx.Input.Method() == "POST" {

		var MemberIds []string

		waiver_id := c.GetString("waiver_id")

		c.Ctx.Input.Bind(&MemberIds, "member_ids")

		fmt.Println(MemberIds)

		//data := make(map[string][]string)

		//data["WaiverIds"] = WaiverIds
		team_id := c.GetSession("TeamId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.SendTeamWaiver(MemberIds, waiver_id, team_id, user_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

/** This code get the members details of individual waiver depend on waiver_id & fk_event_id .*/
func (c *WaiverController) GetIndividualWavierMemberDetail() {

	fmt.Println("In GetIndividualWavierMemberDetail")
	c.TplName = "team/teamView.html"
	waiver_id := c.GetString("waiverId")
	fk_team_id := c.GetString("teamId")
	fk_event_id := c.GetString("eventId")
	result := models.GetWaiverMembers(waiver_id, fk_team_id, fk_event_id)
	c.Ctx.Output.JSON(result, false, false)
	fmt.Println("Out GetIndividualWavierMemberDetail")
}
func (c *WaiverController) UploadSignedTeamWaiver() {
	c.TplName = "team/teamEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		waiver_member_id := c.GetString("waiver_member_id_upload")
		waiver_id := c.GetString("waiver_id_upload")
		emergency_contact_name := c.GetString("contact_name")
		emergency_contact_number := c.GetString("contact_number")
		medical_condition := c.GetString("medical_condition_desc")
		team_id := c.GetString("team_id_upload")
		event_id := c.GetString("event_id_upload")

		fmt.Println(waiver_member_id)
		fmt.Println(waiver_id)
		fmt.Println(emergency_contact_name, emergency_contact_number)
		fmt.Println(medical_condition)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/waivers/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
		} else {

			fmt.Printf("response %s", awsutil.StringValue(resp))
			fmt.Println(waiver_member_id)
			fmt.Println(waiver_id)
			fmt.Println(emergency_contact_name, emergency_contact_number)
			fmt.Println(medical_condition)
			fmt.Println(path)
			fmt.Println(team_id)
			fmt.Println(event_id)

			user_age := models.GetUserAgeByUserId(waiver_member_id)
			isUnder_18 := false
			if user_age < "18" {
				isUnder_18 = true

			} else {
				isUnder_18 = false
			}
			result := models.SaveSignedUploadedWaiver(path, waiver_member_id, waiver_id, emergency_contact_name, emergency_contact_number, medical_condition, isUnder_18, team_id, event_id)

			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}

	}
}
func (c *WaiverController) ViewUploadedWaiver() {

	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	c.Ctx.ResponseWriter.Header().Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	c.TplName = "waiver/ViewUploadWaiver.html"
	c.Data["PageTitle"] = "Create Team"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	user_id := ""

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		fmt.Println("Logged in")
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		id := c.GetSession("UserId").(int)
		user_id = strconv.Itoa(id)

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	member_id := c.Ctx.Input.Param(":fk_member_id")
	waiver_id := c.Ctx.Input.Param(":waiverId")
	teamid := c.Ctx.Input.Param(":teamId")
	eventid := c.Ctx.Input.Param(":eventId")
	fmt.Println(member_id)
	fmt.Println(waiver_id)
	fmt.Println(teamid)
	fmt.Println(eventid)
	fmt.Println(user_id)

	c.Data["TeamUploadWaiver"] = models.GetUploadedWaiverDetails(waiver_id, member_id, eventid, teamid)

}

func (c *WaiverController) ValidateSignedTeamWaiver() {

	member_id := c.GetString("member_id")
	waiver_id := c.GetString("waiver_id")
	team_id := c.GetString("team_id")
	event_id := c.GetString("event_id")

	result := models.ValidateSignedTeamWaiver(member_id, waiver_id, team_id, event_id)

	c.Ctx.Output.Body([]byte(result))

}
func (c *WaiverController) AddWaiverFromEvent() {

	event_id := c.GetString("event_id_add_waiver_event")
	waiver_name := c.GetString("waiver_name_add_waiver_event")
	fmt.Println(event_id)
	fmt.Println(waiver_name)
	flag := "flagfalse"
	eventMaps := models.GetEventDetailsByEventId(event_id)
	if len(eventMaps) > 0 {
		for _, v := range eventMaps {
			fmt.Println(v["fk_team_id"].(string))
			if v["fk_team_id"] != nil {
				var team_id = v["fk_team_id"].(string)
				exists := models.CheckWaiver(team_id, event_id)

				if !exists {
					flag = "true"
					token := ""
					creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
					_, err := creds.Get()
					if err != nil {
						fmt.Printf("bad credentials: %s", err)
					}
					cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
					svc := s3.New(session.New(), cfg)

					f, header, _ := c.GetFile("file")

					defer f.Close()

					out, err := os.Create("/tmp/" + header.Filename)
					if err != nil {
						fmt.Println("Unable to create the file for writing. Check your write access privilege")
						return
					}

					defer out.Close()

					// write the content from POST to the file
					_, err = io.Copy(out, f)
					if err != nil {
						fmt.Println(err)
					}

					fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

					file, err := os.Open("/tmp/" + header.Filename)
					if err != nil {
						fmt.Printf("err opening file: %s", err)
					}

					defer file.Close()
					fileInfo, _ := file.Stat()
					size := fileInfo.Size()
					buffer := make([]byte, size) // read file content to buffer

					file.Read(buffer)
					fileBytes := bytes.NewReader(buffer)
					fileType := http.DetectContentType(buffer)

					ext := filepath.Ext(file.Name())

					host_name := os.Getenv("HOST_NAME")
					path := ""
					if host_name == "gogushoulive" {
						path = "/waivers/" + time.Now().Format("20060102150405") + "_image" + ext
					} else {
						path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
					}
					fmt.Println(path)

					params := &s3.PutObjectInput{
						Bucket:        aws.String(os.Getenv("BUCKET")),
						Key:           aws.String(path),
						Body:          fileBytes,
						ACL:           aws.String("public-read"),
						ContentLength: aws.Int64(size),
						ContentType:   aws.String(fileType),
					}
					resp, err := svc.PutObject(params)
					if err != nil {
						fmt.Printf("bad response: %s", err)
					} else {
						fmt.Printf("response %s", awsutil.StringValue(resp))

						result, teampwaiverId := models.SaveWaiver(path, team_id, event_id, waiver_name)
						id := c.GetSession("UserId").(int)
						user_id := strconv.Itoa(id)
						fmt.Println(result)
						//Run in background asynchronously Send the waiver to team members
						jobrunner.Now(WaiverSendOrResendForTeamFromEvent{teampwaiverId, team_id, user_id})
						//c.Ctx.Output.Body([]byte(strconv.FormatBool(exists)))

					}

					err = os.Remove("/tmp/" + header.Filename)
					if err == nil {
						fmt.Println("File removed successfully from tmp folder: " + header.Filename)
					}

				}

				// else {
				// 	c.Ctx.Output.Body([]byte(strconv.FormatBool(exists)))
				// }

				// else {
				// 	//flag = "false"
				// 	//fmt.Println(flag)
				// 	result = flag
				// 	fmt.Println("Selected event already has a waiver")
				// 	c.Ctx.Output.Body([]byte(result))

				// }
			}

		}
		c.Ctx.Output.Body([]byte(flag))
	} else {
		//result = "No Teams Registered For Event"
		c.Ctx.Output.Body([]byte("No Teams Registered For Event"))
	}

}
func (c *WaiverController) UploadUnder18SignedWaiver() {
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		waiver_member_id := c.GetString("member_id_under18")
		waiver_id := c.GetString("waiver_id_under18")
		emergency_contact_name := c.GetString("contact_name_under18")
		emergency_contact_number := c.GetString("contact_number_under18")
		medical_con := c.GetString("medical_cond_under18")
		medical_condition := c.GetString("medical_cond_desc_under18")
		team_id := c.GetString("team_id_under18")
		event_id := c.GetString("event_id_under18")

		fmt.Println(waiver_member_id)
		fmt.Println(waiver_id)
		fmt.Println(emergency_contact_name, emergency_contact_number)
		fmt.Println(medical_condition)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/waivers/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
		} else {

			fmt.Printf("response %s", awsutil.StringValue(resp))
			fmt.Println(waiver_member_id)
			fmt.Println(waiver_id)
			fmt.Println(emergency_contact_name, emergency_contact_number)
			fmt.Println(medical_condition)
			fmt.Println(path)
			fmt.Println(team_id)
			fmt.Println(event_id)

			user_age := models.GetUserAgeByUserId(waiver_member_id)
			isUnder_18 := false
			if user_age < "18" {
				isUnder_18 = true

			} else {
				isUnder_18 = false
			}

			medical_con_checkbox := ""

			if medical_con == "false" {
				medical_con_checkbox = "no"
			} else {
				medical_con_checkbox = "yes"
			}
			result := models.SaveUnder18SignedUploadedWaiver(path, waiver_member_id, waiver_id, emergency_contact_name, emergency_contact_number, medical_con_checkbox, medical_condition, isUnder_18, team_id, event_id)

			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}

	}
}
func (c *WaiverController) SendEventTeamWaiver() {
	fmt.Println("In SendTeamWaiver")

	if c.Ctx.Input.Method() == "POST" {

		var MemberIds string
		var TeamIdArray []string
		result := ""

		c.Ctx.Input.Bind(&MemberIds, "waiver_members_event")

		fmt.Println(MemberIds)

		var mem = strings.Split(MemberIds, ",")
		fmt.Println(mem)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		for _, v := range mem {

			var mem1 = strings.Split(v, "_")

			fmt.Println(v)
			fmt.Println(mem1)
			if mem1[0] == "team" {

				var event_reg_id = mem1[1]
				team_id, res := models.SendEventTeamWaiver(event_reg_id, user_id)
				result = res
				TeamIdArray = append(TeamIdArray, team_id)
				fmt.Println(TeamIdArray)
			} else {

				var event_id, team_id, event_reg_id, member_id string

				event_id = mem1[1]
				team_id = mem1[2]
				member_id = mem1[3]
				event_reg_id = mem1[4]
				if len(TeamIdArray) > 0 {
					if !common.StringInArray(team_id, TeamIdArray) {
						result1 := models.SendEventTeamIndividualWaiver(event_id, team_id, member_id, event_reg_id, user_id)
						result = result1
						fmt.Println(result1)
					}
				} else {
					result1 := models.SendEventTeamIndividualWaiver(event_id, team_id, member_id, event_reg_id, user_id)
					result = result1
					fmt.Println(result1)
				}
			}
		}
		c.Ctx.Output.Body([]byte(result))
	}

}
