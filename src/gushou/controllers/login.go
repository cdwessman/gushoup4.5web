package controllers

import (
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

var (
	AccountSids = "AC6c6d752dd85d71f685c32c3f85374fa5"
	AuthTokens  = "94aa631f042ed87b1726cfa062f1aa8b"
)

type LoginController struct {
	beego.Controller
}

func (c *LoginController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY
}

type credentialValidate struct {
	Email    string `form:"email" valid:"Email"`
	Password string `form:"password" valid:"Required"`
}

type forgotPasswordValidate struct {
	Email string `form:"email" valid:"Email"`
}

type resetPasswordValidate struct {
	Password        string `form:"resetPasswordPassword" valid:"Required"`
	ConfirmPassword string `form:"resetPasswordPasswordConfirm" valid:"Required"`
}

type PracticeStatus struct {
	Response string `json:"response"`
	Reason   string `json:"reason"`
}

func (c *LoginController) Login() {

	fmt.Println("SMS Test for Gushou Login...")

	c.TplName = "login.html"
	c.Data["PageTitle"] = "Sign in"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	fmt.Println(c.Ctx.Input.Method())

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		credentials := credentialValidate{}
		if err := c.ParseForm(&credentials); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = credentials
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&credentials); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		result := make(map[string]string)
		result = models.Login(strings.ToLower(credentials.Email), credentials.Password)

		if result["status"] == "1" {

			//if redirct url contain path then redirect to concern page otherwise move to dashboard page.
			defaultRedirectUrl := "/dashboard"
			urlRedirectPath := c.GetString("redirecturl")
			if urlRedirectPath != "" {
				defaultRedirectUrl = urlRedirectPath
				fmt.Println("urlparam", urlRedirectPath)
			}
			models.Resetfreetrial_subscription_activationpopup(result["user_id"])

			// end

			fmt.Println("status 1")
			if beego.AppConfig.String("SessionOn") == "true" {
				v := c.GetSession("GushouSession")
				if v == nil {
					c.SetSession("GushouSession", int(1))
					c.SetSession("FirstName", result["first_name"])
					c.SetSession("LastName", result["last_name"])

					if result["super_admin"] == "true" {
						c.SetSession("SuperAdmin", "true")
					}

					c.SetSession("Team_Manage_Admin", result["team_manage_admin"])
					c.SetSession("Event_Manage_Admin", result["event_manage_admin"])
					c.SetSession("Paddling_Admin", result["paddling_admin"])

					id, err := strconv.Atoi(result["user_id"])
					if err == nil {
						c.SetSession("UserId", id)
					}

					id, err = strconv.Atoi(result["profile_id"])
					if err == nil {
						c.SetSession("ProfileId", id)
					}

					fmt.Println(c.GetSession("UserId"))
					c.Redirect(defaultRedirectUrl, 302)
				} else {
					c.SetSession("GushouSession", v.(int)+1)
				}
			} else {
				c.Redirect(defaultRedirectUrl, 302)
			}
			//c.Redirect("/dashboard", 302)
		} else if result["redirect"] != "" {

			id, err := strconv.Atoi(result["user_id"])
			if err == nil {
				c.SetSession("UserId", id)
			}

			id, err = strconv.Atoi(result["profile_id"])
			if err == nil {
				c.SetSession("ProfileId", id)
			}

			fmt.Println(c.GetSession("UserId"))
			fmt.Println(c.GetSession("ProfileId"))

			c.SetSession("verificationEmailSent", "true")

			c.Redirect("/complete-register", 302)

		} else if result["errorMessage"] != "" {
			c.Data["errorMessage"] = result["errorMessage"]
		} else if result["notVerified"] == "true" {
			c.Data["notVerified"] = true
			id, err := strconv.Atoi(result["user_id"])
			if err == nil {
				c.SetSession("UserId", id)
			}
		} else {
			c.Data["InvalidCredentials"] = true
		}

	}

}

func (c *LoginController) Logout() {
	//c.TplName = "stock/add-inventory.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Logout"

	if beego.AppConfig.String("SessionOn") == "true" {
		v := c.GetSession("GushouSession")
		if v == nil {
			c.Data["IsLoggedIn"] = 0
			c.Data["PageTitle"] = "Logout"
			c.DestroySession()
			c.Redirect("/", 302)
		} else {
			c.Data["IsLoggedIn"] = 0
			c.DestroySession()
			c.Data["PageTitle"] = "Logout"
			c.Redirect("/", 302)
		}
	} else {
		c.Redirect("/dashboard", 302)
	}
}

func (c *LoginController) Dashboard() {
	c.TplName = "dashboard.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	user_id := c.GetSession("UserId").(int)

	user_profile := make(map[string]orm.Params)

	user_profile = models.GetUserDetails(strconv.Itoa(user_id))
	if _, ok := user_profile["status"]; !ok {
		c.Data["UserProfile"] = user_profile
	}

	team_practice := make(map[string][]orm.Params)
	loginUserTimeZone := GetTimezoneInSessionLogin(c)
	team_practice = models.GetUserPractices(user_id, loginUserTimeZone)
	if _, ok := team_practice["status"]; !ok {
		c.Data["TeamPractice"] = team_practice
	}

	fmt.Println("PRACTICES ")
	//fmt.Println(team_practice)

	team_manage := make(map[string]orm.Params)
	team_manage = models.GetTeam(user_id, true)
	if _, ok := team_manage["status"]; !ok {
		c.Data["TeamManage"] = team_manage
	}
	if len(team_manage) > 2 {
		c.Data["myTeamCount"] = true
	}

	team_part_of := make(map[string]orm.Params)
	team_part_of = models.GetTeamPartOf(user_id, true)
	if _, ok := team_part_of["status"]; !ok {
		c.Data["TeamPartOf"] = team_part_of
	}
	if len(team_part_of) > 2 {
		c.Data["userTeamCount"] = true
	}

	all_my_teams := make(map[string]orm.Params)
	all_my_teams = models.GetAllMyTeams(user_id, true)
	if _, ok := all_my_teams["status"]; !ok {
		c.Data["AllMyTeams"] = all_my_teams
	}
	if len(all_my_teams) > 2 {
		c.Data["allMyTeamCount"] = true
	}

	my_events := make(map[string]orm.Params)
	my_events = models.GetEvent(user_id, true)
	if _, ok := my_events["status"]; !ok {
		c.Data["MyEvent"] = my_events
	}

	events_part_of := make(map[string]orm.Params)
	events_part_of = models.GetEventPartOf(user_id, true)
	if _, ok := events_part_of["status"]; !ok {

		if _, ok := my_events["status"]; !ok {

			key := strconv.Itoa(len(my_events))
			for _, x := range events_part_of {
				my_events[key] = x

				t, err := strconv.Atoi(key)
				t = t + 1
				if err == nil {
					key = strconv.Itoa(t)
				}

			}

			c.Data["MyEvent"] = my_events

		} else {
			c.Data["MyEvent"] = events_part_of
		}

	}

	//fmt.Println(events_part_of)

	if len(my_events) > 2 {
		c.Data["myEventCount"] = true
	}

	upcoming_events := make(map[string]orm.Params)
	upcoming_events = models.GetUpcomingEvent(strconv.Itoa(user_id))
	if _, ok := upcoming_events["status"]; !ok {
		c.Data["UpcomingEvent"] = upcoming_events
	}

	//fmt.Println(my_events)

	var notification []orm.Params
	notification = models.GetNotification(user_id, true)
	if notification[0] != nil {
		c.Data["MyNotification"] = notification
	}
	if len(notification) > 5 {
		c.Data["myNotificationCount"] = true
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		c.Data["CurrentUserId"] = user_id

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *LoginController) ForgotPassword() {
	c.TplName = "user/forgotPassword.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := forgotPasswordValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		result := models.SendResetLink(items.Email)

		fmt.Println(result)

		if result["status"] == true {
			c.Data["Success"] = true
		} else {
			c.Data["Fail"] = true
		}

	}
}

func (c *LoginController) VerifyEmail() {
	c.TplName = "verifyEmail.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	fmt.Println(c.Ctx.Input.Param(":token"))
	token := c.Ctx.Input.Param(":token")

	isVerified := models.VerifyEmailToken(token)

	fmt.Println("isVerified ", isVerified)

	if isVerified["is_verified"] == "true" {
		id, err := strconv.Atoi(isVerified["user_id"])
		if err == nil {
			c.SetSession("UserId", id)
		}

		c.SetSession("Team_Manage_Admin", isVerified["team_manage_admin"])
		c.SetSession("Event_Manage_Admin", isVerified["event_manage_admin"])
		c.SetSession("Paddling_Admin", isVerified["paddling_admin"])

		id, err = strconv.Atoi(isVerified["profile_id"])
		if err == nil {
			c.SetSession("ProfileId", id)
		}
		if isVerified["redirect"] == "dashboard" {

			isUserActive := isVerified["is_user_active"]

			if isUserActive == "1" {
				if models.IsLoggedIn(c.GetSession("GushouSession")) {
					c.Redirect("/dashboard", 302)
				} else {
					c.Redirect("/login", 302)
				}
			} else {

				c.SetSession("GushouSession", int(1))
				c.SetSession("FirstName", isVerified["first_name"])
				c.SetSession("LastName", isVerified["last_name"])

				user_id := strconv.Itoa(id)
				user_details := models.GetUserDetails(user_id)
				email_address := user_details["user"]["email_address"].(string)
				name := isVerified["first_name"]
				footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

				footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

				email_html_message := "Hi there, " + name + constants.REGISTERATION_CONTENT + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe
				fmt.Printf("verify email")
				fmt.Println(footer_msg)
				common.SendEmail(email_address, "Do You Gushou? Yeah, You Do.", email_html_message)

				c.Redirect("/account-created", 302)
			}

		} else {
			c.Redirect("/complete-register", 302)
		}

	} else {
		if isVerified["expired"] == "true" {
			c.Data["Expired"] = true
		} else if isVerified["invalid"] == "true" {
			c.Data["Invalid"] = true
		}
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *LoginController) ResetPassword() {
	c.TplName = "user/resetPassword.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	fmt.Println(c.Ctx.Input.Param(":token"))
	token := c.Ctx.Input.Param(":token")

	isVerified := models.VerifyResetPasswordToken(token)

	fmt.Println("isVerified ", isVerified)

	if isVerified["is_verified"] == "true" {
		id, err := strconv.Atoi(isVerified["user_id"])
		if err == nil {
			c.SetSession("UserId", id)
		}

		/*id, err = strconv.Atoi(isVerified["profile_id"])
		if err == nil {
			c.SetSession("ProfileId", id)
		}*/

		/*if isVerified["redirect"] == "dashboard" {
			c.SetSession("GushouSession", int(1))
			c.SetSession("FirstName", isVerified["first_name"])
			c.SetSession("LastName", isVerified["last_name"])
			c.Redirect("/dashboard", 302)
		} else {
			c.Redirect("/complete-register",302)
		}*/

		//c.Redirect("/complete-register",302)

	} else {
		if isVerified["expired"] == "true" {
			c.Data["Expired"] = true
		} else if isVerified["invalid"] == "true" {
			c.Data["Invalid"] = true
		}
	}

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := resetPasswordValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		isVerified = models.ResetPassword(token, items.Password)

		fmt.Println("isVerified ", isVerified)

		if isVerified["status"] == "1" {
			id, err := strconv.Atoi(isVerified["user_id"])
			if err == nil {
				c.SetSession("UserId", id)
			}

			id, err = strconv.Atoi(isVerified["profile_id"])
			if err == nil {
				c.SetSession("ProfileId", id)
			}

			if isVerified["super_admin"] == "true" {
				c.SetSession("SuperAdmin", "true")
			}

			c.SetSession("Team_Manage_Admin", isVerified["team_manage_admin"])
			c.SetSession("Event_Manage_Admin", isVerified["event_manage_admin"])
			c.SetSession("Paddling_Admin", isVerified["paddling_admin"])

			if isVerified["redirect"] == "dashboard" {
				c.SetSession("GushouSession", int(1))
				c.SetSession("FirstName", isVerified["first_name"])
				c.SetSession("LastName", isVerified["last_name"])
				c.Redirect("/dashboard", 302)
			} else {
				c.Redirect("/complete-register", 302)
			}

		}
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *LoginController) AccountCreated() {
	c.TplName = "user/accountCreated.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	if c.Ctx.Input.Method() == "POST" {

		//c.Redirect("/complete-register", 302)
		c.Data["completeRegister"] = true

	}
}

func (c *LoginController) GetPracticeStatus() {
	fmt.Println("In GetPracticeStatus")

	user_id := c.GetSession("UserId").(int)
	practice_id := c.GetString("practice_id")

	result := models.GetPracticeStatus(practice_id, user_id)

	fmt.Println(result)

	outputObj := &PracticeStatus{
		Response: result["0"]["response"].(string),
		Reason:   result["0"]["reason"].(string),
	}

	fmt.Println(outputObj)
	b, _ := json.Marshal(outputObj)
	fmt.Println(string(b))

	c.Ctx.Output.Body([]byte(string(b)))
}

func (c *LoginController) SetTimeZoneInSession() {
	status := "false"
	fmt.Println("In SetTimeZoneInSession")
	localUserTimeZone := c.GetString("timeZone")
	fmt.Println("oiii ", localUserTimeZone)
	if localUserTimeZone != "" {
		if c.GetSession("localUserTimeZone") != nil {
			if localUserTimeZone != c.GetSession("localUserTimeZone").(string) {
				c.SetSession("localUserTimeZone", localUserTimeZone)
				status = "true"
			}
		} else {
			c.SetSession("localUserTimeZone", localUserTimeZone)
			status = "true"
		}
	}
	fmt.Println("out SetTimeZoneInSession")
	c.Ctx.Output.Body([]byte(status))
}

func GetTimezoneInSessionLogin(c *LoginController) string {

	if c.GetSession("localUserTimeZone") != nil {
		return c.GetSession("localUserTimeZone").(string)
	} else {
		t := time.Now()
		name, _ := t.Zone()
		return name
	}

	return ""
}
