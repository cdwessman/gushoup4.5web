package controllers

import (
	"encoding/json"
	"gushou/common"
	"gushou/models"

	"github.com/astaxie/beego"
	stripe "github.com/stripe/stripe-go"
)

type StripeWebHooksController struct {
	beego.Controller
}

var gofpdfDir string

func (c *StripeWebHooksController) SubscriptionUpdate() {
	responseText := 400
	if c.Ctx.Input.Method() == "POST" {

		var eventResponse *stripe.Event
		err := json.Unmarshal(c.Ctx.Input.RequestBody, &eventResponse)
		if err == nil {
			if eventResponse.Data != nil {
				tempSubscription := eventResponse.Data.Object
				if tempSubscription["id"] != nil {
					responseText = models.UpdateStripeSubscriptionDetailsFromStripe(tempSubscription["id"].(string))
				}
			}
		}
	}
	c.Ctx.Output.SetStatus(responseText)
	c.Ctx.Output.Body([]byte("2000"))
}

func (c *StripeWebHooksController) SubscriptionCanceled() {

	responseText := 400
	if c.Ctx.Input.Method() == "POST" {

		var eventResponse *stripe.Event
		err := json.Unmarshal(c.Ctx.Input.RequestBody, &eventResponse)
		if err == nil {
			if eventResponse.Data != nil {
				tempSubscription := eventResponse.Data.Object
				if tempSubscription["id"] != nil {
					responseText = models.UpdateStripeSubscriptionCanceledDetailsFromStripe(tempSubscription["id"].(string))
				}
			}
		}
	}
	c.Ctx.Output.SetStatus(responseText)
	c.Ctx.Output.Body([]byte("2000"))
}

func (c *StripeWebHooksController) InvoiceCreated() {
	responseText := 400
	if c.Ctx.Input.Method() == "POST" {

		var eventResponse *stripe.Event
		err := json.Unmarshal(c.Ctx.Input.RequestBody, &eventResponse)
		if err == nil {
			if eventResponse.Data != nil {
				tempInvoice := eventResponse.Data.Object
				if tempInvoice["id"] != nil {
					responseText = models.CreateInvoice(tempInvoice["id"].(string))
				}
			}
		}
	}
	c.Ctx.Output.SetStatus(responseText)
	c.Ctx.Output.Body([]byte("2000"))
}

func (c *StripeWebHooksController) InvoicePaymentFailed_InvoiceUpdated() {
	responseText := 400
	if c.Ctx.Input.Method() == "POST" {

		var eventResponse *stripe.Event
		err := json.Unmarshal(c.Ctx.Input.RequestBody, &eventResponse)
		if err == nil {
			if eventResponse.Data != nil {
				tempInvoice := eventResponse.Data.Object
				if tempInvoice["id"] != nil {
					responseText = models.InvoicePaymentFailed_InvoiceUpdated(tempInvoice["id"].(string))
				}
			}
		}
	}
	c.Ctx.Output.SetStatus(responseText)
	c.Ctx.Output.Body([]byte("2000"))
}

func (c *StripeWebHooksController) InvoicePaymentSucceeded() {
	responseText := 400
	if c.Ctx.Input.Method() == "POST" {

		var eventResponse *stripe.Event
		err := json.Unmarshal(c.Ctx.Input.RequestBody, &eventResponse)
		if err == nil {
			if eventResponse.Data != nil {
				tempInvoice := eventResponse.Data.Object
				if tempInvoice["id"] != nil {
					responseText = models.InvoicePaymentSucceeded(tempInvoice["id"].(string))
				}
			}
		}
	}
	c.Ctx.Output.SetStatus(responseText)
	c.Ctx.Output.Body([]byte("2000"))
}

func (c *StripeAPIController) PlanUpdated() {

	// result, err := models.CancelSubscription(c.Data["UserId"].(string))

	// if err != nil {
	// 	c.Data["json"] = err
	// } else {
	// 	c.Data["json"] = result
	// }
	// c.ServeJSON()
}

// This below api used to connect the stripe srandard account to the platform stripe account
func (c *StripeWebHooksController) AcceptStripeConnectAccount() {

	var errortext string
	var error_description string
	var code string
	var statecode string
	var responseText string
	c.Ctx.Input.Bind(&errortext, "error")
	c.Ctx.Input.Bind(&error_description, "error_description")
	c.Ctx.Input.Bind(&code, "code")
	c.Ctx.Input.Bind(&statecode, "state")

	if code != "" && statecode != "" {
		responseText1, eventid := models.CheckStatecodeExistByStatecode(statecode)
		responseText = responseText1
		if responseText == "" {
			successResponse, errorResponse := common.GetConnetStripeUserAccountIdUrl(code)
			if successResponse.Stripe_user_id != "" {
				responseText = models.AddConnectStripeDetail(successResponse, eventid)
			} else if errorResponse.Error != "" {
				responseText = errorResponse.Error_Description
			}
		}
	} else {
		responseText = "Failed to Connect Account. Please try again!."
	}
	c.Ctx.Output.SetStatus(200)
	c.Ctx.Output.JSON(responseText, false, false)

}
