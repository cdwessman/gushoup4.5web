package controllers

import (
	"fmt"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"math"
	"strconv"
	_ "strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type PaddlerController struct {
	beego.Controller
}

func (c *PaddlerController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY
}

type paddlerSearchValidate struct {
	PaddlerName  string `form:"search_paddlers_name"`
	Email        string `form:"email"`
	Classes      string `form:"classes" `
	Skill        string `form:"skill"`
	Location     string `form:"location"`
	PaddlingSide string `form:"paddling_side"`
	ForTeam      string `form:"for_team" `
	Limit        string `form:"paddler_search_limit"`
	Offset       string `form:"paddler_search_offset"`
	Type         string `form:"paddler_search_type"`
}

func (c *PaddlerController) PaddlerList() {
	c.TplName = "paddler/paddlersList.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := paddlerSearchValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//c.Redirect("/dashboard", 302)

		fmt.Printf("%+v\n", items)

		result := make(map[string]orm.Params)

		data := make(map[string]string)

		data["PaddlerName"] = items.PaddlerName
		data["Email"] = items.Email
		data["Classes"] = items.Classes
		data["PaddlingSide"] = items.PaddlingSide
		data["Skill"] = items.Skill
		data["Location"] = items.Location

		if items.ForTeam == "1" {
			data["ForTeam"] = "available"
		}

		data["Limit"] = items.Limit
		data["Offset"] = items.Offset
		data["Type"] = items.Type

		var offsetInt, limitInt = 0, 0
		num, err := strconv.Atoi(items.Limit)
		if err == nil {
			limitInt = num
		}
		num, err = strconv.Atoi(items.Offset)
		if err == nil {
			offsetInt = num
		}
		c.Data["NewOffset"] = items.Offset
		c.Data["NewLimit"] = items.Limit
		c.Data["Type"] = items.Type

		c.Data["AdvSearch"] = false

		for k, v := range data {

			if k == "Classes" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Skill" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Location" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Email" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "PaddlingSide" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "ForTeam" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			}

		}

		result, totalCount := models.SearchPaddler(data)

		if _, ok := result["status"]; !ok {
			c.Data["Paddlers"] = true
			c.Data["AllPaddlers"] = result

			c.Data["TotalPaddlers"] = totalCount

			i, _ := strconv.ParseInt(items.Limit, 10, 64)

			totalPages := int(math.Ceil(float64(totalCount) / float64(i)))
			fmt.Println("Pages ", totalPages)

			pages := make(map[int]int)
			offset := 0
			for i := 1; i <= totalPages; i++ {
				pages[i] = offset

				offset = offset + limitInt
			}
			c.Data["TotalPages"] = pages
			//fmt.Println(pages)

			tmp := make(map[int]int)
			var currPage int

			if items.Type != "last" {
				for k, v := range pages {

					if v == offsetInt {
						c.Data["CurrPage"] = k
						currPage = k
						break
					}

				}
			} else {
				c.Data["CurrPage"] = totalPages
				currPage = totalPages
			}

			if currPage == 0 {
				for k, _ := range pages {
					if k+1 <= len(pages) {
						//fmt.Println(pages[k + 1] , " > ", offsetInt)
						if pages[k+1] > offsetInt {
							c.Data["CurrPage"] = k
							currPage = k
							break
						}
					}
				}
			}

			fmt.Println(" CurrPage ", currPage)

			if currPage < 4 {

				if len(pages) >= 5 {
					for i := 1; i <= 5; i++ {
						tmp[i] = pages[i]
					}
				} else {
					for i := 1; i <= len(pages); i++ {
						tmp[i] = pages[i]
					}
				}

			} else {

				if currPage+2 <= totalPages {
					for i := currPage - 2; i <= currPage+2; i++ {
						tmp[i] = pages[i]
					}
				} else {
					for i := currPage - 3; i <= totalPages; i++ {
						tmp[i] = pages[i]
					}
				}

			}

			c.Data["FivePages"] = tmp

		} else {
			c.Data["Paddlers"] = false
		}

	} else {
		result := make(map[string]orm.Params)
		result, totalCount := models.SearchPaddler(nil)

		if _, ok := result["status"]; !ok {
			c.Data["Paddlers"] = true
			c.Data["AllPaddlers"] = result

			c.Data["TotalTeams"] = totalCount

			totalPages := int(math.Ceil(float64(totalCount) / float64(10)))
			fmt.Println("Pages ", totalPages)
			fmt.Println("TotalTeams ", totalCount)

			pages := make(map[int]int)
			offset := 0

			for i := 1; i <= totalPages; i++ {
				pages[i] = offset

				offset = offset + 10
			}
			c.Data["TotalPages"] = pages

			tmp := make(map[int]int)

			for i := 1; i <= 5; i++ {
				tmp[i] = pages[i]
			}

			c.Data["FivePages"] = tmp
			c.Data["CurrPage"] = 1

			c.Data["AdvSearch"] = false
		}
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}
