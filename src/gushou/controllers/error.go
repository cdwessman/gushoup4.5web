package controllers

import (
    "github.com/astaxie/beego"
    "fmt"
)

type ErrorController struct {
    beego.Controller
}

func (c *ErrorController) Error404() {
    c.Data["content"] = "page not found"
    fmt.Println("page not found")
    c.TplName = "dashboard.html"
    c.Redirect("/", 302)
}

func (c *ErrorController) Error500() {
    c.Data["content"] = "internal server error"
    c.TplName = "dashboard.html"
    c.Redirect("/", 302)
}

func (c *ErrorController) ErrorDb() {
    c.Data["content"] = "database is now down"
    c.TplName = "dashboard.html"
    c.Redirect("/", 302)
}