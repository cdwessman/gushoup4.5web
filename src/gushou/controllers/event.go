package controllers

import (
	"bytes"
	"encoding/json"
	_ "encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"io"
	"io/ioutil"
	"math"
	"math/big"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/text/currency"

	wkhtml "github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/astaxie/beego"
	_ "github.com/astaxie/beego/context"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/jung-kurt/gofpdf"
	"github.com/leekchan/accounting"
	NContext "golang.org/x/net/context"
	"googlemaps.github.io/maps"
)

type EventController struct {
	beego.Controller
}

func (c *EventController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY
}

type eventValidate struct {
	EventName              string   `form:"event_name" valid:"Required"`
	EventLocation          string   `form:"location" valid:"Required"`
	EventProvince          string   `form:"province" valid:"Required"`
	EventCountry           string   `form:"country" valid:"Required"`
	EventVenue             string   `form:"venue_name" valid:"Required"`
	EventStreet            string   `form:"venue_street_address" valid:"Required"`
	EventStartDate         string   `form:"start_date" valid:"Required"`
	EventEndDate           string   `form:"end_date" `
	EventAboutCause        string   `form:"about_event_cause" `
	EventDesc              string   `form:"event_desc" `
	MXdivision             []string `form:"MX_divisions[]" `
	Mdivision              []string `form:"M_divisions[]" `
	Wdivision              []string `form:"W_divisions[]" `
	Currency               string   `form:"currency"`
	SebDate                string   `form:"seb_date"`
	EbDate                 string   `form:"eb_date"`
	RDate                  string   `form:"r_date"`
	M_seb_price            []string `form:"M_seb_price[]" `
	M_eb_price             []string `form:"M_eb_price[]" `
	M_r_price              []string `form:"M_r_price[]" `
	W_seb_price            []string `form:"W_seb_price[]" `
	W_eb_price             []string `form:"W_eb_price[]" `
	W_r_price              []string `form:"W_r_price[]" `
	MX_seb_price           []string `form:"MX_seb_price[]" `
	MX_eb_price            []string `form:"MX_eb_price[]" `
	MX_r_price             []string `form:"MX_r_price[]" `
	ComplPractice          string   `form:"compl_pract_cost"`
	ComplFreePractice      string   `form:"free_practice_num"`
	AddtionalPractice      string   `form:"additonal_pracs"`
	AdditionalCostPractice string   `form:"additional_practices_cost"`
	CoachService           string   `form:"coach_serv"`
	CoachServiceCost       string   `form:"coaching_service_cost"`
	ItemTitle              []string `form:"item_title[]" `
	ItemCost               []string `form:"item_cost[]" `
	ItemIdValue            []string `form:"item_id_value[]" `
	EventTwitter           string   `form:"twitter_handle" `
	EventFacebook          string   `form:"facebook_page_url" `
	EventYouTube           string   `form:"youtube_url" `
	EventPh                string   `form:"event_phone_number" `
	EventEmail             string   `form:"event_email"`
	EventWebsite           string   `form:"website_url" `
	Question               []string `form:"faqQ[]" `
	Answer                 []string `form:"faqA[]" `
	EventRegistrationOpen  string   `form:"recruit"`
	EventQuestions         []string `form:"event_questions[]" `

	PricingBased        string   `form:"pricing_based" `
	SebPrice            []string `form:"seb_price[]" `
	EbPrice             []string `form:"eb_price[]" `
	Rprice              []string `form:"r_price[]" `
	PricingBasedOther   string   `form:"pricingbasedother" `
	SebEbRPrice         []string `form:"seb_eb_r_price[]" `
	AllDivisions        []string `form:"All_divisions[]" `
	AllBoattype         []string `form:"All_boattype[]" `
	AllBoatmeter        []string `form:"All_boatmeter[]" `
	AllDivClsBottyMeter []string `form:"all_div_cls_botty_meter[]" `
}

type eventEditClassDivisiopnValidate struct {
	Currency            string   `form:"currency"`
	SebDate             string   `form:"seb_date"`
	EbDate              string   `form:"eb_date"`
	RDate               string   `form:"r_date"`
	PricingBased        string   `form:"pricing_based" `
	SebPrice            []string `form:"seb_price[]" `
	EbPrice             []string `form:"eb_price[]" `
	Rprice              []string `form:"r_price[]" `
	PricingBasedOther   string   `form:"pricingbasedother" `
	SebEbRPrice         []string `form:"seb_eb_r_price[]" `
	AllDivisions        []string `form:"All_divisions[]" `
	AllBoattype         []string `form:"All_boattype[]" `
	AllBoatmeter        []string `form:"All_boatmeter[]" `
	AllDivClsBottyMeter []string `form:"all_div_cls_botty_meter[]" `
}

type updateClassesValidate struct {
	MXdivision     []string `form:"MX_event_divisions[]" `
	Mdivision      []string `form:"M_event_divisions[]" `
	Wdivision      []string `form:"W_event_divisions[]" `
	BoatTypeId     []string `form:"boat_type_id[]" `
	BoatDivisionId []string `form:"boat_division_id[]" `
	BoatClassId    []string `form:"boat_class_id[]" `
}

type eventCoOrgValidate struct {
	email   string `form:"email" valid:"Email"`
	message string `form:"message"`
}

type eventRehostValidate struct {
	RhStartDate string `form:"rh_start_date" `
	RhEndDate   string `form:"rh_end_date"`
}

type eventSearchValidate struct {
	EventName      string   `form:"event_name"`
	City           string   `form:"city"`
	Classes        string   `form:"classes" `
	From           string   `form:"date_from"`
	To             string   `form:"date_to"`
	Divisions      []string `form:"check[]"`
	Section        string   `form:"section"`
	UpcomingLimit  string   `form:"upcoming_limit"`
	UpcomingOffset string   `form:"upcoming_offset"`
	UpcomingType   string   `form:"upcoming_type"`
	RecentLimit    string   `form:"recent_limit"`
	RecentOffset   string   `form:"recent_offset"`
	RecentType     string   `form:"recent_type"`
	PastLimit      string   `form:"past_limit"`
	PastOffset     string   `form:"past_offset"`
	PastType       string   `form:"past_type"`
}

type eventTeamRegisterValidate struct {
	EventRegisterTeamid        string   `form:"eventregisterteamid"`
	SelectedDivisionArray      string   `form:"selectedDivisionArray" `
	SelectedClassArray         string   `form:"selectedClassArray" `
	SelectedRosterIdArray      string   `form:"selectedRosterIdArray" `
	SelectedBoatTypeArray      string   `form:"selectedBoatTypeArray" `
	AdditionalPracticeBool     string   `form:"additional_practice_bool" `
	AdditionalPracticeQuantity string   `form:"additional_practice_quantity" `
	Event_RegisterMessage      string   `form:"event_register_message"`
	IteamId                    []string `form:"iteam_id[]" `
	IteamIdQuantity            []string `form:"iteam_id_quantity[]" `
	OnlySelectedClassArray     string   `form:"onlyselectedClassArray" `
	OnlySelectedDivisionArray  string   `form:"onlyselectedDivisionArray" `
	OnlySelectedDistanceArray  string   `form:"onlyselectedDistanceArray" `
	CurrentCreateEventStep     string   `form:"currentregistrationstep" `
	SubmitRosterLater          string   `form:"submitrosterlater" `
	AdditionalQuestion         []string `form:"event_question[]" `
	AdditionalAnswer           []string `form:"event_question_answer[]" `
	ParticipantsCount          string   `form:"participantscount" `
	PerPersonQuantity          []string `form:"per_person_quantity[]" `
	PerPersonIteamId           []string `form:"per_person_iteam_id[]" `
	IteamName                  []string `form:"iteam_name[]" `
	PerPersonIteamName         []string `form:"per_person_iteam_name[]" `
	EventPaymentStatus         string   `form:"eventpaymentstatus"`
}

type eventclass_division_detail_struct struct {
	EventClassDivisionData []orm.Params `json:"eventclassdivisiondata"`
	AllDivisionData        []orm.Params `json:"alldivisiondata"`
	EventBoatType          []orm.Params `json:"eventboattype"`
}

type event_acc_role_struct struct {
	Userdetail          []map[string]string `json:"userdetails" `
	Allroleaccessdetail []map[string]string `json:"allroleaccessdetail" `
}

type eventreg_detail_struct struct {
	EventRegDetailData []map[string]orm.Params
}

type eventRosterSubmitLaterValidate struct {
	SelectedDivisionArray string `form:"selectedDivisionArray" `
	SelectedClassArray    string `form:"selectedClassArray" `
	SelectedRosterIdArray string `form:"selectedRosterIdArray" `
	SelectedDistanceArray string `form:"selectedDistanceArray" `
	RegisterEventId       string `form:"registerEventId" `
	EventSlug             string `form:"eventslug" `
}

type eventEditOrderValidate struct {
	PerPersonIteamId   []string `form:"per_person_iteam_id[]" `
	PerPersonQuantity  []string `form:"per_person_quantity[]" `
	AdditionalQuantity string   `form:"additional_quantity" `
	IteamIdQuantity    []string `form:"iteam_id_quantity[]" `
	IteamId            []string `form:"iteam_id[]" `
	RegisterEventId    string   `form:"registerEventId" `
	EventSlug          string   `form:"eventSlug" `
	Ordernote          string   `form:"ordernote" `
	EventId            string   `form:"eventId" `
	TeamId             string   `form:"registerTeamId" `
}
type event_payment_detail_struct struct {
	PayStatus   string `form:"paystatus" `
	ChoosenType string `form:"choosentype" `
}
type event_payment_team_detail_struct struct {
	Result []map[string]string `form:"result" `
}
type eventTeamSendEmail struct {
	DiscussionMsg      string `form:"discussion_msg"`
	SendEmailTo        string `form:"sendEmailTo"`
	Announcement       string `form:"announcement"`
	EventRegIds        string `form:"eventregIDs"`
	Geteventid         string `form:"eventID"`
	PlainDiscussionMsg string `form:"plain_discussion_msg"`
}

type accredationGeneralPrintDetailStruct struct {
	General_Members []string `form:"general_members[]" `
	Manual_Members  []string `form:"manual_members[]" `
}

var mutex sync.Mutex

func (c *EventController) CreateEvent() {

	//models.GetAllStockItems()
	c.TplName = "event/createEvent.html"
	c.Data["PageTitle"] = "Create Event"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	c.Data["create_event_1"] = true
	c.Data["create_event_2"] = false
	c.Data["create_event_3"] = false
	c.Data["create_event_4"] = false
	c.Data["create_event_5"] = false

	var all_divs []orm.Params
	all_divs = models.GetAllDivisions()
	if all_divs[0] != nil {
		c.Data["AllDivs"] = all_divs
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		user_id := c.GetSession("UserId").(int)
		isValid := models.CanCreateEvent(user_id)

		//c.Data["GetAllDivisionDetail"] = models.GetAllDivisionDetail()

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

		if isValid {
			c.Data["userActivity"] = true
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	fmt.Println(c.Ctx.Input.Method())

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := eventValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//v := make([]string, 0, 3)

		//c.Ctx.Input.Bind(&v, "M_seb_price")

		fmt.Printf("%+v\n", items)

		result := make(map[string]string)
		user_id := c.GetSession("UserId").(int)
		fmt.Println(user_id)
		data := make(map[string]string)
		data["EventName"] = items.EventName
		data["EventLocation"] = items.EventLocation
		data["EventProvince"] = items.EventProvince
		data["EventCountry"] = items.EventCountry
		data["EventVenue"] = items.EventVenue
		data["EventStreet"] = items.EventStreet
		data["Currency"] = items.Currency
		layout := "01/02/2006"
		if len(items.EventStartDate) > 0 {
			str := items.EventStartDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["EventStartDate"] = t_array[0]
			}
		}

		if len(items.EventEndDate) > 0 {
			str := items.EventEndDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["EventEndDate"] = t_array[0]
			}
		} else {
			data["EventEndDate"] = ""
		}

		if len(items.SebDate) > 0 {
			str := items.SebDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["SebDate"] = t_array[0]
				data["Seb"] = "yes"
			}
		} else {
			data["SebDate"] = ""
			data["Seb"] = "no"
		}

		if len(items.EbDate) > 0 {
			str := items.EbDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["EbDate"] = t_array[0]
				data["Eb"] = "yes"
			}
		} else {
			data["EbDate"] = ""
			data["Eb"] = "no"
		}

		if len(items.RDate) > 0 {
			str := items.RDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["RDate"] = t_array[0]
				data["Regular"] = "yes"
			}
		} else {
			data["RDate"] = ""
			data["Regular"] = "no"
		}

		data["EventPh"] = items.EventPh

		if items.EventTwitter != "" {
			data["EventTwitter"] = "https://www.twitter.com/" + items.EventTwitter
		} else {
			data["EventTwitter"] = items.EventTwitter
		}

		if items.EventFacebook != "" {
			data["EventFacebook"] = "https://www.facebook.com/" + items.EventFacebook
		} else {
			data["EventFacebook"] = items.EventFacebook
		}

		if items.EventYouTube != "" {
			data["EventYouTube"] = "https://www.youtube.com/" + items.EventYouTube
		} else {
			data["EventYouTube"] = items.EventYouTube
		}

		data["EventEmail"] = items.EventEmail

		if items.EventWebsite != "" {
			data["EventWebsite"] = items.EventWebsite
		} else {
			data["EventWebsite"] = items.EventWebsite
		}

		if items.CoachServiceCost == "" {
			data["CoachServiceCost"] = "0"
		} else {
			data["CoachServiceCost"] = items.CoachServiceCost
		}

		if items.AdditionalCostPractice == "" {
			data["AdditionalCostPractice"] = "0"
		} else {
			data["AdditionalCostPractice"] = items.AdditionalCostPractice
		}

		if items.ComplFreePractice == "" {
			data["ComplFreePractice"] = "0"
		} else {
			data["ComplFreePractice"] = items.ComplFreePractice
		}

		items.ItemTitle = models.DeleteEmptyElements(items.ItemTitle)
		items.ItemCost = models.DeleteEmptyElements(items.ItemCost)

		items.ItemCost = models.DeleteEmptyElements(items.ItemCost)
		items.ItemCost = models.DeleteEmptyElements(items.ItemCost)

		data["PricingBased"] = items.PricingBased
		items.SebPrice = models.DeleteEmptyElements(items.SebPrice)
		items.EbPrice = models.DeleteEmptyElements(items.EbPrice)
		items.Rprice = models.DeleteEmptyElements(items.Rprice)
		data["PricingBasedOther"] = items.PricingBasedOther
		items.SebEbRPrice = models.DeleteEmptyElements(items.SebEbRPrice)
		TempAllDivisions := models.DeleteEmptyElements(items.AllDivisions)
		TempAllBoattype := models.DeleteEmptyElements(items.AllBoattype)
		TempAllBoatmeter := models.DeleteEmptyElements(items.AllBoatmeter)
		items.AllDivClsBottyMeter = models.DeleteEmptyElements(items.AllDivClsBottyMeter)

		TempAllDivisionsString := strings.Join(TempAllDivisions, ",")
		TempAllBoattypeString := strings.Join(TempAllBoattype, ",")
		TempAllBoatmeterString := strings.Join(TempAllBoatmeter, ",")

		var ItemTitle []string
		var ItemCost []string

		if len(items.ItemTitle) > 0 {
			ItemTitle = items.ItemTitle
		}

		if len(items.ItemCost) > 0 {
			ItemCost = items.ItemCost
		}

		x, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))

		if err != nil {
			fmt.Println("fatal error: %s", err)
		}

		r := &maps.GeocodingRequest{
			Address: items.EventStreet + ", " + items.EventLocation + ", " + items.EventProvince + ", " + items.EventCountry,
		}

		resp, err := x.Geocode(NContext.Background(), r)

		if resp != nil {
			if len(resp) > 0 {
				data["Latitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lat, 'f', -1, 64)
				data["Longitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lng, 'f', -1, 64)
			}
		}

		reg, err := regexp.Compile("[^A-Za-z0-9À-ž]+")
		if err != nil {
			fmt.Println(err)
		}

		name_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.EventName), " "))
		loc_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.EventLocation), " "))

		data["Slug"] = strings.Replace(name_slug, " ", "-", -1) + "-" + strings.Replace(loc_slug, " ", "-", -1)

		result = models.CreateEvent(data, user_id, TempAllDivisionsString, TempAllBoattypeString, TempAllBoatmeterString, ItemTitle, ItemCost, items.EventQuestions, items.SebPrice, items.EbPrice, items.Rprice, items.SebEbRPrice, items.AllDivClsBottyMeter)

		if result["status"] == "1" {

			c.Data["completeRegister"] = true
			c.Redirect("/event-view/"+data["Slug"], 302)
		}
	}

}

func (c *EventController) EventList() {
	c.TplName = "event/eventList.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	var all_divs []orm.Params
	all_divs = models.GetAllDivisions()
	if all_divs[0] != nil {
		c.Data["AllDivs"] = all_divs
	}

	var result []orm.Params

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := eventSearchValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		//c.Redirect("/dashboard", 302)

		fmt.Println(items)

		data := make(map[string]string)

		data["EventName"] = items.EventName
		data["City"] = items.City

		data["Classes"] = items.Classes
		div_string := strings.Join(items.Divisions[:], ",")
		data["Divisions"] = div_string

		data["From"] = items.From
		data["To"] = items.To

		data["UpcomingLimit"] = items.UpcomingLimit
		data["UpcomingOffset"] = items.UpcomingOffset
		data["UpcomingType"] = items.UpcomingType

		data["RecentLimit"] = items.RecentLimit
		data["RecentOffset"] = items.RecentOffset
		data["RecentType"] = items.RecentType

		data["PastLimit"] = items.PastLimit
		data["PastOffset"] = items.PastOffset
		data["PastType"] = items.PastType

		c.Data["NewUpcomingOffset"] = items.UpcomingOffset
		c.Data["NewUpcomingLimit"] = items.UpcomingLimit
		c.Data["UpcomingType"] = items.UpcomingType

		c.Data["NewRecentOffset"] = items.RecentOffset
		c.Data["NewRecentLimit"] = items.RecentLimit
		c.Data["RecentType"] = items.RecentType

		c.Data["NewPastOffset"] = items.PastOffset
		c.Data["NewPastLimit"] = items.PastLimit
		c.Data["PastType"] = items.PastType

		//c.Data["Section"] = items.Section

		var past_events []orm.Params
		var recent_events []orm.Params
		var upcoming_events []orm.Params

		c.Data["AdvSearch"] = false

		for k, v := range data {

			if k == "Classes" {
				if v != "any" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "Divisions" {
				if len(v) > 0 {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "City" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "From" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			} else if k == "To" {
				if v != "" {
					c.Data["AdvSearch"] = true
					break
				}
			}

		}

		upcoming_events, totalUpcomingCount := models.SearchUpcomingEvent(data)
		recent_events, totalRecentCount := models.SearchRecentEvent(data)
		past_events, totalPastCount := models.SearchPastEvent(data)

		//past_events, recent_events, upcoming_events, err := CategoriseEvents(result)

		if upcoming_events[0] != nil || recent_events[0] != nil || past_events[0] != nil {
			c.Data["EventList"] = true
			c.Data["AllEvents"] = result

			if past_events[0] != nil {
				c.Data["PastEvents"] = past_events
				c.Data["NumPastEvents"] = totalPastCount
			} else {
				c.Data["NumPastEvents"] = "0"
			}

			if recent_events[0] != nil {
				c.Data["RecentEvents"] = recent_events
				c.Data["NumRecentEvents"] = totalRecentCount
			} else {
				c.Data["NumRecentEvents"] = "0"
			}

			if upcoming_events[0] != nil {
				c.Data["UpcomingEvents"] = upcoming_events
				c.Data["NumUpcomingEvents"] = totalUpcomingCount
			} else {
				c.Data["NumUpcomingEvents"] = "0"
			}

			c.Data["TotalUpcomingEvents"] = totalUpcomingCount
			currUpcomingPage, upcomingPages, upcomingFivePages := CalculatePages(items.UpcomingLimit, items.UpcomingOffset, items.UpcomingType, totalUpcomingCount)
			c.Data["TotalPagesUpcoming"] = upcomingPages
			fmt.Println(upcomingPages)
			c.Data["CurrPageUpcoming"] = currUpcomingPage
			c.Data["FivePagesUpcoming"] = upcomingFivePages

			c.Data["TotalRecentEvents"] = totalRecentCount
			currRecentPage, recentPages, recentFivePages := CalculatePages(items.RecentLimit, items.RecentOffset, items.RecentType, totalRecentCount)
			c.Data["TotalPagesRecent"] = recentPages
			fmt.Println(recentPages)
			c.Data["CurrPageRecent"] = currRecentPage
			c.Data["FivePagesRecent"] = recentFivePages

			c.Data["TotalPastEvents"] = totalPastCount
			currPastPage, pastPages, pastFivePages := CalculatePages(items.PastLimit, items.PastOffset, items.PastType, totalPastCount)
			c.Data["TotalPagesPast"] = pastPages
			fmt.Println(pastPages)
			c.Data["CurrPagePast"] = currPastPage
			c.Data["FivePagesPast"] = pastFivePages

			if upcoming_events[0] == nil {
				if recent_events[0] == nil {
					c.Data["Section"] = "Past"

				} else {
					c.Data["Section"] = "Recent"
				}
			} else {
				c.Data["Section"] = "Upcoming"
			}

		} else {
			c.Data["NumPastEvents"] = "0"
			c.Data["NumRecentEvents"] = "0"
			c.Data["NumUpcomingEvents"] = "0"

			c.Data["Section"] = "Upcoming"
		}

	} else {

		var past_events []orm.Params
		var recent_events []orm.Params
		var upcoming_events []orm.Params

		upcoming_events, totalUpcomingCount := models.SearchUpcomingEvent(nil)
		recent_events, totalRecentCount := models.SearchRecentEvent(nil)
		past_events, totalPastCount := models.SearchPastEvent(nil)

		//past_events, recent_events, upcoming_events, err := CategoriseEvents(result)

		if upcoming_events[0] != nil || recent_events[0] != nil || past_events[0] != nil {
			c.Data["EventList"] = true
			c.Data["AllEvents"] = result

			if past_events[0] != nil {
				c.Data["PastEvents"] = past_events
				c.Data["NumPastEvents"] = totalPastCount
			} else {
				c.Data["NumPastEvents"] = "0"
			}

			if recent_events[0] != nil {
				c.Data["RecentEvents"] = recent_events
				c.Data["NumRecentEvents"] = totalRecentCount
			} else {
				c.Data["NumRecentEvents"] = "0"
			}

			if upcoming_events[0] != nil {
				c.Data["UpcomingEvents"] = upcoming_events
				c.Data["NumUpcomingEvents"] = totalUpcomingCount
			} else {
				c.Data["NumUpcomingEvents"] = "0"
			}

			totalPages := int(math.Ceil(float64(totalUpcomingCount) / float64(10)))
			fmt.Println("Pages ", totalPages)
			fmt.Println("Total Upcoming events ", totalUpcomingCount)

			pages := make(map[int]int)
			offset := 0
			condition := 0

			if totalPages > 5 {
				condition = 5
			} else {
				condition = totalPages
			}

			for i := 1; i <= condition; i++ {
				pages[i] = offset

				offset = offset + 10
			}
			c.Data["TotalPagesUpcoming"] = pages

			tmp := make(map[int]int)

			for i := 1; i <= condition; i++ {
				tmp[i] = pages[i]
			}

			c.Data["FivePagesUpcoming"] = tmp
			c.Data["CurrPageUpcoming"] = 1

			totalPages = int(math.Ceil(float64(totalRecentCount) / float64(10)))
			fmt.Println("Pages ", totalPages)
			fmt.Println("Total Recent events ", totalRecentCount)

			pages = make(map[int]int)
			offset = 0

			if totalPages > 5 {
				condition = 5
			} else {
				condition = totalPages
			}

			for i := 1; i <= condition; i++ {
				pages[i] = offset

				offset = offset + 10
			}
			c.Data["TotalPagesRecent"] = pages

			tmp = make(map[int]int)

			for i := 1; i <= condition; i++ {
				tmp[i] = pages[i]
			}

			c.Data["FivePagesRecent"] = tmp
			c.Data["CurrPageRecent"] = 1

			totalPages = int(math.Ceil(float64(totalPastCount) / float64(10)))
			fmt.Println("Pages ", totalPages)
			fmt.Println("Total Recent events ", totalPastCount)

			pages = make(map[int]int)
			offset = 0

			if totalPages > 5 {
				condition = 5
			} else {
				condition = totalPages
			}

			for i := 1; i <= condition; i++ {
				pages[i] = offset

				offset = offset + 10
			}
			c.Data["TotalPagesPast"] = pages

			tmp = make(map[int]int)

			for i := 1; i <= condition; i++ {
				tmp[i] = pages[i]
			}

			c.Data["FivePagesPast"] = tmp
			c.Data["CurrPagePast"] = 1

			if upcoming_events[0] == nil {
				if recent_events[0] == nil {
					c.Data["Section"] = "Past"

				} else {
					c.Data["Section"] = "Recent"
				}
			} else {
				c.Data["Section"] = "Upcoming"
			}

		} else {
			c.Data["NumPastEvents"] = "0"
			c.Data["NumRecentEvents"] = "0"
			c.Data["NumUpcomingEvents"] = "0"
			c.Data["Section"] = "Upcoming"
		}

		c.Data["AdvSearch"] = false

	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func CalculatePages(limit string, offset_string string, type_search string, totalCount int64) (int, int, map[int]int) {
	i, _ := strconv.ParseInt(limit, 10, 64)

	var OffsetInt, LimitInt = 0, 0
	num, err := strconv.Atoi(limit)
	if err == nil {
		LimitInt = num
	}
	num, err = strconv.Atoi(offset_string)
	if err == nil {
		OffsetInt = num
	}

	totalPages := int(math.Ceil(float64(totalCount) / float64(i)))
	fmt.Println("Pages ", totalPages)
	fmt.Println("Total events ", totalCount)

	pages := make(map[int]int)
	offset := 0
	for i := 1; i <= totalPages; i++ {
		pages[i] = offset

		offset = offset + LimitInt
	}
	//c.Data["TotalPagesUpcoming"] = pages
	fmt.Println(pages)

	tmp := make(map[int]int)
	var currPage int

	if type_search != "last" {
		for k, v := range pages {

			if v == OffsetInt {
				//c.Data["CurrPageUpcoming"] = k
				currPage = k
				break
			}

		}
	} else {
		//c.Data["CurrPageUpcoming"] = totalPages
		currPage = totalPages
	}

	if currPage == 0 {
		for k, _ := range pages {
			if k+1 <= len(pages) {
				fmt.Println(pages[k+1], " > ", OffsetInt)
				if pages[k+1] > OffsetInt {
					//c.Data["CurrPageUpcoming"] = k
					currPage = k
					break
				}
			}
		}
	}

	fmt.Println(" CurrPage", currPage)

	if currPage < 4 {

		if len(pages) >= 5 {
			for i := 1; i <= 5; i++ {
				tmp[i] = pages[i]
			}
		} else {
			for i := 1; i <= len(pages); i++ {
				tmp[i] = pages[i]
			}
		}

	} else {

		if currPage+2 <= totalPages {
			for i := currPage - 2; i <= currPage+2; i++ {
				tmp[i] = pages[i]
			}
		} else {
			for i := currPage - 3; i <= totalPages; i++ {
				tmp[i] = pages[i]
			}
		}

	}

	return currPage, totalPages, tmp

}
func (c *EventController) SendEventTeamEmail() {

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := eventTeamSendEmail{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		var event_reg_ids string
		var email_to, message, send_announcement, event_id string

		event_reg_ids = items.EventRegIds
		email_to = items.SendEmailTo
		message = items.DiscussionMsg
		send_announcement = items.Announcement
		event_id = items.Geteventid

		userID := "0"
		if models.IsLoggedIn(c.GetSession("GushouSession")) {
			id := c.GetSession("UserId").(int)
			userID = strconv.Itoa(id)
		}

		send_event_mails := models.SendEventTeamEmail(event_reg_ids, email_to, message, userID, send_announcement, event_id)
		c.Ctx.Output.Body([]byte(send_event_mails))
	}
}
func (c *EventController) EventView() {
	c.TplName = "event/eventView.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	fmt.Println(c.Ctx.Input.Param(":slug"))
	slug := c.Ctx.Input.Param(":slug")

	clickedtabname := c.Ctx.Input.Param(":tabname")

	event_view := make(map[string]orm.Params)
	event_view = models.GetEventBySlug(slug)
	if _, ok := event_view["status"]; !ok {

		c.SetSession("EventId", event_view["event_0"]["id"].(string))

		if event_view["event_0"]["divisions"] != nil {
			s := strings.Replace(event_view["event_0"]["divisions"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Divisions"] = strings.Split(s, ",")
		}

		if event_view["event_0"]["classes"] != nil {
			s := strings.Replace(event_view["event_0"]["classes"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Classes"] = strings.Split(s, ",")
		}

		var maleDivision []string
		var womanDivision []string
		var mixedDivision []string
		var divisionArray []string

		c.Data["MaleDivision"] = maleDivision
		c.Data["FemaleDivision"] = womanDivision
		c.Data["MixedDivision"] = mixedDivision

		for _, v := range event_view {
			//key := "class_" + strconv.Itoa(k)
			if v["class"] == "M" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MDivisions"] = strings.Split(s, ",")
					maleDivision = strings.Split(s, ",")
					c.Data["MDiv"] = true
					c.Data["MaleDivision"] = maleDivision
				}

			} else if v["class"] == "W" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["WDivisions"] = strings.Split(s, ",")
					womanDivision = strings.Split(s, ",")
					c.Data["FemaleDivision"] = womanDivision
					c.Data["WDiv"] = true
				}

			} else if v["class"] == "MX" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					c.Data["MXDivisions"] = strings.Split(s, ",")
					mixedDivision = strings.Split(s, ",")
					c.Data["MixedDivision"] = mixedDivision
					c.Data["MXDiv"] = true
				}

			}

		}
		if models.IsLoggedIn(c.GetSession("GushouSession")) {

			if c.GetSession("EventRegistrationDone") == "true" {
				c.Data["CompleteEventRegistrationStatus"] = "true"
				c.DelSession("EventRegistrationDone")
			}
			c.DelSession("readyToPaymentStatus")
			if c.GetSession("EventRegistrationPaymentDone") == "true" {
				c.Data["CompleteEventRegistrationPaymentDone"] = "true"
				c.DelSession("EventRegistrationPaymentDone")
			}

			for _, v := range maleDivision {
				tempStaus := containsInArray(divisionArray, v)
				if !tempStaus {
					divisionArray = append(divisionArray, v)
				}
			}

			for _, v := range womanDivision {
				tempStaus := containsInArray(divisionArray, v)
				if !tempStaus {
					divisionArray = append(divisionArray, v)
				}
			}

			for _, v := range mixedDivision {
				tempStaus := containsInArray(divisionArray, v)
				if !tempStaus {
					divisionArray = append(divisionArray, v)
				}
			}
		}

		c.Data["DivisionArray"] = divisionArray
		c.Data["EventView"] = event_view
		var contact []int

		if event_view["event_0"]["twitter_handle"] != nil {
			if len(event_view["event_0"]["twitter_handle"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}
		} else {
			contact = append(contact, 0)
		}

		if event_view["event_0"]["facebook_page_url"] != nil {
			if len(event_view["event_0"]["facebook_page_url"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if event_view["event_0"]["youtube_url"] != nil {
			if len(event_view["event_0"]["youtube_url"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if event_view["event_0"]["phone_number"] != nil {
			if len(event_view["event_0"]["phone_number"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if event_view["event_0"]["email"] != nil {
			if len(event_view["event_0"]["email"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		if event_view["event_0"]["website_url"] != nil {
			if len(event_view["event_0"]["website_url"].(string)) == 0 {
				contact = append(contact, 0)
			} else {
				contact = append(contact, 1)
			}

		} else {
			contact = append(contact, 0)
		}

		for _, val := range contact {
			if val == 1 {
				c.Data["ContactSection"] = true
				break
			} else {
				c.Data["ContactSection"] = false
			}
		}

	} else {
		fmt.Println(" No event by the slug")
		c.Redirect("/", 302)
		return
	}
	//fmt.Println(event_view)

	if models.IsLoggedIn(c.GetSession("GushouSession")) {

		event_questionary := make(map[int]orm.Params)
		event_questionary = models.GetEventQuestionary(event_view["event_0"]["id"].(string))
		if event_questionary[1] != nil {
			c.Data["EventQuestionary"] = event_questionary
		}

		id := c.GetSession("UserId").(int)
		fmt.Println(id)
		userId := strconv.Itoa(id)
		var TeamListEvent []orm.Params
		TeamListEvent = models.GetTeamListConcernUserByUserId(userId)
		c.Data["TeamListEvent"] = TeamListEvent

		free_practice := make(map[string]orm.Params)
		free_practice = models.GetEventFreePractice(event_view["event_0"]["id"].(string))
		c.Data["FreePracticeStatus"] = "false"
		if _, ok := free_practice["status"]; !ok {

			for _, v := range free_practice {
				fmt.Println("v[quantity]", v["quantity"])
				if v["quantity"] != "0" {
					c.Data["FreePracticeStatus"] = "true"
				}
			}
			c.Data["FreePractice"] = free_practice
		}

		additonal_practice := make(map[string]orm.Params)
		additonal_practice = models.GetEventAdditionalPractice(event_view["event_0"]["id"].(string))
		if _, ok := additonal_practice["status"]; !ok {
			c.Data["AdditionalPractice"] = additonal_practice
		}

		other_pricing := make(map[int]orm.Params)
		other_pricing = models.GetEventOtherPricing(event_view["event_0"]["id"].(string))
		if other_pricing[1] != nil {
			c.Data["OtherPricing"] = other_pricing
		}

		var eventRegistration []orm.Params
		eventRegistration = models.GetEventRegistrationDetail(event_view["event_0"]["id"].(string))
		c.Data["EventRegistration"] = eventRegistration
	}

	event_logo := models.GetEventLogo(event_view["event_0"]["id"].(string))
	if event_logo != "" {
		c.Data["EventLogo"] = event_logo
	}

	event_organisers := make(map[string]orm.Params)
	event_organisers = models.GetEventOrganisers(event_view["event_0"]["id"].(string))
	if _, ok := event_organisers["status"]; !ok {
		c.Data["EventOrganisers"] = event_organisers

		var event_og_id []string
		for _, v := range event_organisers {
			event_og_id = append(event_og_id, v["fk_user_id"].(string))
		}

		if len(event_og_id) > 0 {
			c.Data["EventOrgIds"] = event_og_id
		}
	}

	event_faq := make(map[string]orm.Params)
	event_faq = models.GetEventFaq(event_view["event_0"]["id"].(string))
	if _, ok := event_faq["status"]; !ok {
		c.Data["EventFaq"] = event_faq
	}

	//fmt.Println(c.Data["EventFaq"])

	event_pricing := make(map[string]orm.Params)
	men_pricing := make(map[string]orm.Params)
	women_pricing := make(map[string]orm.Params)
	mixed_pricing := make(map[string]orm.Params)

	event_pricing = models.GetEventPricing(event_view["event_0"]["id"].(string), "view")
	c.Data["EventPricingDetail"] = event_pricing
	if _, ok := event_pricing["status"]; !ok {
		c.Data["EventPricing"] = true

		for k, v := range event_pricing {
			if v["pricing_class"] == "M" {
				men_pricing[k] = v
			} else if v["pricing_class"] == "W" {
				women_pricing[k] = v
			} else if v["pricing_class"] == "MX" {
				mixed_pricing[k] = v
			}
		}

		c.Data["MenPricing"] = men_pricing
		c.Data["WomenPricing"] = women_pricing
		c.Data["MixedPricing"] = mixed_pricing

	}

	event_announcement := make(map[string]orm.Params)
	event_announcement = models.GetEventAnnouncement(event_view["event_0"]["id"].(string))
	if _, ok := event_announcement["status"]; !ok {
		c.Data["EventAnnouncement"] = event_announcement
	}

	event_sponsor := make(map[string]orm.Params)
	event_sponsor = models.GetEventSponsor(event_view["event_0"]["id"].(string))
	if _, ok := event_sponsor["status"]; !ok {
		c.Data["EventSponsor"] = event_sponsor
	}

	var event_version []orm.Params
	event_version = models.GetEventVersion(event_view["event_0"]["id"].(string))
	if event_version != nil {
		c.Data["EventVersion"] = event_version

		if event_version[0]["id"].(string) == event_view["event_0"]["id"].(string) {
			c.Data["Latest"] = true
		}
	}

	rehost := models.CanRehost(event_view["event_0"]["id"].(string))
	if rehost {
		c.Data["Rehost"] = true
		c.Data["Claim"] = true
	}

	//Rehost an event

	if c.Ctx.Input.Method() == "POST" && !strings.HasPrefix(c.Ctx.Input.URL(), "/add-announcement") && clickedtabname != "event-payment-view" && clickedtabname != "event-accreditation-general-view" && clickedtabname != "event-waivers" && clickedtabname != "event-reports" {
		flash := beego.NewFlash()
		items := eventRehostValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		data := make(map[string]string)
		data["RhStartDate"] = items.RhStartDate
		data["RhEndDate"] = items.RhEndDate

		rehost_result := make(map[string]string)
		event_id := c.GetSession("EventId").(string)
		rehost_result = models.RehostEvent(data, user_id, event_id, "rehost")

		if rehost_result["status"] == "1" {

			c.Redirect("/event-view/"+slug, 302)
		}

	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {

		id := c.GetSession("UserId").(int)
		fmt.Println(id)
		tempuserId := strconv.Itoa(id)
		eventOrganizer, CoeventOrganizer := models.CheckUserIsEventOrCoEventOrganizer(event_view["event_0"]["id"].(string), tempuserId)
		c.Data["EventOranizerStatus"] = eventOrganizer
		c.Data["CoEventOranizerStatus"] = CoeventOrganizer

		if clickedtabname == "event-teams-view" {
			tempResultData := models.GetEventRegisterTeamDetail(event_view["event_0"]["id"].(string))
			tempResultRosterStatusAscendingData := tempResultData
			c.Data["Event_Register_Team_Detail_List"] = tempResultData

			c.Data["Event_Register_Team_Detail_List_Order_By_Team"] = models.GetEventRegisterTeamDetailOrderByTeam(event_view["event_0"]["id"].(string))
			sort.Slice(tempResultRosterStatusAscendingData, func(i, j int) bool {
				tempcase1 := ""
				tempcase2 := ""
				if tempResultRosterStatusAscendingData[i]["roster_incomplete_complete"] != nil {
					tempcase1 = tempResultRosterStatusAscendingData[i]["roster_incomplete_complete"]["case"].(string)
				} else if tempResultRosterStatusAscendingData[i]["roster_partial"] != nil {
					tempcase1 = tempResultRosterStatusAscendingData[i]["roster_partial"]["case"].(string)
				}

				if tempResultRosterStatusAscendingData[j]["roster_incomplete_complete"] != nil {
					tempcase2 = tempResultRosterStatusAscendingData[j]["roster_incomplete_complete"]["case"].(string)
				} else if tempResultRosterStatusAscendingData[j]["roster_partial"] != nil {
					tempcase2 = tempResultRosterStatusAscendingData[j]["roster_partial"]["case"].(string)
				}

				return tempcase2 > tempcase1
			})

			c.Data["Event_Register_Team_Detail_List_Order_By_Roster"] = tempResultRosterStatusAscendingData
		} else if clickedtabname == "event-payment-view" {

			var eventPaymentList []map[string]orm.Params
			var tempPayStatusAscendingData []map[string]orm.Params
			var tempBalanceAmtAscendingData []map[string]orm.Params
			var eventPaymentList_sortByTeamName []map[string]orm.Params
			mode := big.ToNearestEven
			if c.Ctx.Input.Method() == "POST" {
				var teamName, filterall, filterpaid, filterunpaid, filterpartial string

				c.Ctx.Input.Bind(&teamName, "teamName")
				c.Ctx.Input.Bind(&filterall, "all_filter")
				c.Ctx.Input.Bind(&filterpaid, "paid_filter")
				c.Ctx.Input.Bind(&filterunpaid, "unpaid_filter")
				c.Ctx.Input.Bind(&filterpartial, "partial_filter")

				eventPaymentList, tempPayStatusAscendingData, tempBalanceAmtAscendingData = models.GetEventPaymentList(event_view["event_0"]["id"].(string), teamName, filterpaid, filterunpaid, filterpartial)

				eventPaymentList_sortByTeamName = models.GetEventRegisterTeamDetail_sortByTeamName(event_view["event_0"]["id"].(string), teamName, filterpaid, filterunpaid, filterpartial)

				c.Data["FilterTeamName"] = teamName
				c.Data["FilterPaid"] = filterpaid
				c.Data["FilterUnpaid"] = filterunpaid
				c.Data["FilterPartial"] = filterpartial
				c.Data["FilterAll"] = filterall
			} else {
				eventPaymentList, tempPayStatusAscendingData, tempBalanceAmtAscendingData = models.GetEventPaymentList(event_view["event_0"]["id"].(string), "", "", "", "")
				eventPaymentList_sortByTeamName = models.GetEventRegisterTeamDetail_sortByTeamName(event_view["event_0"]["id"].(string), "", "", "", "")

			}

			// tempResultData := models.GetEventRegisterTeamDetail(event_view["event_0"]["id"].(string))
			// c.Data["Event_Register_Team_Detail_List"] = tempResultData

			unit, err_unit := currency.ParseISO(event_view["event_0"]["currency"].(string))
			if err_unit == nil {
				currencySymbol := currency.Symbol(unit)
				c.Data["CurrencySymbol"] = currencySymbol
			}

			c.Data["EventPaymentList"] = eventPaymentList
			c.Data["EventPaymentList_SortByTeamName"] = eventPaymentList_sortByTeamName

			sort.Slice(tempPayStatusAscendingData, func(i, j int) bool {
				tempcase1 := ""
				tempcase2 := ""

				if tempPayStatusAscendingData[i]["registerEventPaymentData"]["paymentstatus"] != nil {
					tempcase1 = tempPayStatusAscendingData[i]["registerEventPaymentData"]["paymentstatus"].(string)
				}
				if tempPayStatusAscendingData[j]["registerEventPaymentData"]["paymentstatus"] != nil {
					tempcase2 = tempPayStatusAscendingData[j]["registerEventPaymentData"]["paymentstatus"].(string)
				}

				return tempcase2 < tempcase1
			})
			c.Data["EventPaymentList_SortByPayStatus"] = tempPayStatusAscendingData

			sort.Slice(tempBalanceAmtAscendingData, func(i, j int) bool {
				tempcase1 := "0.00"
				tempcase2 := "0.00"
				var temp1 *big.Float
				var temp2 *big.Float
				var temps1 float64
				var temps2 float64
				if tempBalanceAmtAscendingData[i]["registerEventPaymentData"]["balance_amount"] != nil {

					tempcase1 = tempBalanceAmtAscendingData[i]["registerEventPaymentData"]["balance_amount"].(string)
					amount_paid := new(big.Rat)
					amount_paid.SetString(tempcase1)
					temp1 = new(big.Float).SetPrec(0).SetMode(mode).SetRat(amount_paid)

					if s, err := strconv.ParseFloat(temp1.String(), 64); err == nil {
						fmt.Println(s)
						temps1 = s
					}
				}
				if tempBalanceAmtAscendingData[j]["registerEventPaymentData"]["balance_amount"] != nil {
					tempcase2 = tempBalanceAmtAscendingData[j]["registerEventPaymentData"]["balance_amount"].(string)
					amount_paid := new(big.Rat)
					amount_paid.SetString(tempcase2)
					temp2 = new(big.Float).SetPrec(0).SetMode(mode).SetRat(amount_paid)

					if s, err := strconv.ParseFloat(temp2.String(), 64); err == nil {
						fmt.Println(s)
						temps2 = s
					}
				}

				return temps1 > temps2
			})
			c.Data["EventPaymentList_SortByBalanceAmt"] = tempBalanceAmtAscendingData

			fmt.Println("eventId", event_view["event_0"]["id"].(string))
			eventOverallCalData := models.EventOverallAmountCalculation(event_view["event_0"]["id"].(string))

			c.Data["TotalFeechangeamount"] = models.GetTotalFeeChange(event_view["event_0"]["id"].(string))

			c.Data["EventOverallCalculation"] = eventOverallCalData
		} else if clickedtabname == "event-accreditation-general-view" {

			var tempResultGeneralData []map[string]string
			var tempResultGeneralNameAscendingData []map[string]string

			if c.Ctx.Input.Method() == "POST" {

				var accsearchtext, accbyteamname, accbyname, accbyrole, accbyaccess string

				c.Ctx.Input.Bind(&accsearchtext, "accsearchtext")
				c.Ctx.Input.Bind(&accbyteamname, "accbyteamname")
				c.Ctx.Input.Bind(&accbyname, "accbyname")
				c.Ctx.Input.Bind(&accbyrole, "accbyrole")
				c.Ctx.Input.Bind(&accbyaccess, "accbyaccess")

				tempResultGeneralData, tempResultGeneralNameAscendingData = models.GetAccreditationGeneralDetail(event_view["event_0"]["id"].(string), accsearchtext, accbyteamname, accbyname, accbyrole, accbyaccess)

				c.Data["AccSearchText"] = accsearchtext
				c.Data["AccbyTeamName"] = accbyteamname
				c.Data["AccbyName"] = accbyname
				c.Data["AccbyRole"] = accbyrole
				c.Data["AccbyAccess"] = accbyaccess

			} else {
				tempResultGeneralData, tempResultGeneralNameAscendingData = models.GetAccreditationGeneralDetail(event_view["event_0"]["id"].(string), "", "", "", "", "")
			}

			c.Data["All_Role"] = models.GetAllRoleDetail()
			c.Data["All_Access"] = models.GetAllAccessDetail()

			sort.Slice(tempResultGeneralData, func(i, j int) bool {
				tempteamname1 := ""
				tempteamname2 := ""

				if tempResultGeneralData[i]["teamname"] != "" {
					tempteamname1 = tempResultGeneralData[i]["teamname"]
				}

				if tempResultGeneralData[j]["teamname"] != "" {
					tempteamname2 = tempResultGeneralData[j]["teamname"]
				}

				return strings.ToLower(tempteamname2) > strings.ToLower(tempteamname1)
			})

			c.Data["AccreditationGeneralList"] = tempResultGeneralData

			sort.Slice(tempResultGeneralNameAscendingData, func(i, j int) bool {
				tempfirstlastname1 := ""
				tempfirstlastname2 := ""

				tempfirst1 := ""
				tempfirst2 := ""

				tempfirst11 := ""
				tempfirst22 := ""

				if tempResultGeneralNameAscendingData[i]["firstname"] != "" {
					tempfirst1 = tempResultGeneralNameAscendingData[i]["firstname"]
				}
				if tempResultGeneralNameAscendingData[i]["lastname"] != "" {
					tempfirst2 = tempResultGeneralNameAscendingData[i]["lastname"]
				}

				if tempResultGeneralNameAscendingData[j]["firstname"] != "" {
					tempfirst11 = tempResultGeneralNameAscendingData[j]["firstname"]
				}
				if tempResultGeneralNameAscendingData[j]["lastname"] != "" {
					tempfirst22 = tempResultGeneralNameAscendingData[j]["lastname"]
				}

				tempfirstlastname1 = strings.ToLower(tempfirst1) + " " + strings.ToLower(tempfirst2)

				tempfirstlastname2 = strings.ToLower(tempfirst11) + " " + strings.ToLower(tempfirst22)

				return tempfirstlastname2 > tempfirstlastname1
			})

			c.Data["AccreditationGeneralTeamnameListAsc"] = tempResultGeneralNameAscendingData

			c.Data["AccreditationGeneralTab"] = "true"
		} else if clickedtabname == "event-accreditation-manual-view" {
			teamAccreditationManualList, teamAccreditationManualList1 := models.GetAccreditationManualDetail(event_view["event_0"]["id"].(string))

			c.Data["AccreditationManualList"] = teamAccreditationManualList

			sort.Slice(teamAccreditationManualList1, func(i, j int) bool {
				tempteamname1 := ""
				tempteamname2 := ""

				if teamAccreditationManualList1[i]["name"] != "" {
					tempteamname1 = teamAccreditationManualList1[i]["name"]
				}

				if teamAccreditationManualList1[j]["name"] != "" {
					tempteamname2 = teamAccreditationManualList1[j]["name"]
				}

				return strings.ToLower(tempteamname2) > strings.ToLower(tempteamname1)
			})

			c.Data["AccreditationManualNameSortList"] = teamAccreditationManualList1

			c.Data["AccreditationMAnualTab"] = "true"
			c.Data["All_Role"] = models.GetAllRoleDetail()
			c.Data["All_Access"] = models.GetAllAccessDetail()

		} else if clickedtabname == "event-waivers" {

			event_name := models.GetEventName(event_view["event_0"]["id"].(string))
			c.Data["EventName"] = event_name
			c.Data["EventId"] = event_view["event_0"]["id"].(string)

			eventWaiverList, flag := models.GetEventWaiverList(event_view["event_0"]["id"].(string))
			c.Data["EventWaiverList"] = eventWaiverList
			c.Data["EventWaiverListflag"] = flag

		} else if clickedtabname == "event-reports" {

			if c.Ctx.Input.Method() == "POST" {
				var event_reg_id, race string
				//var event_reg_id_array []string

				c.Ctx.Input.Bind(&event_reg_id, "getMulTeamName")
				fmt.Println(event_reg_id)

				c.Ctx.Input.Bind(&race, "getMulRaceType")
				fmt.Println(race)
				c.Data["EventId"] = event_view["event_0"]["id"].(string)
				if event_reg_id != "" {

					result, team_name := models.GetRosterDetailForTeam(event_reg_id)
					c.Data["ReportTeamName"] = team_name
					c.Data["ReportForIndividualTeam"] = result

				}

				if race != "" {

					result, race_name := models.GetRosterDetailForRace(race, event_view["event_0"]["id"].(string))
					c.Data["ReportRaceTypeName"] = race_name
					c.Data["ReportForRaceType"] = result

				}

			}

			c.Data["ReportTeamNameList"] = models.GetEventReportList(event_view["event_0"]["id"].(string))
			c.Data["ReportRaceList"] = models.GetEventAllRace(event_view["event_0"]["id"].(string))

		}

		c.Data["Event_Register_Team_Count"] = models.GetEventRegisterCount(event_view["event_0"]["id"].(string))
		c.Data["StripeConnectedStatus"] = "false"
		if eventOrganizer {
			stripeConnectedStatus := models.CheckAlreadyConnetWithStripeToHideButton(event_view["event_0"]["id"].(string))
			c.Data["StripeConnectedStatus"] = stripeConnectedStatus
		}

		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		if event_view["event_0"]["fk_organizer_id"] != nil {
			c.Data["EventOrg"] = event_view["event_0"]["fk_organizer_id"].(string)
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *EventController) EventAll() {
	c.TplName = "event/eventsAll.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	user_id := c.GetSession("UserId").(int)

	my_events := make(map[string]orm.Params)
	my_events = models.GetEvent(user_id, false)
	if _, ok := my_events["status"]; !ok {
		c.Data["MyEvent"] = my_events
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *EventController) EditEvent() {
	c.TplName = "event/eventEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	var all_divs []orm.Params
	all_divs = models.GetAllDivisions()
	if all_divs[0] != nil {
		c.Data["AllDivs"] = all_divs
	}

	if c.GetSession("UpdateSuccess") == "true" {
		c.Data["UpdateSuccess"] = true
		c.DelSession("UpdateSuccess")
	}

	if c.GetSession("EmptyClass") == "true" {
		c.Data["EmptyClass"] = true
		c.DelSession("EmptyClass")
	}

	fmt.Println(c.Ctx.Input.Param(":slug"))
	slug := c.Ctx.Input.Param(":slug")

	event_view := make(map[string]orm.Params)
	event_view = models.GetEventBySlug(slug)
	if _, ok := event_view["status"]; !ok {

		c.SetSession("EventId", event_view["event_0"]["id"].(string))

		c.Data["AllowEditStatus"] = true

		teamDateEvent := event_view["event_0"]["start_date"].(string)

		if event_view["event_0"]["end_date"] != nil {
			teamDateEvent = event_view["event_0"]["end_date"].(string)
		}
		id := c.GetSession("UserId").(int)
		fmt.Println(id)
		tempuserId := strconv.Itoa(id)

		eventOrganizer, CoeventOrganizer := models.CheckUserIsEventOrCoEventOrganizer(event_view["event_0"]["id"].(string), tempuserId)
		c.Data["EventOranizerStatus"] = eventOrganizer
		c.Data["CoEventOranizerStatus"] = CoeventOrganizer
		fmt.Println("event organizer---", eventOrganizer)
		if eventOrganizer {
			stripeConnectedStatus := models.CheckAlreadyConnetWithStripeToHideButton(event_view["event_0"]["id"].(string))
			c.Data["StripeConnectedStatus"] = stripeConnectedStatus
		}

		isFeatureEventRegistration := common.IsFeatureDateWithoutUtc(teamDateEvent)
		if event_view["event_0"]["recruit"].(string) == "true" && isFeatureEventRegistration == "true" {
			regstatus := models.CheckAnyEventRegistrationHappen(event_view["event_0"]["id"].(string))

			if regstatus {
				c.Data["AllowEditStatus"] = false
			}
		}

		if event_view["event_0"]["divisions"] != nil {
			s := strings.Replace(event_view["event_0"]["divisions"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Divisions"] = strings.Split(s, ",")
		}

		if event_view["event_0"]["classes"] != nil {
			s := strings.Replace(event_view["event_0"]["classes"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["Classes"] = strings.Split(s, ",")
		}

		var divisionType []string
		var maleType []string
		var womenType []string
		var mixedType []string
		for _, v := range event_view {
			//key := "class_" + strconv.Itoa(k)
			if v["class"] == "M" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					divisionType = append(divisionType, s)
					maleType = append(maleType, s)
					//c.Data["MDivisions"] = strings.Split(s, ",")
					c.Data["MDiv"] = true
				}

			} else if v["class"] == "W" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					divisionType = append(divisionType, s)
					womenType = append(womenType, s)
					c.Data["WDivisions"] = strings.Split(s, ",")
				}

			} else if v["class"] == "MX" {
				if v["div"] != nil {
					s := strings.Replace(v["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					divisionType = append(divisionType, s)
					mixedType = append(mixedType, s)
					c.Data["MXDivisions"] = strings.Split(s, ",")
				}

			}

		}
		c.Data["MDivisions"] = maleType
		c.Data["WDivisions"] = womenType
		c.Data["MXDivisions"] = mixedType

		if event_view["event_0"]["boattype"] != nil {
			s := strings.Replace(event_view["event_0"]["boattype"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["EventSelectedBoatType"] = strings.Split(s, ",")
		}

		if event_view["event_0"]["eventdistance"] != nil {
			s := strings.Replace(event_view["event_0"]["eventdistance"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			c.Data["EventSelecteddistance"] = strings.Split(s, ",")
		}

		c.Data["EventSelectedDivisions"] = divisionType

		tempGetEventRegisterClassDivisionDetail := models.GetEventSelectedClassDivision(event_view["event_0"]["id"].(string))
		tempresponseDataText, err := json.Marshal(tempGetEventRegisterClassDivisionDetail)
		if err == nil {
			c.Data["EventRegisterClassDivisionDetailWithJosn"] = string(tempresponseDataText)
		}
		c.Data["EventRegisterClassDivisionDetail"] = tempGetEventRegisterClassDivisionDetail
		c.Data["EventView"] = event_view
	} else {
		fmt.Println(" No event by the slug")
		c.Redirect("/", 302)
		return
	}
	//fmt.Println(event_view)

	//c.Data["GetAllDivisionDetail"] = models.GetAllDivisionDetail()

	event_organisers := make(map[string]orm.Params)
	event_organisers = models.GetEventOrganisers(event_view["event_0"]["id"].(string))
	if _, ok := event_view["status"]; !ok {
		c.Data["EventOrganisers"] = event_organisers
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		var event_og_id []string
		for _, v := range event_organisers {
			event_og_id = append(event_og_id, v["fk_user_id"].(string))
		}

		if c.GetSession("SuperAdmin") != "true" {
			if len(event_og_id) > 0 {
				if !common.InArray(user_id, event_og_id) {
					fmt.Println(" You cannot edit the event")
					fmt.Println("User id ", user_id, " Org ids ", event_og_id)
					c.Redirect("/", 302)
				}
				c.Data["EventOrgIds"] = event_og_id
			}
		}

	}

	//fmt.Println(c.Data["EventOrganisers"])

	event_logo := models.GetEventLogo(event_view["event_0"]["id"].(string))
	if event_logo != "" {
		c.Data["EventLogo"] = event_logo
	}

	event_faq := make(map[string]orm.Params)
	event_faq = models.GetEventFaq(event_view["event_0"]["id"].(string))
	if _, ok := event_faq["status"]; !ok {
		c.Data["EventFaq"] = event_faq
	}
	c.Data["IsEditPage"] = true
	//fmt.Println(c.Data["EventFaq"])

	event_pricing := make(map[string]orm.Params)
	men_pricing := make(map[string]orm.Params)
	women_pricing := make(map[string]orm.Params)
	mixed_pricing := make(map[string]orm.Params)

	event_pricing = models.GetEventPricing(event_view["event_0"]["id"].(string), "edit")
	if event_view["event_0"]["price_based"] != nil {
		c.Data["EventPrice_basedDetail"] = event_view["event_0"]["price_based"].(string)
	}
	c.Data["EventPricingDetail"] = event_pricing
	c.Data["EventPricingBasedDetail"] = event_pricing
	responseTextField, err := json.Marshal(event_pricing)
	fmt.Println("errorjson", string(responseTextField))
	if err == nil {
		c.Data["EventPricingDetailWithJosn"] = string(responseTextField)
	}

	if _, ok := event_pricing["status"]; !ok {
		c.Data["EventPricing"] = true

		for k, v := range event_pricing {
			if v["pricing_class"] == "M" {
				men_pricing[k] = v
			} else if v["pricing_class"] == "W" {
				women_pricing[k] = v
			} else if v["pricing_class"] == "MX" {
				mixed_pricing[k] = v
			}
		}

		c.Data["MenPricing"] = men_pricing
		c.Data["WomenPricing"] = women_pricing
		c.Data["MixedPricing"] = mixed_pricing

	}

	//fmt.Println(event_pricing)

	event_sponsor := make(map[string]orm.Params)
	event_sponsor = models.GetEventSponsor(event_view["event_0"]["id"].(string))
	if _, ok := event_sponsor["status"]; !ok {
		c.Data["EventSponsor"] = event_sponsor
	}

	free_practice := make(map[string]orm.Params)
	free_practice = models.GetEventFreePractice(event_view["event_0"]["id"].(string))
	if _, ok := free_practice["status"]; !ok {
		c.Data["FreePractice"] = free_practice
	}

	additonal_practice := make(map[string]orm.Params)
	additonal_practice = models.GetEventAdditionalPractice(event_view["event_0"]["id"].(string))
	if _, ok := additonal_practice["status"]; !ok {
		c.Data["AdditionalPractice"] = additonal_practice
	}

	other_pricing := make(map[int]orm.Params)
	other_pricing = models.GetEventOtherPricing(event_view["event_0"]["id"].(string))
	if other_pricing[1] != nil {
		c.Data["OtherPricing"] = other_pricing
	}

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := eventValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)
		result := make(map[string]string)

		data["EventName"] = items.EventName
		data["EventLocation"] = items.EventLocation
		data["EventProvince"] = items.EventProvince
		data["EventCountry"] = items.EventCountry
		data["EventVenue"] = items.EventVenue
		data["EventStreet"] = items.EventStreet

		if len(items.EventStartDate) > 0 {
			data["EventStartDate"] = items.EventStartDate
		}

		if len(items.EventEndDate) > 0 {
			data["EventEndDate"] = items.EventEndDate
		} else {
			data["EventEndDate"] = ""
		}

		data["EventAboutCause"] = strings.TrimSpace(items.EventAboutCause)
		data["EventDesc"] = strings.TrimSpace(items.EventDesc)
		data["EventPh"] = items.EventPh

		if items.EventTwitter != "" {
			data["EventTwitter"] = "https://www.twitter.com/" + strings.TrimSpace(items.EventTwitter)
		} else {
			data["EventTwitter"] = items.EventTwitter
		}

		if items.EventFacebook != "" {
			data["EventFacebook"] = "https://www.facebook.com/" + strings.TrimSpace(items.EventFacebook)
		} else {
			data["EventFacebook"] = items.EventFacebook
		}

		if items.EventYouTube != "" {
			data["EventYouTube"] = "https://www.youtube.com/" + strings.TrimSpace(items.EventYouTube)
		} else {
			data["EventYouTube"] = items.EventYouTube
		}

		data["EventEmail"] = items.EventEmail

		if items.EventWebsite != "" {
			data["EventWebsite"] = strings.TrimSpace(items.EventWebsite)
		} else {
			data["EventWebsite"] = items.EventWebsite
		}

		if items.CoachServiceCost == "" {
			data["CoachServiceCost"] = "0"
		} else {
			data["CoachServiceCost"] = items.CoachServiceCost
		}

		if items.AdditionalCostPractice == "" {
			data["AdditionalCostPractice"] = "0"
		} else {
			data["AdditionalCostPractice"] = items.AdditionalCostPractice
		}

		if items.AddtionalPractice == "" {
			data["AddtionalPractice"] = "0"
		} else {
			data["AddtionalPractice"] = items.AddtionalPractice
		}

		if items.ComplFreePractice == "" {
			data["ComplFreePractice"] = "0"
		} else {
			data["ComplFreePractice"] = items.ComplFreePractice
		}

		if items.ComplPractice == "" {
			data["ComplPractice"] = "0"
		} else {
			data["ComplPractice"] = items.ComplPractice
		}

		items.ItemTitle = models.DeleteEmptyElements(items.ItemTitle)
		items.ItemCost = models.DeleteEmptyElements(items.ItemCost)
		items.ItemIdValue = models.DeleteEmptyElements(items.ItemIdValue)

		var ItemTitle []string
		var ItemCost []string
		var ItemIdValue []string

		if len(items.ItemTitle) > 0 {
			ItemTitle = items.ItemTitle
		}

		if len(items.ItemCost) > 0 {
			ItemCost = items.ItemCost
		}

		if len(items.ItemIdValue) > 0 {
			ItemIdValue = items.ItemIdValue
		}

		items.Question = models.DeleteEmptyElements(items.Question)
		items.Answer = models.DeleteEmptyElements(items.Answer)
		var Question []string
		var Answer []string

		if len(items.Question) > 0 {
			Question = items.Question
		}

		if len(items.Answer) > 0 {
			Answer = items.Answer
		}

		x, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
		if err != nil {
			fmt.Println("fatal error: %s", err)
		}

		r := &maps.GeocodingRequest{
			Address: items.EventStreet + ", " + items.EventLocation + ", " + items.EventProvince + ", " + items.EventCountry,
		}

		fmt.Println(items.EventStreet + ", " + items.EventLocation + ", " + items.EventProvince + ", " + items.EventCountry)

		resp, err := x.Geocode(NContext.Background(), r)
		if resp != nil {
			if len(resp) > 0 {
				data["Latitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lat, 'f', -1, 64)
				data["Longitude"] = strconv.FormatFloat(resp[0].Geometry.Location.Lng, 'f', -1, 64)
			}
		}

		reg, err := regexp.Compile("[^A-Za-z0-9À-ž]+")
		if err != nil {
			fmt.Println(err)
		}

		name_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.EventName), " "))
		loc_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(items.EventLocation), " "))

		data["Slug"] = strings.Replace(name_slug, " ", "-", -1) + "-" + strings.Replace(loc_slug, " ", "-", -1)

		data["EventRegistrationOpen"] = items.EventRegistrationOpen
		result = models.EditEvent(data, event_view["event_0"]["id"].(string), ItemTitle, ItemCost, Question, Answer, items.EventQuestions, ItemIdValue)

		if result["status"] == "1" {

			c.SetSession("UpdateSuccess", "true")
			c.Redirect("/event-edit/"+data["Slug"], 302)
		} else if result["status"] == "2" {
			c.SetSession("EmptyClass", "true")
			c.Redirect("/event-edit/"+slug, 302)
		}
	}

	event_questionary := make(map[int]orm.Params)
	event_questionary = models.GetEventQuestionary(event_view["event_0"]["id"].(string))
	if event_questionary[1] != nil {
		c.Data["EventQuestionary"] = event_questionary
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

func (c *EventController) SendCoOrganizerInvite() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In Send CoOrganizerInvite")

	if c.Ctx.Input.Method() == "POST" {
		/*var ctx *context.Context
		x := c.Ctx.Request
		fmt.Println(ctx.BeegoInput.IsPost())
		fmt.Println(x)
		c.Data["ajax"] = true
		fmt.Println(c.GetString("data"))
		c.TplName = "event/createEvent.html"*/
		fmt.Println("In post")

		var email_ids []string
		c.Ctx.Input.Bind(&email_ids, "email")

		message := c.GetString("message")

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		event_id := c.GetSession("EventId").(string)
		sent, alreadyInvited, alreadyMember, status := models.AddCoOrganizer(email_ids, message, user_id, event_id)

		outputObj := &mystruct{
			Sent:           sent,
			AlreadyInvited: alreadyInvited,
			AlreadyMember:  alreadyMember,
			Status:         status,
		}

		b, _ := json.Marshal(outputObj)

		c.Ctx.Output.Body([]byte(string(b)))
	}
	//return &ClassDivs{"test", "ajax"}
	//c.AjaxResponse()

}

func (c *EventController) UpdateEventClasses() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In UpdateEventClasses")

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := eventEditClassDivisiopnValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		fmt.Printf("%+v\n", items)
		data := make(map[string]string)

		layout := "01/02/2006"

		if len(items.SebDate) > 0 {
			str := items.SebDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["SebDate"] = t_array[0]
				data["Seb"] = "yes"
			}
		} else {
			data["SebDate"] = ""
			data["Seb"] = "no"
		}

		if len(items.EbDate) > 0 {
			str := items.EbDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["EbDate"] = t_array[0]
				data["Eb"] = "yes"
			}
		} else {
			data["EbDate"] = ""
			data["Eb"] = "no"
		}

		if len(items.RDate) > 0 {
			str := items.RDate
			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				t_array := strings.Split(t.String(), "+")
				data["RDate"] = t_array[0]
				data["Regular"] = "yes"
			}
		} else {
			data["RDate"] = ""
			data["Regular"] = "no"
		}

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		event_id := c.GetSession("EventId").(string)
		event_id = c.GetString("currenteventId")

		data["Currency"] = items.Currency
		data["PricingBased"] = items.PricingBased
		items.SebPrice = models.DeleteEmptyElements(items.SebPrice)
		items.EbPrice = models.DeleteEmptyElements(items.EbPrice)
		items.Rprice = models.DeleteEmptyElements(items.Rprice)
		data["PricingBasedOther"] = items.PricingBasedOther
		items.SebEbRPrice = models.DeleteEmptyElements(items.SebEbRPrice)
		TempAllDivisions := models.DeleteEmptyElements(items.AllDivisions)
		TempAllBoattype := models.DeleteEmptyElements(items.AllBoattype)
		TempAllBoatmeter := models.DeleteEmptyElements(items.AllBoatmeter)
		items.AllDivClsBottyMeter = models.DeleteEmptyElements(items.AllDivClsBottyMeter)

		TempAllDivisionsString := strings.Join(TempAllDivisions, ",")
		TempAllBoattypeString := strings.Join(TempAllBoattype, ",")
		TempAllBoatmeterString := strings.Join(TempAllBoatmeter, ",")

		result := models.EditEventClasses(data, user_id, event_id, TempAllDivisionsString, TempAllBoattypeString, TempAllBoatmeterString, items.SebPrice, items.EbPrice, items.Rprice, items.SebEbRPrice, items.AllDivClsBottyMeter)
		c.Ctx.Output.Body([]byte(result))

	}
}

func CategoriseEvents(result []orm.Params) (past_events []orm.Params, recent_events []orm.Params, upcoming_events []orm.Params, err string) {
	fmt.Println("In CategoriseEvents ")
	/*var past_events []orm.Params
	var recent_events []orm.Params
	var upcoming_events []orm.Params*/
	err = ""
	if result[0] != nil {

		for _, v := range result {

			//fmt.Println(k,v["start_date"],v["end_date"])
			layout := "2006-01-02T15:04:05Z07:00"
			str := ""
			if v["end_date"] != nil {
				str = v["end_date"].(string)
			} else if v["start_date"] != nil {
				str = v["start_date"].(string)
			} else {
				break
			}

			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {
				if t.Before(time.Now().AddDate(0, -1, 0)) {
					//fmt.Println("Past event ", v["id"])
					//past_events[k] = v
					past_events = append(past_events, v)
				} else if t.After(time.Now().AddDate(0, -1, 0)) && t.Before(time.Now()) {
					//fmt.Println("Recent event ",v["id"])
					//recent_events[k] = v
					recent_events = append(recent_events, v)
				} else if t.After(time.Now()) {
					//fmt.Println("Upcoming event ", v["id"])
					//upcoming_events[k] = v
					upcoming_events = append(upcoming_events, v)
				}

			}

		}

		//past_events = reverseArray(past_events)

	} else {
		err = "No events to categorise"
	}

	return past_events, recent_events, upcoming_events, err
}

func (c *EventController) UploadEventImg() {
	c.TplName = "user/profileSettings.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetSession("EventId").(string)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}

		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			result := models.SaveEventUrl(event_id, path, header.Filename)
			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *EventController) AcceptCoOrganizerInvite() {
	c.TplName = "dashboard.html"
	fmt.Println("In AcceptCoOrganizerInvite")

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetString("event_id")
		notf_id := c.GetString("notf_id")

		id := c.GetSession("UserId").(int)
		organizer_id := strconv.Itoa(id)

		result := models.AcceptCoOrganizer(event_id, organizer_id, notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *EventController) RemoveEventCoOrganizer() {
	c.TplName = "dashboard.html"
	fmt.Println("In RemoveEventCoOrganizer")

	if models.IsLoggedIn(c.GetSession("GushouSession")) {

		if c.Ctx.Input.Method() == "POST" {

			eo_id := c.GetString("eo_id")

			event_id := c.GetSession("EventId").(string)

			id := c.GetSession("UserId").(int)
			inviters_id := strconv.Itoa(id)

			result := models.RemoveEventCoOrganizer(event_id, eo_id, inviters_id)

			fmt.Println(result)
			c.Ctx.Output.Body([]byte(result))
		}

	} else {
		c.Ctx.Output.Body([]byte("You must log in"))
	}

}

func (c *EventController) DeleteEvent() {
	c.TplName = "event/eventList.html"
	fmt.Println("In DeleteEvent")

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetString("event_id")
		fmt.Println(event_id)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.DeleteEvent(event_id, user_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *EventController) AddEventAnnouncement() {
	c.TplName = "event/eventView.html"
	fmt.Println("In AddEventAnnouncement")

	if c.Ctx.Input.Method() == "POST" {

		msg := c.GetString("msg")
		fmt.Println(msg)

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		event_id := c.GetSession("EventId").(string)

		result := models.AddEventAnnouncement(msg, user_id, event_id)

		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) UploadEventSponsor() {
	c.TplName = "event/eventEdit.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Dashboard"

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetSession("EventId").(string)

		token := ""
		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")

		defer f.Close()

		out, err := os.Create("/tmp/" + header.Filename)
		if err != nil {
			fmt.Println("Unable to create the file for writing. Check your write access privilege")
			return
		}

		defer out.Close()

		// write the content from POST to the file
		_, err = io.Copy(out, f)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

		file, err := os.Open("/tmp/" + header.Filename)
		if err != nil {
			fmt.Printf("err opening file: %s", err)
		}

		defer file.Close()
		fileInfo, _ := file.Stat()
		size := fileInfo.Size()
		buffer := make([]byte, size) // read file content to buffer

		file.Read(buffer)
		fileBytes := bytes.NewReader(buffer)
		fileType := http.DetectContentType(buffer)

		ext := filepath.Ext(file.Name())

		host_name := os.Getenv("HOST_NAME")
		path := ""
		if host_name == "gogushoulive" {
			path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
		} else {
			path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
		}
		fmt.Println(path)

		params := &s3.PutObjectInput{
			Bucket:        aws.String(os.Getenv("BUCKET")),
			Key:           aws.String(path),
			Body:          fileBytes,
			ACL:           aws.String("public-read"),
			ContentLength: aws.Int64(size),
			ContentType:   aws.String(fileType),
		}
		resp, err := svc.PutObject(params)
		if err != nil {
			fmt.Printf("bad response: %s", err)
			c.Ctx.Output.Body([]byte("bad response"))
		} else {
			fmt.Printf("response %s", awsutil.StringValue(resp))
			result := models.SaveEventSponsorUrl(event_id, path, header.Filename)
			c.Ctx.Output.Body([]byte(result))
		}

		err = os.Remove("/tmp/" + header.Filename)
		if err == nil {
			fmt.Println("File removed successfully from tmp folder: " + header.Filename)
		}
	}
}

func (c *EventController) RemoveEventSponsor() {
	c.TplName = "event/eventEdit.html"
	fmt.Println("In RemoveEventSponsor")

	if c.Ctx.Input.Method() == "POST" {

		var SponsorIds []string
		c.Ctx.Input.Bind(&SponsorIds, "ids")

		fmt.Println(SponsorIds)

		data := make(map[string][]string)

		data["SponsorIds"] = SponsorIds
		event_id := c.GetSession("EventId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		removed_by := "captain"

		if c.GetSession("SuperAdmin") == "true" {
			removed_by = "super_admin"
		}

		result := models.RemoveEventSponsor(data, event_id, user_id, removed_by)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) ClaimEventRequest() {
	c.TplName = "event/eventView.html"
	fmt.Println("In ClaimEventRequest")

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetSession("EventId").(string)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.ClaimEventRequest(user_id, event_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) AcceptClaimEventRequest() {
	c.TplName = "event/eventView.html"
	fmt.Println("In AcceptClaimEventRequest")

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetString("event_id")
		notf_id := c.GetString("notf_id")
		initiator_id := c.GetString("initiator_id")

		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		result := models.AcceptClaimEventRequest(user_id, event_id, notf_id, initiator_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) VolunteerRequest() {
	c.TplName = "team/eventView.html"
	fmt.Println("In VolunteerRequest")

	if c.Ctx.Input.Method() == "POST" {

		msg := c.GetString("msg")
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		event_id := c.GetSession("EventId").(string)

		result := models.SendVolunteerRequest(msg, user_id, event_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) CheckEventName() {
	c.TplName = "dashboard.html"
	fmt.Println("In CheckEventName")

	if c.Ctx.Input.Method() == "POST" {

		event_name := c.GetString("event_name")
		location := c.GetString("location")
		caller := c.GetString("caller")

		reg, err := regexp.Compile("[^A-Za-z0-9À-ž]+")
		if err != nil {
			fmt.Println(err)
		}

		name_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(event_name), " "))
		loc_slug := strings.TrimSpace(reg.ReplaceAllString(strings.ToLower(location), " "))

		slug := strings.Replace(name_slug, " ", "-", -1) + "-" + strings.Replace(loc_slug, " ", "-", -1)

		result := models.GetEventId(slug)

		fmt.Println(result)

		if result != "" {

			if caller == "edit" {
				event_id := c.GetSession("EventId").(string)
				if result != event_id {
					c.Ctx.Output.Body([]byte("exists"))
				} else {
					c.Ctx.Output.Body([]byte(""))
				}

			} else {
				c.Ctx.Output.Body([]byte("exists"))
			}

		} else {
			c.Ctx.Output.Body([]byte(""))
		}

	}

}

func (c *EventController) DeclineEventOrgInvite() {
	c.TplName = "dashboard.html"
	fmt.Println("In DeclineEventOrgInvite")

	if c.Ctx.Input.Method() == "POST" {

		event_id := c.GetString("event_id")
		notf_id := c.GetString("notf_id")

		id := c.GetSession("UserId").(int)
		member_id := strconv.Itoa(id)

		result := models.DeclineEventOrg(event_id, member_id, notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

}

func (c *EventController) DeclineClaimRequest() {
	c.TplName = "team/teamView.html"
	fmt.Println("In DeclineTeamJoinRequest")

	if c.Ctx.Input.Method() == "POST" {

		org_id := c.GetString("org_id")
		event_id := c.GetString("event_id")
		notf_id := c.GetString("notf_id")

		result := models.DeclineClaimRequest(org_id, event_id, notf_id)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))

	}

}

func (c *EventController) GetTeamListAutoComplete() {
	c.TplName = "event/eventView.html"
	fmt.Println("In GetTeamListAutoComplete")

	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		searchText := c.GetString("searchText")
		if c.GetSession("SuperAdmin") == "true" {
			result := models.GetTeamListConcernUserByUserId_SuperAdmin("", searchText)
			c.Ctx.Output.JSON(result, false, false)
		} else if c.GetSession("UserId") != nil {
			id := c.GetSession("UserId").(int)
			userId := strconv.Itoa(id)
			result := models.GetTeamListConcernUserByUserId_SuperAdmin(userId, searchText)
			c.Ctx.Output.JSON(result, false, false)
		}
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetTeamListAutoComplete")
	}
}

func (c *EventController) GetRosterListTeamIdAutoComplete() {
	c.TplName = "event/eventView.html"
	fmt.Println("In GetRosterListTeamIdAutoComplete")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		searchText := c.GetString("searchText")
		team_id := c.GetString("team_id")
		result := models.GetRosterListDetailByTeamId(searchText, team_id)
		b, _ := json.Marshal(result)
		c.Ctx.Output.Body([]byte(string(b)))
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetRosterListTeamIdAutoComplete")
	}
}

func (c *EventController) GetRosterByTeamIdEventId() {
	c.TplName = "event/eventView.html"
	fmt.Println("In GetRosterByTeamIdEventId")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		team_id := c.GetString("team_id")
		event_id := c.GetSession("EventId").(string)
		result := models.GetRosterByTeamIdEventId(event_id, team_id)
		c.Ctx.Output.JSON(result, false, false)
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetRosterByTeamIdEventId")
	}
}

func (c *EventController) GetEventDetailByEventId() {
	c.TplName = "event/eventView.html"
	fmt.Println("In GetEventDetailByEventId")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		event_id := c.GetString("eventid")
		teamid := c.GetString("teamid")
		event_class_divisions, all_divisions := models.GetEventClassDivisionsDetail(event_id, teamid)
		outputObj := &eventclass_division_detail_struct{
			EventClassDivisionData: event_class_divisions,
			AllDivisionData:        all_divisions,
		}
		c.Ctx.Output.JSON(outputObj, false, false)
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetEventDetailByEventId")
	}
}

func (c *EventController) EventRegisterRosterToEvent() {
	c.TplName = "event/eventView.html"
	fmt.Println("In EventRegisterRosterToEvent")
	if c.Ctx.Input.Method() == "POST" {
		event_id := c.GetString("register_event_id")
		team_id := c.GetSession("TeamId").(string)
		register_roster_id := c.GetString("register_roster_id")
		register_class := c.GetString("register_class")
		register_division := c.GetString("register_division")
		register_boatType := ""
		if c.GetString("register_boatType") != "" {
			register_boatType = c.GetString("register_boatType")
		}
		result := models.EventRegisterRosterToEvent(event_id, register_roster_id, register_class, register_division, team_id, register_boatType)
		c.Ctx.Output.Body([]byte(result))
	}

	fmt.Println("Out EventRegisterRosterToEvent")
}

func (c *EventController) CheckTeamAlreadyRegisterInEvent() {
	c.TplName = "event/eventView.html"
	fmt.Println("In CheckTeamAlreadyRegisterInEvent")
	if c.Ctx.Input.Method() == "GET" {
		team_id := c.GetString("team_Id")
		event_id := c.GetSession("EventId").(string)

		result := models.CheckTeamAlreadyRegisterInEvent(team_id, event_id)
		c.Ctx.Output.Body([]byte(result))
	}

	fmt.Println("Out CheckTeamAlreadyRegisterInEvent")
}

func containsInArray(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

func (c *EventController) TeamRegisterEvent() {
	c.TplName = "event/createTeamEventRegistration.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Register Event"

	c.Data["create_team_event_1"] = true
	c.Data["create_team_event_2"] = false
	c.Data["create_team_event_3"] = false
	c.Data["currentregistrationstep"] = ""
	c.Data["CompleteRegistration"] = "false"
	c.Data["AlreadyRegistration"] = "false"
	c.Data["errorinregistration"] = false
	c.Data["readyToPaymentStatus"] = false
	resultData := "false"

	slug := c.Ctx.Input.Param(":slug")
	selectregisterteam := "false"
	tempeventregisterteamID := "0"
	event_view := make(map[string]orm.Params)
	event_view = models.GetEventBySlug(slug)

	if c.GetSession("AlreadyRegistration") == "true" {
		c.Data["AlreadyRegistration"] = "true"
		c.DelSession("AlreadyRegistration")
	}

	if c.Ctx.Input.Method() == "POST" {
		tempeventregisterteamID = c.GetString("tempeventregisterteamid")

		selectregisterteam = c.GetString("selectregisterteam")
		cancelEventRegistrationstatus := c.GetString("cancelEventRegistrationstatus")
		c.Data["RegisterSeletedTeamName"] = c.GetString("registerteamSeletedName")

		flash := beego.NewFlash()
		items := eventTeamRegisterValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		data := make(map[string]string)

		data["EventRegisterTeamid"] = items.EventRegisterTeamid
		data["EventRegisterEventid"] = event_view["event_0"]["id"].(string)
		//
		data["CurrentCreateEventStep"] = items.CurrentCreateEventStep
		data["EventPaymentStatus"] = items.EventPaymentStatus

		if cancelEventRegistrationstatus == "true" {
			if c.GetString("currentEventRegisterId") != "" {
				models.DeleteRegisterTeamEventFromDB(c.GetString("currentEventRegisterId"))
				c.Redirect("/event-view/"+event_view["event_0"]["slug"].(string), 302)
			} else {
				c.Redirect("/event-view/"+event_view["event_0"]["slug"].(string), 302)
			}

		}

		alreadyRegister := models.CheckTeamRegisterOrNot(event_view["event_0"]["id"].(string), tempeventregisterteamID)
		if alreadyRegister == "true" {
			if c.GetSession("readyToPaymentStatus") == nil || items.CurrentCreateEventStep != "4" {
				c.SetSession("AlreadyRegistration", "true")
				c.Redirect("/event/team-event-registration/"+event_view["event_0"]["slug"].(string), 302)
			} else {
				c.DelSession("readyToPaymentStatus")
			}
		}

		unit, err_unit := currency.ParseISO(event_view["event_0"]["currency"].(string))
		if err_unit == nil {
			currencySymbol := currency.Symbol(unit)
			c.Data["CurrencySymbol"] = currencySymbol
		}

		id := c.GetSession("UserId").(int)
		tempuserId := strconv.Itoa(id)

		if items.CurrentCreateEventStep == "1" {

			data["SubmitRosterLater"] = items.SubmitRosterLater

			tempSelectedDivisionArray := strings.Split(items.SelectedDivisionArray, ",")
			tempSelectedClassArray := strings.Split(items.SelectedClassArray, ",")
			tempSelectedRosterIdArray := strings.Split(items.SelectedRosterIdArray, ",")
			tempOnlySelectedClassArray := strings.Split(items.OnlySelectedClassArray, ",")
			tempOnlySelectedDivisionArray := strings.Split(items.OnlySelectedDivisionArray, ",")
			tempOnlySelectedDistanceArray := strings.Split(items.OnlySelectedDistanceArray, ",")
			tempOnlySelectedBoatTypeArray := strings.Split(items.SelectedBoatTypeArray, ",")
			tempOnlySelectedBoatTypeArray = models.DeleteEmptyElements(tempOnlySelectedBoatTypeArray)

			if c.GetString("currentEventRegisterId") == "" {
				resultData = models.RegisterTeamEvent(data, event_view["event_0"]["id"].(string), tempSelectedDivisionArray, tempSelectedClassArray, tempSelectedRosterIdArray, tempOnlySelectedClassArray, tempOnlySelectedDivisionArray, tempOnlySelectedBoatTypeArray, tempOnlySelectedDistanceArray)
			} else if c.GetString("currentEventRegisterId") != "" {
				resultData = models.UpdateRegisterTeamEvent(data, event_view["event_0"]["id"].(string), tempSelectedDivisionArray, tempSelectedClassArray, tempSelectedRosterIdArray, tempOnlySelectedClassArray, tempOnlySelectedDivisionArray, c.GetString("currentEventRegisterId"), tempOnlySelectedBoatTypeArray, tempOnlySelectedDistanceArray)
			}
			if resultData == "true" {
				c.Data["create_team_event_1"] = false
				c.Data["create_team_event_2"] = true
				c.Data["create_team_event_3"] = false
				c.Data["currentregistrationstep"] = ""
			}

		} else if items.CurrentCreateEventStep == "2" {
			price_Based := ""
			if event_view["event_0"]["price_based"] != nil {
				price_Based = event_view["event_0"]["price_based"].(string)
			}
			data["AdditionalPracticeBool"] = items.AdditionalPracticeBool
			data["AdditionalPracticeQuantity"] = items.AdditionalPracticeQuantity
			data["ParticipantsCount"] = items.ParticipantsCount

			isSuperAdmin := false
			if c.GetSession("SuperAdmin") == "true" {
				isSuperAdmin = true
			}
			totalPayAmount := "0"
			resultData, totalPayAmount = models.UpdateRegisterTeamEventStep2(data, items.IteamId, items.IteamIdQuantity, items.PerPersonIteamId, items.PerPersonQuantity, price_Based, c.GetString("currentEventRegisterId"), items.IteamName, items.PerPersonIteamName, tempuserId, isSuperAdmin)
			if resultData == "true" {
				c.Data["create_team_event_1"] = false
				c.Data["create_team_event_2"] = false
				c.Data["create_team_event_3"] = true
				if totalPayAmount != "0" {
					netpaystatus := models.CheckStripeOptionEnableOrNotByEventOrg(event_view["event_0"]["id"].(string))
					c.Data["NetPayStatus"] = netpaystatus
				}
			} else {
				c.Data["errorinregistration"] = true
				c.Data["create_team_event_1"] = false
				c.Data["create_team_event_2"] = true
				c.Data["create_team_event_3"] = false
			}

		} else if items.CurrentCreateEventStep == "3" {
			data["Event_RegisterMessage"] = items.Event_RegisterMessage
			resultData = models.UpdateRegisterTeamEventStep3(data, items.AdditionalQuestion, items.AdditionalAnswer, c.GetString("currentEventRegisterId"), event_view["event_0"]["id"].(string), tempeventregisterteamID)

			if data["EventPaymentStatus"] == "false" {
				c.Data["create_team_event_1"] = true
				c.Data["create_team_event_2"] = false
				c.Data["create_team_event_3"] = false
				c.Data["currentregistrationstep"] = ""
				tempeventregisterteamID = "0"
				c.SetSession("EventRegistrationDone", "true")
				c.Redirect("/event-view/"+event_view["event_0"]["slug"].(string), 302)
			}
		}

		if items.CurrentCreateEventStep == "4" || data["EventPaymentStatus"] == "true" {
			netpaystatus := models.CheckStripeOptionEnableOrNotByEventOrg(event_view["event_0"]["id"].(string))
			c.Data["NetPayStatus"] = netpaystatus
			c.Data["create_team_event_1"] = false
			c.Data["create_team_event_2"] = false
			c.Data["create_team_event_3"] = true
			c.Data["readyToPaymentStatus"] = true
			c.SetSession("readyToPaymentStatus", true)
			remaamount, team_name, remaamountwithformat := models.GetTotalRemaningPaidAmount(c.GetString("currentEventRegisterId"))
			stripeSourceCardStatus, last4digit := models.CheckSubscription_Card_Detail_By_UserId(tempuserId)
			c.Data["EventRegisterPaymentId"] = models.GetEventRegisterTeamPaymentId(c.GetString("currentEventRegisterId"))
			c.Data["RemainingAmount"] = remaamount
			c.Data["Remaamountwithformat"] = remaamountwithformat
			c.Data["Team_Name"] = team_name
			c.Data["StripeSourceCardStatus"] = stripeSourceCardStatus
			c.Data["StripeSourceCardLast4"] = last4digit
		}

		fmt.Printf("%+v\n", items)
	}
	c.Data["TempEventRegisterTeamID"] = tempeventregisterteamID

	if _, ok := event_view["status"]; !ok {

		isFeatureDate := common.IsFeatureDateWithoutUtc(event_view["event_0"]["start_date"].(string))
		if event_view["event_0"]["recruit"].(string) == "true" && isFeatureDate == "true" && c.GetSession("Team_Manage_Admin") == "true" {

			id := c.GetSession("UserId").(int)
			fmt.Println(id)
			userId := strconv.Itoa(id)

			var TeamListEvent []orm.Params
			TeamListEvent = models.GetTeamListConcernUserByUserId(userId)
			c.Data["TeamListEvent"] = TeamListEvent
			c.Data["EventView"] = event_view

			var allEventRegisterDivision []orm.Params
			var tempAllEventRegisterDivision []string
			var allDivisionBoatSize []string

			if selectregisterteam == "true" {

				c.Data["allDivisionBoatSize"] = allDivisionBoatSize

				for _, v := range event_view {
					//key := "class_" + strconv.Itoa(k)
					if v["class"] == "M" {
						allEventRegisterDivision = append(allEventRegisterDivision, v)
						if v["div"] != nil {
							s := strings.Replace(v["div"].(string), "{", "", -1)
							s = strings.Replace(s, "}", "", -1)
							c.Data["MDivisions"] = strings.Split(s, ",")
							c.Data["MDiv"] = true
							if v["boat_type"] != nil {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s+"_"+v["boat_type"].(string))
							} else {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s)
							}

						}

					} else if v["class"] == "W" {
						allEventRegisterDivision = append(allEventRegisterDivision, v)
						if v["div"] != nil {
							s := strings.Replace(v["div"].(string), "{", "", -1)
							s = strings.Replace(s, "}", "", -1)
							c.Data["WDivisions"] = strings.Split(s, ",")
							c.Data["WDiv"] = true

							if v["boat_type"] != nil {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s+"_"+v["boat_type"].(string))
							} else {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s)
							}
						}
					} else if v["class"] == "MX" {
						allEventRegisterDivision = append(allEventRegisterDivision, v)
						if v["div"] != nil {
							s := strings.Replace(v["div"].(string), "{", "", -1)
							s = strings.Replace(s, "}", "", -1)
							c.Data["MXDivisions"] = strings.Split(s, ",")
							c.Data["MXDiv"] = true
							if v["boat_type"] != nil {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s+"_"+v["boat_type"].(string))
							} else {
								tempAllEventRegisterDivision = common.UniqueDivisionappend(tempAllEventRegisterDivision, s)
							}

						}

					}

				}

				c.Data["Male_Female_Mixed_Length"] = len(allEventRegisterDivision)

				c.Data["DivisionArray"] = allEventRegisterDivision
				c.Data["DivisionArrayLength"] = len(allEventRegisterDivision)
				responseDataText, err := json.Marshal(allEventRegisterDivision)
				if err == nil {
					c.Data["DivisionArrayWithJosn"] = string(responseDataText)
				}

				c.Data["RemoveDuplicateDivisionArray"] = tempAllEventRegisterDivision

				free_practice := make(map[string]orm.Params)
				free_practice = models.GetEventFreePractice(event_view["event_0"]["id"].(string))
				c.Data["FreePracticeStatus"] = "false"
				if _, ok := free_practice["status"]; !ok {

					for _, v := range free_practice {
						fmt.Println("v[quantity]", v["quantity"])
						if v["quantity"] != "0" {
							c.Data["FreePracticeStatus"] = "true"
						}
					}
					c.Data["FreePractice"] = free_practice
				}

				additonal_practice := make(map[string]orm.Params)
				additonal_practice = models.GetEventAdditionalPractice(event_view["event_0"]["id"].(string))
				if _, ok := additonal_practice["status"]; !ok {
					c.Data["AdditionalPractice"] = additonal_practice
				}

				other_pricing := make(map[int]orm.Params)
				other_pricing = models.GetEventOtherPricing(event_view["event_0"]["id"].(string))
				if other_pricing[1] != nil {
					c.Data["OtherPricing"] = other_pricing
				}

				event_person_pricing := models.GetEventPricingFee(event_view["event_0"]["id"].(string))
				c.Data["Event_Person_pricing"] = event_person_pricing

				event_questionary := make(map[int]orm.Params)
				event_questionary = models.GetEventQuestionary(event_view["event_0"]["id"].(string))
				if event_questionary[1] != nil {
					c.Data["EventQuestionary"] = event_questionary
				}

				responseData := models.GetRosterByTeamIdEventId(event_view["event_0"]["id"].(string), tempeventregisterteamID)
				c.Data["TeamRosterDetail"] = responseData
				responseDataText, err = json.Marshal(responseData)
				if err == nil {
					c.Data["TeamRosterDetailWithJosn"] = string(responseDataText)
				}

				selected_event_register, selected_event_register_roster, only_selected_Division_Class, register_pricing_other, register_event_questionary, register_event_role_pricing := models.GetTeamEventRegistrationDetail(tempeventregisterteamID, event_view["event_0"]["id"].(string))
				c.Data["SelectedEventRegister"] = selected_event_register
				c.Data["SelectedEventRegisterRoster"] = selected_event_register_roster
				c.Data["OnlySelectedDivisionClass"] = only_selected_Division_Class
				c.Data["RegisterPricingOther"] = register_pricing_other
				c.Data["RegisterEventQuestionary"] = register_event_questionary
				c.Data["RegisterEventRolePricing"] = register_event_role_pricing

				// var event_class_divisions []orm.Params
				// event_class_divisions = models.GetEventClassDivisions(event_view["event_0"]["id"].(string))
				// c.Data["Event_Class_Divisions"] = event_class_divisions
			}
			if models.IsLoggedIn(c.GetSession("GushouSession")) {
				c.Data["IsLoggedIn"] = 1
				c.Data["FirstName"] = c.GetSession("FirstName")
				c.Data["LastName"] = c.GetSession("LastName")
				c.Data["UserId"] = c.GetSession("UserId")

				if c.GetSession("SuperAdmin") == "true" {
					c.Data["SuperAdmin"] = "true"
				}

			} else {
				c.Data["IsLoggedIn"] = 0
			}
		} else {
			fmt.Println(" No event open for registration or not future date")
			c.Redirect("/", 302)
			return
		}
	} else {
		fmt.Println(" No event by the slug")
		c.Redirect("/", 302)
		return
	}

}

func (c *EventController) GetEventRegisterDetailById() {
	c.TplName = "event/eventView.html"
	fmt.Println("In GetEventRegisterDetailById")
	if c.Ctx.Input.Method() == "GET" {
		mutex.Lock()
		eventRegisterId := c.GetString("eventRegisterId")
		result := models.GetEventRegisterDetailById(eventRegisterId)
		c.Ctx.Output.JSON(result, false, false)
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out GetEventRegisterDetailById")
	}
}

func (c *EventController) EditEventRegisterDetail() {
	c.TplName = "event/eventView.html"
	fmt.Println("In EditEventRegisterDetail")
	if c.Ctx.Input.Method() == "POST" {
		mutex.Lock()
		reg_event_captain_name := c.GetString("reg_event_captain_name")
		reg_event_captain_email := c.GetString("reg_event_captain_email")
		reg_event_captain_phone := c.GetString("reg_event_captain_phone")
		reg_event_captain_note := c.GetString("reg_event_captain_note")
		reg_event_id := c.GetString("reg_event_id")
		result := models.EditEventRegisterDetail(reg_event_captain_name, reg_event_captain_email, reg_event_captain_phone, reg_event_captain_note, reg_event_id)
		c.Ctx.Output.Body([]byte(result))
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out EditEventRegisterDetail")
	}
}

func (c *EventController) CancelEventRegisterDetailById() {
	c.TplName = "event/eventView.html"
	fmt.Println("In CancelEventRegisterDetailById")
	if c.Ctx.Input.Method() == "POST" {
		mutex.Lock()
		eventRegisterId := c.GetString("eventRegisterId")
		result := models.CancelEventRegisterDetailById(eventRegisterId)
		c.Ctx.Output.Body([]byte(result))
		mutex.Unlock()
		time.Sleep(time.Second)
		fmt.Println("Out CancelEventRegisterDetailById")
	}
}

func (c *EventController) EventDetailRegisterTeamView() {
	c.TplName = "event/eventTeamOrderView.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Event Team Detail"

	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	slug := c.Ctx.Input.Param(":slug")
	regeventid := c.Ctx.Input.Param(":regeventid")

	event_view := make(map[string]orm.Params)
	event_view = models.GetEventBySlug(slug)

	if _, ok := event_view["status"]; !ok {

		unit, err_unit := currency.ParseISO(event_view["event_0"]["currency"].(string))
		if err_unit == nil {
			currencySymbol := currency.Symbol(unit)
			c.Data["currencysymbol"] = currencySymbol
		}

		eventregDetail := models.CheckValidRegisterEvent(regeventid, event_view["event_0"]["id"].(string))
		if eventregDetail != nil {
			c.Data["EventView"] = event_view
			c.Data["EventSlug"] = slug
			c.Data["EventRegDetail"] = eventregDetail
			if eventregDetail["event_reg0"]["register_user_file"] != nil {
				c.Data["EventRegTeamFileDetail"] = eventregDetail["event_reg0"]["register_user_file"].(string)
			}

			c.Data["CurrentTeamId"] = eventregDetail["event_reg0"]["fk_team_id"].(string)
			id := c.GetSession("UserId").(int)
			fmt.Println(id)
			tempuserId := strconv.Itoa(id)

			c.Data["CaptainCoCaptainStatus"] = models.CheckUserIsCaptainOrCoCaptain(eventregDetail["event_reg0"]["fk_team_id"].(string), tempuserId)

			eventOrganizer, CoeventOrganizer := models.CheckUserIsEventOrCoEventOrganizer(event_view["event_0"]["id"].(string), tempuserId)
			c.Data["EventOranizerStatus"] = eventOrganizer
			c.Data["CoEventOranizerStatus"] = CoeventOrganizer

			c.Data["StripeConnectedStatus"] = "false"
			if eventOrganizer {
				stripeConnectedStatus := models.CheckAlreadyConnetWithStripeToHideButton(event_view["event_0"]["id"].(string))
				c.Data["StripeConnectedStatus"] = stripeConnectedStatus
			}

			getSelectedTeamPaymentDetails := models.GetSelectedTeamPaymentDetails(regeventid)
			c.Data["SelectedTeamPaymentDetails"] = getSelectedTeamPaymentDetails

			registerLaterClassDivisionList := models.GetEventRegisterClassDivisionByRegisterEventId(regeventid)
			c.Data["RegisterLaterClassDivisionList"] = registerLaterClassDivisionList

			teamroosterlist := models.GetTeamRoosterListByTeamId(eventregDetail["event_reg0"]["fk_team_id"].(string), regeventid)
			c.Data["TeamRosterList"] = teamroosterlist

			tempeventRegAdditionalPracticeBool := ""
			if eventregDetail["event_reg0"]["additional_practice_bool"] != nil {

				tempeventRegAdditionalPracticeBool = eventregDetail["event_reg0"]["additional_practice_bool"].(string)
				c.Data["EventRegAdditionalPracticeBool"] = tempeventRegAdditionalPracticeBool
			}

			tempResultData := models.GetRegisterEventDetailByRegisterEventId(regeventid)
			c.Data["Event_Register_Team_Detail_List"] = tempResultData

			if len(tempResultData) > 0 {
				c.Data["Event_Register_Team_CompleteOrUnComplete"] = tempResultData[0]["roster_partial"]["case"].(string)
			}

			eventAdditionalPracticeDetail := models.GetEventPracticePricingByEventId(event_view["event_0"]["id"].(string))
			c.Data["EventAdditionalPracticeDetail"] = eventAdditionalPracticeDetail

			eventPricingOtherDetail := models.GetPricingOtherIteamByEventId(event_view["event_0"]["id"].(string))
			c.Data["EventPricingOtherDetail"] = eventPricingOtherDetail

			eventregisterpricingother := models.GetEventRegisterPricingOtherByEventRegId(regeventid)
			c.Data["EventRegisterPricingOther"] = eventregisterpricingother

			teampOrderDetail := []map[string]string{}
			teampTotalOrderCount := 0.00
			if eventPricingOtherDetail != nil && eventregisterpricingother != nil {
				teampOrderDetail, teampTotalOrderCount = models.GetTotalOrderItemList(eventPricingOtherDetail, eventregisterpricingother)
			}
			c.Data["TeampOrderDetail"] = teampOrderDetail

			bigval_other_fees := new(big.Float)
			valuestrign_other_fees := strconv.FormatFloat(float64(teampTotalOrderCount), 'f', 0, 64)
			bigval_other_fees.SetString(valuestrign_other_fees)
			ac := accounting.Accounting{Symbol: "", Precision: 0}

			c.Data["TeampTotalOrderCount"] = ac.FormatMoneyBigFloat(bigval_other_fees)

			c.Data["RegisterEventId"] = regeventid

			tempRolePricingTotalCount := 0.00

			tempRoleTotalCount := int64(0)
			tempPriceBased := ""
			if event_view["event_0"]["price_based"] != nil {
				if event_view["event_0"]["price_based"].(string) == "PP" {
					tempPriceBased = event_view["event_0"]["price_based"].(string)
					eventRolePricingDetail := models.GetEventPricingFee(event_view["event_0"]["id"].(string))
					c.Data["EventRolePricingDetail"] = eventRolePricingDetail

					registeEventRolePricingDetail := models.GetRegisterEventPricingFee(regeventid)
					c.Data["RegisterEventRolePricingDetail"] = registeEventRolePricingDetail

					tempRolePricingDetail := []map[string]string{}
					tempRolePricingTotalCount = 0.00

					if eventRolePricingDetail != nil && registeEventRolePricingDetail != nil {
						tempRolePricingDetail, tempRolePricingTotalCount, tempRoleTotalCount = models.GetTotalRoleQuantityItemList(eventRolePricingDetail, registeEventRolePricingDetail)
					}
					c.Data["TempRolePricingDetail"] = tempRolePricingDetail

					bigval_event_fees := new(big.Float)
					valuestrign_event_fees := strconv.FormatFloat(float64(tempRolePricingTotalCount), 'f', 0, 64)
					bigval_event_fees.SetString(valuestrign_event_fees)
					c.Data["TempRolePricingTotalCount"] = ac.FormatMoneyBigFloat(bigval_event_fees)
				}
			}

			tempGushouMemberCountPrice := 0.00
			tempTotalMeberCount := "0"
			if eventregDetail["event_reg0"]["participant_member_count"] != nil && tempPriceBased != "PP" {
				tempparticipant_member_count := eventregDetail["event_reg0"]["participant_member_count"].(string)
				tempTotalMeberCount = tempparticipant_member_count
				f, err := strconv.ParseFloat(tempparticipant_member_count, 64)
				fmt.Println(err)
				tempGushouMemberCountPrice = f * float64(constants.GUSHOUFEE)
			} else {
				tempGushouMemberCountPrice = float64(tempRoleTotalCount) * float64(constants.GUSHOUFEE)
			}

			bigval_gushou_fee := new(big.Float)
			valuestrign_gushou_fee := strconv.FormatFloat(float64(tempGushouMemberCountPrice), 'f', 0, 64)
			bigval_gushou_fee.SetString(valuestrign_gushou_fee)
			c.Data["TempGushouMemberCountPrice"] = ac.FormatMoneyBigFloat(bigval_gushou_fee)

			c.Data["TempTotalMeberCount"] = tempTotalMeberCount

			regeventquestionary := models.GetRegEventQuestionaryByEventRegId(regeventid)
			c.Data["RegEventQuestionary"] = regeventquestionary

			regeventorderlog := models.GetRegEventOrderLogByEventRegId(regeventid)
			c.Data["RegEventOrderLog"] = regeventorderlog

			registerRostersList := models.GetRegisterRostersListByEventRegId(regeventid)
			c.Data["RegisterRostersIdList"] = registerRostersList

			registerTeamDetail := models.GetRegisterTeamDetailByTeamId(eventregDetail["event_reg0"]["fk_team_id"].(string))
			c.Data["RegisterTeamDetail"] = registerTeamDetail
			if registerTeamDetail != nil {
				if registerTeamDetail[0]["teamvalue"] != nil {
					c.Data["RegisterTeamId"] = registerTeamDetail[0]["teamvalue"].(string)
				}
			}
			totalPracticePrice := 0.00
			if tempeventRegAdditionalPracticeBool == "true" {

				tempaddtionalracticequantity := eventregDetail["event_reg0"]["additional_practice_quantity"].(string)

				if eventAdditionalPracticeDetail != nil {

					if eventAdditionalPracticeDetail[0] != nil {

						tempnooffreepractice := eventAdditionalPracticeDetail[0]["quantity"].(string)

						if eventAdditionalPracticeDetail[1] != nil {

							tempaddtionalracticeprice := eventAdditionalPracticeDetail[1]["price"].(string)

							f, err := strconv.ParseFloat(tempaddtionalracticeprice, 64)
							i, err2 := strconv.Atoi(tempnooffreepractice)
							j, err3 := strconv.Atoi(tempaddtionalracticequantity)

							if err == nil && err2 == nil && err3 == nil {
								tempcal := j
								if tempcal >= 0 {
									tempf := float64(tempcal)
									totalPracticePrice = tempf * f

									bigval_prac_fee := new(big.Float)
									valuestrign_prac_fee := strconv.FormatFloat(float64(totalPracticePrice), 'f', 0, 64)

									bigval_prac_fee.SetString(valuestrign_prac_fee)
									c.Data["TotalPracticePrice"] = ac.FormatMoneyBigFloat(bigval_prac_fee)
									tempf1 := float64(j)
									tempf2 := float64(i)
									totaladditionalPracticeTotoalPrice := tempf1 * f
									totalfreePracticeTotoalPrice := tempf2 * f
									c.Data["TotalAdtionalPrice"] = totaladditionalPracticeTotoalPrice
									c.Data["TotalFreePrice"] = totalfreePracticeTotoalPrice
								}
							}
						}
					}
				}

			}

			bigval_total := new(big.Float)
			overallOrderDetailCount := strconv.FormatFloat(float64(totalPracticePrice+tempGushouMemberCountPrice+teampTotalOrderCount+tempRolePricingTotalCount), 'f', 0, 64)
			bigval_total.SetString(overallOrderDetailCount)
			c.Data["OverallOrderDetailCount"] = ac.FormatMoneyBigFloat(bigval_total)

			bigval_edittotal := new(big.Float)
			editOverallOrderDetailCount := strconv.FormatFloat(float64(totalPracticePrice+teampTotalOrderCount+tempRolePricingTotalCount), 'f', 0, 64)
			bigval_edittotal.SetString(editOverallOrderDetailCount)
			c.Data["EditOverallOrderDetailCount"] = ac.FormatMoneyBigFloat(bigval_edittotal)

			netpaystatus := models.CheckStripeOptionEnableOrNotByEventOrg(event_view["event_0"]["id"].(string))
			c.Data["NetPayStatus"] = netpaystatus

			stripeSourceCardStatus, last4digit := models.CheckSubscription_Card_Detail_By_UserId(tempuserId)

			c.Data["regeventpayid"] = models.GetEventRegisterTeamPaymentId(regeventid)

			_, _, remainingAmount := models.GetTotalRemaningPaidAmount(regeventid)

			dte := time.Now()
			tempCurrentFormatEndDate := dte.Format("2006-01-02")
			c.Data["isEventEnd"] = models.IsEventEndByCurrentDate(event_view["event_0"]["id"].(string), tempCurrentFormatEndDate)

			c.Data["RemainingAmount"] = remainingAmount

			c.Data["StripeSourceCardStatus"] = stripeSourceCardStatus
			c.Data["StripeSourceCardLast4"] = last4digit

			if models.IsLoggedIn(c.GetSession("GushouSession")) {
				c.Data["IsLoggedIn"] = 1
				c.Data["FirstName"] = c.GetSession("FirstName")
				c.Data["LastName"] = c.GetSession("LastName")
				c.Data["UserId"] = c.GetSession("UserId")
				if event_view["event_0"]["fk_organizer_id"] != nil {
					c.Data["EventOrg"] = event_view["event_0"]["fk_organizer_id"].(string)
				}
			} else {
				c.Data["IsLoggedIn"] = 0
			}

		} else {
			fmt.Println(" No register event by the slug")
			c.Redirect("/", 302)
			return
		}
	} else {
		fmt.Println(" No event by the slug")
		c.Redirect("/", 302)
		return
	}
}

func (c *EventController) EventSubmitRosterLaterOrderPage() {
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()
	items := eventRosterSubmitLaterValidate{}
	if err := c.ParseForm(&items); err != nil {
		flash.Error("Cannot parse form")
		flash.Store(&c.Controller)
		fmt.Println("Cannot parse form")
		return
	}
	resultData := models.GetRegisterEventDetailByRegEventId(items.RegisterEventId)
	fkeventId := resultData[0]["fk_event_id"].(string)
	SubitRosterLaterToEvent(items, fkeventId)
	c.Redirect("/event/team-view/"+items.EventSlug+"/"+items.RegisterEventId, 302)
}

func (c *EventController) EventSubmitRosterLaterTeamPage() {
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()
	items := eventRosterSubmitLaterValidate{}
	if err := c.ParseForm(&items); err != nil {
		flash.Error("Cannot parse form")
		flash.Store(&c.Controller)
		fmt.Println("Cannot parse form")
		return
	}
	resultData := models.GetRegisterEventDetailByRegEventId(items.RegisterEventId)
	fkeventId := resultData[0]["fk_event_id"].(string)
	SubitRosterLaterToEvent(items, fkeventId)
	teamSlugName := models.GetTeamSlugNameByTeamId(resultData[0]["fk_team_id"].(string))
	c.Redirect("/team-view/"+teamSlugName+"/event_register", 302)
}

func SubitRosterLaterToEvent(Tempitems eventRosterSubmitLaterValidate, fkeventId string) bool {
	data := make(map[string]string)
	data["SelectedDivisionArray"] = Tempitems.SelectedDivisionArray
	data["SelectedClassArray"] = Tempitems.SelectedClassArray
	data["SelectedRosterIdArray"] = Tempitems.SelectedRosterIdArray
	data["SelectedDistanceArray"] = Tempitems.SelectedDistanceArray
	data["RegisterEventId"] = Tempitems.RegisterEventId
	data["EventSlug"] = Tempitems.EventSlug
	data["EventId"] = fkeventId

	tempSelectedDivisionArray := strings.Split(Tempitems.SelectedDivisionArray, ",")
	tempSelectedClassArray := strings.Split(Tempitems.SelectedClassArray, ",")
	tempSelectedRosterIdArray := strings.Split(Tempitems.SelectedRosterIdArray, ",")
	tempSelectedDistanceArray := strings.Split(Tempitems.SelectedDistanceArray, ",")

	result := models.SubitRosterLaterToEventByRegisterEventId(data, tempSelectedDivisionArray, tempSelectedClassArray, tempSelectedRosterIdArray, tempSelectedDistanceArray)
	return result
}

func (c *EventController) EditEventOrder() {
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()
	items := eventEditOrderValidate{}
	if err := c.ParseForm(&items); err != nil {
		flash.Error("Cannot parse form")
		flash.Store(&c.Controller)
		fmt.Println("Cannot parse form")
		return
	}
	registerEventId := items.RegisterEventId
	eventSlug := items.EventSlug
	eventId := items.EventId

	id := c.GetSession("UserId").(int)
	tempuserId := strconv.Itoa(id)

	eventOrganizer, CoeventOrganizer := models.CheckUserIsEventOrCoEventOrganizer(eventId, tempuserId)

	captainCoCaptainStatus := models.CheckUserIsCaptainOrCoCaptain(items.TeamId, tempuserId)

	getUserName := models.GetUserName(tempuserId)

	notificationStatus := false

	if eventOrganizer || CoeventOrganizer {
		notificationStatus = true

	} else if captainCoCaptainStatus {
	} else if c.GetSession("SuperAdmin") == "true" {
		notificationStatus = true

	}

	models.UpdateEventOrderEditDetail(items.PerPersonIteamId, items.PerPersonQuantity, items.AdditionalQuantity, items.IteamIdQuantity, items.IteamId, registerEventId, items.Ordernote, notificationStatus, eventSlug, tempuserId, items.TeamId, getUserName)
	c.Redirect("/event/team-view/"+eventSlug+"/"+registerEventId, 302)
}

func (c *EventController) EventManualPayment() {
	if c.Ctx.Input.Method() == "POST" {
		var event_reg_pay_id, amount_paid, apply_discount, transaction_note, currency, choosen_type string

		c.Ctx.Input.Bind(&event_reg_pay_id, "event_reg_pay_id")
		c.Ctx.Input.Bind(&amount_paid, "amount_paid")
		c.Ctx.Input.Bind(&apply_discount, "apply_discount")
		c.Ctx.Input.Bind(&transaction_note, "transaction_note")
		c.Ctx.Input.Bind(&currency, "currency")
		c.Ctx.Input.Bind(&choosen_type, "choosen_type")

		userID := "0"
		if models.IsLoggedIn(c.GetSession("GushouSession")) {
			id := c.GetSession("UserId").(int)
			userID = strconv.Itoa(id)
		}
		eventpayment, choosenType, paymentlogId := models.EventPaymentManual(event_reg_pay_id, userID, amount_paid, apply_discount, transaction_note, currency, choosen_type)
		result := ""
		tempFile := ""

		if paymentlogId != "0" {
			result, tempFile = GeneratePaymentLogDetailInPdfFormat(paymentlogId, result, tempFile)

			if result == "true" {
				UploadTheEventPaymentInvoiceToS3Bucket(tempFile, paymentlogId)
			}
		}

		outputObj := &event_payment_detail_struct{
			PayStatus:   eventpayment,
			ChoosenType: choosenType,
		}
		fmt.Println(outputObj)
		c.Ctx.Output.JSON(outputObj, false, false)
	}
}

func (c *EventController) EventGetPaymentTracking() {
	c.TplName = "event/eventPaymentTracking.html"
	if c.GetSession("SuperAdmin") == "true" {
		c.Data["SuperAdmin"] = "true"
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
	} else {
		c.Data["IsLoggedIn"] = 0
	}

	regeventpayid := c.Ctx.Input.Param(":regeventpayid")

	eventpayid := c.Ctx.Input.Param(":eventid")

	teampayid := c.Ctx.Input.Param(":teamid")

	userID := "0"
	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		id := c.GetSession("UserId").(int)
		userID = strconv.Itoa(id)
	}
	eventTeamDetails := models.GetTeamEventDetails(regeventpayid, eventpayid, teampayid, userID)
	c.Data["GetTeamEventDetailsList"] = eventTeamDetails
	eventPaymentTrackingList, currency_unit, stripe_event_conversion_currency, stripe_gushou_conversion_currency := models.EventPaymentTrackingList(regeventpayid, eventpayid, teampayid, userID)
	c.Data["EventPaymentTrackingList"] = eventPaymentTrackingList

	c.Data["ExtralOrDiscountFee"] = models.GetExtralOrDiscountFeesAmount(regeventpayid)

	currency_unit_symbol := common.ConvertUnitToCurrencySynbol(currency_unit)

	stripe_event_conversion_currency_symbol := common.ConvertUnitToCurrencySynbol(stripe_event_conversion_currency)
	stripe_gushou_conversion_currency_symbol := common.ConvertUnitToCurrencySynbol(stripe_gushou_conversion_currency)

	c.Data["CurrencyUnit"] = currency_unit_symbol
	c.Data["StripeEventCurrencyUnit"] = stripe_event_conversion_currency_symbol
	c.Data["StripeGushouCurrencyUnit"] = stripe_gushou_conversion_currency_symbol

}
func (c *EventController) SendEventPaymentEmailForTeams() {
	if c.Ctx.Input.Method() == "POST" {
		var event_reg_ids string
		c.Ctx.Input.Bind(&event_reg_ids, "event_reg_ids")
		send_event_mails := models.SendEventPaymentEmailForTeams(event_reg_ids)
		c.Ctx.Output.Body([]byte(send_event_mails))
	}
}
func (c *EventController) GetSelectedTeamPaymentDetails() {
	if c.Ctx.Input.Method() == "POST" {
		var event_reg_pay_id string
		c.Ctx.Input.Bind(&event_reg_pay_id, "event_reg_pay_id")

		getSelectedTeamEventPaymentDetails := models.GetSelectedTeamEventPaymentDetails(event_reg_pay_id)
		outputObj := &event_payment_team_detail_struct{
			Result: getSelectedTeamEventPaymentDetails,
		}
		fmt.Println(getSelectedTeamEventPaymentDetails)
		fmt.Println(outputObj)
		c.Ctx.Output.JSON(outputObj, false, false)

	}
}
func (c *EventController) GetIndividualTeamMemberWavierDetail() {

	c.TplName = "event/eventWaivers.html"
	event_reg_id := c.GetString("event_reg_id")
	result := models.GetSubmittedRosterForEventByEventRegId(event_reg_id)
	c.Ctx.Output.JSON(result, false, false)
	fmt.Println("Out GetIndividualTeamMemberWavierDetail")

}

func (c *EventController) DownloadAccrediationPdf() {
	c.TplName = "event/eventAccreditation.html"
	fmt.Println("In DownloadAccrediationPdf")

	resulterrortxt := ""
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := accredationGeneralPrintDetailStruct{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}

		team_event_userId := items.General_Members

		var currentPrintdetail []map[string]string

		manualmembersId := items.Manual_Members

		if len(team_event_userId) > 0 {
			teampTeamDetail := models.GetAllAccPrintDetailByTeamEventUserId(team_event_userId)
			currentPrintdetail = teampTeamDetail
		} else {
			manualDetail := models.GetAllAccManualPrintDetailById(manualmembersId)
			currentPrintdetail = manualDetail
		}

		//wkhtml.SetPath("C:/Program Files (x86)/wkhtmltopdf/bin/wkhtmltopdf.exe")

		wkhtml.SetPath("/usr/local/bin/wkhtmltopdf")

		fmt.Println(wkhtml.GetPath())
		pdfg, err := wkhtml.NewPDFGenerator()
		if err != nil {
			//fmt.Println(err)
			resulterrortxt = "Not NewPDFGenerator"
		}

		pdfg.Dpi.Set(300)
		pdfg.PageSize.Set(wkhtml.PageSizeA4)
		pdfg.MarginLeft.Set(0)
		pdfg.MarginRight.Set(6)
		pdfg.MarginTop.Set(6)
		pdfg.MarginBottom.Set(16)
		pdfg.NoPdfCompression.Set(true)
		if len(currentPrintdetail) > 0 {

			html := "<html><head><meta charset='UTF-8'><meta name='viewport' content='width=device, inital-scale=1.0'>" +

				"<style> * {box-sizing: border-box;} body {font-family: 'Merriweather Sans', sans-serif;} .vl {border-left: 2px dotted;height: 100% ;position: absolute;left: 51.6%;top: 0;}  .column {float: left;width: 50%;}.row:after {content: '';display: table;clear: both;} .button {background-color: #3c8a74;border: none;color: white;padding: 5px 5px;text-align: center;text-decoration: none;display: inline-block;font-size: 23px;margin: 6px 4px;width: 67px;font-weight: 700;height: 43px;margin-top: 13px;}hr {display: block;margin-top: 4.0em;margin-bottom: 0.5em;width: 500px;border-style: solid;border-width: 2px;} hr1 {display: block;margin-top: 4.0em;margin-bottom: 0.5em;width: 500px;border-style: solid;border-width: 2px;} .horizontal_dotted_line{border-bottom: 6px dotted;width: 100% ;position: absolute;left: 0;}.buttonback {background-color: #3c8a74;border: none;color: white;padding: 5px 5px;text-align: center;text-decoration: none;display: inline-block;font-size: 14px;margin: 6px 5px;width: 34px;height: 34px;border-radius: 23px;} .buttonback {background-color: #3c8a74;border: none;color: white;padding: 5px 5px;text-align: center;text-decoration: none;display: inline-block;font-size: 19px;margin: 6px 5px;width: 50px;height: 50px;border-radius: 39px;}</style></head>" +
				"<body><div class='main' style='width:1489px;height:2048px;'> <div class='row' style=''>"
			ivalue := 0
			positionvalue := 1
			for v1, v := range currentPrintdetail {
				fmt.Println(v)
				teamMemberAccessarraystring := strings.Split(v["accessrolearray"], ",")
				if !common.Checkeventoradd(positionvalue) {
					html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box;'>" +
						"<div class='page-col' style='height: 957px;width: 689px;border-style:solid;margin: 40px;border-width: 3px;'>" +
						"<div class='col' style='float: left;padding:1px;height:100%;'>" +
						"<img src='https://testgo.gogushou.com/static/images/gushou side banner .jpg' style='width: 119px;height: 100%;'>" +
						"</div>" +
						"<div class='row' style='width: 100%;'>" +
						"<img src='https://testgo.gogushou.com/static/images/icf logo.png' style='width: 270px;height: 216px;display: block;margin-right: auto;margin-left:auto;margin-top: 20px;'>"
					if v["filepath"] != "" && v["fileexist"] == "true" {
						html += "<img src='" + v["filepath"] + "' style='width:200px;height:200px;border-radius:50%;margin-left: auto;margin-right:auto;display:block;margin-top: 22px;'>"
					} else {
						html += "<img src='https://testgo.gogushou.com/static/images/user-default.png' style='width:200px;height:200px;border-radius:50%;margin-left: auto;margin-right:auto;display:block;margin-top: 22px;'>"
					}

					html += "<span style='font-size: 200%;display:block;text-align:center;margin-top: 12px;'>" + v["firstname"] + " " + v["lastname"] + "</span>" +
						"<span style='text-align:center;display:  block;font-size: 150%;'>" + v["eventname"] + "</span>" +
						"<span style='text-align:center;font-size: 135%;padding-left: 100px;padding-right: 96px; display:table;margin: 0 auto;margin-top: 18px;' >" + v["member_role"] + "</span>" +
						"<div style='display: block;text-align: center;margin-top: 29px;'>"

					for _, v11 := range teamMemberAccessarraystring {
						if v11 != "" {
							html += "<input type='button' class='button' value=" + v11 + " style='margin-right: 20px;'>"
						}
					}
					html += "</div>" +
						"<img src='https://testgo.gogushou.com/static/images/lake lanier logo.png' style='width:330px;height: 120px;display: block;margin-right: auto;margin-left:  auto;margin-top: 44px;'>" +
						"<hr>" +
						" </div></div></div><div class='vl'></div>"
					ivalue = ivalue + 1
					positionvalue = positionvalue + 1
				} else if common.Checkeventoradd(positionvalue) {
					html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box; padding-left:37px'>" +
						"<div class='page-col' style='height: 957px;width: 689px;border-style:solid;margin: 40px;border-width: 3px;'>" +
						"<div class='col' style='float: left;padding:1px;height:100%;'>" +
						"<img src='https://testgo.gogushou.com/static/images/gushou side banner .jpg' style='width: 119px;height: 100%;'>" +
						"</div>" +
						"<div class='row' style='width: 100%;'>" +
						"<img src='https://testgo.gogushou.com/static/images/icf logo.png' style='width: 270px;height: 216px;display: block;margin-right: auto;margin-left:auto;margin-top: 20px;'>"
					if v["filepath"] != "" && v["fileexist"] == "true" {
						html += "<img src='" + v["filepath"] + "' style='width:200px;height:200px;border-radius:50%;margin-left: auto;margin-right:auto;display:block;margin-top: 22px;'>"
					} else {
						html += "<img src='https://testgo.gogushou.com/static/images/user-default.png' style='width:200px;height:200px;border-radius:50%;margin-left: auto;margin-right:auto;display:block;margin-top: 22px;'>"
					}
					html += "<span style='font-size: 200%;display:block;text-align:center;margin-top: 12px;'>" + v["firstname"] + " " + v["lastname"] + "</span>" +
						"<span style='text-align:center;display:  block;font-size: 150%;'>" + v["eventname"] + "</span>" +
						"<span style='text-align:center;font-size: 135%;padding-left: 100px;padding-right: 96px; display:table;margin: 0 auto;margin-top: 18px;' >" + v["member_role"] + "</span>" +
						"<div style='display: block;text-align: center;margin-top: 29px;'>"
					for _, v11 := range teamMemberAccessarraystring {
						if v11 != "" {
							html += "<input type='button' class='button' value=" + v11 + " style='margin-right: 20px;'>"
						}
					}
					html += "</div>" +
						"<img src='https://testgo.gogushou.com/static/images/lake lanier logo.png' style='width:330px;height: 120px;display: block;margin-right: auto;margin-left:  auto;margin-top: 44px;'>" +
						"<hr>" +
						" </div></div></div> <div class='vl'></div>"
					ivalue = ivalue + 1
					positionvalue = positionvalue + 1
				}

				if ivalue < 4 && len(currentPrintdetail)-1 == v1 {
					for j := ivalue; j < 4; j++ {
						if !common.Checkeventoradd(positionvalue) {
							html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box;'>" +
								"<div class='page-col' style='height: 957px;width: 689px;border-style:solid;margin: 40px;border-width: 3px;'>" +
								"</div></div><div class='vl'></div>"
							positionvalue = positionvalue + 1
							ivalue = ivalue + 1
						} else if common.Checkeventoradd(positionvalue) {
							html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box; padding-left:37px'>" +
								"<div class='page-col' style='height: 957px;width: 689px;border-style:solid;margin: 40px;border-width: 3px;'>" +
								"</div></div>"
							positionvalue = positionvalue + 1
							ivalue = ivalue + 1
						}
					}
				}

				if ivalue == 4 || len(currentPrintdetail)-1 == v1 {
					ivalue2 := ivalue
					ivalue = 0
					for j := 1; j <= 4; j++ {
						if !common.Checkeventoradd(positionvalue) {
							html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box;'>" +
								"<div class='page-col' style='height: 957px;width: 687px;border-style:solid;margin: 40px;border-width: 3px;'>" +
								"<div class='row' style='display: block;width: 100%;float:  left;'>" +
								"<img src='https://testgo.gogushou.com/static/images/back header.png' style='padding:1px;width:233px;height: 130px;margin-right: auto;margin-left:  auto;margin-top: 0px;'>" +
								"<img src='https://testgo.gogushou.com/static/images/icf logo.png' style='width: 270px;height: 216px;display: block;margin-right: auto;margin-left:  auto;margin-top: -35px;'>" +
								"<div style='display: block;text-align:  center; margin-top: 30px;'>" +
								"<span style='font-size: 200%;text-align:center;margin-top: 12px;'></span>" +
								"</div>" +
								"<div>" +
								"<input type='button' class='buttonback' value='AV' style='display: block;float:left;margin-left: 26px;'>" +
								"<span style='font-size: 150%;margin-left: 76px;display: block;'>" +
								"<p style='font-size: 95%;font-weight: 500;padding-top: 15px;'>Athlete Village: Paddlers, Drummers Coaches, Team Leaders, Team Support Staff, Event Staff, Official</p>" +
								"</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='VIP' style='margin-left: 26px; margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;margin-top: 12px;margin-left: 7px;'>VIP: NF President, Secretary,Event Staff, Official</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='TA' style='margin-left: 26px;margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;text-align:  center;margin-top: 11px;margin-right: 125px;margin-left: 7px;'>Tower Access: Official, Event Staff</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='MC' style='margin-left: 26px;margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;margin-top: 12px;margin-left: 7px;'>Media Center: Media, Event Staff, Official</span>" +
								"</div>" +
								"<img src='https://testgo.gogushou.com/static/images/lake lanier logo.png' style='width:330px;height: 120px;display: block;margin-right: auto;margin-left:  auto;margin-top: 44px;'>" +
								"<img src='https://testgo.gogushou.com/static/images/back footer.png' style='padding:1px;width:100%;height:151px;display: block;margin-right: auto;margin-left:  auto;margin-top: -9px;'>" +
								"</div></div></div><div class='vl'></div>"
							positionvalue = positionvalue + 1
						} else if common.Checkeventoradd(positionvalue) {
							html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box; padding-left:37px'>" +
								"<div class='page-col' style='height: 957px;width: 687px;border-style:solid;margin: 40px;border-width: 3px;'>" +
								"<div class='row' style='display: block;width: 100%;float:  left;'>" +
								"<img src='https://testgo.gogushou.com/static/images/back header.png' style='padding:1px;width:233px;height: 130px;margin-right: auto;margin-left:  auto;margin-top: 0px;'>" +
								"<img src='https://testgo.gogushou.com/static/images/icf logo.png' style='width: 270px;height: 216px;display: block;margin-right: auto;margin-left:  auto;margin-top: -35px;'>" +
								"<div style='display: block;text-align:  center; margin-top: 30px;'>" +
								"<span style='font-size: 200%;text-align:center;margin-top: 12px;'></span>" +
								"</div>" +
								"<div>" +
								"<input type='button' class='buttonback' value='AV' style='display: block;float:left;margin-left: 26px;'>" +
								"<span style='font-size: 150%;margin-left: 76px;display: block;'>" +
								"<p style='font-size: 95%;font-weight: 500;padding-top: 15px;'>Athlete Village: Paddlers, Drummers Coaches, Team Leaders, Team Support Staff, Event Staff , Official</p>" +
								"</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='VIP' style='margin-left: 26px; margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;margin-top: 12px;margin-left: 7px;'>VIP: NF President, Secretary,Event Staff , Official</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='TA' style='margin-left: 26px;margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;text-align:  center;margin-top: 11px;margin-right: 125px;margin-left: 7px;'>Tower Access: Official, Event Staff</span>" +
								"</div>" +
								"<div style='display: block;margin-top: 15px;'>" +
								"<input type='button' class='buttonback' value='MC' style='margin-left: 26px;margin-right: 0;'>" +
								"<span style='font-size: 150%;font-weight: 500;margin-top: 12px;margin-left: 7px;'>Media Center: Media, Event Staff, Official</span>" +
								"</div>" +
								"<img src='https://testgo.gogushou.com/static/images/lake lanier logo.png' style='width:330px;height: 120px;display: block;margin-right: auto;margin-left:  auto;margin-top: 44px;'>" +
								"<img src='https://testgo.gogushou.com/static/images/back footer.png' style='padding:1px;width:100%;height:151px;display: block;margin-right: auto;margin-left:  auto;margin-top: -9px;'>" +
								"</div></div></div>"
							positionvalue = positionvalue + 1
						}
					}

					if ivalue2 < 4 {
						for j := ivalue2; j <= 4; j++ {
							if !common.Checkeventoradd(positionvalue) {
								html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box;'>" +
									"<div class='page-col' style='height: 957px;width: 687px;border-style:solid;margin: 40px;border-width: 3px;'>" +
									"</div></div><div class='vl'></div>"
								positionvalue = positionvalue + 1
							} else if common.Checkeventoradd(positionvalue) {
								html += "<div class='column' style='float: left;width: 50%;box-sizing: border-box; padding-left:37px'>" +
									"<div class='page-col' style='height: 957px;width: 687px;border-style:solid;margin: 40px;border-width: 3px;'>" +
									"</div></div>"
								positionvalue = positionvalue + 1
							}
						}
					}
				}

			}
			html += "</div></div></div><body></html>"

			pdfg.AddPage(wkhtml.NewPageReader(strings.NewReader(html)))
		} else {
			pdfg.AddPage(wkhtml.NewPageReader(strings.NewReader("No Record In this File")))
		}
		// html1 := ""

		// Add to document

		// Create PDF document in internal buffer
		err = pdfg.Create()
		if err != nil {
			fmt.Println(err)
			resulterrortxt = "PDF Not Create"
		}

		tmpFile, err := ioutil.TempFile(os.TempDir(), "prefix-")
		if err != nil {
			resulterrortxt = "Cannot create temporary file"
		}

		// Write buffer contents to file on disk
		err = pdfg.WriteFile(tmpFile.Name())
		if err != nil {
			resulterrortxt = "Pdf WriteFile Error"
		}

		streamPDFbytes, err := ioutil.ReadFile(tmpFile.Name())

		if err != nil {
			resulterrortxt = "Pdf ReadFile  Error"
		}
		c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")
		c.Ctx.Output.Header("Content-Disposition", "attachment; filename=generalacc.pdf")

		defer os.Remove(tmpFile.Name())
		if resulterrortxt != "" {
			c.Ctx.Output.JSON(resulterrortxt, false, false)
		} else {
			c.Ctx.Output.Body(streamPDFbytes)
		}
	} else {
		c.Redirect("/", 302)
		return
	}

	fmt.Println("Out DownloadAccrediationPdf")

}

func (c *EventController) GetAccGeneralUpdateDetail() {

	c.TplName = "event/eventAccrediation.html"

	teamusereventid := c.GetString("teamusereventid")
	sliptarray := strings.Split(teamusereventid, "_")

	result := models.GetAccGeneralUpdateDetailByTeamEventUserId(sliptarray[0], sliptarray[1], sliptarray[2])
	result1 := models.GetRoleAccessDetail()

	outputObj := &event_acc_role_struct{
		Userdetail:          result,
		Allroleaccessdetail: result1,
	}

	c.Ctx.Output.JSON(outputObj, false, false)

	fmt.Println("Out GetAccGeneralUpdateDetail")
}

func (c *EventController) UpdateManualAccData() {
	c.TplName = "event/eventAccrediation.html"

	if c.Ctx.Input.Method() == "POST" {
		accusername := c.GetString("accusername")
		accroleselectmultiple := c.GetString("accuserrole")
		accaccessrole := c.GetString("accaccessrole")
		acc_even_id := c.GetString("acc_even_id")
		accmanualuserid := c.GetString("accmanualuserid")
		accfilepath := c.GetString("accfilepath")
		accmanualupdateid := c.GetString("accmanualupdateid")

		token := ""
		path := "/static/images/user-default.png"
		result := true
		if accfilepath != "" {
			path = accfilepath
		}

		creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
		_, err := creds.Get()
		if err != nil {
			fmt.Printf("bad credentials: %s", err)
		}
		cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
		svc := s3.New(session.New(), cfg)

		f, header, _ := c.GetFile("file")
		if f != nil {
			defer f.Close()

			out, err := os.Create("/tmp/" + header.Filename)
			if err != nil {
				fmt.Println("Unable to create the file for writing. Check your write access privilege")
				return
			}

			defer out.Close()
			// write the content from POST to the file
			_, err = io.Copy(out, f)
			if err != nil {
				fmt.Println(err)
			}

			fmt.Println("File uploaded successfully to tmp folder: " + header.Filename)

			file, err := os.Open("/tmp/" + header.Filename)
			if err != nil {
				fmt.Printf("err opening file: %s", err)
			}

			defer file.Close()
			fileInfo, _ := file.Stat()
			size := fileInfo.Size()
			buffer := make([]byte, size) // read file content to buffer

			file.Read(buffer)
			fileBytes := bytes.NewReader(buffer)
			fileType := http.DetectContentType(buffer)

			ext := filepath.Ext(file.Name())

			host_name := os.Getenv("HOST_NAME")

			if host_name == "gogushoulive" {
				path = "/images/" + time.Now().Format("20060102150405") + "_image" + ext
			} else {
				path = "/testingImages/" + time.Now().Format("20060102150405") + "_image" + ext
			}

			params := &s3.PutObjectInput{
				Bucket:        aws.String(os.Getenv("BUCKET")),
				Key:           aws.String(path),
				Body:          fileBytes,
				ACL:           aws.String("public-read"),
				ContentLength: aws.Int64(size),
				ContentType:   aws.String(fileType),
			}
			resp, err := svc.PutObject(params)
			fmt.Println(resp, err)

			if err != nil {
				result = false
			}

			os.Remove("/tmp/" + header.Filename)
		}
		if result {
			currentuserID := "0"
			if models.IsLoggedIn(c.GetSession("GushouSession")) {
				id := c.GetSession("UserId").(int)
				currentuserID = strconv.Itoa(id)
			}

			if accmanualupdateid == "0" {
				result = models.SaveManualAccDetail(accusername, accroleselectmultiple, accaccessrole, acc_even_id, accmanualuserid, path, currentuserID)
			} else if accmanualupdateid != "0" && accmanualupdateid != "" {
				result = models.UpdateManualAccDetail(accusername, accroleselectmultiple, accaccessrole, acc_even_id, path, accmanualupdateid, currentuserID)
			}

		}

		c.Ctx.Output.JSON(result, false, false)
	}
	fmt.Println("Out GetAccGeneralUpdateDetail")
}
func (c *EventController) DownloadTeamReport() {

	c.TplName = "event/eventReport.html"

	if c.Ctx.Input.Method() == "POST" {
		var event_reg_id string
		pdf := gofpdf.New("P", "mm", "A4", "")

		c.Ctx.Input.Bind(&event_reg_id, "getMulTeamName")
		fmt.Println(event_reg_id)
		result, team_name := models.GetRosterDetailForTeam(event_reg_id)
		fmt.Println(team_name)
		pdffilename := ""
		fmt.Println(len(team_name))
		if len(team_name) > 0 {
			arr := common.AllSameStrings(team_name)
			if arr {
				pdffilename = team_name[0] + " Crew Lists"

			} else {
				pdffilename = "Team Crew Lists"
			}
		} else {
			pdffilename = "Team Crew Lists"
		}

		fmt.Println(pdffilename)
		fileName := ReportCommonContents(result, pdf)
		fmt.Println(fileName)
		err := pdf.OutputFileAndClose(fileName)
		fmt.Println(err)
		fmt.Println("temp file name--", fileName)
		if err == nil {
			streamPDFbytes, err := ioutil.ReadFile(fileName)

			if err != nil {
				fmt.Println(err)
			}

			c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")
			c.Ctx.Output.Header("Content-Disposition", "attachment; filename="+pdffilename+".pdf")

			if err != nil {
				c.Ctx.Output.JSON(err, false, false)
			} else {
				c.Ctx.Output.Body(streamPDFbytes)
			}
			defer os.Remove(fileName)

			//c.Ctx.Output.Body(streamPDFbytes)
		} else {
			c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
		}
	}

}
func (c *EventController) DownloadRaceReport() {

	c.TplName = "event/eventReport.html"

	if c.Ctx.Input.Method() == "POST" {
		var race, getEventId string
		pdf := gofpdf.New("P", "mm", "A4", "")

		c.Ctx.Input.Bind(&race, "getMulRaceType")
		fmt.Println(race)

		c.Ctx.Input.Bind(&getEventId, "getEventId")
		fmt.Println(getEventId)

		result, team_name := models.GetRosterDetailForRace(race, getEventId)
		fmt.Println(team_name)
		var pdffilename = ""
		if len(team_name) > 0 {
			arr := common.AllSameStrings(team_name)
			if arr {
				pdffilename = team_name[0] + " Crew Lists"

			} else {
				pdffilename = "Race Crew Lists"
			}
		} else {
			pdffilename = "Race Crew Lists"
		}

		fmt.Println(pdffilename)
		fileName := ReportCommonContents(result, pdf)
		fmt.Println(fileName)
		err := pdf.OutputFileAndClose(fileName)
		fmt.Println(err)
		fmt.Println("temp file name--", fileName)
		if err == nil {
			streamPDFbytes, err := ioutil.ReadFile(fileName)

			if err != nil {
				fmt.Println(err)
			}

			c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")
			c.Ctx.Output.Header("Content-Disposition", "attachment; filename="+pdffilename+".pdf")

			if err != nil {
				c.Ctx.Output.JSON(err, false, false)
			} else {
				c.Ctx.Output.Body(streamPDFbytes)
			}
			defer os.Remove(fileName)

			//c.Ctx.Output.Body(streamPDFbytes)
		} else {
			c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
		}
	}

}
func ReportCommonContents(result []map[string]string, pdf *gofpdf.Fpdf) string {

	var fileName = ""
	//pdf := gofpdf.New("P", "mm", "A4", "")
	for _, v1 := range result {

		team_id := v1["team_id"]
		var roster_id = v1["roster_id"]
		var event_reg_id = v1["event_reg_id"]

		fmt.Println(team_id)
		teamdetails := models.GetEventRegDetails(event_reg_id)
		if len(teamdetails) > 0 {
			for _, v2 := range teamdetails {
				var team_name = v2["team_name"].(string)
				var reg_captain_name = v2["reg_captain_name"].(string)
				var reg_email_id = v2["reg_email_id"].(string)
				var reg_phone_number = v2["reg_phone_number"].(string)
				var fk_captain_id = v2["fk_captain_id"].(string)
				user_details := models.GetUserDetails(fk_captain_id)

				captain_email := user_details["user"]["email_address"].(string)
				captain_name := user_details["user"]["name"].(string) + " " + user_details["user"]["last_name"].(string)
				captain_phone := " "
				if user_details["user"]["country_dial_code"] != nil && user_details["user"]["phone"] != nil {
					captain_phone = user_details["user"]["country_dial_code"].(string) + " " + user_details["user"]["phone"].(string)

				} else {
					captain_phone = " "

				}

				division := v1["division"]
				full_div_name := models.GetFullDivName(division)
				clases := v1["class"]
				var full_class_name = ""
				if clases == "M" {
					full_class_name = "Men"
				} else if clases == "W" {
					full_class_name = "Women"
				} else if clases == "MX" {
					full_class_name = "Mixed"
				}
				boat_type := v1["boat_type"]
				distance := v1["distance"]
				var full_diatance = ""
				if distance == "200" {
					full_diatance = "200m"
				} else if distance == "500" {
					full_diatance = "500m"
				} else if distance == "2000" {
					full_diatance = "2km"
				} else if distance == "1000" {
					full_diatance = "1km"
				}

				tmpFile, err := ioutil.TempFile(os.TempDir(), "prefix-")
				if err != nil {

				}
				marginH := 15.0

				pdf.SetTopMargin(20)

				dte := time.Now()
				tempCurrentFormatDate := dte.Format("Mon-02-2006")
				fmt.Println(tempCurrentFormatDate)
				pdf.SetFooterFunc(func() {

					pdf.SetY(-20)
					pdf.SetFont("Arial", "I", 8)
					pdf.CellFormat(0, 10, fmt.Sprintf("Page %d/{nb}", pdf.PageNo()),
						"", 0, "C", false, 0, "")

					pdf.SetY(-15)
					pdf.SetFont("Arial", "I", 8)
					pdf.CellFormat(0, 10, "Exported from Gushou, goGushou.com, "+tempCurrentFormatDate+" | dragonboat@gogushou.com",
						"", 0, "C", false, 0, "")
				})

				pdf.SetMargins(marginH, 15, marginH)
				pdf.SetFont("Times", "", 12)
				pdf.SetAutoPageBreak(false, 0)
				tr := pdf.UnicodeTranslatorFromDescriptor("")
				pdf.AddPage()

				pagew, pageh := pdf.GetPageSize()
				fmt.Println(pagew)
				fmt.Println(pageh)

				pdf.AliasNbPages("")

				marginCell := 2.
				_, _, _, mbottom := pdf.GetMargins()

				cols_table1 := []float64{70, 110}

				aligntb1 := []string{"LM", "LM"}

				rows := [][]string{}

				rows = append(rows, []string{"Name of Federation", tr(team_name)})
				if reg_captain_name == captain_name {
					rows = append(rows, []string{"Contact person", tr(reg_captain_name)})
				} else {
					rows = append(rows, []string{"Contact person", tr(reg_captain_name) + " / " + tr(captain_name)})
				}
				if reg_email_id == captain_email {
					rows = append(rows, []string{"E-mail", tr(reg_email_id)})
				} else {
					rows = append(rows, []string{"E-mail", tr(reg_email_id) + " / " + tr(captain_email)})

				}
				if reg_phone_number == captain_phone {
					rows = append(rows, []string{"Phone", reg_phone_number})
				} else {
					rows = append(rows, []string{"Phone", reg_phone_number + " / " + captain_phone})
				}

				pdf.SetX(pdf.GetX() + 15)
				pdf.Image("static/images/icf logo.png", 10, 7, 26, 26, false, "", 0, "")
				pdf.SetY(pdf.GetY() + 5)

				//pdf.SetX(155)
				pdf.SetX(pdf.GetX() + 145)
				pdf.Image("static/images/lake lanier logo.png", -70, 15, 35, 20, false, "", 0, "")
				pdf.SetY(pdf.GetY() + 5)

				pdf.SetFont("Arial", "B", 17)
				pdf.SetY(pdf.GetY() + 10)
				pdf.CellFormat(40, 7, "NOMINAL ENTRY", "0", 0, "M", false, 0, "")

				pdf.SetTextColor(24, 24, 24)
				pdf.SetFillColor(255, 255, 255)
				pdf.SetFont("Arial", "B", 12)
				pdf.SetY(pdf.GetY() + 13)

				pdf = RowgenerateReport(rows, pdf, marginCell, cols_table1, pageh, mbottom, aligntb1)

				pdf.SetFont("Arial", "B", 13)
				pdf.SetY(pdf.GetY() + 5)
				pdf.CellFormat(40, 7, "RACE: "+tr(boat_type)+" Seater "+tr(full_div_name)+" "+tr(full_class_name)+" "+tr(full_diatance), "0", 0, "L", false, 0, "")

				rows1 := [][]string{}

				cols := []float64{10, 45, 40, 40, 17, 28}

				align1 := []string{"LM", "LM", "LM", "LM", "LM", "LM"}
				//align2 := []string{"LM", "C", "C", "C","C","C"}

				pdf.SetFont("Arial", "", 12)
				pdf.SetY(pdf.GetY() + 12)
				header := [6]string{"No", "Position", "Surname", "Name", "Gender", "Date Of Birth"}

				pdf.SetTextColor(224, 224, 224)
				pdf.SetFillColor(64, 64, 64)
				for colJ := 0; colJ < 6; colJ++ {
					pdf.CellFormat(cols[colJ], 10, header[colJ], "1", 0, "CM", true, 0, "")
				}
				pdf.Ln(-1)

				team_roster_roles := models.GetTeamRosterMemberRole(roster_id)

				for i, tm := range team_roster_roles {

					var member_role = tm["member_role"].(string)
					var sr_no = i + 1
					member_role = strings.Replace(member_role, `}`, "", -1)
					member_role = strings.Replace(member_role, `{`, "", -1)

					temproleArray := strings.Split(member_role, ",")

					flag := "false"
					var role []string
					if common.StringInArray("steerer", temproleArray) {
						flag = "true"
						role = append(role, "Steerer")
					}
					if common.StringInArray("drummer", temproleArray) {
						flag = "true"
						role = append(role, "Drummer")
					}
					if common.StringInArray("paddler", temproleArray) {
						flag = "true"
						role = append(role, "Paddler")
					}
					if common.StringInArray("substitute", temproleArray) {
						flag = "true"
						role = append(role, "Substitute")
					}

					fmt.Println("role--", role)
					roleString := strings.Join(role, ",")
					fmt.Println("roleString--", roleString)

					if flag == "true" {
						//var roster_member_id = tm["roster_member_id"].(string)
						//roster_mem_details := models.GetUserDetails(roster_member_id)

						member_fname := tm["name"].(string)
						member_lname := tm["last_name"].(string)
						member_gender := tm["classes"].(string)
						member_dob := tm["dob_format_report"].(string)

						var gender = ""
						if member_gender == "W" {
							gender = "F"
						}
						if member_gender == "M" {
							gender = "M"
						}

						// add a new page if the height of the row doesn't fit on the page
						if i == 26 {
							pdf.AddPage()
							pdf.SetY(pdf.GetY() + 20)

						}
						rows1 = append(rows1, []string{strconv.Itoa(sr_no), roleString, tr(member_lname), tr(member_fname), gender, member_dob})

						pdf.SetTextColor(24, 24, 24)
						pdf.SetFillColor(255, 255, 255)
						pdf.SetFont("Arial", "", 10)

						pdf = RowgenerateReport(rows1, pdf, marginCell, cols, pageh, mbottom, align1)
						rows1 = [][]string{}
					} else {
						rows2 := [][]string{}

						rows2 = append(rows2, []string{"", "", "", "", ""})
						pdf.SetTextColor(24, 24, 24)
						pdf.SetFillColor(255, 255, 255)
						pdf.SetFont("Arial", "", 10)

						pdf = RowgenerateReport(rows2, pdf, marginCell, cols, pageh, mbottom, align1)
						rows2 = [][]string{}
					}

				}
				if pdf.GetY() > pageh-mbottom-20 {
					pdf.AddPage()

				}
				pdf.SetFont("Arial", "B", 13)
				pdf.SetTextColor(24, 24, 24)
				pdf.SetFillColor(255, 255, 255)
				pdf.SetY(pdf.GetY() + 5)
				pdf.CellFormat(40, 7, "DATE:                                                                                      SIGNATURE:", "0", 0, "L", false, 0, "")

				// if pdf.GetY() > pageh-mbottom-20 {
				// 	pdf.AddPage()
				// 	pdf.SetY(pdf.GetY() + 20)

				// }
				// pdf.SetFont("Arial", "B", 13)
				// pdf.SetY(pdf.GetY() + 12)
				// pdf.CellFormat(40, 7, "SIGNATURE:", "0", 0, "L", false, 0, "")

				fileName = tmpFile.Name()
				//pdf.AddPage()
			}

		}

	}
	return fileName
}
func RowgenerateReport(rows [][]string, pdf *gofpdf.Fpdf, marginCell float64, cols []float64, pageh float64, mbottom float64, align []string) (f *gofpdf.Fpdf) {

	for _, row := range rows {
		curx, y := pdf.GetXY()
		x := curx

		height := 0.
		_, lineHt := pdf.GetFontSize()

		for i, txt := range row {
			lines := pdf.SplitLines([]byte(txt), cols[i])
			h := float64(len(lines))*lineHt + marginCell*float64(len(lines))
			if h > height {
				height = h
			}
		}
		// add a new page if the height of the row doesn't fit on the page
		if pdf.GetY()+height > pageh-mbottom-20 {
			pdf.AddPage()
			//pdf.SetY(pdf.GetY() + 20)
			y = pdf.GetY()
		}
		for i, txt := range row {
			width := cols[i]
			align1 := align[i]
			pdf.Rect(x, y, width, height, "")
			pdf.MultiCell(width, lineHt+marginCell, txt, "", align1, false)
			x += width
			pdf.SetXY(x, y)
		}
		pdf.SetXY(curx, y+height)
	}
	return pdf
}

func (c *EventController) DownloadIndividualTeamReport() {

	c.TplName = "event/eventReport.html"
	pdf := gofpdf.New("P", "mm", "A4", "")

	event_reg_id := c.Ctx.Input.Param(":event_reg_id")
	boat := c.Ctx.Input.Param(":boat")
	div := c.Ctx.Input.Param(":div")
	class := c.Ctx.Input.Param(":class")
	dist := c.Ctx.Input.Param(":dist")
	teamdetails := models.GetEventRegDetails(event_reg_id)
	var pdffilename = ""
	if len(teamdetails) > 0 {
		for _, v2 := range teamdetails {
			var team_name = v2["team_name"].(string)
			pdffilename = team_name + " Crew Lists"
		}
	}
	fmt.Println(pdffilename)
	result := models.GetRosterDetailForIndividualTeam(event_reg_id, boat, div, class, dist)
	fileName := ReportCommonContents(result, pdf)
	fmt.Println(fileName)
	err := pdf.OutputFileAndClose(fileName)
	fmt.Println(err)
	fmt.Println("temp file name--", fileName)
	if err == nil {
		streamPDFbytes, err := ioutil.ReadFile(fileName)

		if err != nil {
			fmt.Println(err)
		}

		c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")
		c.Ctx.Output.Header("Content-Disposition", "attachment; filename="+pdffilename+".pdf")

		if err != nil {
			c.Ctx.Output.JSON(err, false, false)
		} else {
			c.Ctx.Output.Body(streamPDFbytes)
		}
		defer os.Remove(fileName)

		//c.Ctx.Output.Body(streamPDFbytes)
	} else {
		c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
	}

}
func (c *EventController) DownloadMultipleTeamReport() {

	c.TplName = "event/eventReport.html"
	pdf := gofpdf.New("P", "mm", "A4", "")

	var getselectedMemForReport string

	c.Ctx.Input.Bind(&getselectedMemForReport, "getselectedMemForReport")
	fmt.Println(getselectedMemForReport)

	data_arr := strings.Split(getselectedMemForReport, ",")
	var fileName = ""
	for _, mulTeam := range data_arr {

		data_arr := strings.Split(mulTeam, "_")
		event_reg_id := data_arr[0]
		boat := data_arr[1]
		div := data_arr[2]
		class := data_arr[3]
		dist := data_arr[4]
		result := models.GetRosterDetailForIndividualTeam(event_reg_id, boat, div, class, dist)
		fileName = ReportCommonContents(result, pdf)

	}
	fmt.Println(fileName)
	err := pdf.OutputFileAndClose(fileName)
	fmt.Println(err)
	fmt.Println("temp file name--", fileName)
	if err == nil {
		streamPDFbytes, err := ioutil.ReadFile(fileName)

		if err != nil {
			fmt.Println(err)
		}

		c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")
		c.Ctx.Output.Header("Content-Disposition", "attachment; filename=Team Crew Lists.pdf")

		if err != nil {
			c.Ctx.Output.JSON(err, false, false)
		} else {
			c.Ctx.Output.Body(streamPDFbytes)
		}
		defer os.Remove(fileName)

		//c.Ctx.Output.Body(streamPDFbytes)
	} else {
		c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
	}
}

func (c *EventController) GetAccManualUpdateDetail() {

	c.TplName = "event/eventAccrediation.html"
	fmt.Println("In GetAccManualUpdateDetail")
	manualautoid := c.GetString("manualautoid")

	result := models.GetAccManualUpdateDetailByManualAutoId(manualautoid)
	result1 := models.GetRoleAccessDetail()

	outputObj := &event_acc_role_struct{
		Userdetail:          result,
		Allroleaccessdetail: result1,
	}

	c.Ctx.Output.JSON(outputObj, false, false)

	fmt.Println("Out GetAccManualUpdateDetail")
}

func UploadTheEventPaymentInvoiceToS3Bucket(tempFile string, paymentlogId string) {

	token := ""
	creds := credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_ACCESS_KEY"), token)
	_, err := creds.Get()
	if err != nil {
		fmt.Printf("bad credentials: %s", err)
	}
	cfg := aws.NewConfig().WithRegion(os.Getenv("REGION")).WithCredentials(creds)
	svc := s3.New(session.New(), cfg)

	file, err := os.Open(tempFile)
	if err != nil {
		fmt.Printf("err opening file: %s", err)
	}

	defer file.Close()

	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer

	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	ext := ".pdf"

	host_name := os.Getenv("HOST_NAME")
	path := ""
	if host_name == "gogushoulive" {
		path = "/images/" + time.Now().Format("20060102150405") + "_invoice" + ext
	} else {
		path = "/testingImages/" + time.Now().Format("20060102150405") + "_invoice" + ext
	}
	params := &s3.PutObjectInput{
		Bucket:        aws.String(os.Getenv("BUCKET")),
		Key:           aws.String(path),
		Body:          fileBytes,
		ACL:           aws.String("public-read"),
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
	}
	_, err = svc.PutObject(params)

	if err != nil {
		fmt.Printf("bad response: %s", err)
	} else {
		models.SaveEventPaymentLogUrl(path, paymentlogId)
		//c.Ctx.Output.Body([]byte(result))
	}

	err = os.Remove(tempFile)
	if err == nil {
		fmt.Println("File removed successfully from tmp folder: " + tempFile)
	}

}
