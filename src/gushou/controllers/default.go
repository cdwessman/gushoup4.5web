package controllers

import (
	"github.com/astaxie/beego"
	"gushou/models"
	_"strings"
	"fmt"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.TplName = "landing.html"
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	fmt.Println()
}

func (c *MainController) Features() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	c.TplName = "features.html"
}

func (c *MainController) Pricing() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	c.TplName = "pricing.html"
}

func (c *MainController) SubsNewsletter() {
	fmt.Println("In SubsNewsletter")
	
	if c.Ctx.Input.Method() == "POST" {

		fmt.Println("In post")

		email := c.GetString("email")

		result := models.AddSubscription(email)

		fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
	}

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

	} else {
		c.Data["IsLoggedIn"] = 0
	}

	c.TplName = "pricing.html"
}



