package controllers

import (
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"gushou/models"
	"html/template"
	"io/ioutil"
	"math/big"
	"os"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/jung-kurt/gofpdf"
	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/payout"
)

type StripeAPIController struct {
	beego.Controller
}

type add_subscription_response struct {
	RedirectUrl string `json:"redirecturl"`
	Status      string `json:"status"`
}

type add_change_card_subscription_response struct {
	CardId string `json:"cardid"`
	Status string `json:"status"`
}

type eventpaymentdetail struct {
	Teamid               string `json:"teamid"`
	Eventslug            string `json:"eventslug"`
	EventRegisterId      string `json:"eventregisterId"`
	CurrencySymbol       string `json:"currencySymbol"`
	Team_Name            string `json:"team_name"`
	RemainingAmount      string `json:"remainingAmount"`
	RemaamountWithFormat string `json:"remaamountwithformat"`
	CurrencyISO          string `json:"currencyiso"`
}

func (c *StripeAPIController) Prepare() {
	c.Data["Team_Manage_Admin"] = c.GetSession("Team_Manage_Admin")
	c.Data["Event_Manage_Admin"] = c.GetSession("Event_Manage_Admin")
	c.Data["Paddling_Admin"] = c.GetSession("Paddling_Admin")
	c.Data["Free_Subcription_Text"] = constants.FREE_TRIAL_SUBCRIPTION
	c.Data["Stripe_Env_Public_Key"] = constants.STRIPE_ENV_PUBLIC_KEY

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")
		//c.Data["TeamCaptainId"] = team_view["team_0"]["fk_captain_id"].(string)

		//user_id := c.GetSession("UserId").(int)
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		c.Data["CurrentUserId"] = user_id

	} else {
		c.Data["IsLoggedIn"] = 0
	}
}

type subscriptionValidate struct {
	Subscription_Radiobox string `form:"free_subscription_radiobox" `
	Subscription_Checkbox string `form:"free_subscription_checkbox"`
	SubscriptionPage      string `form:"free_subscription_page"`
}

func (c *StripeAPIController) GetCustomer() {

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}
	} else {
		c.Data["IsLoggedIn"] = 0
		c.Data["UserId"] = "2"
	}

	result, err := models.GetStripeCustomerDetails(c.Data["UserId"].(string))

	if err != nil {
		c.Data["json"] = err
	} else {
		c.Data["json"] = result
	}
	c.ServeJSON()
}

func (c *StripeAPIController) CreateCustomer() {

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}
	} else {
		c.Data["IsLoggedIn"] = 0
		c.Data["UserId"] = "4"
	}

	// result, err := models.CreateStripeCustomer(c.Data["UserId"].(string))
	// if err != nil {
	// 	c.Data["json"] = err
	// } else {
	// 	c.Data["json"] = result
	// }
	c.ServeJSON()
}

func (c *StripeAPIController) UpdateCustomer() {

	if models.IsLoggedIn(c.GetSession("GushouSession")) {
		c.Data["IsLoggedIn"] = 1
		c.Data["FirstName"] = c.GetSession("FirstName")
		c.Data["LastName"] = c.GetSession("LastName")
		c.Data["UserId"] = c.GetSession("UserId")

		if c.GetSession("SuperAdmin") == "true" {
			c.Data["SuperAdmin"] = "true"
		}
	} else {
		c.Data["IsLoggedIn"] = 0
		c.Data["UserId"] = "4"
	}

	result, err := models.UpdateStripeCustomerDetails(c.Data["UserId"].(string))
	if err != nil {
		c.Data["json"] = err
	} else {
		c.Data["json"] = result
	}
	c.ServeJSON()
}

func (c *StripeAPIController) CreateCard() {

	c.TplName = "user/profileSettings.html"
	fmt.Println("In CreateCard")
	if c.Ctx.Input.Method() == "POST" {
		var outputObj *add_change_card_subscription_response
		token := c.GetString("stripeToken")
		fmt.Println("token", token)
		user_id := c.GetSession("UserId").(int)
		userId := strconv.Itoa(user_id)
		fmt.Println("userId", userId)
		status, cardId := models.CreateCard(userId, token)

		outputObj = &add_change_card_subscription_response{
			CardId: cardId,
			Status: status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(string(b)))
	}
	fmt.Println("Out CreateCard")
}

func (c *StripeAPIController) DeleteOrMakeDefaultCard() {
	c.TplName = "user/profileSettings.html"
	fmt.Println("In DeleteOrMakeDefaultCard")
	status := ""
	responsecardId := ""
	if c.Ctx.Input.Method() == "POST" {
		cardId := c.GetString("cardId")
		optionType := c.GetString("optionType")
		user_id := c.GetSession("UserId").(int)
		userId := strconv.Itoa(user_id)
		if optionType == "make_default" {
			status, responsecardId = models.SetDefaultCard(userId, cardId)
		} else if optionType == "delete" {
			status, responsecardId = models.DeleteCard(userId, cardId)
		}
		outputObj := &add_change_card_subscription_response{
			CardId: responsecardId,
			Status: status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(string(b)))
	}
	fmt.Println("Out DeleteOrMakeDefaultCard")
}

func (c *StripeAPIController) StripeSubscriptionOption() {
	fmt.Println("In StripeSubscriptionOption")

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := subscriptionValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			fmt.Println("Cannot parse form")
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			fmt.Println(c.Data["Errors"])
			return
		}
		data := make(map[string]string)
		data["Subscription_Radiobox"] = items.Subscription_Radiobox
		data["Subscription_Checkbox"] = items.Subscription_Checkbox
		teamId := c.GetSession("TeamId").(string)

		// if data["Subscription_Checkbox"] == "on" {
		// 	models.SetTeam_Subscription_Notification_Popup(teamId, false)
		// }

		if data["Subscription_Radiobox"] == "Subscription" {
			c.Redirect("/stripe/subscription-view", 302)
		} else if data["Subscription_Radiobox"] == "Trial" {
			models.SetContinuOnFreeTrial(teamId)
			resultSlug := models.GetTeamSlugText(teamId)
			c.Redirect("/team-view/"+resultSlug, 302)
		}
	}
	fmt.Println("Out StripeSubscriptionOption")
}

func (c *StripeAPIController) SubscriptionView() {
	fmt.Println("In SubscriptionView")
	c.TplName = "subscription/subscriptionaccountteam.html"
	c.Data["PageTitle"] = "Subscription"
	var subscription_plans []orm.Params
	var subscription_card_detail []orm.Params
	teamId := ""
	c.Data["RemoveSmsStatus"] = false
	c.Data["RemoveCaptainStatus"] = false
	c.Data["FromStettingPage"] = false
	c.Data["DefaultTeamPage"] = false

	if c.GetSession("TeamId") != nil {
		teamId = c.GetSession("TeamId").(string)
	}
	if c.Ctx.Input.Method() == "GET" {
		if c.GetString("removeCaptainStatus") != "" {
			c.SetSession("removeCaptainStatus", c.GetString("removeCaptainStatus"))
			c.SetSession("removeCaptainTeamId", teamId)
			c.SetSession("removeCaptainId", c.GetString("removeCaptainId"))
			c.Data["RemoveCaptainStatus"] = true
			c.DelSession("removeSmsStatus")
			c.DelSession("subscription_from_setting_page")
		} else if c.GetString("removeSmsStatus") != "" {
			c.Data["RemoveSmsStatus"] = true
			c.SetSession("removeSmsStatus", c.GetString("removeSmsStatus"))
			c.DelSession("removeCaptainTeamId")
			c.DelSession("removeCaptainId")
			c.DelSession("removeCaptainStatus")
			c.DelSession("subscription_from_setting_page")
			c.TplName = "subscription/subscriptionaccountsms.html"
		} else if c.GetString("subscription_from_setting_page") != "" {
			c.Data["FromStettingPage"] = true
			c.SetSession("subscription_from_setting_page", c.Data["FromStettingPage"])
			teamId = c.GetString("subscriptionTeamId")
			fmt.Println("teamIdteamIdteamId", teamId)
			c.DelSession("removeSmsStatus")
			c.DelSession("removeCaptainTeamId")
			c.DelSession("removeCaptainId")
			c.DelSession("removeCaptainStatus")

		} else {
			c.Data["DefaultTeamPage"] = true
		}
	}
	temp_yearly_subscriptionamount := "0"
	temp_monthly_subscriptionamount := "0"

	temp_yearly_planid := ""
	temp_monthly_planid := ""
	subscription_plans = models.GetSubscription_Plans()
	if subscription_plans != nil {
		for _, m := range subscription_plans {
			if m["plan_id"] != nil {
				if m["plan_id"].(string) == "yearly-subscription" && m["price_format"] != nil {
					temp_yearly_subscriptionamount = m["price_format"].(string)
					temp_yearly_planid = m["plan_id"].(string)
				} else if m["plan_id"].(string) == "monthly-subscription" && m["price_format"] != nil {
					temp_monthly_subscriptionamount = m["price_format"].(string)
					temp_monthly_planid = m["plan_id"].(string)
				}
			}
		}

	}
	c.Data["Subscription_Plan"] = subscription_plans
	c.Data["Yearly_SubscriptionAmount"] = temp_yearly_subscriptionamount
	c.Data["Monthly_SubscriptionAmount"] = temp_monthly_subscriptionamount
	c.Data["Yearly_Subscription_Plan_Id"] = temp_yearly_planid
	c.Data["Monthly_Subscription_Plan_Id"] = temp_monthly_planid
	c.Data["TeamId"] = teamId
	id := c.GetSession("UserId").(int)
	user_id := strconv.Itoa(id)
	subscription_card_detail = models.GetSubscription_Card_Detail_By_UserId(user_id)

	moneySubscriptionDetail := models.GetMoneySubscriptionDetailByTeamId(teamId, user_id)

	c.Data["Subscription_Card_Detail"] = subscription_card_detail

	c.Data["MoneySubscriptionDetailStatus"] = moneySubscriptionDetail

	fmt.Println("Out SubscriptionView")
}

func (c *StripeAPIController) AddTeamSubscription() {
	fmt.Println("In AddTeamSubscription")
	result := "false"
	var outputObj *add_subscription_response

	if c.Ctx.Input.Method() == "POST" {
		subscriptionplan := c.GetString("subscriptionplan")
		subscriptioncard := c.GetString("subscriptioncard")
		subscriptionteamid := c.GetString("subscriptionteamid")
		subscriptionuserid := c.GetString("subscriptionuserid")
		subscriptionskipstatus := c.GetString("subscriptionskipstatus")

		if c.GetSession("removeCaptainStatus") != nil && c.GetSession("removeCaptainTeamId") != nil {

			removeCaptainTeamId := c.GetSession("removeCaptainTeamId").(string)
			removeCaptainId := c.GetSession("removeCaptainId").(string)
			resultStatus := "false"
			statusText := ""
			statusResult, endDatePeriodTime, teamPaidName := models.RemoveCaptainSubscription(removeCaptainId, removeCaptainTeamId, subscriptionuserid)
			if subscriptionskipstatus == "false" {
				if statusResult == "true" && endDatePeriodTime != "" {
					result = models.AddSubscriptionWithTrialPeriod(endDatePeriodTime, subscriptionplan, subscriptioncard, subscriptionteamid, subscriptionuserid, teamPaidName)
					if result == "true" {
						resultStatus = models.TeamRemoveCaptain(removeCaptainId, removeCaptainTeamId, subscriptionuserid)
						if resultStatus == "true" {
							statusText = "Subscription Sucess. Also You assign as Captain sucessfully"
						} else {
							statusText = "Subscription Sucess.Error in code not assign as captain sucessfully"
						}

					} else {
						statusText = result
					}
				} else if statusResult == "notfalse" || statusResult == "true" {
					result = models.AddTeamSubscription(subscriptionplan, subscriptioncard, subscriptionteamid, subscriptionuserid)
					if result == "true" {
						resultStatus = models.TeamRemoveCaptain(removeCaptainId, removeCaptainTeamId, subscriptionuserid)
						if resultStatus == "true" {
							statusText = "Subscription Sucess. Also You assign as Captain sucessfully"
						} else {
							statusText = "Subscription Sucess.Error in code not assign as captain sucessfully"
						}
					} else {
						statusText = result
					}
				}

			} else {
				if statusResult == "true" {
					resultStatus = models.TeamRemoveCaptain(removeCaptainId, removeCaptainTeamId, subscriptionuserid)
				}
				if resultStatus == "true" {
					statusText = "SuccessFully assign as Captain"
				} else {
					statusText = "Error in code not assign as captain sucessfully"
				}

			}
			tempresultSlug := ""
			if resultStatus == "true" {
				resultSlug := models.GetTeamSlugText(removeCaptainTeamId)
				tempresultSlug = "/team-view/" + resultSlug
			}

			outputObj = &add_subscription_response{
				RedirectUrl: tempresultSlug,
				Status:      statusText,
			}

			c.DelSession("removeCaptainTeamId")
			c.DelSession("removeCaptainId")
			c.DelSession("removeCaptainStatus")

		} else if c.GetSession("removeSmsStatus") != nil {

			resultStatus := models.TeamSubscriptionSmsPlan(subscriptioncard, subscriptionteamid, subscriptionuserid)
			tempresultSlug := ""
			if resultStatus == "true" {
				resultSlug := models.GetTeamSlugText(subscriptionteamid)
				tempresultSlug = "/team-view/" + resultSlug
				result = "Payment Succeed ,Additional SMS added Your Team"
			} else if resultStatus == "false" {
				result = "Payment Not Succeed Please Try Again."
			} else {
				result = resultStatus
			}
			c.DelSession("removeSmsStatus")
			outputObj = &add_subscription_response{
				RedirectUrl: tempresultSlug,
				Status:      result,
			}
		} else {
			result = models.AddTeamSubscription(subscriptionplan, subscriptioncard, subscriptionteamid, subscriptionuserid)
			tempresultSlug := ""
			if result == "true" {
				if c.GetSession("subscription_from_setting_page") != nil {
					tempresultSlug = "/profile-setting"
					result = "Payment Succeed"
					c.DelSession("subscription_from_setting_page")
				} else {
					resultSlug := models.GetTeamSlugText(subscriptionteamid)
					tempresultSlug = "/team-view/" + resultSlug
					result = "Payment Succeed"
				}
			} else if result == "false" {
				result = "Payment Not Succeed Please Try Again."
			}
			outputObj = &add_subscription_response{
				RedirectUrl: tempresultSlug,
				Status:      result,
			}
		}
	}
	b, _ := json.Marshal(outputObj)
	c.Ctx.Output.Body([]byte(string(b)))
	fmt.Println("Out AddTeamSubscription")
}

func (c *StripeAPIController) CancelTeamSubscription() {
	fmt.Println("In CancelTeamSubscription")
	if c.Ctx.Input.Method() == "POST" {
		subscriptionId := c.GetString("subscriptionId")
		teamId := c.GetString("teamId")
		fromPage := c.GetString("fromPage")                   //profile
		tempendCancelStatus := c.GetString("endCancelStatus") //true
		endCancelStatus, _ := strconv.ParseBool(tempendCancelStatus)

		result := models.CancelTeamSubscription(subscriptionId, teamId, endCancelStatus, fromPage)

		c.Ctx.Output.Body([]byte(result))
	}
	fmt.Println("Out CancelTeamSubscription")
}

func (c *StripeAPIController) ReactivateTeamSubscription() {
	fmt.Println("In ReactivateTeamSubscription")
	if c.Ctx.Input.Method() == "POST" {
		subscriptionId := c.GetString("subscriptionId")
		teamId := c.GetString("teamId")

		result := models.ReactivateTeamSubscription(subscriptionId, teamId)

		c.Ctx.Output.Body([]byte(result))
	}
	fmt.Println("Out ReactivateTeamSubscription")
}

func (c *StripeAPIController) CancelTeamTrialSubscription() {
	fmt.Println("In CancelTeamTrialSubscription")
	if c.Ctx.Input.Method() == "POST" {
		subscriptionId := c.GetString("subscriptionId")
		teamId := c.GetString("teamId")
		fromPage := c.GetString("fromPage")
		tempendCancelStatus := c.GetString("endCancelStatus")
		endCancelStatus, _ := strconv.ParseBool(tempendCancelStatus)

		result := models.CancelTeamTrialSubscription(subscriptionId, teamId, endCancelStatus, fromPage)

		c.Ctx.Output.Body([]byte(result))
	}
	fmt.Println("Out CancelTeamTrialSubscription")
}

func (c *StripeAPIController) RequestToConnetStrip() {
	fmt.Println("In RequestToConnetStrip")
	result := "true"
	if c.Ctx.Input.Method() == "POST" {
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)

		currenteventId := c.GetString("currenteventId")
		if currenteventId != "" {
			result = models.CheckAlreadyConnetWithStripe(currenteventId)
			if result == "" {
				emailText := models.GetUserEmailByUserId(user_id)
				if emailText != "" {
					result = models.SendStripeAccountConnetRequestByUserId(user_id, emailText, currenteventId)
				} else {
					result = "false"
				}
			}
		} else {
			result = "false"
		}

		c.Ctx.Output.Body([]byte(result))
	}
	fmt.Println("Out RequestToConnetStrip")
}

func (c *StripeAPIController) EventRegistrationPayment() {
	fmt.Println("In EventRegistrationPayment")
	result := "false"
	tempresultSlug := ""
	if c.Ctx.Input.Method() == "POST" {
		id := c.GetSession("UserId").(int)
		user_id := strconv.Itoa(id)
		eventteampayamount := c.GetString("event_team_payamount")
		transaction_note := c.GetString("event_team_transaction_note")

		eventregid := c.GetString("currentEventRegisterId")
		payeventslug := c.GetString("payeventslug")

		chargetoken := c.GetString("stripeToken")
		chargetoken1 := c.GetString("stripeToken1")

		_, _, remaamountwithformat := models.GetTotalRemaningPaidAmount(eventregid)

		remaamountwithformatRat, eventteampayamountRat := new(big.Rat), new(big.Rat)

		remaamountwithformatRat.SetString(remaamountwithformat)
		eventteampayamountRat.SetString(eventteampayamount)

		if eventteampayamountRat.Cmp(remaamountwithformatRat) == -1 || eventteampayamountRat.Cmp(remaamountwithformatRat) == 0 {
			result = "true"
			paymentlogId := "0"
			result, paymentlogId = models.EventRegistrationCharge(eventregid, eventteampayamount, transaction_note, user_id, chargetoken, chargetoken1)
			tempresultSlug = ""

			if paymentlogId != "0" {
				result1 := ""
				tempFile := ""

				result1, tempFile = GeneratePaymentLogDetailInPdfFormat(paymentlogId, result1, tempFile)

				if result1 == "true" {
					UploadTheEventPaymentInvoiceToS3Bucket(tempFile, paymentlogId)
				}
			}

			if c.GetSession("readyToPaymentStatus") != nil && result == "true" && payeventslug != "" {
				c.DelSession("readyToPaymentStatus")
				c.SetSession("EventRegistrationDone", "true")
				c.SetSession("EventRegistrationPaymentDone", "true")
				tempresultSlug = "/event-view/" + payeventslug
			}
		} else {
			result = "Paid amount exceeding the owed amount."
		}

		var outputObj *add_subscription_response
		outputObj = &add_subscription_response{
			RedirectUrl: tempresultSlug,
			Status:      result,
		}

		c.Ctx.Output.JSON(outputObj, false, false)

	}

	fmt.Println("Out EventRegistrationPayment")
}

func (c *StripeAPIController) GetTeamPaymentDetail() {
	fmt.Println("In GetTeamPaymentDetail")

	if c.Ctx.Input.Method() == "GET" {

		regPayId := c.GetString("regPayId")

		regPaymentDetail := models.GetTeamEventPaymentRegistration(regPayId)
		fkeventId := "0"
		fkteamId := "0"
		eventregId := "0"
		eventslug := ""
		eventcurrency := ""
		eventcurrency1 := ""

		if regPaymentDetail != nil {
			if regPaymentDetail[0]["fk_event_id"] != nil {
				fkeventId = regPaymentDetail[0]["fk_event_id"].(string)
			}
			if regPaymentDetail[0]["fk_team_id"] != nil {
				fkteamId = regPaymentDetail[0]["fk_team_id"].(string)
			}
			if regPaymentDetail[0]["fk_event_reg_id"] != nil {
				eventregId = regPaymentDetail[0]["fk_event_reg_id"].(string)
			}
		}

		regEventDetail := models.GetEventDEtailByEventId(fkeventId)

		if regEventDetail != nil {
			if regEventDetail[0]["slug"] != nil {
				eventslug = regEventDetail[0]["slug"].(string)
			}

			if regEventDetail[0]["currency"] != nil {
				eventcurrency = regEventDetail[0]["currency"].(string)
				eventcurrency1 = regEventDetail[0]["currency"].(string)
				eventcurrency = common.ConvertUnitToCurrencySynbol(eventcurrency)
			}
		}

		remaamount, team_name, remaamountwithformat := models.GetTotalRemaningPaidAmount(eventregId)

		outputObj := &eventpaymentdetail{
			Teamid:               fkteamId,
			Eventslug:            eventslug,
			EventRegisterId:      eventregId,
			CurrencySymbol:       eventcurrency,
			Team_Name:            team_name,
			RemainingAmount:      remaamount,
			RemaamountWithFormat: remaamountwithformat,
			CurrencyISO:          eventcurrency1,
		}

		c.Ctx.Output.JSON(outputObj, false, false)
	}
	fmt.Println("Out GetTeamPaymentDetail")
}

func (c *StripeAPIController) DownloadEventRegistrationInvoice() {

	paymentlogsId := c.Ctx.Input.Param(":paymentlogsId")

	result := ""
	tempFile := ""

	paymentlogDetail := models.GetEventTransactionLogByLogId(paymentlogsId)

	if paymentlogDetail != nil {
		result, tempFile = GeneratePaymentLogDetailInPdfFormat(paymentlogsId, result, tempFile)

		fmt.Println(result)
		fmt.Println(tempFile)
		if result == "true" {
			streamPDFbytes, err := ioutil.ReadFile(tempFile)

			if err != nil {
				fmt.Println(err)
			}
			c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")

			defer os.Remove(tempFile)

			c.Ctx.Output.Body(streamPDFbytes)
		} else {
			c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
		}
	} else {
		c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
	}
}

func GetTeamPaymentDetails(paymentId string, dateCreated string, eventRegDate string, gushouInvoice string, stripeTransactionNumber string, currency_unit string, paymenttotalAmount string, stripeTransactionNumber1 string, invoicetype string, transaction_action string, transaction_note string) (string, string) {

	mode := big.ToNearestEven
	result := "true"
	tempFileName := ""

	paymentIdDetail := models.GetEventRegisterTeamPaymentDetail(paymentId)
	if paymentIdDetail != nil {
		eventId := "0"
		teamId := "0"
		eventregId := "0"
		discount := ""

		feechange := new(big.Rat)
		feechange.SetString("0")

		zerovalue := new(big.Rat)
		zerovalue.SetString("0")

		if paymentIdDetail[0]["fk_event_id"] != nil {
			eventId = paymentIdDetail[0]["fk_event_id"].(string)
		}

		if paymentIdDetail[0]["fk_team_id"] != nil {
			teamId = paymentIdDetail[0]["fk_team_id"].(string)
		}

		if paymentIdDetail[0]["fk_event_reg_id"] != nil {
			eventregId = paymentIdDetail[0]["fk_event_reg_id"].(string)
		}

		if paymentIdDetail[0]["discount"] != nil {
			discount = paymentIdDetail[0]["discount"].(string)
		}

		if paymentIdDetail[0]["changefee"] != nil {
			feechange.SetString(paymentIdDetail[0]["changefee"].(string))
		}

		if eventId != "0" && teamId != "0" && eventregId != "0" {

			teamOrganizerId := "0"

			teamDetail := models.GetTeamDetailByTeamId(teamId)

			eventDetail := models.GetEventDEtailByEventId(eventId)

			eventregDetail := models.GetEventRegisterDetailById(eventregId)

			if teamDetail[0]["fk_captain_id"] != nil {
				teamOrganizerId = teamDetail[0]["fk_captain_id"].(string)
			}

			teamCaptainUserDetail := models.GetUserDetailByUserId(teamOrganizerId)

			if teamDetail != nil && eventDetail != nil && eventregDetail != nil && teamCaptainUserDetail != nil {

				eventName := "-"
				eventPhone := "-"
				eventEmail := "-"
				eventAddress := "-"
				eventAddress1 := "-"
				eventCity := "-"
				eventProvinceState := "-"
				eventCountry := "-"
				eventCurrency := ""

				teamName := "-"
				captainName := "-"
				captainEmail := "-"
				captainPhone := "-"
				captainCountryCode := "-"

				additionalPracticeQuantity := "0"
				participantmembercount := "0"
				pricebased := ""

				captainFistName := "-"
				captainLastName := "-"

				if teamDetail[0]["team_name"] != nil {
					teamName = teamDetail[0]["team_name"].(string)
				}

				if teamCaptainUserDetail[0]["name"] != nil {
					captainFistName = teamCaptainUserDetail[0]["name"].(string)
				}

				if teamCaptainUserDetail[0]["last_name"] != nil {
					captainLastName = teamCaptainUserDetail[0]["last_name"].(string)
				}

				captainName = captainFistName + " " + captainLastName

				if teamCaptainUserDetail[0]["email_address"] != nil {
					captainEmail = teamCaptainUserDetail[0]["email_address"].(string)
				}

				if teamCaptainUserDetail[0]["phone"] != nil {
					captainPhone = teamCaptainUserDetail[0]["phone"].(string)
				}

				if teamCaptainUserDetail[0]["country_dial_code"] != nil {
					captainCountryCode = teamCaptainUserDetail[0]["country_dial_code"].(string)
				}

				if eventDetail[0]["event_name"] != nil {
					eventName = eventDetail[0]["event_name"].(string)
				}

				if eventDetail[0]["phone_number"] != nil {
					eventPhone = eventDetail[0]["phone_number"].(string)
				}

				if eventDetail[0]["email"] != nil {
					eventEmail = eventDetail[0]["email"].(string)
				}

				if eventDetail[0]["venue_name"] != nil {
					eventAddress = eventDetail[0]["venue_name"].(string)
				}

				if eventDetail[0]["venue_street_address"] != nil {
					eventAddress1 = eventDetail[0]["venue_street_address"].(string)
				}

				if eventDetail[0]["location"] != nil {
					eventCity = eventDetail[0]["location"].(string)
				}

				if eventDetail[0]["province"] != nil {
					eventProvinceState = eventDetail[0]["province"].(string)
				}

				if eventDetail[0]["country"] != nil {
					eventCountry = eventDetail[0]["country"].(string)
				}

				if eventDetail[0]["currency"] != nil {
					eventCurrency = eventDetail[0]["currency"].(string)
					eventCurrency = strings.ToUpper(eventCurrency)
					// eventCurrency1 := common.ConvertUnitToCurrencySynbol(eventCurrency)
					// if eventCurrency1 != "" {
					// 	eventCurrency = eventCurrency1
					// }
				}

				if eventregDetail[0]["additional_practice_quantity"] != nil {
					additionalPracticeQuantity = eventregDetail[0]["additional_practice_quantity"].(string)
				}

				if eventregDetail[0]["participant_member_count"] != nil {
					participantmembercount = eventregDetail[0]["participant_member_count"].(string)
				}

				if eventDetail[0]["price_based"] != nil {
					pricebased = eventDetail[0]["price_based"].(string)
				}

				tmpFile, err := ioutil.TempFile(os.TempDir(), "prefix-")
				if err != nil {
					//log.Fatal("Cannot create temporary file", err)
				}

				marginH := 15.0
				pdf := gofpdf.New("P", "mm", "A4", "")
				pdf.SetTopMargin(20)
				pdf.SetFooterFunc(func() {
					pdf.SetY(-15)
					pdf.SetFont("Arial", "I", 8)
					pdf.CellFormat(0, 10, fmt.Sprintf("Page %d/{nb}", pdf.PageNo()),
						"", 0, "C", false, 0, "")
				})
				pdf.SetMargins(marginH, 15, marginH)
				pdf.SetFont("Times", "", 12)
				pdf.SetAutoPageBreak(false, 0)
				tr := pdf.UnicodeTranslatorFromDescriptor("")
				pdf.AddPage()

				pdf.SetFont("Arial", "B", 17)
				pdf.SetY(pdf.GetY() + 10)
				pdf.CellFormat(40, 7, eventName, "0", 0, "L", false, 0, "")

				if eventPhone != "" && eventEmail != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 10)
					pdf.CellFormat(40, 7, eventPhone+" | "+eventEmail, "0", 0, "L", false, 0, "")
				} else if eventPhone != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 10)
					pdf.CellFormat(40, 7, eventPhone, "0", 0, "L", false, 0, "")
				} else if eventEmail != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 10)
					pdf.CellFormat(40, 7, eventEmail, "0", 0, "L", false, 0, "")
				}

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				pdf.CellFormat(40, 7, eventAddress+" "+eventAddress1+", "+eventCity+", "+eventProvinceState+", "+eventCountry, "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "B", 13)
				pdf.SetY(pdf.GetY() + 13)
				pdf.CellFormat(40, 7, "INVOICE TO:", "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				pdf.CellFormat(40, 7, teamName, "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				pdf.CellFormat(40, 7, captainName, "0", 0, "L", false, 0, "")

				if captainEmail != "" && captainCountryCode != "" && captainPhone != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 8)
					pdf.CellFormat(40, 7, captainEmail+" | "+captainCountryCode+" "+captainPhone, "0", 0, "L", false, 0, "")
				} else if captainEmail != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 8)
					pdf.CellFormat(40, 7, captainEmail, "0", 0, "L", false, 0, "")
				} else if captainPhone != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 8)
					pdf.CellFormat(40, 7, captainCountryCode+" "+captainPhone, "0", 0, "L", false, 0, "")
				}

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				pdf.CellFormat(40, 7, "Event Registered Date: "+eventRegDate, "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				if dateCreated != "" {
					pdf.CellFormat(40, 7, "DateCreated: "+dateCreated, "0", 0, "L", false, 0, "")
				} else {
					pdf.CellFormat(40, 7, "DateCreated: N/A", "0", 0, "L", false, 0, "")
				}

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				if gushouInvoice != "" {
					pdf.CellFormat(40, 7, "Gushou Invoice No. : "+gushouInvoice, "0", 0, "L", false, 0, "")

				} else {
					pdf.CellFormat(40, 7, "Gushou Invoice No. : N/A", "0", 0, "L", false, 0, "")

				}

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 8)
				fmt.Println("stripeTransactionNumber-", stripeTransactionNumber)
				if stripeTransactionNumber != "" {
					pdf.CellFormat(40, 7, "Stripe Transaction No. : "+stripeTransactionNumber, "0", 0, "L", false, 0, "")
				} else {
					pdf.CellFormat(40, 7, "Stripe Transaction No. : N/A", "0", 0, "L", false, 0, "")

				}

				pagew, pageh := pdf.GetPageSize()
				fmt.Println(pagew)
				fmt.Println(pageh)

				pdf.AliasNbPages("")

				marginCell := 2.
				_, _, _, mbottom := pdf.GetMargins()

				cols := []float64{60, 40, 40, 40}

				align1 := []string{"LM", "C", "C", "C"}
				align2 := []string{"LM", "C", "LM", "C"}

				pdf.SetFont("Arial", "", 12)
				pdf.SetY(pdf.GetY() + 10)
				header := [4]string{"Description", "Quantity", "Unit Price(" + tr(eventCurrency) + ")", "Cost(" + tr(eventCurrency) + ")"}

				pdf.SetTextColor(224, 224, 224)
				pdf.SetFillColor(64, 64, 64)
				for colJ := 0; colJ < 4; colJ++ {
					pdf.CellFormat(cols[colJ], 10, header[colJ], "1", 0, "CM", true, 0, "")
				}
				pdf.Ln(-1)

				rows := [][]string{}

				gushouAdminFees := new(big.Rat)
				gushouAdminFees.SetString("0")

				perpersonIteamTotal := new(big.Rat)
				perpersonIteamTotal.SetString("0")

				tempRoleTotalCount := new(big.Rat)
				tempRoleTotalCount.SetString("0")

				if pricebased == "PP" {
					rows, perpersonIteamTotal, tempRoleTotalCount = models.GetPerpersonOrderDetailForPdfGeneration(eventId, eventregId)
				}

				if pricebased != "PP" && participantmembercount != "0" {
					tempRoleTotalCount.SetString(participantmembercount)
					f, err := strconv.ParseFloat(participantmembercount, 64)
					fmt.Println(err)
					tempGushouMemberCountPrice := f * float64(constants.GUSHOUFEE)
					gushouAdminFees.SetFloat64(tempGushouMemberCountPrice)
				} else {
					eachGushoueFeePerperson := new(big.Rat)
					eachGushoueFeePerperson.SetInt64(int64(constants.GUSHOUFEE))
					gushouAdminFees.Mul(tempRoleTotalCount, eachGushoueFeePerperson)
				}
				tempgushouAdminFees := new(big.Float).SetPrec(0).SetMode(mode).SetRat(gushouAdminFees)
				gushouRoleTotalCount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(tempRoleTotalCount)

				pdf.SetTextColor(24, 24, 24)
				pdf.SetFillColor(255, 255, 255)
				pdf.SetFont("Arial", "", 10)
				pdf = Rowgenerate(rows, pdf, marginCell, cols, pageh, mbottom, align1)

				rows = [][]string{}

				otherIteamTotal := new(big.Rat)
				otherIteamTotal.SetString("0")

				rows, otherIteamTotal = models.GetOrderOtherIteamDetailForPdfGeneration(eventId, eventregId)

				pdf.SetTextColor(24, 24, 24)
				pdf.SetFillColor(255, 255, 255)
				pdf.SetFont("Arial", "", 10)
				pdf = Rowgenerate(rows, pdf, marginCell, cols, pageh, mbottom, align1)

				rows = [][]string{}

				additionalIteamTotal := new(big.Rat)
				additionalIteamTotal.SetString("0")

				rows, additionalIteamTotal = models.GetOrderAdditionalPracticeIteamDetailForPdfGeneration(additionalPracticeQuantity, eventId)

				pdf.SetTextColor(24, 24, 24)
				pdf.SetFillColor(255, 255, 255)
				pdf.SetFont("Arial", "", 10)
				pdf = Rowgenerate(rows, pdf, marginCell, cols, pageh, mbottom, align1)

				rows = [][]string{}
				for i := 1; i <= 1; i++ {
					tempgushouAdminFees1 := common.ConvertAmountIntoFormat(tempgushouAdminFees.String())
					rows = append(rows, []string{"Gushou Admin Fee ", gushouRoleTotalCount.String(), constants.GUSHOUFEESTRING, tempgushouAdminFees1})
				}
				pdf = Rowgenerate(rows, pdf, marginCell, cols, pageh, mbottom, align1)

				totalOrderAmount := new(big.Rat)
				totalOrderAmount.SetString("0")
				totalOrderAmount.Add(totalOrderAmount, gushouAdminFees)
				totalOrderAmount.Add(totalOrderAmount, perpersonIteamTotal)
				totalOrderAmount.Add(totalOrderAmount, otherIteamTotal)
				totalOrderAmount.Add(totalOrderAmount, additionalIteamTotal)

				amount_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(totalOrderAmount)

				remaamount := models.GetTotalRemaningPaidAmountEventRegPdfGeneration(paymentId)

				if discount != "" {
					remaamount = "0.00"
				}

				rows = [][]string{}
				amount_paid_float1 := common.ConvertAmountIntoFormat(amount_paid_float.String())
				rows = append(rows, []string{"", "", "Total(" + tr(eventCurrency) + ")", amount_paid_float1})
				if feechange.Cmp(zerovalue) == 1 || feechange.Cmp(zerovalue) == -1 {
					feechange_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(feechange)
					feechange_float1 := common.ConvertAmountIntoFormat(feechange_float.String())
					rows = append(rows, []string{"", "", "Fee Change*(" + tr(eventCurrency) + ")", feechange_float1})
				}
				rows = append(rows, []string{"", "", "Balance Owed(" + tr(eventCurrency) + ")", remaamount})

				var eventNetCurrency string
				eventNetfees := 0.00
				eventTransactionfees := 0.00

				//gushouNetfees := 0.00
				//gushouTransactionfees := 0.00

				// if stripeTransactionNumber != "" {
				// 	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
				// 	eventBalanceTransaction, err := balance.GetBalanceTransaction(stripeTransactionNumber, nil)
				// 	if err == nil {
				// 		eventNetCurrency = string(eventBalanceTransaction.Currency)
				// 		eventNetfees = float64(eventBalanceTransaction.Net) / 100
				// 		eventTransactionfees = float64(eventBalanceTransaction.Fee) / 100

				// 	}

				// 	if stripeTransactionNumber1 != "" {
				// 		gushouBalanceTransaction, err := balance.GetBalanceTransaction(stripeTransactionNumber1, nil)
				// 		if err == nil {
				// 			//eventNetCurrency = string(eventBalanceTransaction.Currency)
				// 			gushouNetfees = float64(gushouBalanceTransaction.Net) / 100
				// 			gushouTransactionfees = float64(gushouBalanceTransaction.Fee) / 100

				// 		}
				// 	}
				// } else {
				eventNetCurrency = strings.ToUpper(currency_unit)
				eventNetfees = 0.00
				f, err := strconv.ParseFloat(paymenttotalAmount, 64)
				if err == nil {
					eventNetfees = f
				}
				eventTransactionfees = 0.00
				//}

				eventNetfeesrat := new(big.Rat)
				eventNetfeesrat.SetFloat64(eventNetfees)

				eventTransactionfeesrat := new(big.Rat)
				eventTransactionfeesrat.SetFloat64(eventTransactionfees)

				teampTotalPaid := new(big.Rat)
				teampTotalPaid.SetString("0")

				teampTotalPaid.Add(eventNetfeesrat, eventTransactionfeesrat)

				alreadyPaidAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(teampTotalPaid)
				alreadyPaidAmount1 := common.ConvertAmountIntoFormat(alreadyPaidAmount.String())

				if teampTotalPaid.Cmp(zerovalue) == 1 && transaction_action != "owing" {
					if eventNetCurrency != "" {
						//eventNetCurrency1 := common.ConvertUnitToCurrencySynbol(eventNetCurrency)
						if invoicetype == "withoutlog" {
							rows = append(rows, []string{"", "", "Paid to date(" + tr(eventNetCurrency) + ")", alreadyPaidAmount1})
						} else {
							rows = append(rows, []string{"", "", "Transaction Amount(" + tr(eventNetCurrency) + ")", alreadyPaidAmount1})
						}
					} else {
						if invoicetype == "withoutlog" {
							rows = append(rows, []string{"", "", "Paid to date(" + tr(eventNetCurrency) + ")", alreadyPaidAmount1})
						} else {
							rows = append(rows, []string{"", "", "Transaction Amount ", alreadyPaidAmount1})
						}
					}
				}

				pdf = Rowgenerate(rows, pdf, marginCell, cols, pageh, mbottom, align2)

				if transaction_note != "" {
					pdf.SetFont("Arial", "", 10)
					pdf.SetY(pdf.GetY() + 5)
					//pdf.CellFormat(40, 7, "* Note: "+transaction_note, "0", 0, "L", false, 0, "")
					pdf.MultiCell(0, 5, "* Note: "+transaction_note, "", "L", false)
				}

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 5)
				pdf.CellFormat(40, 7, "Invoice generated by Gushou Inc.", "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 10)
				pdf.CellFormat(40, 7, "Payments made online and through Gushou Inc are non-refundable.", "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 10)
				pdf.CellFormat(40, 7, "Offline payments made directly to Event Organizers are non-refundable unless explicitly stated by Event Organizer.", "0", 0, "L", false, 0, "")

				pdf.SetFont("Arial", "", 10)
				pdf.SetY(pdf.GetY() + 10)
				pdf.CellFormat(40, 7, "Thank you.", "0", 0, "L", false, 0, "")

				err = pdf.OutputFileAndClose(tmpFile.Name())
				fmt.Println(err)
				tempFileName = tmpFile.Name()
				fmt.Println("temp file name--", tempFileName)
			} else {
				result = "false"
			}
		} else {
			result = "false"
		}
	} else {
		result = "false"
	}

	return result, tempFileName
}

func Rowgenerate(rows [][]string, pdf *gofpdf.Fpdf, marginCell float64, cols []float64, pageh float64, mbottom float64, align []string) (f *gofpdf.Fpdf) {

	for _, row := range rows {
		curx, y := pdf.GetXY()
		x := curx

		height := 0.
		_, lineHt := pdf.GetFontSize()

		for i, txt := range row {
			lines := pdf.SplitLines([]byte(txt), cols[i])
			h := float64(len(lines))*lineHt + marginCell*float64(len(lines))
			if h > height {
				height = h
			}
		}
		// add a new page if the height of the row doesn't fit on the page
		if pdf.GetY()+height > pageh-mbottom-20 {
			pdf.AddPage()
			pdf.SetY(pdf.GetY() + 20)
			y = pdf.GetY()
		}
		for i, txt := range row {
			width := cols[i]
			align1 := align[i]
			pdf.Rect(x, y, width, height, "")
			pdf.MultiCell(width, lineHt+marginCell, txt, "", align1, false)
			x += width
			pdf.SetXY(x, y)
		}
		pdf.SetXY(curx, y+height)
	}
	return pdf
}

func (c *StripeAPIController) DownloadOrderInvoice() {

	eventRegPaymentId := c.Ctx.Input.Param(":eventRegPaymentId")
	fmt.Println(eventRegPaymentId)
	result := "true"
	tempFile := ""
	currencyUnit := ""
	total_paid := "0"
	eventRegDate := ""
	getEventId := models.GetEventRegisterTeamPaymentDetail(eventRegPaymentId)
	if getEventId != nil {
		eventId := "0"
		teamId := "0"

		if getEventId[0]["fk_event_id"] != nil {
			eventId = getEventId[0]["fk_event_id"].(string)
		}
		if getEventId[0]["total_paid"] != nil {
			total_paid = getEventId[0]["total_paid"].(string)
		}
		if getEventId[0]["fk_team_id"] != nil {
			teamId = getEventId[0]["fk_team_id"].(string)
		}

		if eventId != "0" {
			eventDetail := models.GetEventDEtailByEventId(eventId)

			if eventDetail != nil {
				if eventDetail[0]["currency"] != nil {
					currencyUnit = eventDetail[0]["currency"].(string)
					//currencyUnit = common.ConvertUnitToCurrencySynbol(currencyUnit)

				}
			}

			geteventRegDate := models.GetEventCreatedDateByEventId(eventId, teamId)
			fmt.Println(geteventRegDate)
			if geteventRegDate != nil {

				if geteventRegDate[0]["registerdate"] != nil {
					eventRegDate = geteventRegDate[0]["registerdate"].(string)
				}
			}

		}
	}
	fmt.Println("currencyUnit-", currencyUnit)
	fmt.Println("total_paid-", total_paid)
	result, tempFile = GetTeamPaymentDetails(eventRegPaymentId, "", eventRegDate, "", "", currencyUnit, total_paid, "", "withoutlog", "", "")

	fmt.Println(result)
	fmt.Println(tempFile)
	if result == "true" {
		streamPDFbytes, err := ioutil.ReadFile(tempFile)

		if err != nil {
			fmt.Println(err)
		}
		c.Ctx.Output.Header("Content-type", "application/pdf; charset=utf-8")

		defer os.Remove(tempFile)

		c.Ctx.Output.Body(streamPDFbytes)
	} else {
		c.Ctx.Output.JSON("Error During Pdf Generartion", false, false)
	}

}

func (c *StripeAPIController) ManualPayoutMethod() {

	fmt.Println("ManualPayoutMethod In")

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	params := &stripe.PayoutParams{
		Amount:   stripe.Int64(100),
		Currency: stripe.String("usd"),
	}
	p, err := payout.New(params)

	fmt.Println(p)
	fmt.Println(err)

	fmt.Println("ManualPayoutMethod Out")
}

func GeneratePaymentLogDetailInPdfFormat(paymentlogsId string, result string, tempFile string) (string, string) {

	dateCreated := "-"
	gushouInvoice := "-"
	stripeTransactionNumber := "-"
	stripeTransactionNumber1 := "-"
	paymenttotalAmount := "0"
	currency_unit := ""
	eventRegDate := ""
	eventId := ""
	teamId := ""
	transaction_action := ""
	transaction_note := ""
	paymentlogDetail := models.GetEventTransactionLogByLogId(paymentlogsId)

	if paymentlogDetail != nil {
		paymentId := "0"

		if paymentlogDetail[0]["fk_event_reg_pay"] != nil {
			paymentId = paymentlogDetail[0]["fk_event_reg_pay"].(string)
		}

		if paymentlogDetail[0]["createddate"] != nil {
			dateCreated = paymentlogDetail[0]["createddate"].(string)
		}

		if paymentlogDetail[0]["id"] != nil {
			gushouInvoice = paymentlogDetail[0]["id"].(string)
		}

		if paymentlogDetail[0]["balance_trasaction_id"] != nil {
			stripeTransactionNumber = paymentlogDetail[0]["balance_trasaction_id"].(string)
		}

		if paymentlogDetail[0]["balance_trasaction_id1"] != nil {
			stripeTransactionNumber1 = paymentlogDetail[0]["balance_trasaction_id1"].(string)
		}

		if paymentlogDetail[0]["amount"] != nil {
			paymenttotalAmount = paymentlogDetail[0]["amount"].(string)
		}

		if paymentlogDetail[0]["currency_unit"] != nil {
			currency_unit = paymentlogDetail[0]["currency_unit"].(string)
		}

		if paymentlogDetail[0]["transaction_action"] != nil {
			transaction_action = paymentlogDetail[0]["transaction_action"].(string)
		}

		if paymentlogDetail[0]["transaction_note"] != nil {
			transaction_note = paymentlogDetail[0]["transaction_note"].(string)
		}

		if transaction_note == "" {
			transaction_note = models.GetLastTransactionHistoryByRegPayId(paymentId)
		}

		getEventId := models.GetEventRegisterTeamPaymentDetail(paymentId)

		if getEventId != nil {
			if getEventId[0]["fk_event_id"] != nil {
				eventId = getEventId[0]["fk_event_id"].(string)
			}
			if getEventId[0]["fk_team_id"] != nil {
				teamId = getEventId[0]["fk_team_id"].(string)
			}
		}

		geteventRegDate := models.GetEventCreatedDateByEventId(eventId, teamId)
		if geteventRegDate != nil {

			if geteventRegDate[0]["registerdate"] != nil {
				eventRegDate = geteventRegDate[0]["registerdate"].(string)
			}
		}

		result, tempFile = GetTeamPaymentDetails(paymentId, dateCreated, eventRegDate, gushouInvoice, stripeTransactionNumber, currency_unit, paymenttotalAmount, stripeTransactionNumber1, "withlog", transaction_action, transaction_note)
	}
	return result, tempFile
}
