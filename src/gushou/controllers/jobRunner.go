package controllers

import (
	"fmt"
	"gushou/models"

	"github.com/astaxie/beego"
)

// Waiver Automatically Send to teampMembers (Job runner)
type WaiverSendOrResend struct {
	waiverId string
	team_id  string
	user_id  string
}

func (e WaiverSendOrResend) Run() {
	e.SendTeamWaiver_JobRunner(e.waiverId, e.team_id, e.user_id)
}

type WaiverSendOrResendForTeamFromEvent struct {
	waiverId string
	team_id  string
	user_id  string
}

func (e WaiverSendOrResendForTeamFromEvent) Run() {
	e.SendTeamEventWaiver_JobRunner(e.waiverId, e.team_id, e.user_id)
}

type WaiverSendOrResendForIndividualMember struct {
	user_id string
	team_id string
}

func (e WaiverSendOrResendForIndividualMember) Run() {
	e.SendIndividualTeamWaiver_JobRunner(e.user_id, e.team_id)
}

//end

type JobRunnerController struct {
	beego.Controller
}

func (e WaiverSendOrResend) SendTeamWaiver_JobRunner(waiverId string, teamid string, userid string) {
	fmt.Println("In SendTeamWaiver_JobRunner")
	activeTeamMemberId := models.GetActiveTeamMemberForWaiver1(teamid)
	models.SendTeamWaiver(activeTeamMemberId, waiverId, teamid, userid)
	fmt.Println("Out SendTeamWaiver_JobRunner")
}

func (e WaiverSendOrResendForIndividualMember) SendIndividualTeamWaiver_JobRunner(userid string, teamid string) {
	fmt.Println("In SendIndividualTeamWaiver_JobRunner")
	models.SendIndividualTeamWaiver_To_IndividualMember(teamid, userid)
	fmt.Println("Out SendIndividualTeamWaiver_JobRunner")
}
func (e WaiverSendOrResendForTeamFromEvent) SendTeamEventWaiver_JobRunner(waiverId string, teamid string, userid string) {
	fmt.Println("In SendTeamEventWaiver_JobRunner")
	activeTeamMemberId := models.GetActiveTeamMemberForWaiver1(teamid)
	models.SendTeamWaiverEve(activeTeamMemberId, waiverId, teamid, userid)
	fmt.Println("Out SendTeamEventWaiver_JobRunner")
}
