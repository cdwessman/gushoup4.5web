package models

import (
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"math/big"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/balance"
	"github.com/stripe/stripe-go/card"
	"github.com/stripe/stripe-go/charge"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/sub"
	_ "golang.org/x/crypto/bcrypt"
)

func GetStripeCustomerDetails(userID string) (*stripe.Customer, error) {

	userDetails := GetUserDetail(userID)

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	result, err := customer.Get(userDetails[0]["stripe_cust_id"].(string), nil)

	if err != nil {
		return nil, err
	}
	return result, nil
}

func CreateStripeCustomer(userID string) {
	userDetails := GetUserDetails(userID)

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	if userDetails["user"]["stripe_cust_id"] == nil {
		StripeAccountCreation(userDetails, userID)
	}
}

func StripeAccountCreation(userDetails map[string]orm.Params, userID string) string {

	resultStripeId := ""

	customerParams := &stripe.CustomerParams{
		Email:       stripe.String(userDetails["user"]["email_address"].(string)),
		Description: stripe.String("Gushou Customer with ID" + userDetails["user"]["id"].(string)),
	}

	result, err := customer.New(customerParams)

	if err == nil {
		o := orm.NewOrm()
		var maps []orm.Params
		_, err := o.Raw("UPDATE user_profile set stripe_cust_id = ? WHERE fk_user_id = ?", result.ID, userID).Values(&maps)
		if err == nil {
			resultStripeId = result.ID
		}
	}

	return resultStripeId
}

func UpdateStripeCustomerDetails(userID string) (*stripe.Customer, error) {

	userDetails := GetUserDetail(userID)

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	result, err := customer.Update(
		userDetails[0]["stripe_cust_id"].(string),
		&stripe.CustomerParams{
			Description: stripe.String("Sample update of description"),
		},
	)

	if err != nil {
		return nil, err
	}
	return result, nil
}

func CreateCard(userID string, cardToken string) (string, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	resultStatus := "false"
	cardId := ""
	userDetails := GetUserDetails(userID)
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	var teampresult = stripe.Card{}
	result := &teampresult
	var err error

	if userDetails["user"]["stripe_cust_id"] == nil {
		responseStripId := StripeAccountCreation(userDetails, userID)
		if responseStripId != "" {
			result, err = card.New(
				&stripe.CardParams{
					Customer: stripe.String(responseStripId),
					Token:    stripe.String(cardToken),
				},
			)
		}
	} else {
		result, err = card.New(
			&stripe.CardParams{
				Customer: stripe.String(userDetails["user"]["stripe_cust_id"].(string)),
				Token:    stripe.String(cardToken),
			},
		)
	}

	if result != nil && err == nil {
		_, err := o.Raw("INSERT INTO card_detail (card_id, brand, default_source, fk_user_id, last4, exp_month, exp_year, created_at, updated_at ) VALUES (?,?,?,?,?,?,?,?,?)", result.ID, result.Brand, false, userID, result.Last4, result.ExpMonth, result.ExpYear, time.Now(), time.Now()).Values(&maps)
		if err == nil {
			cardId = result.ID
			resultStatus = "true"
			CheckDefaultCardDetail(userID, result.ID)
		}
	} else {

		b, _ := json.Marshal(err)
		resultStatus = string(b)
	}
	return resultStatus, cardId
}

func UpdateCard(userID string, cardID string) (*stripe.Card, error) {

	userDetails := GetUserDetail(userID)

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	result, err := card.Update(
		cardID,
		&stripe.CardParams{
			Customer: stripe.String(userDetails[0]["stripe_cust_id"].(string)),
			Name:     stripe.String("Test Card for Test"),
		},
	)

	if err != nil {
		return nil, err
	}
	return result, nil
}

func DeleteCard(userID string, cardID string) (string, string) {
	deleteStatus := "false"
	cardId := ""
	userDetails := GetUserDetail(userID)

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	_, err := card.Del(
		cardID,
		&stripe.CardParams{
			Customer: stripe.String(userDetails[0]["stripe_cust_id"].(string)),
		},
	)

	if err == nil {
		o := orm.NewOrm()
		var maps []orm.Params
		_, err := o.Raw("delete from card_detail where card_id =? ", cardID).Values(&maps)
		fmt.Println(err)
		if err == nil {
			if userDetails[0]["default_card_id"] != nil {
				if userDetails[0]["default_card_id"].(string) == cardID {
					_, err := o.Raw("UPDATE user_profile set default_card_id = ? WHERE fk_user_id = ?", nil, userID).Values(&maps)
					if err == nil {
						deleteStatus = "true"
					}
				}
			}

			deleteStatus = "true"
		}
	}
	return deleteStatus, cardId
}

func CheckDefaultCardDetail(userId string, cardId string) {
	result, _ := GetStripeCustomerDetails(userId)
	if result != nil {
		if result.DefaultSource.ID == cardId {
			o := orm.NewOrm()
			var maps []orm.Params
			_, err := o.Raw("UPDATE user_profile set default_card_id = ? WHERE fk_user_id = ?", cardId, userId).Values(&maps)
			fmt.Println(err)
			_, err = o.Raw("UPDATE card_detail set default_source = ?,updated_at =? WHERE card_id = ?", true, time.Now(), cardId).Values(&maps)
			fmt.Println(err)
		}
	}
}

func GetCardDetails(user_id_string string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select * from card_detail where fk_user_id =? order by created_at desc ", user_id_string).Values(&maps)
	fmt.Println(err)
	return maps
}

func SetDefaultCard(userID string, cardID string) (string, string) {
	updateStatus := "false"
	userDetails := GetUserDetail(userID)
	cardId := ""
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	_, err := customer.Update(
		userDetails[0]["stripe_cust_id"].(string),
		&stripe.CustomerParams{
			DefaultSource: stripe.String(cardID),
		},
	)

	if err == nil {
		o := orm.NewOrm()
		var maps []orm.Params
		if userDetails[0]["default_card_id"] != nil {
			cardId = userDetails[0]["default_card_id"].(string)
			_, err = o.Raw("UPDATE card_detail set default_source = ?,updated_at =? WHERE card_id = ?", false, time.Now(), userDetails[0]["default_card_id"].(string)).Values(&maps)
			fmt.Println(err)
		}
		if err == nil || userDetails[0]["default_card_id"] == nil {
			cardId = cardID
			_, err := o.Raw("UPDATE card_detail set default_source = ?, updated_at =? WHERE card_id = ?", true, time.Now(), cardID).Values(&maps)
			fmt.Println(err)
			if err == nil {
				{
					_, err := o.Raw("UPDATE user_profile set default_card_id = ? WHERE fk_user_id = ?", cardID, userID).Values(&maps)
					if err == nil {
						updateStatus = "true"
					}
				}
			}
		}
	}
	return updateStatus, cardId
}

func GetSubscription_Plans() []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select price ::money::numeric::float8 as price_format ,* from subscription_plans").Values(&maps)
	fmt.Println(err)
	return maps
}

func GetSubscription_Card_Detail_By_UserId(userId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select * from card_detail where fk_user_id=? order by default_source desc", userId).Values(&maps)
	fmt.Println(err)
	return maps
}

func AddTeamSubscription(subscriptionplan string, subscriptioncard string, subscriptionteamid string, subscriptionuserid string) string {
	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	userDetails := GetUserDetail(subscriptionuserid)

	resultStatus, err := sub.New(&stripe.SubscriptionParams{
		Customer: stripe.String(userDetails[0]["stripe_cust_id"].(string)),
		Items: []*stripe.SubscriptionItemsParams{
			{
				Plan: stripe.String(subscriptionplan),
			},
		},
	})

	if err == nil && resultStatus.Status == "active" {
		periodStartDate := time.Unix(resultStatus.CurrentPeriodStart, 0)
		periodEndDate := time.Unix(resultStatus.CurrentPeriodEnd, 0)
		nextMonthDate := common.AddMonth(periodStartDate.Format(constants.DATE_TIME_ZONE_FORMAT), 1)
		tempName := ""
		tempLastName := ""
		if userDetails[0]["name"] != nil {
			tempName = userDetails[0]["name"].(string)
		}

		if userDetails[0]["display_name"].(string) == "1" && userDetails[0]["last_name"] != nil {
			tempLastName = userDetails[0]["last_name"].(string)
		}
		tempNameData := tempName + " " + tempLastName
		_, err = o.Raw("INSERT INTO money_subscription (fk_user_id, fk_team_id, subscription_id, subscription_plans_id, status, current_start_date, current_end_date, sms_pack_enddate, paid_by, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)", subscriptionuserid, subscriptionteamid, resultStatus.ID, resultStatus.Plan.ID, resultStatus.Status, periodStartDate, periodEndDate, nextMonthDate, tempNameData, time.Now(), time.Now()).Values(&maps)
		fmt.Println(err)
		if err == nil {
			_, err = o.Raw("UPDATE team SET free_trial =?, subscription_on_off=?  WHERE id = ? ", false, true, subscriptionteamid).Values(&maps)
			if err == nil {
				result = "true"
			}
		}
	} else {
		b, _ := json.Marshal(err)
		var erroPram *stripe.Error
		json.Unmarshal(b, &erroPram)
		result = erroPram.Msg
	}
	return result
}

func GetMoneySubscriptionDetailByTeamId(teamId string, userId string) bool {
	o := orm.NewOrm()
	var maps []orm.Params

	result := false
	num, err := o.Raw("select * from money_subscription where fk_team_id=? and fk_user_id=? and subscription_active_inactive= true and (status = 'active') order by created_at desc ", teamId, userId).Values(&maps)
	fmt.Println(err)
	if num > 0 {

		str := maps[0]["current_end_date"].(string)
		current_end_data, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
		if err == nil {
			current_data := time.Now().UTC()
			if current_end_data.After(current_data) || current_end_data.Equal(current_data) {
				result = true
			}

		}
	}
	return result
}

func GetUserSubscriptionDetail(userId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select ((te.trial_period_end_date) - INTERVAL '21 DAY') as trial_period_start_date,to_char(((te.trial_period_end_date) - INTERVAL '21 DAYS'),'Month DD, YYYY' ) as format_trial_period_start_date,to_char(te.trial_period_end_date, 'Month DD, YYYY') as format_trial_period_end_date,te.team_name,te.id as team_id, ms.subscription_id, to_char(ms.current_start_date, 'DD Mon YYYY') as current_start_date, to_char(ms.current_end_date, 'DD Mon YYYY') as current_end_data, sp.name,sp.price, ms.subscription_end_date, te.free_trial, te.subscription_on_off, te.trial_period_end_date as trial_period_end_date , ms.subscription_active_inactive, to_char(te.basic_plan_start_date, 'Month DD, YYYY') as format_basic_plan_start_date, ms.status, ms.paid_by,ms.fk_user_id as subscription_userid, ms.canceled_date ,te.slug as team_slug  from team te  left join money_subscription ms on te.id = ms.fk_team_id left join subscription_plans sp on sp.plan_id = ms.subscription_plans_id   where te.fk_captain_id =? order by te.created_at desc ", userId).Values(&maps)
	fmt.Println(err)
	return maps
}

func CancelTeamSubscription(subscriptionid string, subscriptionteamid string, endCancelStatus bool, fromPage string) string {
	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	// _, err := sub.Cancel(
	// 	subscriptionid,
	// 	&stripe.SubscriptionCancelParams{
	// 		AtPeriodEnd: stripe.Bool(endCancelStatus),
	// 	},
	// )
	params := &stripe.SubscriptionParams{
		CancelAtPeriodEnd: stripe.Bool(endCancelStatus),
	}
	_, err := sub.Update(subscriptionid, params)

	if err == nil {
		s, err := sub.Get(subscriptionid, nil)

		if err == nil {
			if s.CanceledAt > 0 && s.CurrentPeriodEnd > 0 {
				canceled_date := time.Unix(s.CanceledAt, 0)
				subscription_end_date := time.Unix(s.CurrentPeriodEnd, 0)

				subscription_active_inactive := false
				if fromPage == "profile" {
					subscription_active_inactive = true
				}
				_, err = o.Raw("UPDATE money_subscription SET canceled_date=? ,subscription_end_date=?,updated_at=?,subscription_active_inactive=? WHERE subscription_id = ? ", canceled_date, subscription_end_date, time.Now(), subscription_active_inactive, subscriptionid).Values(&maps)
				fmt.Println(err)
				if err == nil {
					result = "true"
				}
			}
		}
	}

	return result
}

func SetContinuOnFreeTrial(subscriptionteamid string) {

	var maps []orm.Params
	o := orm.NewOrm()
	_, err := o.Raw("UPDATE team SET free_trial =?, subscription_on_off=?, popup_notification_on_off='Not Show', basic_plan_start_date=?  WHERE id = ? ", false, false, time.Now(), subscriptionteamid).Values(&maps)
	fmt.Println(err)

}

func ReactivateTeamSubscription(subscriptionid string, subscriptionteamid string) string {
	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	num, err := o.Raw("select * from  money_subscription WHERE subscription_id = ? and fk_team_id =? and subscription_active_inactive =? ", subscriptionid, subscriptionteamid, true).Values(&maps)
	if err == nil && num > 0 {
		subscriptionPlanId := maps[0]["subscription_plans_id"].(string)

		subscription, err := sub.Get(subscriptionid, nil)
		if err == nil {
			params := &stripe.SubscriptionParams{
				Items: []*stripe.SubscriptionItemsParams{
					{
						ID:   stripe.String(subscription.Items.Data[0].ID),
						Plan: stripe.String(subscriptionPlanId),
					},
				},
			}

			subscription, err = sub.Update(subscriptionid, params)
			if err == nil {
				_, err = o.Raw("UPDATE money_subscription SET canceled_date=? ,subscription_end_date=?, subscription_active_inactive =?, updated_at =? WHERE subscription_id = ? ", nil, nil, true, time.Now(), subscriptionid).Values(&maps)
				fmt.Println(err)
				if err == nil {
					result = "true"
				}
			}
		}
	}
	return result
}

func RemoveCaptainSubscription(captainUserId string, teamId string, loginUserId string) (string, string, string) {

	o := orm.NewOrm()
	var maps []orm.Params
	statusResult := "notfalse"
	endDatePeriodTime := ""
	teamPaidByName := ""
	num, err := o.Raw("select * from  money_subscription WHERE fk_user_id =?  and fk_team_id =? and subscription_active_inactive =true and (status='active' or status='trialing') ", captainUserId, teamId).Values(&maps)
	if err == nil && num > 0 {
		if maps[0]["paid_by"] != nil {
			teamPaidByName = maps[0]["paid_by"].(string)

		}
		tempStatus := true
		if maps[0]["status"].(string) == "trialing" {
			tempStatus = false
		}
		statusResult = CancelTeamSubscription(maps[0]["subscription_id"].(string), teamId, tempStatus, "TeamPage")
		if statusResult == "true" {
			layout := "2006-01-02T15:04:05Z07:00"
			endSubscriptiobDate := maps[0]["current_end_date"].(string)
			then, err := time.Parse(layout, endSubscriptiobDate)
			if err == nil {
				futureDateTime := then.UTC()
				currentTime := time.Now().UTC()

				diff := futureDateTime.Sub(currentTime)
				tempnoofDays := int(diff.Hours() / 24)
				if tempnoofDays > 0 {
					endDatePeriodTime = maps[0]["current_end_date"].(string)
				}

			}
		}
	}

	return statusResult, endDatePeriodTime, teamPaidByName
}

func AddSubscriptionWithTrialPeriod(endDatePeriodTime string, subscriptionplan string, subscriptioncard string, subscriptionteamid string, subscriptionuserid string, PaidByName string) string {
	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY

	userDetails := GetUserDetail(subscriptionuserid)
	layout := "2006-01-02T15:04:05Z07:00"
	then, err := time.Parse(layout, endDatePeriodTime)
	if err == nil {
		unixTimeStamp := then.Unix()
		items := []*stripe.SubscriptionItemsParams{
			{Plan: stripe.String(subscriptionplan)},
		}
		params := &stripe.SubscriptionParams{
			Customer: stripe.String(userDetails[0]["stripe_cust_id"].(string)),
			Items:    items,
			TrialEnd: stripe.Int64(unixTimeStamp),
		}
		resultStatus, err := sub.New(params)
		if err == nil && resultStatus.Status == "trialing" {
			periodStartDate := time.Unix(resultStatus.CurrentPeriodStart, 0)
			periodEndDate := time.Unix(resultStatus.CurrentPeriodEnd, 0)
			num, err := o.Raw("select * from  money_subscription WHERE fk_team_id =? and status='trialing' ", subscriptionteamid).Values(&maps)
			if err == nil && num > 0 {
				_, err = o.Raw("UPDATE money_subscription SET fk_user_id =?, fk_team_id=?, subscription_id=?, subscription_plans_id=?, status =?, current_start_date=? ,current_end_date=?, paid_by=?, subscription_active_inactive=? , updated_at =? WHERE id = ? ", subscriptionuserid, subscriptionteamid, resultStatus.ID, resultStatus.Plan.ID, resultStatus.Status, periodStartDate, periodEndDate, PaidByName, true, time.Now(), maps[0]["id"].(string)).Values(&maps)

				fmt.Println(err)
				if err == nil {
					_, err = o.Raw("UPDATE team SET free_trial =?, subscription_on_off=? , popup_notification_on_off='true' WHERE id = ? ", false, true, subscriptionteamid).Values(&maps)
					if err == nil {
						result = "true"
					}
				}
			} else {
				_, err = o.Raw("INSERT INTO money_subscription (fk_user_id, fk_team_id, subscription_id, subscription_plans_id, status, current_start_date, current_end_date,paid_by,subscription_active_inactive, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)", subscriptionuserid, subscriptionteamid, resultStatus.ID, resultStatus.Plan.ID, resultStatus.Status, periodStartDate, periodEndDate, PaidByName, true, time.Now(), time.Now()).Values(&maps)
				fmt.Println(err)
				if err == nil {
					_, err = o.Raw("UPDATE team SET free_trial =?, subscription_on_off=? , popup_notification_on_off='true' WHERE id = ? ", false, true, subscriptionteamid).Values(&maps)
					if err == nil {
						result = "true"
					}
				}
			}
		} else {
			b, _ := json.Marshal(err)
			var erroPram *stripe.Error
			json.Unmarshal(b, &erroPram)
			result = erroPram.Msg
		}
	}
	return result
}

func TeamSubscriptionSmsPlan(subscriptioncard string, subscriptionteamid string, subscriptionuserid string) string {
	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	userDetails := GetUserDetails(subscriptionuserid)

	chargeParams := &stripe.ChargeParams{
		Amount:              stripe.Int64(constants.SMS_PACKAGE_AMOUNT),
		Currency:            stripe.String("usd"),
		Description:         stripe.String("SMS Package for $15 USD"),
		Customer:            stripe.String(userDetails["user"]["stripe_cust_id"].(string)),
		StatementDescriptor: stripe.String("Gushou - SMS Package"),
		ReceiptEmail:        stripe.String(userDetails["user"]["email_address"].(string)),
	}
	//chargeParams.Customer("tok_amex") // obtained with Stripe.js
	ch, err := charge.New(chargeParams)

	if err == nil && ch.Paid {
		num, err := o.Raw("select * from sms_package where fk_team_id=?", subscriptionteamid).Values(&maps)
		futureDate := common.GetFeatureDateFromNumberOfdays(365)
		if num > 0 {
			tempCount := maps[0]["sms_limits"].(string)
			tempSmsCount, _ := strconv.Atoi(tempCount)
			_, err = o.Raw("UPDATE sms_package SET sms_limits =?, expiry_date=?, updated_at=?  WHERE id = ? ", tempSmsCount+1000, futureDate, time.Now(), maps[0]["id"].(string)).Values(&maps1)
		} else {
			_, err = o.Raw("INSERT INTO sms_package (fk_team_id, sms_limits, expiry_date, created_at, updated_at) VALUES (?,?,?,?,?) RETURNING id", subscriptionteamid, 1000, futureDate, time.Now(), time.Now()).Values(&maps)
			fmt.Println(err)
		}

		if err == nil && maps[0]["id"] != nil {
			_, err = o.Raw("INSERT INTO sms_subscription_detail (fk_sms_package_id, user_id, charge_id, created_at, updated_at) VALUES (?,?,?,?,?) RETURNING id", maps[0]["id"].(string), subscriptionuserid, ch.ID, time.Now(), time.Now()).Values(&maps)
			fmt.Println(err)
			result = "true"
		}
	} else {
		b, _ := json.Marshal(err)
		var erroPram *stripe.Error
		json.Unmarshal(b, &erroPram)
		result = erroPram.Msg
	}

	return result
}

func CancelOldCaptainSubscription(team_id string) string {
	result := "true"
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	num, err := o.Raw("select * FROM team WHERE id = ? ", team_id).Values(&maps)
	fmt.Println(err)
	if num > 0 && err == nil {
		num, err := o.Raw("select * from  money_subscription WHERE fk_user_id =?  and fk_team_id =? and subscription_active_inactive =true and (status='active' or status='trialing') ", maps[0]["fk_captain_id"].(string), team_id).Values(&maps1)
		if err == nil && num > 0 {
			tempStatus := true
			if maps1[0]["status"].(string) == "trialing" {
				tempStatus = false
			}
			result = CancelTeamSubscription(maps1[0]["subscription_id"].(string), team_id, tempStatus, "TeamPage")

			if result == "true" {
				_, err = o.Raw("UPDATE team SET change_captain_status_by_admin = ? where id =?", true, team_id).Values(&maps)
				if err != nil {
					result = "false"
				}
			}
		} else if err != nil {
			result = "false"
		}

	} else {
		result = "false"
	}

	return result
}

func Check_The_Team_SubscriptionValid_Time(teamId string) (string, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	dayDiffrence := ""
	plan_end_date := ""

	num, err := o.Raw("select DATE_PART('day', current_end_date - now() ) as dayDiffrence, to_char(current_end_date, 'MON-DD-YY') as format_plan_end_date from  money_subscription WHERE fk_team_id =? and status='active' order by id desc LIMIT 1", teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		itemp, _ := strconv.Atoi(maps[0]["daydiffrence"].(string))
		if itemp > 0 {
			dayDiffrence = maps[0]["daydiffrence"].(string)
		}
		plan_end_date = maps[0]["format_plan_end_date"].(string)
	}

	return dayDiffrence, plan_end_date
}

func CancelTeamTrialSubscription(subscriptionid string, subscriptionteamid string, endCancelStatus bool, fromPage string) string {

	result := "true"
	subscription, err := sub.Get(subscriptionid, nil)
	if err == nil {
		if subscription.Status == "trialing" {
			result = CancelTeamSubscription(subscriptionid, subscriptionteamid, endCancelStatus, fromPage)
		}
	} else {
		result = "false"
	}

	return result
}

func Check_The_SmsCreditStartingDate(teamId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	sms_nextcreditDate := ""

	num, err := o.Raw("select to_char((sms_pack_enddate + interval '1 day') , 'MON DD, YYYY') as format_plan_end_date from  money_subscription WHERE fk_team_id =? and status='active' and subscription_active_inactive= true and canceled_date isnull  order by id desc LIMIT 1", teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		if maps[0]["format_plan_end_date"] != nil {
			sms_nextcreditDate = maps[0]["format_plan_end_date"].(string)
		}
	}

	return sms_nextcreditDate
}

func SendStripeAccountConnetRequestByUserId(userid string, emailText string, eventId string) string {

	result := "true"

	o := orm.NewOrm()
	var values []orm.Params

	userEmail := GetUserEmailByUserId(userid)
	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	//eventname := GetEventNameDetailByEventId(eventId)
	username := GetUserNameDataByUserId(userid)
	token := generateToken()
	token = token + "_" + userid
	sitehttp := "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" + constants.STRIPE_CONNET_CLIENT_KEY + "&scope=read_write&state=" + token

	testUrl := "<p> Please, click below to set-up your bank information on Stripe so you can start collecting event fees immediately.</p>"
	stripeConnectBtn := "<a href='" + sitehttp + "'><img src='" + site_url + "static/images/stripe-connect-button.png' alt=Connect To Stripe style=height: 50px;width: 200px;></a>"
	testUrl1 := "<p>If you're not the treasurer or the person in charge of banking, forward this email to the correct person and have them connect using the button above.</p><p>Thank you,</p>"
	testUrl2 := "<span style='display: block;'>Chrissy Wessman,</span><span style='display: block;'>Co-Founder, Gushou</span><span style='display: block;'><a href='mailto:dragonboat@gogushou.com' target='_blank'>dragonbboat@gogushou.com</a> | <a href='mailto:gogushou.com' target='_blank'>gogushou.com</a></span>"

	unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
	html_email_message := "<html><p>Hi " + username + ",</p>" + testUrl + "<br>" + stripeConnectBtn + testUrl1 + testUrl2 + unsubscribe_link + "</html>"

	_, errorDes := common.SendEmail(userEmail, "Gushou - Set Up Banking Information on Stripe", html_email_message)
	if errorDes != nil {
		result = "false"
	} else {
		num, _ := o.Raw("SELECT * FROM user_stripe_verification_tokens WHERE fk_event_id = ? ", eventId).Values(&values)
		if num > 0 {
			_, err := o.Raw("UPDATE user_stripe_verification_tokens SET token = ?, lastupdate = ? WHERE fk_event_id = ?", token, time.Now(), eventId).Values(&values)
			if err != nil {
				result = "false"
			}
		} else {
			_, err := o.Raw("INSERT INTO user_stripe_verification_tokens (fk_event_id, address, token, lastupdate) VALUES (?,?,?,?) ", eventId, emailText, token, time.Now()).Values(&values)
			if err != nil {
				result = "false"
			}
		}
	}
	return result
}

func EventRegistrationCharge(eventregid string, eventteampayamount string, transaction_note string, userid string, chargetoken string, chargetoken1 string) (string, string) {

	result := "true"
	paymentlogId := "0"
	o := orm.NewOrm()
	var maps []orm.Params

	intial100Value := new(big.Rat)
	intial100Value.SetString("100")

	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	userDetails := GetUserDetails(userid)
	fmt.Println(userid)
	num1, _ := o.Raw("SELECT * FROM event_register_payments WHERE fk_event_reg_id = ? ", eventregid).Values(&maps)
	mode := big.ToNearestEven
	eventRegisterDetail := GetEventRegisterDetailByEveRegisterId(eventregid)

	if num1 > 0 && eventRegisterDetail != nil {

		registerTeamId := "0"
		registerEventId := "0"
		gushouBalanceTransactionId := ""
		if eventRegisterDetail[0]["fk_team_id"] != nil {
			registerTeamId = eventRegisterDetail[0]["fk_team_id"].(string)
		}

		if eventRegisterDetail[0]["fk_event_id"] != nil {
			registerEventId = eventRegisterDetail[0]["fk_event_id"].(string)
		}

		if registerTeamId != "0" && registerEventId != "0" {

			eventdetail := GetEvent_By_EventId(registerEventId)

			tempCurrency := ""
			eventtname := ""
			if eventdetail != nil {

				if eventdetail[0]["currency"] != nil {
					tempCurrency = strings.ToLower(eventdetail[0]["currency"].(string))
				}

				if eventdetail[0]["event_name"] != nil {
					eventtname = eventdetail[0]["event_name"].(string)
					if len(eventtname) > 10 {
						runes := []rune(eventtname)
						eventtname = string(runes[0:10])
						eventtname = eventtname + ".."
					}

				}

				stripeConnectUserId := GetEventOrganizerConnectedAccountDetail(eventdetail[0]["id"].(string))

				if tempCurrency != "" && stripeConnectUserId != "" {

					total_gushou_fee := new(big.Rat)
					total_gushou_fee.SetString("0")

					total_gushou_fee_recevied := new(big.Rat)
					total_gushou_fee_recevied.SetString("0")

					intial_value := new(big.Rat)
					intial_value.SetString("0")

					currentPayAmount := new(big.Rat)
					currentPayAmount.SetString(eventteampayamount)

					currentEventPayAmount := new(big.Rat)
					currentEventPayAmount.SetString("0")

					if maps[0]["total_gushou_fee"] != nil {
						total_gushou_fee.SetString(maps[0]["total_gushou_fee"].(string))
					}

					if maps[0]["total_gushou_recevie"] != nil {
						total_gushou_fee_recevied.SetString(maps[0]["total_gushou_recevie"].(string))
					}

					remainingGushouFee := new(big.Rat)
					remainingGushouFee.SetString("0")

					remainingGushouFee.Sub(total_gushou_fee, total_gushou_fee_recevied)

					if remainingGushouFee.Cmp(intial_value) == 1 {
						if remainingGushouFee.Cmp(currentPayAmount) == 1 || remainingGushouFee.Cmp(currentPayAmount) == 0 {

							currentPayAmountfloat := new(big.Rat)
							currentPayAmountfloat.SetString("0")
							currentPayAmountfloat.Mul(intial100Value, currentPayAmount)

							tempRemainingGushouFeeFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentPayAmountfloat)
							tempRemainingGushouFeeString := tempRemainingGushouFeeFloat.String()
							int64value, err := strconv.ParseInt(tempRemainingGushouFeeString, 10, 64)

							if err == nil {
								result, gushouBalanceTransactionId, paymentlogId = EventRegisterCreateChargeGushou(int64value, tempCurrency, tempRemainingGushouFeeString, userDetails, result, transaction_note, maps, eventregid, true, eventtname, chargetoken, paymentlogId)
							} else {
								result = "false"
							}
						} else if remainingGushouFee.Cmp(currentPayAmount) == -1 {
							currentEventPayAmount.Sub(currentPayAmount, remainingGushouFee)

							currentPayAmountfloat := new(big.Rat)
							currentPayAmountfloat.SetString("0")
							currentPayAmountfloat.Mul(intial100Value, remainingGushouFee)

							tempRemainingGushouFeeFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentPayAmountfloat)
							tempRemainingGushouFeeString := tempRemainingGushouFeeFloat.String()
							remainigGushouint64value, err := strconv.ParseInt(tempRemainingGushouFeeString, 10, 64)

							if err == nil {
								result, gushouBalanceTransactionId, paymentlogId = EventRegisterCreateChargeGushou(remainigGushouint64value, tempCurrency, tempRemainingGushouFeeString, userDetails, result, transaction_note, maps, eventregid, false, eventtname, chargetoken, paymentlogId)
							} else {
								result = "false"
							}

							if currentEventPayAmount.Cmp(intial_value) == 1 && result == "true" {

								gushouComisionFees, eventOrganizerfees, gushouCommisionfees := CalculateCommisionFeesForStripe(currentEventPayAmount)

								if gushouComisionFees != "0" && eventOrganizerfees != "0" {
									remainingGushouFeeFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(remainingGushouFee)
									result, paymentlogId = EventRegisterCreateChargeEventOrganizer(gushouComisionFees, eventOrganizerfees, userDetails, result, stripeConnectUserId, tempCurrency, transaction_note, maps, gushouCommisionfees, eventregid, gushouBalanceTransactionId, remainingGushouFeeFloat.String(), eventtname, chargetoken1, paymentlogId)
								} else {
									result = "false"
								}

							}

						}

					} else {

						gushouTotalFees, eventOrganizerfees, gushouCommisionfees := CalculateCommisionFeesForStripe(currentPayAmount)

						if gushouTotalFees != "0" && eventOrganizerfees != "0" {
							result, paymentlogId = EventRegisterCreateChargeEventOrganizer(gushouTotalFees, eventOrganizerfees, userDetails, result, stripeConnectUserId, tempCurrency, transaction_note, maps, gushouCommisionfees, eventregid, "", "0", eventtname, chargetoken, paymentlogId)
						} else {
							result = "false"
						}
					}

				} else {
					result = "false"
				}
			} else {
				result = "false"
			}
		} else {
			result = "false"
		}

	} else {
		result = "false"
	}

	return result, paymentlogId
}

func CalculateCommisionFeesForStripe(currentEventPayAmount *big.Rat) (string, string, string) {
	mode := big.ToNearestEven

	tempcurrentEventPayAmount := new(big.Rat)
	tempcurrentEventPayAmount.SetString("0")

	//stripeCommisionFee := new(big.Rat)
	//stripeCommisionFee.SetFloat64(constants.STRIPEFEE1)

	intial100Value := new(big.Rat)
	intial100Value.SetString("100")

	//stripeCommisionFee1 := new(big.Rat)
	//stripeCommisionFee1.SetFloat64(constants.STRIPEFEE2)

	gushouCommisionFee1 := new(big.Rat)
	gushouCommisionFee1.SetString(constants.GUSHOUCOMISSIONFEE)

	tempcurrentEventPayAmount.Mul(currentEventPayAmount, gushouCommisionFee1)
	tempcurrentEventPayAmount.Quo(tempcurrentEventPayAmount, intial100Value)
	//tempcurrentEventPayAmount.Add(tempcurrentEventPayAmount, stripeCommisionFee1)

	tempGushouComissionFeeString := new(big.Float).SetRat(tempcurrentEventPayAmount).Text('f', 2)

	tempGushouComissionFeeRat := new(big.Rat)
	tempGushouComissionFeeRat.SetString(tempGushouComissionFeeString)

	finalcurrentEventPayAmount := new(big.Rat)
	finalcurrentEventPayAmount.SetString("0")

	finalcurrentEventPayAmount.Sub(currentEventPayAmount, tempGushouComissionFeeRat)

	//tempfinalcurrentEventPayAmount := new(big.Rat)
	//tempfinalcurrentEventPayAmount.SetString("0")

	//tempfinalcurrentEventPayAmount.Mul(finalcurrentEventPayAmount, gushouCommisionFee1)
	//tempfinalcurrentEventPayAmount.Quo(tempfinalcurrentEventPayAmount, intial100Value)

	//	tempGushouComissionFeeString1 := new(big.Float).SetRat(tempfinalcurrentEventPayAmount).Text('f', 2)

	//tempGushouComissionFeeString1Rat := new(big.Rat)
	//tempGushouComissionFeeString1Rat.SetString(tempGushouComissionFeeString1)

	//finalcurrentEventPayAmount1 := new(big.Rat)
	//finalcurrentEventPayAmount1.SetString("0")

	//finalcurrentEventPayAmount1.Sub(finalcurrentEventPayAmount, tempGushouComissionFeeString1Rat)

	//tempGushouComissionFeeString1Rat.Add(tempGushouComissionFeeString1Rat, tempGushouComissionFeeRat)

	gushouComisionFees1 := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentEventPayAmount)

	eventOrganizerfees := new(big.Float).SetPrec(0).SetMode(mode).SetRat(finalcurrentEventPayAmount)

	gushouCommisionfees2 := new(big.Float).SetPrec(0).SetMode(mode).SetRat(tempGushouComissionFeeRat)

	return gushouComisionFees1.String(), eventOrganizerfees.String(), gushouCommisionfees2.String()
}

func EventRegisterCreateChargeEventOrganizer(Totalfees string, eventOrganizerfees string, userDetails map[string]orm.Params, result string, stripeConnectUserId string, tempCurrency string, transaction_note string, eventregpaymet []orm.Params, gushouCommisionfees string, eventregid string, gushouBalanceTransactionId string, tempGushouTotalfeesString string, eventtname string, chargetoken string, paymentlogId string) (string, string) {

	if eventregpaymet != nil {
		mode := big.ToNearestEven
		o := orm.NewOrm()

		var mapslog []orm.Params
		var maps []orm.Params
		cardType := ""
		reg_Pay_Id := ""

		if eventregpaymet[0]["id"] != nil {
			reg_Pay_Id = eventregpaymet[0]["id"].(string)
		}

		_, _ = o.Raw("SELECT * FROM event_register_payments WHERE fk_event_reg_id = ? ", eventregid).Values(&maps)

		eventregpaymet = maps

		totalfeesRat := new(big.Rat)
		totalfeesRat.SetString(Totalfees)

		intialRat100 := new(big.Rat)
		intialRat100.SetString("100")

		eventOrganizerfeesRat := new(big.Rat)
		eventOrganizerfeesRat.SetString(eventOrganizerfees)

		currentPayAmountfloat := new(big.Rat)
		currentPayAmountfloat.SetString("0")
		currentPayAmountfloat.Mul(intialRat100, totalfeesRat)

		tempTotalfeesFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentPayAmountfloat)
		tempTotalfeesString := tempTotalfeesFloat.String()
		tempTotalfeesint64value, err := strconv.ParseInt(tempTotalfeesString, 10, 64)

		eventOrganizerfeesString := new(big.Float).SetPrec(0).SetMode(mode).SetRat(eventOrganizerfeesRat).Text('f', 2)

		convertStringtoRat := new(big.Rat)
		convertStringtoRat.SetString(eventOrganizerfeesString)

		convertStringtoRat.Mul(convertStringtoRat, intialRat100)

		eventOrganizerfeesString1 := new(big.Float).SetPrec(0).SetMode(mode).SetRat(convertStringtoRat).Text('f', 0)

		eventOrganizerfeesint64value, err1 := strconv.ParseInt(eventOrganizerfeesString1, 10, 64)

		if err == nil && err1 == nil {
			var chargeParams *stripe.ChargeParams

			if chargetoken == "" {
				chargeParams = &stripe.ChargeParams{
					Amount:              stripe.Int64(tempTotalfeesint64value),
					Currency:            stripe.String(tempCurrency),
					Description:         stripe.String("REG FEE - " + eventtname),
					Customer:            stripe.String(userDetails["user"]["stripe_cust_id"].(string)),
					StatementDescriptor: stripe.String("Gushou Event Reg Fees"),
					ReceiptEmail:        stripe.String(userDetails["user"]["email_address"].(string)),
					Destination: &stripe.DestinationParams{
						Amount:  stripe.Int64(eventOrganizerfeesint64value),
						Account: stripe.String(stripeConnectUserId),
					},
				}
			} else {
				chargeParams = &stripe.ChargeParams{
					Amount:              stripe.Int64(tempTotalfeesint64value),
					Currency:            stripe.String(tempCurrency),
					Description:         stripe.String("REG FEE - " + eventtname),
					StatementDescriptor: stripe.String("Gushou Event Reg Fees"),
					ReceiptEmail:        stripe.String(userDetails["user"]["email_address"].(string)),
					Destination: &stripe.DestinationParams{
						Amount:  stripe.Int64(eventOrganizerfeesint64value),
						Account: stripe.String(stripeConnectUserId),
					},
				}
				chargeParams.SetSource(chargetoken)
			}
			ch, err := charge.New(chargeParams)

			if err == nil {

				cardType = string(ch.Source.Type)
				firstname := ""
				lastname := ""
				if userDetails["user"]["name"] != nil {
					firstname = userDetails["user"]["name"].(string)
				}

				if userDetails["user"]["last_name"] != nil {
					lastname = userDetails["user"]["last_name"].(string)
				}

				tempName := firstname + " " + lastname

				tabletotal_paid := ""
				tabletotal_event_recevie := ""
				tempgushoucomissionfee := ""

				if eventregpaymet[0]["total_paid"] != nil {
					tabletotal_paid = eventregpaymet[0]["total_paid"].(string)
				}

				if eventregpaymet[0]["total_event_receive"] != nil {
					tabletotal_event_recevie = eventregpaymet[0]["total_event_receive"].(string)
				}

				if eventregpaymet[0]["gushou_commision_fee"] != nil {
					tempgushoucomissionfee = eventregpaymet[0]["gushou_commision_fee"].(string)
				}

				temptabletotal_paid := new(big.Rat)
				temptabletotal_paid.SetString(tabletotal_paid)

				temptabletotal_event_recevie := new(big.Rat)
				temptabletotal_event_recevie.SetString(tabletotal_event_recevie)

				tempgushou_recevieValue := new(big.Rat)
				tempgushou_recevieValue.SetString(Totalfees)

				tempevent_recevieValue := new(big.Rat)
				tempevent_recevieValue.SetString(eventOrganizerfeesString)

				tempgushouCommisionValue := new(big.Rat)
				tempgushouCommisionValue.SetString(tempgushoucomissionfee)

				newgushouCommisionValue := new(big.Rat)
				newgushouCommisionValue.SetString(gushouCommisionfees)

				temptabletotal_paid.Add(temptabletotal_paid, tempgushou_recevieValue)
				temptabletotal_event_recevie.Add(temptabletotal_event_recevie, tempevent_recevieValue)
				tempgushouCommisionValue.Add(tempgushouCommisionValue, newgushouCommisionValue)

				temptabletotal_paidFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(temptabletotal_paid)
				temptabletotal_event_recevieFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(temptabletotal_event_recevie)
				tempgushouCommisionValueFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(tempgushouCommisionValue)

				_, err := o.Raw("UPDATE event_register_payments set total_paid = ? , total_event_receive = ?, last_updated_at =?, gushou_commision_fee =? WHERE id = ?", temptabletotal_paidFloat.String(), temptabletotal_event_recevieFloat.String(), time.Now(), tempgushouCommisionValueFloat.String(), reg_Pay_Id).Values(&mapslog)
				if err == nil {
					result, paymentlogId = creategushoucommisionlog(ch, gushouBalanceTransactionId, reg_Pay_Id, Totalfees, tempGushouTotalfeesString, result, tempName, transaction_note, gushouCommisionfees, ch.BalanceTransaction.ID, tempCurrency, cardType, paymentlogId)
				} else {
					result = "false"
				}

			} else {
				b, _ := json.Marshal(err)
				var erroPram *stripe.Error
				json.Unmarshal(b, &erroPram)
				result = erroPram.Msg
			}
		} else {
			result = "false"
		}
	} else {
		result = "false"
	}
	return result, paymentlogId
}

func creategushoucommisionlog(ch *stripe.Charge, gushouBalanceTransactionId string, reg_Pay_Id string, tempEventTotalfeesString string, tempGushouTotalfeesString string, result string, tempName string, transaction_note string, gushouCommisionfees string, eventBalanceTransactionId string, tempCurrency string, cardType string, paymentlogId string) (string, string) {
	o := orm.NewOrm()
	var mapslog []orm.Params

	currentDate := time.Unix(ch.Created, 0)
	amountReceivedBy := ""

	var eventNetCurrency string
	eventNetfees := 0.00
	eventTransactionfees := 0.00

	var gushouNetCurrency string
	gushouNetfees := 0.00
	gushouTransactionfees := 0.00

	totalAmount := "0"

	intialvalu100 := new(big.Rat)
	intialvalu100.SetString("100")

	tempEventTotalfeesString1 := new(big.Rat)
	tempEventTotalfeesString1.SetString(tempEventTotalfeesString)

	tempGushouTotalfeesString1 := new(big.Rat)
	tempGushouTotalfeesString1.SetString(tempGushouTotalfeesString)

	totalamountremaning := new(big.Rat)
	totalamountremaning.SetString("0")

	totalamountremaning.Add(tempEventTotalfeesString1, tempGushouTotalfeesString1)

	totalAmount = new(big.Float).SetPrec(0).SetMode(big.ToNearestEven).SetRat(totalamountremaning).Text('f', 2)

	if gushouBalanceTransactionId != "" {
		gushouBalanceTransaction, err1 := balance.GetBalanceTransaction(gushouBalanceTransactionId, nil)
		amountReceivedBy = "gushou"
		if err1 == nil {
			gushouNetCurrency = string(gushouBalanceTransaction.Currency)
			gushouNetfees = float64(gushouBalanceTransaction.Net) / 100
			gushouTransactionfees = float64(gushouBalanceTransaction.Fee) / 100
		}

	}

	if eventBalanceTransactionId != "" {
		eventBalanceTransaction, err := balance.GetBalanceTransaction(eventBalanceTransactionId, nil)

		if err == nil {
			eventNetCurrency = string(eventBalanceTransaction.Currency)
			eventNetfees = float64(eventBalanceTransaction.Net) / 100
			eventTransactionfees = float64(eventBalanceTransaction.Fee) / 100

			if gushouBalanceTransactionId != "" {
				amountReceivedBy = amountReceivedBy + "-"
			}
			amountReceivedBy = amountReceivedBy + "eventorganizer"
		}
	}

	balaceAmount := GetBalanceFromTotalAmountInEventRegistration(reg_Pay_Id)
	cardId := ""
	if ch.Customer != nil {
		cardId = ch.Customer.ID
	} else if ch.Source != nil {
		cardId = ch.Source.ID
	}

	_, err := o.Raw("INSERT INTO event_reg_payment_log (chargeId, balance_trasaction_id, created_date, currency_unit, amount, customerId, paidstatus, fk_event_reg_pay, transaction_type, paidby, status, transaction_note, transaction_action, amount_recived, balance_amount, gushoucommisionfee, stripe_event_conversion_currency, gushou_fee_net_fee, gushou_transaction_fee, event_fee_net_fee, event_transaction_fee, stripe_gushou_conversion_currency, paymenttype, balance_trasaction_id1 ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", ch.ID, ch.BalanceTransaction.ID, currentDate, strings.ToUpper(tempCurrency), totalAmount, cardId, ch.Paid, reg_Pay_Id, "stripe", tempName, ch.Status, transaction_note, "credit", amountReceivedBy, balaceAmount, gushouCommisionfees, strings.ToUpper(eventNetCurrency), gushouNetfees, gushouTransactionfees, eventNetfees, eventTransactionfees, strings.ToUpper(gushouNetCurrency), cardType, gushouBalanceTransactionId).Values(&mapslog)

	paid_date := currentDate.Format("January 02,2006")
	pay_log_id := mapslog[0]["id"].(string)

	if mapslog[0] != nil {
		paymentlogId = pay_log_id
	}

	SendMailForEventOrganizer(pay_log_id, reg_Pay_Id, ch.ID, ch.BalanceTransaction.ID, paid_date, tempCurrency, totalAmount, cardType)

	if err != nil {
		result = "false"
	}

	return result, paymentlogId
}

func SendMailForEventOrganizer(pay_log_id string, reg_Pay_Id string, chargeId string, transactionId string, paid_date string, currency_unit string, amount_paid string, cardType string) string {

	result := "true"
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	num, err := o.Raw("select erp.fk_team_id,erp.fk_event_id from event_reg_payment_log as erpl left join event_register_payments as erp on erp.id = erpl.fk_event_reg_pay where erpl.fk_event_reg_pay = ? ", reg_Pay_Id).Values(&maps)

	if err != nil {
		result = "false"
	}
	if num > 0 {
		fk_team_id := maps[0]["fk_team_id"].(string)
		fk_event_id := maps[0]["fk_event_id"].(string)

		if fk_team_id != "" && fk_event_id != "" {

			num1, err1 := o.Raw("select te.team_name as teamname,us.email_address as event_organizer_email, usp.name as firstname,usp.last_name as lastname, ev.fk_organizer_id as eventcaptain, ev.event_name,ev.currency as eventcurrency, erp.fk_event_reg_id as evenrregid, erp.id as orderid,erp.total_paid,erp.total_amount,(erp.total_amount - erp.total_paid) as balance from event_register_payments as erp left join event_register as er on er.id=erp.fk_event_reg_id left join event as ev on ev.id = erp.fk_event_id left join users as us on us.id = ev.fk_organizer_id left join user_profile as usp on usp.fk_user_id = us.id left join team as te on te.id = erp.fk_team_id where erp.fk_team_id = ? and erp.fk_event_id = ? and er.active= 1 and usp.instant_emails = 1 and er.registration_status ='Success' order by erp.id desc", fk_team_id, fk_event_id).Values(&maps1)

			if err1 != nil {
				result = "false"
			}
			if num1 > 0 {

				var event_organizer_email, event_name, event_organizer_name, team_name, event_currency, total_amount, balance, pay_method string
				event_organizer_email = maps1[0]["event_organizer_email"].(string)
				event_name = maps1[0]["event_name"].(string)
				event_organizer_name = maps1[0]["firstname"].(string) + " " + maps1[0]["lastname"].(string)
				team_name = maps1[0]["teamname"].(string)
				total_amount = maps1[0]["total_amount"].(string)
				balance = maps1[0]["balance"].(string)
				event_currency = common.ConvertUnitToCurrencySynbol(maps1[0]["eventcurrency"].(string))

				headerSubject := strings.Replace(constants.PAYEVENTEMAIL_SUBJECT, "#InvoiceId", pay_log_id, -1)
				headerSubject = strings.Replace(headerSubject, "#teamname", team_name, -1)

				invoice_link := "<a href='" + site_url + "stripe/downloadeventregistrationinvoice/" + pay_log_id + "'" + " target='_blank'>" + pay_log_id + "</a>"

				fmt.Println(invoice_link)
				if cardType != "" {
					pay_method = cardType
				} else {
					pay_method = "-"
				}
				mail_content1 := strings.Replace(constants.PAYEVENTEMAIL_CONTENT1, "#InvoiceId", invoice_link, -1)
				mail_content2 := constants.PAYEVENTEMAIL_CONTENT2
				mail_content3 := strings.Replace(constants.PAYEVENTEMAIL_CONTENT3, "#transactionId", transactionId, -1)
				mail_content4 := constants.PAYEVENTEMAIL_CONTENT4

				row1 := strings.Replace(constants.TABLE_CONTENT, "#title", "Paid To", -1)
				row1 = strings.Replace(row1, "#content", event_name, -1)

				row2 := strings.Replace(constants.TABLE_CONTENT, "#title", "Invoice No", -1)
				row2 = strings.Replace(row2, "#content", invoice_link, -1)

				row3 := strings.Replace(constants.TABLE_CONTENT, "#title", "Paid Date", -1)
				row3 = strings.Replace(row3, "#content", paid_date, -1)

				row4 := strings.Replace(constants.TABLE_CONTENT, "#title", "Payment Method", -1)
				row4 = strings.Replace(row4, "#content", pay_method, -1)

				amount_paid_with_unit := event_currency + " " + amount_paid
				row5 := strings.Replace(constants.TABLE_CONTENT, "#title", "Payment Amount", -1)
				row5 = strings.Replace(row5, "#content", amount_paid_with_unit, -1)

				total_amount_with_unit := event_currency + " " + total_amount
				row6 := strings.Replace(constants.TABLE_CONTENT, "#title", "Invoice Total", -1)
				row6 = strings.Replace(row6, "#content", total_amount_with_unit, -1)

				balance_with_unit := event_currency + " " + balance
				row7 := strings.Replace(constants.TABLE_CONTENT, "#title", "Invoice Balance", -1)
				row7 = strings.Replace(row7, "#content", balance_with_unit, -1)

				mail_table := "<table style='width:30%'>" + row1 + row2 + row3 + row4 + row5 + row6 + row7 + "</table>"

				footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
				unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
				html_email_message := "<html>Hello " + event_organizer_name + ", <br/><br/>" + mail_content1 + mail_content2 + mail_table + "<br><br>" + mail_content3 + mail_content4 + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
				common.SendEmail(event_organizer_email, headerSubject, html_email_message)

			}

		}
	}
	return result
}

func EventRegisterCreateChargeGushou(tempint64value int64, tempCurrency string, gushouFeeString string, userDetails map[string]orm.Params, result string, transaction_note string, eventregpaymet []orm.Params, eventregid string, writeLog bool, eventtname string, chargetoken string, paymentlogId string) (string, string, string) {

	blanaceTracsactionId := ""
	if eventregpaymet != nil {

		o := orm.NewOrm()
		mode := big.ToNearestEven
		var mapslog []orm.Params
		var maps []orm.Params
		cardType := ""
		reg_Pay_Id := ""

		intialvalu100 := new(big.Rat)
		intialvalu100.SetString("100")

		if eventregpaymet[0]["id"] != nil {
			reg_Pay_Id = eventregpaymet[0]["id"].(string)
		}

		_, _ = o.Raw("SELECT * FROM event_register_payments WHERE fk_event_reg_id = ? ", eventregid).Values(&maps)

		eventregpaymet = maps

		var chargeParams *stripe.ChargeParams

		if chargetoken == "" {
			chargeParams = &stripe.ChargeParams{
				Amount:              stripe.Int64(tempint64value),
				Currency:            stripe.String(tempCurrency),
				Description:         stripe.String("REG FEE - " + eventtname),
				Customer:            stripe.String(userDetails["user"]["stripe_cust_id"].(string)),
				StatementDescriptor: stripe.String("Gushou Event Reg Fees"),
				ReceiptEmail:        stripe.String(userDetails["user"]["email_address"].(string)),
			}
		} else {
			chargeParams = &stripe.ChargeParams{
				Amount:              stripe.Int64(tempint64value),
				Currency:            stripe.String(tempCurrency),
				Description:         stripe.String("REG FEE - " + eventtname),
				StatementDescriptor: stripe.String("Gushou Event Reg Fees"),
				ReceiptEmail:        stripe.String(userDetails["user"]["email_address"].(string)),
			}
			chargeParams.SetSource(chargetoken)
		}

		ch, err := charge.New(chargeParams)

		if err == nil {

			blanaceTracsactionId = ch.BalanceTransaction.ID
			cardType = string(ch.Source.Type)
			firstname := ""
			lastname := ""
			if userDetails["user"]["name"] != nil {
				firstname = userDetails["user"]["name"].(string)
			}

			if userDetails["user"]["last_name"] != nil {
				lastname = userDetails["user"]["last_name"].(string)
			}

			tempName := firstname + " " + lastname

			tabletotal_paid := ""
			tabletotal_gushou_recevie := ""

			if eventregpaymet[0]["total_paid"] != nil {
				tabletotal_paid = eventregpaymet[0]["total_paid"].(string)
			}

			if eventregpaymet[0]["total_gushou_recevie"] != nil {
				tabletotal_gushou_recevie = eventregpaymet[0]["total_gushou_recevie"].(string)
			}

			temptabletotal_paid := new(big.Rat)
			temptabletotal_paid.SetString(tabletotal_paid)

			temptabletotal_gushou_recevie := new(big.Rat)
			temptabletotal_gushou_recevie.SetString(tabletotal_gushou_recevie)

			gushouFeeFloat := new(big.Rat)
			gushouFeeFloat.SetString(gushouFeeString)
			gushouFeeFloat.Quo(gushouFeeFloat, intialvalu100)

			tempgushouFeeFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(gushouFeeFloat)

			tempgushou_recevieValue := new(big.Rat)
			tempgushou_recevieValue.SetString(tempgushouFeeFloat.String())

			temptabletotal_paid.Add(temptabletotal_paid, tempgushou_recevieValue)
			temptabletotal_gushou_recevie.Add(temptabletotal_gushou_recevie, tempgushou_recevieValue)

			temptabletotal_paidFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(temptabletotal_paid)
			fmt.Println(temptabletotal_paidFloat)
			temptabletotal_gushou_recevieFloat := new(big.Float).SetPrec(0).SetMode(mode).SetRat(temptabletotal_gushou_recevie)

			fmt.Println(temptabletotal_gushou_recevieFloat)
			_, err := o.Raw("UPDATE event_register_payments set total_paid = ? , total_gushou_recevie = ?, last_updated_at =? WHERE id = ?", temptabletotal_paidFloat.String(), temptabletotal_gushou_recevieFloat.String(), time.Now(), reg_Pay_Id).Values(&mapslog)
			if err != nil {
				result = "false"
			} else {
				if writeLog {
					gushouFeeString12 := new(big.Rat)
					gushouFeeString12.SetString(gushouFeeString)
					gushouFeeString12.Quo(gushouFeeString12, intialvalu100)
					fmt.Println(gushouFeeString12)
					gushouFeeString12Float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(gushouFeeString12)
					result, paymentlogId = creategushoucommisionlog(ch, ch.BalanceTransaction.ID, reg_Pay_Id, "0", gushouFeeString12Float.String(), result, tempName, transaction_note, "0", "", tempCurrency, cardType, paymentlogId)
				}
			}

		} else {
			b, _ := json.Marshal(err)
			var erroPram *stripe.Error
			json.Unmarshal(b, &erroPram)
			result = erroPram.Msg
		}

	} else {
		result = "false"
	}
	return result, blanaceTracsactionId, paymentlogId
}

func CheckStripeOptionEnableOrNotByEventOrg(eventid string) string {
	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"
	num, _ := o.Raw("SELECT * FROM user_stripe_connect_detail WHERE fk_event_id =? ", eventid).Values(&maps)

	if num > 0 {
		result = "true"
	}
	return result
}

func CheckStripeOptionEnableOrNotByEventOrgFullDetail(eventid string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("SELECT * FROM user_stripe_connect_detail WHERE fk_event_id =? ", eventid).Values(&maps)

	return maps
}

func CheckSubscription_Card_Detail_By_UserId(userId string) (bool, string) {
	o := orm.NewOrm()
	result := false
	last := ""
	var maps []orm.Params
	num, _ := o.Raw("select * from card_detail where fk_user_id=? order by default_source desc", userId).Values(&maps)

	if num > 0 {
		result = true
		if maps[0]["last4"] != nil {
			last = maps[0]["last4"].(string)
		}
	}
	return result, last
}

func GetEventRegisterDetailByEveRegisterId(eventregid string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("select * from event_register where id=? ", eventregid).Values(&maps)

	return maps
}

func GetEventOrganizerConnectedAccountDetail(eventid string) string {
	o := orm.NewOrm()

	var maps []orm.Params
	result := ""
	num, _ := o.Raw("SELECT * FROM user_stripe_connect_detail WHERE fk_event_id =? ", eventid).Values(&maps)

	if num > 0 {
		if maps[0]["stripe_user_id"] != nil {
			result = maps[0]["stripe_user_id"].(string)
		}
	}
	return result
}

func GetLastTransactionHistoryByRegPayId(regPayId string) string {
	o := orm.NewOrm()
	result := ""
	var maps []orm.Params
	num, _ := o.Raw("select * from event_reg_payment_log where fk_event_reg_pay=? order by id desc limit 1", regPayId).Values(&maps)

	if num > 0 {
		if maps[0]["transaction_note"] != nil {
			result = maps[0]["transaction_note"].(string)
		}
	}
	return result
}

func GetTeamEventPaymentRegistration(eventregid string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("select * from event_register_payments where id=? ", eventregid).Values(&maps)

	return maps
}

func GetPaymentTrackingDetails(eventregpay string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("select to_char(amount, 'FM999,999,999,990D00') as formatedamount, to_char(balance_amount, 'FM999,999,999,990D00') as formated_balanceamount ,*, to_char(created_date, 'DD/MM/YY at HH12:MI AM ') as tempcreatedate, lower(currency_unit) as currency_unitlower	 from event_reg_payment_log where fk_event_reg_pay=? order by id desc", eventregpay).Values(&maps)

	return maps
}

func GetBalanceFromTotalAmountInEventRegistration(event_pay_reg_id string) string {
	o := orm.NewOrm()
	result := "-"
	var maps []orm.Params
	num1, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps)

	if num1 > 0 {
		if maps[0]["balance_amount"] != nil {
			result = maps[0]["balance_amount"].(string)
		}
	}
	return result
}

func GetEventTransactionLogByLogId(id string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, _ = o.Raw("select to_char(created_date, 'Mon DD,YYYY') as createddate, * from event_reg_payment_log where id=? ", id).Values(&maps)
	return maps
}
