package models

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	_ "golang.org/x/crypto/bcrypt"
	//"github.com/astaxie/beego"
	"gushou/common"
	"gushou/constants"
	"os"
)

func CheckWaiver(team_id string, event_id string) bool {

	result := false

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM team_waivers WHERE active = 1 AND fk_event_id = ? AND fk_team_id = ?", event_id, team_id).Values(&maps)
	if num > 0 {
		fmt.Println("Waiver exists")
		result = true

	} else {
		fmt.Println("Waiver does not exists")
		result = false
	}

	return result

}

func SaveWaiver(path string, team_id string, event_id string, waiver_name string) (string, string) {

	result := ""
	//result["status"] = "0"
	var maps []orm.Params
	var temp []orm.Params
	var captains []orm.Params
	waiverId := ""
	o := orm.NewOrm()

	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("INSERT INTO team_waivers (active, uploaded_date, waiver_name, waiver_url, fk_team_id, fk_event_id)  VALUES (?,?,?,?,?,?) RETURNING id", "1", time.Now(), waiver_name, url, team_id, event_id).Values(&maps)

	if err == nil {

		waiverId = maps[0]["id"].(string)

		num, _ := o.Raw("SELECT fk_co_captains FROM team WHERE active = 1 AND id = ? ", team_id).Values(&captains)

		if num > 0 {
			s := strings.Replace(captains[0]["fk_co_captains"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			captain_arr := strings.Split(s, ",")

			for _, v := range captain_arr {

				num, _ := o.Raw("SELECT * FROM waivers WHERE fk_event_id = ? AND fk_team_id = ? AND fk_team_waiver_id = ? AND fk_member_id = ?", event_id, team_id, maps[0]["id"].(string), v).Values(&temp)

				if num == 0 {
					_, _ = o.Raw("INSERT INTO waivers (active, fk_team_id, fk_event_id, fk_team_waiver_id, fk_member_id,signed_upload_waiver_path,isunder18,validate,signed_upload_date)  VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", team_id, event_id, maps[0]["id"].(string), v, "", false, false, nil).Exec()
				}
			}
		} else {
			fmt.Println("No team captains")
		}

		fmt.Println("Waiver saved")
		result = "true"
	} else {
		fmt.Println("Failed")
		result = "false"
	}

	return result, waiverId

}

func GetTeamWaiver(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Team id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params

	num, _ := o.Raw("SELECT * FROM team_waivers WHERE active = 1 AND fk_team_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "waiver_" + strconv.Itoa(k)
			count, _ := o.Raw("SELECT event_name FROM event WHERE active = ? AND id = ? AND COALESCE (end_date, start_date) > NOW()", 1, v["fk_event_id"]).Values(&values)
			if count > 0 {
				v["event_name"] = values[0]["event_name"]
				result[key] = v
			}

		}

	} else {
		fmt.Println("No current waiver for this team")
		result["status"] = nil
	}

	return result

}

func GetWaiverMembers(id string, team_id string, event_id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Waiver id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	//SELECT *, to_char(waiver_send_date, 'DD Mon YYYY') as send_date, to_char(waiver_signed_date, 'DD Mon YYYY') as signed_date

	//SELECT * FROM team_waivers tw join teamjoin tj on tw.fk_team_id = tj.fk_team_id WHERE tw.active = 1 AND tj.request_status = 2 and tw.id = 99

	var maps []orm.Params
	var values []orm.Params
	var details []orm.Params
	//var data orm.Params

	num, _ := o.Raw("SELECT fk_member_id, tj.active FROM team_waivers tw join teamjoin tj on tw.fk_team_id = tj.fk_team_id WHERE tj.fk_member_id is NOT NULL AND tw.active = 1 AND tj.request_status = 2 AND tw.id = ?", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "waiver_member_" + strconv.Itoa(k)

			count, _ := o.Raw("SELECT name, last_name, classes, files, date_part('year',age(dob)) as age FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.id = ? AND name is NOT NULL AND last_name is NOT NULL", v["fk_member_id"]).Values(&details)
			if count > 0 {
				v["user_name"] = details[0]["name"].(string) + " " + details[0]["last_name"].(string)
				v["classes"] = ""
				v["files"] = ""
				v["age"] = ""
				if details[0]["classes"] != nil {
					v["classes"] = details[0]["classes"].(string)
				}

				if details[0]["files"] != nil {
					v["files"] = details[0]["files"].(string)
				}

				if details[0]["age"] != nil {
					v["age"] = details[0]["age"].(string)
				}
			}

			count, _ = o.Raw("SELECT *,to_char(signed_upload_date, 'DD Mon YYYY') as waiver_upload_date, to_char(waiver_send_date, 'DD Mon YYYY') as send_date, to_char(waiver_signed_date, 'DD Mon YYYY') as signed_date FROM waivers WHERE fk_member_id = ? AND fk_team_id = ? AND fk_event_id = ? and fk_team_waiver_id =? ", v["fk_member_id"], team_id, event_id, id).Values(&values)
			if count > 0 {

				if values[0]["send_date"] != nil {
					v["send_date"] = values[0]["send_date"]
				}
				if values[0]["waiver_upload_date"] != nil {
					v["waiver_upload_date"] = values[0]["waiver_upload_date"]
				}

				if values[0]["signed_date"] != nil {
					v["signed_date"] = values[0]["signed_date"]
				}

				if values[0]["contact_name"] != nil {
					v["contact_name"] = values[0]["contact_name"]
				}

				if values[0]["contact_number"] != nil {
					v["contact_number"] = values[0]["contact_number"]
				}

				if values[0]["medical_condition_desc"] != nil {
					v["medical_condition_desc"] = values[0]["medical_condition_desc"]
				}

			}

			if v["active"] == "9" {
				if v["signed_date"] != nil {
					result[key] = v
				}

			} else if v["active"] == "7" {
				if v["signed_date"] != nil {
					result[key] = v
				}

			} else {
				result[key] = v
			}

			//fmt.Println(v)
		}

	} else {
		fmt.Println("No waiver members")
		result["status"] = nil
	}

	return result

}

func SendTeamWaiver(MemberIds []string, waiver_id string, team_id string, user_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params
	var values1 []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT waiver_name, tw.fk_event_id, event_name, slug "+
		"FROM team_waivers tw "+
		"inner join event ev on tw.fk_event_id = ev.id  "+
		"WHERE tw.id = ?", waiver_id).Values(&maps)

	waiver_name := maps[0]["waiver_name"].(string)
	event_id := maps[0]["fk_event_id"].(string)
	event_name := maps[0]["event_name"].(string)
	event_slug := maps[0]["slug"].(string)

	eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
	desc := strings.Replace(constants.SEND_TEAM_WAIVER_NOTIFICATION, "#eventName", eventUrl, -1)

	for _, v := range MemberIds {

		userStatus := CheckUserEmailVerificationStatus(v)

		if userStatus {
			exists, _ := o.Raw("SELECT fk_member_id, waiver_signed_date, waiver_send_date FROM waivers WHERE fk_team_waiver_id = ? AND fk_member_id = ? AND fk_team_id = ? AND fk_event_id = ?", waiver_id, v, team_id, event_id).Values(&maps)
			if exists == 0 {
				_, err1 := o.Raw("INSERT INTO waivers (active, waiver_send_date, fk_team_id, fk_event_id, fk_team_waiver_id, fk_member_id,signed_upload_waiver_path,isunder18,validate,signed_upload_date)  VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", time.Now(), team_id, event_id, waiver_id, v, "", false, false, nil).Exec()
				fmt.Println(err1)
				_, err2 := o.Raw("SELECT fk_member_id, waiver_signed_date, waiver_send_date FROM waivers WHERE fk_team_waiver_id = ? AND fk_member_id = ?", waiver_id, v).Values(&maps)
				fmt.Println(err2)
				member_id := maps[0]["fk_member_id"].(string)
				//waiver_signed_date := maps[0]["waiver_signed_date"]
				waiver_send_date := maps[0]["waiver_send_date"]

				user_details := GetUserDetails(member_id)
				member_name := user_details["user"]["name"].(string)
				email_address := user_details["user"]["email_address"].(string)
				send_instant_emails := user_details["user"]["instant_emails"].(string)
				//send_email_digest := receiver_details["user"]["subscribe"]

				fmt.Println("Waiver send date")
				fmt.Println(waiver_send_date)
				//digest_mesg := member_name + " has asked you to sign a waiver for event " + event_name

				receiver := "{" + member_id + "}"

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, receiver).Values(&maps)

				if err == nil {
					status = "true"

					if send_instant_emails == "1" {
						subject := "Sign waiver"
						waiver_link := "<a href='" + site_url + "waiver-all'>link</a>"
						email_address := email_address

						email_html_message := strings.Replace(constants.SEND_TEAM_WAIVER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
						html_email_message := "<html> Hi " + member_name + ", <br/><br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						//fmt.Printf("html_email_message \n")
						//fmt.Printf(html_email_message)
						common.SendEmail(email_address, subject, html_email_message)
					}

				} else {
					fmt.Println("Notification failed")
				}

			} else {

				if maps[0]["waiver_send_date"] != nil && maps[0]["waiver_signed_date"] == nil {
					_, err := o.Raw("UPDATE waivers SET waiver_send_date = ? WHERE fk_team_waiver_id = ? AND fk_member_id = ?", time.Now(), waiver_id, v).Values(&values1)
					fmt.Println(err)
					member_id := "{" + v + "}"
					user_details := GetUserDetails(v)
					member_name := user_details["user"]["name"].(string)
					email_address := user_details["user"]["email_address"].(string)
					send_instant_emails := user_details["user"]["instant_emails"].(string)

					count, err := o.Raw("UPDATE notification_list SET date = ? WHERE fk_team_id = ? and fk_event_id = ? and fk_receiver = ? and (notification_desc = ? OR notification_desc like ? OR notification_desc like ? ) and active = 1 RETURNING id", time.Now(), team_id, event_id, member_id, desc, "Please accept your%", "Please accept electronic waiver for%").Values(&maps)

					if count == 0 {

						_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, member_id).Values(&maps)

					}

					if err == nil {
						status = "true"
						fmt.Println("Notification updated")

					} else {
						fmt.Println("Notification failed")
					}

					if send_instant_emails == "1" {
						subject := event_name + " waiver request"
						waiver_link := "<a href='" + site_url + "waiver-all'>link</a>"
						email_address := email_address

						email_html_message := strings.Replace(constants.SEND_TEAM_WAIVER_REMINDER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html> Hi " + member_name + ", <br/><br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						common.SendEmail(email_address, subject, html_email_message)
					}
				} else if maps[0]["waiver_send_date"] == nil && maps[0]["waiver_signed_date"] == nil {
					_, _ = o.Raw("UPDATE waivers SET waiver_send_date = ? WHERE fk_team_waiver_id = ? AND fk_member_id = ?", time.Now(), waiver_id, v).Values(&values)

					member_id := "{" + v + "}"
					user_details := GetUserDetails(v)
					member_name := user_details["user"]["name"].(string)
					email_address := user_details["user"]["email_address"].(string)
					send_instant_emails := user_details["user"]["instant_emails"].(string)

					count, err := o.Raw("UPDATE notification_list SET date = ? WHERE fk_team_id = ? and fk_event_id = ? and fk_receiver = ? and (notification_desc = ? OR notification_desc like ? OR notification_desc like ? ) and active = 1 RETURNING id", time.Now(), team_id, event_id, member_id, desc, "Please accept your%", "Please accept electronic waiver for%").Values(&maps)

					if count == 0 {

						_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, member_id).Values(&maps)

					}

					if err == nil {
						status = "true"
						fmt.Println("Notification updated")

					} else {
						fmt.Println("Notification failed")
					}

					if send_instant_emails == "1" {
						subject := event_name + " waiver request"
						waiver_link := "<a href='" + site_url + "waiver-all'>link</a>"
						email_address := email_address

						email_html_message := strings.Replace(constants.SEND_TEAM_WAIVER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html> Hi " + member_name + ", <br/><br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						common.SendEmail(email_address, subject, html_email_message)
					}

				} else {
					fmt.Println("Waiver already signed")
				}

			}
		}
	}

	return status
}

func GetWaiver(event_id string, team_id string, user_id string, admin string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params
	var captains []orm.Params
	var temp []orm.Params

	num, _ := o.Raw("SELECT * FROM team_waivers join waivers on team_waivers.id = waivers.fk_team_waiver_id join teamjoin on team_waivers.fk_team_id = teamjoin.fk_team_id WHERE team_waivers.fk_event_id = ? AND team_waivers.fk_team_id = ? AND waivers.fk_member_id = ? AND teamjoin.fk_member_id = ? AND teamjoin.active = ?", event_id, team_id, user_id, user_id, 1).Values(&maps)
	if num > 0 {

		for k, v := range maps {

			key := "team_waiver_" + strconv.Itoa(k)

			result[key] = v

		}

	} else {

		if admin == "true" {

			num, err := o.Raw("SELECT * FROM team WHERE active = 1 AND fk_co_captains @> ARRAY [?]::bigint[] AND id = ?", user_id, team_id).Values(&maps)
			fmt.Println(err)
			if num > 0 {

				num, _ := o.Raw("SELECT * FROM team_waivers join waivers on team_waivers.id = waivers.fk_team_waiver_id WHERE team_waivers.fk_event_id = ? AND team_waivers.fk_team_id = ? AND waivers.fk_member_id = ? ", event_id, team_id, user_id).Values(&maps)
				if num > 0 {

					for k, v := range maps {
						key := "team_waiver_" + strconv.Itoa(k)

						result[key] = v
					}

				} else {
					num, _ := o.Raw("SELECT id FROM team_waivers WHERE fk_event_id = ? AND fk_team_id = ? order by id desc", event_id, team_id).Values(&maps)
					if num > 0 {

						num, _ := o.Raw("SELECT * FROM waivers WHERE fk_event_id = ? AND fk_team_id = ? AND fk_team_waiver_id = ? AND fk_member_id = ?", event_id, team_id, maps[0]["id"].(string), user_id).Values(&temp)

						if num == 0 {
							_, _ = o.Raw("INSERT INTO waivers (active, fk_team_id, fk_event_id, fk_team_waiver_id, fk_member_id,signed_upload_waiver_path,isunder18,validate,signed_upload_date)  VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", team_id, event_id, maps[0]["id"].(string), user_id, "", false, false, nil).Exec()
						}

						num, _ = o.Raw("SELECT * FROM team_waivers join waivers on team_waivers.id = waivers.fk_team_waiver_id WHERE team_waivers.fk_event_id = ? AND team_waivers.fk_team_id = ? AND waivers.fk_member_id = ? ", event_id, team_id, user_id).Values(&maps)
						if num > 0 {

							for k, v := range maps {
								key := "team_waiver_" + strconv.Itoa(k)

								result[key] = v
							}

						}

					}

				}

			} else {
				num, _ := o.Raw("SELECT team_waivers.waiver_name, team_waivers.waiver_url FROM team_waivers WHERE team_waivers.fk_event_id = ? AND team_waivers.fk_team_id = ? order by id desc", event_id, team_id).Values(&maps)

				if num > 0 {

					for k, v := range maps {
						key := "team_waiver_" + strconv.Itoa(k)

						result[key] = v
					}

				}
			}

		} else {

			num, _ := o.Raw("SELECT id FROM team_waivers WHERE fk_event_id = ? AND fk_team_id = ? order by id desc ", event_id, team_id).Values(&maps)

			if num > 0 {
				num, _ := o.Raw("SELECT fk_co_captains FROM team WHERE active = 1 AND id = ? ", team_id).Values(&captains)

				if num > 0 {
					s := strings.Replace(captains[0]["fk_co_captains"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					captain_arr := strings.Split(s, ",")

					for _, v := range captain_arr {

						num, _ := o.Raw("SELECT * FROM waivers WHERE fk_event_id = ? AND fk_team_id = ? AND fk_team_waiver_id = ? AND fk_member_id = ?", event_id, team_id, maps[0]["id"].(string), v).Values(&temp)

						if num == 0 {
							_, _ = o.Raw("INSERT INTO waivers (active, fk_team_id, fk_event_id, fk_team_waiver_id, fk_member_id,signed_upload_waiver_path,isunder18,validate,signed_upload_date)  VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", team_id, event_id, maps[0]["id"].(string), v, "", false, false, nil).Exec()
						}

					}

					num, _ := o.Raw("SELECT * FROM team_waivers join waivers on team_waivers.id = waivers.fk_team_waiver_id WHERE team_waivers.fk_event_id = ? AND team_waivers.fk_team_id = ? AND waivers.fk_member_id = ? ", event_id, team_id, user_id).Values(&maps)
					if num > 0 {

						for k, v := range maps {
							key := "team_waiver_" + strconv.Itoa(k)

							result[key] = v
						}

					}

				} else {
					fmt.Println("No team captains")
				}
			} else {
				fmt.Println("No team waiver exists")
				result["status"] = nil
			}

		}

	}

	return result

}

func UpdateWaiver(data map[string]string, team_id string, event_id string, fk_team_waiver_id string, fk_member_id string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	fmt.Println("UpdateWaiver")
	fmt.Println(data)

	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("UPDATE waivers SET contact_name = ?, contact_number = ?, i_agree = ? , medical_condition_desc = ? ,medical_option = ? ,waiver_signed_date = ? WHERE fk_team_id = ? AND fk_event_id = ? AND fk_team_waiver_id = ? AND fk_member_id = ? RETURNING id", data["ContactName"], data["ContactNum"], "I agree", data["MedicalConditionDesc"], data["MedicalCondition"], time.Now(), team_id, event_id, fk_team_waiver_id, fk_member_id).Values(&maps)

	//result["user_id"] = maps[0]["id"].(string)

	if err == nil {

		_, _ = o.Raw("UPDATE notification_list SET active = ? WHERE type = 3 AND fk_team_id = ? AND fk_event_id = ? AND fk_receiver @> ARRAY [?]::bigint[]  RETURNING id", "9", team_id, event_id, fk_member_id).Values(&maps)

		result["status"] = "1"
		fmt.Println("Waiver Updated")
	}

	return result

}

func GetMyWaiver(user_id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params
	var values []orm.Params

	num, err := o.Raw("SELECT w.fk_team_waiver_id, w.fk_team_id, w.fk_event_id, tw.waiver_name, to_char(w.waiver_signed_date, 'DD Mon YYYY') as signed_date FROM waivers w join team_waivers tw on w.fk_team_waiver_id = tw.id WHERE fk_member_id = ? GROUP BY w.fk_team_waiver_id, w.fk_team_id, w.fk_event_id, tw.waiver_name, w.waiver_signed_date  ORDER  BY w.fk_team_waiver_id desc  ", user_id).Values(&maps)
	fmt.Println("erroroccure", err)
	if num > 0 {

		for k, v := range maps {
			key := "my_waiver_" + strconv.Itoa(k)
			_, _ = o.Raw("SELECT event_name FROM event WHERE id = ?", v["fk_event_id"]).Values(&values)
			v["event_name"] = values[0]["event_name"].(string)

			_, _ = o.Raw("SELECT team_name FROM team WHERE id = ?", v["fk_team_id"]).Values(&values)
			v["team_name"] = values[0]["team_name"].(string)

			result[key] = v
		}

	} else {
		fmt.Println("No waiver for the user exists")
		result["status"] = nil
	}

	return result

}

func GetWaiverRequestSendOrNot(id string) bool {

	result := false
	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM waivers WHERE fk_team_waiver_id = ?", id).Values(&maps)

	if num > 0 {
		result = true
	}
	return result
}

func SendIndividualTeamWaiver_To_IndividualMember(teamid string, userid string) {

	fmt.Println("In SendIndividualTeamWaiver_To_IndividualMember")
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var alreadyMember []string
	num, err := o.Raw("select * from team where id=?", teamid).Values(&maps)
	if num > 0 && err == nil {
		num, err := o.Raw("select * from team_waivers where fk_team_id=? order by id desc limit 1", teamid).Values(&maps1)
		if num > 0 && err == nil {
			alreadyMember = append(alreadyMember, userid)
			SendTeamWaiver(alreadyMember, maps1[0]["id"].(string), teamid, maps[0]["fk_captain_id"].(string))
		}
	}
	fmt.Println("Out SendIndividualTeamWaiver_To_IndividualMember")
}

func SaveSignedUploadedWaiver(path string, waiver_member_id string, waiver_id string, emergency_contact_name string, emergency_contact_number string, medical_condition string, isUnder_18 bool, team_id string, event_id string) string {

	result := ""

	var maps []orm.Params

	o := orm.NewOrm()

	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("UPDATE waivers SET signed_upload_waiver_path =?,signed_upload_date=?,contact_name=?,contact_number=?,medical_condition_desc=?,isunder18 = ? where fk_team_waiver_id =? and fk_member_id=? and fk_team_id=? and fk_event_id=?", url, time.Now(), emergency_contact_name, emergency_contact_number, medical_condition, isUnder_18, waiver_id, waiver_member_id, team_id, event_id).Values(&maps)

	fmt.Println(err)
	if err == nil {

		fmt.Println("Waiver saved")
		result = "true"
	} else {
		fmt.Println("Failed")
		result = "false"
	}

	return result

}
func GetUploadedWaiverDetails(waiver_id string, member_id string, event_id string, team_id string) []map[string]string {
	result := make(map[string]string)
	resultList := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	team_slug := ""
	event_slug := ""
	num, err := o.Raw("select * from waivers as wai left join team_waivers as te_waiver on te_waiver.id = wai.fk_team_waiver_id where wai.fk_team_id = ? and wai.fk_event_id = ? and wai.fk_team_waiver_id = ? and wai.fk_member_id = ?", team_id, event_id, waiver_id, member_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		team_slug = GetTeamSlug(team_id)
		event_slug = GetEventSlug(event_id)

		result["team_slug"] = team_slug
		result["event_slug"] = event_slug

		if maps[0]["contact_name"] != nil {

			result["contact_name"] = maps[0]["contact_name"].(string)
		} else {
			result["contact_name"] = ""
		}

		if maps[0]["contact_number"] != nil {

			result["contact_number"] = maps[0]["contact_number"].(string)
		} else {
			result["contact_number"] = ""
		}

		if maps[0]["medical_condition_desc"] != nil {

			result["medical_condition_desc"] = maps[0]["medical_condition_desc"].(string)
		} else {
			result["medical_condition_desc"] = ""
		}

		if maps[0]["signed_upload_waiver_path"] != nil {

			result["signed_upload_waiver_path"] = maps[0]["signed_upload_waiver_path"].(string)
		} else {
			result["signed_upload_waiver_path"] = ""
		}

		if maps[0]["waiver_name"] != nil {

			result["waiver_name"] = maps[0]["waiver_name"].(string)
		} else {
			result["waiver_name"] = ""
		}

		resultList = append(resultList, result)
		result = make(map[string]string)
	}

	return resultList
}
func ValidateSignedTeamWaiver(member_id string, waiver_id string, fk_team_id string, fk_event_id string) string {

	result := "false"
	o := orm.NewOrm()
	var maps []orm.Params

	num1, err := o.Raw("UPDATE waivers SET validate =? where fk_team_waiver_id =? and fk_member_id=? and fk_team_id=? and fk_event_id=?", true, waiver_id, member_id, fk_team_id, fk_event_id).Values(&maps)
	fmt.Println(err)
	if err == nil {
		if num1 > 0 {
			result = "true"
		}
	}

	return result

}
func GetEventDetailsByEventId(event_id string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("select * from event_register where fk_event_id = ? and active=1 and registration_status =? and reg_cancel_status = ? order by id desc", event_id, "Success", "false").Values(&maps)
	fmt.Println(err)

	return maps
}
func SaveUnder18SignedUploadedWaiver(path string, waiver_member_id string, waiver_id string, emergency_contact_name string, emergency_contact_number string, medical_con string, medical_condition_des string, isUnder_18 bool, team_id string, event_id string) string {

	result := ""

	var maps []orm.Params

	o := orm.NewOrm()

	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("UPDATE waivers SET signed_upload_waiver_path =?,signed_upload_date=?,contact_name=?,contact_number=?,medical_option = ?,i_agree = ?,medical_condition_desc=?,isunder18 = ? where fk_team_waiver_id =? and fk_member_id=? and fk_team_id=? and fk_event_id=?", url, time.Now(), emergency_contact_name, emergency_contact_number, medical_con, "I agree", medical_condition_des, true, waiver_id, waiver_member_id, team_id, event_id).Values(&maps)

	fmt.Println(err)
	if err == nil {

		fmt.Println("Waiver saved")
		result = "true"
	} else {
		fmt.Println("Failed")
		result = "false"
	}

	return result

}
func SendEventTeamWaiver(event_reg_id string, user_id string) (string, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	result := "true"
	team_id := ""
	var member_array []string
	num, err := o.Raw("select * from event_register where id = ? and active=1 and registration_status =? and reg_cancel_status = ?", event_reg_id, "Success", "false").Values(&maps)
	fmt.Println(err)
	if num > 0 {
		var event_id string
		team_id = maps[0]["fk_team_id"].(string)
		event_id = maps[0]["fk_event_id"].(string)

		num1, err1 := o.Raw("select * from team_waivers where fk_event_id = ? and fk_team_id = ?", event_id, team_id).Values(&maps1)
		fmt.Println(err1)
		if num1 > 0 {

			var waiver_id = maps1[0]["id"].(string)

			num2, err2 := o.Raw("select * from waivers where fk_team_id = ? and fk_event_id = ? and fk_team_waiver_id = ?", team_id, event_id, waiver_id).Values(&maps2)
			fmt.Println(err2)

			if num2 > 0 {

				for _, v := range maps2 {

					member_array = append(member_array, v["fk_member_id"].(string))

				}
				fmt.Println(member_array)

				SendTeamWaiverEve(member_array, waiver_id, team_id, user_id)
			}

		} else {
			result = "add waiver"
		}

	}

	return team_id, result
}

func SendEventTeamIndividualWaiver(event_id string, team_id string, member_id string, event_reg_id string, user_id string) string {
	result := "true"
	o := orm.NewOrm()
	var maps []orm.Params
	var member_array []string
	num1, err1 := o.Raw("select * from team_waivers where fk_event_id = ? and fk_team_id = ?", event_id, team_id).Values(&maps)
	fmt.Println(err1)
	if num1 > 0 {
		result = "true"
		var waiver_id = maps[0]["id"].(string)
		member_array = append(member_array, member_id)
		SendTeamWaiverEve(member_array, waiver_id, team_id, user_id)
	} else {
		result = "add waiver"
	}
	return result
}

func SendTeamWaiverEve(MemberIds []string, waiver_id string, team_id string, user_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params
	var values1 []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT waiver_name, tw.fk_event_id, event_name, slug "+
		"FROM team_waivers tw "+
		"inner join event ev on tw.fk_event_id = ev.id  "+
		"WHERE tw.id = ?", waiver_id).Values(&maps)

	waiver_name := maps[0]["waiver_name"].(string)
	event_id := maps[0]["fk_event_id"].(string)
	event_name := maps[0]["event_name"].(string)
	event_slug := maps[0]["slug"].(string)
	team_slug := GetTeamSlug(team_id)
	fmt.Println(team_slug)
	from := "emailwaiver"
	eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
	desc := strings.Replace(constants.SEND_TEAM_WAIVER_NOTIFICATION, "#eventName", eventUrl, -1)

	for _, v := range MemberIds {

		userStatus := CheckUserEmailVerificationStatus(v)

		if userStatus {
			exists, _ := o.Raw("SELECT fk_member_id, waiver_signed_date, waiver_send_date FROM waivers WHERE fk_team_waiver_id = ? AND fk_member_id = ? AND fk_team_id = ? AND fk_event_id = ?", waiver_id, v, team_id, event_id).Values(&maps)
			if exists == 0 {
				_, err1 := o.Raw("INSERT INTO waivers (active, waiver_send_date, fk_team_id, fk_event_id, fk_team_waiver_id, fk_member_id,signed_upload_waiver_path,isunder18,validate,signed_upload_date)  VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", time.Now(), team_id, event_id, waiver_id, v, "", false, false, nil).Exec()
				fmt.Println(err1)
				_, err2 := o.Raw("SELECT fk_member_id, waiver_signed_date, waiver_send_date FROM waivers WHERE fk_team_waiver_id = ? AND fk_member_id = ?", waiver_id, v).Values(&maps)
				fmt.Println(err2)
				member_id := maps[0]["fk_member_id"].(string)

				//waiver_signed_date := maps[0]["waiver_signed_date"]
				waiver_send_date := maps[0]["waiver_send_date"]

				user_details := GetUserDetails(member_id)
				without_login_member_id := user_details["user"]["fk_user_id"].(string)
				member_name := user_details["user"]["name"].(string)
				email_address := user_details["user"]["email_address"].(string)
				send_instant_emails := user_details["user"]["instant_emails"].(string)
				//send_email_digest := receiver_details["user"]["subscribe"]

				fmt.Println("Waiver send date")
				fmt.Println(waiver_send_date)
				//digest_mesg := member_name + " has asked you to sign a waiver for event " + event_name

				receiver := "{" + member_id + "}"

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, receiver).Values(&maps)

				if err == nil {
					status = "true"

					if send_instant_emails == "1" {
						subject := "Online Waiver Request, " + event_name

						waiver_link := "<a href='" + site_url + "emailwaiver/" + event_slug + "/" + team_slug + "/" + without_login_member_id + "/" + from + "'>HERE</a>"
						email_address := email_address
						fmt.Println(waiver_link)

						email_html_message := strings.Replace(constants.SEND_EVENT_TEAM_WAIVER_REMINDER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
						html_email_message := "<html> Hi " + member_name + ", <br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						//fmt.Printf("html_email_message \n")
						//fmt.Printf(html_email_message)
						common.SendEmail(email_address, subject, html_email_message)
					}

				} else {
					fmt.Println("Notification failed")
				}

			} else {

				if maps[0]["waiver_send_date"] != nil && maps[0]["waiver_signed_date"] == nil {
					_, err := o.Raw("UPDATE waivers SET waiver_send_date = ? WHERE fk_team_waiver_id = ? AND fk_member_id = ?", time.Now(), waiver_id, v).Values(&values1)
					fmt.Println(err)
					member_id := "{" + v + "}"
					user_details := GetUserDetails(v)
					without_login_member_id := user_details["user"]["fk_user_id"].(string)
					member_name := user_details["user"]["name"].(string)
					email_address := user_details["user"]["email_address"].(string)
					send_instant_emails := user_details["user"]["instant_emails"].(string)

					count, err := o.Raw("UPDATE notification_list SET date = ? WHERE fk_team_id = ? and fk_event_id = ? and fk_receiver = ? and (notification_desc = ? OR notification_desc like ? OR notification_desc like ? ) and active = 1 RETURNING id", time.Now(), team_id, event_id, member_id, desc, "Please accept your%", "Please accept electronic waiver for%").Values(&maps)

					if count == 0 {

						_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, member_id).Values(&maps)

					}

					if err == nil {
						status = "true"
						fmt.Println("Notification updated")

					} else {
						fmt.Println("Notification failed")
					}

					if send_instant_emails == "1" {
						subject := "Online Waiver Request, " + event_name
						//waiver_link := "<a href='" + site_url + "waiver-all'>HERE</a>"
						waiver_link := "<a href='" + site_url + "emailwaiver/" + event_slug + "/" + team_slug + "/" + without_login_member_id + "/" + from + "'>HERE</a>"
						fmt.Println(waiver_link)
						email_address := email_address

						email_html_message := strings.Replace(constants.SEND_EVENT_TEAM_WAIVER_REMINDER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html> Hi " + member_name + ", <br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						common.SendEmail(email_address, subject, html_email_message)
					}
				} else if maps[0]["waiver_send_date"] == nil && maps[0]["waiver_signed_date"] == nil {
					_, _ = o.Raw("UPDATE waivers SET waiver_send_date = ? WHERE fk_team_waiver_id = ? AND fk_member_id = ?", time.Now(), waiver_id, v).Values(&values)

					member_id := "{" + v + "}"
					user_details := GetUserDetails(v)
					member_name := user_details["user"]["name"].(string)
					without_login_member_id := user_details["user"]["fk_user_id"].(string)
					email_address := user_details["user"]["email_address"].(string)
					send_instant_emails := user_details["user"]["instant_emails"].(string)

					count, err := o.Raw("UPDATE notification_list SET date = ? WHERE fk_team_id = ? and fk_event_id = ? and fk_receiver = ? and (notification_desc = ? OR notification_desc like ? OR notification_desc like ? ) and active = 1 RETURNING id", time.Now(), team_id, event_id, member_id, desc, "Please accept your%", "Please accept electronic waiver for%").Values(&maps)

					if count == 0 {

						_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "3", team_id, event_id, user_id, member_id).Values(&maps)

					}

					if err == nil {
						status = "true"
						fmt.Println("Notification updated")

					} else {
						fmt.Println("Notification failed")
					}

					if send_instant_emails == "1" {
						subject := "Online Waiver Request, " + event_name
						//waiver_link := "<a href='" + site_url + "waiver-all'>HERE</a>"
						waiver_link := "<a href='" + site_url + "emailwaiver/" + event_slug + "/" + team_slug + "/" + without_login_member_id + "/" + from + "'>HERE</a>"
						fmt.Println(waiver_link)
						email_address := email_address

						email_html_message := strings.Replace(constants.SEND_EVENT_TEAM_WAIVER_REMINDER_INSTANT_EMAIL, "#siteUrl", site_url, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverName", waiver_name, -1)
						email_html_message = strings.Replace(email_html_message, "#waiverLink", waiver_link, -1)
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html> Hi " + member_name + ", <br/>" + email_html_message + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"

						common.SendEmail(email_address, subject, html_email_message)
					}

				} else {
					fmt.Println("Waiver already signed")
				}

			}
		}
	}

	return status
}
