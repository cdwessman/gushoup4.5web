package models

import (
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"gushou/smscommon"
	"os"
	"os/exec"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
	"unsafe"

	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"github.com/elgs/gostrgen"
	_ "golang.org/x/crypto/bcrypt"
)

type smsResponseStatus struct {
	EmailAddress             string `json:"email_address"`
	DisplayName              string `json:"display_name"`
	SmsStatus                string `json:"sms_status"`
	MobileNumberWithDialCode string `json:"mobile_number_dial_code"`
	InstantSms               string `json:"instant_sms"`
	UserSMSError             string `json:"user_sms_error"`
}

type CommunicationResponse struct {
	Status                     string              `json:"status"`
	AvailableSMSCount          string              `json:"available_sms_count"`
	DefaultSmsAvailableForTeam string              `json:"default_sms_available_for_team"`
	SmsResponseStatus          []smsResponseStatus `json:"sms_ersponse_status"`
}

type messageServiceData struct {
	FriendlyName string `json:"friendly_name"`
	AccountSid   string `json:"account_sid"`
	Sid          string `json:"sid"`
}

type inboundMessage struct {
	MessageSid         string `json:"sid"`
	To                 string `json:"to"`
	From               string `json:"from"`
	MessagingServiceID string `json:"messaging_service_sid"`
	MsgBody            string `json:"body"`
	Status             string `json:"status"`
	Direction          string `json:"direction"`
	MsgDateSent        string `json:"date_sent"`
}

type messageData struct {
	MessageList []inboundMessage `json:"messages"`
}

type userSmsResponse struct {
	TeamMemberID   string `json:"team_member_id"`
	TeamPracticeID string `json:"team_practice_id"`
	MsgBody        string `json:"body"`
	MsgDateSent    string `json:"date_sent"`
}

type FromNumber struct {
	fromnumber string
	phonesid   string
}

//Get all teams of a captain
func GetTeam(id int, limit bool) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params

	var sql string

	if limit == true {
		sql = "SELECT * FROM team WHERE active = 1 AND fk_co_captains @> ARRAY [?]::bigint[] LIMIT 6"
	} else {
		sql = "SELECT * FROM team WHERE active = 1 AND fk_co_captains @> ARRAY [?]::bigint[]"
	}

	num, _ := o.Raw(sql, id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)

			num, _ := o.Raw("SELECT * FROM teamjoin WHERE active = ? AND fk_member_id = ? AND fk_team_id = ?", 1, id, v["id"]).Values(&maps)

			if num > 0 {

				count, _ := o.Raw("SELECT * FROM teamjoin tj INNER JOIN team t on tj.fk_team_id = t.id INNER JOIN users us on tj.fk_member_id = us.id WHERE tj.active = ? AND request_status = ? AND fk_team_id = ?", "1", "2", v["id"]).Values(&values)

				v["members"] = count

				count, _ = o.Raw("SELECT * FROM team_logo_url WHERE fk_team_id = ? ", v["id"]).Values(&values)
				if count > 0 {
					v["logo_url"] = values[0]["path"]
				}

				num, _ := o.Raw("SELECT div FROM team_class_divisions WHERE fk_team_id = ? ", v["id"]).Values(&values)

				if num > 0 {
					div_string := ""

					for _, x := range values {
						s := strings.Replace(x["div"].(string), "{", "", -1)
						s = strings.Replace(s, "}", "", -1)
						div_string = div_string + s + ","

					}
					var div_arr []string
					div_arr = strings.Split(div_string, ",")
					div_arr = RemoveDuplicates(div_arr)

					v["divs"] = div_arr
				}

				result[key] = v
			}

		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetTeamLogo(id string) string {

	result := ""

	fmt.Println("Team id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT path FROM team_logo_url WHERE fk_team_id = ? ", id).Values(&maps)
	if num > 0 {
		result = maps[0]["path"].(string)

	} else {
		fmt.Println("No teamm Logo")
		result = ""
	}

	//fmt.Println(result)

	return result

}

func GetTeamBySlug(slug string, loginStatus bool, userId string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Slug ", slug)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM team WHERE slug = ? ", slug).Values(&maps)
	if num > 0 {
		returnStatus := checkFreeTrailValid(maps, maps[0]["id"].(string))
		if returnStatus {
			_, _ = o.Raw("SELECT * FROM team WHERE slug = ? ", slug).Values(&maps)
		}
		for k, v := range maps {
			key := "team_" + strconv.Itoa(k)
			result[key] = v
		}

		num, _ := o.Raw("SELECT * FROM team_class_divisions WHERE fk_team_id = ? ", maps[0]["id"]).Values(&maps)
		if num > 0 {
			for k, v := range maps {
				key := "class_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			fmt.Println("No class n divisions")
		}

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

/*
Below code used for sorting practices by date and time
reference: https://stackoverflow.com/questions/23121026/sorting-by-time-time-in-golang
*/
type practice_data struct {
	practice_id string
	time        time.Time
}

type timeSlice []practice_data

func (p timeSlice) Len() int {
	return len(p)
}

func (p timeSlice) Less(i, j int) bool {
	return p[i].time.Before(p[j].time)
}

func (p timeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func GetUserPractices(user_id int, loginUserTimeZone string) map[string][]orm.Params {

	result := make(map[string][]orm.Params)
	var all_teams []string

	var all_dates []string

	fmt.Println("User id ", user_id)

	o := orm.NewOrm()

	var maps []orm.Params
	var team_map []orm.Params
	var sorted_map []string
	//var name []orm.Params

	num, _ := o.Raw("SELECT fk_team_id FROM teamjoin WHERE fk_member_id = ? AND active = 1 AND request_status = 2", user_id).Values(&maps)
	if num > 0 {

		for _, v := range maps {
			/*key := "practice_" + strconv.Itoa(k)
			num, _ = o.Raw("SELECT *, to_char(practice_date, 'Dy') as week_day, to_char(practice_date, 'Mon DD') as month_day FROM team_practices WHERE active = ? AND fk_team_id = ? order by practice_date", "1", v["fk_team_id"]).Values(&team_map)
			if num > 0 {

				_,_ = o.Raw("SELECT team_name FROM team WHERE id = ? ", v["fk_team_id"]).Values(&name)

				logo_url := GetTeamLogo(v["fk_team_id"].(string))

				for _, j := range team_map {
					j["team_name"] = name[0]["team_name"]
					if logo_url != "" {
						j["logo_url"] = logo_url
					}

				}
				result[key] = team_map
			}*/

			all_teams = append(all_teams, v["fk_team_id"].(string))
		}

		all_teams_string := strings.Join(all_teams[:], ",")
		fmt.Println("AAL TEAMS ID ", all_teams_string)

		num, err := o.Raw("SELECT team.team_name, team.slug , team_practices.*, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as week_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE'"+loginUserTimeZone+"'), 'Mon DD') as month_day FROM team_practices inner join team on team_practices.fk_team_id = team.id WHERE team_practices.active = ? AND team_practices.fk_team_id IN ( "+all_teams_string+" ) order by practice_date", "1").Values(&team_map)
		fmt.Println("err", err)
		if num > 0 {
			for _, j := range team_map {

				//key := "practice_" + strconv.Itoa(k)
				// logo_url := GetTeamLogo(j["fk_team_id"].(string))
				// if logo_url != "" {
				// 	j["logo_url"] = logo_url
				// }

				all_dates = append(all_dates, j["practice_date"].(string))

			}

			//result["practices"] = team_map

			for k := 0; k < len(team_map); k++ {

				start, end := GetStartEnd(all_dates, team_map[k]["practice_date"].(string))

				//fmt.Println("Practice dates ",all_dates, start, end, team_map[k]["practice_date"].(string))

				if start == end {
					sorted_map = append(sorted_map, team_map[k]["id"].(string))
				} else {
					to_sort := team_map[start : end+1]
					var practice_data_map = make(map[int]practice_data)

					for p, q := range to_sort {
						layout := "3:04 PM"
						str := q["practice_time"].(string)
						t, err := time.Parse(layout, str)
						if err != nil {
							fmt.Println(err)
						} else {
							//fmt.Println(t)
							practice_data_map[p] = practice_data{time: t, practice_id: q["id"].(string)}
						}
					}

					time_sorted_practice := make(timeSlice, 0, len(practice_data_map))
					for _, d := range practice_data_map {
						time_sorted_practice = append(time_sorted_practice, d)
					}

					sort.Sort(time_sorted_practice)
					//fmt.Println(" SORTED ")

					fmt.Println(time_sorted_practice)

					for _, z := range time_sorted_practice {
						sorted_map = append(sorted_map, z.practice_id)
					}

				}

				k = end + (k - k)

			}

			userMobileNo := ""
			var usersMaps []orm.Params
			num, err := o.Raw("SELECT *, (up.country_dial_code || '' || up.phone) as user_mobile_no FROM users join user_profile up on users.id= up.fk_user_id WHERE users.id = ? ", user_id).Values(&usersMaps)
			fmt.Println("errnum", err)
			if num > 0 {
				if usersMaps[0]["user_mobile_no"] != nil {
					userMobileNo = usersMaps[0]["user_mobile_no"].(string)
				}
			}

			for _, practice_id := range sorted_map {

				attendenceUpdateMap := make(map[string][]userSmsResponse)
				num, _ = o.Raw("SELECT team.team_name , team.slug, team_practices.*,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'MM')  as tempmonth,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'WW')  as tempweek_number,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'YYYY')  as tempyear,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'YYYY-MM-DD') as twiliodate, to_char((practices_end_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as end_week_day, to_char((practices_end_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Mon DD') as end_month_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as week_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Mon DD') as month_day FROM team_practices inner join team on team_practices.fk_team_id = team.id WHERE team_practices.active = ? AND team_practices.id = ?", "1", practice_id).Values(&team_map)

				logo_url := GetTeamLogo(team_map[0]["fk_team_id"].(string))
				if logo_url != "" {
					team_map[0]["logo_url"] = logo_url
				}

				maps = append(maps, team_map[0])

				user_time_zone := ""

				if team_map[0]["user_time_zone"] != nil {
					user_time_zone = team_map[0]["user_time_zone"].(string)
				}

				// IsFeaturePracticeOrNot := common.IsFeaturePracticeOrNotTwilio(team_map[0]["practice_date"].(string), user_time_zone)
				if false {
					practiceDateData := ""
					if team_map[0]["practice_date"] != nil {
						practiceDateData = team_map[0]["practice_date"].(string)
					}
					twilioEndDate := ""
					twilioStartDate := ""
					if team_map[0]["twiliodate"] != nil && team_map[0]["twiliodate"] != "" {
						twilioEndDate = team_map[0]["twiliodate"].(string)
						layout := "2006-01-02"
						then, _ := time.Parse(layout, twilioEndDate)
						startDate := then.AddDate(0, -6, 0)
						twilioStartDate = startDate.Format(layout)
					}

					viewMessageListData := twilioReadSmsLog(twilioEndDate, twilioStartDate)

					teamMemberID := strconv.Itoa(user_id)
					teamPracticeID := team_map[0]["id"].(string)
					practiceMessageService := team_map[0]["message_service_uniqueid"]

					if practiceMessageService != nil {
						practiceMessageService = practiceMessageService.(string)
					} else {
						practiceMessageService = ""
					}
					if practiceMessageService != "" {

						msgList := viewMessageListData.MessageList
						if len(msgList) > 0 {
							attendenceUpdateMap = attendenceMarkUpCode(msgList, practiceMessageService.(string), userMobileNo, teamPracticeID, teamMemberID, attendenceUpdateMap, practiceDateData, user_time_zone)
							if len(attendenceUpdateMap) > 0 {
								attendenceUpdateMaking(attendenceUpdateMap)
							}

						}
					}

				}
			}

			result["practices"] = maps

		}

	} else {
		fmt.Println("No practices")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

/* Input : [2017-03-16T00:00:00+05:30 2017-03-17T00:00:00+05:30 2017-03-17T00:00:00+05:30 2017-03-18T00:00:00+05:30 2017-04-02T00:00:00+05:30 2017-04-02T00:00:00+05:30 2017-04-09T00:00:00+05:30 2017-04-09T00:00:00+05:30 2017-04-16T00:00:00+05:30 2017-04-16T00:00:00+05:30 2017-04-23T00:00:00+05:30 2017-04-23T00:00:00+05:30 2017-04-30T00:00:00+05:30 2017-04-30T00:00:00+05:30 2017-05-05T00:00:00+05:30], 2017-04-02T00:00:00+05:30

Output: 4, 5
*/
func GetStartEnd(all_dates []string, date string) (int, int) {

	var start = 0
	var end int
	var count = 0
	var start_found = false
	for k, v := range all_dates {

		if start_found == false {
			if v == date {
				start = k
				start_found = true
			}
		} else {
			if v == date {
				count = count + 1
			}
		}

	}

	end = start + count

	return start, end

}

func GetTeamPractices(team_id string, loginUserTimeZone string) []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Team id ", team_id)

	o := orm.NewOrm()

	var team_map []orm.Params
	var maps []orm.Params
	var all_dates []string
	var sorted_map []string

	num, _ := o.Raw("SELECT *, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as week_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Mon DD') as month_day FROM team_practices WHERE active = ? AND fk_team_id = ? order by practice_date", "1", team_id).Values(&team_map)

	if num > 0 {

		for _, j := range team_map {

			all_dates = append(all_dates, j["practice_date"].(string))

		}

		//result["practices"] = team_map

		for k := 0; k < len(team_map); k++ {

			start, end := GetStartEnd(all_dates, team_map[k]["practice_date"].(string))

			//fmt.Println("Practice dates ",all_dates, start, end, team_map[k]["practice_date"].(string))

			if start == end {
				sorted_map = append(sorted_map, team_map[k]["id"].(string))
			} else {
				to_sort := team_map[start : end+1]
				var practice_data_map = make(map[int]practice_data)

				for p, q := range to_sort {
					layout := "3:04 PM"
					str := q["practice_time"].(string)
					t, err := time.Parse(layout, str)
					if err != nil {
						fmt.Println(err)
					} else {
						//fmt.Println(t)
						practice_data_map[p] = practice_data{time: t, practice_id: q["id"].(string)}
					}
				}

				time_sorted_practice := make(timeSlice, 0, len(practice_data_map))
				for _, d := range practice_data_map {
					time_sorted_practice = append(time_sorted_practice, d)
				}

				sort.Sort(time_sorted_practice)
				//fmt.Println(" SORTED ")

				fmt.Println(time_sorted_practice)

				for _, z := range time_sorted_practice {
					sorted_map = append(sorted_map, z.practice_id)
				}

			}

			k = end + (k - k)

		}

		fmt.Println("SORTED Practice id ", sorted_map)

		for _, practice_id := range sorted_map {
			num, _ = o.Raw("SELECT t.team_name , tp.*,to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'MM')  as tempmonth, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'WW')  as tempweek_number, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'YYYY')  as temyear, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as week_day, to_char((practices_end_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Dy') as end_week_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Mon DD') as month_day, to_char((practices_end_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'Mon DD') as end_month_day, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+loginUserTimeZone+"'), 'YYYY-MM-DD') as twiliodate, (SELECT count(pa.id) FROM practice_attendees pa WHERE pa.fk_practice_id = tp.id AND pa.response = 'attending') as attendees FROM team_practices tp inner join team t on tp.fk_team_id = t.id WHERE tp.active = ? AND tp.id = ?", "1", practice_id).Values(&team_map)
			//fmt.Println("twilioDate", team_map[0]["twiliodate"].(string))
			logo_url := GetTeamLogo(team_map[0]["fk_team_id"].(string))
			if logo_url != "" {
				team_map[0]["logo_url"] = logo_url
			}

			// twilioEndDate := ""
			// twilioStartDate := ""
			// if team_map[0]["twiliodate"] != nil && team_map[0]["twiliodate"] != "" {
			// 	twilioEndDate = team_map[0]["twiliodate"].(string)
			// 	layout := "2006-01-02"
			// 	then, _ := time.Parse(layout, twilioEndDate)
			// 	startDate := then.AddDate(0, -6, 0)
			// 	twilioStartDate = startDate.Format(layout)
			// }

			// practiceDateData := ""
			// if team_map[0]["practice_date"] != nil {
			// 	practiceDateData = team_map[0]["practice_date"].(string)
			// }
			// user_time_zone := ""

			// if team_map[0]["user_time_zone"] != nil {
			// 	user_time_zone = team_map[0]["user_time_zone"].(string)
			// }
			// IsFeaturePracticeOrNot := common.IsFeaturePracticeOrNotTwilio(team_map[0]["practice_date"].(string), user_time_zone)
			// if IsFeaturePracticeOrNot {
			// 	// get the users based on practice to mark the attendence by the SMS response
			// 	_, _ = o.Raw("SELECT tp.id as team_practice_id,tp.fk_team_id,tp.message_service_uniqueid,tj.fk_member_id,tj.email,(up.country_dial_code || '' || up.phone) as user_mobile_no FROM team_practices tp INNER JOIN teamjoin tj ON tp.fk_team_id = tj.fk_team_id INNER JOIN user_profile up ON up.fk_user_id = tj.fk_member_id WHERE tj.active = ? AND tj.request_status = ? AND tp.id = ?", "1", "2", practice_id).Values(&team_practice_mark_attendance)
			// 	attendenceUpdateMap := make(map[string][]userSmsResponse)

			// 	viewMessageListData := twilioReadSmsLog(twilioEndDate, twilioStartDate)

			// 	for tpmaKey, _ := range team_practice_mark_attendance {

			// 		teamMemberID := team_practice_mark_attendance[tpmaKey]["fk_member_id"].(string)
			// 		teamPracticeID := team_practice_mark_attendance[tpmaKey]["team_practice_id"].(string)
			// 		userMobileNo := ""
			// 		if team_practice_mark_attendance[tpmaKey]["user_mobile_no"] != nil {
			// 			userMobileNo = team_practice_mark_attendance[tpmaKey]["user_mobile_no"].(string)
			// 		}
			// 		practiceMessageService := team_practice_mark_attendance[tpmaKey]["message_service_uniqueid"]

			// 		if practiceMessageService != nil {
			// 			practiceMessageService = practiceMessageService.(string)
			// 		} else {
			// 			practiceMessageService = ""
			// 		}
			// 		if practiceMessageService != "" {

			// 			msgList := viewMessageListData.MessageList
			// 			if len(msgList) > 0 {
			// 				attendenceUpdateMap = attendenceMarkUpCode(msgList, practiceMessageService.(string), userMobileNo, teamPracticeID, teamMemberID, attendenceUpdateMap, practiceDateData, user_time_zone)
			// 				if len(attendenceUpdateMap) > 0 {
			// 					attendenceUpdateMaking(attendenceUpdateMap)
			// 				}
			// 			}
			// 		}

			// 	}
			// }

			maps = append(maps, team_map[0])
		}

		result = maps

	} else {
		fmt.Println("No practices")
		result = append(result, nil)
	}

	return result

}

func GetTeamPartOf(id int, limit bool) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("User id ", id)

	o := orm.NewOrm()

	var maps []orm.Params
	var team_map []orm.Params
	var values []orm.Params
	var sql string

	if limit == true {
		sql = "SELECT fk_team_id FROM teamjoin WHERE active = ? AND fk_member_id = ? AND request_status = ? LIMIT 6"
	} else {
		sql = "SELECT fk_team_id FROM teamjoin WHERE active = ? AND fk_member_id = ? AND request_status = ?"
	}

	num, _ := o.Raw(sql, "1", id, "2").Values(&maps)
	if num > 0 {
		teamPartIndex := 0
		for _, v := range maps {

			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			num, _ = o.Raw("SELECT * FROM team WHERE active = 1 AND id = ? AND NOT (fk_co_captains @> ARRAY [?]::bigint[] )", v["fk_team_id"], id).Values(&team_map)
			if num > 0 {
				count, _ := o.Raw("SELECT * FROM teamjoin tj INNER JOIN team t on tj.fk_team_id = t.id INNER JOIN users us on tj.fk_member_id = us.id WHERE tj.active = ? AND request_status = ? AND fk_team_id = ?", "1", "2", v["fk_team_id"]).Values(&values)

				team_map[0]["members"] = count

				count, _ = o.Raw("SELECT * FROM team_logo_url WHERE fk_team_id = ? ", v["fk_team_id"]).Values(&values)
				if count > 0 {
					team_map[0]["logo_url"] = values[0]["path"]
				}

				num, _ := o.Raw("SELECT div FROM team_class_divisions WHERE fk_team_id = ? ", v["fk_team_id"]).Values(&values)

				if num > 0 {
					div_string := ""

					for _, x := range values {
						s := strings.Replace(x["div"].(string), "{", "", -1)
						s = strings.Replace(s, "}", "", -1)
						div_string = div_string + s + ","

					}
					var div_arr []string
					div_arr = strings.Split(div_string, ",")
					div_arr = RemoveDuplicates(div_arr)

					team_map[0]["divs"] = div_arr
				}
				result[strconv.Itoa(teamPartIndex)] = team_map[0]
				teamPartIndex++
			}

		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetAllMyTeams(id int, limit bool) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("User id ", id)

	o := orm.NewOrm()

	var maps []orm.Params
	var team_map []orm.Params
	var values []orm.Params
	var sql string

	if limit == true {
		sql = "SELECT fk_team_id FROM teamjoin WHERE active = ? AND fk_member_id = ? AND request_status = ? LIMIT 6"
	} else {
		sql = "SELECT fk_team_id FROM teamjoin WHERE active = ? AND fk_member_id = ? AND request_status = ? "
	}

	num, _ := o.Raw(sql, "1", id, "2").Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)
			num, _ = o.Raw("SELECT * FROM team WHERE active = 1 AND id = ?", v["fk_team_id"]).Values(&team_map)
			if num > 0 {
				count, _ := o.Raw("SELECT * FROM teamjoin tj INNER JOIN team t on tj.fk_team_id = t.id INNER JOIN users us on tj.fk_member_id = us.id WHERE tj.active = ? AND request_status = ? AND fk_team_id = ?", "1", "2", v["fk_team_id"]).Values(&values)

				team_map[0]["members"] = count

				count, _ = o.Raw("SELECT * FROM team_logo_url WHERE fk_team_id = ? ", v["fk_team_id"]).Values(&values)
				if count > 0 {
					team_map[0]["logo_url"] = values[0]["path"]
				}

				num, _ := o.Raw("SELECT div FROM team_class_divisions WHERE fk_team_id = ? ", v["fk_team_id"]).Values(&values)

				if num > 0 {
					div_string := ""

					for _, x := range values {
						s := strings.Replace(x["div"].(string), "{", "", -1)
						s = strings.Replace(s, "}", "", -1)
						div_string = div_string + s + ","

					}
					var div_arr []string
					div_arr = strings.Split(div_string, ",")
					div_arr = RemoveDuplicates(div_arr)

					team_map[0]["divs"] = div_arr
				}

				result[key] = team_map[0]
			}

		}

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

//Working on sort
type TeamMember struct {
	name string `json:"name"`
	//joinedDate time.Time
}
type AllTeamMembers []TeamMember

// The number of the elements in the collection
func (pc AllTeamMembers) Len() int {
	return len(pc)
}

func (pbn memberByName) Less(i, j int) bool {
	return pbn.AllTeamMembers[i].name < pbn.AllTeamMembers[j].name
}

func (pc AllTeamMembers) Swap(i, j int) {
	pc[i], pc[j] = pc[j], pc[i]
}

type memberByName struct {
	AllTeamMembers
}

func MemberByName(data AllTeamMembers) memberByName {
	return memberByName{data}
}

/*func GetAllTeamMembers(id string) AllTeamMembers {

	fmt.Println("Team id ", id)

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT fk_member_id, to_char(accepted_date, 'DD Mon YYYY') as accepted_date, t.team_name, t.slug, up.* , us.email_address "+
					"FROM teamjoin tj "+
					"INNER JOIN team t on tj.fk_team_id = t.id "+
					"INNER JOIN users us on tj.fk_member_id = us.id "+
					"INNER JOIN user_profile up on us.id = up.fk_user_id "+
					"WHERE tj.active = 1 AND request_status = 2 AND fk_team_id = ? "+
					"ORDER BY accepted_date desc ", id).Values(&maps)
	fmt.Println(maps)
	//
	result := make(AllTeamMembers, 0, len(maps))
	fmt.Println(result)
	sort.Sort(memberByName(result))

	fmt.Println(result)

	return result

}*/

//End of sort

func GetTeamMembers(id string) []orm.Params {

	var result []orm.Params

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT fk_member_id, to_char(accepted_date, 'DD Mon YYYY') as accepted_date_format, t.team_name, t.slug, up.* , date_part('year',age(dob)) as age, us.email_address "+
		"FROM teamjoin tj "+
		"INNER JOIN team t on tj.fk_team_id = t.id "+
		"INNER JOIN users us on tj.fk_member_id = us.id "+
		"INNER JOIN user_profile up on us.id = up.fk_user_id "+
		"WHERE tj.active = 1 AND request_status = 2 AND fk_team_id = ? "+
		"ORDER BY lower(name) ", id).Values(&maps)
	if num > 0 {
		for _, v := range maps {
			result = append(result, v)
		}

	} else {
		result = append(result, nil)
	}
	return result
}

func GetTeamMembersAlpha(id string) []orm.Params {

	var result []orm.Params
	fmt.Println("Team id ", id)

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT fk_member_id, to_char(accepted_date, 'DD Mon YYYY') as accepted_date_format, t.team_name, t.slug, up.* , us.email_address "+
		"FROM teamjoin tj "+
		"INNER JOIN team t on tj.fk_team_id = t.id "+
		"INNER JOIN users us on tj.fk_member_id = us.id "+
		"INNER JOIN user_profile up on us.id = up.fk_user_id "+
		"WHERE tj.active = 1 AND request_status = 2 AND fk_team_id = ? "+
		"ORDER BY lower(name) ", id).Values(&maps)
	if num > 0 {
		result = maps

	} else {
		fmt.Println("No members")
		result = append(result, nil)
	}
	return result
}

func GetTeamPendingReq(id string) []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Team id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	//var team_map []orm.Params

	//num, _ := o.Raw("SELECT fk_member_id, to_char(date, 'DD Mon YYYY') as date_1 FROM teamjoin WHERE request_status = 1 AND active = 1 AND fk_team_id = ? ", id).Values(&maps)
	num, _ := o.Raw("select fk_member_id, to_char(date, 'DD Mon YYYY') as first_request_date, email, name, last_name, files, us.id from teamjoin tj "+
		"left join users us on tj.fk_member_id = us.id "+
		"left join user_profile up on us.id = up.fk_user_id "+
		"where fk_team_id = ? and request_status = 1 and tj.active = 1 ORDER BY date DESC", id).Values(&maps)

	if num > 0 {
		for _, v := range maps {
			//key := "member_" + strconv.Itoa(k)
			//fmt.Println("member id ", v["fk_member_id"])
			//fmt.Println("valus", v)
			//num, _ = o.Raw("SELECT * FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.id = ? ", v["fk_member_id"]).Values(&team_map)
			//if num > 0 {

			//maps[0]["first_request_date"] = maps[0]["date_1"]
			result = append(result, v)
			//}

		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No Pending Requests")
		result = append(result, nil)
	}

	//fmt.Println(result)

	return result

}

func GetTeamPendingReqAlpha(id string) []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Team id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	//var team_map []orm.Params

	//num, _ := o.Raw("SELECT fk_member_id, to_char(date, 'DD Mon YYYY') as date_1 FROM teamjoin WHERE request_status = 1 AND active = 1 AND fk_team_id = ? ", id).Values(&maps)
	num, _ := o.Raw("select fk_member_id, to_char(date, 'DD Mon YYYY') as first_request_date, email, name, last_name, files, us.id from teamjoin tj "+
		"left join users us on tj.fk_member_id = us.id "+
		"left join user_profile up on us.id = up.fk_user_id "+
		"where fk_team_id = ? and request_status = 1 and tj.active = 1 ORDER BY lower(name) ", id).Values(&maps)

	if num > 0 {

		result = maps
	} else {
		fmt.Println("No Pending Requests")
		result = append(result, nil)
	}

	//fmt.Println(result)

	return result

}

func GetTeamDiscussion(id string, notify_type []string, limit int) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Team id ", id)
	fmt.Println(" Notify type ", notify_type)
	//notify_type_string := "(" + strings.Join(notify_type,",") + ")"

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(created_at, 'Dy Mon DD YYYY, HH:MI am') as date_format FROM team_discussion WHERE fk_team_id = ? AND notify_type IN (?,?,?) order by created_at DESC LIMIT ?", id, notify_type, limit).Values(&maps)
	fmt.Println("Dis ", num)
	if num > 0 {
		for k, v := range maps {
			user_details := GetUserDetails(v["fk_user_id"].(string))
			user_name := user_details["user"]["name"].(string)
			img_url := ""
			if user_details["user"]["files"] != nil {
				img_url = user_details["user"]["files"].(string)
			}
			//key = key + " " + user_details["user"]["last_name"].(string) + "_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			key := strconv.Itoa(k)
			v["user_name"] = user_name
			v["img_url"] = img_url
			result[key] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No discussion")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetTotalTeamDiscussion(id string, notify_type []string, limit int) bool {

	o := orm.NewOrm()

	var maps []orm.Params

	moreResult := false
	_, _ = o.Raw("SELECT Count(*) FROM team_discussion WHERE fk_team_id = ? AND notify_type IN (?,?,?)", id, notify_type).Values(&maps)
	count, _ := strconv.Atoi(maps[0]["count"].(string))

	if count > limit {
		moreResult = true
	}

	return moreResult

}

func GetDiscussionReply(id string, limit string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Discussion id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(created_at, 'Dy Mon DD YYYY, HH:MI am') as date_format FROM replydiscussion WHERE fk_discussion_id = ? order by created_at DESC LIMIT ?", id, limit).Values(&maps)
	fmt.Println("Replies ", num)
	if num > 0 {

		maps = ReverseArray(maps)

		for k, v := range maps {
			user_details := GetUserDetails(v["fk_user_id"].(string))
			user_name := user_details["user"]["name"].(string)
			img_url := ""
			if user_details["user"]["files"] != nil {
				img_url = user_details["user"]["files"].(string)
			}
			//key = key + " " + user_details["user"]["last_name"].(string) + "_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			key := strconv.Itoa(k)
			v["user_name"] = user_name
			v["img_url"] = img_url
			result[key] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No replies")
		//result["status"] = nil
	}

	fmt.Println(result)

	return result

}

func GetTotalDiscussionReply(id string, limit string) bool {

	o := orm.NewOrm()

	var maps []orm.Params

	moreResult := false
	_, _ = o.Raw("SELECT Count(*) FROM replydiscussion WHERE fk_discussion_id = ?", id).Values(&maps)
	count, _ := strconv.Atoi(maps[0]["count"].(string))
	limitInt, _ := strconv.Atoi(limit)

	if count > limitInt {
		moreResult = true
	}

	return moreResult

}

func CreateTeam(data map[string]string, user_id int, email_address string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	fmt.Println("CompleteUserProfile")
	fmt.Println(data)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params
	var values1 []orm.Params

	co_captain := "{" + strconv.Itoa(user_id) + "}"
	var lat *string
	var lng *string
	var makePublic *bool
	lat, lng, makePublic = nil, nil, nil

	if data["Latitude"] != "" {
		t := data["Latitude"]
		lat = &t
	}
	if data["Longitude"] != "" {
		t := data["Longitude"]
		lng = &t
	}

	if data["TeamMakePublic"] == "on" {
		t := true
		makePublic = &t
	} else {
		t := false
		makePublic = &t
	}

	_, err := o.Raw("INSERT INTO team (active, captain_id, classes, divisions, email, facebook_page_url, latitude, longitude, phone_number, recruit, slug, sponsors, team_country, team_description, team_from, team_name, team_province, twitter_handle, website_url, youtube_link, created_at, fk_captain_id, fk_co_captains, sms_limits, team_make_public) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", "", data["Classes"], data["Divisions"], data["TeamEmail"], data["TeamFacebook"], &lat, &lng, data["TeamPh"], "1", data["Slug"], data["SponsorName"], data["TeamCountry"], "", data["TeamLocation"], data["TeamName"], data["TeamProvince"], data["TeamTwitter"], data["TeamWebsite"], data["TeamYouTube"], time.Now(), user_id, co_captain, data["SmsLimits"], &makePublic).Values(&maps)
	//fmt.Println(err, maps[0]["id"])
	//result["user_id"] = maps[0]["id"].(string)

	if err == nil {
		//num, err = o.Raw("INSERT INTO user_profile (fk_user_id, activities) VALUES (?,?) RETURNING id", maps[0]["id"], activities).Values(&maps)
		//result["profile_id"] = maps[0]["id"].(string)

		if data["MTeamdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", maps[0]["id"], "M", data["MTeamdivision"]).Values(&values)
		}

		if data["WTeamdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", maps[0]["id"], "W", data["WTeamdivision"]).Values(&values)
		}
		if data["MXTeamdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", maps[0]["id"], "MX", data["MXTeamdivision"]).Values(&values)
		}

		num, _ := o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? AND fk_member_id = ? ", maps[0]["id"], user_id).Values(&values1)
		if num > 0 {
			_, err = o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, email = ?, date = ?, fk_inviters_id = ?  WHERE fk_team_id = ? AND fk_member_id = ? ", "2", "1", strings.ToLower(email_address), time.Now(), user_id, maps[0]["id"], user_id).Values(&values1)
		} else {
			_, err = o.Raw("INSERT INTO teamjoin (request_status, active, email, date, accepted_date, fk_team_id, fk_member_id, fk_inviters_id ) VALUES (?,?,?,?,?,?,?,?) RETURNING id", "2", "1", email_address, time.Now(), time.Now(), maps[0]["id"], user_id, user_id).Values(&maps)
		}

		if err == nil {
			result["status"] = "1"
			fmt.Println("Team created")
		}

	} else {
		fmt.Println("Cannot create team ", err)
	}

	return result

}

func SearchTeam(data map[string]string) (map[string]orm.Params, int64) {

	result := make(map[string]orm.Params)
	//result["status"] = "0"
	o := orm.NewOrm()

	var maps []orm.Params

	fmt.Println("Search by ", data)

	sql := "SELECT distinct(t.*) FROM team t join team_class_divisions tc on t.id = tc.fk_team_id WHERE active = 1 AND"
	sqlChanged := false

	if len(data) > 0 {
		if len(data["TeamName"]) > 0 {
			sql = sql + " lower(team_name) LIKE '%" + strings.ToLower(data["TeamName"]) + "%'"
			sqlChanged = true
		}

		if len(data["City"]) > 0 {
			if !sqlChanged {
				sql = sql + " lower(team_from) = '" + strings.ToLower(data["City"]) + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND lower(team_from) = '" + strings.ToLower(data["City"]) + "'"
			}

		}
		if len(data["Recruit"]) > 0 {
			if !sqlChanged {
				sql = sql + " recruit = '" + data["Recruit"] + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND recruit = '" + data["Recruit"] + "'"
			}
		}

		if len(data["Captain"]) > 0 {

			var captain_id []string
			captain_id_string := ""
			num, _ := o.Raw("SELECT fk_user_id FROM user_profile WHERE active = 1 AND lower(name) LIKE '%" + strings.ToLower(data["Captain"]) + "%' OR lower(last_name) LIKE '%" + strings.ToLower(data["Captain"]) + "%'").Values(&maps)

			if num > 0 {
				for _, v := range maps {
					captain_id = append(captain_id, v["fk_user_id"].(string))
				}

				captain_id_string = "(" + strings.Join(captain_id, ",") + ")"
			} else {
				captain_id_string = "(0)"
			}

			if !sqlChanged {
				sql = sql + " fk_captain_id IN " + captain_id_string
				sqlChanged = true
			} else {
				sql = sql + " AND fk_captain_id IN " + captain_id_string
			}
		}

		if len(data["Divisions"]) > 0 {
			if !sqlChanged {
				sql = sql + " tc.div @> '{" + data["Divisions"] + "}' :: varchar[]"
				sqlChanged = true
			} else {
				sql = sql + " AND tc.div @> '{" + data["Divisions"] + "}' :: varchar[]"
			}
		}

		if len(data["Classes"]) > 0 {
			if data["Classes"] == "any" {
				if !sqlChanged {
					sql = "SELECT * FROM team WHERE active = 1"
					sqlChanged = true
				} else {
					sql = sql
				}
			}
			if data["Classes"] == "all" {
				if !sqlChanged {
					sql = sql + " tc.class in ('W','MX','M') group by t.id having count(t.id) = 3"
					sqlChanged = true
				} else {
					sql = sql + " AND tc.class in ('W','MX','M') group by t.id having count(t.id) = 3"
				}

				sql = strings.Replace(sql, "distinct(t.*)", "t.id, t.*", -1)
			}
			if data["Classes"] == "M" {
				if !sqlChanged {
					sql = sql + " tc.class = 'M'"
					sqlChanged = true
				} else {
					sql = sql + " AND tc.class = 'M'"
				}
			}
			if data["Classes"] == "W" {
				if !sqlChanged {
					sql = sql + " tc.class = 'W'"
					sqlChanged = true
				} else {
					sql = sql + " AND tc.class = 'W'"
				}
			}
			if data["Classes"] == "MX" {
				if !sqlChanged {
					sql = sql + " tc.class = 'MX'"
					sqlChanged = true
				} else {
					sql = sql + " AND tc.class = 'MX'"
				}
			}

		}

		sql = sql + " order by id ASC LIMIT " + data["Limit"] + " OFFSET " + data["Offset"]

		if data["Type"] == "last" {
			sql = strings.Replace(sql, "ASC", "DESC", -1)
			sql = strings.TrimSuffix(sql, "OFFSET "+data["Offset"])
		}

	} else {
		sql = "SELECT * From team WHERE active = 1 LIMIT 10"
	}

	fmt.Println("SQL = " + sql)

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			/*if len(data["Divisions"]) > 0 {

			if v["divisions"] != nil {
				/*s := strings.Replace(v["divisions"].(string), "{", "", -1)
				s = strings.Replace(s, "}", "", -1)*/

			/*s := strings.Join(GetTeamDivision(v["id"].(string)),",")

					div_array := strings.Split(s, ",")
					search_div := strings.Split(data["Divisions"],",")
					for _, div := range search_div {

						if in_array(div, div_array) {
							key := "team_" + strconv.Itoa(k)
							result[key] = v
							break
						}
					}
				}


			} else {*/
			key := "team_" + strconv.Itoa(k)
			result[key] = v
			//}

		}
		//result["user_id"] = maps[0]["id"].(string)

		if len(result) == 0 {
			fmt.Println("No teams with searched divs")
			result["status"] = nil
		}

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)
	sql = strings.TrimSuffix(sql, "OFFSET "+data["Offset"])
	//fmt.Println(sql)

	if len(data["Limit"]) > 0 {
		sql = strings.Replace(sql, "LIMIT "+data["Limit"], "", -1)
	} else {
		sql = strings.Replace(sql, "LIMIT 10", "", -1)
	}

	//fmt.Println(sql)

	num, _ = o.Raw(sql).Values(&maps)

	/*var totalTeam []orm.Params

	if num > 0 {
		for _, v := range maps {
			if len(data["Divisions"]) > 0 {

				if v["divisions"] != nil {

					s := strings.Join(GetTeamDivision(v["id"].(string)),",")

					div_array := strings.Split(s, ",")
					search_div := strings.Split(data["Divisions"],",")
					for _, div := range search_div {

						if in_array(div, div_array) {
							totalTeam = append(totalTeam, v)
							break
						}
					}
				}


			} else {
				totalTeam = append(totalTeam, v)
			}

		}

		if len(totalTeam) == 0 {
			fmt.Println("No events with searched divs")
			totalTeam = append(totalTeam, nil)
		}

	}*/

	return result, num

}

func in_array(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}

func EditTeam(data map[string]string, team_id string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	fmt.Println("EditTeam")
	fmt.Println(data)

	o := orm.NewOrm()
	var maps []orm.Params

	var lat *string
	var lng *string
	var makePublic *bool
	lat, lng, makePublic = nil, nil, nil

	if data["Latitude"] != "" {
		t := data["Latitude"]
		lat = &t
	}
	if data["Longitude"] != "" {
		t := data["Longitude"]
		lng = &t
	}

	if data["TeamMakePublic"] == "on" {
		t := true
		makePublic = &t
	} else {
		t := false
		makePublic = &t
	}

	_, err := o.Raw("UPDATE team SET email = ?, facebook_page_url = ?, phone_number = ? , recruit = ? ,slug = ? ,team_country = ? ,team_description = ? ,team_from = ? ,team_name = ? ,team_province = ? ,twitter_handle = ?,website_url = ? ,youtube_link = ?, latitude = ?, longitude = ?, team_make_public = ? WHERE id = ? RETURNING id", data["TeamEmail"], data["TeamFacebook"], data["TeamPh"], data["Recruit"], data["Slug"], data["TeamCountry"], data["Description"], data["TeamLocation"], data["TeamName"], data["TeamProvince"], data["TeamTwitter"], data["TeamWebsite"], data["TeamYouTube"], &lat, &lng, &makePublic, team_id).Values(&maps)

	//result["user_id"] = maps[0]["id"].(string)

	if err == nil {
		result["status"] = "1"
		fmt.Println("Team Updated")
	}

	return result

}

func AddTeamMember(email_ids []string, message string, user_id string, team_id string, statusflag bool) ([]string, []string, []string, string) {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params
	var sent []string
	var alreadyInvited []string
	var alreadyMember []string

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	for _, email := range email_ids {

		num, err := o.Raw("SELECT us.id as id FROM users as us left join user_profile as up on up.fk_user_id = us.id WHERE lower(us.email_address) = ? and  up.active =1", strings.ToLower(email)).Values(&values)
		fmt.Println(err)
		if num > 0 {
			member_id := values[0]["id"]
			num, err := o.Raw("SELECT * FROM teamjoin WHERE request_status != 3 AND active = 1 AND fk_team_id = ? AND fk_member_id = ? ", team_id, member_id).Values(&maps)

			if num == 0 || statusflag {

				num, _ = o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? AND fk_member_id = ? ", team_id, member_id).Values(&maps)
				if num > 0 {
					_, err = o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, message = ?, email = ?, date = ?, fk_inviters_id = ?, accepted_date = ?  WHERE fk_team_id = ? AND fk_member_id = ? ", "2", "1", message, strings.ToLower(email), time.Now(), user_id, time.Now(), team_id, member_id).Values(&maps)
				} else {
					_, err = o.Raw("INSERT INTO teamjoin (request_status, active, message, email, date, fk_team_id, fk_member_id, fk_inviters_id, accepted_date ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "2", "1", message, strings.ToLower(email), time.Now(), team_id, member_id, user_id, time.Now()).Values(&maps)
				}

				if err == nil {

					_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
					team_name := maps[0]["team_name"].(string)
					team_slug := maps[0]["slug"].(string)

					teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
					desc := strings.Replace(constants.TEAM_ACCEPT_TEAM_NOTIFICATION, "#teamName", teamUrl, -1)
					member_id = "{" + member_id.(string) + "}"

					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{4}", "1", time.Now(), true, desc, "4", message, team_id, nil, user_id, member_id).Values(&maps)

					if err == nil {
						status = "true"

						member_id = values[0]["id"].(string)
						user_details := GetUserDetails(member_id.(string))
						name := "there"
						if user_details["user"]["name"] != nil {
							name = user_details["user"]["name"].(string)
						}
						instant_emails := user_details["user"]["instant_emails"].(string)
						email_address := user_details["user"]["email_address"].(string)

						sent = append(sent, email)

						//send mail
						if instant_emails == "1" {

							loginuser_details := GetUserDetails(user_id)
							teamCaptainFirstName := ""
							teamCaptainLastName := ""

							if loginuser_details != nil {
								if loginuser_details["user"]["name"] != nil {
									teamCaptainFirstName = loginuser_details["user"]["name"].(string)
								}
								if loginuser_details["user"]["last_name"] != nil {
									teamCaptainLastName = loginuser_details["user"]["last_name"].(string)
								}
							}

							content := strings.Replace(constants.TEAM_INVITE_INSTANT_EMAIL, "#teamName", team_name, -1)
							content = strings.Replace(content, "#teamCaptainFirstName", teamCaptainFirstName, -1)
							content = strings.Replace(content, "#teamCaptainLastName", teamCaptainLastName, -1)
							content = strings.Replace(content, "#useremail", email_address, -1)

							if len(message) > 0 {
								content = strings.Replace(content, "#teamInviteMessage", message, -1)
								content = strings.Replace(content, "#attributeValue", "block", -1)
							} else {
								content = strings.Replace(content, "#attributeValue", "none", -1)
							}

							email_content := strings.Replace(content, "#siteUrl", site_url, -1)
							footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
							unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

							html_email_message := "<html>Hi " + name + ", <br/><br/>" + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

							common.SendEmail(email_address, constants.TEAM_INVITE_EMAIL_SUBJECT, html_email_message)
						}

					} else {
						fmt.Println("Notification failed")
					}

				}
			} else {

				if maps[0]["request_status"] != nil {
					if maps[0]["request_status"] == "1" {
						fmt.Println("The member already invited")
						status = "already invited"
						alreadyInvited = append(alreadyInvited, email)

					} else if maps[0]["request_status"] == "2" {
						fmt.Println("Already a member")
						status = "already member"
						alreadyMember = append(alreadyMember, email)
					}
				}

			}

		} else {
			fmt.Println("The member invited does not exists")
			teamJoinID := ""
			num, _ := o.Raw("SELECT * FROM teamjoin WHERE request_status != 3 AND active = 1 AND fk_team_id = ? AND lower(email) = ? ", team_id, strings.ToLower(email)).Values(&maps)

			if num == 0 {

				num, err := o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? AND lower(email) = ? ", team_id, strings.ToLower(email)).Values(&maps)

				if num > 0 {
					_, err = o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, message = ?, email = ?, date = ?, fk_inviters_id = ?  WHERE fk_team_id = ? AND lower(email) = ? RETURNING id", "1", "1", message, strings.ToLower(email), time.Now(), user_id, team_id, strings.ToLower(email)).Values(&maps)
					teamJoinID = maps[0]["id"].(string)
				} else {
					_, err = o.Raw("INSERT INTO teamjoin (request_status, active, message, email, date, accepted_date, fk_team_id, fk_member_id, fk_inviters_id ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", "1", message, strings.ToLower(email), time.Now(), time.Time{}, team_id, nil, user_id).Values(&maps)
					teamJoinID = maps[0]["id"].(string)
				}

				if err == nil {

					_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
					team_name := maps[0]["team_name"].(string)
					team_slug := maps[0]["slug"].(string)
					team_CaptainId := "0"
					if maps[0]["fk_captain_id"] != nil {
						team_CaptainId = maps[0]["fk_captain_id"].(string)
					}

					// generate the token to send in the url of the email template
					verificationToken := SendTeamInviteVerficationLink(email)
					showPage := ""
					if verificationToken != "" {
						showPage = site_url + "register/" + teamJoinID + "/" + verificationToken
					} else {
						showPage = site_url + "register/" + teamJoinID
					}
					teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

					status = "true"

					sent = append(sent, email)

					teamCaptain_User_Details := GetUserDetails(team_CaptainId)
					teamCaptainFirstName := ""
					teamCaptainLastName := ""

					if teamCaptain_User_Details != nil {
						if teamCaptain_User_Details["user"]["name"] != nil {
							teamCaptainFirstName = teamCaptain_User_Details["user"]["name"].(string)
						}
						if teamCaptain_User_Details["user"]["last_name"] != nil {
							teamCaptainLastName = teamCaptain_User_Details["user"]["last_name"].(string)
						}
					}

					//send mail
					content := strings.Replace(constants.TEAM_INVITE_REGISTERATION, "#teamName", teamUrl, -1)
					content = strings.Replace(content, "#teamCaptainFirstName", teamCaptainFirstName, -1)
					content = strings.Replace(content, "#teamCaptainLastName", teamCaptainLastName, -1)
					content = strings.Replace(content, "#useremail", email, -1)

					content = strings.Replace(content, "#showPage", showPage, -1)

					if len(message) > 0 {
						content = strings.Replace(content, "#teamInviteMessage", message, -1)
						content = strings.Replace(content, "#attributeValue", "block", -1)
					} else {
						content = strings.Replace(content, "#attributeValue", "none", -1)
					}

					email_content := strings.Replace(content, "#siteUrl", site_url, -1)
					footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

					html_email_message := "<html>Hi there, <br/>" + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + "</html>"

					common.SendEmail(email, constants.TEAM_INVITE_EMAIL_SUBJECT, html_email_message)

				}
			} else {

				if maps[0]["request_status"] != nil {
					if maps[0]["request_status"] == "1" {
						fmt.Println("The member already invited")
						status = "already invited"
						alreadyInvited = append(alreadyInvited, email)

					} else if maps[0]["request_status"] == "2" {
						fmt.Println("Already a member")
						status = "already member"
						alreadyMember = append(alreadyMember, email)
					}
				}

			}

		}

	}

	return sent, alreadyInvited, alreadyMember, status
}

func AcceptTeamMember(team_id string, member_id string, notf_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	num, _ := o.Raw("SELECT * FROM teamjoin WHERE request_status = 1 AND fk_team_id = ? AND fk_member_id = ? ", team_id, member_id).Values(&maps)

	if num > 0 {
		_, err := o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, accepted_date = ? WHERE id = ? ", "2", "1", time.Now(), maps[0]["id"]).Values(&maps)

		if err == nil {

			_, err := o.Raw("UPDATE notification_list SET active = ? WHERE id = ? RETURNING fk_initiator_id", "9", notf_id).Values(&maps)

			if err == nil {

				_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&values)

				team_name := values[0]["team_name"].(string)
				team_slug := values[0]["slug"].(string)

				user_name := ""

				teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

				user_details := GetUserDetails(member_id)
				user_name = user_details["user"]["name"].(string)

				message := strings.Replace(constants.TEAM_INVITE_ACCEPTED_DIGEST_NOTIFICATION, "#userName", user_name, -1)
				message = strings.Replace(message, "#teamName", teamUrl, -1)

				receiver_id := "{" + maps[0]["fk_initiator_id"].(string) + "}"

				receiver_details := GetUserDetails(maps[0]["fk_initiator_id"].(string))
				rc_id := maps[0]["fk_initiator_id"].(string)
				receiver_email := receiver_details["user"]["email_address"].(string)
				send_email_digest := receiver_details["user"]["subscribe"].(string)

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, message, "9", nil, nil, nil, member_id, receiver_id).Values(&maps)

				//send mail
				if send_email_digest == "1" {
					_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "7", message, receiver_email, time.Now(), "0", rc_id, team_id).Values(&maps)

					if err == nil {
						status = "true"
					} else {
						fmt.Println("Email Digest Failed")
					}

					status = "true"
				}

			} else {
				fmt.Println("Notification failed")
			}

		}
	} else {
		fmt.Println("Invalid invitation")
		status = "invalid"
	}

	return status
}

func DeclineTeamMember(team_id string, member_id string, notf_id string) string {

	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params
	var mapsupdate []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	statusCaptain := CheckUserIsCaptain(team_id, member_id)

	if !statusCaptain {
		num, _ := o.Raw("SELECT * FROM teamjoin WHERE request_status = 2 AND fk_team_id = ? AND fk_member_id = ? ", team_id, member_id).Values(&maps)
		_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&values)
		if num > 0 {
			_, err := o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, accepted_date = ? WHERE id = ? ", "3", "1", time.Now(), maps[0]["id"]).Values(&maps)
			if err == nil {

				statusCocaptain := CheckUserIsCoCaptain(team_id, member_id)

				if statusCocaptain {
					_, err := o.Raw("UPDATE team SET fk_co_captains = array_remove(fk_co_captains, '"+member_id+"') WHERE id = ?", team_id).Values(&mapsupdate)
					fmt.Println(err)
				}

				_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&values)

				team_name := values[0]["team_name"].(string)
				team_slug := values[0]["slug"].(string)
				teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

				desc := strings.Replace(constants.TEAM_ACCEPT_REJECTED_NOTIFICATION, "#teamName", teamUrl, -1)

				_, err := o.Raw("UPDATE notification_list SET active = ?,actions =?, type =?, notification_desc=?  WHERE id = ? Returning fk_initiator_id", "1", "{9}", 9, desc, notf_id).Values(&maps)
				fmt.Println(err)
				if err == nil {

					user_name := ""
					user_details := GetUserDetails(member_id)
					user_name = user_details["user"]["name"].(string)

					message := strings.Replace(constants.TEAM_INVITE_REJECTED_NOTIFICATION, "#userName", user_name, -1)
					message = strings.Replace(message, "#teamName", teamUrl, -1)

					receiver_id := "{" + maps[0]["fk_initiator_id"].(string) + "}"

					receiver_details := GetUserDetails(maps[0]["fk_initiator_id"].(string))
					rc_id := maps[0]["fk_initiator_id"].(string)
					receiver_email := receiver_details["user"]["email_address"].(string)
					send_email_digest := receiver_details["user"]["subscribe"].(string)

					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, message, "9", nil, nil, nil, member_id, receiver_id).Values(&maps)

					//send mail
					if send_email_digest == "1" {
						_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "7", message, receiver_email, time.Now(), "0", rc_id, team_id).Values(&maps)

						if err == nil {
							status = "true"
						} else {
							fmt.Println("Email Digest Failed")
						}

						status = "true"
					}

				} else {
					fmt.Println("Notification failed")
				}

			}
		} else {
			fmt.Println("Invalid invitation")
			status = "invalid"
		}
	} else {
		status = "captain"
	}
	return status
}

func CanCreateTeam(user_id int) bool {

	//user_details := GetUserDetails(user_id)
	var maps []orm.Params
	o := orm.NewOrm()

	num, _ := o.Raw("SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE users.id = ? AND activities @> ARRAY [?]::varchar[] ", user_id, "Team Management").Values(&maps)

	if num == 1 {
		return true
	} else {
		return false
	}

}

func AddTeamPracticeSchedule(data map[string]string, logged_user_id string, team_id string) string {

	result := "false"

	fmt.Println("In AddTeamPracticeSchedule")
	fmt.Println(data)

	var outputEndDate *string
	outputEndDate = nil

	localToUtcTime := common.CovertToUtcTimeZone(data["PracticeDateTimeZone"])

	o := orm.NewOrm()
	if len(data["PracticeDate"]) > 0 {

		repeat := 1

		if data["PracticeRepeats"] == "Weekly" || data["PracticeRepeats"] == "Monthly" {
			repeat = 5

		} else {
			repeat = 1
		}

		var maps []orm.Params

		digest_practice_period := ""
		if data["PracticeRepeats"] == "Weekly" {

			digest_practice_period = "repeats weekly for 5 weeks"

			for i := 1; i <= repeat; i++ {

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)

				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					if i != 1 {
						t = t.AddDate(0, 0, 7*(i-1))
						fmt.Println(t, i)
					}
					_, month, _ := t.Date()
					month_val := int(month) - 1
					year, week := t.ISOWeek()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

					_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], data["PracticeType"]).Values(&maps)

					if err == nil {
						result = "true"
						generateUniqueId(maps[0]["id"].(string))
					} else {
						result = "false"
					}
				}
			}

		} else if data["PracticeRepeats"] == "Monthly" {

			digest_practice_period = "repeats Monthly for 5 weeks"

			for i := 1; i <= repeat; i++ {

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					if i != 1 {
						t = t.AddDate(0, 1*(i-1), 0)
					}

					_, month, _ := t.Date()
					month_val := int(month) - 1
					year, week := t.ISOWeek()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

					_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone , team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], data["PracticeType"]).Values(&maps)

					if err == nil {
						result = "true"
						fmt.Println(maps[0]["id"].(string))
						generateUniqueId(maps[0]["id"].(string))
					} else {
						result = "false"
					}

				}
			}

		} else {
			digest_practice_period = "" //repeats never

			for i := 1; i <= repeat; i++ {

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)

				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					_, month, _ := t.Date()
					month_val := int(month) - 1
					year, week := t.ISOWeek()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)
					fmt.Println("output123", output)

					if data["PracticeType"] == "Activity" && data["PracticeEndDateTimeZone"] != "" {
						str := data["PracticeEndDateTimeZone"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							temp := t.Format(constants.DATE_TIME_ZONE_FORMAT)
							outputEndDate = &temp
						}
					}
					if data["PracticeType"] == "Activity" && data["PracticeEndDateTimeZone"] != "" {
						_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type, practices_end_date  ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], nil, data["Type"], week, year, team_id, false, data["LocationTimeZone"], data["PracticeType"], outputEndDate).Values(&maps)
						fmt.Println(err)
					} else {
						_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], nil, data["Type"], week, year, team_id, false, data["LocationTimeZone"], data["PracticeType"]).Values(&maps)
						fmt.Println(err)
					}

					if err == nil {
						result = "true"
						fmt.Println(maps[0]["id"].(string))
						generateUniqueId(maps[0]["id"].(string))
					} else {
						result = "false"
					}

				}
			}

		}

		if data["Type"] == "team" && result == "true" {

			team_members := GetTeamMembers(team_id)
			team_slug := GetTeamSlug(team_id)

			dt, _ := time.Parse("01/02/2006", data["PracticeDate"])
			formatted_date := dt.Format("Mon, Jan 2, 2006")

			formatted_end_date := ""
			if data["PracticeType"] == "Activity" {
				dt, _ := time.Parse("01/02/2006", data["PracticeEndDate"])
				formatted_end_date = dt.Format("Mon, Jan 2, 2006")
			}

			for _, member := range team_members {

				member_id := member["fk_member_id"].(string)
				send_email_digest := member["subscribe"].(string)

				site_url := ""
				if len(os.Getenv("SITE_URL")) > 0 {
					site_url = os.Getenv("SITE_URL")
				} else {
					site_url = "http://localhost:5000/"
				}

				team_name := member["team_name"].(string)

				fmt.Printf("Team Name \n")

				team_name = "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
				practice_desc := ""
				if data["PracticeType"] == "Activity" {
					practice_desc = strings.Replace(constants.TEAM_ACTIVITY_ADDED_DIGEST_NOTIFICATION, "#teamName", team_name, -1)
					practice_desc = strings.Replace(practice_desc, "#practiceEndDate", formatted_end_date, -1)
				} else {
					practice_desc = strings.Replace(constants.TEAM_PRACTICE_ADDED_DIGEST_NOTIFICATION, "#teamName", team_name, -1)
				}

				practice_desc = strings.Replace(practice_desc, "#practiceDate", formatted_date, -1)
				practice_desc = strings.Replace(practice_desc, "#practiceTime", data["PracticeTime"], -1)

				if digest_practice_period == "" {
					practice_desc = strings.Replace(practice_desc, "(#practiceRepeats)", digest_practice_period, -1)
				} else {
					practice_desc = strings.Replace(practice_desc, "#practiceRepeats", digest_practice_period, -1)
				}

				if send_email_digest == "1" {

					_, _ = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "10", practice_desc, member["email_address"], time.Now(), "0", member["fk_member_id"], team_id).Values(&maps)

				}

				member_id = "{" + member_id + "}"

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, practice_desc, "9", team_id, nil, logged_user_id, member_id).Values(&maps)

				if err == nil {
					fmt.Printf("Notification Sent")
				} else {
					fmt.Printf("Notification Error Occured")
					fmt.Println(err)
				}
			}

		}

	}

	if result == "true" {
		fmt.Println(localToUtcTime)
		activateFreeTrial(team_id, localToUtcTime)
	}
	return result
}

func EditTeamPracticeSchedule(data map[string]string, logged_user_id string, team_id string, loginUserTimeZone string) string {

	result := "false"
	isEditPractice := true
	fmt.Println("In EditTeamPracticeSchedule")
	fmt.Println(data)

	var practice_ids []string
	var mapData []orm.Params
	var tempmapData []orm.Params
	o := orm.NewOrm()

	var outputEndDate *string
	outputEndDate = nil
	team_practices_type := ""

	localToUtcTime := common.CovertToUtcTimeZone(data["PracticeDateTimeZone"])

	if len(data["PracticeDate"]) > 0 {
		// check db and user input data match or not. avoid unwanted update process
		num, _ := o.Raw("select * from  team_practices WHERE id = ? ", data["PracticeId"]).Values(&mapData)
		fmt.Print(num)
		if num == 1 {
			practiceDate := mapData[0]["practice_date"].(string)
			practiceLength := mapData[0]["practice_length"]
			practiceTime := mapData[0]["practice_time"]
			practiceLocation := mapData[0]["practice_location"]
			repeats := mapData[0]["repeats"]
			practiceType := mapData[0]["type"]
			note := mapData[0]["note"]
			team_practices_type = mapData[0]["team_practices_type"].(string)

			if team_practices_type == "Practice" {
				if localToUtcTime == practiceDate && data["PracticeLength"] == practiceLength &&
					data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation &&
					data["PracticeRepeats"] == repeats && data["Type"] == practiceType && data["PracticeNote"] == note {
					isEditPractice = false
					return "NoChanges"
				}

				if localToUtcTime == practiceDate && data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation &&
					(data["PracticeRepeats"] != repeats || data["PracticeLength"] != practiceLength || data["PracticeNote"] == note || data["Type"] == practiceType) {
					isEditPractice = false
				}
			} else {
				if data["PracticeEndDate"] != "" {
					localToUtcEndTime := common.CovertToUtcTimeZone(data["PracticeEndDate"])
					if mapData[0]["practices_end_date"] != nil {
						practiceEndDate := mapData[0]["practices_end_date"].(string)
						if localToUtcTime == practiceDate && localToUtcEndTime == practiceEndDate && data["PracticeLength"] == practiceLength &&
							data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation && data["Type"] == practiceType && data["PracticeNote"] == note {
							isEditPractice = false
							return "NoChanges"
						}

						if localToUtcTime == practiceDate && localToUtcEndTime == practiceEndDate && data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation &&
							(data["PracticeRepeats"] != repeats || data["PracticeLength"] != practiceLength || data["PracticeNote"] == note || data["Type"] == practiceType) {
							isEditPractice = false
						}
					}
				} else if mapData[0]["practices_end_date"] == nil {
					if localToUtcTime == practiceDate && data["PracticeLength"] == practiceLength &&
						data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation && data["Type"] == practiceType && data["PracticeNote"] == note {
						isEditPractice = false
						return "NoChanges"
					}

					if localToUtcTime == practiceDate && data["PracticeTime"] == practiceTime && data["PracticeLocation"] == practiceLocation &&
						(data["PracticeRepeats"] != repeats || data["PracticeLength"] != practiceLength || data["PracticeNote"] == note || data["Type"] == practiceType) {
						isEditPractice = false
					}
				}
			}
		}
		//End Part

		if data["PracticeRepeats"] == "Weekly" {

			var maps []orm.Params

			if data["EditOption"] == "only" {
				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id = ? ", "1", data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice, data["PracticeId"]).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				}

			} else if data["EditOption"] == "all_following" {

				isRepeatFromNoneToWeeklyOrMonthy := false
				_, _ = o.Raw("SELECT practice_date,repeats FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

				layout := "2006-01-02T15:04:05Z07:00"
				initial_date := ""

				if maps[0]["practice_date"] != nil {
					initial_date = maps[0]["practice_date"].(string)
					fmt.Println(initial_date)
				}

				if maps[0]["repeats"] == nil && data["PracticeRepeats"] != "" {
					pRepeats := maps[0]["repeats"]
					fmt.Println("p_repeats = ", pRepeats, "PracticeRepeats", data["PracticeRepeats"])
					fmt.Println(isRepeatFromNoneToWeeklyOrMonthy)
					isRepeatFromNoneToWeeklyOrMonthy = true
				}

				if initial_date != "" {

					// get following practices for the series
					t, err := time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {
						for {
							t = t.AddDate(0, 0, 7)
							fmt.Println(t)
							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}
						fmt.Println("practice_ids", practice_ids)
					}

					// get current practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
								if isEditPractice {
									_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
								}
							}
						}

					}
				}

				fmt.Println("Rows to be edited ", practice_ids)
				fmt.Println("initial_date = ", initial_date)

				if initial_date == localToUtcTime {

					practice_ids_string := strings.Join(practice_ids[:], ",")

					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id  IN ( "+practice_ids_string+" )", "1", data["PracticeNote"], data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}

				} else {

					if len(practice_ids) == 4 {

						//Changing all 4 subsequent practices means the first one becomes a practice where repeats is null

						_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

						layout := "2006-01-02T15:04:05Z07:00"
						initial_date := ""

						if maps[0]["practice_date"] != nil {
							initial_date = maps[0]["practice_date"].(string)
						}

						t, err := time.Parse(layout, initial_date)

						if err != nil {
							fmt.Println(err)
						} else {

							for {
								t = t.AddDate(0, 0, -7)

								count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

								if count > 0 {
									for _, p := range maps {
										fmt.Println("Rows for which update repeats ", p["id"].(string))
										_, _ = o.Raw("UPDATE team_practices set repeats = ?, ispractice_last_edit = ?  WHERE id = ?", nil, isEditPractice, p["id"].(string)).Exec()
									}
									t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
								} else {
									break
								}
							}

						}
					}

					i := 1
					for _, pid := range practice_ids {

						str := data["PracticeDateTimeZone"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							if i != 1 {
								t = t.AddDate(0, 0, 7*(i-1))
								fmt.Println(t, i)
							}

							_, month, _ := t.Date()
							month_val := int(month) - 1
							year, week := t.ISOWeek()
							output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

							_, err := o.Raw("UPDATE team_practices set active = ? , month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, ispractice_last_edit = ?  WHERE id = ?", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, isEditPractice, pid).Values(&maps)

							if err == nil {
								result = "true"
							} else {
								result = "false"
							}

						}

						i = i + 1
					}

					if len(practice_ids) < 5 {

						for k := 1; k <= (5 - len(practice_ids)); k++ {

							str := data["PracticeDateTimeZone"]
							local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
							if err != nil {
								fmt.Println(err)
							} else {
								t := local.UTC()
								if i != 1 {
									t = t.AddDate(0, 0, 7*(i-1))
									fmt.Println(t, i)
								}

								_, month, _ := t.Date()
								month_val := int(month) - 1
								year, week := t.ISOWeek()
								output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

								_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

								if err == nil {
									result = "true"
								} else {
									result = "false"
								}
							}

							i = i + 1
						}
					}

				}

			} else if data["EditOption"] == "all" {

				_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

				layout := "2006-01-02T15:04:05Z07:00"
				initial_date := ""

				if maps[0]["practice_date"] != nil {
					initial_date = maps[0]["practice_date"].(string)
				}

				if initial_date != "" {

					// get following practices for the series
					t, err := time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						for {
							t = t.AddDate(0, 0, 7)

							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}

					}

					// get previous practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						for {
							t = t.AddDate(0, 0, -7)

							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}

					}

					// get current practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
								if isEditPractice {
									_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
								}
							}
						}

					}
				}

				fmt.Println("Rows to be edited ", practice_ids)

				if initial_date == localToUtcTime {

					practice_ids_string := strings.Join(practice_ids[:], ",")

					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id  IN ( "+practice_ids_string+" )", "1", data["PracticeNote"], data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				} else {
					i := 1
					for _, pid := range practice_ids {

						str := data["PracticeDateTimeZone"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							if i != 1 {
								t = t.AddDate(0, 0, 7*(i-1))
								fmt.Println(t, i)
							}

							_, month, _ := t.Date()
							month_val := int(month) - 1
							year, week := t.ISOWeek()
							output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

							_, err := o.Raw("UPDATE team_practices set active = ? , month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, ispractice_last_edit = ? WHERE id = ?", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, isEditPractice, pid).Values(&maps)

							if err == nil {
								result = "true"
							} else {
								result = "false"
							}

						}

						i = i + 1
					}

					if len(practice_ids) < 5 {

						for k := 1; k <= (5 - len(practice_ids)); k++ {

							str := data["PracticeDateTimeZone"]
							local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
							if err != nil {
								fmt.Println(err)
							} else {
								t := local.UTC()
								if i != 1 {
									t = t.AddDate(0, 0, 7*(i-1))
									fmt.Println(t, i)
								}

								_, month, _ := t.Date()
								month_val := int(month) - 1
								year, week := t.ISOWeek()
								output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

								_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

								if err == nil {
									result = "true"
								} else {
									result = "false"
								}
							}

							i = i + 1
						}
					}
				}

			} else if data["PrevPracticeRepeats"] == "" {
				fmt.Println("reoccuring code for weekly here...")

				// updating the current non-occuring practice

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)
					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, repeats = ?, ispractice_last_edit = ? WHERE id = ? ", "1", data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, data["PracticeRepeats"], isEditPractice, data["PracticeId"]).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				}

				// repeats weekly for 5 weeks
				repeat := 4

				for i := 1; i <= repeat; i++ {

					str := data["PracticeDateTimeZone"]
					local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
					if err != nil {
						fmt.Println(err)
					} else {
						t := local.UTC()
						t = t.AddDate(0, 0, 7*(i+1-1))
						fmt.Println(t, i)

						_, month, _ := t.Date()
						month_val := int(month) - 1
						year, week := t.ISOWeek()
						output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

						_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

						if err == nil {
							result = "true"
						} else {
							result = "false"
						}
					}
				}
			}

		} else if data["PracticeRepeats"] == "Monthly" {

			var maps []orm.Params

			if data["EditOption"] == "only" {

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)
					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id = ? ", "1", data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice, data["PracticeId"]).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				}

			} else if data["EditOption"] == "all_following" {

				_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

				layout := "2006-01-02T15:04:05Z07:00"
				initial_date := ""

				if maps[0]["practice_date"] != nil {
					initial_date = maps[0]["practice_date"].(string)
				}

				if initial_date != "" {

					// get following practices for the series
					t, err := time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						for {
							t = t.AddDate(0, 1, 0)

							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Monthly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}

					}

					// get current practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Monthly", "1", team_id).Values(&maps)
						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
								if isEditPractice {
									_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
								}
							}
						}

					}
				}

				fmt.Println("Rows to be edited ", practice_ids)

				if initial_date == localToUtcTime {

					practice_ids_string := strings.Join(practice_ids[:], ",")

					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id  IN ( "+practice_ids_string+" )", "1", data["PracticeNote"], data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				} else {

					if len(practice_ids) == 4 {

						//Changing all 4 subsequent practices means the first one becomes a practice where repeats is null

						_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

						layout := "2006-01-02T15:04:05Z07:00"
						initial_date := ""

						if maps[0]["practice_date"] != nil {
							initial_date = maps[0]["practice_date"].(string)
						}

						t, err := time.Parse(layout, initial_date)

						if err != nil {
							fmt.Println(err)
						} else {

							for {
								t = t.AddDate(0, -1, 0)

								count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Weekly", "1", team_id).Values(&maps)

								if count > 0 {
									for _, p := range maps {
										fmt.Println("Rows for which update repeats ", p["id"].(string))
										_, _ = o.Raw("UPDATE team_practices set repeats = ?, ispractice_last_edit = ? WHERE id = ?", nil, isEditPractice, p["id"].(string)).Exec()
									}
									t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
								} else {
									break
								}
							}

						}
					}

					i := 1
					for _, pid := range practice_ids {

						str := data["PracticeDateTimeZone"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							if i != 1 {
								t = t.AddDate(0, 1*(i-1), 0)
								fmt.Println(t, i)
							}

							_, month, _ := t.Date()
							month_val := int(month) - 1
							year, week := t.ISOWeek()
							output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

							_, err := o.Raw("UPDATE team_practices set active = ? , month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, ispractice_last_edit = ?  WHERE id = ?", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, isEditPractice, pid).Values(&maps)

							if err == nil {
								result = "true"
							} else {
								result = "false"
							}

						}

						i = i + 1
					}

					if len(practice_ids) < 5 {

						for k := 1; k <= (5 - len(practice_ids)); k++ {

							str := data["PracticeDateTimeZone"]
							local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
							if err != nil {
								fmt.Println(err)
							} else {
								t := local.UTC()
								if i != 1 {
									t = t.AddDate(0, 1*(i-1), 0)
									fmt.Println(t, i)
								}

								_, month, _ := t.Date()
								month_val := int(month) - 1
								year, week := t.ISOWeek()
								output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

								_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

								if err == nil {
									result = "true"
								} else {
									result = "false"
								}
							}

							i = i + 1
						}
					}
				}

			} else if data["EditOption"] == "all" {

				_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

				layout := "2006-01-02T15:04:05Z07:00"
				initial_date := ""

				if maps[0]["practice_date"] != nil {
					initial_date = maps[0]["practice_date"].(string)
				}

				if initial_date != "" {

					// get following practices for the series
					t, err := time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						for {
							t = t.AddDate(0, 1, 0)

							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Monthly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}

					}

					// get previous practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						for {
							t = t.AddDate(0, -1, 0)

							count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Monthly", "1", team_id).Values(&maps)

							if count > 0 {
								for _, p := range maps {
									practice_ids = append(practice_ids, p["id"].(string))
									if isEditPractice {
										_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
									}
								}
								t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
							} else {
								break
							}
						}

					}

					// get current practices for the series
					t, err = time.Parse(layout, initial_date)

					if err != nil {
						fmt.Println(err)
					} else {

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND repeats = ? AND active = ? AND fk_team_id = ? ORDER BY id ASC", t, "Monthly", "1", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
								if isEditPractice {
									_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", p["id"].(string)).Values(&tempmapData)
								}
							}
						}

					}
				}

				fmt.Println("Rows to be edited ", practice_ids)

				if initial_date == localToUtcTime {
					practice_ids_string := strings.Join(practice_ids[:], ",")

					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, ispractice_last_edit = ? WHERE id  IN ( "+practice_ids_string+" )", "1", data["PracticeNote"], data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, isEditPractice).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				} else {

					i := 1
					for _, pid := range practice_ids {

						str := data["PracticeDateTimeZone"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							if i != 1 {
								t = t.AddDate(0, 1*(i-1), 0)
								fmt.Println(t, i)
							}

							_, month, _ := t.Date()
							month_val := int(month) - 1
							year, week := t.ISOWeek()
							output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

							_, err := o.Raw("UPDATE team_practices set active = ? , month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, ispractice_last_edit = ?  WHERE id = ?", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, isEditPractice, pid).Values(&maps)

							if err == nil {
								result = "true"
							} else {
								result = "false"
							}

						}

						i = i + 1
					}

					if len(practice_ids) < 5 {

						for k := 1; k <= (5 - len(practice_ids)); k++ {

							str := data["PracticeDateTimeZone"]
							local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
							if err != nil {
								fmt.Println(err)
							} else {
								t := local.UTC()
								if i != 1 {
									t = t.AddDate(0, 1*(i-1), 0)
									fmt.Println(t, i)
								}

								_, month, _ := t.Date()
								month_val := int(month) - 1
								year, week := t.ISOWeek()
								output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

								_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

								if err == nil {
									result = "true"
								} else {
									result = "false"
								}
							}

							i = i + 1
						}
					}
				}

			} else if data["PrevPracticeRepeats"] == "" {
				fmt.Println("reoccuring code for monthly here...")

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)
					_, err := o.Raw("UPDATE team_practices set active = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, type = ?, fk_team_id = ?, repeats = ?, ispractice_last_edit = ? WHERE id = ? ", "1", data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["Type"], team_id, data["PracticeRepeats"], isEditPractice, data["PracticeId"]).Values(&maps)

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				}

				// repeats Monthly for 5 weeks
				repeat := 4
				for i := 1; i <= repeat; i++ {

					str := data["PracticeDateTimeZone"]
					local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
					if err != nil {
						fmt.Println(err)
					} else {
						t := local.UTC()
						t = t.AddDate(0, 1*(i+1-1), 0)

						_, month, _ := t.Date()
						month_val := int(month) - 1
						year, week := t.ISOWeek()
						output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

						_, err := o.Raw("INSERT INTO team_practices (active, month, note, practice_date, practice_length, practice_location, practice_time, repeats, type, week_number, year, fk_team_id, ispractice_last_edit, user_time_zone, team_practices_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, false, data["LocationTimeZone"], "Practice").Values(&maps)

						if err == nil {
							result = "true"
						} else {
							result = "false"
						}

					}
				}

			}

		} else {
			var maps []orm.Params

			if data["EditOption"] == "only" {

				str := data["PracticeDateTimeZone"]
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err != nil {
					fmt.Println(err)
				} else {
					t := local.UTC()
					_, month, _ := t.Date()
					month_val := int(month) - 1
					year, week := t.ISOWeek()
					output := t.Format(constants.DATE_TIME_ZONE_FORMAT)

					if team_practices_type == "Activity" && data["PracticeEndDate"] != "" {
						str := data["PracticeEndDate"]
						local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
						if err != nil {
							fmt.Println(err)
						} else {
							t := local.UTC()
							temp := t.Format(constants.DATE_TIME_ZONE_FORMAT)
							outputEndDate = &temp
						}
					}

					if team_practices_type == "Activity" && data["PracticeEndDate"] != "" {

						_, err = o.Raw("UPDATE team_practices set active = ?, month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, fk_team_id = ?, ispractice_last_edit =? , practices_end_date =? WHERE id = ? ", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, isEditPractice, outputEndDate, data["PracticeId"]).Values(&maps)

					} else if team_practices_type == "Activity" && data["PracticeEndDate"] == "" && mapData[0]["practices_end_date"] != nil {
						_, err = o.Raw("UPDATE team_practices set active = ?, month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, fk_team_id = ?, ispractice_last_edit =? , practices_end_date =? WHERE id = ? ", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, isEditPractice, nil, data["PracticeId"]).Values(&maps)

					} else {
						_, err = o.Raw("UPDATE team_practices set active = ?, month = ?, note = ?, practice_date = ?, practice_length = ?, practice_location = ?, practice_time = ?, repeats = ?, type = ?, week_number = ?, year = ?, fk_team_id = ?, ispractice_last_edit =? WHERE id = ? ", "1", month_val, data["PracticeNote"], output, data["PracticeLength"], data["PracticeLocation"], data["PracticeTime"], data["PracticeRepeats"], data["Type"], week, year, team_id, isEditPractice, data["PracticeId"]).Values(&maps)

					}

					if err == nil {
						result = "true"
					} else {
						result = "false"
					}
				}
			}

		}
		if result == "true" {
			activateFreeTrial(team_id, localToUtcTime)
			if isEditPractice {
				_, _ = o.Raw("delete from practice_attendees where fk_practice_id = ?", data["PracticeId"]).Values(&mapData)
			}
		}
	} else if data["PracticeDate"] == "" && data["PracticeType"] == "" {

		_, err := o.Raw("UPDATE team_practices set note = ? WHERE id = ? ", data["PracticeNote"], data["PracticeId"]).Values(&mapData)
		if err == nil {
			result = "true"
		}
	}
	return result
}

func SaveTeamUrl(team_id string, path string, fileName string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	update, err := o.Raw("UPDATE team_logo_url SET name = ?, path = ? WHERE fk_team_id = ? RETURNING id", fileName, url, team_id).Values(&maps)

	if update == 0 {

		_, err = o.Raw("INSERT INTO team_logo_url (fk_team_id, name, path) VALUES (?,?,?) RETURNING id", team_id, fileName, url).Values(&maps)

	}

	if err == nil {
		status = "true"
	} else {
		status = "false"
		fmt.Println(err)
	}
	fmt.Println(status)
	return url
}

func RemoveTeamMembers(data map[string][]string, team_id string, user_id string, removed_by string) (string, string) {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params
	code := ""
	last_captain := ""

	if removed_by == "super_admin" {
		code = "7"
	} else {
		code = "9"
	}

	for _, member_id := range data["MemberIds"] {

		_, err := strconv.Atoi(member_id)

		if err == nil {
			num, _ := o.Raw("SELECT fk_co_captains, array_length(fk_co_captains, 1) as count FROM team WHERE id = ? AND fk_co_captains @> ARRAY [?]::bigint[] ", team_id, member_id).Values(&maps)

			if num > 0 {

				fmt.Println(" Co Captains Count", maps[0]["count"])

				// if maps[0]["count"] == "1" {
				// 	err = errors.New("Cannot remove last captain")
				// 	user_details := GetUserDetails(member_id)
				// 	last_captain = user_details["user"]["name"].(string) + " " + user_details["user"]["last_name"].(string)

				// } else {
				_, err = o.Raw("UPDATE team SET fk_co_captains = array_remove(fk_co_captains, ? ) WHERE id = ? ", member_id, team_id).Values(&maps)

				_, err = o.Raw("UPDATE teamjoin SET active = ? WHERE fk_team_id = ? AND fk_member_id = ? ", code, team_id, member_id).Values(&maps)
				deleteTeamMemberDependencies(team_id, member_id)
				// }

			} else {
				_, err = o.Raw("UPDATE teamjoin SET active = ? WHERE fk_team_id = ? AND fk_member_id = ? ", code, team_id, member_id).Values(&maps)
				deleteTeamMemberDependencies(team_id, member_id)
			}

			if err == nil {
				status = "true"
			}

		} else {
			// user not in the system
			fmt.Println(" user not in the system ")
			email := member_id
			_, err = o.Raw("UPDATE teamjoin SET active = ? WHERE fk_team_id = ? AND email = ? ", code, team_id, email).Values(&maps)

			num, err := o.Raw("select * from  users WHERE email_address = ? ", email).Values(&maps)
			if num > 0 {
				if maps[0]["id"] != nil {
					deleteTeamMemberDependencies(team_id, maps[0]["id"].(string))
				}
			}

			fmt.Println(err)

			if err == nil {
				status = "true"
			}
		}
	}

	return status, last_captain
}

func SaveTeamMembers(data map[string][]string) string {
	status := "false"
	userWeight := 0
	o := orm.NewOrm()

	var maps []orm.Params

	for _, update_tm_data := range data["Team_members_update"] {

		team_user_data := strings.Split(update_tm_data, "#")
		fkUserID, paddlingSide, skillLevel, weight, countryCode, phoneNumber, dialCountryCode := team_user_data[0], team_user_data[1], team_user_data[2], team_user_data[3], team_user_data[4], team_user_data[5], team_user_data[6]
		if weight == "" {
			userWeight = 0
		} else {
			i, _ := strconv.Atoi(weight)
			userWeight = i
		}

		_, err := o.Raw("UPDATE user_profile SET paddling_side = ?, skill_level = ?, weight = ?, country_dial_code = ?, phone = ?, dial_county_code=? WHERE fk_user_id = ? ", paddlingSide, skillLevel, userWeight, countryCode, phoneNumber, dialCountryCode, fkUserID).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			status = "false"
			fmt.Println(err)
		}
	}
	return status
}

func AddTeamDiscussion(data map[string]string, team_id string, user_id string) CommunicationResponse {

	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var maps1 []orm.Params
	var receivers []string
	smsResponseStatusList := make([]smsResponseStatus, 0)
	team_sms_subscription_limits := "0"
	isSmsSentToAnyone := 0
	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
	team_sms_limits := maps[0]["sms_limits"].(string)

	num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ? ", team_id).Values(&maps1)
	if num > 0 {
		team_sms_subscription_limits = maps1[0]["sms_limits"].(string)
	}
	temp_team_sms_limits, _ := strconv.Atoi(team_sms_limits)
	temp_team_sms_subscription_limits, _ := strconv.Atoi(team_sms_subscription_limits)
	sms_limits := temp_team_sms_limits + temp_team_sms_subscription_limits

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, err := o.Raw("INSERT INTO team_discussion (discussion_msg, notify_type, created_at, fk_team_id, fk_user_id, sent_as ) VALUES (?,?,?,?,?,?) RETURNING id", data["DiscussionMsg"], data["Type"], time.Now(), team_id, user_id, data["SendAs"]).Values(&maps)

	if err == nil {
		//smsCountCheck, _ := strconv.Atoi(constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM)
		if data["Type"] != "captain" {
			team_mem, _ := o.Raw("SELECT fk_member_id FROM teamjoin WHERE fk_team_id = ? AND request_status = 2 AND active = 1 AND fk_member_id IS NOT NULL", team_id).Values(&maps)

			team_mem_string := ""

			if team_mem > 0 {
				for i, member_id := range maps {
					fmt.Println(member_id, i)
					if i == 0 {
						team_mem_string = team_mem_string + member_id["fk_member_id"].(string)
					} else {
						team_mem_string = team_mem_string + "," + member_id["fk_member_id"].(string)
					}
					receivers = append(receivers, member_id["fk_member_id"].(string))

				}
				team_mem_string = "{" + team_mem_string + "}"

				fmt.Println("team_mem_string ", team_mem_string)
			}

			_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
			team_name := maps[0]["team_name"].(string)
			team_slug := maps[0]["slug"].(string)

			sender_email := ""
			sender_name := ""
			sender_instant_email := ""
			sender_details := GetUserDetails(user_id)
			sender_email = sender_details["user"]["email_address"].(string)

			if sender_details["user"]["name"] != nil {
				sender_name = sender_details["user"]["name"].(string)
			}
			if sender_details["user"]["last_name"] != nil {
				sender_name = sender_name + " " + sender_details["user"]["last_name"].(string)
			}
			if sender_details["user"]["instant_emails"] != nil {
				sender_instant_email = sender_details["user"]["instant_emails"].(string)
			}

			teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

			desc := strings.Replace(constants.TEAM_DISCUSSION_DIGEST_NOTIFICATION, "#teamName", teamUrl, -1)

			instant_email := strings.Replace(constants.TEAM_DISCUSSION_INSTANT_NOTIFICATION, "#teamName", teamUrl, -1)
			instant_email = strings.Replace(instant_email, "#senderName", sender_name, -1)

			exists, err := o.Raw("SELECT * FROM notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%post%'", "9", team_id).Values(&maps)

			if exists == 0 {
				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", team_id, nil, user_id, team_mem_string).Values(&maps)
				if err == nil {
					status = "true"
				} else {
					fmt.Println("Notification failed")
				}

			} else {
				_, err := o.Raw("UPDATE notification_list  SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE id = ( SELECT id from notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%post%' LIMIT 1 )", "1", time.Now(), desc, user_id, team_mem_string, "9", team_id).Values(&maps)
				if err == nil {
					status = "true"
				} else {
					fmt.Println("Notification failed")
				}
			}

			if data["DiscussionMsg"] != "" {

				user_details := GetUserDetails(user_id)
				name := user_details["user"]["name"].(string)
				desc = desc + "<br><span style='padding-left: 4em; '>" + name + ": </span><span style='font-style:italic;'>" + data["DiscussionMsg"] + "</span>"
			}

			for _, r := range receivers {

				receiver_details := GetUserDetails(r)
				receiver_email := receiver_details["user"]["email_address"].(string)

				receiver_name := ""
				receiverFirstname := ""
				if receiver_details["user"]["name"] != nil {
					receiver_name = receiver_details["user"]["name"].(string)
					receiverFirstname = receiver_name
				}
				if receiver_details["user"]["last_name"] != nil {
					receiver_name = receiver_name + " " + receiver_details["user"]["last_name"].(string)
				}

				send_email_digest := receiver_details["user"]["subscribe"]
				send_instant_emails := receiver_details["user"]["instant_emails"]

				send_instant_sms_dial_code := ""
				if receiver_details["user"]["country_dial_code"] != nil {
					send_instant_sms_dial_code = receiver_details["user"]["country_dial_code"].(string)
				}
				send_instant_sms_phone := ""
				if receiver_details["user"]["phone"] != nil {
					send_instant_sms_phone = receiver_details["user"]["phone"].(string)
				}
				send_instant_sms := receiver_details["user"]["instant_sms"].(string)

				fmt.Println(send_instant_sms_dial_code)
				fmt.Println(send_instant_sms_phone)
				fmt.Println(send_instant_sms)

				if data["SendAs"] == "digest" {
					fmt.Printf("Send email digest = %s \n", send_email_digest)
					if send_email_digest != nil {
						send_email_digest = send_email_digest.(string)
						if send_email_digest == "1" {
							//exists, _ = o.Raw("SELECT * FROM email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%post%'", "12", receiver_email ).Values(&maps)

							//if exists == 0 {
							_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "12", desc, receiver_email, time.Now(), "0", r, team_id).Values(&maps)

							//} else {
							//	_, err = o.Raw("UPDATE email_digest SET active = ?, message = ? WHERE id = ( SELECT id from email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%post%' LIMIT 1 )","0", desc, "12", receiver_email ).Values(&maps)

							//}
						}
					}
				} else if data["SendAs"] == "instant" {
					if send_instant_emails != nil {
						send_instant_emails = send_instant_emails.(string)
						if send_instant_emails == "1" {
							footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
							unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

							html_email_message := "<html> Hi " + receiver_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["DiscussionMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

							if sender_email != receiver_email {

								common.SendDiscussionEmail(receiver_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
							}

						}
					}
				}

				if data["SendAsSms"] == "instant_sms" {
					isSmsStatus := "false"
					isErrorDuringSending := "false"
					if send_instant_sms == "1" && send_instant_sms_dial_code != "" && send_instant_sms_phone != "" && sms_limits > 0 {
						isSmsStatus = "true"
						toSms := send_instant_sms_dial_code + send_instant_sms_phone
						isErrorDuringSending = smscommon.SendSms(toSms, data["PlainDiscussionMsg"], "", "")
						if isErrorDuringSending == "true" {
							isSmsSentToAnyone++
						}
					} else {
						isSmsStatus = "false"
					}
					userSmsErrorMsg := ""

					if isSmsSentToAnyone > 0 && isSmsStatus == "true" {
						if temp_team_sms_subscription_limits > 0 {
							_, err := o.Raw("update sms_package set sms_limits = sms_limits-1 where fk_team_id = ?", team_id).Values(&maps)
							if err == nil {
								fmt.Println("The sms of this team is substracted by 1 from the default sms count")
							}
						} else if temp_team_sms_limits > 0 {
							_, err := o.Raw("update team set sms_limits = sms_limits-1 where id = ?", team_id).Values(&maps)
							if err == nil {
								fmt.Println("The sms of this team is substracted by 1 from the default sms count")
							}
						}
					}

					if sms_limits < 1 {
						userSmsErrorMsg = "invalid SMS credits"
					} else if send_instant_sms == "0" {
						userSmsErrorMsg = "unsubscribed to SMS"
					} else if isSmsStatus == "false" {
						userSmsErrorMsg = "invalid phone"
					} else if isErrorDuringSending == "false" {
						userSmsErrorMsg = "Twilio not sending the message"
					}
					smsResponseStatusObj := smsResponseStatus{
						EmailAddress:             receiver_email,
						DisplayName:              receiverFirstname,
						MobileNumberWithDialCode: send_instant_sms_dial_code + "-" + send_instant_sms_phone,
						SmsStatus:                isSmsStatus,
						InstantSms:               send_instant_sms,
						UserSMSError:             userSmsErrorMsg,
					}

					if sms_limits > 0 {
						_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
						team_sms_limits = maps[0]["sms_limits"].(string)

						num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ? ", team_id).Values(&maps1)
						if num > 0 {
							team_sms_subscription_limits = maps1[0]["sms_limits"].(string)
						}
						temp_team_sms_limits, _ = strconv.Atoi(team_sms_limits)
						temp_team_sms_subscription_limits, _ = strconv.Atoi(team_sms_subscription_limits)
						sms_limits = temp_team_sms_limits + temp_team_sms_subscription_limits
					}

					smsResponseStatusList = append(smsResponseStatusList, smsResponseStatusObj)
				}

				if err != nil {
					fmt.Println("Email Digest Failed ", receiver_email)
				}
			}

			fmt.Println(smsResponseStatusList)

			//CC the mail to the sender

			if data["SendAs"] == "instant" {

				if sender_instant_email == "1" {
					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

					html_email_message := "<html> Hi " + sender_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["DiscussionMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

					common.SendDiscussionEmail(sender_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
				}

			}

		} else {
			team_mem, _ := o.Raw("SELECT fk_co_captains FROM team WHERE id = ? AND active = 1 ", team_id).Values(&maps)

			if team_mem > 0 {

				//captain_id := maps[0]["fk_captain_id"].(string)
				captain_id_string := maps[0]["fk_co_captains"].(string)

				_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
				team_name := maps[0]["team_name"].(string)
				team_slug := maps[0]["slug"].(string)

				sender_email := ""
				sender_name := ""
				sender_instant_email := ""

				sender_details := GetUserDetails(user_id)
				sender_email = sender_details["user"]["email_address"].(string)

				if sender_details["user"]["name"] != nil {
					sender_name = sender_details["user"]["name"].(string)
				}
				if sender_details["user"]["last_name"] != nil {
					sender_name = sender_name + " " + sender_details["user"]["last_name"].(string)
				}
				if sender_details["user"]["instant_emails"] != nil {
					sender_instant_email = sender_details["user"]["instant_emails"].(string)
				}

				teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

				desc := strings.Replace(constants.TEAM_DISCUSSION_DIGEST_NOTIFICATION, "#teamName", teamUrl, -1)

				instant_email := strings.Replace(constants.TEAM_DISCUSSION_INSTANT_NOTIFICATION, "#teamName", teamUrl, -1)
				instant_email = strings.Replace(instant_email, "#senderName", sender_name, -1)

				exists, err := o.Raw("SELECT * FROM notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%post%'", "9", team_id).Values(&maps)

				if exists == 0 {
					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", team_id, nil, user_id, captain_id_string).Values(&maps)
					if err == nil {
						status = "true"
					} else {
						fmt.Println("Notification failed")
					}

				} else {
					_, err := o.Raw("UPDATE notification_list SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE id = ( SELECT id FROM notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%post%' LIMIT 1 )", "1", time.Now(), desc, user_id, captain_id_string, "9", team_id).Values(&maps)
					if err == nil {
						status = "true"
					} else {
						fmt.Println("Notification failed")
					}
				}

				s := strings.Replace(captain_id_string, "{", "", -1)
				s = strings.Replace(s, "}", "", -1)
				receivers := strings.Split(s, ",")

				if data["DiscussionMsg"] != "" {

					user_details := GetUserDetails(user_id)
					name := user_details["user"]["name"].(string)
					desc = desc + "<br><span style='padding-left: 4em; '>" + name + ": </span><span style='font-style:italic;'>" + data["DiscussionMsg"] + "</span>"
				}

				for _, rid := range receivers {
					receiver_details := GetUserDetails(rid)
					receiver_email := receiver_details["user"]["email_address"].(string)

					receiver_name := ""
					receiverFirstname := ""
					if receiver_details["user"]["name"] != nil {
						receiver_name = receiver_details["user"]["name"].(string)
						receiverFirstname = receiver_name
					}
					if receiver_details["user"]["last_name"] != nil {
						receiver_name = receiver_name + " " + receiver_details["user"]["last_name"].(string)
					}

					send_email_digest := receiver_details["user"]["subscribe"].(string)
					send_instant_emails := receiver_details["user"]["instant_emails"].(string)

					send_instant_sms_dial_code := ""
					if receiver_details["user"]["country_dial_code"] != nil {
						send_instant_sms_dial_code = receiver_details["user"]["country_dial_code"].(string)
					}
					send_instant_sms_phone := ""
					if receiver_details["user"]["phone"] != nil {
						send_instant_sms_phone = receiver_details["user"]["phone"].(string)
					}
					send_instant_sms := receiver_details["user"]["instant_sms"].(string)

					fmt.Println(send_instant_sms_dial_code)
					fmt.Println(send_instant_sms_phone)
					fmt.Println(send_instant_sms)

					if data["SendAs"] == "digest" {
						if send_email_digest == "1" {
							//exists, _ = o.Raw("SELECT * FROM email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%post%'", "12", receiver_email ).Values(&maps)

							//if exists == 0 {
							_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "12", desc, receiver_email, time.Now(), "0", rid, team_id).Values(&maps)

							//} else {
							//_, err = o.Raw("UPDATE email_digest SET active = ?, message = ? WHERE id = ( SELECT id FROM email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%post%' LIMIT 1 )","0", desc, "12", receiver_email ).Values(&maps)

							//}
						}
					} else if data["SendAs"] == "instant" {

						if send_instant_emails == "1" {
							footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

							unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

							html_email_message := "<html> Hi " + receiver_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["DiscussionMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

							if sender_email != receiver_email {
								common.SendDiscussionEmail(receiver_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
							}
						}

					}

					if data["SendAsSms"] == "instant_sms" {
						isErrorDuringSending := "false"
						isSmsStatus := ""
						if send_instant_sms == "1" && send_instant_sms_dial_code != "" && send_instant_sms_phone != "" && sms_limits > 0 {
							isSmsStatus = "true"
							toSms := send_instant_sms_dial_code + send_instant_sms_phone
							isErrorDuringSending = smscommon.SendSms(toSms, data["PlainDiscussionMsg"], "", "")
							if isErrorDuringSending == "true" {
								isSmsSentToAnyone++
							}
						} else {
							isSmsStatus = "false"
						}

						userSmsErrorMsg := ""

						if isSmsSentToAnyone > 0 && isSmsStatus == "true" {
							if temp_team_sms_subscription_limits > 0 {
								_, err := o.Raw("update sms_package set sms_limits = sms_limits-1 where fk_team_id = ?", team_id).Values(&maps)
								if err == nil {
									fmt.Println("The sms of this team is substracted by 1 from the default sms count")
								}
							} else if temp_team_sms_limits > 0 {
								_, err := o.Raw("update team set sms_limits = sms_limits-1 where id = ?", team_id).Values(&maps)
								if err == nil {
									fmt.Println("The sms of this team is substracted by 1 from the default sms count")
								}
							}
						}

						if sms_limits < 1 {
							userSmsErrorMsg = "invalid SMS credits"
						} else if send_instant_sms == "0" {
							userSmsErrorMsg = "unsubscribed to SMS"
						} else if isSmsStatus == "false" {
							userSmsErrorMsg = "invalid phone"
						} else if isErrorDuringSending == "false" {
							userSmsErrorMsg = "Twilio not sending the message"
						}
						smsResponseStatusObj := smsResponseStatus{
							EmailAddress:             receiver_email,
							DisplayName:              receiverFirstname,
							MobileNumberWithDialCode: send_instant_sms_dial_code + "-" + send_instant_sms_phone,
							SmsStatus:                isSmsStatus,
							InstantSms:               send_instant_sms,
							UserSMSError:             userSmsErrorMsg,
						}

						if sms_limits > 0 {
							_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
							team_sms_limits = maps[0]["sms_limits"].(string)

							num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ? ", team_id).Values(&maps1)
							if num > 0 {
								team_sms_subscription_limits = maps1[0]["sms_limits"].(string)
							}
							temp_team_sms_limits, _ = strconv.Atoi(team_sms_limits)
							temp_team_sms_subscription_limits, _ = strconv.Atoi(team_sms_subscription_limits)
							sms_limits = temp_team_sms_limits + temp_team_sms_subscription_limits
						}

						smsResponseStatusList = append(smsResponseStatusList, smsResponseStatusObj)
					}

					if err != nil {
						fmt.Println("Email Digest Failed ", receiver_email)
					}
				}

				//CC the mail to the sender

				if data["SendAs"] == "instant" {

					if sender_instant_email == "1" {
						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

						unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html> Hi " + sender_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["DiscussionMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

						common.SendDiscussionEmail(sender_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
					}

				}

			} else {
				fmt.Println("No captain exists")
			}
		}
		status = "true"
	}

	communicatioRespObj := CommunicationResponse{Status: status, SmsResponseStatus: smsResponseStatusList}
	return communicatioRespObj

}

func AddTeamDiscussionReply(data map[string]string, team_id string, user_id string) string {

	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var receivers []string
	var send_as string

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, err := o.Raw("INSERT INTO replydiscussion (discussion_reply_msg, notify_type, created_at, fk_discussion_id, fk_user_id ) VALUES (?,?,?,?,?) RETURNING id", data["ReplyMsg"], data["Type"], time.Now(), data["DisId"], user_id).Values(&maps)

	count, _ := o.Raw("SELECT sent_as FROM team_discussion WHERE id = ? ", data["DisId"]).Values(&maps)

	if count > 0 {
		if maps[0]["sent_as"] == nil {
			send_as = "digest"
		} else if maps[0]["sent_as"] == "digest" {
			send_as = "digest"
		} else if maps[0]["sent_as"] == "instant" {
			send_as = "instant"
		}
	}

	if err == nil {

		team_mem, _ := o.Raw("SELECT fk_member_id FROM teamjoin WHERE fk_team_id = ? AND request_status = 2 AND active = 1 AND fk_member_id IS NOT NULL", team_id).Values(&maps)

		team_mem_string := ""

		if team_mem > 0 {
			for i, member_id := range maps {
				if member_id != nil {
					fmt.Println(member_id, i)
					if i == 0 {
						team_mem_string = team_mem_string + member_id["fk_member_id"].(string)
					} else {
						team_mem_string = team_mem_string + "," + member_id["fk_member_id"].(string)
					}
					receivers = append(receivers, member_id["fk_member_id"].(string))
				}

			}
			team_mem_string = "{" + team_mem_string + "}"

			fmt.Println("team_mem_string ", team_mem_string)
		}

		_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

		team_name := maps[0]["team_name"].(string)
		team_slug := maps[0]["slug"].(string)

		sender_email := ""
		sender_name := ""
		sender_instant_email := ""

		teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

		desc := strings.Replace(constants.TEAM_DISCUSSION_REPLY_DIGEST_NOTIFICATION, "#teamName", teamUrl, -1)

		exists, err := o.Raw("SELECT * FROM notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%reply%'", "9", team_id).Values(&maps)

		if exists == 0 {
			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", team_id, nil, user_id, team_mem_string).Values(&maps)
			if err == nil {
				status = "true"
			} else {
				fmt.Println("Notification failed")
			}

		} else {
			_, err := o.Raw("UPDATE notification_list SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE id = ( SELECT id FROM notification_list WHERE type = ? AND fk_team_id = ? AND notification_desc LIKE '%reply%' LIMIT 1 )", "1", time.Now(), desc, user_id, team_mem_string, "9", team_id).Values(&maps)
			if err == nil {
				status = "true"
			} else {
				fmt.Println("Notification failed")
			}
		}

		if data["ReplyMsg"] != "" {

			user_details := GetUserDetails(user_id)
			name := user_details["user"]["name"].(string)
			sender_email = user_details["user"]["email_address"].(string)

			if user_details["user"]["name"] != nil {
				sender_name = user_details["user"]["name"].(string)
			}
			if user_details["user"]["last_name"] != nil {
				sender_name = sender_name + " " + user_details["user"]["last_name"].(string)
			}
			if user_details["user"]["instant_emails"] != nil {
				sender_instant_email = user_details["user"]["instant_emails"].(string)
			}

			desc = desc + "<br><span style='padding-left: 4em; '>" + name + ": </span><span style='font-style:italic;'>" + data["ReplyMsg"] + "</span>"
		}

		instant_email := strings.Replace(constants.TEAM_DISCUSSION_INSTANT_NOTIFICATION, "#teamName", teamUrl, -1)
		instant_email = strings.Replace(instant_email, "#senderName", sender_name, -1)

		for _, r := range receivers {

			receiver_details := GetUserDetails(r)
			receiver_email := receiver_details["user"]["email_address"].(string)

			receiver_name := ""
			if receiver_details["user"]["name"] != nil {
				receiver_name = receiver_details["user"]["name"].(string)
			}
			if receiver_details["user"]["last_name"] != nil {
				receiver_name = receiver_name + " " + receiver_details["user"]["last_name"].(string)
			}

			send_email_digest := receiver_details["user"]["subscribe"].(string)
			send_instant_emails := receiver_details["user"]["instant_emails"].(string)

			if send_as == "digest" {

				if send_email_digest == "1" {
					//exists, _ = o.Raw("SELECT * FROM email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%reply%'", "13", receiver_email ).Values(&maps)

					//if exists == 0 {
					_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "13", desc, receiver_email, time.Now(), "0", r, team_id).Values(&maps)

					//} else {
					//	_, err = o.Raw("UPDATE email_digest SET active = ?, message= ? WHERE id = ( SELECT id FROM email_digest WHERE type = ? AND receiver_email = ? AND message LIKE '%reply%' LIMIT 1 )","0", desc, "13", receiver_email ).Values(&maps)

					//}
				}
			} else if send_as == "instant" {

				if send_instant_emails == "1" {
					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

					html_email_message := "<html> Hi " + receiver_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["ReplyMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

					if sender_email != receiver_email {
						common.SendDiscussionEmail(receiver_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
					}

				}

			}

			if err != nil {
				fmt.Println("Email Digest Failed ", receiver_email)
			}
		}

		//CC the mail to the sender

		if send_as == "instant" {

			if sender_instant_email == "1" {
				footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
				unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
				html_email_message := "<html> Hi " + sender_name + ", <br/><br/>" + instant_email + "<div style='font-style:italic;padding-left: 2em;'>" + data["ReplyMsg"] + "</div>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

				common.SendEmail(sender_email+"-"+sender_email, constants.TEAM_MEMBER_INSTANT_EMAIL_SUBJECT, html_email_message)
			}

		}

		status = "true"

	}

	return status

}

func DeleteTeam(team_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	_, err := o.Raw("UPDATE notification_list SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE teamjoin SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE team_join_request SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE team_practices SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE team_sponsors SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE team_waivers SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)
	_, err = o.Raw("UPDATE waivers SET active = ? WHERE fk_team_id = ? ", "7", team_id).Values(&maps)

	deleted_slug := GetTeamSlug(team_id)
	deleted_slug = deleted_slug + "_old_" + time.Now().Format("20060102150405")

	_, err = o.Raw("UPDATE team SET active = ?, slug = ? WHERE id = ? ", "7", deleted_slug, team_id).Values(&maps)

	if err == nil {
		status = "true"
	}

	//send mail

	return status
}

func SaveTeamSponsorUrl(team_id string, path string, fileName string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("INSERT INTO team_sponsors (active, fk_team_id) VALUES (?,?) RETURNING id", "1", team_id).Values(&maps)

	if err == nil {
		_, err := o.Raw("INSERT INTO sponsers (fk_team_sponser_id, name, path) VALUES (?,?,?) RETURNING id", maps[0]["id"].(string), fileName, url).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			status = "false"
			fmt.Println(err)
		}

	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func GetTeamSponsor(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("Team id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM team_sponsors join sponsers on team_sponsors.id = sponsers.fk_team_sponser_id WHERE team_sponsors.active = 1 AND team_sponsors.fk_team_id = ? ", id).Values(&maps)

	if num > 0 {
		for k, v := range maps {
			key := "sponsor_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No teamm Logo")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func RemoveTeamSponsor(data map[string][]string, team_id string, user_id string, removed_by string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params
	code := ""

	if removed_by == "super_admin" {
		code = "7"
	} else {
		code = "9"
	}

	//_, _ = o.Raw("SELECT team_name FROM team WHERE id = ? ", team_id).Values(&maps)

	//desc := "You have been removed team " + maps[0]["team_name"].(string)

	for _, sponsor_id := range data["SponsorIds"] {
		_, err := o.Raw("UPDATE team_sponsors SET active = ? WHERE id = ? ", code, sponsor_id).Values(&maps)

		if err == nil {

			/*member_id = "{" + member_id + "}"

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id","{9}", "1", time.Now(), true, desc, "9", team_id , nil, user_id, member_id ).Values(&maps)

			if err == nil {
				status = "true"
			} else {
				fmt.Println("Notification failed")
			}*/

			status = "true"

		}
	}

	//send mail

	return status
}

func AddTeamCoCaptain(co_captain_id string, team_id string) string {

	result := "false"

	fmt.Println("Team id ", team_id)

	o := orm.NewOrm()

	var maps []orm.Params
	tempco_captain_id := co_captain_id
	co_captain_id = "{" + co_captain_id + "}"
	_, err := o.Raw("UPDATE team SET fk_co_captains = array_cat(fk_co_captains, ? ) WHERE id = ? ", co_captain_id, team_id).Values(&maps)
	if err == nil {
		AddTeamCaptainRoleToUser(tempco_captain_id)
		fmt.Println("Made co captain")
		result = "true"

	} else {
		fmt.Println("Failed make co captain")
		result = "false"
	}

	return result

}

func RemoveTeamCoCaptain(co_captain_id string, logged_user_id string, team_id string) string {

	result := "false"

	fmt.Println("Team id ", team_id)

	o := orm.NewOrm()

	var maps []orm.Params
	//co_captain_id = "{" + co_captain_id + "}"

	row, _ := o.Raw("SELECT fk_co_captains FROM team WHERE id = ? GROUP BY fk_co_captains Having array_length(fk_co_captains,1) > 0", team_id).Values(&maps)

	if row != 0 {

		_, err := o.Raw("UPDATE team SET fk_co_captains = array_remove(fk_co_captains, ? ) WHERE id = ? Returning fk_captain_id", co_captain_id, team_id).Values(&maps)

		if err == nil {
			fmt.Println("Removed co captain")

			//If co-captain removes main captain, make co-captain the main captain

			if maps[0]["fk_captain_id"] != nil {
				if maps[0]["fk_captain_id"] == co_captain_id {

					is_cap, _ := o.Raw("SELECT * FROM team  WHERE fk_co_captains @> ARRAY [?]::bigint[] AND id = ?", logged_user_id, team_id).Values(&maps)

					if is_cap > 0 {
						_, err = o.Raw("UPDATE team SET fk_captain_id =  ? WHERE id = ?", logged_user_id, team_id).Values(&maps)
					}

				} else {

					// case when there are only co-captains and no main captain
					is_cap, _ := o.Raw("SELECT * FROM team  WHERE fk_co_captains @> ARRAY [?]::bigint[] AND id = ?", maps[0]["fk_captain_id"], team_id).Values(&maps)

					if is_cap == 0 {

						is_cap, _ = o.Raw("SELECT * FROM team  WHERE fk_co_captains @> ARRAY [?]::bigint[] AND id = ?", logged_user_id, team_id).Values(&maps)

						if is_cap > 0 {
							_, err = o.Raw("UPDATE team SET fk_captain_id =  ? WHERE id = ?", logged_user_id, team_id).Values(&maps)
						}
					}
				}
			}
			result = "true"

			num, _ := o.Raw("SELECT team_name, slug FROM team t WHERE t.id = ? ", team_id).Values(&maps)
			if num > 0 {
				team_name := maps[0]["team_name"].(string)
				team_slug := maps[0]["slug"].(string)

				site_url := ""
				if len(os.Getenv("SITE_URL")) > 0 {
					site_url = os.Getenv("SITE_URL")
				} else {
					site_url = "http://localhost:5000/"
				}

				team_name = "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

				member_id := "{" + co_captain_id + "}"

				cap_rem_desc := strings.Replace(constants.TEAM_CAPTAIN_REMOVED, "#teamName", team_name, -1)

				/*_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id","{9}", "1", time.Now(), true, cap_rem_desc, "9", team_id , nil, logged_user_id, member_id ).Values(&maps)*/

				exists, _ := o.Raw("SELECT * FROM notification_list WHERE type = ? AND fk_team_id = ? AND fk_receiver @> ARRAY [?]::bigint[] AND notification_desc LIKE '%You have been removed as a captain from%'", "9", team_id, co_captain_id).Values(&maps)

				if exists == 0 {
					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, cap_rem_desc, "9", team_id, nil, logged_user_id, member_id).Values(&maps)
					if err == nil {
						result = "true"
					} else {
						fmt.Println("Notification failed")
					}

				} else {
					_, err := o.Raw("UPDATE notification_list  SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE id = ( SELECT id from notification_list WHERE type = ? AND fk_team_id = ? AND fk_receiver @> ARRAY [?]::bigint[] AND notification_desc LIKE '%You have been removed as a captain from%' LIMIT 1 )", "1", time.Now(), cap_rem_desc, logged_user_id, member_id, "9", team_id, co_captain_id).Values(&maps)
					if err == nil {
						result = "true"
					} else {
						fmt.Println("Notification failed")
					}
				}

			}

		} else {
			fmt.Println("Failed to remove co captain")
			result = "false"
		}
	} else {
		result = "false"
	}

	return result

}

func EditTeamClasses(data map[string]string, user_id string, team_id string, Mdivision []string, Wdivision []string, MXdivision []string) string {

	result := "false"
	fmt.Println("EditEventClasses")
	fmt.Println(data)
	o := orm.NewOrm()

	var maps []orm.Params
	var values []orm.Params

	_, err := o.Raw("DELETE FROM team_class_divisions WHERE fk_team_id = ? ", team_id).Values(&maps)

	if err == nil {

		if data["Mdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", team_id, "M", data["Mdivision"]).Values(&values)
		}

		if data["Wdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", team_id, "W", data["Wdivision"]).Values(&values)
		}
		if data["MXdivision"] != "{}" {
			_, err = o.Raw("INSERT INTO team_class_divisions (fk_team_id, class, div) VALUES (?,?,?)", team_id, "MX", data["MXdivision"]).Values(&values)
		}

		result = "true"
	}

	return result

}

func SaveTeamNoteUrl(team_id string, user_id string, user_name string, path string, fileName string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("INSERT INTO team_uploads (active, file_name, file_url, uploaded_by_name, uploaded_date, fk_team_id, fk_uploaded_by) VALUES (?,?,?,?,?,?,?) RETURNING id", "1", fileName, url, user_name, time.Now(), team_id, user_id).Values(&maps)

	if err == nil {
		status = "true"

	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func GetTeamNote(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("Team id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM team_uploads WHERE active = 1 AND fk_team_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "note_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No team notes")
		result["status"] = nil
	}

	return result

}

func RemoveTeamNote(data map[string][]string, team_id string, user_id string, removed_by string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params
	code := ""

	if removed_by == "super_admin" {
		code = "7"
	} else {
		code = "9"
	}

	//_, _ = o.Raw("SELECT team_name FROM team WHERE id = ? ", team_id).Values(&maps)

	//desc := "You have been removed team " + maps[0]["team_name"].(string)

	for _, file_id := range data["FileIds"] {
		_, err := o.Raw("UPDATE team_uploads SET active = ? WHERE id = ? ", code, file_id).Values(&maps)

		if err == nil {

			/*member_id = "{" + member_id + "}"

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id","{9}", "1", time.Now(), true, desc, "9", team_id , nil, user_id, member_id ).Values(&maps)

			if err == nil {
				status = "true"
			} else {
				fmt.Println("Notification failed")
			}*/

			status = "true"

		}
	}

	//send mail

	return status
}

func RemoveTeamPractice(practice_id string, logged_user_id string, delete_option string, repeats string, team_id string) string {
	result := "false"

	fmt.Println("In RemoveTeamPractice")

	var practice_ids []string

	o := orm.NewOrm()

	var mapsData []orm.Params

	_, _ = o.Raw("select user_time_zone  from team_practices WHERE id = ? ", practice_id).Values(&mapsData)

	user_time_zone := ""
	if mapsData[0]["user_time_zone"] != nil {
		user_time_zone = mapsData[0]["user_time_zone"].(string)
	}

	if repeats == "Weekly" {

		var maps []orm.Params

		if delete_option == "only" {

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id = ? ", "9", practice_id).Values(&maps)

			if err == nil {
				deletePracticeRoster(practice_id)
				result = "true"
			} else {
				result = "false"
			}

		} else if delete_option == "all_following" {

			_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", practice_id).Values(&maps)

			layout := "2006-01-02T15:04:05Z07:00"
			initial_date := ""

			if maps[0]["practice_date"] != nil {
				initial_date = maps[0]["practice_date"].(string)
			}

			if initial_date != "" {

				// get following practices for the series
				t, err := time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, 0, 7)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get current practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

					if count > 0 {
						for _, p := range maps {
							practice_ids = append(practice_ids, p["id"].(string))
						}
					}

				}
			}

			fmt.Println("Rows to be edited ", practice_ids)

			practice_ids_string := strings.Join(practice_ids[:], ",")

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id  IN ( "+practice_ids_string+" )", "9").Values(&maps)

			if err == nil {
				deletePracticeRoster(practice_ids_string)
				result = "true"
			} else {
				result = "false"
			}

		} else if delete_option == "all" {

			_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", practice_id).Values(&maps)

			layout := "2006-01-02T15:04:05Z07:00"
			initial_date := ""

			if maps[0]["practice_date"] != nil {
				initial_date = maps[0]["practice_date"].(string)
			}

			if initial_date != "" {

				// get following practices for the series
				t, err := time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, 0, 7)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get previous practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, 0, -7)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get current practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

					if count > 0 {
						for _, p := range maps {
							practice_ids = append(practice_ids, p["id"].(string))
						}
					}

				}
			}

			fmt.Println("Rows to be edited ", practice_ids)

			practice_ids_string := strings.Join(practice_ids[:], ",")

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id  IN ( "+practice_ids_string+" )", "9").Values(&maps)

			if err == nil {
				deletePracticeRoster(practice_ids_string)
				result = "true"
			} else {
				result = "false"
			}

		}

	} else if repeats == "Monthly" {

		var maps []orm.Params

		if delete_option == "only" {

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id = ? ", "9", practice_id).Values(&maps)

			if err == nil {
				deletePracticeRoster(practice_id)
				result = "true"
			} else {
				result = "false"
			}

		} else if delete_option == "all_following" {

			_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", practice_id).Values(&maps)

			layout := "2006-01-02T15:04:05Z07:00"
			initial_date := ""

			if maps[0]["practice_date"] != nil {
				initial_date = maps[0]["practice_date"].(string)
			}

			if initial_date != "" {

				// get following practices for the series
				t, err := time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, 1, 0)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get current practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

					if count > 0 {
						for _, p := range maps {
							practice_ids = append(practice_ids, p["id"].(string))
						}
					}

				}
			}

			fmt.Println("Rows to be edited ", practice_ids)

			practice_ids_string := strings.Join(practice_ids[:], ",")

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id  IN ( "+practice_ids_string+" )", "9").Values(&maps)

			if err == nil {
				result = "true"
				deletePracticeRoster(practice_ids_string)
			} else {
				result = "false"
			}

		} else if delete_option == "all" {

			_, _ = o.Raw("SELECT practice_date FROM team_practices WHERE id = ? ", practice_id).Values(&maps)

			layout := "2006-01-02T15:04:05Z07:00"
			initial_date := ""

			if maps[0]["practice_date"] != nil {
				initial_date = maps[0]["practice_date"].(string)
			}

			if initial_date != "" {

				// get following practices for the series
				t, err := time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, 1, 0)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get previous practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					for {
						t = t.AddDate(0, -1, 0)

						count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

						if count > 0 {
							for _, p := range maps {
								practice_ids = append(practice_ids, p["id"].(string))
							}
							t, _ = time.Parse(layout, maps[0]["practice_date"].(string))
						} else {
							break
						}
					}

				}

				// get current practices for the series
				t, err = time.Parse(layout, initial_date)

				if err != nil {
					fmt.Println(err)
				} else {

					count, _ := o.Raw("SELECT id, practice_date FROM team_practices WHERE practice_date = ? AND (repeats != ? OR repeats IS NOT NULL) AND fk_team_id = ? ORDER BY id ASC", t, "", team_id).Values(&maps)

					if count > 0 {
						for _, p := range maps {
							practice_ids = append(practice_ids, p["id"].(string))
						}
					}

				}
			}

			fmt.Println("Rows to be edited ", practice_ids)

			practice_ids_string := strings.Join(practice_ids[:], ",")

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id  IN ( "+practice_ids_string+" )", "9").Values(&maps)

			if err == nil {
				result = "true"
				deletePracticeRoster(practice_ids_string)
			} else {
				result = "false"
			}

		}

	} else {
		var maps []orm.Params

		if delete_option == "only" {

			_, err := o.Raw("UPDATE team_practices set active = ? WHERE id = ? ", "9", practice_id).Values(&maps)

			if err == nil {
				deletePracticeRoster(practice_id)
				result = "true"
			} else {
				result = "false"
			}
		}

	}

	var maps []orm.Params

	if result == "true" {

		/*member_id = "{" + member_id + "}"

		_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id","{9}", "1", time.Now(), true, desc, "9", team_id , nil, user_id, member_id ).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			fmt.Println("Notification failed")
			}*/

		result = "true"

		num, _ := o.Raw("SELECT team_name, t.id as team_id, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"')) as practiceLocalTimeDate,practice_date, practice_time, practice_location, repeats FROM team t INNER JOIN team_practices tp on t.id = tp.fk_team_id WHERE tp.id = ? ", practice_id).Values(&maps)

		if num > 0 {

			team_id := maps[0]["team_id"].(string)
			practice_date := maps[0]["practiceLocalTimeDate"].(string)
			practice_time := maps[0]["practice_time"].(string)

			team_members := GetTeamMembers(team_id)
			team_slug := GetTeamSlug(team_id)

			dt, _ := time.Parse(time.RFC3339, practice_date)
			formatted_date := dt.Format("Mon, Jan 2, 2006")

			digest_practice_period := ""
			if maps[0]["repeats"] == "Weekly" {
				digest_practice_period = "repeats weekly for 5 weeks"
			} else if maps[0]["repeats"] == "Monthly" {
				digest_practice_period = "repeats monthly for 5 weeks"
			} else {
				digest_practice_period = "repeats never"
			}

			for _, member := range team_members {

				member_id := member["fk_member_id"].(string)
				send_email_digest := member["subscribe"].(string)

				site_url := ""
				if len(os.Getenv("SITE_URL")) > 0 {
					site_url = os.Getenv("SITE_URL")
				} else {
					site_url = "http://localhost:5000/"
				}

				team_name := member["team_name"].(string)

				fmt.Printf("Team Name \n")

				team_name = "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
				practice_desc := strings.Replace(constants.TEAM_PRACTICE_REMOVED_DIGEST_NOTIFICATION, "#teamName", team_name, -1)
				fmt.Println(practice_desc)
				fmt.Println(team_name)
				practice_desc = strings.Replace(practice_desc, "#practiceDate", formatted_date, -1)
				practice_desc = strings.Replace(practice_desc, "#practiceTime", practice_time, -1)
				practice_desc = strings.Replace(practice_desc, "#practiceRepeats", digest_practice_period, -1)
				if send_email_digest == "1" {
					//desc := "Captain has added a practice for the team <a href='"+ site_url + team_name + "'>"
					_, _ = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "10", practice_desc, member["email_address"], time.Now(), "0", member["fk_member_id"], team_id).Values(&maps)

				}

				member_id = "{" + member_id + "}"

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, practice_desc, "9", team_id, nil, logged_user_id, member_id).Values(&maps)

				if err == nil {
					fmt.Printf("Notification Sent")
				} else {
					fmt.Printf("Notification Error Occured")
					fmt.Println(err)
				}
			}
		}
	}

	//send mail

	return result
}

func deletePracticeRoster(practice_id string) {
	o := orm.NewOrm()
	var rosterData []orm.Params
	_, _ = o.Raw("select id from team_roster WHERE fk_practice_id IN ( " + practice_id + " )").Values(&rosterData)

	var roster_ids []string

	for _, p := range rosterData {
		roster_ids = append(roster_ids, p["id"].(string))
	}

	if len(roster_ids) > 0 {
		roster_ids_string := strings.Join(roster_ids[:], ",")
		_, _ = o.Raw("UPDATE team_roster set type = ?, fk_practice_id = ? WHERE id  IN ( "+roster_ids_string+" )", "N/A", "0").Values(&rosterData)
	}
}

func SendEmailMessage(member_id string, msg string, user_id string, team_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var maps1 []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
	_, _ = o.Raw("select name as sender_fname  from user_profile WHERE fk_user_id = ? ", user_id).Values(&maps1)

	captain_name := ""
	captain_email := ""
	captionUserId := ""
	sender_name := ""
	if maps1[0]["sender_fname"] != nil {
		sender_name = maps1[0]["sender_fname"].(string)
	}
	team_name := maps[0]["team_name"].(string)
	//team_slug := maps[0]["slug"].(string)

	//teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
	captain_send_instant_emails := ""
	if maps[0]["fk_captain_id"] != nil {
		captionUserId = maps[0]["fk_captain_id"].(string)
		captain_details := GetUserDetails(maps[0]["fk_captain_id"].(string))
		captain_name = captain_details["user"]["name"].(string)
		captain_email = captain_details["user"]["email_address"].(string)
		captain_send_instant_emails = captain_details["user"]["instant_emails"].(string)

	}

	// desc := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION, "#teamName", teamUrl, -1)
	// desc = strings.Replace(desc, "#captain_name", captain_name, 1)
	// desc = strings.Replace(desc, "#captainMessage", msg, -1)

	desc := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION_BY_SENDER, "#sender_name", sender_name, -1)
	desc = strings.Replace(desc, "#captainMessage", msg, -1)

	receiver := "{" + member_id + "}"

	_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", team_id, nil, user_id, receiver).Values(&maps)

	if err == nil {
		status = "true"

		//send mail

		fmt.Printf("member_id = %s \n", member_id)
		member_details := GetUserDetails(member_id)
		member_name := member_details["user"]["name"].(string)
		email_address := member_details["user"]["email_address"].(string)
		send_instant_emails := member_details["user"]["instant_emails"].(string)

		if send_instant_emails == "1" {
			headerSubject := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_EMAIL_SUBJECT, "#teamName", team_name, -1)

			headerBodyData := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION_BY_SENDER, "#sender_name", sender_name, -1)
			//headerBodyData := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION, "#teamName", team_name, -1)
			headerBodyData = strings.Replace(headerBodyData, "#captain_name", captain_name, -1)
			headerBodyData = strings.Replace(headerBodyData, "#captainMessage", msg, -1)

			footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
			unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

			html_email_message := "<html>Hi " + member_name + ", <br/><br/>" + headerBodyData + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

			if user_id == captionUserId {
				if captain_send_instant_emails != "1" {
					captain_email = "xxx@gmail.com"
				}
				common.SendEmailWithCC(email_address+"-"+captain_email, headerSubject, html_email_message)
			} else {
				member_details := GetUserDetails(user_id)
				senderEmail_address := member_details["user"]["email_address"].(string)
				if captain_send_instant_emails != "1" {
					captain_email = "xxx@gmail.com"
				}

				if member_details["user"]["instant_emails"].(string) != "1" {
					senderEmail_address = "xxx@gmail.com"
				}

				common.SendEmailWithCC(email_address+"-"+captain_email+","+senderEmail_address, headerSubject, html_email_message)
			}

		}

	}
	return status
}

func SendTeamJoinRequest(msg string, user_id string, team_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	num, err := o.Raw("SELECT * FROM teamjoin WHERE active = 1 AND fk_team_id = ? AND fk_member_id = ? ", team_id, user_id).Values(&maps)

	if num > 0 {

		if maps[0]["request_status"] == "1" {
			status = "1"

		} else if maps[0]["request_status"] == "2" {

			status = "2"

		} else if maps[0]["request_status"] == "3" {

			status = "3"

		}

	} else {

		num, _ = o.Raw("SELECT * FROM team_join_request WHERE active = 1 AND fk_team_id = ? AND fk_member_id = ? ", team_id, user_id).Values(&maps)

		if num > 0 && maps[0]["request_status"] != "1" {
			/*if maps[0]["request_status"] == "1" {
				status = "4"

			} else */

			if maps[0]["request_status"] == "2" {

				status = "2"

			} else if maps[0]["request_status"] == "3" {

				status = "3"

			}

		} else {

			user_details := GetUserDetails(user_id)
			user_name := user_details["user"]["name"].(string)
			user_email := user_details["user"]["email_address"].(string)

			if num > 0 {
				_, err = o.Raw("UPDATE team_join_request SET active = 1, request_status = 1 WHERE fk_team_id = ? AND fk_member_id = ? ", team_id, user_id).Values(&maps)

			} else {
				_, err = o.Raw("INSERT INTO team_join_request (active, member_email, member_name, request_status, fk_team_id, fk_member_id) VALUES (?,?,?,?,?,?) RETURNING id", "1", user_email, user_name, "1", team_id, user_id).Values(&maps)
			}

			if err == nil {
				_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

				team_name := maps[0]["team_name"].(string)
				team_slug := maps[0]["slug"].(string)

				teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

				receiver_id := "{}"
				rc_id := ""
				receiver_email := ""

				if maps[0]["fk_captain_id"] != nil {

					receiver_id = "{" + maps[0]["fk_captain_id"].(string) + "}"

					receiver_details := GetUserDetails(maps[0]["fk_captain_id"].(string))
					rc_id = maps[0]["fk_captain_id"].(string)
					receiver_email = receiver_details["user"]["email_address"].(string)
				}

				desc := strings.Replace(constants.TEAM_JOIN_REQUEST_NOTIFICATION, "#userName", user_name, -1)
				desc = strings.Replace(desc, "#teamName", teamUrl, -1)
				desc = strings.Replace(desc, "#teamJoinMessage", msg, -1)

				exists, _ := o.Raw("SELECT * FROM notification_list WHERE type = ? AND fk_team_id = ? AND fk_receiver @> ARRAY [?]::bigint[]", "5", team_id, rc_id).Values(&maps)

				if exists == 0 {

					_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "5", team_id, nil, user_id, receiver_id).Values(&maps)

				} else {

					_, err = o.Raw("UPDATE notification_list SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE type = ? AND fk_team_id = ? AND fk_receiver @> ARRAY [?]::bigint[] ", "1", time.Now(), desc, user_id, receiver_id, "5", team_id, rc_id).Values(&maps)

				}

				if err == nil {
					status = "true"

					content := strings.Replace(constants.TEAM_JOIN_REQUEST_DIGEST, "#userName", user_name, -1)
					content = strings.Replace(content, "#teamName", teamUrl, -1)
					content = strings.Replace(content, "#teamJoinMessage", msg, -1)

					//exists, _ := o.Raw("SELECT * FROM email_digest WHERE type = ? AND receiver_email = ?", "5", receiver_email).Values(&maps)

					//if exists == 0 {

					_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "5", content, receiver_email, time.Now(), "0", rc_id, team_id).Values(&maps)

					//} else {

					//	_, err = o.Raw("UPDATE email_digest SET active = ?, inserted_date = ? WHERE type = ? AND receiver_email = ?","0", time.Now(), "5", receiver_email).Values(&maps)

					//}

					if err == nil {
						status = "true"
					} else {
						fmt.Println("Email Digest Failed")
					}

				}
			}

		}

	}

	return status
}

func AcceptTeamJoinRequest(user_id string, team_id string, notf_id string, member_id string) string {

	result := "true"

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	o := orm.NewOrm()

	var maps []orm.Params
	var values []orm.Params

	var email_array []string

	_, _ = o.Raw("SELECT fk_initiator_id FROM notification_list WHERE id = ? ", notf_id).Values(&maps)

	user_details := GetUserDetails(maps[0]["fk_initiator_id"].(string))
	user_email := user_details["user"]["email_address"].(string)

	email_array = append(email_array, user_email)

	//_,_,_,result = AddTeamMember(email_array , "", member_id, team_id)

	if result == "true" {
		_, err := o.Raw("UPDATE team_join_request SET active = ? WHERE fk_team_id = ? AND fk_member_id = ?", "2", team_id, member_id).Values(&values)

		num, _ := o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? AND fk_member_id = ? ", team_id, member_id).Values(&maps)
		if num > 0 {
			_, err = o.Raw("UPDATE teamjoin SET request_status = ?, active = ?, email = ?, date = ?, fk_inviters_id = ?  WHERE fk_team_id = ? AND fk_member_id = ? ", "2", "1", strings.ToLower(user_email), time.Now(), user_id, team_id, member_id).Values(&maps)

		} else {
			_, err = o.Raw("INSERT INTO teamjoin (request_status, active, email, date, fk_team_id, fk_member_id, fk_inviters_id ) VALUES (?,?,?,?,?,?,?) RETURNING id", "2", "1", strings.ToLower(user_email), time.Now(), team_id, member_id, user_id).Values(&maps)

		}

		_, err = o.Raw("UPDATE notification_list SET active = ? WHERE id = ? RETURNING fk_initiator_id", "9", notf_id).Values(&values)

		if err == nil {

			_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

			team_name := maps[0]["team_name"].(string)
			team_slug := maps[0]["slug"].(string)

			teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

			message := strings.Replace(constants.TEAM_JOIN_REQUEST_ACCEPT_DIGEST_NOTIFICATION, "#teamName", teamUrl, -1)

			receiver_id := "{" + values[0]["fk_initiator_id"].(string) + "}"

			receiver_details := GetUserDetails(values[0]["fk_initiator_id"].(string))
			rc_id := values[0]["fk_initiator_id"].(string)
			receiver_email := receiver_details["user"]["email_address"].(string)
			send_email_digest := receiver_details["user"]["subscribe"].(string)

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, message, "9", nil, team_id, nil, user_id, receiver_id).Values(&maps)

			if send_email_digest == "1" {
				_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id, team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "6", message, receiver_email, time.Now(), "0", rc_id, team_id).Values(&maps)

				if err == nil {
					result = "true"
				} else {
					fmt.Println("Email Digest Failed")
				}

				result = "true"
			}
		} else {
			fmt.Println("Notification failed")
		}
	} else {
		result = "false"
	}

	return result

}

func GetPendingReqEmail(member_ids []string, team_id string) []string {

	var result []string

	fmt.Println("Team id ", team_id)

	o := orm.NewOrm()

	var maps []orm.Params

	for _, mem := range member_ids {
		_, err := strconv.Atoi(mem)

		if err == nil {
			_, _ = o.Raw("SELECT email FROM teamjoin WHERE fk_member_id = ? AND fk_team_id = ? ", mem, team_id).Values(&maps)

			if maps[0]["email"] != nil {
				result = append(result, maps[0]["email"].(string))
			}
		} else {

			// users not in the system
			result = append(result, mem)
		}

	}

	return result

}

func TeamPracticeInfo(practice_id string, loginUserTimeZone string) ([]orm.Params, bool) {

	var result []orm.Params
	isOldPracticeStatus := false
	fmt.Println("Practice id ", practice_id)

	o := orm.NewOrm()

	var maps []orm.Params

	var mapsData []orm.Params

	_, _ = o.Raw("select user_time_zone  from team_practices WHERE id = ? ", practice_id).Values(&mapsData)

	user_time_zone := ""
	if mapsData[0]["user_time_zone"] != nil {
		user_time_zone = mapsData[0]["user_time_zone"].(string)
	}

	count, _ := o.Raw("SELECT *, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'MM/DD/YYYY') as practice_date_format, to_char((practices_end_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'MM/DD/YYYY') as practice_end_date_format, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'Dy, Mon DD') as practice_date_format_2 FROM team_practices WHERE id = ? ", practice_id).Values(&maps)

	if count > 0 {
		result = maps
		p_practice_date := maps[0]["practice_date"].(string)
		isOldPracticeStatus = common.IsFeaturePracticeOrNot(p_practice_date, user_time_zone)
	} else {
		result = nil
	}

	return result, isOldPracticeStatus

}

func RemoveTeamWaiver(team_waiver_id string, removed_by string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	code := ""

	if removed_by == "super_admin" {
		code = "3"
	} else {
		code = "9"
	}

	_, err := o.Raw("UPDATE team_waivers SET active = ? WHERE id = ? ", code, team_waiver_id).Values(&maps)

	if err == nil {
		status = "true"
	} else {
		fmt.Println("Cannot remove waiver")
	}

	return status
}

func ResendTeamInvite(member_ids []string, team_id string, user_id string, msg string) string {

	site_url := ""
	status := "false"
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

	team_name := maps[0]["team_name"].(string)
	team_slug := maps[0]["slug"].(string)

	teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

	for _, mem := range member_ids {
		_, err := strconv.Atoi(mem)

		valid := true

		if err != nil {
			// mem is an email address

			exists, err := o.Raw("SELECT * from users as us left join user_profile as up on up.fk_user_id = us.id WHERE lower(us.email_address) = ? and  up.active =1", strings.ToLower(mem)).Values(&maps)
			fmt.Println(err)
			if exists > 0 {

				mem_id := maps[0]["id"].(string)
				_, _ = o.Raw("UPDATE teamjoin SET fk_member_id = ?, active = ? WHERE email = ? AND fk_team_id = ? ", mem_id, 1, strings.ToLower(mem), team_id).Values(&maps)

				mem = mem_id
				valid = true

			} else {
				valid = false
				num, _ := o.Raw("SELECT * from users WHERE lower(email_address) = ? ", mem).Values(&maps1)
				if num > 0 {
					mem = maps1[0]["email_address"].(string)
				}
			}

		} else {
			//mem is an user id
			num, _ := o.Raw("SELECT * from users as us left join user_profile as up on up.fk_user_id = us.id   WHERE us.id = ? and up.active =1 ", mem).Values(&maps)

			if num > 0 {
				valid = true
			} else {
				valid = false
				num, _ := o.Raw("SELECT * from users WHERE id = ? ", mem).Values(&maps1)
				if num > 0 {
					mem = maps1[0]["email_address"].(string)
				}
			}

		}

		if valid == true {
			num, _ := o.Raw("SELECT * from users WHERE id = ? ", mem).Values(&maps)

			if num > 0 {
				var result []string
				result = append(result, maps[0]["email_address"].(string))
				_, _, _, status = AddTeamMember(result, msg, user_id, team_id, true)
			}

		} else {
			// users not in the system
			// result = append(result, mem)

			email_address := mem

			_, _ = o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? AND lower(email) = ? ", team_id, strings.ToLower(email_address)).Values(&maps)
			team_join_id := maps[0]["id"].(string)
			// generate the token to send in the url of the email template
			showPage := ""
			verificationToken := SendTeamInviteVerficationLink(email_address)
			if verificationToken != "" {
				showPage = site_url + "register/" + team_join_id + "/" + verificationToken
			} else {
				showPage = site_url + "register/" + team_join_id
			}
			content := strings.Replace(constants.TEAM_INVITATION_NEW_USER_REMINDER_INSTANT_EMAIL, "#teamName", teamUrl, -1)
			content = strings.Replace(content, "#showPage", showPage, -1)

			if len(msg) > 0 {
				content = strings.Replace(content, "#teamReminderMessage", msg, -1)
				content = strings.Replace(content, "#attributeValue", "block", -1)
			} else {
				content = strings.Replace(content, "#attributeValue", "none", -1)
			}

			email_content := strings.Replace(content, "#siteUrl", site_url, -1)

			footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

			html_email_message := "<html>Hi there, <br/><br/>" + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + "</html>"

			common.SendEmail(email_address, constants.TEAM_INVITATION_REMINDER_SUBJECT, html_email_message)

			status = "true"

		}

	}

	return status

}

func DeclineTeamJoinRequest(user_id string, member_id string, team_id string, notf_id string) string {

	//result := "false"

	/*site_url := ""
		if len(os.Getenv("SITE_URL")) > 0 {
	        site_url = os.Getenv("SITE_URL")
	    }else{
	        site_url = "http://localhost:5000/"
	    }*/

	/*o := orm.NewOrm()

	var values []orm.Params


	_, err := o.Raw("UPDATE team_join_request SET active = ? WHERE fk_team_id = ? AND fk_member_id = ?","3", team_id, member_id).Values(&values)

	_, err = o.Raw("UPDATE notification_list SET active = ? WHERE id = ? ","9", notf_id).Values(&values)

	if err == nil {
		result = "true"
	}


	return result*/

	result := "true"

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	o := orm.NewOrm()

	var maps []orm.Params
	var values []orm.Params

	var email_array []string

	_, _ = o.Raw("SELECT fk_initiator_id FROM notification_list WHERE id = ? ", notf_id).Values(&maps)

	user_details := GetUserDetails(maps[0]["fk_initiator_id"].(string))
	user_email := user_details["user"]["email_address"].(string)

	email_array = append(email_array, user_email)

	//_,_,_,result = AddTeamMember(email_array , "", user_id, team_id)

	if result == "true" {

		_, err := o.Raw("UPDATE team_join_request SET active = ? WHERE fk_team_id = ? AND fk_member_id = ?", "3", team_id, member_id).Values(&values)

		_, err = o.Raw("UPDATE notification_list SET active = ? WHERE id = ? RETURNING fk_initiator_id", "9", notf_id).Values(&values)

		if err == nil {

			_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

			team_name := maps[0]["team_name"].(string)
			team_slug := maps[0]["slug"].(string)

			teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

			message := strings.Replace(constants.TEAM_JOIN_REQUEST_DECLINE_NOTIFICATION, "#teamName", teamUrl, -1)

			receiver_id := "{" + values[0]["fk_initiator_id"].(string) + "}"

			receiver_details := GetUserDetails(values[0]["fk_initiator_id"].(string))
			rc_id := values[0]["fk_initiator_id"].(string)
			receiver_email := receiver_details["user"]["email_address"].(string)
			send_email_digest := receiver_details["user"]["subscribe"].(string)

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, message, "9", nil, team_id, nil, user_id, receiver_id).Values(&maps)

			if send_email_digest == "1" {
				_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id,team_id) VALUES (?,?,?,?,?,?,?) RETURNING id", "6", message, receiver_email, time.Now(), "0", rc_id, team_id).Values(&maps)

				if err == nil {
					result = "true"
				} else {
					fmt.Println("Email Digest Failed")
				}

				result = "true"
			}
		} else {
			fmt.Println("Notification failed")
		}
	} else {
		result = "false"
	}

	return result

}

func SetPracticeAvailability(data map[string]string, user_id string) string {

	result := "false"
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("select practice_date,practice_time,user_time_zone  from team_practices WHERE id = ? ", data["PracticeId"]).Values(&maps)

	practice_date := maps[0]["practice_date"].(string)

	user_time_zone := ""
	if maps[0]["user_time_zone"] != nil {
		user_time_zone = maps[0]["user_time_zone"].(string)
	}

	isValidPractice := common.IsFeaturePracticeOrNot(practice_date, user_time_zone)
	// practice_date = practice_date[:10]
	// practice_time := maps[0]["practice_time"].(string)
	// practice_date_time := practice_date + " " + practice_time

	// isAmOrPm := practice_time[len(practice_time)-2 : len(practice_time)]
	// playout := "2006-01-02 15:04 " + isAmOrPm
	// actual_pdate_time, _ := time.Parse(playout, practice_date_time)

	// cdate_time := data["CurrentDateTime"]
	// isCAmOrPm := cdate_time[len(cdate_time)-2 : len(cdate_time)]
	// clayout := "2006-01-02 15:04 " + isCAmOrPm
	// current_date_time, _ := time.Parse(clayout, cdate_time)

	if isValidPractice {

		count, err := o.Raw("UPDATE practice_attendees SET response = ?, reason = ? where fk_practice_id = ? AND fk_user_id = ? RETURNING id", data["Response"], data["Reason"], data["PracticeId"], user_id).Values(&maps)

		if count == 0 {
			_, err = o.Raw("INSERT INTO practice_attendees (response, fk_practice_id, fk_user_id, reason ) VALUES (?,?,?,?) ", data["Response"], data["PracticeId"], user_id, data["Reason"]).Values(&maps)
		}

		if err == nil {
			result = data["Response"]
		}

	} else {
		result = "error_attendance"
	}

	return result
}

func GetPracticeResponse(practice_id string, user_id string) string {

	result := "NoResponse"
	o := orm.NewOrm()

	var maps []orm.Params

	count, err := o.Raw("SELECT response FROM practice_attendees WHERE fk_practice_id = ? AND fk_user_id = ?", practice_id, user_id).Values(&maps)

	if err == nil && count > 0 {

		if maps[0]["response"] != nil {
			result = maps[0]["response"].(string)
		}

		if result == "attending" {
			result = "Attending"
		} else if result == "not attending" {
			result = "Not Attending"
		} else if result == "maybe" {
			result = "Maybe"
		} else {
			result = "NoResponse"
		}
	}
	return result
}

func GetPracticeAttendees(practice_id string) ([]orm.Params, []orm.Params, []orm.Params, []orm.Params, []orm.Params, string, string, string, string, string, string, string, bool) {

	result := ""
	o := orm.NewOrm()
	var attending []orm.Params
	var not_attending []orm.Params
	var maybe []orm.Params
	var all []orm.Params
	p_date := ""
	p_time := ""
	p_duration := ""
	p_location := ""
	p_last_reminder := ""
	p_sms_last_reminder := ""
	team_id := ""
	var responded []string
	var not_responded []orm.Params

	var maps []orm.Params
	var isOldPractice bool = false

	var teamMaps []orm.Params
	_, _ = o.Raw("SELECT * FROM team_practices WHERE id = ?", practice_id).Values(&teamMaps)

	user_time_zone := ""
	if teamMaps[0]["user_time_zone"] != nil {
		user_time_zone = teamMaps[0]["user_time_zone"].(string)
	}

	count, _ := o.Raw("SELECT *, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'Dy, Mon DD') as practice_date_format, to_char(tp.reminder_date, 'DD Mon YYYY') as reminder_date_format, to_char(tp.sms_reminder_date, 'DD Mon YYYY') as sms_reminder_date_format FROM practice_attendees pa join users u on pa.fk_user_id = u.id join user_profile up on up.fk_user_id = u.id join team_practices tp on pa.fk_practice_id = tp.id where pa.fk_practice_id = ? AND pa.response IS NOT NULL ORDER BY lower(up.name)", practice_id).Values(&maps)

	if count > 0 {
		all = maps
		for _, attendee := range maps {

			responded = append(responded, attendee["fk_user_id"].(string))

			if attendee["response"] == "attending" {
				attending = append(attending, attendee)
			} else if attendee["response"] == "not attending" {
				not_attending = append(not_attending, attendee)
			} else if attendee["response"] == "maybe" {
				maybe = append(maybe, attendee)
			}

		}

		result = "true"

		p_date = maps[0]["practice_date_format"].(string)
		p_time = maps[0]["practice_time"].(string)
		p_duration = maps[0]["practice_length"].(string)
		p_location = maps[0]["practice_location"].(string)
		p_practice_date := maps[0]["practice_date"].(string)

		isOldPractice = common.IsFeaturePracticeOrNot(p_practice_date, user_time_zone)

		if maps[0]["reminder_date_format"] != nil {
			p_last_reminder = maps[0]["reminder_date_format"].(string)
		}

		if maps[0]["sms_reminder_date_format"] != nil {
			p_sms_last_reminder = maps[0]["sms_reminder_date_format"].(string)
		}

		team_id = maps[0]["fk_team_id"].(string)

		fmt.Println("responded ", responded)

		responded_string := strings.Join(responded[:], ",")

		count, _ = o.Raw("SELECT * FROM teamjoin tj join user_profile up on up.fk_user_id = tj.fk_member_id WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? AND tj.fk_member_id NOT IN ( "+responded_string+" ) ORDER BY lower(up.name)", team_id, 1, 2).Values(&maps)

		if count > 0 {
			not_responded = maps
		}
	} else {

		//When no one has responded to the practice

		count, _ = o.Raw("SELECT *, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'Dy, Mon DD') as practice_date_format, to_char(tp.reminder_date, 'DD Mon YYYY') as reminder_date_format, to_char(tp.sms_reminder_date, 'DD Mon YYYY') as sms_reminder_date_format FROM team_practices tp WHERE tp.id = ?", practice_id).Values(&maps)
		if count > 0 {

			result = "true"

			p_date = maps[0]["practice_date_format"].(string)
			p_time = maps[0]["practice_time"].(string)
			p_duration = maps[0]["practice_length"].(string)
			p_location = maps[0]["practice_location"].(string)
			p_practice_date := maps[0]["practice_date"].(string)
			isOldPractice = common.IsFeaturePracticeOrNot(p_practice_date, user_time_zone)
			if maps[0]["reminder_date_format"] != nil {
				p_last_reminder = maps[0]["reminder_date_format"].(string)
			}

			if maps[0]["sms_reminder_date_format"] != nil {
				p_sms_last_reminder = maps[0]["sms_reminder_date_format"].(string)
			}

			team_id = maps[0]["fk_team_id"].(string)

			count, _ = o.Raw("SELECT * FROM teamjoin tj join user_profile up on up.fk_user_id = tj.fk_member_id WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? ORDER BY lower(up.name)", team_id, 1, 2).Values(&maps)

			if count > 0 {
				not_responded = maps
			}
		}

	}

	if len(not_responded) > 0 {
		for _, nr := range not_responded {
			all = append(all, nr)
		}
	}

	return all, attending, not_attending, maybe, not_responded, result, p_date, p_time, p_duration, p_location, p_last_reminder, p_sms_last_reminder, isOldPractice
}

func MarkPracticeAttendance(Attending []string, NotAttending []string, Maybe []string, NoResponse []string, practice_id string, team_id string, user_id string) string {

	result := "false"
	o := orm.NewOrm()

	var maps []orm.Params

	if len(Attending) > 0 {
		for _, id := range Attending {
			count, err := o.Raw("UPDATE practice_attendees SET response = ?, reason = ? where fk_practice_id = ? AND fk_user_id = ? RETURNING id", "attending", nil, practice_id, id).Values(&maps)

			if count == 0 {
				_, err = o.Raw("INSERT INTO practice_attendees (response, fk_practice_id, fk_user_id ) VALUES (?,?,?) ", "attending", practice_id, id).Values(&maps)
			}

			if err == nil {
				result = "true"
			}
		}
	}

	if len(NotAttending) > 0 {
		for _, id := range NotAttending {
			count, err := o.Raw("UPDATE practice_attendees SET response = ?, reason = ? where fk_practice_id = ? AND fk_user_id = ?  RETURNING id", "not attending", nil, practice_id, id).Values(&maps)

			if count == 0 {
				_, err = o.Raw("INSERT INTO practice_attendees (response, fk_practice_id, fk_user_id ) VALUES (?,?,?) ", "not attending", practice_id, id).Values(&maps)
			}

			if err == nil {
				result = "true"
			}
		}
	}

	if len(Maybe) > 0 {
		for _, id := range Maybe {
			count, err := o.Raw("UPDATE practice_attendees SET response = ?, reason = ? where fk_practice_id = ? AND fk_user_id = ?  RETURNING id", "maybe", nil, practice_id, id).Values(&maps)

			if count == 0 {
				_, err = o.Raw("INSERT INTO practice_attendees (response, fk_practice_id, fk_user_id ) VALUES (?,?,?) ", "maybe", practice_id, id).Values(&maps)
			}
			if err == nil {
				result = "true"
			}
		}
	}

	if len(NoResponse) > 0 {
		for _, id := range NoResponse {
			_, err := o.Raw("UPDATE practice_attendees SET response = ?, reason = ? where fk_practice_id = ? AND fk_user_id = ? RETURNING id", nil, nil, practice_id, id).Values(&maps)

			if err == nil {
				result = "true"
			}
		}
	}

	return result
}

func SendPracticeReminder(practice_id string, team_id string, user_id string, comm_channel string, superAdmin string) CommunicationResponse {

	result := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	var maps2 []orm.Params
	var not_responded []orm.Params
	var responded []string
	var twilioMsMaps []orm.Params
	var respondedYes []string
	var respondedMayBe []string

	var respondedYes_MaybeEmail []string
	var respondedEmail []string

	var yes_responded []orm.Params
	var mayBe_responded []orm.Params

	practice_date := ""
	practice_time := ""
	practice_location := ""
	site_url := ""
	isSmsSentToAnyone := 0
	messageServiceID := ""
	ispracticeLastEdit := false

	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT * FROM team_practices WHERE id = ?", practice_id).Values(&twilioMsMaps)

	user_time_zone := ""
	team_subscription_sms_limits := "0"
	if twilioMsMaps[0]["user_time_zone"] != nil {
		user_time_zone = twilioMsMaps[0]["user_time_zone"].(string)
	}

	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
	team_name := maps[0]["team_name"].(string)
	team_slug := maps[0]["slug"].(string)
	team_sms_limits := maps[0]["sms_limits"].(string)
	if maps[0]["free_trial"] != nil {
		if maps[0]["free_trial"].(string) == "false" && maps[0]["subscription_on_off"].(string) == "false" && superAdmin != "true" && comm_channel == "sms" {
			communicatioRespObj := CommunicationResponse{Status: "subscription"}
			return communicatioRespObj
		}
	}
	isValidPractice := common.IsFeaturePracticeOrNot(twilioMsMaps[0]["practice_date"].(string), user_time_zone)

	if isValidPractice {

		if comm_channel == "sms" {

			if twilioMsMaps[0]["message_service_uniqueid"] != nil {
				messageServiceID = twilioMsMaps[0]["message_service_uniqueid"].(string)
			}

			if twilioMsMaps[0]["ispractice_last_edit"] != nil {
				ispracticeLastEdit, _ = strconv.ParseBool(twilioMsMaps[0]["ispractice_last_edit"].(string))
			}
			if ispracticeLastEdit {
				messageServiceID = generateUniqueId(practice_id)
			}
		}

		count, _ := o.Raw("SELECT pa.fk_user_id, to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'Dy, Mon DD') as practice_date_format , tp.practice_time, tp.practice_location, pa.response FROM practice_attendees pa join team_practices tp on pa.fk_practice_id = tp.id where pa.fk_practice_id = ? and (pa.response='not attending' or pa.response='attending' or pa.response='maybe')", practice_id).Values(&maps)

		if count > 0 {
			for _, attendee := range maps {

				responded = append(responded, attendee["fk_user_id"].(string))

				if attendee["response"] != nil {
					if attendee["response"].(string) == "attending" {
						respondedYes = append(respondedYes, attendee["fk_user_id"].(string))
					} else if attendee["response"].(string) == "maybe" {
						respondedMayBe = append(respondedMayBe, attendee["fk_user_id"].(string))
					}
				}
			}

			if maps[0]["practice_date_format"] != nil {
				practice_date = maps[0]["practice_date_format"].(string)
			}

			if maps[0]["practice_time"] != nil {
				practice_time = maps[0]["practice_time"].(string)
			}

			if maps[0]["practice_location"] != nil {
				practice_location = maps[0]["practice_location"].(string)
			}

			fmt.Println("responded ", responded)

			responded_string := strings.Join(responded[:], ",")

			if responded_string != "" {

				count, _ = o.Raw("SELECT fk_member_id FROM teamjoin tj WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? AND tj.fk_member_id NOT IN ( "+responded_string+" )", team_id, 1, 2).Values(&maps)

				if count > 0 {
					not_responded = maps
				}

			}

			responded_string = strings.Join(respondedYes[:], ",")

			if responded_string != "" {
				count, _ = o.Raw("SELECT fk_member_id FROM teamjoin tj WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? AND tj.fk_member_id IN ( "+responded_string+" )", team_id, 1, 2).Values(&maps)
				if count > 0 {
					yes_responded = maps
				}
			}

			responded_string = strings.Join(respondedMayBe[:], ",")

			if responded_string != "" {
				count, _ = o.Raw("SELECT fk_member_id FROM teamjoin tj WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? AND tj.fk_member_id IN ( "+responded_string+" )", team_id, 1, 2).Values(&maps)

				if count > 0 {
					mayBe_responded = maps
				}
			}
		} else {

			//When no one has responded to the practice

			count, _ := o.Raw("SELECT fk_member_id FROM teamjoin WHERE fk_team_id = ? AND active = ? AND request_status = ? AND fk_member_id IS NOT NULL", team_id, 1, 2).Values(&maps)

			if count > 0 {
				not_responded = maps
			}

			count, err := o.Raw("SELECT *, to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+user_time_zone+"'), 'Dy, Mon DD') as practice_date_format FROM team_practices WHERE id = ? ", practice_id).Values(&maps)
			fmt.Println(err)
			if count > 0 {

				if maps[0]["practice_date_format"] != nil {
					practice_date = maps[0]["practice_date_format"].(string)
				}

				if maps[0]["practice_time"] != nil {
					practice_time = maps[0]["practice_time"].(string)
				}

				if maps[0]["practice_location"] != nil {
					practice_location = maps[0]["practice_location"].(string)
				}
			}

		}

		if len(not_responded) > 0 || len(yes_responded) > 0 || len(mayBe_responded) > 0 {
			if comm_channel == "email" && len(not_responded) > 0 {

				for _, member := range not_responded {
					member_id := member["fk_member_id"].(string)
					user_details := GetUserDetails(member_id)
					instant_emails := user_details["user"]["instant_emails"].(string)
					tempUserId := user_details["user"]["fk_user_id"].(string)
					if instant_emails == "1" {
						email_address := user_details["user"]["email_address"].(string)
						//respondedEmail = append(respondedEmail, email_address)
						name := "there"
						teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

						yesUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + tempUserId + "/attending'><b>Yes</b></a>"
						noUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + tempUserId + "/not attending'><b>No</b></a>"
						maybeUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + tempUserId + "/maybe'><b>Maybe</b></a>"

						content := strings.Replace(constants.TEAM_PRACTICE_REMINDER_CONTENT, "#teamName", teamUrl, -1)
						content = strings.Replace(content, "#practiceDate", practice_date, -1)
						content = strings.Replace(content, "#practiceTime", practice_time, -1)
						content = strings.Replace(content, "#practiceLocation", practice_location, -1)

						content = strings.Replace(content, "#Yes", yesUrl, -1)
						content = strings.Replace(content, "#No", noUrl, -1)
						content = strings.Replace(content, "#Maybe", maybeUrl, -1)

						email_content := strings.Replace(content, "#siteUrl", site_url, -1)
						footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						html_email_message := "<html>Hi " + name + "," + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
						//email_address := strings.Join(respondedEmail[:], ",")
						//fmt.Println(email_address)
						common.SendEmail(email_address, constants.TEAM_PRACTICE_REMINDER_SUBJECT_NEW, html_email_message)
					}
				}

				loginuser_details := GetUserDetails(user_id)
				temp_email_address := loginuser_details["user"]["email_address"].(string)
				login_instant_emails := loginuser_details["user"]["instant_emails"].(string)

				for _, member := range yes_responded {
					member_id := member["fk_member_id"].(string)
					user_details := GetUserDetails(member_id)
					instant_emails := user_details["user"]["instant_emails"].(string)

					if instant_emails == "1" {
						email_address := user_details["user"]["email_address"].(string)
						if email_address != temp_email_address {
							respondedYes_MaybeEmail = append(respondedYes_MaybeEmail, email_address)
						}
					}
				}

				for _, member := range mayBe_responded {
					member_id := member["fk_member_id"].(string)
					user_details := GetUserDetails(member_id)
					instant_emails := user_details["user"]["instant_emails"].(string)

					if instant_emails == "1" {
						email_address := user_details["user"]["email_address"].(string)

						if email_address != temp_email_address {
							respondedYes_MaybeEmail = append(respondedYes_MaybeEmail, email_address)
						}
					}
				}

				if len(respondedEmail) > 0 {
					// name := "there"
					// teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

					// yesUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + user_id + "/attending'><b>Yes</b></a>"
					// noUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + user_id + "/not attending'><b>No</b></a>"
					// maybeUrl := "<a style='color: #2a7560' href='" + site_url + "team-attendence/" + team_id + "/" + practice_id + "/" + user_id + "/maybe'><b>Maybe</b></a>"

					// content := strings.Replace(constants.TEAM_PRACTICE_REMINDER_CONTENT, "#teamName", teamUrl, -1)
					// content = strings.Replace(content, "#practiceDate", practice_date, -1)
					// content = strings.Replace(content, "#practiceTime", practice_time, -1)
					// content = strings.Replace(content, "#practiceLocation", practice_location, -1)

					// content = strings.Replace(content, "#Yes", yesUrl, -1)
					// content = strings.Replace(content, "#No", noUrl, -1)
					// content = strings.Replace(content, "#Maybe", maybeUrl, -1)

					// email_content := strings.Replace(content, "#siteUrl", site_url, -1)
					// footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					// unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

					// html_email_message := "<html>Hi " + name + "," + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
					// email_address := strings.Join(respondedEmail[:], ",")
					// fmt.Println(email_address)
					// common.SendEmail(email_address, constants.TEAM_PRACTICE_REMINDER_SUBJECT_NEW, html_email_message)
				}

				if len(respondedYes_MaybeEmail) > 0 {
					Gushou_dashboard := "<a href='" + site_url + "' target='_blank' >Gushou dashboard</a>"
					name := "there"
					teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
					content := strings.Replace(constants.TEAM_PRACTICE_REMINDER_YES_MAYBE_CONTENT, "#teamName", teamUrl, -1)
					content = strings.Replace(content, "#practiceDate", practice_date, -1)
					content = strings.Replace(content, "#practiceTime", practice_time, -1)
					content = strings.Replace(content, "#practiceLocation", practice_location, -1)
					content = strings.Replace(content, "#Gushou_dashboard", Gushou_dashboard, -1)

					email_content := strings.Replace(content, "#siteUrl", site_url, -1)
					footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

					html_email_message := "<html>Hi " + name + "," + email_content + footer_links + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
					email_address := strings.Join(respondedYes_MaybeEmail[:], ",")
					fmt.Println(email_address)
					if login_instant_emails != "1" {
						temp_email_address = "xxx@gmail.com"
					}
					common.SendEmailWithBCC(temp_email_address+"-"+email_address, constants.TEAM_PRACTICE_REMINDER_SUBJECT_NEW, html_email_message)
				}

				_, err := o.Raw("UPDATE team_practices SET reminder_date = ? where id = ? ", time.Now(), practice_id).Values(&maps)
				if err == nil {
					result = "true"
				}
				communicatioRespObj := CommunicationResponse{Status: result}
				return communicatioRespObj

			} else if comm_channel == "sms" {
				availableSmsCount := constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM
				smsResponseStatusList := make([]smsResponseStatus, 0)
				fromSmsNumber := constants.FROM

				for _, member := range not_responded {

					num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ?", team_id).Values(&maps2)
					if num > 0 {
						team_subscription_sms_limits = maps2[0]["sms_limits"].(string)
					}
					_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
					team_sms_limits = maps[0]["sms_limits"].(string)

					isSmsSentToAnyone, availableSmsCount, smsResponseStatusList, result, messageServiceID = sendSMSCommonCode(member["fk_member_id"].(string), team_sms_limits, availableSmsCount, messageServiceID, practice_date, practice_time, practice_location, team_name, fromSmsNumber, isSmsSentToAnyone, practice_id, smsResponseStatusList, team_id, result, 0, team_subscription_sms_limits)
				}

				for _, member := range yes_responded {

					num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ?", team_id).Values(&maps2)
					if num > 0 {
						team_subscription_sms_limits = maps2[0]["sms_limits"].(string)
					}
					_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
					team_sms_limits = maps[0]["sms_limits"].(string)

					isSmsSentToAnyone, availableSmsCount, smsResponseStatusList, result, messageServiceID = sendSMSCommonCode(member["fk_member_id"].(string), team_sms_limits, availableSmsCount, messageServiceID, practice_date, practice_time, practice_location, team_name, fromSmsNumber, isSmsSentToAnyone, practice_id, smsResponseStatusList, team_id, result, 1, team_subscription_sms_limits)
				}

				for _, member := range mayBe_responded {

					num, _ := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ?", team_id).Values(&maps2)
					if num > 0 {
						team_subscription_sms_limits = maps2[0]["sms_limits"].(string)
					}
					_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)
					team_sms_limits = maps[0]["sms_limits"].(string)
					isSmsSentToAnyone, availableSmsCount, smsResponseStatusList, result, messageServiceID = sendSMSCommonCode(member["fk_member_id"].(string), team_sms_limits, availableSmsCount, messageServiceID, practice_date, practice_time, practice_location, team_name, fromSmsNumber, isSmsSentToAnyone, practice_id, smsResponseStatusList, team_id, result, 0, team_subscription_sms_limits)
				}

				if isSmsSentToAnyone > 0 {
					_, err := o.Raw("update team_practices set sms_reminder_date = ? where id = ?", time.Now(), practice_id).Values(&maps)
					if err == nil {
						fmt.Println("The fromSmsNumber was update successFully in team_practices table")
						result = "true"
					}
				} else {
					result = "true"
				}

				if isSmsSentToAnyone > 0 && ispracticeLastEdit {
					_, err := o.Raw("update team_practices set ispractice_last_edit =? where id = ?", "false", practice_id).Values(&maps)
					if err == nil {
						fmt.Println("The fromSmsNumber was update successFully in team_practices table")
						result = "true"
					}
				}

				communicatioRespObj := CommunicationResponse{Status: result, SmsResponseStatus: smsResponseStatusList, AvailableSMSCount: availableSmsCount, DefaultSmsAvailableForTeam: constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM}
				return communicatioRespObj
			} else {
				result = "none"
			}

		} else {
			result = "none"
		}

	} else {
		result = "oldPractice"
	}
	communicatioRespObj := CommunicationResponse{Status: result}
	return communicatioRespObj
}

func sendSMSCommonCode(memberId string, team_sms_limits string, availableSmsCount string, messageServiceID string, practice_date string, practice_time string, practice_location string, team_name string, fromSmsNumber string, isSmsSentToAnyone int, practice_id string, smsResponseStatusList []smsResponseStatus, team_id string, result string, smsMsgFlag int, tempteam_subscription_sms_limits string) (int, string, []smsResponseStatus, string, string) {
	member_id := memberId
	user_details := GetUserDetails(member_id)
	instant_sms := user_details["user"]["instant_sms"].(string)
	send_instant_sms_dial_code := ""
	if user_details["user"]["country_dial_code"] != nil {
		send_instant_sms_dial_code = user_details["user"]["country_dial_code"].(string)
	}
	send_instant_sms_phone := ""
	if user_details["user"]["phone"] != nil {
		send_instant_sms_phone = user_details["user"]["phone"].(string)
	}
	email_address := user_details["user"]["email_address"].(string)
	receiver_name := ""
	if user_details["user"]["name"] != nil {
		receiver_name = user_details["user"]["name"].(string)
	}

	isSmsStatus := "false"
	tempteam_sms_limits, _ := strconv.Atoi(team_sms_limits)
	team_subscription_sms_limits, _ := strconv.Atoi(tempteam_subscription_sms_limits)
	sms_limitsvalue := tempteam_sms_limits + team_subscription_sms_limits
	sms_limitsvaluestring := strconv.Itoa(sms_limitsvalue)
	availableSmsCount = sms_limitsvaluestring
	if instant_sms == "1" && send_instant_sms_dial_code != "" && send_instant_sms_phone != "" {
		if sms_limitsvalue > 0 {
			toSms := send_instant_sms_dial_code + send_instant_sms_phone
			if messageServiceID != "" {
				smsMessage := "Gushou Dragon Boat Practice Reminder:\n\n" + team_name + " | " + practice_date +
					" | " + practice_time + " | " + practice_location + "\n\nWill you be attending? Please text back with one of the following responses (including code):\n\n" + messageServiceID + "-Yes\n\n" + messageServiceID + "-No\n\n" + messageServiceID + "-Maybe"
				if smsMsgFlag == 1 {
					smsMessage = "Gushou Dragon Boat Practice Reminder:\n\n" + team_name + " | " + practice_date +
						" | " + practice_time + " | " + practice_location
				}
				fmt.Println("smsMessage", smsMessage)
				isSmsSent := smscommon.SendSms(toSms, smsMessage, messageServiceID, fromSmsNumber)
				isSmsStatus = isSmsSent
				if isSmsStatus == "true" {
					isSmsSentToAnyone++
				}
			} else {
				messageServiceID = generateUniqueId(practice_id)
				if messageServiceID != "" {
					smsMessage := "Gushou Dragon Boat Practice Reminder:\n\n" + team_name + " | " + practice_date +
						" | " + practice_time + " | " + practice_location + "\n\nWill you be attending? Please text back with one of the following responses (including code):\n\n" + messageServiceID + "-Yes\n\n" + messageServiceID + "-No\n\n" + messageServiceID + "-Maybe"
					if smsMsgFlag == 1 {
						smsMessage = "Gushou Dragon Boat Practice Reminder:\n\n" + team_name + " | " + practice_date +
							" | " + practice_time + " | " + practice_location
					}
					fmt.Println("smsMessage", smsMessage)
					isSmsSent := smscommon.SendSms(toSms, smsMessage, messageServiceID, fromSmsNumber)
					isSmsStatus = isSmsSent
					if isSmsStatus == "true" {
						isSmsSentToAnyone++
					}
				}
			}
		}
	}

	userSmsErrorMsg := ""

	//smsCountCheck, _ := strconv.Atoi(constants.DEFAULT_SMS_AVAILABLE_FOR_TEAM)
	if sms_limitsvalue < 1 {
		userSmsErrorMsg = "invalid SMS credits"
	} else if instant_sms == "0" {
		userSmsErrorMsg = "unsubscribed to SMS"
	} else if isSmsStatus == "false" {
		userSmsErrorMsg = "invalid phone"
	}

	smsResponseStatusObj := smsResponseStatus{
		EmailAddress:             email_address,
		DisplayName:              receiver_name,
		MobileNumberWithDialCode: send_instant_sms_dial_code + "-" + send_instant_sms_phone,
		SmsStatus:                isSmsStatus,
		InstantSms:               instant_sms,
		UserSMSError:             userSmsErrorMsg,
	}

	smsResponseStatusList = append(smsResponseStatusList, smsResponseStatusObj)

	if isSmsSentToAnyone > 0 && isSmsStatus == "true" {
		o := orm.NewOrm()
		var maps []orm.Params
		if team_subscription_sms_limits > 0 {
			_, err := o.Raw("update sms_package set sms_limits = sms_limits-1 where fk_team_id = ? RETURNING sms_limits", team_id).Values(&maps)
			temp_sms_limits, _ := strconv.Atoi(maps[0]["sms_limits"].(string))
			availableSmsCount = strconv.Itoa(temp_sms_limits + tempteam_sms_limits)
			tempteam_subscription_sms_limits = maps[0]["sms_limits"].(string)
			if err == nil {
				result = "true"
				fmt.Println("The sms of this team is substracted by 1 from the default sms count")
			}

		} else if tempteam_sms_limits > 0 {
			_, err := o.Raw("update team set sms_limits = sms_limits-1 where id = ? RETURNING sms_limits", team_id).Values(&maps)
			temp_sms_limits, _ := strconv.Atoi(maps[0]["sms_limits"].(string))
			availableSmsCount = strconv.Itoa(temp_sms_limits + team_subscription_sms_limits)
			team_sms_limits = maps[0]["sms_limits"].(string)
			if err == nil {
				result = "true"
				fmt.Println("The sms of this team is substracted by 1 from the default sms count")
			}
		}
	} else {
		result = "true"
	}

	return isSmsSentToAnyone, availableSmsCount, smsResponseStatusList, result, messageServiceID
}

func AddTeamLinks(data map[string]string, user_id string, team_id string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("INSERT INTO team_links (active, link, title, uploaded_date, fk_team_id, fk_uploaded_by) VALUES (?,?,?,?,?,?) RETURNING id", "1", data["Link"], data["LinkTitle"], time.Now(), team_id, user_id).Values(&maps)

	if err == nil {
		status = "true"

	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func GetTeamLinks(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("Team id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM team_links WHERE active = 1 AND fk_team_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "link_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No team links")
		result["status"] = nil
	}

	return result

}

func RemoveTeamLinks(data map[string][]string, team_id string, user_id string, removed_by string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params
	code := ""

	if removed_by == "super_admin" {
		code = "7"
	} else {
		code = "9"
	}

	for _, file_id := range data["LinkIds"] {
		_, err := o.Raw("UPDATE team_links SET active = ? WHERE id = ? ", code, file_id).Values(&maps)

		if err == nil {

			status = "true"

		}
	}

	//send mail

	return status
}

func SendTeamInviteVerficationLink(email string) string {

	fmt.Println("SendTeamInviteVerficationLink")

	o := orm.NewOrm()
	var values []orm.Params

	token := generateToken()
	expiry_date := time.Now().AddDate(0, 0, 10)

	fmt.Println("time now : ", time.Now())
	fmt.Println("expiry_date : ", expiry_date)

	for i := 0; i < 10; i++ {
		num, err := o.Raw("SELECT * FROM user_verification_tokens WHERE address = ? ", email).Values(&values)

		if num > 0 {
			_, err = o.Raw("UPDATE user_verification_tokens SET fk_user_id = ?, address = ?, token = ?, expiry = ?,"+`"when"`+" = ? "+" WHERE address = ?", nil, email, token, expiry_date, time.Now(), email).Exec()
		} else {
			_, err = o.Raw("INSERT INTO user_verification_tokens (fk_user_id, address, token, expiry, "+`"when"`+")  VALUES (?,?,?,?,?) ", nil, email, token, expiry_date, time.Now()).Exec()

		}
		if err == nil {
			fmt.Println("Token Inserted")
			break
		} else {
			fmt.Println("Regenarate")
			token = generateToken()
		}
	}
	return token
}

func CheckTeamSMSLimit(id string) (map[string]orm.Params, string) {

	result := make(map[string]orm.Params)

	fmt.Println("Team id ", id)

	o := orm.NewOrm()

	var maps []orm.Params
	sms_limits_subscription := "0"

	num, _ := o.Raw("select membership_level,sms_limits,free_trial,subscription_on_off  from team where id = ? ", id).Values(&maps)

	if num > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)
			result[key] = v
		}
		num, err := o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ?", id).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			sms_limits_subscription = maps[0]["sms_limits"].(string)
		}

	} else {
		fmt.Println("No default sms limitation found for team")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result, sms_limits_subscription

}

func GetPracticeStatus(practice_id string, user_id int) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params

	count, _ := o.Raw("SELECT response,reason FROM practice_attendees WHERE fk_practice_id = ? AND fk_user_id = ?", practice_id, user_id).Values(&maps)

	if count > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}

	return result
}

func CreateTwilioMessageServiceForPractice(practice_id string) string {

	messageServiceId := ""
	msgServiceCreateCurl := exec.Command("curl", constants.CREATE_MESSAGE_SERVICE_URL, "-X", "POST", "--data-urlencode", constants.MSG_SERVICE_FRIENDLY_NAME+practice_id, "-u", constants.ACCOUNT_SID+":"+constants.AUTH_TOKEN)
	outCreateMS, err := msgServiceCreateCurl.Output()
	if err != nil {
		fmt.Println(err)
	} else {
		pingData := BytesToString(outCreateMS)
		messageServiceData := messageServiceData{}
		err := json.Unmarshal([]byte(pingData), &messageServiceData)
		if err != nil {
			fmt.Println(err)
		}
		messageServiceId = messageServiceData.Sid
		o := orm.NewOrm()
		var maps []orm.Params
		_, _ = o.Raw("UPDATE team_practices SET message_service_uniqueid = ? WHERE id = ? ", messageServiceId, practice_id).Values(&maps)

	}
	return messageServiceId
}

func BytesToString(b []byte) string {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}
	return *(*string)(unsafe.Pointer(&sh))
}

func CaseInsensitiveContains(s string) string {
	userAttendanceStatus := ""
	s, substr := strings.ToUpper(s), strings.ToUpper("Yes")
	if strings.Contains(s, substr) {
		userAttendanceStatus = "attending"
	} else {
		if strings.Contains(s, strings.ToUpper("No")) {
			userAttendanceStatus = "not attending"
		} else {
			if strings.Contains(s, strings.ToUpper("Maybe")) {
				userAttendanceStatus = "maybe"
			} else {
				userAttendanceStatus = "maybe"
			}
		}
	}
	return userAttendanceStatus
}

// check last phone number index and return next phone number index value.
func getLastPhoneNumberIndex(word string, data []string) int {
	for k, v := range data {
		if word == v {
			if k < (len(data) - 1) {
				return k + 1
			} else {
				return 0
			}
		}
	}
	return -1
}

// this function can generate uniqueid and add each to each practice. to trace the sms reponding using this coding.
func generateUniqueId(practiceId string) string {
	practice_id := practiceId

	charsToGenerate := 4
	charSet := gostrgen.Lower
	includes := ""       // optionally include some additional letters
	excludes := "~^(){}" //exclude big 'O' and small 'l' to avoid confusion with zero and one.

	secretCode, err := gostrgen.RandGen(charsToGenerate, charSet, includes, excludes)
	if err != nil {
		//fmt.Println(err)
	}
	//fmt.Println(secretCode)
	practiceIdLength := len(practiceId)
	if practiceIdLength > 2 {
		runes := []rune(practiceId)
		practiceId = string(runes[practiceIdLength-2 : practiceIdLength])
	}
	if len(secretCode) > 1 {
		o := orm.NewOrm()
		var maps []orm.Params
		_, err := o.Raw("UPDATE team_practices SET message_service_uniqueid = ? WHERE id = ? ", practiceId+secretCode, practice_id).Values(&maps)
		if err != nil {
			practiceId = ""
			secretCode = ""
		}
	}

	return practiceId + strings.ToLower(secretCode)
}

func attendenceMarkUpCode(msgList []inboundMessage, practiceMessageService string, userMobileNo string, teamPracticeID string, teamMemberID string, attendenceUpdateMap map[string][]userSmsResponse, practiceDate string, user_TimeZone string) map[string][]userSmsResponse {
	for mlKey, _ := range msgList {
		msgBody := strings.Trim(msgList[mlKey].MsgBody, " ")
		msgBody1 := strings.Trim(msgList[mlKey].MsgBody, " ")
		isValidPractice := common.IsValidSmsOrNot(msgList[mlKey].MsgDateSent, practiceDate, user_TimeZone)
		if msgBody != "" && isValidPractice {
			if msgList[mlKey].Direction == "inbound" { // change to 'inbound' once check twilio MS, "outbound-api"
				indexValue := strings.Index(msgBody, "-")
				if indexValue != -1 {
					runes := []rune(msgBody)
					runes1 := []rune(msgBody1)
					msgBody = strings.Trim(string(runes[0:indexValue]), " ")
					if msgBody == practiceMessageService {
						userSmsMobileNo := msgList[mlKey].From // need to change to 'From'
						replaceMobileNumber := strings.Replace(userMobileNo, "-", "", -1)
						replaceMobileNumber = strings.Replace(replaceMobileNumber, ".", "", -1)
						replaceMobileNumber = strings.Replace(replaceMobileNumber, " ", "", -1)
						if replaceMobileNumber == userSmsMobileNo {
							msgBody = strings.Trim(string(runes1[indexValue+1:len(msgBody1)]), " ")
							if value, exist := attendenceUpdateMap[userSmsMobileNo]; exist {
								fmt.Println("Key found value is: ", value)
								userSmsResponseObj := userSmsResponse{
									TeamPracticeID: teamPracticeID,
									TeamMemberID:   teamMemberID,
									MsgDateSent:    msgList[mlKey].MsgDateSent,
									MsgBody:        msgBody,
								}
								attendenceUpdateMap[userSmsMobileNo] = append(attendenceUpdateMap[userSmsMobileNo], userSmsResponseObj)
								break
							} else {
								fmt.Println("Key not found")
								userSmsResponseObj := userSmsResponse{
									TeamPracticeID: teamPracticeID,
									TeamMemberID:   teamMemberID,
									MsgDateSent:    msgList[mlKey].MsgDateSent,
									MsgBody:        msgBody,
								}
								var userSmsResponseColns []userSmsResponse
								userSmsResponseColns = append(userSmsResponseColns, userSmsResponseObj)
								attendenceUpdateMap[userSmsMobileNo] = userSmsResponseColns
								break
							}
						}
					}
				}
			}

		}
	}
	return attendenceUpdateMap
}

func attendenceUpdateMaking(attendenceUpdateMap map[string][]userSmsResponse) {
	o := orm.NewOrm()
	layout := "Mon, 02 Jan 2006 15:04:05 +0000"
	for _, aumValue := range attendenceUpdateMap {
		recentSMSReply := userSmsResponse{}
		msgSentDate := time.Now()
		for k, v := range aumValue {
			fmt.Println(k, v)
			if k == 0 {
				t, err := time.Parse(layout, v.MsgDateSent)
				if err != nil {
					fmt.Println(err, t)
				}
				msgSentDate = t
				recentSMSReply = v
			} else {
				vMsgDateSent, err := time.Parse(layout, v.MsgDateSent)
				if err != nil {
					fmt.Println(err)
				}
				if vMsgDateSent.After(msgSentDate) {
					msgSentDate = vMsgDateSent
					recentSMSReply = v
				}
			}
		}

		// mark attendance based on most recent sms reply
		var insertAttendenceEntry []orm.Params
		var isPresentAttendenceEntry []orm.Params
		attendanceStatus := CaseInsensitiveContains(recentSMSReply.MsgBody)
		// check whether the attendance is updated or not
		num, err := o.Raw("SELECT * FROM practice_attendees WHERE fk_practice_id = ? AND fk_user_id = ? ", recentSMSReply.TeamPracticeID, recentSMSReply.TeamMemberID).Values(&isPresentAttendenceEntry)
		if num > 0 {
			_, err = o.Raw("UPDATE practice_attendees SET response = ? WHERE fk_practice_id = ? AND fk_user_id = ? ", attendanceStatus, recentSMSReply.TeamPracticeID, recentSMSReply.TeamMemberID).Values(&insertAttendenceEntry)
			if err != nil {
				fmt.Println("error : ", err)
			}
		} else {
			_, err = o.Raw("INSERT INTO practice_attendees (response,fk_practice_id,fk_user_id) VALUES (?,?,?) ", attendanceStatus, recentSMSReply.TeamPracticeID, recentSMSReply.TeamMemberID).Values(&insertAttendenceEntry)
			if err != nil {
				fmt.Println("error : ", err)
			}
		}
	}
}

func twilioReadSmsLog(twilioEndDate string, twilioStartDate string) messageData {
	viewMsgList := "https://api.twilio.com/2010-04-01/Accounts/" + constants.ACCOUNT_SID + "/Messages.json"
	curlViewMsgList := exec.Command("curl", viewMsgList, "-G", "--data-urlencode", "To="+constants.FROM, "--data-urlencode", "DateSent>="+twilioStartDate, "--data-urlencode", "DateSent<="+twilioEndDate, "-u", constants.ACCOUNT_SID+":"+constants.AUTH_TOKEN)
	msgListout, err := curlViewMsgList.Output()
	viewMessageList := messageData{}
	if err != nil {
		fmt.Println(err)
	} else {
		msgListStr := BytesToString(msgListout)
		err := json.Unmarshal([]byte(msgListStr), &viewMessageList)
		if err != nil {
			fmt.Println(err)
		}
	}
	return viewMessageList
}

func AttendenceMarkUpCodeTemp(fromNumber string, toNumber string, msgBodyData string) {
	logs.SetLogger("file", `{"filename":"test.log"}`)
	o := orm.NewOrm()
	var twilioMap []orm.Params
	var userMap []orm.Params
	var teamJoinMap []orm.Params

	msgBody := strings.Trim(msgBodyData, " ")
	msgBody1 := strings.Trim(msgBodyData, " ")

	if msgBody != "" {
		indexValue := strings.Index(msgBody, "-")
		if indexValue != -1 {
			runes := []rune(msgBody)
			runes1 := []rune(fromNumber)
			runes2 := []rune(msgBody)
			msgBody = strings.Trim(string(runes[0:indexValue]), " ")
			num, err := o.Raw("SELECT * FROM team_practices WHERE message_service_uniqueid = ? ", msgBody).Values(&twilioMap)
			logs.Alert("message_service_uniqueid", err)
			if err == nil && num > 0 {
				practiceId := twilioMap[0]["id"].(string)
				teamId1 := twilioMap[0]["fk_team_id"].(string)
				logs.Alert("teamId1", teamId1)
				indexValueData := len(fromNumber) - 10
				userPhoneSplit := strings.Trim(string(runes1[indexValueData:len(fromNumber)]), " ")
				logs.Alert("userPhoneSplit", userPhoneSplit)
				if len(userPhoneSplit) == 10 {
					concadinationPhone := strings.Split(userPhoneSplit, "")
					fromFormatNumber := concadinationPhone[0] + concadinationPhone[1] + concadinationPhone[2] + "-" + concadinationPhone[3] + concadinationPhone[4] + concadinationPhone[5] + "-" + concadinationPhone[6] + concadinationPhone[7] + concadinationPhone[8] + concadinationPhone[9]
					logs.Alert("fromFormatNumber", fromFormatNumber)
					fromNumber1 := strings.Replace(fromFormatNumber, "-", ".", 3)
					fromNumber2 := strings.Replace(fromFormatNumber, "-", "", 3)
					num, err := o.Raw("SELECT *, (country_dial_code || '' || phone) as user_mobile_no  FROM user_profile WHERE phone = ? Or phone = ? Or phone = ? ", fromFormatNumber, fromNumber1, fromNumber2).Values(&userMap)
					logs.Alert("errcountry_dial_code", err)
					if err == nil && num > 0 {
						if userMap[0]["user_mobile_no"] != nil {
							replaceMobileNumber := strings.Replace(userMap[0]["user_mobile_no"].(string), "-", "", -1)
							replaceMobileNumber = strings.Replace(replaceMobileNumber, ".", "", -1)
							replaceMobileNumber = strings.Replace(replaceMobileNumber, " ", "", -1)

							if replaceMobileNumber == fromNumber {
								var userIdList []string
								for _, m := range userMap {
									userIdList = append(userIdList, m["fk_user_id"].(string))
								}
								if len(userIdList) > 0 {
									all_userId_string := strings.Join(userIdList[:], ",")
									teamId := twilioMap[0]["fk_team_id"].(string)

									num, err := o.Raw("SELECT * FROM teamjoin WHERE fk_team_id = ? and fk_member_id in ( "+all_userId_string+" ) and active =1 and request_status=2 ", teamId).Values(&teamJoinMap)
									logs.Alert("teamjoin", err)
									if err == nil && num > 0 {
										userId := teamJoinMap[0]["fk_member_id"].(string)
										// mark attendance based on most recent sms reply
										var insertAttendenceEntry []orm.Params
										var isPresentAttendenceEntry []orm.Params
										msgBodyResponse := strings.Trim(string(runes2[indexValue+1:len(msgBody1)]), " ")
										attendanceStatus := CaseInsensitiveContains(msgBodyResponse)
										logs.Alert("attendanceStatus", attendanceStatus)
										logs.Alert("practiceId", practiceId)
										logs.Alert("userId", userId)
										// check whether the attendance is updated or not
										num, err := o.Raw("SELECT * FROM practice_attendees WHERE fk_practice_id = ? AND fk_user_id = ? ", practiceId, userId).Values(&isPresentAttendenceEntry)
										if num > 0 {
											_, err = o.Raw("UPDATE practice_attendees SET response = ? WHERE fk_practice_id = ? AND fk_user_id = ? ", attendanceStatus, practiceId, userId).Values(&insertAttendenceEntry)
											if err != nil {
												fmt.Println("error : ", err)
											}
										} else {
											_, err = o.Raw("INSERT INTO practice_attendees (response,fk_practice_id,fk_user_id) VALUES (?,?,?) ", attendanceStatus, practiceId, userId).Values(&insertAttendenceEntry)
											if err != nil {
												fmt.Println("error : ", err)
											}
										}

									} else {
										logs.Alert("teamjoin", "empty")
									}
								} else {
									logs.Alert("userIdList", "empty")
								}
							}
						} else {
							logs.Alert("user_mobile_no", "empty")
						}
					} else {
						logs.Alert("errcountry_dial_code", "empty")
					}
				} else {
					logs.Alert("userPhoneSplit length", len(userPhoneSplit))
				}
			} else {
				logs.Alert("teamPracticeData", "empty")
			}
		}
	} else {
		logs.Alert("msgBody", "empty")
	}
}

func deleteTeamMemberDependencies(team_id string, member_id string) {
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("select * from notification_list where fk_team_id = ?", team_id).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, m := range maps {
			_, err = o.Raw("UPDATE notification_list SET fk_receiver = array_remove(fk_receiver, '"+member_id+"') where id =?", m["id"].(string)).Values(&maps)
			fmt.Println(err)
		}
	}

	_, err = o.Raw("DELETE FROM email_digest WHERE fk_receiver_id = ? and team_id=? ", member_id, team_id).Values(&maps)
	fmt.Println(err)

	_, err = o.Raw("DELETE FROM team_discussion WHERE fk_user_id = ? and fk_team_id=? ", member_id, team_id).Values(&maps)
	fmt.Println(err)

	_, err = o.Raw("DELETE FROM notification_list WHERE fk_initiator_id = ? and fk_team_id=? ", member_id, team_id).Values(&maps)
	fmt.Println(err)

	num, err = o.Raw("select id FROM team_practices WHERE fk_team_id = ? ", team_id).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		var practiceIds []string
		for _, m := range maps {
			practiceIds = append(practiceIds, m["id"].(string))
		}
		if len(practiceIds) > 0 {
			all_practiceIds_string := strings.Join(practiceIds[:], ",")
			_, err = o.Raw("DELETE FROM practice_attendees WHERE fk_practice_id in ("+all_practiceIds_string+") and fk_user_id=? ", member_id).Values(&maps)
			fmt.Println(err)
		}
	}

	deleterosterLineupDependencies(member_id)
}

func deleterosterLineupDependencies(member_id string) {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT id FROM team_roster WHERE " + member_id + " = ANY(members)").Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, m := range maps {
			_, err = o.Raw("UPDATE team_roster SET members = array_remove(members, '"+member_id+"') where id =?", m["id"].(string)).Values(&maps)
			fmt.Println(err)
		}
	}

	num, err = o.Raw("SELECT id FROM team_lineup WHERE " + member_id + " = ANY(left_padller_members) or " + member_id + " = ANY(right_padller_members) or " + member_id + " = ANY(other_members)").Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, m := range maps {
			_, err = o.Raw("UPDATE team_lineup SET left_padller_members = array_remove(left_padller_members, '"+member_id+"'),right_padller_members = array_remove(right_padller_members, '"+member_id+"'), other_members = array_remove(other_members, '"+member_id+"')  where id =?", m["id"].(string)).Values(&maps)
			fmt.Println(err)
		}
	}

	num, err = o.Raw("SELECT id FROM team_lineup WHERE " + member_id + " = ANY(selected_left_paddlers) or " + member_id + " = ANY(selected_right_paddlers) or " + member_id + " = ANY(selected_drummers) or " + member_id + " = ANY(selected_sparess)").Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, m := range maps {
			_, err = o.Raw("UPDATE team_lineup SET selected_left_paddlers = array_replace(selected_left_paddlers, '"+member_id+"', 0), selected_right_paddlers = array_replace(selected_right_paddlers, '"+member_id+"', 0), selected_drummers = array_replace(selected_drummers, '"+member_id+"', 0),selected_sparess = array_replace(selected_sparess, '"+member_id+"', 0)  where id =?", m["id"].(string)).Values(&maps)
			fmt.Println(err)
		}
	}
	_, _ = o.Raw("delete FROM team_roster_member_role WHERE roster_member_id=?", member_id).Values(&maps)
}

func deleteIndividualrosterLineupDependencies(member_id string, rosterUpdateId string) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params

	num, err := o.Raw("SELECT id FROM team_roster WHERE id= ? and "+member_id+" = ANY(members)", rosterUpdateId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, m := range maps {
			_, err = o.Raw("UPDATE team_roster SET members = array_remove(members, '"+member_id+"') where id =?", m["id"].(string)).Values(&maps)
			fmt.Println(err)

			num, err = o.Raw("SELECT id FROM team_lineup WHERE fk_roster_id=? and "+member_id+" = ANY(left_padller_members) or "+member_id+" = ANY(right_padller_members) or "+member_id+" = ANY(other_members)", m["id"].(string)).Values(&maps1)
			fmt.Println(err)
			if num > 0 {
				for _, m := range maps1 {
					_, err = o.Raw("UPDATE team_lineup SET left_padller_members = array_remove(left_padller_members, '"+member_id+"'),right_padller_members = array_remove(right_padller_members, '"+member_id+"'), other_members = array_remove(other_members, '"+member_id+"')  where id =?", m["id"].(string)).Values(&maps3)
					fmt.Println(err)
				}
			}

			num, err = o.Raw("SELECT id FROM team_lineup WHERE fk_roster_id=? and "+member_id+" = ANY(selected_left_paddlers) or "+member_id+" = ANY(selected_right_paddlers) or "+member_id+" = ANY(selected_drummers) or "+member_id+" = ANY(selected_sparess)", m["id"].(string)).Values(&maps2)
			fmt.Println(err)
			if num > 0 {
				for _, m := range maps2 {
					_, err = o.Raw("UPDATE team_lineup SET selected_left_paddlers = array_replace(selected_left_paddlers, '"+member_id+"', 0), selected_right_paddlers = array_replace(selected_right_paddlers, '"+member_id+"', 0), selected_drummers = array_replace(selected_drummers, '"+member_id+"', 0),selected_sparess = array_replace(selected_sparess, '"+member_id+"', 0)  where id =?", m["id"].(string)).Values(&maps3)
					fmt.Println(err)
				}
			}
		}
		_, _ = o.Raw("delete FROM team_roster_member_role WHERE roster_member_id = ? and fk_roster_id =?", member_id, rosterUpdateId).Values(&maps4)

	}

}

func SendMessagesToTeam(member_ids string, toEmailIds []string, msg string, user_id string, team_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var maps1 []orm.Params
	captionUserId := ""
	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps)

	_, _ = o.Raw("select name as sender_fname  from user_profile WHERE fk_user_id = ? ", user_id).Values(&maps1)

	captain_name := ""
	captain_email := ""
	team_name := maps[0]["team_name"].(string)
	team_slug := maps[0]["slug"].(string)
	captain_instantemail := ""
	sender_name := ""

	teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"

	if maps[0]["fk_captain_id"] != nil {
		captionUserId = maps[0]["fk_captain_id"].(string)
		captain_details := GetUserDetails(maps[0]["fk_captain_id"].(string))
		captain_name = captain_details["user"]["name"].(string)
		captain_email = captain_details["user"]["email_address"].(string)
		captain_instantemail = captain_details["user"]["instant_emails"].(string)
		if maps1[0]["sender_fname"] != nil {
			sender_name = maps1[0]["sender_fname"].(string)
		}
	}

	fmt.Println(teamUrl)
	fmt.Println(captain_name)
	desc := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION_BY_SENDER, "#sender_name", sender_name, -1)
	desc = strings.Replace(desc, "#captainMessage", msg, -1)

	receiver := "{" + member_ids + "}"

	_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", team_id, nil, user_id, receiver).Values(&maps)

	if err == nil {
		status = "true"

		//send mail

		footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

		headerSubject := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_EMAIL_SUBJECT, "#teamName", team_name, -1)

		headerBodyData := strings.Replace(constants.TEAM_MEMBER_NEW_INSTANT_MESSAGE_NOTIFICATION_BY_SENDER, "#sender_name", sender_name, -1)
		//headerBodyData = strings.Replace(headerBodyData, "#sender_name", sender_name, -1)
		headerBodyData = strings.Replace(headerBodyData, "#captainMessage", msg, -1)
		unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
		if user_id == captionUserId {
			html_email_message := "<html>Hi, <br/><br/>" + headerBodyData + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
			toEmailIds = common.RemoveStrinInArray(toEmailIds, captain_email)

			var temptoEmailIds []string
			for _, m := range toEmailIds {
				temp_member_details := GetUserDetailsByEmailId(m)
				if _, ok := temp_member_details["status"]; !ok {
					if temp_member_details["user"]["instant_emails"].(string) == "1" {
						temptoEmailIds = append(temptoEmailIds, m)
					}
				}
			}

			justStringEmailId := strings.Join(temptoEmailIds, ",")
			if captain_instantemail != "1" {
				captain_email = "xxx@gmail.com"
			}
			common.SendEmailWithBCC(captain_email+"-"+justStringEmailId, headerSubject, html_email_message)

		} else {
			member_details := GetUserDetails(user_id)
			senderEmail_address := member_details["user"]["email_address"].(string)
			senderInstantEmailStatus := member_details["user"]["instant_emails"].(string)
			html_email_message := "<html>Hi, <br/><br/>" + headerBodyData + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
			toEmailIds = common.RemoveStrinInArray(toEmailIds, captain_email)
			toEmailIds = common.RemoveStrinInArray(toEmailIds, senderEmail_address)

			var temptoEmailIds []string
			for _, m := range toEmailIds {
				temp_member_details := GetUserDetailsByEmailId(m)
				if _, ok := temp_member_details["status"]; !ok {
					if temp_member_details["user"]["instant_emails"].(string) == "1" {
						temptoEmailIds = append(temptoEmailIds, m)
					}
				}
			}

			justStringEmailId := strings.Join(temptoEmailIds, ",")
			if captain_instantemail != "1" {
				captain_email = "xxx@gmail.com"
			}

			if senderInstantEmailStatus != "1" {
				common.SendEmailWithBCC(captain_email+"-"+justStringEmailId, headerSubject, html_email_message)
			} else {
				common.SendEmailWithBCC(captain_email+","+senderEmail_address+"-"+justStringEmailId, headerSubject, html_email_message)
			}
		}
	}
	return status
}

func GetCreatedUpcommingPractice(localUserTimeZone string, teamId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params

	_, _ = o.Raw("SELECT (to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD YYYY')|| ' | ' || practice_time || ' | ' ||practice_location) as practicedate_location, id, active FROM team_practices WHERE active = 1 and fk_team_id =? order by practice_date desc ", teamId).Values(&maps)
	return maps
}

func GetIndividualPracticeAttendeesDetail(practice_id string, team_id string) ([]orm.Params, []orm.Params) {

	o := orm.NewOrm()
	var all []orm.Params
	var maps []orm.Params
	var responded []string
	var not_responded []orm.Params

	_, err := o.Raw("SELECT *  FROM practice_attendees  where fk_practice_id = ?", practice_id).Values(&all)
	for _, attendee := range all {
		responded = append(responded, attendee["fk_user_id"].(string))
	}

	if len(responded) > 0 {
		responded_string := strings.Join(responded[:], ",")
		count, _ := o.Raw("SELECT tj.fk_member_id FROM teamjoin tj join user_profile up on up.fk_user_id = tj.fk_member_id WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? AND tj.fk_member_id NOT IN ( "+responded_string+" ) ORDER BY lower(up.name)", team_id, 1, 2).Values(&maps)
		if count > 0 {
			not_responded = maps
		}
	} else {
		count, _ := o.Raw("SELECT tj.fk_member_id FROM teamjoin tj join user_profile up on up.fk_user_id = tj.fk_member_id WHERE tj.fk_team_id = ? AND tj.active = ? AND tj.request_status = ? ORDER BY lower(up.name)", team_id, 1, 2).Values(&maps)

		if count > 0 {
			not_responded = maps
		}
	}
	fmt.Println(err)
	return all, not_responded
}

func CreateRoster(data map[string]string, team_id string, teamRosterMembersstring string, localUserTimeZone string, teamRosterMembersarray []string, teamRosterMembersRolearray []string) ([]orm.Params, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var team_Roaster []orm.Params
	var team_Roster_Member_Role []orm.Params
	rosterSubmitStatus := "false"

	if data["RosterEventId"] == "" {
		data["RosterEventId"] = "0"
	}

	if data["RosterPracticeId"] == "" {
		data["RosterPracticeId"] = "0"
	}

	num, err := o.Raw("INSERT INTO team_roster (name, type, fk_event_id, fk_practice_id, fk_team_id, members , comments, save_draft, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id", data["NewRosterName"], data["RosterType"], data["RosterEventId"], data["RosterPracticeId"], team_id, "{"+teamRosterMembersstring+"}", data["RosterComment"], data["SaveDraft"], time.Now(), time.Now()).Values(&maps)
	fmt.Println(err)
	if num > 0 && maps[0]["id"] != nil {
		_, err := o.Raw("select *,ev.event_name as eventname,ev.active  as eventactivestatus,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD') as formatedpracticedate, tr.type as rostertype, array_length(tr.members, 1) as memberlength, tr.id as rosterid from team_roster tr left join team_practices tp on  tp.id =tr.fk_practice_id  left join event ev on  ev.id =tr.fk_event_id  where tr.id =? ", maps[0]["id"].(string)).Values(&team_Roaster)
		fmt.Println(err)
		if data["RosterEventId"] != "0" {
			num, err := o.Raw("select * from event_register where fk_event_id =?", data["RosterEventId"]).Values(&maps1)
			fmt.Println(err)
			if num > 0 {
				rosterSubmitStatus = "true"
			}
		}
		for i, m := range teamRosterMembersarray {
			if m != "" {

				teamRosterMembersRolearray[i] = strings.Replace(teamRosterMembersRolearray[i], "_", ",", -1)
				teamrosterroleaccessid, accessvaluestring := GetRoleAccessIdsByRoles(teamRosterMembersRolearray[i])
				_, _ = o.Raw("INSERT INTO team_roster_member_role (fk_roster_id, roster_member_id, member_role, role_ids, accessrolearray) VALUES (?,?,?,?,?)", maps[0]["id"], teamRosterMembersarray[i], "{"+teamRosterMembersRolearray[i]+"}", "{"+teamrosterroleaccessid+"}", "{"+accessvaluestring+"}").Values(&team_Roster_Member_Role)

			}
		}
	}
	return team_Roaster, rosterSubmitStatus
}

func GetRoosterDetails(team_id string, localUserTimeZone string, activeUser bool) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	if activeUser {
		_, err := o.Raw("select *,ev.active as eventactivestatus,ev.event_name as eventname,tp.active  as activestatus,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD') as formatedpracticedate, tr.type as rostertype, array_length(tr.members, 1) as memberlength, tr.id as rosterid, tr.fk_team_id as registerteamid, ev.rostersubmitstatus  from team_roster tr left join team_practices tp on  tp.id =tr.fk_practice_id  left join event ev on  ev.id =tr.fk_event_id where tr.fk_team_id =? and save_draft ='1' order by tr.created_at desc ", team_id).Values(&maps)
		fmt.Println(err)
	} else {
		_, err := o.Raw("select *,ev.active as eventactivestatus,ev.event_name as eventname,tp.active  as activestatus,to_char((practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD') as formatedpracticedate, tr.type as rostertype, array_length(tr.members, 1) as memberlength, tr.id as rosterid, tr.fk_team_id as registerteamid, ev.rostersubmitstatus from team_roster tr left join team_practices tp on  tp.id =tr.fk_practice_id  left join event ev on  ev.id =tr.fk_event_id where tr.fk_team_id =? order by tr.created_at desc ", team_id).Values(&maps)
		fmt.Println(err)
	}
	return maps
}

func GetIndividualRosterDetail(roasterId string) ([]orm.Params, []orm.Params, []orm.Params) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params

	_, _ = o.Raw("select *,tr.id as rosteridvalue,ev.event_name as eventname from team_roster tr left join event ev on  ev.id =tr.fk_event_id  where tr.id =? ", roasterId).Values(&maps)

	_, _ = o.Raw("select * from team_roster_member_role  where fk_roster_id =? ", roasterId).Values(&maps1)

	_, _ = o.Raw("select CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END from event_register_roster  where roster_id =? ", roasterId).Values(&maps2)

	return maps, maps1, maps2
}
func GetIndividualRosterDetail1(roasterId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("select *,tr.id as rosteridvalue,ev.event_name as eventname from team_roster tr left join event ev on  ev.id =tr.fk_event_id  where tr.id =? ", roasterId).Values(&maps)
	fmt.Println(err)

	return maps
}
func CheckRosterName(rosterName string, teamId string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false
	num, err := o.Raw("select * from team_roster where name =? and fk_team_id =?", rosterName, teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		result = true
	}
	return result
}

func UpdateRoster(data map[string]string, team_id string, teamRosterMembersstring string, teamRosterMemberarray []string, teamRosterMembersRolearray []string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var team_Roster_Member_Role []orm.Params

	var remainingUserId []string

	result := false

	if data["RosterEventId"] == "" {
		data["RosterEventId"] = "0"
	}

	if data["RosterPracticeId"] == "" {
		data["RosterPracticeId"] = "0"
	}
	individualRosterMaps := GetIndividualRosterDetail1(data["RosterUpdateId"])
	if individualRosterMaps != nil {
		membersListTemp := individualRosterMaps[0]["members"].(string)
		membersListTemp = strings.Replace(membersListTemp, "{", "", 1)
		membersListTemp = strings.Replace(membersListTemp, "}", "", 1)
		membersList := strings.Split(membersListTemp, ",")
		for _, v := range membersList {
			tempStaus := contains(teamRosterMemberarray, v)
			if !tempStaus {
				if v != "" {
					remainingUserId = append(remainingUserId, v)
				}
			}
		}
		for _, v := range remainingUserId {
			deleteIndividualrosterLineupDependencies(v, data["RosterUpdateId"])
		}

		_, err := o.Raw("UPDATE team_roster SET name = ?, type = ?, fk_event_id = ? , fk_practice_id = ? ,fk_team_id = ? ,members = ? ,comments = ?, save_draft =? ,updated_at = ? WHERE id = ?", data["NewRosterName"], data["RosterType"], data["RosterEventId"], data["RosterPracticeId"], team_id, "{"+teamRosterMembersstring+"}", data["RosterComment"], data["SaveDraft"], time.Now(), data["RosterUpdateId"]).Values(&maps)
		fmt.Println(err)
		if err == nil {
			result = true
		}
		for i, v1 := range teamRosterMembersRolearray {
			if v1 != "" {

				teamRosterMembersRolearray[i] = strings.Replace(teamRosterMembersRolearray[i], "_", ",", -1)
				teamrosterroleaccessid, accessvaluestring := GetRoleAccessIdsByRoles(teamRosterMembersRolearray[i])

				num, _ := o.Raw("select * from team_roster_member_role  where fk_roster_id =? AND roster_member_id=?", data["RosterUpdateId"], teamRosterMemberarray[i]).Values(&maps1)
				if num > 0 {
					_, _ = o.Raw("update team_roster_member_role set member_role = ?, role_ids = ?, accessrolearray=?  where fk_roster_id =? AND roster_member_id = ?", "{"+teamRosterMembersRolearray[i]+"}", "{"+teamrosterroleaccessid+"}", "{"+accessvaluestring+"}", data["RosterUpdateId"], teamRosterMemberarray[i]).Values(&maps2)

				} else {
					_, _ = o.Raw("INSERT INTO team_roster_member_role (fk_roster_id, roster_member_id, member_role, role_ids, accessrolearray) VALUES (?,?,?,?,?)", data["RosterUpdateId"], teamRosterMemberarray[i], "{"+teamRosterMembersRolearray[i]+"}", "{"+teamrosterroleaccessid+"}", "{"+accessvaluestring+"}").Values(&team_Roster_Member_Role)

				}

			}

		}

	}

	return result
}

func contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

func DeleteRosterName(rosterId string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false

	_, err := o.Raw("delete from event_register_roster where roster_id =? ", rosterId).Values(&maps)

	_, err = o.Raw("delete from team_lineup where fk_roster_id =? ", rosterId).Values(&maps)

	if err == nil {
		_, err := o.Raw("delete from team_roster where id =? ", rosterId).Values(&maps)
		if err == nil {

			_, _ = o.Raw("delete from team_roster_member_role where fk_roster_id =? ", rosterId).Values(&maps)
			result = true
		}
	}
	return result
}

func GetRosterUserDetail(rosterId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	var usermaps []orm.Params
	num, err := o.Raw("select members from team_roster where id =? ", rosterId).Values(&maps)
	fmt.Println(err)
	if num > 0 {

		membersId := maps[0]["members"].(string)
		membersId = strings.Replace(membersId, "{", "", 1)
		membersId = strings.Replace(membersId, "}", "", 1)
		_, err := o.Raw("select * from user_profile where fk_user_id IN ( " + membersId + " )").Values(&usermaps)
		fmt.Println(err)
	}

	return usermaps
}

func GetRosterDetailList(teamId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select tr.id,tr.name, tp.active  as activestatus,tr.type from team_roster tr left join team_practices tp on  tp.id =tr.fk_practice_id where tr.fk_team_id =? order by created_at desc ", teamId).Values(&maps)
	fmt.Println(err)

	return maps
}

func CreateLineup(data map[string]string, team_id string, leftPadllerMembers string, rightPadllerMembers string, otherMembers string, selectedLeftPaddlers string, selectedRightPaddlers string, selectedDrummers string, selectedSpares string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "false"
	_, err := o.Raw("INSERT INTO team_lineup (lineup_name, fk_roster_id, fk_team_id, left_padller_members, right_padller_members, other_members , selected_left_paddlers, selected_right_paddlers, selected_drummers, selected_sparess,save_draft, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", data["LineupName"], data["RosterId"], team_id, "{"+leftPadllerMembers+"}", "{"+rightPadllerMembers+"}", "{"+otherMembers+"}", "{"+selectedLeftPaddlers+"}", "{"+selectedRightPaddlers+"}", "{"+selectedDrummers+"}", "{"+selectedSpares+"}", data["SaveDraft"], time.Now(), time.Now()).Values(&maps)
	fmt.Println(err)
	if maps[0]["id"] != nil {
		result = "true"
	}
	return result
}

func GetLineUpDetails(team_id string, localUserTimeZone string, activeUser bool) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	if activeUser {
		_, err := o.Raw("select *,ev.event_name as eventname,ev.active  as eventactivestatus, tp.active  as activestatus,to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD') as formatedpracticedate, tr.type as rostertype, array_length(tr.members, 1) as memberlength, tl.id as lineupid, tr.id as rosterid, tl.save_draft as save_draft_lineup  from team_lineup tl inner join team_roster tr on  tl.fk_roster_id =tr.id  left join team_practices tp on  tp.id =tr.fk_practice_id left join event ev on  ev.id =tr.fk_event_id where tl.fk_team_id =? and tl.save_draft ='1' order by tl.created_at desc ", team_id).Values(&maps)
		fmt.Println(err)
	} else {
		_, err := o.Raw("select *,ev.event_name as eventname,ev.active  as eventactivestatus, tp.active  as activestatus,to_char((tp.practice_date::TIMESTAMPTZ AT TIME ZONE '"+localUserTimeZone+"'), 'Dy Mon DD') as formatedpracticedate, tr.type as rostertype, array_length(tr.members, 1) as memberlength, tl.id as lineupid, tr.id as rosterid, tl.save_draft as save_draft_lineup  from team_lineup tl inner join team_roster tr on  tl.fk_roster_id =tr.id  left join team_practices tp on  tp.id =tr.fk_practice_id left join event ev on  ev.id =tr.fk_event_id where tl.fk_team_id =? order by tl.created_at desc ", team_id).Values(&maps)
		fmt.Println(err)
	}
	return maps
}

func CheckLineUpName(lineupName string, team_id string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false
	num, err := o.Raw("select * from team_lineup where lineup_name =? and fk_team_id =? ", lineupName, team_id).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		result = true
	}
	return result
}

func GetLineUpIndividualDetail(lineup_id string, roster_id string) ([]orm.Params, []orm.Params) {
	o := orm.NewOrm()
	var maps []orm.Params
	var usermaps []orm.Params
	usermaps = GetRosterUserDetail(roster_id)
	_, err := o.Raw("select * from team_lineup where id =? ", lineup_id).Values(&maps)
	fmt.Println(err)

	return maps, usermaps
}

func UpdateLineup(data map[string]string, team_id string, leftPadllerMembers string, rightPadllerMembers string, otherMembers string, selectedLeftPaddlers string, selectedRightPaddlers string, selectedDrummers string, selectedSpares string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false
	_, err := o.Raw("UPDATE team_lineup SET fk_roster_id = ?, fk_team_id = ? , left_padller_members = ?,right_padller_members = ?, other_members = ? ,selected_left_paddlers = ? ,selected_right_paddlers = ? ,selected_drummers = ? ,selected_sparess = ?,save_draft =? ,updated_at = ? WHERE id = ?", data["RosterId"], team_id, "{"+leftPadllerMembers+"}", "{"+rightPadllerMembers+"}", "{"+otherMembers+"}", "{"+selectedLeftPaddlers+"}", "{"+selectedRightPaddlers+"}", "{"+selectedDrummers+"}", "{"+selectedSpares+"}", data["SaveDraft"], time.Now(), data["LineupUpdateId"]).Values(&maps)
	fmt.Println(err)
	if err == nil {
		result = true
	}
	return result
}

func DeleteLineUp(lineupId string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false
	_, err := o.Raw("delete from team_lineup where id =? ", lineupId).Values(&maps)
	if err == nil {
		result = true
	}
	return result
}

func GetTeamListConcernUserByUserId(id string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT * FROM team WHERE "+id+" = ANY(fk_co_captains) or  fk_captain_id =? ", id).Values(&maps)
	fmt.Println(err)

	return maps
}

func GetTeamListConcernUserByUserId_SuperAdmin(id string, searchtext string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	if id != "" {
		_, err := o.Raw("SELECT id, team_name FROM team WHERE ("+id+" = ANY(fk_co_captains) or  fk_captain_id =? ) and (team_name ILIKE '%"+searchtext+"%')", id).Values(&maps)
		fmt.Println(err)
	} else {
		_, err := o.Raw("SELECT id, team_name FROM team WHERE team_name ILIKE '%" + searchtext + "%'").Values(&maps)
		fmt.Println(err)
	}
	return maps
}

func GetRosterListDetailByTeamId(searchtext string, team_id string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT id, name FROM team_roster WHERE name ILIKE '%"+searchtext+"%' and fk_team_id =?", team_id).Values(&maps)
	fmt.Println(err)
	return maps
}

func GetRosterByTeamIdEventId(event_id string, team_id string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT id, name FROM team_roster WHERE (fk_team_id =?  and type='Event') and fk_event_id =?", team_id, event_id).Values(&maps)
	fmt.Println(err)
	return maps
}

func GetEventListAutoComplete(searchtext string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	currentTime := time.Now().UTC()
	formatedDate := currentTime.Format("2006-01-02")
	_, err := o.Raw("SELECT id, event_name FROM event WHERE active =1 and start_date >= '" + formatedDate + "'::timestamp  and event_name ILIKE '%" + searchtext + "%'").Values(&maps)
	fmt.Println(err)
	return maps
}

func SetTeamFreeTrial(teamId string) {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("UPDATE team set free_trial = ? WHERE id = ?", false, teamId).Values(&maps)
	fmt.Println(err)
}

func SetTeam_Subscription_Notification_Popup(teamId string, subscription_Checkbox bool) {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("UPDATE team set popup_notification_on_off = ? WHERE id = ?", subscription_Checkbox, teamId).Values(&maps)
	fmt.Println(err)
}

func GetTeamSlugText(teamId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	slugText := ""
	num, _ := o.Raw("SELECT slug FROM team WHERE id = ? ", teamId).Values(&maps)
	if num > 0 {
		slugText = maps[0]["slug"].(string)
	}

	return slugText
}

func activateFreeTrial(team_id string, practiceDate string) bool {
	result := false
	o := orm.NewOrm()
	var trialMaps []orm.Params
	var trialMaps1 []orm.Params
	num, err := o.Raw("select * from team where id=?", team_id).Values(&trialMaps)
	if num > 0 {
		statusValue := false
		if trialMaps[0]["free_trial"] == "true" {
			if trialMaps[0]["trial_period_end_date"] != nil {
				statusValue = common.PracticeDateIsOldDateOrNot(trialMaps[0]["trial_period_end_date"].(string), practiceDate)
			} else {
				statusValue = true
			}

			if trialMaps[0]["trial_period_end_date"] == nil || statusValue {
				str := practiceDate
				local, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
				if err == nil {
					afterTwentyOneDays := local.Add(time.Hour * 24 * 21)
					_, err = o.Raw("UPDATE team SET trial_period_end_date = ?, free_trial =? WHERE id = ? ", afterTwentyOneDays, true, team_id).Values(&trialMaps1)
					fmt.Println(err)
					if err == nil {
						result = true
					}
				}
			}
		}
	}
	fmt.Println(err)
	return result
}

func checkFreeTrailValid(maps []orm.Params, team_id string) bool {
	result := false
	o := orm.NewOrm()
	var trialMaps []orm.Params

	if maps[0]["free_trial"].(string) == "true" && maps[0]["trial_period_end_date"] != nil {
		str := maps[0]["trial_period_end_date"].(string)
		practiceDateValue, err := time.Parse(constants.DATE_TIME_ZONE_FORMAT, str)
		if err == nil {
			currentDate := time.Now().UTC()
			if currentDate.After(practiceDateValue) {
				_, err = o.Raw("UPDATE team SET free_trial =? WHERE id = ? ", false, team_id).Values(&trialMaps)
				if err == nil {
					result = true
				}
			}
		}
	}
	return result
}

func CheckCaptainSubscriptionOrNot(captainId string, teamId string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"

	num, err := o.Raw("SELECT * FROM money_subscription WHERE fk_team_id = ? and fk_user_id = ? and subscription_active_inactive =true and (status ='active' or status ='trialing')", teamId, captainId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		result = "true"
	}
	return result
}

func TeamRemoveCaptain(captainId string, teamId string, loginUserId string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"

	num, err := o.Raw("SELECT * FROM team WHERE id = ? and active=? ", teamId, 1).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		tempcaptainId := captainId
		if captainId == "0" && maps[0]["fk_captain_id"] != nil {
			tempcaptainId = maps[0]["fk_captain_id"].(string)
		}
		_, err := o.Raw("UPDATE team SET fk_captain_id = ? , fk_co_captains = array_remove(fk_co_captains, '"+loginUserId+"') WHERE id = ?", loginUserId, teamId).Values(&maps)
		fmt.Println(err)
		if err == nil {
			_, err := o.Raw("UPDATE team SET fk_captain_id = ? , fk_co_captains =  array_append(fk_co_captains, '"+tempcaptainId+"')  WHERE id = ?", loginUserId, teamId).Values(&maps)
			fmt.Println(err)
			if err == nil {
				AddTeamCaptainRoleToUser(loginUserId)
				result = "true"
			}
		}
	}
	return result
}

func GetSmsCount(teamId string) (int, string) {
	o := orm.NewOrm()

	var maps []orm.Params
	tempSmsCount := 0
	subcriptionSmsCount := 0
	IsOldDate := "false"
	num, err := o.Raw("SELECT * FROM team WHERE id = ?", teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		tempCount := maps[0]["sms_limits"].(string)
		tempSmsCount, _ = strconv.Atoi(tempCount)
		if maps[0]["sms_popup_notification_enddate"] != nil {
			IsOldDate = common.IsFeatureDate(maps[0]["sms_popup_notification_enddate"].(string))
		}
	}

	num, err = o.Raw("SELECT sms_limits FROM sms_package WHERE fk_team_id = ?", teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		tempCount := maps[0]["sms_limits"].(string)
		subcriptionSmsCount, _ = strconv.Atoi(tempCount)
	}
	return tempSmsCount + subcriptionSmsCount, IsOldDate
}

func InactiveSmsSubscriptionPopup(teamId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "false"
	featureDate := common.GetFeatureDateFromNumberOfdays(1)
	_, err := o.Raw("UPDATE team SET sms_popup_notification_enddate = ? WHERE id = ?", featureDate, teamId).Values(&maps)
	if err == nil {
		result = "true"
	}
	return result
}

func ResetSubscriptionSmsPackageCount(teamId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "false"
	num, err := o.Raw("SELECT * FROM sms_package WHERE fk_team_id = ?", teamId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		expiry_date := maps[0]["expiry_date"].(string)
		featureDate := common.IsFeatureDate(expiry_date)

		if featureDate == "false" {
			_, err := o.Raw("UPDATE sms_package SET sms_limits = ? WHERE fk_team_id = ?", 0, teamId).Values(&maps)
			if err == nil {
				result = "true"
			}
		}
	}
	return result
}

func CheckShowPopupTrialPeriodNotification(team_view map[string]orm.Params) (int, string, string) {
	result := "false"
	noOfDays := 0
	popupOnOff := "true"
	if team_view["team_0"]["free_trial"].(string) == "true" {
		if team_view["team_0"]["trial_period_end_date"] != nil {
			trial_period_end_date := team_view["team_0"]["trial_period_end_date"].(string)
			practiceStartDate := common.SubractDate(trial_period_end_date)
			if practiceStartDate == "true" {
				result = common.IsFeatureDate(trial_period_end_date)
				noOfDays = common.DayDiffrence(trial_period_end_date)
				if team_view["team_0"]["freetrial_popup_notification_on_off"] != nil {
					popupOnOff = team_view["team_0"]["freetrial_popup_notification_on_off"].(string)
				}
			}
		}
	}
	return noOfDays, result, popupOnOff
}

func InactiveFreeTrialSubscriptionPopup(teamId string, status string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "false"
	_, err := o.Raw("UPDATE team SET freetrial_popup_notification_on_off =? WHERE id = ?", status, teamId).Values(&maps)
	if err == nil {
		result = "true"
	}
	return result
}

func ActiveFreeTrialForExistingUsers() string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	result := "false"

	num, err := o.Raw("SELECT * FROM team WHERE active=1").Values(&maps)
	fmt.Println(err)
	if num > 0 && err == nil {
		local := time.Now().UTC()
		startingDateFromThisYear := time.Date(local.Year(), 01, 01, 00, 00, 00, 00, time.UTC)
		afterTwentyOneDays := local.Add(time.Hour * 24 * 21)
		fmt.Println(startingDateFromThisYear)
		for _, m := range maps {
			num1, err := o.Raw("select * from team_practices where fk_team_id =? and practice_date >=? order by id desc", m["id"].(string), startingDateFromThisYear).Values(&maps1)
			fmt.Println(err)
			if num1 > 0 && m["trial_period_end_date"] == nil {
				_, err = o.Raw("UPDATE team SET trial_period_end_date = ?, free_trial =?WHERE id = ? ", afterTwentyOneDays, true, m["id"].(string)).Values(&maps1)
				if err == nil {
					result = "true"
				}
			}
		}
	}
	return result
}

func MarkPracticeAttendanceViaEmail(teamID string, practiceID string, userId string, responseText string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	result := "fail"

	num, err := o.Raw("SELECT * FROM team WHERE active=1 and id =?", teamID).Values(&maps)
	fmt.Println(err)
	if num > 0 && err == nil {
		num1, err := o.Raw("SELECT * FROM teamjoin WHERE request_status = 2 AND fk_team_id = ? AND fk_member_id = ? ", teamID, userId).Values(&maps1)
		fmt.Println(err)
		if num1 > 0 && err == nil {
			num2, err := o.Raw("SELECT * FROM team_practices WHERE active =1 and fk_team_id = ? and id=?", teamID, practiceID).Values(&maps)

			if num2 > 0 && err == nil {
				num3, err := o.Raw("SELECT * FROM practice_attendees WHERE fk_practice_id = ? and fk_user_id = ?", practiceID, userId).Values(&maps)

				if num3 > 0 && err == nil {
					_, err = o.Raw("UPDATE practice_attendees SET response = ? WHERE fk_practice_id = ? AND fk_user_id = ? ", responseText, practiceID, userId).Values(&maps)
					if err == nil {
						result = "success"
					}
				} else {
					_, err = o.Raw("INSERT INTO practice_attendees (response,fk_practice_id,fk_user_id) VALUES (?,?,?) ", responseText, practiceID, userId).Values(&maps)
					if err == nil {
						result = "success"
					}
				}
			}
		}
	}
	return result
}

func Add_400_Sms_Every_Month(teamID string, userId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	//var maps1 []orm.Params
	result := "fail"

	num, err := o.Raw("SELECT * FROM money_subscription WHERE status='active' and subscription_plan_id ='yearly-subscription'").Values(&maps)
	if num > 0 && err == nil {
		if maps[0]["sms_pack_enddate"] != nil {
			response := common.IsOldDate(maps[0]["sms_pack_enddate"].(string))
			response1 := common.IsFeatureDate(maps[0]["current_end_date"].(string))
			if response == "true" && response1 == "true" {
				t := time.Now().UTC()
				_, month, _ := t.Date()
				year, _ := t.ISOWeek()
				lostDay := common.LastDayOfMonth(year, month)
				fmt.Println(lostDay)
			}
		}
	}
	return result
}

func Resetfreetrial_subscription_activationpopup(userId string) {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("UPDATE team SET freetrial_popup_notification_on_off = 'true' WHERE free_trial = true and freetrial_popup_notification_on_off = 'false' and fk_captain_id = ?", userId).Values(&maps)
	fmt.Println(err)

	_, err = o.Raw("UPDATE team SET popup_notification_on_off = 'true' WHERE popup_notification_on_off = 'false' and subscription_on_off = false and fk_captain_id = ?", userId).Values(&maps)
	fmt.Println(err)
}

func UpdateChange_Captain_Status(teamID string, status bool) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "true"
	_, err := o.Raw("UPDATE team SET change_captain_status_by_admin = ? where id =?", status, teamID).Values(&maps)
	if err != nil {
		result = "false"
	}

	return result
}

func AddTeamCaptainRoleToUser(captain_cocaptainuserid string) string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := "true"
	num, err := o.Raw("select * from user_profile WHERE 'Team Management' = ANY (activities)  and fk_user_id = ?", captain_cocaptainuserid).Values(&maps)
	if num == 0 && err == nil {
		_, err := o.Raw("UPDATE user_profile SET activities = array_append(activities, 'Team Management' ) WHERE fk_user_id = ? ", captain_cocaptainuserid).Values(&maps)
		if err != nil {
			result = "false"
		}
	}
	return result

}

func GetActiveUserStatus(userId string, teamId string) bool {
	o := orm.NewOrm()
	var maps []orm.Params
	result := false
	num, err := o.Raw("SELECT * FROM team WHERE ("+userId+" = ANY(fk_co_captains) or  fk_captain_id =?) and id=? ", userId, teamId).Values(&maps)
	if num == 0 && err == nil {
		num, err := o.Raw("select * from teamjoin where active = 1 AND request_status = 2 AND fk_team_id =? And fk_member_id = ?", teamId, userId).Values(&maps)
		fmt.Println(err)
		if num > 0 && err == nil {
			result = true
		}
	}
	return result
}

func GetActiveTeamMemberForWaiver1(id string) []string {

	var alreadyMember []string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT tj.fk_member_id FROM teamjoin tj INNER JOIN team t on tj.fk_team_id = t.id INNER JOIN users us on tj.fk_member_id = us.id INNER JOIN user_profile up on us.id = up.fk_user_id WHERE tj.active = 1 AND request_status = 2 AND fk_team_id = ? ORDER BY lower(name) ", id).Values(&maps)
	fmt.Println(err)
	if num > 0 && err == nil {
		for _, v := range maps {
			alreadyMember = append(alreadyMember, v["fk_member_id"].(string))
		}

	}
	return alreadyMember
}

func InactiveBasicSubscriptionPopup(teamId string, status string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	result := "false"
	_, err := o.Raw("UPDATE team SET popup_notification_on_off = ? WHERE id = ?", status, teamId).Values(&maps)
	if err == nil {
		result = "true"
	}
	return result
}

func GetTeamRoosterListByTeamId(teamId string, regeventid string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("select tr.name,tr.id  from  team_roster tr where not exists (select 1 from event_register_roster  err where err.roster_id= tr.id and err.fk_event_reg_id != ?) and fk_team_id= ?", regeventid, teamId).Values(&maps)
	fmt.Println(err)
	return maps
}

func CheckUserIsCaptainOrCoCaptain(teamId string, userId string) bool {
	o := orm.NewOrm()
	result := false
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM team WHERE ("+userId+" = ANY(fk_co_captains) or  fk_captain_id =?) and id=? ", userId, teamId).Values(&maps)
	if num > 0 {
		result = true
	}
	return result
}

func CheckUserIsCaptain(teamId string, userId string) bool {
	o := orm.NewOrm()
	result := false
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM team WHERE fk_captain_id =? and id=? ", userId, teamId).Values(&maps)
	if num > 0 {
		result = true
	}
	return result
}

func CheckUserIsCoCaptain(teamId string, userId string) bool {
	o := orm.NewOrm()
	result := false
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM team WHERE "+userId+" = ANY(fk_co_captains) and id=? ", teamId).Values(&maps)
	if num > 0 {
		result = true
	}
	return result
}

func GetTeamSlugNameByTeamId(teamId string) string {
	o := orm.NewOrm()
	result := ""
	var maps []orm.Params
	num, _ := o.Raw("SELECT slug FROM team WHERE id=? ", teamId).Values(&maps)
	if num > 0 {
		result = maps[0]["slug"].(string)
	}
	return result
}

func GetTeamDetailByTeamId(teamId string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", teamId).Values(&maps)
	return maps
}

func GetRosterSubmittedIdByClass_Division_EventId(register_eventId string, tempClass string, tempDiv string, tempBoatType string, tempDistance string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	var num int64
	var err error
	roster_id := "0"
	if tempBoatType != "" && tempDistance != "" {
		num, err = o.Raw("SELECT roster_id FROM event_register_roster  WHERE fk_event_reg_id =? and clases = ? and division=? and boat_type =? and distance =? order by id desc", register_eventId, tempClass, tempDiv, tempBoatType, tempDistance).Values(&maps)
	} else {
		num, err = o.Raw("SELECT roster_id FROM event_register_roster  WHERE fk_event_reg_id =? and clases = ? and division=? order by id desc", register_eventId, tempClass, tempDiv).Values(&maps)
	}
	fmt.Println(err)
	if num > 0 {
		if maps[0]["roster_id"] != nil {
			roster_id = maps[0]["roster_id"].(string)
		}
	}
	return roster_id
}

func GetAllRolesList() []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT * FROM all_role order by rolename asc").Values(&maps)
	fmt.Println(err)
	return maps
}

func GetRoleAccessIdsByRoles(teamRosterMembersRolearray string) (string, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var roleaccessid []string
	var accessArray []string

	roleaccessidstring := ""
	accessvaluestring := ""

	teamRosterMembersRolearraystring := strings.Split(teamRosterMembersRolearray, ",")
	for _, k := range teamRosterMembersRolearraystring {
		num, err := o.Raw("select id,fk_access_id  from role_access_jct where fk_role_id in (select id from all_role where rolename_value =?)", k).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			roleaccessid = append(roleaccessid, maps[0]["id"].(string))
			if maps[0]["fk_access_id"] != nil {

				accessid := strings.Replace(maps[0]["fk_access_id"].(string), "{", "", -1)
				accessid = strings.Replace(accessid, "}", "", -1)

				num1, err1 := o.Raw("select accessname_value from all_access where id IN (" + accessid + ")").Values(&maps1)
				fmt.Println(err1)
				if num1 > 0 {
					for _, k1 := range maps1 {
						if !common.StringInArray(k1["accessname_value"].(string), accessArray) {
							accessArray = append(accessArray, k1["accessname_value"].(string))
						}

					}
				}
			}
		}
	}

	if len(roleaccessid) > 0 {
		roleaccessidstring = strings.Join(roleaccessid, ", ")
	}

	if len(accessArray) > 0 {
		accessvaluestring = strings.Join(accessArray, ", ")
	}

	return roleaccessidstring, accessvaluestring
}

func GetRoleNameByRolesvalues(teamMemberRolearray string, roleaccess []string) []string {
	o := orm.NewOrm()
	var maps []orm.Params

	teamMemberRolearraystring1 := strings.Replace(teamMemberRolearray, "{", "", -1)
	teamMemberRolearraystring1 = strings.Replace(teamMemberRolearraystring1, "}", "", -1)
	teamMemberRolearraystring := strings.Split(teamMemberRolearraystring1, ",")
	for _, k := range teamMemberRolearraystring {
		num, err := o.Raw("select rolename from all_role where rolename_value =?", k).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			if !common.StringInArray(maps[0]["rolename"].(string), roleaccess) {
				roleaccess = append(roleaccess, maps[0]["rolename"].(string))
			}
		}
	}

	return roleaccess
}

func GetAccessNameByAccessvalues(teamMemberAccessarray string, accessrole []string) []string {
	o := orm.NewOrm()
	var maps []orm.Params
	teamMemberAccessarraystring1 := strings.Replace(teamMemberAccessarray, "{", "", -1)
	teamMemberAccessarraystring1 = strings.Replace(teamMemberAccessarraystring1, "}", "", -1)
	teamMemberAccessarraystring := strings.Split(teamMemberAccessarraystring1, ",")
	for _, k := range teamMemberAccessarraystring {
		num, err := o.Raw("select accessname from all_access where accessname_value =?", k).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			if maps[0]["accessname"] != nil {
				if !common.StringInArray(maps[0]["accessname"].(string), accessrole) {
					accessrole = append(accessrole, maps[0]["accessname"].(string))
				}
			}
		}
	}

	return accessrole
}

func GetAccessNameByAccessCode(teamMemberAccessarray string, accessrole []string) []string {
	o := orm.NewOrm()
	var maps []orm.Params
	teamMemberAccessarraystring1 := strings.Replace(teamMemberAccessarray, "{", "", -1)
	teamMemberAccessarraystring1 = strings.Replace(teamMemberAccessarraystring1, "}", "", -1)
	teamMemberAccessarraystring := strings.Split(teamMemberAccessarraystring1, ",")
	for _, k := range teamMemberAccessarraystring {
		num, err := o.Raw("select accessname_value_code from all_access where accessname_value =?", k).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			if maps[0]["accessname_value_code"] != nil {
				acesscode := strings.ToUpper(maps[0]["accessname_value_code"].(string))
				if !common.StringInArray(acesscode, accessrole) {
					accessrole = append(accessrole, acesscode)
				}
			}
		}
	}

	return accessrole
}

func GetAccessNameByAccessId(teamMemberAccessarray string, accessrole []string) []string {
	o := orm.NewOrm()
	var maps []orm.Params
	teamMemberAccessarraystring1 := strings.Replace(teamMemberAccessarray, "{", "", -1)
	teamMemberAccessarraystring1 = strings.Replace(teamMemberAccessarraystring1, "}", "", -1)
	teamMemberAccessarraystring := strings.Split(teamMemberAccessarraystring1, ",")
	for _, k := range teamMemberAccessarraystring {
		num, err := o.Raw("select accessname_value_code from all_access where id =?", k).Values(&maps)
		fmt.Println(err)
		if num > 0 {
			if maps[0]["accessname_value_code"] != nil {
				acesscode := maps[0]["accessname_value_code"].(string)
				if !common.StringInArray(acesscode, accessrole) {
					accessrole = append(accessrole, acesscode)
				}
			}
		}
	}

	return accessrole
}

func GetRoleValueArrayByRolesvalues(teamMemberRolearray string, roleaccess []string) []string {
	teamMemberRolearraystring1 := strings.Replace(teamMemberRolearray, "{", "", -1)
	teamMemberRolearraystring1 = strings.Replace(teamMemberRolearraystring1, "}", "", -1)
	teamMemberRolearraystring := strings.Split(teamMemberRolearraystring1, ",")
	for _, k := range teamMemberRolearraystring {
		if !common.StringInArray(k, roleaccess) && k != "" {
			roleaccess = append(roleaccess, k)
		}
	}

	return roleaccess
}

func GetAccessNameByAccessNameValue(teamMemberAccessarray string, accessrole []string) []string {
	teamMemberAccessarraystring1 := strings.Replace(teamMemberAccessarray, "{", "", -1)
	teamMemberAccessarraystring1 = strings.Replace(teamMemberAccessarraystring1, "}", "", -1)
	teamMemberAccessarraystring := strings.Split(teamMemberAccessarraystring1, ",")
	for _, k := range teamMemberAccessarraystring {

		if !common.StringInArray(k, accessrole) {
			accessrole = append(accessrole, k)
		}
	}

	return accessrole
}
