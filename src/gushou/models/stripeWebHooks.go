package models

import (
	"fmt"
	"gushou/common"
	"gushou/constants"
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/invoice"
	"github.com/stripe/stripe-go/sub"
)

func GetSubscriptionDetailsFromStripe(tempSubscriptionId string) (*stripe.Subscription, error) {
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	s, err := sub.Get(tempSubscriptionId, nil)

	return s, err
}

func UpdateStripeSubscriptionDetailsFromStripe(tempSubscriptionId string) int {
	result := 400
	o := orm.NewOrm()
	var maps []orm.Params

	s, err := GetSubscriptionDetailsFromStripe(tempSubscriptionId)

	if err == nil {
		periodStart := time.Unix(s.CurrentPeriodStart, 0)
		periodEnd := time.Unix(s.CurrentPeriodEnd, 0)
		num, err := o.Raw("select * from money_subscription where subscription_id=? ", tempSubscriptionId).Values(&maps)
		if num > 0 && err == nil {
			_, err = o.Raw("UPDATE money_subscription SET subscription_plans_id=? ,status=?,current_start_date=?,current_end_date=?,subscription_end_date=?,updated_at=? WHERE subscription_id = ? ", s.Plan.ID, s.Status, periodStart, periodEnd, periodEnd, time.Now(), tempSubscriptionId).Values(&maps)
			fmt.Println(err)
			if err == nil {
				result = 200
			}
		}
	}
	return result
}

func UpdateStripeSubscriptionCanceledDetailsFromStripe(tempSubscriptionId string) int {
	result := 400
	s, err := GetSubscriptionDetailsFromStripe(tempSubscriptionId)

	if err == nil {
		result = deactivateSubscriptionPlanBySubsriptionId(s, tempSubscriptionId)
	}
	return result
}

func deactivateSubscriptionPlanBySubsriptionId(subscriptionDetail *stripe.Subscription, tempSubscriptionIdValue string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	result := 400
	statusValue := true
	num, err := o.Raw("select * from money_subscription where subscription_id=? ", tempSubscriptionIdValue).Values(&maps)
	if num > 0 && err == nil {
		if subscriptionDetail.Status == "canceled" {
			periodStart := time.Unix(subscriptionDetail.CurrentPeriodStart, 0)
			periodEnd := time.Unix(subscriptionDetail.CurrentPeriodEnd, 0)

			num1, err1 := o.Raw("select * from money_subscription where subscription_active_inactive =true and fk_team_id=? order by id desc limit 1", maps[0]["fk_team_id"].(string)).Values(&maps2)

			if maps[0]["subscription_active_inactive"].(string) == "true" {
				_, err = o.Raw("UPDATE team SET subscription_on_off=?, popup_notification_on_off='true' WHERE id = ?", false, maps[0]["fk_team_id"].(string)).Values(&maps1)
				if err != nil {
					statusValue = false
				}
			} else if num1 == 0 && err1 == nil {
				var maps3 []orm.Params
				num1, err1 := o.Raw("select * from money_subscription where status='active' and fk_team_id=? order by id desc limit 1", maps[0]["fk_team_id"].(string)).Values(&maps3)
				if num1 > 0 && err1 == nil {
					if maps3[0]["subscription_id"].(string) == tempSubscriptionIdValue {
						_, err = o.Raw("UPDATE team SET subscription_on_off=?, popup_notification_on_off='true' WHERE id = ?", false, maps[0]["fk_team_id"].(string)).Values(&maps1)
						if err != nil {
							statusValue = false
						}
					}
				}

			}
			if statusValue {
				_, err = o.Raw("UPDATE money_subscription SET subscription_plans_id=? ,status=?,current_start_date=?,current_end_date=?,subscription_end_date=?,subscription_active_inactive=?,updated_at=? WHERE subscription_id = ? ", subscriptionDetail.Plan.ID, subscriptionDetail.Status, periodStart, periodEnd, periodEnd, false, time.Now(), tempSubscriptionIdValue).Values(&maps1)
				fmt.Println(err)
				if err == nil {
					result = 200
				}
			}
		}
	}
	return result
}

func GetInvoiceDetailsFromStripe(tempInvoiceId string) (*stripe.Invoice, error) {
	stripe.Key = constants.STRIPE_ENV_SECRET_KEY
	s, err := invoice.Get(tempInvoiceId, nil)

	return s, err
}

func CreateInvoice(tempInvoiceId string) int {
	result := 400
	s, err := GetInvoiceDetailsFromStripe(tempInvoiceId)

	if err == nil {
		result = AddInvoiceDetail(s, tempInvoiceId)
	}
	return result
}

func AddInvoiceDetail(subscriptionDetail *stripe.Invoice, tempInvoiceIdValue string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	result := 400
	num, err := o.Raw("select * from invoice_detail where invoice_id=? ", tempInvoiceIdValue).Values(&maps)
	if num == 0 && err == nil {
		periodStart := time.Unix(subscriptionDetail.PeriodStart, 0)
		periodEnd := time.Unix(subscriptionDetail.PeriodEnd, 0)
		_, err = o.Raw("INSERT INTO invoice_detail (invoice_id, currency,fk_stripe_cust_id, paid, period_end, period_start, receipt_number,description,statement_descriptor,fk_subscription_id,subtotal,tax,tax_percent,total, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", subscriptionDetail.ID, subscriptionDetail.Currency, subscriptionDetail.Customer.ID, subscriptionDetail.Paid, periodStart, periodEnd, subscriptionDetail.ReceiptNumber, subscriptionDetail.Description, subscriptionDetail.StatementDescriptor, subscriptionDetail.Subscription, subscriptionDetail.Subtotal, subscriptionDetail.Tax, subscriptionDetail.TaxPercent, subscriptionDetail.Total, time.Now(), time.Now()).Values(&maps)
		fmt.Println(err)
		if err == nil {
			result = 200
		}
	}
	return result
}

func InvoicePaymentFailed_InvoiceUpdated(tempInvoiceId string) int {
	result := 400
	s, err := GetInvoiceDetailsFromStripe(tempInvoiceId)

	if err == nil {
		result = InvoiceCreatedOrUpdated(s, tempInvoiceId)
	}
	return result
}

func InvoiceCreatedOrUpdated(subscriptionInvoiceDetail *stripe.Invoice, tempInvoiceIdValue string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	result := 400
	num, err := o.Raw("select * from invoice_detail where invoice_id=? ", tempInvoiceIdValue).Values(&maps)
	if num > 0 && err == nil {
		result = UpdateInvoice_Detail(subscriptionInvoiceDetail, tempInvoiceIdValue)
	} else if num == 0 && err == nil {
		result = AddInvoiceDetail(subscriptionInvoiceDetail, tempInvoiceIdValue)
	}
	return result
}

func UpdateInvoice_Detail(invoiceDetail *stripe.Invoice, tempInvoiceIdValue string) int {
	result := 400
	o := orm.NewOrm()
	var maps []orm.Params
	periodStart := time.Unix(invoiceDetail.PeriodStart, 0)
	periodEnd := time.Unix(invoiceDetail.PeriodEnd, 0)
	_, err := o.Raw("UPDATE invoice_detail SET currency=?, fk_stripe_cust_id=?, paid=?, period_end=?, period_start=?, receipt_number=?, description=? ,statement_descriptor=? ,fk_subscription_id=?,subtotal=?,tax=?,tax_percent=?,total=?, updated_at=? WHERE invoice_id = ? ", invoiceDetail.Currency, invoiceDetail.Customer.ID, invoiceDetail.Paid, periodStart, periodEnd, invoiceDetail.ReceiptNumber, invoiceDetail.Description, invoiceDetail.StatementDescriptor, invoiceDetail.Subscription, invoiceDetail.Subtotal, invoiceDetail.Tax, invoiceDetail.TaxPercent, invoiceDetail.Total, time.Now(), invoiceDetail.ID).Values(&maps)
	fmt.Println(err)
	if err == nil {
		result = 200
	}
	return result
}

func InvoicePaymentSucceeded(tempInvoiceId string) int {
	result := 400
	s, err := GetInvoiceDetailsFromStripe(tempInvoiceId)

	if err == nil {
		result = InvoiceCreatedOrUpdated(s, tempInvoiceId)

		if result == 200 && s.Paid {
			result = addSmsCount(s, tempInvoiceId)
		}
	}
	return result
}

func addSmsCount(invoiceDetail *stripe.Invoice, tempInvoiceIdValue string) int {
	result := 400
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	tempSubscriptionIdValue := invoiceDetail.Subscription

	num, err := o.Raw("select * from money_subscription where subscription_id=? ", tempSubscriptionIdValue).Values(&maps)
	if num > 0 && err == nil {
		num, err := o.Raw("select * from team where id=? ", maps[0]["fk_team_id"].(string)).Values(&maps1)
		if num > 0 && err == nil {
			smsCount := 0
			if maps1[0]["sms_limits"] != nil {
				tempCount := maps1[0]["sms_limits"].(string)
				smsCount, _ = strconv.Atoi(tempCount)
			}
			smsCount = smsCount + constants.SMS_COUNT
			_, err = o.Raw("UPDATE team SET sms_limits=? WHERE id = ?", smsCount, maps1[0]["id"].(string)).Values(&maps1)
			if err == nil {
				result = 200
			}
		}
	}
	return result
}

func AddConnectStripeDetail(stripesuceessdetail common.StripeConnectAccountSucessResponse, eventid string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "Error"
	num, err := o.Raw("select * from user_stripe_connect_detail where fk_event_id=? ", eventid).Values(&maps)
	if num > 0 && err == nil {
		_, err = o.Raw("UPDATE user_stripe_connect_detail SET access_token=?, refresh_token=?, stripe_user_id=?, created_at=? WHERE fk_event_id = ?", stripesuceessdetail.Access_token, stripesuceessdetail.Refresh_token, stripesuceessdetail.Stripe_user_id, time.Now(), eventid).Values(&maps)
		if err == nil {
			result = "Sucessfully Connect Your Account"
		}
	} else {
		_, err := o.Raw("INSERT INTO user_stripe_connect_detail (access_token, refresh_token, stripe_user_id, fk_event_id, created_at) VALUES (?,?,?,?,?) ", stripesuceessdetail.Access_token, stripesuceessdetail.Refresh_token, stripesuceessdetail.Stripe_user_id, eventid, time.Now()).Values(&maps)
		if err == nil {
			result = "Sucessfully Connect Your Account"
		}
	}

	return result

}

func CheckAlreadyConnetWithStripe(eventid string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := ""
	num, _ := o.Raw("SELECT * FROM user_stripe_connect_detail WHERE fk_event_id = ? ", eventid).Values(&maps)
	if num > 0 {
		result = "Already Stripe Connect With your account"
	}

	return result

}

func CheckAlreadyConnetWithStripeToHideButton(eventid string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"
	num, _ := o.Raw("SELECT * FROM user_stripe_connect_detail WHERE fk_event_id = ? ", eventid).Values(&maps)
	if num > 0 {
		result = "true"
	}

	return result

}

func CheckStatecodeExistByStatecode(statecode string) (string, string) {

	o := orm.NewOrm()

	var maps []orm.Params
	result := ""
	eventid := ""
	num, err := o.Raw("SELECT fk_event_id FROM user_stripe_verification_tokens WHERE token = ?", statecode).Values(&maps)
	fmt.Println(err)
	if num == 0 {
		result = "Please click the latest Stripe connect link."
	} else {
		if maps[0]["fk_event_id"] != nil {
			eventid = maps[0]["fk_event_id"].(string)
		}
	}

	return result, eventid

}
