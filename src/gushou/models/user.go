package models

import (
	"crypto/rand"
	_ "encoding/base64"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/mattbaird/gochimp"
	"github.com/metakeule/fmtdate"
	"golang.org/x/crypto/bcrypt"
)

type Users struct {
	Id            int `orm:"pk",-"`
	_Id           string
	EmailAddress  string
	EmailVerified bool
	Password      string
}
type UserProfile struct {
	Id         int `orm:"pk","-"`
	FkUserId   int
	Name       string
	LastName   string
	Activities string
	Active     int
}

func init() {
	orm.RegisterModel(new(Users))
	orm.RegisterModel(new(UserProfile))
}

func Login(email string, password string) map[string]string {
	// var stock []Stock
	// o := orm.NewOrm()
	// num, err := o.Raw("SELECT name FROM item_master WHERE name = ?", "Test").QueryRows(&stock)
	// if err == nil {
	// 	fmt.Println("user nums: ", num)
	// }
	result := make(map[string]string)
	result["status"] = "0"

	o := orm.NewOrm()
	user := Users{EmailAddress: email, EmailVerified: true}
	err := o.Read(&user, "EmailAddress", "EmailVerified")
	fmt.Println(err)

	if err == orm.ErrNoRows {
		fmt.Println("No result found.")
		user = Users{EmailAddress: email}
		err = o.Read(&user, "EmailAddress")
		if err == orm.ErrNoRows {
			fmt.Println("No result found.")
			result["errorMessage"] = "User not found"

		} else if err == orm.ErrMissPK {
			fmt.Println("No primary key found.")
		} else {

			if !user.EmailVerified {

				// For old users where email verified field is blank or false
				fmt.Println("Show reset link button json1")
				result["notVerified"] = "true"
				id := strconv.Itoa(user.Id)
				result["user_id"] = id
			} else {
				password_byte := []byte(password)
				hash_byte := []byte(user.Password)
				err = bcrypt.CompareHashAndPassword(hash_byte, password_byte)
				fmt.Println(err)
				if err == nil {

					user_profile := UserProfile{FkUserId: user.Id}
					err := o.Read(&user_profile, "FkUserId")
					if err == orm.ErrNoRows {
						fmt.Println("No result found.")
					} else if err == orm.ErrMissPK {
						fmt.Println("No primary key found.")
					} else {

						if user_profile.Active != 0 {
							teamManagmentStatus, eventorganisingStatus, paddlingStatus := common.CheckUserRole(user_profile.Activities)

							result["team_manage_admin"] = teamManagmentStatus
							result["event_manage_admin"] = eventorganisingStatus
							result["paddling_admin"] = paddlingStatus
						}
						if strings.Contains(user_profile.Activities, "Super Admin") && user_profile.Active != 0 {

							fmt.Println("Super Admin")
							result["status"] = "1"
							result["first_name"] = user_profile.Name
							result["last_name"] = user_profile.LastName
							result["super_admin"] = "true"

							id := strconv.Itoa(user_profile.Id)
							result["profile_id"] = id

							id = strconv.Itoa(user.Id)
							result["user_id"] = id
						} else if user_profile.Active == 1 && !user.EmailVerified {

							// For old users where email verified field is blank or false
							fmt.Println("Show reset link button")
							result["notVerified"] = "true"
							id := strconv.Itoa(user.Id)
							result["user_id"] = id
						} else {
							fmt.Println("This user did not complete step 2")

							id := strconv.Itoa(user_profile.Id)
							result["profile_id"] = id

							id = strconv.Itoa(user.Id)
							result["user_id"] = id

							result["redirect"] = "complete-register"
						}
					}

				}
			}

		}
	} else if err == orm.ErrMissPK {
		fmt.Println("No primary key found.")
	} else {
		//fmt.Println(user.Id, user.EmailAddress)
		password_byte := []byte(password)
		hash_byte := []byte(user.Password)
		err = bcrypt.CompareHashAndPassword(hash_byte, password_byte)
		fmt.Println(err)
		if err == nil {
			//result["status"] = "1"

			user_profile := UserProfile{FkUserId: user.Id}
			err := o.Read(&user_profile, "FkUserId")
			if err == orm.ErrNoRows {
				fmt.Println("No result found.")
			} else if err == orm.ErrMissPK {
				fmt.Println("No primary key found.")
			} else {

				if user_profile.Active != 0 {
					teamManagmentStatus, eventorganisingStatus, paddlingStatus := common.CheckUserRole(user_profile.Activities)

					result["team_manage_admin"] = teamManagmentStatus
					result["event_manage_admin"] = eventorganisingStatus
					result["paddling_admin"] = paddlingStatus
				}

				if strings.Contains(user_profile.Activities, "Super Admin") && user_profile.Active != 0 {
					fmt.Println("Super Admin")
					result["status"] = "1"
					result["first_name"] = user_profile.Name
					result["last_name"] = user_profile.LastName
					result["super_admin"] = "true"

					id := strconv.Itoa(user_profile.Id)
					result["profile_id"] = id

					id = strconv.Itoa(user.Id)
					result["user_id"] = id
					result["status"] = "1"

				} else if user_profile.Name == "" {
					fmt.Println("This user din't complete step 2")

					id := strconv.Itoa(user_profile.Id)
					result["profile_id"] = id

					id = strconv.Itoa(user.Id)
					result["user_id"] = id

					result["redirect"] = "complete-register"
				} else if user_profile.Active == 0 {
					fmt.Println("Show reset link button")
					result["notVerified"] = "true"
					id := strconv.Itoa(user.Id)
					result["user_id"] = id
				} else {
					result["first_name"] = user_profile.Name
					result["last_name"] = user_profile.LastName

					id := strconv.Itoa(user_profile.Id)
					result["profile_id"] = id

					id = strconv.Itoa(user.Id)
					result["user_id"] = id
					result["status"] = "1"
				}

			}
		}

	}

	return result

}

func IsLoggedIn(name interface{}) bool {
	//fmt.Println("Here")
	//fmt.Println(name)
	if name != nil {
		return true
	} else {
		return false
	}

}

func IsValidUser(user_id string) bool {
	result := false

	user_details := GetUserDetails(user_id)
	status := "0"

	if user_details["user"]["active"] != nil {
		status = user_details["user"]["active"].(string)
	}

	if status == "1" {
		result = true
	} else {
		result = false
	}

	return result
}

func AddUser(email string, password string, activities string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"

	email = strings.ToLower(email)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM users WHERE lower(email_address) = ? ", email).Values(&maps)
	if num == 0 {
		password_byte := []byte(password)
		hashedPassword, err := bcrypt.GenerateFromPassword(password_byte, bcrypt.DefaultCost)
		fmt.Println(string(hashedPassword))

		if err == nil {
			num, err = o.Raw("INSERT INTO users (email_address, email_verified, password) VALUES (?,?,?) RETURNING id", email, false, string(hashedPassword)).Values(&maps)
			fmt.Println(maps[0]["id"])
			result["user_id"] = maps[0]["id"].(string)

			if err == nil {
				/*num, _ := res.RowsAffected()
				fmt.Println("pg row affected nums: ", num)
				num, _ = res.LastInsertId()
				fmt.Println("pg row id: ", num)*/
				num, err = o.Raw("INSERT INTO user_profile (fk_user_id, active ,activities, instant_emails, subscribe) VALUES (?,?,?,?,?) RETURNING id", maps[0]["id"], "0", activities, "1", "1").Values(&maps)
				result["profile_id"] = maps[0]["id"].(string)

				if err == nil {
					result["status"] = "1"

					/* token := generateToken()
					expiry_date := time.Now().AddDate(0, 0, 10)

					for i := 0; i < 10; i++ {
						_, err := o.Raw("INSERT INTO user_verification_tokens (fk_user_id, address, token, expiry, "+`"when"`+")  VALUES (?,?,?,?,?) ", result["user_id"], email, token, expiry_date, time.Now()).Exec()
						if err == nil {
							result["status"] = "1"
							fmt.Println("Token Inserted")
							break
						} else {
							fmt.Println("Regenarate")
							token = generateToken()
						}
					}

					sendVerificationEmail(token, email) */
				}

			}
		}

	} else {
		fmt.Println("Email Address exists")
		result["status"] = "0"
	}

	/*if created, id, err := o.ReadOrCreate(&user, "EmailAddress"); err == nil {
		if created {
			fmt.Println("New Insert an object. Id:", id)

			user_profile := UserProfile{ FkUserId: id, Activities: activities}

			profile_id, err := o.Insert(&user_profile)

			if err == nil {
				fmt.Println("Account Created ", profile_id)
			}
			fmt.Println(user_profile)

		} else {

			fmt.Println("Get an object. Id:", id)
		}
	}*/

	return result

}

func ResendVerificationLink(user_id string) string {

	result := "false"
	fmt.Println("ResendVerificationLink")

	o := orm.NewOrm()
	var values []orm.Params

	user_details := GetUserDetails(user_id)
	email := user_details["user"]["email_address"].(string)

	token := generateToken()
	expiry_date := time.Now().AddDate(0, 0, 10)

	for i := 0; i < 10; i++ {
		num, err := o.Raw("SELECT * FROM user_verification_tokens WHERE address = ? ", email).Values(&values)

		if num > 0 {
			_, err = o.Raw("UPDATE user_verification_tokens SET fk_user_id = ?, address = ?, token = ?, expiry = ?,"+`"when"`+" = ? "+" WHERE address = ?", user_id, email, token, expiry_date, time.Now(), email).Exec()
		} else {
			_, err = o.Raw("INSERT INTO user_verification_tokens (fk_user_id, address, token, expiry, "+`"when"`+")  VALUES (?,?,?,?,?) ", user_id, email, token, expiry_date, time.Now()).Exec()

		}

		if err == nil {
			result = "true"
			fmt.Println("Token Inserted")
			break
		} else {
			fmt.Println("Regenarate")
			token = generateToken()
		}
	}

	sendVerificationEmail(token, email)

	return result

}

func CompleteUserProfile(data map[string]string, profile_id int) map[string]string {

	result := make(map[string]string)
	result["status"] = "1"
	fmt.Println("CompleteUserProfile")
	fmt.Println(data)

	// site_url := ""
	// if len(os.Getenv("SITE_URL")) > 0 {
	// 	site_url = os.Getenv("SITE_URL")
	// } else {
	// 	site_url = "http://localhost:5000/"
	// }

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	//var maps1 []orm.Params
	//var values []orm.Params

	_, err := o.Raw("UPDATE user_profile SET name = ?, last_name = ?, classes = ?, display_name = ?, dob = ?, country = ?, location = ?, postal_code = ?, province = ?,phone = ?,country_dial_code = ?,instant_sms = ?,dial_county_code=? WHERE id = ? RETURNING fk_user_id", data["FirstName"], data["LastName"], data["Classes"], data["DisplayName"], data["DOB"], data["Country"], data["Location"], data["PostalCode"], data["Province"], data["Phone"], data["CountryDialCode"], "1", data["DialCountryCode"], profile_id).Values(&maps)

	if err == nil {

		user_email := ""
		user_details := GetUserDetails(maps[0]["fk_user_id"].(string))
		user_email = user_details["user"]["email_address"].(string)
		user_email = strings.ToLower(user_email)
		mailchimDOB := ""

		t, err := time.Parse("2006-01-02T15:04:05Z07:00", user_details["user"]["dob"].(string))
		if err != nil {
			fmt.Println(err)
		} else {
			mailchimDOB = fmtdate.Format("MM/DD", t)
		}

		merge_var := map[string]interface{}{
			"FNAME": user_details["user"]["name"].(string),
			"LNAME": user_details["user"]["last_name"].(string),
			"DOB":   mailchimDOB,
			//"ADDRESS": user_details["user"]["location"].(string),
		}

		row, _ := o.Raw("SELECT * FROM subscription WHERE lower(email) = ? ", user_email).Values(&maps)

		if data["Newsletters"] == "1" {

			if row == 0 {

				chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

				user := gochimp.Email{user_email, "", ""}
				temp_data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, merge_var, "", false, false, false, true}

				email, err := chimpApi.ListsSubscribe(temp_data)

				if err == nil {
					_, err := o.Raw("INSERT INTO subscription (email, euid, leid, subscribe) VALUES (?,?,?,?) RETURNING id", email.Email, email.Euid, email.Leid, "1").Values(&maps)
					fmt.Println(err)
					result["status"] = "1"

				} else {
					//result["status"] = err.Error()
					result["status"] = "1"
					// fmt.Println("Subscription Error ", err)
				}

			} else {

				if maps[0]["subscribe"] == "0" {

					chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

					user := gochimp.Email{user_email, "", ""}
					data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, merge_var, "", false, false, false, true}

					email, err := chimpApi.ListsSubscribe(data)

					if err == nil {

						_, err = o.Raw("UPDATE subscription SET subscribe = ?, euid = ?, leid = ? WHERE lower(email) = ? ", "1", email.Euid, email.Leid, email.Email).Values(&maps)

						result["status"] = "1"

					} else {
						//result["status"] = err.Error()
						result["status"] = "1"
						// fmt.Println("Subscription Error ", err)
					}
				} else {

					_, err = o.Raw("UPDATE subscription SET subscribe = ? WHERE lower(email) = ? ", "1", user_email).Values(&maps)
				}

				result["status"] = "1"
			}

		}

	} else {
		result["error"] = err.Error()
	}

	return result

}

func GetUserDetails(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(dob, 'DD.MM.YYYY') as dob_format_report, to_char(dob, 'MM/DD/YYYY') as dob_format FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.id = ? ", id).Values(&maps)
	if num > 0 {
		for _, v := range maps {
			result["user"] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("User not found")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetAllAdmins() map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(dob, 'MM/DD/YYYY') as dob_format FROM users join user_profile on users.id= user_profile.fk_user_id WHERE activities @> ARRAY [?]::varchar[] ANd active = 1 ", "Super Admin").Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "admin_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No admins found")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func generateToken() string {
	size := 32 // change the length of the generated random string here

	dictionary := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	rb := make([]byte, size)
	_, err := rand.Read(rb)

	if err != nil {
		fmt.Println(err)
	}

	//rs := base64.URLEncoding.EncodeToString(rb)

	for k, v := range rb {
		rb[k] = dictionary[v%byte(len(dictionary))]
	}

	return string(rb)

}

func sendVerificationEmail(token string, email string) {

	//host_name := os.Getenv("HOST_NAME")
	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	verify_link := "<a href='" + site_url + "verify-email/" + token + "'>link</a>"

	content := strings.Replace(constants.VERIFY_EMAIL_CONTENT, "#verifyLink", verify_link, -1)

	footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

	footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

	html_email_message := "<html> Hi there, <br/><br/>" + content + "<a>" + site_url + "verify-email/" + token + "</a>" + footer_links + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe + "</html>"
	common.SendEmail(email, constants.VERIFY_EMAIL_SUBJECT, html_email_message)
}

func SendResetLink(email string) map[string]bool {
	o := orm.NewOrm()
	result := make(map[string]bool)

	var maps []orm.Params
	var values []orm.Params

	num, _ := o.Raw("SELECT * FROM users WHERE email_address = ? ", email).Values(&maps)

	if num > 0 {
		token := generateToken()
		for i := 0; i < 10; i++ {

			num, err := o.Raw("SELECT * FROM user_login_tokens WHERE address = ? ", email).Values(&values)
			expiry_date := time.Now().AddDate(0, 0, 10)
			if num > 0 {
				_, err = o.Raw("UPDATE user_login_tokens SET fk_user_id = ?, address = ?, token = ?, expiry = ?, "+`"when"`+" = ? "+" WHERE address = ?", maps[0]["id"].(string), email, token, expiry_date, time.Now(), email).Exec()
			} else {
				_, err = o.Raw("INSERT INTO user_login_tokens (fk_user_id, address, token, expiry, "+`"when"`+")  VALUES (?,?,?,?,?) ", maps[0]["id"].(string), email, token, expiry_date, time.Now()).Exec()

			}

			if err == nil {
				fmt.Println("Token Inserted")
				break
			} else {
				fmt.Println("Regenarate")
				token = generateToken()
			}
		}

		num, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? ", maps[0]["id"].(string)).Values(&maps)
		if num > 0 {
			//sendPasswordResetEmail(email, maps[0]["name"].(string), token)

			//host_name := os.Getenv("HOST_NAME")

			site_url := ""
			if len(os.Getenv("SITE_URL")) > 0 {
				site_url = os.Getenv("SITE_URL")
			} else {
				site_url = "http://localhost:5000/"
			}

			fmt.Printf("SITE_URL FROM MAIN.GO CONSTANTS %s \n", site_url)

			name := "there"
			if maps[0]["name"] != nil {
				name = maps[0]["name"].(string)
			}

			reset_link := "<a href='" + site_url + "reset-password/" + token + "'>here</a>"

			content := strings.Replace(constants.RESET_PASSWORD_CONTENT, "#resetLink", reset_link, -1)

			footer_links := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

			html_email_message := "<html>Hi " + name + "," + "<br/><br/>" + content + "<a>" + site_url + "reset-password/" + token + "</a>" + footer_links + constants.EMAILS_NO_REPLY_FOOTER + "</html>"

			common.SendEmail(email, constants.RESET_PASSWORD_SUBJECT, html_email_message)

			result["status"] = true
		}

	} else {
		fmt.Println("Email does not exists")
		result["status"] = false
	}
	return result
}

// func sendPasswordResetEmail (email string, name string, token string) {
// 	// body := "Hi " + name + "," + `

// 	// 		 Please reset your Gushou password here:

// 	// 		 http://gogushoutesting-env.us-west-2.elasticbeanstalk.com/reset-password/` + token

// 	mg := mailgun.NewMailgun(beego.AppConfig.String("SandboxDomain"), beego.AppConfig.String("ApiKey"), "")
// 	message := mailgun.NewMessage(
// 		constants.FROMEMAIL,
// 		constants.RESET_PASSWORD_SUBJECT,
// 		"",
// 		beego.AppConfig.String("Testing_email_recipients"))

// 	message.SetHtml("<html>Hi " + name + "," + "<br/><br/>" + constants.RESET_PASSWORD_CONTENT + constants.SITE_URL + "reset-password/" + token + constants.EMAILS_SOCIAL_FOOTER + constants.EMAILS_NO_REPLY_FOOTER + "</html>")

// 	resp, id, err := mg.Send(message)
// 	if err != nil {
// 		//log.Fatal(err)
// 		fmt.Println("ERROR : ", err)
// 	}
// 	fmt.Printf("ID: %s Resp: %s\n", id, resp)
// }

func ResetPassword(token string, password string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"

	fmt.Println("User token ", token)

	//var maps []orm.Params

	o := orm.NewOrm()
	var maps []orm.Params
	var values []orm.Params

	num, _ := o.Raw("SELECT * FROM user_login_tokens WHERE token = ? ", token).Values(&maps)
	if num > 0 {
		user_id := maps[0]["fk_user_id"].(string)
		row, _ := o.Raw("SELECT * FROM users WHERE id = ? ", user_id).Values(&values)
		if row > 0 {

			password_byte := []byte(password)
			hashedPassword, err := bcrypt.GenerateFromPassword(password_byte, bcrypt.DefaultCost)
			fmt.Println(string(hashedPassword))
			//fmt.Println("New password ", password)

			if err == nil {
				_, err := o.Raw("UPDATE users SET password = ? WHERE id = ? ", string(hashedPassword), values[0]["id"]).Values(&maps)

				if err == nil {
					result["status"] = "1"
					result["user_id"] = values[0]["id"].(string)
				}

				row, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? AND active = 1 AND name IS NOT NULL AND last_name IS NOT NULL", values[0]["id"].(string)).Values(&maps)
				if row > 0 {
					fmt.Println(maps[0]["name"])
					fmt.Println(maps[0]["activities"])
					result["profile_id"] = maps[0]["id"].(string)
					result["first_name"] = maps[0]["name"].(string)
					result["last_name"] = maps[0]["last_name"].(string)

					if maps[0]["name"] == nil {
						result["redirect"] = "complete-register"
					} else {

						if strings.Contains(maps[0]["activities"].(string), "Super Admin") {
							result["super_admin"] = "true"
						}

						teamManagmentStatus, eventorganisingStatus, paddlingStatus := common.CheckUserRole(maps[0]["activities"].(string))

						result["team_manage_admin"] = teamManagmentStatus
						result["event_manage_admin"] = eventorganisingStatus
						result["paddling_admin"] = paddlingStatus

						result["redirect"] = "dashboard"
					}
				} else {

					row, _ = o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? ", values[0]["id"].(string)).Values(&maps)

					if row > 0 {
						result["profile_id"] = maps[0]["id"].(string)
					}

					result["redirect"] = "complete-register"
				}

				if err == nil {
					_, err = o.Raw("UPDATE user_login_tokens SET expiry = ? WHERE token = ? ", time.Now(), token).Values(&maps)

					//_, err = o.Raw("UPDATE users SET email_verified = ? WHERE id = ? ", true, user_id).Values(&maps)
				}

			}

		} else {
			fmt.Println("User not found")
			result["status"] = "0"
		}

	} else {
		fmt.Println(" Token not in db ")
		result["status"] = "0"

	}

	return result
}

func EditUserDetails(data map[string]string, user_id string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	fmt.Println("EditUserDetails")
	fmt.Println(data)

	o := orm.NewOrm()
	var p *string
	weight := data["Weight"]

	if data["Weight"] == "" {
		fmt.Println("here")
		p = nil
	} else {
		fmt.Println("there")
		p = &weight
	}
	fmt.Println(&p)
	var maps []orm.Params

	_, err := o.Raw("UPDATE user_profile SET name = ?, last_name = ?, display_name = ?, dob = ?, location = ?, paddling_side = ?, skill_level = ?, status = ?, tshirt_size = ?, weight = ?, activities = ?, about = ?, classes = ?, phone = ?, instant_emails = ?, subscribe = ?, instant_sms = ?, country_dial_code = ?, dial_county_code =?  WHERE fk_user_id = ? RETURNING id", data["FirstName"], data["LastName"], data["ShowFullName"], data["DOB"], data["Location"], data["PaddlingSide"], data["SkillLevel"], data["Recruit"], data["TshirtSize"], &p, data["Activities"], data["About"], data["Classes"], data["Phone"], data["SubscribeInstant"], data["SubscribeDigest"], data["SubscribeSms"], data["CountryDialCode"], data["DialCountryCode"], user_id).Values(&maps)

	if err == nil {
		//num, err = o.Raw("INSERT INTO user_profile (fk_user_id, activities) VALUES (?,?) RETURNING id", maps[0]["id"], activities).Values(&maps)
		//result["profile_id"] = maps[0]["id"].(string)
		user_email := ""
		user_details := GetUserDetails(user_id)
		user_email = user_details["user"]["email_address"].(string)
		mailchimDOB := ""

		t, err := time.Parse("2006-01-02T15:04:05Z07:00", user_details["user"]["dob"].(string))
		if err != nil {
			fmt.Println(err)
		} else {
			mailchimDOB = fmtdate.Format("MM/DD", t)
		}

		merge_var := map[string]interface{}{
			"FNAME": user_details["user"]["name"].(string),
			"LNAME": user_details["user"]["last_name"].(string),
			"DOB":   mailchimDOB,
			//"ADDRESS": user_details["user"]["location"].(string),
		}

		row, _ := o.Raw("SELECT * FROM subscription WHERE email = ? ", user_email).Values(&maps)

		if data["Newsletters"] == "1" {

			if row == 0 {

				chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

				user := gochimp.Email{user_email, "", ""}
				data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, merge_var, "", false, false, false, true}

				email, err := chimpApi.ListsSubscribe(data)
				fmt.Println(err)
				if err == nil {
					_, _ = o.Raw("INSERT INTO subscription (email, euid, leid, subscribe) VALUES (?,?,?,?) RETURNING id", email.Email, email.Euid, email.Leid, "1").Values(&maps)

					result["status"] = "1"

				} else {
					result["status"] = "0"
					fmt.Println("Subscription Error ", err)
				}

			} else {

				if maps[0]["subscribe"] == "0" {

					chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

					user := gochimp.Email{user_email, "", ""}
					data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, merge_var, "", false, false, false, true}

					email, err := chimpApi.ListsSubscribe(data)

					if err == nil {

						_, err = o.Raw("UPDATE subscription SET subscribe = ?, euid = ?, leid = ? WHERE email = ? ", "1", email.Euid, email.Leid, email.Email).Values(&maps)

						result["status"] = "1"

					} else {
						result["status"] = "0"
						fmt.Println("Subscription Error ", err)
					}
				} else {

					_, err = o.Raw("UPDATE subscription SET subscribe = ? WHERE email = ? ", "1", user_email).Values(&maps)
				}

				result["status"] = "1"
			}

		} else {

			if row > 0 {

				chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

				user := gochimp.Email{user_email, "", ""}
				data := gochimp.ListsUnsubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, false, false, false}

				err := chimpApi.ListsUnsubscribe(data)

				if err == nil {
					_, err = o.Raw("UPDATE subscription SET subscribe = ? WHERE email = ? ", "0", user_email).Values(&maps)

					result["status"] = "1"

				} else {
					result["status"] = "0"
					fmt.Println("UnSubscription Error ", err)
				}

			} else {

				_, err = o.Raw("UPDATE subscription SET subscribe = ? WHERE email = ? ", "0", user_email).Values(&maps)
				result["status"] = "1"
			}

		}

	} else {
		result["status"] = "0"
		fmt.Println(err)
	}

	return result

}

func ChangePassword(user_id string, curr_password string, new_password string) string {

	result := "false"
	fmt.Println("ChangePassword")

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT password FROM users WHERE id = ?", user_id).Values(&maps)

	if num > 0 {

		current_hash := []byte(maps[0]["password"].(string))
		curr_password_byte := []byte(curr_password)
		err := bcrypt.CompareHashAndPassword(current_hash, curr_password_byte)
		if err == nil {

			password_byte := []byte(new_password)
			hashedPassword, err := bcrypt.GenerateFromPassword(password_byte, bcrypt.DefaultCost)

			_, err = o.Raw("UPDATE users SET password = ? WHERE id = ? ", string(hashedPassword), user_id).Values(&maps)

			if err == nil {

				result = "true"
			}

		} else {
			result = "false"
		}

	} else {
		fmt.Println("User not found")
	}

	return result

}

func GetNotification(id int, limit bool) []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()

	var maps []orm.Params
	var sql string

	if limit == true {
		sql = "SELECT *,to_char(date, 'Dy Mon DD YYYY, HH:MI AM' ) as date_1 FROM notification_list WHERE active = 1 AND fk_receiver @> ARRAY [?]::bigint[] order by date desc LIMIT 6"
	} else {
		sql = "SELECT *,to_char(date, 'Dy Mon DD YYYY, HH:MI AM' ) as date_1 FROM notification_list WHERE active = 1 AND fk_receiver @> ARRAY [?]::bigint[] order by date desc"
	}

	num, _ := o.Raw(sql, id).Values(&maps)
	if num > 0 {
		// for k, v := range maps {
		// 	key := strconv.Itoa(k)
		// 	//fmt.Println("key ", key)
		// 	//fmt.Println("valus", v)
		// 	result[key] = v
		// }
		//result["user_id"] = maps[0]["id"].(string)
		result = maps

	} else {
		fmt.Println("No notifications")
		result = append(result, nil)
	}

	//fmt.Println(result)

	return result

}

func SaveProfileUrl(user_id string, path string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("UPDATE user_profile SET files = ? WHERE fk_user_id = ? RETURNING id", url, user_id).Values(&maps)

	if err == nil {
		status = "true"
	} else {
		status = "false"
		//fmt.Println(err)
	}
	fmt.Println(status)
	return url
}

func DeleteUser(user_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params

	num, _ := o.Raw("SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE users.id = ? AND active != 9 ", user_id).Values(&values)

	if num > 0 {

		num, _ = o.Raw("SELECT * FROM event WHERE fk_organizer_id = ? AND active = 1 ", user_id).Values(&maps)

		if num > 0 {

			status = "false"

		} else {
			num, _ = o.Raw("SELECT * FROM event_organizers WHERE fk_organizer_id = ? AND active = 1 AND request_status = 2 ", user_id).Values(&maps)

			if num > 0 {

				status = "false"

			} else {
				num, _ = o.Raw("SELECT * FROM team WHERE fk_co_captains @> ARRAY [?]::bigint[] AND active = 1 ", user_id).Values(&maps)

				if num > 0 {

					status = "false"

				} else {

					new_email := values[0]["id"].(string) + "_" + time.Now().Format("20060102150405")

					new_user_email := values[0]["email_address"].(string)

					_, err := o.Raw("Delete from  card_detail WHERE fk_user_id = ?", user_id).Values(&maps)

					_, err = o.Raw("UPDATE email_digest SET receiver_email =? , active = ? WHERE fk_receiver_id = ?", new_email, "7", user_id).Values(&maps)

					// _, err = o.Raw("UPDATE event SET active = ?, email= ?,facebook_page_url =?, phone_number =?, website_url =? WHERE fk_organizer_id = ?", "9", new_email, new_email, new_email, new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE event_organizers SET active = ?, message =?, organizer_email =? WHERE fk_organizer_id = ?", "7", new_email, new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE event_register SET active = ?, reg_captain_name=?, reg_email_id=?, reg_phone_number=?, register_user_file=? WHERE reg_email_id = ?", "7", new_email, new_email, new_email, new_email, new_user_email).Values(&maps)

					_, err = o.Raw("UPDATE notification_list SET active = ? WHERE fk_receiver @> ARRAY [?]::bigint[] ", "7", user_id).Values(&maps)

					_, err = o.Raw("UPDATE teamjoin SET active = ?, email=? WHERE fk_member_id = ?", "7", new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE team_join_request SET active = ?, member_email =? WHERE fk_member_id = ? ", "7", new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE waivers SET active = ?, contact_name =?, contact_number =? WHERE fk_member_id = ? ", "7", new_email, new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE user_login_tokens SET address = ? WHERE fk_user_id = ?", new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE user_verification_tokens SET address = ? WHERE fk_user_id = ?", new_email, user_id).Values(&maps)

					_, err = o.Raw("UPDATE user_profile SET active = ?, files =?, last_name=?, name=?, phone=?, postal_code=?, country_dial_code =?, stripe_cust_id =?, default_card_id=?, dial_county_code=?, dob =? WHERE fk_user_id = ?", "9", new_email, new_email, new_email, new_email, new_email, new_email, new_email, new_email, new_email, time.Now(), user_id).Values(&maps)
					fmt.Println(err)
					_, err = o.Raw("UPDATE users SET email_address = ? WHERE id = ? RETURNING id", new_email, user_id).Values(&maps)

					if err == nil {

						status = "true"

					} else {

						status = "false"
						fmt.Println(err)

					}
				}
			}
		}

		/*activities := maps[0]["activities"].(string)

		if strings.Contains(activities, "Team Management") || strings.Contains(activities, "Event Organising") {

		} else {


		}*/

	}

	//send mail

	return status
}

func IsSubscribed(user_email string) bool {
	result := false
	o := orm.NewOrm()
	var maps []orm.Params
	row, _ := o.Raw("SELECT * FROM subscription WHERE email = ? AND subscribe = ?", user_email, "1").Values(&maps)

	if row == 0 {
		result = false
	} else {
		result = true
	}

	return result
}

func RemoveNotification(notf_id string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("UPDATE notification_list SET active = 9 WHERE id = ? ", notf_id).Values(&maps)

	if err == nil {
		status = "true"
	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func GetEmailByTeamJoinId(team_join_id string) string {
	newUserEmail := ""
	o := orm.NewOrm()
	var maps []orm.Params
	row, _ := o.Raw("SELECT email FROM teamjoin WHERE id = ?", team_join_id).Values(&maps)
	if row > 0 {
		newUserEmail = maps[0]["email"].(string)
	}
	return newUserEmail
}

func UpdateEmailForNewUser(email_id string, team_join_id string, oldEmail string) bool {
	result := false
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("UPDATE teamjoin SET email = ? WHERE id = ?", email_id, team_join_id).Values(&maps)

	_, err = o.Raw("UPDATE teamjoin SET email = ? WHERE email = ? and fk_member_id is null ", email_id, oldEmail).Values(&maps)

	if err == nil {
		result = true
	} else {
		result = false
		fmt.Println(err)
	}
	return result
}

func IsRegisteredUserByEmail(email_id string) bool {
	result := false

	user_details := GetUserDetailsByEmail(email_id)
	status := "0"

	if user_details["user"]["active"] != nil {
		status = user_details["user"]["active"].(string)
	}

	if status == "1" {
		result = true
	} else {
		result = false
	}

	return result
}

func GetUserDetailsByEmail(email string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Email id ", email)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(dob, 'MM/DD/YYYY') as dob_format FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.email_address = ? ", email).Values(&maps)
	if num > 0 {
		for _, v := range maps {
			result["user"] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("User not found")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetUserDetail(user_id string) []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params
	row, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ?", user_id).Values(&maps)
	if row > 0 {
		return maps
	}
	return maps
}

func GetUserNameList(user_id string) []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params
	row, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ?", user_id).Values(&maps)
	if row > 0 {
		return maps
	}
	return maps
}

func GetUserNameListData(member_ids []string) []string {

	var result []string
	o := orm.NewOrm()

	var maps []orm.Params

	for _, mem := range member_ids {
		_, err := strconv.Atoi(mem)

		if err == nil {
			_, _ = o.Raw("SELECT name FROM user_profile WHERE fk_user_id = ?", mem).Values(&maps)

			if maps[0]["name"] != nil {
				result = append(result, maps[0]["name"].(string))
			}
		}

	}

	return result

}

func GetUserEmailList(member_ids []string) []string {
	var result []string
	o := orm.NewOrm()

	var maps []orm.Params

	for _, mem := range member_ids {
		_, err := strconv.Atoi(mem)

		if err == nil {
			_, err := o.Raw("SELECT us.email_address FROM user_profile as up inner join users as us on us.id =up.fk_user_id  WHERE up.fk_user_id =? and up.instant_emails= ?", mem, 1).Values(&maps)
			fmt.Println(err)
			if maps != nil {
				if maps[0]["email_address"] != nil {
					result = append(result, maps[0]["email_address"].(string))
				}
			}
		}

	}

	return result

}

func GetLatestUserActivitiesDetail(userId int) (string, string, string) {
	o := orm.NewOrm()
	var values []orm.Params

	teamManagmentStatus := "false"
	eventorganisingStatus := "false"
	paddlingStatus := "false"

	num, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? ", userId).Values(&values)
	if num > 0 {
		teamManagmentStatus, eventorganisingStatus, paddlingStatus = common.CheckUserRole(values[0]["activities"].(string))
	}
	return teamManagmentStatus, eventorganisingStatus, paddlingStatus
}

func GetUserDetailsByEmailId(email string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(dob, 'MM/DD/YYYY') as dob_format FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.email_address = ? ", email).Values(&maps)
	if num > 0 {
		for _, v := range maps {
			result["user"] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("User not found")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetUserEmailByUserId(id string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := ""
	num, _ := o.Raw("SELECT * FROM users WHERE id = ? ", id).Values(&maps)
	if num > 0 {
		if maps[0]["email_address"] != nil {
			result = maps[0]["email_address"].(string)
		}
	}

	return result
}

func CheckUserEmailVerificationStatus(userid string) bool {

	o := orm.NewOrm()

	result := false

	var values []orm.Params
	num, _ := o.Raw("SELECT * FROM users WHERE id = ? ", userid).Values(&values)

	if num > 0 {
		if values[0]["email_verified"] != nil {
			result1 := values[0]["email_verified"].(string)
			if result1 == "true" {
				result = true

			}
		}
	}
	return result
}

func GetUserDetailByUserId(id string) []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params
	_, _ = o.Raw("SELECT * FROM users as us left join user_profile as up on us.id= up.fk_user_id WHERE us.id = ? ", id).Values(&maps)

	return maps
}

func GetUserNameDataByUserId(userid string) string {

	var result string
	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("SELECT name FROM user_profile WHERE fk_user_id = ?", userid).Values(&maps)

	if maps[0]["name"] != nil {
		result = maps[0]["name"].(string)
	}

	return result

}
