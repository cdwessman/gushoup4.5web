package models

import (
	"encoding/json"
	"fmt"
	"gushou/common"
	"gushou/constants"
	"os"
	"sort"
	"strconv"
	_ "strconv"
	"strings"
	"time"

	_ "github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/mattbaird/gochimp"
	"github.com/metakeule/fmtdate"
	_ "golang.org/x/crypto/bcrypt"
)

type subscription_newletter_error struct {
	Status string `json:"status"`
	Code   int    `json:"code"`
	Name   string `json:"name"`
	Err    string `json:"err"`
}

func GetAllDivisions() []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM all_divisions ORDER BY div_name asc").Values(&maps)
	if num > 0 {
		result = maps

	} else {
		fmt.Println("No Divisions")
		result[0] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventDivision(event_id string) []string {

	//result := make(map[string]orm.Params)
	//result["status"] = "0"
	var output []string

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT div FROM event_class_divisions WHERE fk_event_id = ? AND div is NOT NULL", event_id).Values(&maps)
	if num > 0 {
		for _, v := range maps {

			s := strings.Replace(v["div"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			tmp := strings.Split(s, ",")

			for _, div := range tmp {
				output = append(output, div)
			}

		}

	} else {
		fmt.Println("No Divisions")
		output = append(output, "")
	}

	//fmt.Println(result)
	output = RemoveDuplicates(output)

	sort.Strings(output)

	return output

}

func GetTeamDivision(event_id string) []string {

	//result := make(map[string]orm.Params)
	//result["status"] = "0"
	var output []string

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT div FROM team_class_divisions WHERE fk_team_id = ? AND div is NOT NULL", event_id).Values(&maps)
	if num > 0 {
		for _, v := range maps {

			s := strings.Replace(v["div"].(string), "{", "", -1)
			s = strings.Replace(s, "}", "", -1)
			tmp := strings.Split(s, ",")

			for _, div := range tmp {
				output = append(output, div)
			}

		}

	} else {
		fmt.Println("No Divisions")
		output = append(output, "")
	}

	//fmt.Println(result)
	output = RemoveDuplicates(output)

	sort.Strings(output)

	return output

}

func GetFullDivName(div string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM all_divisions WHERE value = ? ", div).Values(&maps)
	if num > 0 {

		result = maps[0]["div_name"].(string)

	} else {
		fmt.Println("No Div exists")
		result = ""
	}

	//fmt.Println(result)

	return result

}

func GetTeamSlug(id string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT slug FROM team WHERE id = ? ", id).Values(&maps)
	if num > 0 {

		result = maps[0]["slug"].(string)

	} else {
		fmt.Println("No team exists")
		result = ""
	}

	return result

}

func GetEventSlug(id string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT slug FROM event WHERE id = ? ", id).Values(&maps)
	if num > 0 {

		result = maps[0]["slug"].(string)

	} else {
		fmt.Println("No event exists")
		result = ""
	}

	return result

}

func GetTeamId(slug string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT id FROM team WHERE slug = ? ", slug).Values(&maps)
	if num > 0 {

		result = maps[0]["id"].(string)

	} else {
		fmt.Println("No team exists")
		result = ""
	}

	return result

}

func CheckTeamName(name string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT id FROM team WHERE team_name = ? AND (active = ? or active = ?)", name, 1, 7).Values(&maps)
	fmt.Print(err)
	if num > 0 {

		result = maps[0]["id"].(string)

	} else {
		fmt.Println("No team exists")
		result = ""
	}

	return result

}

func GetEventId(slug string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT id FROM event WHERE slug = ? AND active = ?", slug, 1).Values(&maps)
	if num > 0 {

		result = maps[0]["id"].(string)

	} else {
		fmt.Println("No event exists")
		result = ""
	}

	return result

}
func GetEventRegDetails(event_reg_id string) []orm.Params {

	//result["status"] = "0"
	o := orm.NewOrm()
	var maps []orm.Params

	_, _ = o.Raw("select * from event_register as er left join team as te on te.id = er.fk_team_id where er.id = ? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status ='false'", event_reg_id).Values(&maps)

	return maps

}
func IsEventEndByCurrentDate(event_id string, current_date string) string {

	//result["status"] = "0"
	o := orm.NewOrm()
	var maps []orm.Params
	result := ""

	num, err := o.Raw("select * from event where id = ? order by id desc", event_id).Values(&maps)
	fmt.Println(err)
	if num > 0 {

		var event_start_date, event_end_date string

		if maps[0]["start_date"] != nil && maps[0]["end_date"] == nil {
			event_start_date = maps[0]["start_date"].(string)
			result = common.IsFeatureDateWithoutUtc(event_start_date)
			fmt.Println(event_start_date)

		}
		if maps[0]["start_date"] != nil && maps[0]["end_date"] != nil {
			event_end_date = maps[0]["end_date"].(string)
			result = common.IsFeatureDateWithoutUtc(event_end_date)
			fmt.Println(event_end_date)
		}

	} else {
		result = "false"
	}
	return result

}

func GetTeamRosterMemberRole(roster_id string) []orm.Params {

	//result["status"] = "0"
	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("select * ,to_char(usp.dob, 'DD.MM.YYYY') as dob_format_report,case when trm.member_role Like'%steerer%' then 1 when trm.member_role Like'%drummer%' then 2 when trm.member_role Like'%paddler%' then 3 when trm.member_role Like'%substitute%' then 4 end as reportrolestatus from team_roster_member_role as trm left join users as us on trm.roster_member_id = us.id left join user_profile as usp on us.id= usp.fk_user_id WHERE trm.fk_roster_id = ? order by reportrolestatus,usp.last_name asc", roster_id).Values(&maps)
	fmt.Println(err)
	return maps

}

func GetEventName(event_id string) string {

	result := ""
	//result["status"] = "0"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("select event_name from event where id= ?", event_id).Values(&maps)
	if num > 0 {

		result = maps[0]["event_name"].(string)

	} else {
		fmt.Println("No event exists")
		result = ""
	}

	return result

}

func VerifyEmailToken(token string) map[string]string {

	result := make(map[string]string)
	result["is_verified"] = "false"

	fmt.Println("User token ", token)

	o := orm.NewOrm()
	var maps []orm.Params
	var values []orm.Params
	var userMaps []orm.Params

	num, _ := o.Raw("SELECT * FROM user_verification_tokens WHERE token = ? ", token).Values(&maps)
	if num > 0 {
		fmt.Println("Valid Token")
		fk_user_id := maps[0]["fk_user_id"].(string)
		fmt.Println(maps[0]["when"])
		layout := "2006-01-02T15:04:05Z07:00"
		str := ""

		if maps[0]["expiry"] != nil {
			str = maps[0]["expiry"].(string)
		} else {
			expiry, err := time.Parse(layout, maps[0]["when"].(string))
			if err != nil {
				fmt.Println(err)
			}
			str = expiry.AddDate(0, 0, 10).Format("2006-01-02T15:04:05Z07:00")
		}

		t, err := time.Parse(layout, str)
		if err != nil {
			fmt.Println(err)
		} else {
			if time.Now().Before(t) {
				fmt.Println("Active token")
				_, err := o.Raw("UPDATE users SET email_verified = TRUE WHERE id = ? ", maps[0]["fk_user_id"]).Values(&values)
				if err == nil {
					result["is_verified"] = "true"
				}

				_, err = o.Raw("UPDATE user_profile SET active = 1 WHERE fk_user_id = ? ", maps[0]["fk_user_id"]).Values(&values)
				if err == nil {
					result["is_active"] = "true"
				}

				CreateStripeCustomer(maps[0]["fk_user_id"].(string))
				result["user_id"] = maps[0]["fk_user_id"].(string)

				num, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? ", maps[0]["fk_user_id"]).Values(&values)
				if num > 0 {
					result["profile_id"] = values[0]["id"].(string)

					if values[0]["name"] == nil {
						result["redirect"] = "complete-register"
					} else {
						result["first_name"] = values[0]["name"].(string)
						result["last_name"] = values[0]["last_name"].(string)
						result["redirect"] = "dashboard"

						userEmail := maps[0]["address"].(string)
						Registeruserteam_event_invite(userEmail, values[0])
						teamManagmentStatus, eventorganisingStatus, paddlingStatus := common.CheckUserRole(values[0]["activities"].(string))
						result["team_manage_admin"] = teamManagmentStatus
						result["event_manage_admin"] = eventorganisingStatus
						result["paddling_admin"] = paddlingStatus

						_, err = o.Raw("UPDATE user_verification_tokens SET expiry = ? WHERE token = ? ", time.Now(), token).Values(&maps)

					}

				} else {
					result["redirect"] = "complete-register"
				}

			} else {
				num, _ := o.Raw("SELECT * FROM users us JOIN user_profile up ON us.id = up.fk_user_id JOIN user_verification_tokens uvt ON uvt.fk_user_id = up.fk_user_id WHERE up.fk_user_id = ? ", fk_user_id).Values(&userMaps)

				expiry, _ := time.Parse(layout, userMaps[0]["expiry"].(string))
				if expiry.Before(time.Now()) {
					if num > 0 {
						emailVerified := userMaps[0]["email_verified"].(string)
						if emailVerified == "true" {
							result["profile_id"] = userMaps[0]["fk_user_id"].(string)
							result["user_id"] = userMaps[0]["fk_user_id"].(string)
							result["first_name"] = userMaps[0]["name"].(string)
							result["last_name"] = userMaps[0]["last_name"].(string)
							result["is_user_active"] = userMaps[0]["active"].(string)
							result["redirect"] = "dashboard"
							result["is_verified"] = "true"
							result["expired"] = "true"
							userEmail := userMaps[0]["email_address"].(string)
							CreateStripeCustomer(userMaps[0]["fk_user_id"].(string))
							Registeruserteam_event_invite(userEmail, userMaps[0])
							teamManagmentStatus, eventorganisingStatus, paddlingStatus := common.CheckUserRole(userMaps[0]["activities"].(string))
							result["team_manage_admin"] = teamManagmentStatus
							result["event_manage_admin"] = eventorganisingStatus
							result["paddling_admin"] = paddlingStatus
						} else {
							fmt.Println("Expired Token")
							result["is_verified"] = "false"
							result["expired"] = "true"
						}
					} else {
						fmt.Println("Expired Token")
						result["is_verified"] = "false"
						result["expired"] = "true"
					}
				} else {
					fmt.Println("Expired Token")
					result["is_verified"] = "false"
					result["expired"] = "true"
				}

			}
			fmt.Println(t)
		}

	} else {
		fmt.Println("Invalid Token")
		result["is_verified"] = "false"
		result["invalid"] = "true"
	}

	return result

}

func Registeruserteam_event_invite(user_email string, user_details orm.Params) {

	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	num, _ := o.Raw("SELECT * FROM teamjoin WHERE active = 1 AND lower(email) = ? ", user_email).Values(&maps)

	if num > 0 {
		// check if the person has got team invites
		fmt.Println("This person has got team invites")
		receiver := user_details["fk_user_id"].(string)

		for _, v := range maps {

			team_id := ""
			if v["fk_team_id"] != nil {
				team_id = v["fk_team_id"].(string)
			}

			initiator := ""
			if v["fk_inviters_id"] != nil {
				initiator = v["fk_inviters_id"].(string)
			}

			message := ""
			if v["message"] != nil {
				message = v["message"].(string)
			}

			_, err := o.Raw("UPDATE teamjoin SET fk_member_id = ?, email=?, request_status = ?, accepted_date = ? WHERE lower(email) = ? AND fk_team_id = ? AND active = ?", user_details["fk_user_id"], user_email, "2", time.Now(), user_email, team_id, 1).Values(&maps1)

			if err == nil {

				_, _ = o.Raw("SELECT * FROM team WHERE id = ? ", team_id).Values(&maps1)
				team_name := maps1[0]["team_name"].(string)
				team_slug := maps1[0]["slug"].(string)

				teamUrl := "<a href='" + site_url + "team-view/" + team_slug + "'>" + team_name + "</a>"
				desc := strings.Replace(constants.TEAM_INVITE_NOTIFICATION, "#teamName", teamUrl, -1)

				receiver = "{" + receiver + "}"
				tempreceiver, _ := strconv.Atoi(user_details["fk_user_id"].(string))
				exists, err := o.Raw("SELECT * FROM notification_list WHERE type = ? AND notification_desc = ? AND fk_team_id = ? AND fk_receiver @> ARRAY [?]::bigint[] AND active = ? and fk_initiator_id=? ", "4", desc, team_id, tempreceiver, 1, initiator).Values(&maps1)
				fmt.Println(err)
				if exists == 0 && err == nil {
					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{4}", "1", time.Now(), true, desc, "4", message, team_id, nil, initiator, receiver).Values(&maps1)

					if err != nil {
						fmt.Println("Notification failed")
					}
				}
			}
		}

	}

	// check if the person has got event organizer invites

	num, _ = o.Raw("SELECT * FROM event_organizers WHERE active = 1 AND lower(organizer_email) = ? ", user_email).Values(&maps)

	if num > 0 {

		fmt.Println("This person has got event organizer invites")
		receiver := user_details["fk_user_id"].(string)

		for _, v := range maps {

			event_id := ""
			if v["fk_event_id"] != nil {
				event_id = v["fk_event_id"].(string)
			}

			initiator := ""
			if v["fk_inviters_id"] != nil {
				initiator = v["fk_inviters_id"].(string)
			}

			message := ""
			if v["message"] != nil {
				message = v["message"].(string)
			}

			_, err := o.Raw("UPDATE event_organizers SET fk_organizer_id = ? WHERE lower(organizer_email) = ? AND fk_event_id = ? AND active = ?", user_details["fk_user_id"], user_email, event_id, 1).Values(&maps1)

			if err == nil {

				_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", event_id).Values(&maps1)
				event_name := maps1[0]["event_name"].(string)
				event_slug := maps1[0]["slug"].(string)

				eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
				desc := "You have been invited to become a co-event organizer of " + eventUrl
				receiver = "{" + receiver + "}"

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{3,9}", "1", time.Now(), true, desc, "2", message, nil, event_id, initiator, receiver).Values(&maps1)

				if err != nil {
					fmt.Println("Notification failed")
				}
			}
		}

	}
}

func VerifyResetPasswordToken(token string) map[string]string {

	result := make(map[string]string)
	result["is_verified"] = "false"

	fmt.Println("User token ", token)

	//var maps []orm.Params
	//time.Now().AddDate(0,0,10)

	o := orm.NewOrm()
	var maps []orm.Params
	//var values []orm.Params

	num, _ := o.Raw("SELECT * FROM user_login_tokens WHERE token = ? ", token).Values(&maps)
	if num > 0 {
		fmt.Println("Valid Token")
		layout := "2006-01-02T15:04:05Z07:00"
		str := ""

		if maps[0]["expiry"] != nil {
			str = maps[0]["expiry"].(string)
		}
		t, err := time.Parse(layout, str)
		if err != nil {
			fmt.Println(err)
		} else {
			if time.Now().Before(t) {
				fmt.Println("Active token")

				result["user_id"] = maps[0]["fk_user_id"].(string)
				result["is_verified"] = "true"

				/*num, _ := o.Raw("SELECT * FROM user_profile WHERE fk_user_id = ? ", maps[0]["fk_user_id"]).Values(&values)
				if num > 0 {
					fmt.Println(values[0]["name"])
					result["profile_id"] = values[0]["id"].(string)
					result["first_name"] = values[0]["name"].(string)
					result["last_name"] = values[0]["last_name"].(string)

					if values[0]["name"] == nil {
						result["redirect"] = "complete-register"
					} else {
						result["redirect"] = "dashboard"
					}

				} else {
					result["redirect"] = "complete-register"
				}*/

			} else {
				fmt.Println("Expired Token")
				result["is_verified"] = "false"
				result["expired"] = "true"

			}
			fmt.Println(t)
		}

	} else {
		fmt.Println("Invalid Token")
		result["is_verified"] = "false"
		result["invalid"] = "true"
	}

	return result

}

func AddSubscription(email string) string {
	result := ""
	o := orm.NewOrm()
	var maps []orm.Params

	row, _ := o.Raw("SELECT * FROM users WHERE email_address = ? ", email).Values(&maps)

	if row > 0 {
		user_email := ""
		user_details := GetUserDetails(maps[0]["id"].(string))
		user_email = user_details["user"]["email_address"].(string)
		mailchimDOB := ""

		if user_details["user"]["dob"] != nil {
			t, err := time.Parse("2006-01-02T15:04:05Z07:00", user_details["user"]["dob"].(string))
			if err != nil {
				fmt.Println(err)
			} else {
				mailchimDOB = fmtdate.Format("MM/DD", t)
			}
		}
		merge_var := make(map[string]interface{})

		if user_details["user"]["name"] != nil {
			merge_var = map[string]interface{}{
				"FNAME": user_details["user"]["name"].(string),
				"LNAME": user_details["user"]["last_name"].(string),
				"DOB":   mailchimDOB,
			}
		}

		row, _ := o.Raw("SELECT * FROM subscription WHERE email = ? AND subscribe = 1", user_email).Values(&maps)

		if row == 0 {

			chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

			user := gochimp.Email{user_email, "", ""}
			data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, merge_var, "", false, false, false, true}

			email, err := chimpApi.ListsSubscribe(data)

			if err == nil {
				_, _ = o.Raw("INSERT INTO subscription (email, euid, leid, subscribe) VALUES (?,?,?,?) RETURNING id", email.Email, email.Euid, email.Leid, "1").Values(&maps)

				result = "true"

			} else {
				b, _ := json.Marshal(err)
				var erroPram *subscription_newletter_error
				json.Unmarshal(b, &erroPram)
				if erroPram.Name == "List_AlreadySubscribed" {
					result = "already subscribed"
				} else {
					result = erroPram.Name
				}

			}

		} else {

			result = "already subscribed"
		}

	} else {

		row, _ := o.Raw("SELECT * FROM subscription WHERE email = ? AND subscribe = 1", email).Values(&maps)

		if row == 0 {
			chimpApi := gochimp.NewChimp(os.Getenv("MAILCHIMP_API_KEY"), true)

			user := gochimp.Email{email, "", ""}
			data := gochimp.ListsSubscribe{os.Getenv("MAILCHIMP_API_KEY"), os.Getenv("MAILCHIMP_LIST_ID"), user, nil, "", false, false, false, true}

			email, err := chimpApi.ListsSubscribe(data)

			if err == nil {
				_, _ = o.Raw("INSERT INTO subscription (email, euid, leid, subscribe) VALUES (?,?,?,?) RETURNING id", email.Email, email.Euid, email.Leid, "1").Values(&maps)

				result = "true"

			} else {
				b, _ := json.Marshal(err)
				var erroPram *subscription_newletter_error
				json.Unmarshal(b, &erroPram)
				if erroPram.Name == "List_AlreadySubscribed" {
					result = "already subscribed"
				} else {
					result = erroPram.Name
				}

				fmt.Println("Subscription Error ", err)
			}
		} else {

			result = "already subscribed"
		}
	}

	return result

}

func RemoveDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true || elements[v] == "" {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func DeleteEmptyElements(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

func ReverseArray(input []orm.Params) []orm.Params {
	if len(input) == 0 {
		return input
	}
	return append(ReverseArray(input[1:]), input[0])
}

func ReverseArrayInt(numbers []int) []int {
	for i, j := 0, len(numbers)-1; i < j; i, j = i+1, j-1 {
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}

	return numbers
}

func VerifyTeamInvitationToken(token string) map[string]string {

	result := make(map[string]string)
	result["is_verified"] = "false"

	o := orm.NewOrm()
	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM user_verification_tokens WHERE token = ? ", token).Values(&maps)
	if num > 0 {
		fmt.Println("Valid Token")
		layout := "2006-01-02T15:04:05Z07:00"
		str := ""
		if maps[0]["expiry"] != nil {
			str = maps[0]["expiry"].(string)
		} else {
			expiry, err := time.Parse(layout, maps[0]["when"].(string))
			if err != nil {
				fmt.Println(err)
			}
			str = expiry.AddDate(0, 0, 10).Format("2006-01-02T15:04:05Z07:00")
		}
		t, err := time.Parse(layout, str)
		if err != nil {
			fmt.Println(err)
		} else {
			if time.Now().Before(t) {
				fmt.Println("Active token")
				result["is_verified"] = "true"
				_, err = o.Raw("UPDATE user_verification_tokens SET expiry = ? WHERE token = ? ", time.Now(), token).Values(&maps)
			} else {
				fmt.Println("Expired Token")
				result["is_verified"] = "false"
				result["expired"] = "true"
			}
			fmt.Println(t)
		}
	} else {
		fmt.Println("Invalid Token")
		result["is_verified"] = "false"
		result["invalid"] = "true"
	}
	return result

}
