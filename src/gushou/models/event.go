package models

import (
	"fmt"
	"math/big"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	_ "golang.org/x/crypto/bcrypt"

	//"gopkg.in/mailgun/mailgun-go.v1"""
	//"github.com/astaxie/beego"
	"gushou/common"
	"gushou/constants"
	"os"

	"github.com/asaskevich/govalidator"
)

func GetEvent(id int, limit bool) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params
	var sql string

	if limit == true {
		sql = "SELECT *, to_char(start_date, 'Mon DD') as s_date , to_char(end_date, 'Mon DD') as e_date FROM event WHERE active = 1 AND (event_status is null OR event_status = '') AND fk_organizer_id = ? LIMIT 6"
	} else {
		sql = "SELECT *, to_char(start_date, 'Mon DD') as s_date , to_char(end_date, 'Mon DD') as e_date FROM event WHERE active = 1 AND (event_status is null OR event_status = '') AND fk_organizer_id = ? "
	}

	num, _ := o.Raw(sql, id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)

			count, _ := o.Raw("SELECT * FROM event_logo_url WHERE fk_event_id = ? ", v["id"]).Values(&values)
			if count > 0 {
				v["logo_url"] = values[0]["path"]
			}

			num, _ := o.Raw("SELECT div FROM event_class_divisions WHERE fk_event_id = ? ", v["id"]).Values(&values)

			if num > 0 {
				div_string := ""

				for _, x := range values {
					s := strings.Replace(x["div"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)
					div_string = div_string + s + ","

				}
				var div_arr []string
				div_arr = strings.Split(div_string, ",")
				div_arr = RemoveDuplicates(div_arr)

				v["divs"] = div_arr
			}

			result[key] = v
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No events")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventPartOf(id int, limit bool) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params
	var event_map []orm.Params
	var sql string

	if limit == true {
		sql = "SELECT fk_event_id FROM event_organizers WHERE active = 1 AND request_status = 2 AND fk_organizer_id = ? LIMIT 6"
	} else {
		sql = "SELECT fk_event_id FROM event_organizers WHERE active = 1 AND request_status = 2 AND fk_organizer_id = ? "
	}

	num, _ := o.Raw(sql, id).Values(&maps)

	if num > 0 {
		for k, v := range maps {
			key := "event_part_of_" + strconv.Itoa(k)

			num, _ = o.Raw("SELECT *, to_char(start_date, 'Mon DD') as s_date , to_char(end_date, 'Mon DD') as e_date FROM event WHERE active = 1 AND (event_status is null OR event_status = '') AND id = ? ", v["fk_event_id"]).Values(&event_map)

			if num > 0 {
				count, _ := o.Raw("SELECT * FROM event_logo_url WHERE fk_event_id = ? ", v["fk_event_id"]).Values(&values)
				if count > 0 {
					event_map[0]["logo_url"] = values[0]["path"]
				}

				num, _ := o.Raw("SELECT div FROM event_class_divisions WHERE fk_event_id = ? ", v["fk_event_id"]).Values(&values)

				if num > 0 {
					div_string := ""

					for _, x := range values {
						s := strings.Replace(x["div"].(string), "{", "", -1)
						s = strings.Replace(s, "}", "", -1)
						div_string = div_string + s + ","

					}
					var div_arr []string
					div_arr = strings.Split(div_string, ",")
					div_arr = RemoveDuplicates(div_arr)

					event_map[0]["divs"] = div_arr
				}

				result[key] = event_map[0]
			}
		}
	} else {
		fmt.Println("No events part of")
		result["status"] = nil
	}

	return result

}

func GetUpcomingEvent(id string) map[string]orm.Params {

	//result := make(map[string]orm.Params)

	fmt.Println("User id ", id)

	o := orm.NewOrm()

	var maps []orm.Params
	upcoming_events := make(map[string]orm.Params)

	num, _ := o.Raw("SELECT *, to_char(start_date, 'Dy') as week_day, to_char(start_date, 'Mon DD') as month_day FROM event WHERE active = 1 AND fk_organizer_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "event_" + strconv.Itoa(k)

			layout := "2006-01-02T15:04:05Z07:00"
			str := ""
			if v["end_date"] != nil {
				str = v["end_date"].(string)
			} else {
				str = v["start_date"].(string)
			}

			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {

				if t.After(time.Now()) {
					//fmt.Println("Upcoming event ", t)
					upcoming_events[key] = v
				}

			}

			//result[key] = v
		}

	} else {
		fmt.Println("No events")
		upcoming_events["status"] = nil
	}

	if len(upcoming_events) == 0 {
		fmt.Println("No upcoming events")
		upcoming_events["status"] = nil
	}

	return upcoming_events

}

func GetAllUpcomingEvent() map[string]orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params
	upcoming_events := make(map[string]orm.Params)

	num, _ := o.Raw("SELECT *, to_char(start_date, 'Dy') as week_day, to_char(start_date, 'Mon DD') as month_day FROM event WHERE active = 1 AND COALESCE (end_date, start_date) > NOW()").Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "event_" + strconv.Itoa(k)

			/*layout := "2006-01-02T15:04:05Z07:00"
			str := ""
			if v["end_date"] != nil {
				str = v["end_date"].(string)
			} else {
				str = v["start_date"].(string)
			}

			t, err := time.Parse(layout, str)
			if err != nil {
				fmt.Println(err)
			} else {

				if t.After(time.Now()) {
					upcoming_events[key] = v
				}

			}*/
			upcoming_events[key] = v

		}

	} else {
		fmt.Println("No events")
		upcoming_events["status"] = nil
	}

	if len(upcoming_events) == 0 {
		fmt.Println("No upcoming events")
		upcoming_events["status"] = nil
	}

	return upcoming_events

}

func GetEventLogo(id string) string {

	//result := make(map[string]orm.Params)
	result := ""

	fmt.Println("Event id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT path FROM event_logo_url WHERE fk_event_id = ? ", id).Values(&maps)
	if num > 0 {
		result = maps[0]["path"].(string)

	} else {
		fmt.Println("No event Logo")
		result = ""
	}

	//fmt.Println(result)

	return result

}

/*func GetTeamPartOf(id int) (map[string]orm.Params) {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("User id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var team_map []orm.Params

	num, _ := o.Raw("SELECT team_id FROM teamjoin WHERE member_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "team_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			num, _ = o.Raw("SELECT * FROM team WHERE id = ? ", v["team_id"]).Values(&team_map)
			if num > 0 {

				result[key] = team_map[0]
			}

		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No teams")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}*/
func GetEventBySlug(slug string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Slug ", slug)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw("SELECT * ,trunc(coach_fee,0) as coach_fee_1 , to_char(start_date, 'DD Mon YYYY') as event_start_date, to_char(start_date, 'MM/DD/YYYY') as s_date_1 , to_char(start_date, 'Month DD, YYYY') as s_date_2, to_char(end_date, 'MM/DD/YYYY') as e_date_1, to_char(start_date, 'Dy, Mon DD YYYY') as s_date , to_char(end_date, 'Dy, Mon DD YYYY') as e_date, to_char(super_early_bird_date, 'MM/DD/YYYY') as seb_date , to_char(early_bird_date, 'MM/DD/YYYY') as eb_date , to_char(regular_date, 'MM/DD/YYYY') as r_date FROM event WHERE slug = ? ", slug).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "event_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			result[key] = v
		}

		num, _ := o.Raw("SELECT * FROM event_class_divisions WHERE fk_event_id = ? ", maps[0]["id"]).Values(&maps)
		if num > 0 {
			for k, v := range maps {
				key := "class_" + strconv.Itoa(k)
				//fmt.Println("key ", key)
				//fmt.Println("valus", v)
				result[key] = v
			}

		} else {
			fmt.Println("No class n divisions")
		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No events")
		result["status"] = nil
	}

	return result

}

func GetEventOrganisers(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var og []orm.Params
	var co_og []orm.Params

	num, _ := o.Raw("SELECT * FROM event WHERE id = ? ", id).Values(&og)
	fmt.Println(og[0]["id"])

	if num > 0 {

		organizer := make(map[string]orm.Params)
		if og[0]["fk_organizer_id"] != nil {
			organizer = GetUserDetails(og[0]["fk_organizer_id"].(string))
		}

		if _, ok := organizer["status"]; !ok {
			for _, v := range organizer {
				//fmt.Println("valus", v)
				result["organiser"] = v
			}

			num, _ := o.Raw("SELECT * FROM event_organizers WHERE request_status = 2 AND active = 1 AND fk_event_id = ? ", id).Values(&co_og)
			if num > 0 {

				for k, v := range co_og {
					key := "co_og" + strconv.Itoa(k)
					//fmt.Println("key ", key)
					//fmt.Println("valus", v)
					co_organizer := make(map[string]orm.Params)
					co_organizer = GetUserDetails(v["fk_organizer_id"].(string))
					if _, ok := co_organizer["status"]; !ok {
						for _, v := range co_organizer {
							//fmt.Println("valus", v)
							result[key] = v
						}
					} else {
						fmt.Println("No event co-organiser details")
					}
				}

			} else {
				fmt.Println("No event co-organiser")
			}

		} else {
			fmt.Println("No organiser details")
		}

	} else {
		fmt.Println("No event organiser")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventFaq(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM event_faqs WHERE fk_event_id = ? ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			result[key] = v
		}

	} else {
		fmt.Println("No events faqs")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventPricing(id string, caller string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()
	sql := ""

	var maps []orm.Params

	if caller == "view" {
		sql = "SELECT *, trunc(early_bird_cost,0) as eb_cost ,trunc(super_early_bird_cost,0) as seb_cost ,trunc(regular_cost,0) as r_cost FROM pricing_fee WHERE ( early_bird_cost IS NOT NULL OR super_early_bird_cost IS NOT NULL OR regular_cost IS NOT NULL ) AND( early_bird_cost != 0.00 OR super_early_bird_cost != 0.00 OR regular_cost != 0.00 ) AND status = ? AND  fk_event_id = ? order by id asc"
	} else {
		sql = "SELECT *, trunc(early_bird_cost,0) as eb_cost ,trunc(super_early_bird_cost,0) as seb_cost ,trunc(regular_cost,0) as r_cost FROM pricing_fee WHERE status = ? AND  fk_event_id = ? order by id asc "
	}

	num, _ := o.Raw(sql, "1", id).Values(&maps)

	if caller == "view" && num == 0 {
		sql = "SELECT * FROM pricing_fee WHERE status = ? AND  fk_event_id = ? order by id asc "
		num, _ = o.Raw(sql, "1", id).Values(&maps)
	}
	if num > 0 {
		for k, v := range maps {
			key := "price_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			result[key] = v
		}

	} else {
		fmt.Println("No events pricing")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventAnnouncement(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, to_char(created_at, 'Dy Mon DD YYYY HH:MI AM') as date FROM announcement WHERE fk_event_id = ? order by created_at desc ", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			key := "ancmnt_" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			result[key] = v
		}

	} else {
		fmt.Println("No events announcement")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventFreePractice(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, trunc(price,0) as price_1 FROM event_practice_pricing WHERE fk_event_id = ? AND active = 1 AND type = ? AND show = 1 ", id, "free").Values(&maps)
	if num > 0 {
		for _, v := range maps {
			key := "free"
			result[key] = v
		}

	} else {
		fmt.Println("No events free practices")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventAdditionalPractice(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, trunc(price,0) as price_1 FROM event_practice_pricing WHERE fk_event_id = ? AND active = 1 AND type = ? AND show = 1 ", id, "additional").Values(&maps)
	if num > 0 {
		for _, v := range maps {
			key := "additional"
			result[key] = v
		}

	} else {
		fmt.Println("No events additional practices")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func GetEventOtherPricing(id string) map[int]orm.Params {

	result := make(map[int]orm.Params)
	//result["status"] = "0"

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT *, trunc(cost,0) as cost_1 FROM pricing_other WHERE fk_event_id = ? order by id asc", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			result[k+1] = v
		}
	} else {
		fmt.Println("No events other pricing")
		result[1] = nil
	}

	//fmt.Println(result)

	return result

}

func CreateEvent(data map[string]string, user_id int, TempAllDivisions string, TempAllBoattype string, TempAllBoatmeter string, ItemTitle []string, ItemCost []string, EventQuestions []string, SebPrice []string, EbPrice []string, Rprice []string, SebEbRPrice []string, AllDivClsBottyMeter []string) map[string]string {

	fmt.Println(user_id)
	new_event_org_id := strconv.Itoa(user_id)
	user_details := GetUserDetails(new_event_org_id)
	name := user_details["user"]["name"].(string)
	email := user_details["user"]["email_address"].(string)
	send_instant_emails := user_details["user"]["instant_emails"]
	send_email_digest := user_details["user"]["subscribe"].(string)

	var created_event_id string

	result := make(map[string]string)
	result["status"] = "0"

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	month_val := 0
	week := 0
	year := 0

	layout := "2006-01-02 15:04:05"
	str := data["EventStartDate"]
	str = strings.TrimSpace(str)

	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {

		_, month, _ := t.Date()
		month_val = int(month) - 1
		year, week = t.ISOWeek()
	}

	//null := string(nil)
	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params
	var values []orm.Params

	//This var are declared so as to insert nil values in db if the values sent are blank
	var end_date, seb_date, eb_date, r_date *string
	end_date, seb_date, eb_date, r_date = nil, nil, nil, nil

	if data["EventEndDate"] == "" {
		end_date = nil
	} else {
		t := data["EventEndDate"]
		end_date = &t
	}

	if data["SebDate"] == "" {
		seb_date = nil
	} else {
		t := data["SebDate"]
		seb_date = &t
	}

	if data["EbDate"] == "" {
		eb_date = nil
	} else {
		t := data["EbDate"]
		eb_date = &t
	}

	if data["RDate"] == "" {
		r_date = nil
	} else {
		t := data["RDate"]
		r_date = &t
	}

	var lat *string
	var lng *string
	lat, lng = nil, nil

	if data["Latitude"] != "" {
		t := data["Latitude"]
		lat = &t
	}
	if data["Longitude"] != "" {
		t := data["Longitude"]
		lng = &t
	}

	num, err := o.Raw("INSERT INTO event ( active, classes, coach_fee, country, currency, divisions, early_bird, early_bird_date, email, end_date, event_name, facebook_page_url, faqs, latitude, location, longitude, month, phone_number, province,regular ,regular_date ,slug ,start_date ,steer_fee ,super_early_bird ,super_early_bird_date ,twitter_handle ,venue_name ,venue_street_address ,website_url ,week_number ,year, fk_organizer_id, fk_parent_event, youtube_url, boattype, eventdistance, price_based) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "1", "{M,W,MX}", data["CoachServiceCost"], data["EventCountry"], data["Currency"], "{"+TempAllDivisions+"}", data["Eb"], &eb_date, data["EventEmail"], &end_date, data["EventName"], data["EventFacebook"], "", &lat, data["EventLocation"], &lng, month_val, data["EventPh"], data["EventProvince"], data["Regular"], &r_date, data["Slug"], data["EventStartDate"], "0", data["Seb"], &seb_date, data["EventTwitter"], data["EventVenue"], data["EventStreet"], data["EventWebsite"], week, year, user_id, nil, data["EventYouTube"], "{"+TempAllBoattype+"}", "{"+TempAllBoatmeter+"}", data["PricingBased"]).Values(&maps)
	fmt.Println(err)
	//result["user_id"] = maps[0]["id"].(string)

	if num > 0 && maps != nil {
		if maps[0]["id"] != nil {
			created_event_id = maps[0]["id"].(string)
		}
	}

	if err == nil {
		error_flag := false
		for _, v := range AllDivClsBottyMeter {

			if v != "" {
				tempSplitValue := strings.Split(v, "_")
				_, err = o.Raw("INSERT INTO event_class_divisions (fk_event_id, class, div, boat_type, distance) VALUES (?,?,?,?,?)", maps[0]["id"], tempSplitValue[2], "{"+tempSplitValue[1]+"}", tempSplitValue[3], tempSplitValue[4]).Values(&values)
				fmt.Println(err)
				if err != nil {
					error_flag = true
				}
			}

		}

		if !error_flag {
			if data["PricingBased"] == "BRC" {
				for index, v := range SebPrice {
					if v != "" {
						tempSplitValue := strings.Split(SebEbRPrice[index], "_") //M_Un_10
						_, err = o.Raw("INSERT INTO pricing_fee (early_bird_cost, pricing_class, pricing_division, regular_cost,status, super_early_bird_cost,fk_event_id, boat_type) VALUES (?,?,?,?,?,?,?,?)", EbPrice[index], tempSplitValue[0], tempSplitValue[1], Rprice[index], "1", v, maps[0]["id"], tempSplitValue[2]).Values(&values)
						fmt.Println(err)
						if err != nil {
							error_flag = true
						}
					}

				}
			} else if data["PricingBased"] == "PP" {
				for index, v := range SebPrice {
					if v != "" {
						temprolearray := ""
						if index == 0 {
							temprolearray = "Competitors, Team Leaders, Coaches, Support Staff"
						} else if index == 1 {
							temprolearray = "NF Presidents, Secretary Generals, Officials"
						} else if index == 2 {
							temprolearray = "Media"
						}
						_, err = o.Raw("INSERT INTO pricing_fee (early_bird_cost, regular_cost,status, super_early_bird_cost,fk_event_id, price_fee_role) VALUES (?,?,?,?,?,?)", EbPrice[index], Rprice[index], "1", v, maps[0]["id"], "{"+temprolearray+"}").Values(&values)
						fmt.Println(err)
						if err != nil {
							error_flag = true
						}
					}

				}
			} else if data["PricingBased"] == "OT" {
				_, err = o.Raw("INSERT INTO pricing_fee (status,fk_event_id, other) VALUES (?,?,?)", "1", maps[0]["id"], data["PricingBasedOther"]).Values(&values)
				fmt.Println(err)
				if err != nil {
					error_flag = true
				}
			}
		}

		if !error_flag {

			_, err = o.Raw("INSERT INTO event_practice_pricing (active, name, quantity, price, type, show, fk_event_id) VALUES (?,?,?,?,?,?,?)", "1", "Additional Practice", "0", data["AdditionalCostPractice"], "additional", "1", maps[0]["id"]).Values(&values)

			_, err = o.Raw("INSERT INTO event_practice_pricing (active, name, quantity, price, type, show, fk_event_id) VALUES (?,?,?,?,?,?,?)", "1", "Free Practice", data["ComplFreePractice"], "0", "free", "1", maps[0]["id"]).Values(&values)

			if err == nil {

				for k, v := range ItemTitle {

					if v != "" {
						_, err = o.Raw("INSERT INTO pricing_other (active, cost, item, fk_event_id) VALUES (?,?,?,?)", "1", ItemCost[k], v, maps[0]["id"]).Values(&values)
					}

				}

				for _, v := range EventQuestions {

					if v != "" {
						_, err = o.Raw("INSERT INTO event_questionary (fk_event_id, questions, active) VALUES (?,?,?)", maps[0]["id"], v, "1").Values(&values)
					}

				}

				if err == nil {

					event_name := "undefined"
					event_date := ""
					event_location := ""
					event_slug := ""

					//send email
					_, _ = o.Raw("SELECT *,to_char(start_date, 'DD Mon YYYY') as start_date_2 FROM event WHERE id = ? ", maps[0]["id"].(string)).Values(&values)

					if values != nil {
						event_name = values[0]["event_name"].(string)
						event_date = values[0]["start_date_2"].(string)
						event_location = values[0]["location"].(string)
						event_slug = values[0]["slug"].(string)
					}

					fmt.Printf("send_instant_emails: %s \n", send_instant_emails)
					fmt.Printf("send_email_digest: %s \n", send_email_digest)

					eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"

					desc := strings.Replace(constants.EVENT_CREATED_DIGEST, "#eventName", eventUrl, -1)
					desc = strings.Replace(desc, "#eventDate", event_date, -1)
					desc = strings.Replace(desc, "#eventLocation", event_location, -1)

					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", maps[0]["id"], new_event_org_id, "{"+new_event_org_id+"}").Values(&values)

					if err != nil {
						fmt.Println("Notification failed")
					}

					if send_instant_emails == "1" {

						content := "<p>Congratulations, " + event_name + ", " + event_date + " in " + event_location + " is now on Gushou! Local and international paddlers will now have access to the information on your event page.<br><br>We will continue to build features to help ease your administrative work as an event organizer. Keep checking in and please feel free to contact us with questions or feedback.</p>"

						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
						unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

						email_html_message := "<html>Hi " + name + ", <br/><br/>" + content + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

						common.SendEmail(email, "Your Dragon Boat Event Is On Gushou!", email_html_message)
					}

					digest_html_message := strings.Replace(constants.EVENT_CREATED_DIGEST, "#eventName", event_name, -1)
					digest_html_message = strings.Replace(digest_html_message, "#eventDate", event_date, -1)
					digest_html_message = strings.Replace(digest_html_message, "#eventLocation", event_location, -1)

					if send_email_digest == "1" {
						_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "1", digest_html_message, email, time.Now(), "0", new_event_org_id).Values(&maps)

						if err == nil {
							result["status"] = "1"
						} else {
							fmt.Println("Email Digest Failed")
						}

					}

					result["status"] = "1"
					fmt.Println("Event created")

					//Notify all the superadmins

					all_admins := GetAllAdmins()

					if _, ok := all_admins["status"]; !ok {
						for _, admin := range all_admins {

							fmt.Println("ADMIN ID ", admin["fk_user_id"])

							if new_event_org_id != admin["fk_user_id"] {

								_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", created_event_id, admin["fk_user_id"], "{"+admin["fk_user_id"].(string)+"}").Values(&values)

								if err != nil {
									fmt.Println("Notification failed")
								}

								send_email_digest = admin["subscribe"].(string)
								email = admin["email_address"].(string)

								if send_email_digest == "1" {
									_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "1", digest_html_message, email, time.Now(), "0", new_event_org_id).Values(&maps)

									if err == nil {
										result["status"] = "1"
									} else {
										fmt.Println("Email Digest Failed")
									}

								}

							}

						}
					} else {
						fmt.Println("No admins found")
					}

				}

			}

		}
	}

	return result

}

func EditEvent(data map[string]string, event_id string, ItemTitle []string, ItemCost []string, Question []string, Answer []string, EventQuestions []string, ItemIdValue []string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	o := orm.NewOrm()
	var p *string
	var recuirtValue *string
	p = nil
	recuirtValue = nil

	var maps []orm.Params
	var values []orm.Params

	class_exists, err := o.Raw("SELECT * FROM event_class_divisions WHERE fk_event_id = ? AND class IS NOT NULL AND div IS NOT NULL ", event_id).Values(&maps)

	if class_exists == 0 {
		result["status"] = "2"
		return result
	}

	if len(data["EventStartDate"]) > 0 {
		data["EventStartDate"] = format_input_date(data["EventStartDate"])
	}
	if data["EventEndDate"] == "" {
		p = nil
	} else {
		t := format_input_date(data["EventEndDate"])
		p = &t
	}

	var lat *string
	var lng *string
	lat, lng = nil, nil

	if data["Latitude"] != "" {
		t := data["Latitude"]
		lat = &t
	}
	if data["Longitude"] != "" {
		t := data["Longitude"]
		lng = &t
	}

	month_val := 0
	week := 0
	year := 0

	layout := "2006-01-02 15:04:05"
	str := data["EventStartDate"]
	str = strings.TrimSpace(str)

	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {

		_, month, _ := t.Date()
		month_val = int(month) - 1
		year, week = t.ISOWeek()
	}

	tempvalue := "off"
	if data["EventRegistrationOpen"] == "" {
		recuirtValue = &tempvalue
	} else {
		tempvalue = data["EventRegistrationOpen"]
		recuirtValue = &tempvalue
	}

	_, err = o.Raw("UPDATE event SET about_cause = ?, active = ?, coach_fee = ?, country = ?, email = ?, end_date = ?, event_about = ?, event_name = ?, event_name_acronym = ?, event_status = ?, facebook_page_url = ?, latitude = ?, location = ?, longitude = ?, month = ?, phone_number = ?, province = ? ,start_date = ? ,twitter_handle = ? ,venue_name = ? ,venue_street_address = ? ,website_url = ? ,week_number = ? ,year = ?, slug = ?, youtube_url = ?, recruit = ? WHERE id = ? RETURNING id", data["EventAboutCause"], "1", data["CoachServiceCost"], data["EventCountry"], data["EventEmail"], &p, data["EventDesc"], data["EventName"], "", "", data["EventFacebook"], &lat, data["EventLocation"], &lng, month_val, data["EventPh"], data["EventProvince"], data["EventStartDate"], data["EventTwitter"], data["EventVenue"], data["EventStreet"], data["EventWebsite"], week, year, data["Slug"], data["EventYouTube"], recuirtValue, event_id).Values(&maps)

	if err == nil {
		num, _ := o.Raw("SELECT * FROM event_practice_pricing WHERE fk_event_id = ? ", event_id).Values(&values)
		if num > 0 {

			_, err = o.Raw("UPDATE event_practice_pricing SET price = ?,  quantity = ?  WHERE fk_event_id = ? AND type = ? ", data["ComplPractice"], data["ComplFreePractice"], event_id, "free").Values(&values)

		} else {

			_, err = o.Raw("INSERT INTO event_practice_pricing (active, name, quantity, price, type, show, fk_event_id) VALUES (?,?,?,?,?,?,?)", "1", "Free Practice", data["ComplFreePractice"], data["ComplFreePractice"], "free", "1", event_id).Values(&values)

		}

		if num > 0 {

			_, err = o.Raw("UPDATE event_practice_pricing SET price = ?,  quantity = ?  WHERE fk_event_id = ? AND type = ? ", data["AdditionalCostPractice"], data["AddtionalPractice"], event_id, "additional").Values(&values)

		} else {

			_, err = o.Raw("INSERT INTO event_practice_pricing (active, name, quantity, price, type, show, fk_event_id) VALUES (?,?,?,?,?,?,?)", "1", "Additional Practice", data["AddtionalPractice"], data["AdditionalCostPractice"], "additional", "1", event_id).Values(&values)

		}

		if err == nil {

			tempiteamIdValue := GetPricingOtherIteamId(event_id)

			if len(tempiteamIdValue) > 0 {
				//_, err = o.Raw("DELETE  FROM pricing_other WHERE fk_event_id = ? ", event_id).Values(&values)

				//if err == nil {

				for k, v := range ItemTitle {

					if v != "" {
						if ItemIdValue[k] == "0" {
							_, err = o.Raw("INSERT INTO pricing_other (active, cost, item, fk_event_id) VALUES (?,?,?,?)", "1", ItemCost[k], v, maps[0]["id"]).Values(&values)
						} else {
							_, err = o.Raw("UPDATE pricing_other SET cost = ?,  item = ?  WHERE id = ?", ItemCost[k], v, ItemIdValue[k]).Values(&values)
							tempiteamIdValue = common.RemoveStrinInArray(tempiteamIdValue, ItemIdValue[k])
						}

					}

				}

				for _, v1 := range tempiteamIdValue {
					_, err = o.Raw("DELETE  FROM pricing_other WHERE id = ? ", v1).Values(&values)
				}

			} else {
				for k, v := range ItemTitle {

					if v != "" {
						_, err = o.Raw("INSERT INTO pricing_other (active, cost, item, fk_event_id) VALUES (?,?,?,?)", "1", ItemCost[k], v, maps[0]["id"]).Values(&values)
					}

				}

			}

			num, _ = o.Raw("SELECT * FROM event_questionary WHERE fk_event_id = ? ", event_id).Values(&values)

			if num > 0 {
				_, err = o.Raw("DELETE  FROM event_questionary WHERE fk_event_id = ? ", event_id).Values(&values)
			}

			for _, v := range EventQuestions {

				if v != "" {
					_, err = o.Raw("INSERT INTO event_questionary (fk_event_id, questions, active) VALUES (?,?,?)", event_id, v, "1").Values(&values)
				}

			}

			num, _ = o.Raw("SELECT * FROM event_faqs WHERE fk_event_id = ? ", event_id).Values(&values)

			if num > 0 {
				_, err = o.Raw("DELETE  FROM event_faqs WHERE fk_event_id = ? ", event_id).Values(&values)

				if err == nil {
					for k, v := range Question {

						if v != "" {
							_, err = o.Raw("INSERT INTO event_faqs (active, question, answer, fk_event_id) VALUES (?,?,?,?)", "1", v, Answer[k], maps[0]["id"]).Values(&values)
						}

					}
				}

			} else {
				for k, v := range Question {

					if v != "" {
						_, err = o.Raw("INSERT INTO event_faqs (active, question, answer, fk_event_id) VALUES (?,?,?,?)", "1", v, Answer[k], maps[0]["id"]).Values(&values)
					}

				}

			}

			result["status"] = "1"
			fmt.Println("Event updated")
		}

	}

	return result

}

func EditEventClasses(data map[string]string, user_id string, event_id string, TempAllDivisionsString string, TempAllBoattypeString string, TempAllBoatmeterString string, SebPrice []string, EbPrice []string, Rprice []string, SebEbRPrice []string, AllDivClsBottyMeter []string) string {

	result := "true"

	o := orm.NewOrm()

	var maps []orm.Params
	var values []orm.Params
	error_flag := false

	var seb_date, eb_date, r_date *string
	seb_date, eb_date, r_date = nil, nil, nil
	if data["SebDate"] == "" {
		seb_date = nil
	} else {
		t := data["SebDate"]
		seb_date = &t
	}

	if data["EbDate"] == "" {
		eb_date = nil
	} else {
		t := data["EbDate"]
		eb_date = &t
	}

	if data["RDate"] == "" {
		r_date = nil
	} else {
		t := data["RDate"]
		r_date = &t
	}

	_, err := o.Raw("DELETE FROM event_class_divisions WHERE fk_event_id = ? ", event_id).Values(&maps)
	_, err = o.Raw("DELETE FROM pricing_fee WHERE fk_event_id = ? ", event_id).Values(&maps)

	if err == nil {
		for _, v := range AllDivClsBottyMeter {

			if v != "" {
				tempSplitValue := strings.Split(v, "_")
				_, err = o.Raw("INSERT INTO event_class_divisions (fk_event_id, class, div, boat_type, distance) VALUES (?,?,?,?,?)", event_id, tempSplitValue[2], "{"+tempSplitValue[1]+"}", tempSplitValue[3], tempSplitValue[4]).Values(&values)
				fmt.Println(err)
				if err != nil {
					error_flag = true
					result = "false"
				}
			}

		}

		if !error_flag {
			_, err = o.Raw("UPDATE event SET currency = ?, divisions = ?,early_bird = ?,early_bird_date = ?,regular = ?,regular_date = ?,super_early_bird = ?, super_early_bird_date = ?,boattype = ?,eventdistance = ?, price_based= ?   WHERE id = ? ", data["Currency"], "{"+TempAllDivisionsString+"}", data["Eb"], &eb_date, data["Regular"], &r_date, data["Seb"], &seb_date, "{"+TempAllBoattypeString+"}", "{"+TempAllBoatmeterString+"}", data["PricingBased"], event_id).Values(&maps)
			fmt.Println(err)
		}

		if !error_flag {
			if data["PricingBased"] == "BRC" {
				for index, v := range SebPrice {
					if v != "" {
						tempSplitValue := strings.Split(SebEbRPrice[index], "_") //M_Un_10
						_, err = o.Raw("INSERT INTO pricing_fee (early_bird_cost, pricing_class, pricing_division, regular_cost,status, super_early_bird_cost,fk_event_id, boat_type) VALUES (?,?,?,?,?,?,?,?)", EbPrice[index], tempSplitValue[0], tempSplitValue[1], Rprice[index], "1", v, event_id, tempSplitValue[2]).Values(&values)
						fmt.Println(err)
						if err != nil {
							error_flag = true
							result = "false"
						}
					}

				}
			} else if data["PricingBased"] == "PP" {
				for index, v := range SebPrice {
					if v != "" {

						temprolearray := ""
						if index == 0 {
							temprolearray = "Competitors, Team Leaders, Coaches, Support Staff"
						} else if index == 1 {
							temprolearray = "NF Presidents, Secretary Generals, Officials"
						} else if index == 2 {
							temprolearray = "Media"
						}

						_, err = o.Raw("INSERT INTO pricing_fee (early_bird_cost, regular_cost,status, super_early_bird_cost,fk_event_id, price_fee_role) VALUES (?,?,?,?,?,?)", EbPrice[index], Rprice[index], "1", v, event_id, "{"+temprolearray+"}").Values(&values)
						fmt.Println(err)
						if err != nil {
							error_flag = true
							result = "false"
						}
					}

				}
			} else if data["PricingBased"] == "OT" {
				_, err = o.Raw("INSERT INTO pricing_fee (status,fk_event_id, other) VALUES (?,?,?)", "1", event_id, data["PricingBasedOther"]).Values(&values)
				fmt.Println(err)
				if err != nil {
					error_flag = true
					result = "false"
				}
			}
		}
	} else {
		result = "false"
	}

	return result

}

func SearchEvent(data map[string]string) []orm.Params {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Search by ", data)

	sql := "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event WHERE active = 1 AND "
	sqlChanged := false

	if len(data) > 0 {
		if len(data["EventName"]) > 0 {
			sql = sql + " lower(event_name) LIKE '%" + strings.ToLower(data["EventName"]) + "%'"
			sqlChanged = true
		}

		if len(data["City"]) > 0 {
			if !sqlChanged {
				sql = sql + " lower(location) = '" + strings.ToLower(data["City"]) + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND lower(location) = '" + strings.ToLower(data["City"]) + "'"
			}

		}

		if len(data["From"]) > 0 && len(data["To"]) > 0 {
			from_date := format_input_date(data["From"])
			to_date := format_input_date(data["To"])
			if !sqlChanged {
				sql = sql + " ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
				sqlChanged = true
			} else {
				sql = sql + " AND ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
			}

		} else {
			if len(data["From"]) > 0 {
				from_date := format_input_date(data["From"])
				if !sqlChanged {
					sql = sql + " start_date = '" + from_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND start_date = '" + from_date + "'"
				}
			}

			if len(data["To"]) > 0 {
				to_date := format_input_date(data["To"])
				if !sqlChanged {
					sql = sql + " end_date = '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND end_date = '" + to_date + "'"
				}
			}
		}

		if len(data["Classes"]) > 0 {
			if data["Classes"] == "any" {
				if !sqlChanged {
					sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event WHERE active = 1"
					sqlChanged = true
				} else {
					sql = sql
				}
			}
			if data["Classes"] == "all" {
				if !sqlChanged {
					sql = sql + " classes in ('{M,W,Mx}', '{W,M,MX}', '{W,MX,M}','{MX,M,W}','{M,MX,W}','{MX,W,M}')"
					sqlChanged = true
				} else {
					sql = sql + " AND classes in ('{M,W,Mx}', '{W,M,MX}', '{W,MX,M}','{MX,M,W}','{M,MX,W}','{MX,W,M}')"
				}
			}
			if data["Classes"] == "M" {
				if !sqlChanged {
					sql = sql + " classes in ('{M}')"
					sqlChanged = true
				} else {
					sql = sql + " AND classes in ('{M}')"
				}
			}
			if data["Classes"] == "W" {
				if !sqlChanged {
					sql = sql + " classes in ('{W}')"
					sqlChanged = true
				} else {
					sql = sql + " AND classes in ('{W}')"
				}
			}
			if data["Classes"] == "MX" {
				if !sqlChanged {
					sql = sql + " classes in ('{Mx}')"
					sqlChanged = true
				} else {
					sql = sql + " AND classes in ('{Mx}')"
				}
			}

		}
	} else {

		sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 From event WHERE active = 1"
	}

	sql = sql + " ORDER BY start_date"

	fmt.Println("SQL = " + sql)
	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {
		for _, v := range maps {
			//fmt.Println(" EVENT ID ", v["id"])
			if len(data["Divisions"]) > 0 {

				if v["divisions"] != nil {
					/*s := strings.Replace(v["divisions"].(string), "{", "", -1)
					s = strings.Replace(s, "}", "", -1)*/

					s := strings.Join(GetEventDivision(v["id"].(string)), ",")

					div_array := strings.Split(s, ",")
					search_div := strings.Split(data["Divisions"], ",")
					for _, div := range search_div {

						if in_array(div, div_array) {
							//key := "event_" + strconv.Itoa(k)
							//fmt.Println("key ", key)
							//fmt.Println("valus", v)
							result = append(result, v)
							break
						}
					}
				}

			} else {
				//key := "event_" + strconv.Itoa(k)
				//fmt.Println("key ", key)
				//fmt.Println("valus", v)
				result = append(result, v)
			}

		}
		//result["user_id"] = maps[0]["id"].(string)

	} else {
		fmt.Println("No events")
		result = append(result, nil)
	}

	//fmt.Println(result)

	return result

}

func SearchUpcomingEvent(data map[string]string) ([]orm.Params, int64) {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Search by ", data)

	sql := "SELECT distinct(e.*), to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event e join event_class_divisions ec on e.id = ec.fk_event_id WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() AND"
	sqlChanged := false

	if len(data) > 0 {
		if len(data["EventName"]) > 0 {
			sql = sql + " lower(event_name) ILIKE '%" + strings.ToLower(data["EventName"]) + "%'"
			sqlChanged = true
		}

		if len(data["City"]) > 0 {
			if !sqlChanged {
				sql = sql + " lower(location) = '" + strings.ToLower(data["City"]) + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND lower(location) = '" + strings.ToLower(data["City"]) + "'"
			}

		}

		if len(data["From"]) > 0 && len(data["To"]) > 0 {
			from_date := format_input_date(data["From"])
			to_date := format_input_date(data["To"])

			to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

			if !sqlChanged {
				sql = sql + " ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
				sqlChanged = true
			} else {
				sql = sql + " AND ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
			}

		} else {
			if len(data["From"]) > 0 {
				from_date := format_input_date(data["From"])
				to_date := format_input_date(data["From"])

				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " start_date >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND start_date = '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}

			if len(data["To"]) > 0 {
				from_date := format_input_date(data["To"])
				to_date := format_input_date(data["To"])
				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}
		}

		if len(data["Divisions"]) > 0 {
			if !sqlChanged {
				sql = sql + " ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
				sqlChanged = true
			} else {
				sql = sql + " AND ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
			}
		}

		if len(data["Classes"]) > 0 {
			if data["Classes"] == "any" {
				if !sqlChanged {
					sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() "
					sqlChanged = true
				} else {
					sql = sql
				}
			}
			if data["Classes"] == "all" {
				if !sqlChanged {
					sql = sql + " ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
				}

				sql = strings.Replace(sql, "distinct(e.*)", "e.id, e.*", -1)
			}
			if data["Classes"] == "M" {
				if !sqlChanged {
					sql = sql + " ec.class = 'M'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'M'"
				}
			}
			if data["Classes"] == "W" {
				if !sqlChanged {
					sql = sql + " ec.class = 'W'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'W'"
				}
			}
			if data["Classes"] == "MX" {
				if !sqlChanged {
					sql = sql + " ec.class = 'MX'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'MX'"
				}
			}

		}

		sql = sql + " ORDER BY start_date ASC LIMIT " + data["UpcomingLimit"] + " OFFSET " + data["UpcomingOffset"]

		if data["UpcomingType"] == "last" {
			sql = strings.Replace(sql, "ASC", "DESC", -1)
			sql = strings.TrimSuffix(sql, "OFFSET "+data["UpcomingOffset"])
		}

	} else {

		sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 From event WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() ORDER BY start_date ASC LIMIT 10"
	}

	fmt.Println("SQL = " + sql)
	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {

		result = maps

	} else {
		fmt.Println("No events")
		result = append(result, nil)
	}

	//fmt.Println(result)

	sql = strings.TrimSuffix(sql, "OFFSET "+data["UpcomingOffset"])

	if len(data["UpcomingLimit"]) > 0 {
		sql = strings.Replace(sql, "LIMIT "+data["UpcomingLimit"], "", -1)
	} else {
		sql = strings.Replace(sql, "LIMIT 10", "", -1)
	}

	//fmt.Println(sql)
	num, _ = o.Raw(sql).Values(&maps)

	if data["UpcomingType"] == "last" {
		result = ReverseArray(result)
	}

	return result, num

}

//All events that have passed in last 30 days
func SearchRecentEvent(data map[string]string) ([]orm.Params, int64) {

	var result []orm.Params

	fmt.Println("Search by ", data)

	sql := "SELECT distinct(e.*), to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event e join event_class_divisions ec on e.id = ec.fk_event_id WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() - INTERVAL '30 days' AND COALESCE (end_date, start_date) < NOW() AND "
	sqlChanged := false

	if len(data) > 0 {
		if len(data["EventName"]) > 0 {
			sql = sql + " lower(event_name) ILIKE '%" + strings.ToLower(data["EventName"]) + "%'"
			sqlChanged = true
		}

		if len(data["City"]) > 0 {
			if !sqlChanged {
				sql = sql + " lower(location) = '" + strings.ToLower(data["City"]) + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND lower(location) = '" + strings.ToLower(data["City"]) + "'"
			}

		}

		if len(data["From"]) > 0 && len(data["To"]) > 0 {
			from_date := format_input_date(data["From"])
			to_date := format_input_date(data["To"])

			to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

			if !sqlChanged {
				sql = sql + " ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
				sqlChanged = true
			} else {
				sql = sql + " AND ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
			}

		} else {
			if len(data["From"]) > 0 {
				from_date := format_input_date(data["From"])
				to_date := format_input_date(data["From"])

				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " start_date >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND start_date = '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}

			if len(data["To"]) > 0 {
				from_date := format_input_date(data["To"])
				to_date := format_input_date(data["To"])
				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}
		}

		if len(data["Divisions"]) > 0 {
			if !sqlChanged {
				sql = sql + " ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
				sqlChanged = true
			} else {
				sql = sql + " AND ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
			}
		}

		if len(data["Classes"]) > 0 {
			if data["Classes"] == "any" {
				if !sqlChanged {
					sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() - INTERVAL '30 days' AND COALESCE (end_date, start_date) < NOW() "
					sqlChanged = true
				} else {
					sql = sql
				}
			}
			if data["Classes"] == "all" {
				if !sqlChanged {
					sql = sql + " ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
				}

				sql = strings.Replace(sql, "distinct(e.*)", "e.id, e.*", -1)
			}
			if data["Classes"] == "M" {
				if !sqlChanged {
					sql = sql + " ec.class = 'M'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'M'"
				}
			}
			if data["Classes"] == "W" {
				if !sqlChanged {
					sql = sql + " ec.class = 'W'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'W'"
				}
			}
			if data["Classes"] == "MX" {
				if !sqlChanged {
					sql = sql + " ec.class = 'MX'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'MX'"
				}
			}

		}

		sql = sql + " ORDER BY start_date ASC LIMIT " + data["RecentLimit"] + " OFFSET " + data["RecentOffset"]

		if data["RecentType"] == "last" {
			sql = strings.Replace(sql, "ASC", "DESC", -1)
			sql = strings.TrimSuffix(sql, "OFFSET "+data["RecentOffset"])
		}

	} else {

		sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 From event WHERE active = 1 AND COALESCE (end_date, start_date) > NOW() - INTERVAL '30 days' AND COALESCE (end_date, start_date) < NOW() ORDER BY start_date ASC LIMIT 10"
	}

	fmt.Println("SQL = " + sql)
	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {

		result = maps

	} else {
		fmt.Println("No events")
		result = append(result, nil)
	}

	//fmt.Println(result)

	sql = strings.TrimSuffix(sql, "OFFSET "+data["RecentOffset"])

	if len(data["RecentLimit"]) > 0 {
		sql = strings.Replace(sql, "LIMIT "+data["RecentLimit"], "", -1)
	} else {
		sql = strings.Replace(sql, "LIMIT 10", "", -1)
	}

	//fmt.Println(sql)
	num, _ = o.Raw(sql).Values(&maps)

	if data["RecentType"] == "last" {
		result = ReverseArray(result)
	}

	return result, num

}

//All events that have passed before the start of previous month
func SearchPastEvent(data map[string]string) ([]orm.Params, int64) {

	var result []orm.Params
	//result["status"] = "0"

	fmt.Println("Search by ", data)

	sql := "SELECT distinct(e.*), to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event e join event_class_divisions ec on e.id = ec.fk_event_id WHERE active = 1 AND COALESCE (end_date, start_date) < NOW() - INTERVAL '30 days' AND "
	sqlChanged := false

	if len(data) > 0 {
		if len(data["EventName"]) > 0 {
			sql = sql + " lower(event_name) ILIKE '%" + strings.ToLower(data["EventName"]) + "%'"
			sqlChanged = true
		}

		if len(data["City"]) > 0 {
			if !sqlChanged {
				sql = sql + " lower(location) = '" + strings.ToLower(data["City"]) + "'"
				sqlChanged = true
			} else {
				sql = sql + " AND lower(location) = '" + strings.ToLower(data["City"]) + "'"
			}

		}

		if len(data["From"]) > 0 && len(data["To"]) > 0 {
			from_date := format_input_date(data["From"])
			to_date := format_input_date(data["To"])

			to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

			if !sqlChanged {
				sql = sql + " ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
				sqlChanged = true
			} else {
				sql = sql + " AND ((start_date >= '" + from_date + "' AND end_date <= '" + to_date + "') OR (start_date >= '" + from_date + "' AND start_date <= '" + to_date + "' AND end_date IS NULL))"
			}

		} else {
			if len(data["From"]) > 0 {
				from_date := format_input_date(data["From"])
				to_date := format_input_date(data["From"])

				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " start_date >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND start_date = '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}

			if len(data["To"]) > 0 {
				from_date := format_input_date(data["To"])
				to_date := format_input_date(data["To"])
				to_date = strings.Replace(to_date, "00:00:00", "23:59:59", -1)

				if !sqlChanged {
					sql = sql + " COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
					sqlChanged = true
				} else {
					sql = sql + " AND COALESCE (end_date, start_date) >= '" + from_date + "' AND COALESCE (end_date, start_date) <= '" + to_date + "'"
				}
			}
		}

		if len(data["Divisions"]) > 0 {
			if !sqlChanged {
				sql = sql + " ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
				sqlChanged = true
			} else {
				sql = sql + " AND ec.div @> '{" + data["Divisions"] + "}' :: varchar[]"
			}
		}

		if len(data["Classes"]) > 0 {
			if data["Classes"] == "any" {
				if !sqlChanged {
					sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 FROM event WHERE active = 1 AND COALESCE (end_date, start_date) < NOW() - INTERVAL '30 days' "
					sqlChanged = true
				} else {
					sql = sql
				}
			}
			if data["Classes"] == "all" {
				if !sqlChanged {
					sql = sql + " ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class in ('W','MX','M') group by e.id having count(e.id) = 3"
				}

				sql = strings.Replace(sql, "distinct(e.*)", "e.id, e.*", -1)
			}
			if data["Classes"] == "M" {
				if !sqlChanged {
					sql = sql + " ec.class = 'M'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'M'"
				}
			}
			if data["Classes"] == "W" {
				if !sqlChanged {
					sql = sql + " ec.class = 'W'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'W'"
				}
			}
			if data["Classes"] == "MX" {
				if !sqlChanged {
					sql = sql + " ec.class = 'MX'"
					sqlChanged = true
				} else {
					sql = sql + " AND ec.class = 'MX'"
				}
			}

		}

		sql = sql + " ORDER BY start_date DESC LIMIT " + data["PastLimit"] + " OFFSET " + data["PastOffset"]

		if data["PastType"] == "last" {
			sql = strings.Replace(sql, "DESC", "ASC", -1)
			sql = strings.TrimSuffix(sql, "OFFSET "+data["PastOffset"])
		}

	} else {

		sql = "SELECT *, to_char(start_date, 'DD Mon') as start_date_1, to_char(start_date, 'DD Mon YYYY') as start_date_2, to_char(end_date, 'DD Mon YYYY') as end_date_1 From event WHERE active = 1 AND COALESCE (end_date, start_date) < NOW() - INTERVAL '30 days' ORDER BY start_date DESC LIMIT 10"
	}

	fmt.Println("SQL = " + sql)
	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {

		result = maps

	} else {
		fmt.Println("No events")
		result = append(result, nil)
	}

	//fmt.Println(result)

	sql = strings.TrimSuffix(sql, "OFFSET "+data["PastOffset"])

	if len(data["PastLimit"]) > 0 {
		sql = strings.Replace(sql, "LIMIT "+data["PastLimit"], "", -1)
	} else {
		sql = strings.Replace(sql, "LIMIT 10", "", -1)
	}

	num, _ = o.Raw(sql).Values(&maps)

	if data["PastType"] == "last" {
		result = ReverseArray(result)
	}

	return result, num

}

//Input : 06/21/2017 Output: 2017-06-21 00:00:00

func format_input_date(date string) string {

	layout := "01/02/2006"
	str := date
	output := ""
	var arr []string
	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(t.String())
		arr = strings.Split(t.String(), "+")
		output = arr[0]
	}

	return strings.TrimSpace(output)
}

func AddCoOrganizer(email_ids []string, message string, user_id string, event_id string) ([]string, []string, []string, string) {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var values []orm.Params
	var sent []string
	var alreadyInvited []string
	var alreadyMember []string

	fmt.Printf("In Add organizer ")

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	for _, email := range email_ids {

		num, _ := o.Raw("SELECT us.id, instant_emails, subscribe FROM users us inner join user_profile up on us.id = up.fk_user_id WHERE email_address = ? ", email).Values(&values)

		if num > 0 {
			organizer_id := values[0]["id"]
			send_instant_emails := values[0]["instant_emails"]
			//send_digest_emails := values[0]["subscribe"]

			num, _ := o.Raw("SELECT * FROM event WHERE active = 1 AND id = ? AND fk_organizer_id = ? ", event_id, organizer_id).Values(&maps)

			if num == 0 {
				num, _ = o.Raw("SELECT * FROM event_organizers WHERE request_status != 3 AND active = 1 AND fk_event_id = ? AND fk_organizer_id = ? ", event_id, organizer_id).Values(&maps)

				if num == 0 {
					_, err := o.Raw("INSERT INTO event_organizers (request_status, active, message, organizer_email, added_date, removed_date, fk_event_id, fk_organizer_id, fk_inviters_id ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", "1", message, email, time.Now(), time.Time{}, event_id, organizer_id, user_id).Values(&maps)

					if err == nil {

						_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", event_id).Values(&maps)

						event_name := maps[0]["event_name"].(string)
						event_slug := maps[0]["slug"].(string)

						eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
						desc := "You have been invited to become a co-event organizer of " + eventUrl

						//desc := "You have been invited to become a co-event organizer of " + event_name
						organizer_id = "{" + organizer_id.(string) + "}"

						_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, message, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "{3,9}", "1", time.Now(), true, desc, "2", message, nil, event_id, user_id, organizer_id).Values(&maps)

						if err == nil {
							status = "true"

							sent = append(sent, email)

							fmt.Printf("Add organizer email sending")
							fmt.Printf("Email = %s \n ", email)

							//send mail

							/*mg := mailgun.NewMailgun(beego.AppConfig.String("SandboxDomain"), beego.AppConfig.String("ApiKey"), "")
							email_message := mailgun.NewMessage(
								"Gushou Dragon Boat <dragonboat@gogushou.com>",
								"Do You Gushou? Yeah, You Do.",
								"",
								email)*/
							footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
							footer_unsubscribe := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

							email_html_message := "<p>Hi there,<br><br>Welcome to <a href=" + site_url + ">Gushou</a>, the pulse of the dragon boat community.<br><br>You have been invited to be an event organizer for " + event_name + " and can accept your invitation on your dashboard once you register with Gushou <a href=" + site_url + "login>here</a>.<br><br>Get ready to experience all the time-saving benefits Gushou has to offer:<br><ul style='list-style-type: none;'><li>Discover new teams and paddlers</li><li>Manage practices & schedules</li>    <li>Email your entire team</li>    <li>Streamline your dragon boat life</li>    <li>Process waivers</li></ul><br>You will receive digest emails once per day. Keep that inbox lean and clean! <br><br>Wishing you health, friendship & speed on the water!<br>The Gushou Team<br><br>Did you know \"Gushou\" means drummer in Chinese? They're the pulse of dragon boating - just like us.</p>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + footer_unsubscribe
							//email_message.SetHtml("<p>Hi there,<br><br>Welcome to <a href=http://gogushoutesting-env.us-west-2.elasticbeanstalk.com>Gushou</a>, the pulse of the dragon boat community.<br><br>You have been invited to be an event organizer for " + event_name + " and can accept your invitation on your dashboard once you register with Gushou <a href=http://gogushoutesting-env.us-west-2.elasticbeanstalk.com/login>here</a>.<br><br>Get ready to experience all the time-saving benefits Gushou has to offer:<br><ul style='list-style-type: none;'><li>Discover new teams and paddlers</li><li>Manage practices & schedules</li>    <li>Email your entire team</li>    <li>Streamline your dragon boat life</li>    <li>Process waivers</li></ul><br>You will receive digest emails once per day. Keep that inbox lean and clean! <br><br>Wishing you health, friendship & speed on the water!<br>The Gushou Team<br><br>Did you know \"Gushou\" means drummer in Chinese? They're the pulse of dragon boating - just like us.</p>")

							/*resp, id, err := mg.Send(email_message)
							if err != nil {
								fmt.Println("ERROR : ", err)
							}
							fmt.Printf("ID: %s Resp: %s\n", id, resp)*/
							fmt.Printf("\n send_instant_emails = %d", send_instant_emails)
							if send_instant_emails.(string) == "1" {
								fmt.Printf(" \n Sending add event organizer email")
								common.SendEmail(email, "Do You Gushou? Yeah, You Do.", email_html_message)
							}
							//fmt.Println(err)

						} else {
							fmt.Println("Notification failed")
						}

					}
				} else {

					if maps[0]["request_status"] != nil {
						if maps[0]["request_status"] == "1" {
							fmt.Println("The member already invited")
							status = "already invited"
							alreadyInvited = append(alreadyInvited, email)
						} else if maps[0]["request_status"] == "2" {
							fmt.Println("The member already the event organizer")
							status = "event organize"
							alreadyMember = append(alreadyMember, email)
						}
					}
				}
			} else {

				fmt.Println("The member already the event organizer")
				status = "event organizer"
				alreadyMember = append(alreadyMember, email)

			}
		} else {
			fmt.Println("The organiser invited does not exists")

			num, _ = o.Raw("SELECT * FROM event_organizers WHERE request_status != 3 AND active = 1 AND fk_event_id = ? AND organizer_email = ? ", event_id, email).Values(&maps)

			if num == 0 {
				_, err := o.Raw("INSERT INTO event_organizers (request_status, active, message, organizer_email, added_date, removed_date, fk_event_id, fk_organizer_id, fk_inviters_id ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "1", "1", message, email, time.Now(), time.Time{}, event_id, nil, user_id).Values(&maps)

				if err == nil {

					_, _ = o.Raw("SELECT event_name FROM event WHERE id = ? ", event_id).Values(&maps)

					event_name := maps[0]["event_name"].(string)

					status = "true"

					sent = append(sent, email)

					//send mail

					/*mg := mailgun.NewMailgun(beego.AppConfig.String("SandboxDomain"), beego.AppConfig.String("ApiKey"), "")
					email_message := mailgun.NewMessage(
						"Gushou Dragon Boat <dragonboat@gogushou.com>",
						"Do You Gushou? Yeah, You Do.",
						"",
						beego.AppConfig.String("Testing_email_recipients"))

					email_message.SetHtml("<p>Hi there,<br><br>Welcome to <a href=http://gogushoutesting-env.us-west-2.elasticbeanstalk.com>Gushou</a>, the pulse of the dragon boat community.<br><br>You have been invited to be an event organizer for " + event_name + " and can accept your invitation on your dashboard once you register with Gushou <a href=http://gogushoutesting-env.us-west-2.elasticbeanstalk.com/login>here</a>.<br><br>Get ready to experience all the time-saving benefits Gushou has to offer:<br><ul style='list-style-type: none;'><li>Discover new teams and paddlers</li><li>Manage practices & schedules</li>    <li>Email your entire team</li>    <li>Streamline your dragon boat life</li>    <li>Process waivers</li></ul><br>You will receive digest emails once per day. Keep that inbox lean and clean! <br><br>Wishing you health, friendship & speed on the water!<br>The Gushou Team<br><br>Did you know \"Gushou\" means drummer in Chinese? They're the pulse of dragon boating - just like us.</p>")

					resp, id, err := mg.Send(email_message)
					if err != nil {
						fmt.Println("ERROR : ", err)
					}
					fmt.Printf("ID: %s Resp: %s\n", id, resp)*/
					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

					email_html_message := "<p>Hi there,<br><br>Welcome to <a href=" + site_url + ">Gushou</a>, the pulse of the dragon boat community.<br><br>You have been invited to be an event organizer for " + event_name + " and can accept your invitation on your dashboard once you register with Gushou <a href=" + site_url + "login>here</a>.<br><br>Get ready to experience all the time-saving benefits Gushou has to offer:<br><ul style='list-style-type: none;'><li>Discover new teams and paddlers</li><li>Manage practices & schedules</li>    <li>Email your entire team</li>    <li>Streamline your dragon boat life</li>    <li>Process waivers</li></ul><br>You will receive digest emails once per day. Keep that inbox lean and clean! <br><br>Wishing you health, friendship & speed on the water!<br>The Gushou Team<br><br>Did you know \"Gushou\" means drummer in Chinese? They're the pulse of dragon boating - just like us.</p>" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER

					common.SendEmail(email, "Do You Gushou? Yeah, You Do.", email_html_message)

				}
			} else {

				if maps[0]["request_status"] != nil {
					if maps[0]["request_status"] == "1" {
						fmt.Println("The member already invited")
						status = "already invited"
						alreadyInvited = append(alreadyInvited, email)
					}
				}
			}

		}
	}

	//send mail

	return sent, alreadyInvited, alreadyMember, status
}

func CanCreateEvent(user_id int) bool {

	//user_details := GetUserDetails(user_id)
	var maps []orm.Params
	o := orm.NewOrm()

	num, _ := o.Raw("SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE users.id = ? AND activities @> ARRAY [?]::varchar[] ", user_id, "Event Organising").Values(&maps)

	if num == 1 {
		return true
	} else {
		return false
	}

}

func SaveEventUrl(event_id string, path string, fileName string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	update, err := o.Raw("UPDATE event_logo_url SET name = ?, path = ? WHERE fk_event_id = ? RETURNING id", fileName, url, event_id).Values(&maps)

	if update == 0 {

		_, err = o.Raw("INSERT INTO event_logo_url (fk_event_id, name, path) VALUES (?,?,?) RETURNING id", event_id, fileName, url).Values(&maps)
	}

	if err == nil {
		status = "true"
	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func AcceptCoOrganizer(event_id string, organizer_id string, notf_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params

	_, err := o.Raw("UPDATE event_organizers SET request_status = ? WHERE fk_event_id = ? AND fk_organizer_id = ? ", "2", event_id, organizer_id).Values(&maps)

	if err == nil {

		_, err := o.Raw("UPDATE notification_list SET active = ? WHERE id = ? ", "9", notf_id).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			fmt.Println("Notification failed")
		}

	}

	//send mail

	return status
}

func RemoveEventCoOrganizer(event_id string, eo_id string, inviters_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params

	count, err := o.Raw("SELECT fk_organizer_id FROM event WHERE id = ? ", event_id).Values(&maps)

	if count > 0 {
		if maps[0]["fk_organizer_id"] == eo_id {
			fmt.Println("Deleting main event organiser")
			_, err = o.Raw("UPDATE event SET fk_organizer_id = ? WHERE id = ? ", nil, event_id).Values(&maps)

		} else {
			fmt.Println("Deleting event co-organiser")
			_, err = o.Raw("UPDATE event_organizers SET active = ?, removed_date = ? WHERE fk_event_id = ? AND fk_organizer_id = ? ", "9", time.Now(), event_id, eo_id).Values(&maps)
		}
	}

	if err == nil {
		status = "true"
	}

	//send mail

	return status
}

func DeleteEvent(event_id string, user_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	deleted_slug := GetEventSlug(event_id)
	deleted_slug = deleted_slug + "_old_" + time.Now().Format("20060102150405")

	_, err := o.Raw("UPDATE event SET active = ?, slug = ? WHERE id = ? ", "7", deleted_slug, event_id).Values(&maps)

	if err == nil {

		_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", event_id).Values(&maps)

		event_name := maps[0]["event_name"].(string)
		event_slug := maps[0]["slug"].(string)
		organizer_id := ""
		var og_ids []string

		if maps[0]["fk_organizer_id"] != nil {
			organizer_id = maps[0]["fk_organizer_id"].(string)
		} else {
			_, _ = o.Raw("SELECT fk_organizer_id FROM event_organizers WHERE fk_event_id = ? AND request_status = 2 AND active = 1", event_id).Values(&maps)
			for _, v := range maps {

				if v["fk_organizer_id"] != nil {
					og_ids = append(og_ids, v["fk_organizer_id"].(string))
				}
			}
			organizer_id = strings.Join(og_ids[:], ",")
		}

		eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"

		desc := strings.Replace(constants.EVENT_DELETED_DIGEST, "#eventName", eventUrl, -1)

		receiver := "{" + organizer_id + "}"

		_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", event_id, user_id, receiver).Values(&maps)

		if err == nil {
			status = "true"

			receiver_details := GetUserDetails(user_id)
			receiver_email := receiver_details["user"]["email_address"].(string)
			send_email_digest := receiver_details["user"]["subscribe"].(string)

			if send_email_digest == "1" {
				_, err := o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "3", desc, receiver_email, time.Now(), "0", user_id).Values(&maps)

				if err == nil {
					status = "true"
				} else {
					fmt.Println("Email Digest Failed")
				}
			}
		}
	}

	return status
}

func AddEventAnnouncement(msg string, user_id string, event_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	_, err := o.Raw("INSERT INTO announcement (active, announcement_message, notify, created_at, fk_event_id, fk_announcer_id) VALUES (?,?,?,?,?,?)", "1", msg, "team", time.Now(), event_id, user_id).Values(&maps)

	if err == nil {
		status = "true"
	} else {
		fmt.Println("Error during insert")
	}

	//send mail

	return status
}

func SaveEventSponsorUrl(event_id string, path string, fileName string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("INSERT INTO event_sponsors (active, fk_event_id) VALUES (?,?) RETURNING id", "1", event_id).Values(&maps)

	if err == nil {
		_, err := o.Raw("INSERT INTO event_sponserers (fk_event_sponser_id, name, path) VALUES (?,?,?) RETURNING id", maps[0]["id"].(string), fileName, url).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			status = "false"
			fmt.Println(err)
		}

	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}

func GetEventSponsor(id string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	fmt.Println("Event id ", id)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM event_sponsors join event_sponserers on event_sponsors.id = event_sponserers.fk_event_sponser_id WHERE event_sponsors.active = 1 AND event_sponsors.fk_event_id = ? ", id).Values(&maps)

	if num > 0 {
		for k, v := range maps {
			key := "sponsor_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No event sponsors")
		result["status"] = nil
	}

	//fmt.Println(result)

	return result

}

func RemoveEventSponsor(data map[string][]string, event_id string, user_id string, removed_by string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	//var values []orm.Params
	code := ""

	if removed_by == "super_admin" {
		code = "7"
	} else {
		code = "9"
	}

	//_, _ = o.Raw("SELECT team_name FROM team WHERE id = ? ", team_id).Values(&maps)

	//desc := "You have been removed team " + maps[0]["team_name"].(string)

	for _, sponsor_id := range data["SponsorIds"] {
		_, err := o.Raw("UPDATE event_sponsors SET active = ? WHERE id = ? ", code, sponsor_id).Values(&maps)

		if err == nil {

			/*member_id = "{" + member_id + "}"

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_team_id, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?,?) RETURNING id","{9}", "1", time.Now(), true, desc, "9", team_id , nil, user_id, member_id ).Values(&maps)

			if err == nil {
				status = "true"
			} else {
				fmt.Println("Notification failed")
			}*/

			status = "true"

		}
	}

	//send mail

	return status
}

func Split(r rune) bool {
	return r == '-' || r == '_'
}

func RehostEvent(data map[string]string, user_id string, event_id string, caller string) map[string]string {

	result := make(map[string]string)
	result["status"] = "0"
	fmt.Println("RehostEvent")
	fmt.Println(data)

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	//null := string(nil)
	error_flag := false
	o := orm.NewOrm()

	var maps []orm.Params
	var values []orm.Params
	var old_event []orm.Params
	var old_event_div []orm.Params
	var old_event_pricing []orm.Params
	var old_event_og []orm.Params
	var event_og []string
	var old_main_og string
	var old_main_og_email string

	new_event_id := ""

	update_old_slug := ""
	latest_slug := ""
	var rh_end_date *string
	rh_end_date = nil

	if data["RhEndDate"] == "" {
		rh_end_date = nil
	} else {
		t := format_input_date(data["RhEndDate"])
		rh_end_date = &t
	}

	rh_start_date := format_input_date(data["RhStartDate"])

	month_val := 0
	week := 0
	year := 0

	layout := "01/02/2006"
	str := data["RhStartDate"]

	str = strings.TrimSpace(str)

	t, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	} else {

		_, month, _ := t.Date()
		month_val = int(month) - 1
		year, week = t.ISOWeek()
	}

	_, _ = o.Raw("SELECT * FROM event WHERE active = 1 AND id = ? ", event_id).Values(&old_event)

	old_slug_arr := strings.FieldsFunc(old_event[0]["slug"].(string), Split)
	slug_arr := old_slug_arr

	fmt.Println("OLD slug arr ", old_slug_arr)
	fmt.Println("slug arr ", slug_arr)

	for i := len(old_slug_arr) - 1; i >= 0; i-- {

		if govalidator.IsInt(old_slug_arr[i]) {
			slug_arr = append(old_slug_arr[:i], old_slug_arr[i+1:]...)
		} else {
			break
		}
	}

	latest_slug = strings.Join(slug_arr[:], "-")

	fmt.Println("latest_slug ", slug_arr)

	for _, v := range old_event {

		if v["fk_organizer_id"] != nil {
			old_main_og = v["fk_organizer_id"].(string)

			details := GetUserDetails(old_main_og)
			if details["email_address"] != nil {
				old_main_og_email = details["user"]["email_address"].(string)
			}

		}

		if caller == "claim" {
			v["fk_organizer_id"] = user_id
		} else {
			v["fk_organizer_id"] = user_id
		}

		_, err := o.Raw("INSERT INTO event (about_cause, active, classes, coach_fee, country, currency, divisions, early_bird, early_bird_date, email, end_date, event_about, event_name, event_name_acronym, event_organizer, facebook_page_url, faqs, latitude, location, longitude, month, phone_number, province,regular ,regular_date ,slug ,start_date ,steer_fee ,super_early_bird ,super_early_bird_date ,twitter_handle ,venue_name ,venue_street_address ,website_url ,week_number ,year, fk_organizer_id, fk_parent_event) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", v["about_cause"], "1", v["classes"], v["coach_fee"], v["country"], v["currency"], v["divisions"], v["early_bird"], v["early_bird_date"], v["email"], &rh_end_date, v["event_about"], v["event_name"], v["event_name_acronym"], "", v["facebook_page_url"], v["faqs"], v["latitude"], v["location"], v["longitude"], month_val, v["phone_number"], v["province"], v["regular"], v["regular_date"], latest_slug, rh_start_date, v["steer_fee"], v["super_early_bird"], v["super_early_bird_date"], v["twitter_handle"], v["venue_name"], v["venue_street_address"], v["website_url"], week, year, v["fk_organizer_id"], event_id).Values(&maps)

		if err != nil {
			error_flag = true
		} else {
			new_event_id = maps[0]["id"].(string)
		}

		// Make old main organiser as the new event co-organiser of the new rehosted event

		if old_main_og != "" {
			_, _ = o.Raw("INSERT INTO event_organizers (request_status , active, organizer_email, added_date, fk_event_id, fk_organizer_id, fk_inviters_id) VALUES (?,?,?,?,?,?,?)", "2", "1", old_main_og_email, time.Now(), maps[0]["id"], old_main_og, user_id).Values(&values)
		}

	}

	if govalidator.IsInt(old_slug_arr[len(old_slug_arr)-1]) {
		update_old_slug = old_event[0]["slug"].(string)
	} else {
		update_old_slug = old_event[0]["slug"].(string) + "_" + time.Now().Format("20060102150405")
	}

	_, _ = o.Raw("SELECT * FROM event_class_divisions WHERE fk_event_id = ? ", event_id).Values(&old_event_div)

	for _, v := range old_event_div {

		_, err := o.Raw("INSERT INTO event_class_divisions (fk_event_id, class, div) VALUES (?,?,?)", maps[0]["id"], v["class"], v["div"]).Values(&values)

		if err != nil {
			error_flag = true
		}

	}

	_, _ = o.Raw("SELECT * FROM pricing_fee WHERE fk_event_id = ? ", event_id).Values(&old_event_pricing)

	for _, v := range old_event_pricing {

		_, err := o.Raw("INSERT INTO pricing_fee (early_bird_cost, pricing_class, pricing_division, regular_cost, status, super_early_bird_cost, fk_event_id) VALUES (?,?,?,?,?,?,?)", v["early_bird_cost"], v["pricing_class"], v["pricing_division"], v["regular_cost"], v["status"], v["super_early_bird_cost"], maps[0]["id"]).Values(&values)

		if err != nil {
			error_flag = true
		}

	}

	_, _ = o.Raw("SELECT * FROM event_organizers WHERE fk_event_id = ? AND active = 1 AND request_status = 2 AND fk_organizer_id != ?", event_id, user_id).Values(&old_event_og)

	for _, v := range old_event_og {

		_, err := o.Raw("INSERT INTO event_organizers (request_status , active, organizer_email, added_date, fk_event_id, fk_organizer_id, fk_inviters_id) VALUES (?,?,?,?,?,?,?)", v["request_status"], v["active"], v["organizer_email"], time.Now(), maps[0]["id"], v["fk_organizer_id"], v["fk_inviters_id"]).Values(&values)

		if err != nil {
			error_flag = true
		}

		event_og = append(event_og, v["fk_organizer_id"].(string))

	}

	_, err = o.Raw("UPDATE event SET slug = ?, event_status = ? WHERE slug = ? AND id != ?", latest_slug+"_"+time.Now().Format("20060102150405"), "close", latest_slug, maps[0]["id"]).Values(&maps)
	if err != nil {
		error_flag = true
	}

	_, err = o.Raw("UPDATE event SET slug = ?, event_status = ? WHERE id = ? ", update_old_slug, "close", event_id).Values(&maps)
	if err != nil {
		error_flag = true
	}

	if error_flag == false {
		result["status"] = "1"

		if caller != "claim" {

			event_name := "undefined"
			event_date := ""
			event_location := ""
			event_slug := ""

			_, _ = o.Raw("SELECT *,to_char(start_date, 'DD Mon YYYY') as start_date_2 FROM event WHERE id = ? ", new_event_id).Values(&maps)

			if maps != nil {
				event_name = maps[0]["event_name"].(string)
				event_date = maps[0]["start_date_2"].(string)
				event_location = maps[0]["location"].(string)
				event_slug = maps[0]["slug"].(string)
			}

			receiver_string := "{" + strings.Join(event_og, ",") + "}"
			eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
			desc := strings.Replace(constants.REHOST_EVENT_DIGEST, "#eventName", eventUrl, -1)

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", event_id, user_id, receiver_string).Values(&maps)

			if err != nil {
				fmt.Println("Notification failed")
			}

			//send mail to all event organizers
			//var new_event_org_id string
			for _, v := range event_og {
				fmt.Printf("New event organizer 1")
				//fmt.Println(strconv.Itoa(v))
				new_event_org_id := v
				user_details := GetUserDetails(new_event_org_id)
				fmt.Println(new_event_org_id)
				email := user_details["user"]["email_address"].(string)
				name := user_details["user"]["name"].(string)
				send_instant_emails := user_details["user"]["instant_emails"].(string)
				fmt.Printf("send_instant_emails %d \n", send_instant_emails)
				/*mg := mailgun.NewMailgun(beego.AppConfig.String("SandboxDomain"), beego.AppConfig.String("ApiKey"), "")
				email_message := mailgun.NewMessage(
					constants.FROMEMAIL,
					constants.REHOST_EVENT_EMAIL_SUBJECT,
					"",
					beego.AppConfig.String("Testing_email_recipients")) */

				content := strings.Replace(constants.REHOST_EVENT_EMAIL_CONTENT, "#eventName", event_name, -1)
				content = strings.Replace(content, "#eventDate", event_date, -1)
				content = strings.Replace(content, "#eventLocation", event_location, -1)

				/*email_message.SetHtml("<html> " + content + constants.EMAILS_SOCIAL_FOOTER + constants.EMAILS_NO_REPLY_FOOTER + constants.EMAILS_UNSUBSCRIBE_FOOTER + " </html>")

				resp, id, err := mg.Send(email_message)
				if err != nil {
					//log.Fatal(err)
					fmt.Println("ERROR : ", err)
				}
				fmt.Printf("ID: %s Resp: %s\n", id, resp)*/
				if send_instant_emails == "1" {
					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)

					email_html_message := "<html>Hi " + name + ", <br/><br/>" + content + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
					common.SendEmail(email, constants.REHOST_EVENT_EMAIL_SUBJECT, email_html_message)
				}
			}

		}

		fmt.Println("Rehosted Event")
	}

	return result

}

//Get all past versions of an event

func GetEventVersion(id string) []orm.Params {

	var result []orm.Params
	//output := make(map[string]orm.Params)

	fmt.Println("Event id ", id)

	o := orm.NewOrm()
	//count := 0
	//var arr []string

	var og []orm.Params

	num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE id = ?", id).Values(&og)

	if num > 0 {
		if og[0]["fk_parent_event"] != nil {
			parent_event := og[0]["fk_parent_event"].(string)
			var all_parent []string
			var all_parent_string string
			var added bool
			fmt.Println("PARENT id ", parent_event)
			//key := "version_" + strconv.Itoa(count)
			//result[key] = og[0]

			all_parent = append(all_parent, parent_event)
			all_parent_string = parent_event

			for {
				added = false
				num, _ := o.Raw("SELECT id, fk_parent_event FROM event WHERE id IN ( " + all_parent_string + " ) OR fk_parent_event IN ( " + all_parent_string + ") ").Values(&og)
				if num > 0 {
					for _, obj := range og {

						if !in_array(obj["id"].(string), all_parent) {
							all_parent = append(all_parent, obj["id"].(string))
							all_parent_string = all_parent_string + ", " + obj["id"].(string)
							added = true
						}

						if obj["fk_parent_event"] != nil {
							if !in_array(obj["fk_parent_event"].(string), all_parent) {
								all_parent = append(all_parent, obj["fk_parent_event"].(string))
								all_parent_string = all_parent_string + ", " + obj["fk_parent_event"].(string)
								added = true
							}
						}

					}

				} else {
					break
				}

				if added == false {
					break
				}
			}

			fmt.Println("ALL PARENTS ", all_parent)
			fmt.Println("ALL PARENTS ", all_parent_string)

			for _, _ = range all_parent {
				num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE active = 1 AND id IN ( " + all_parent_string + " ) ORDER BY start_date desc").Values(&og)

				if num > 0 {
					/*for _, obj := range og {
						key := "version_" + strconv.Itoa(count)
						result[key] = obj
						count = count + 1

						fmt.Println(key, " ", obj["id"])

					}*/
					result = og
				}
			}

			/*for {
				if len(parent_event) > 0 {
					num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE id = ?", parent_event).Values(&og)

					if num > 0 {
						for _, obj := range og {
							key := "version_" + strconv.Itoa(count)
							result[key] = obj
							count = count + 1

							fmt.Println(key, " ", obj["id"])
							if obj["fk_parent_event"] != nil && parent_event != obj["fk_parent_event"] {
								parent_event = obj["fk_parent_event"].(string)
							} else {
								parent_event = ""
								//break
							}
						}
					}
				} else {
					break
				}
			}

			for _,v :=range result {

				if !in_array(v["id"].(string), arr) {
					arr = append(arr, v["id"].(string))
				}
			}
			fmt.Println("AARAY", arr)

			if len(arr) > 0 {
				for _,id := range arr {

					for k,v :=range result {

						if v["id"] == id {
							output[k] = v
							break
						}
					}

				}
			}


			result = output*/

			/*num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE fk_parent_event = ? ", parent_event).Values(&og)

			if num > 0 {
				for _, obj := range og {
					key := "version_" + strconv.Itoa(count)
					result[key] = obj
					count = count + 1
					fmt.Println("here")
					fmt.Println(key, " ", obj["id"])
				}
			}

			for {
				fmt.Println(parent_event)
				if len(parent_event) > 0 {
					num, _ = o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE id = ? ", parent_event).Values(&og)

					if num > 0 {
						for _, obj := range og {
							key := "version_" + strconv.Itoa(count)
							result[key] = obj
							count = count + 1
							fmt.Println("there")
							fmt.Println(key, " ", obj["id"], obj["fk_parent_event"])
							if obj["fk_parent_event"] != nil {
								parent_event = obj["fk_parent_event"].(string)
							} else {
								parent_event = ""
								//break
							}
						}
					}
				} else {
					break
				}

			}*/

			//fmt.Println("ALL PARENT ", all_parent)

		} else {
			/*num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE fk_parent_event = ? ", id).Values(&og)

			if num > 0 {
				if og[0]["fk_parent_event"] != nil {
					parent_event := og[0]["fk_parent_event"].(string)
					key := "version_" + strconv.Itoa(count)
					result[key] = og[0]

					for {
						fmt.Println(key)
						num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE id = ? ", parent_event).Values(&og)
						if num > 0 {
							count = count + 1
							key = "version_" + strconv.Itoa(count)
							result[key] = og[0]

							if og[0]["fk_parent_event"] != nil {
								parent_event = og[0]["fk_parent_event"].(string)

							} else {
								break
							}

						} else {
							break
						}
					}
				}
			} else {
				fmt.Println("No event found")
				result["status"] = nil
			}*/

			parent_event := og[0]["id"].(string)
			var all_parent []string
			var all_parent_string string
			fmt.Println("PARENT id ", parent_event)
			//key := "version_" + strconv.Itoa(count)
			//result[key] = og[0]

			all_parent = append(all_parent, parent_event)
			all_parent_string = parent_event

			for {
				num, _ := o.Raw("SELECT id FROM event WHERE fk_parent_event IN ( " + all_parent_string + ") ").Values(&og)
				if num > 0 && len(all_parent) <= int(num) {
					for _, obj := range og {

						if !in_array(obj["id"].(string), all_parent) {
							all_parent = append(all_parent, obj["id"].(string))
							all_parent_string = all_parent_string + ", " + obj["id"].(string)
						}

					}

				} else {
					break
				}
			}

			fmt.Println("ALL PARENTS ", all_parent)

			for _, _ = range all_parent {
				num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE active = 1 AND id IN ( " + all_parent_string + " ) ORDER BY start_date desc").Values(&og)

				if num > 0 {
					/*for _, obj := range og {
						key := "version_" + strconv.Itoa(count)
						result[key] = obj
						count = count + 1

						fmt.Println(key, " ", obj["id"])

					}*/
					result = og
				}
			}
			/*for {
				if len(id) > 0 {
					num, _ := o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE fk_parent_event = ? OR id = ?", id, id).Values(&og)

					if num > 0 {
						for _, obj := range og {
							key := "version_" + strconv.Itoa(count)
							result[key] = obj
							count = count + 1

							fmt.Println(key, " ", obj["id"])
							if obj["fk_parent_event"] != nil && id != obj["fk_parent_event"] {
								id = obj["id"].(string)
							} else {
								id = ""
								//break
							}
						}
					}
				} else {
					break
				}
			}

			for _,v :=range result {

				if !in_array(v["id"].(string), arr) {
					arr = append(arr, v["id"].(string))
				}
			}
			fmt.Println("AARAY", arr)

			if len(arr) > 0 {
				for _,id := range arr {

					for k,v :=range result {

						if v["id"] == id {
							output[k] = v
							break
						}
					}

				}
			}


			result = output*/

			/*for {
				fmt.Println(id)
				if len(id) > 0 {
					num, _ = o.Raw("SELECT *, to_char(start_date, 'Month DD, YYYY') as s_date FROM event WHERE id = ? ", id).Values(&og)

					if num > 0 {
						for _, obj := range og {
							key := "version_" + strconv.Itoa(count)
							result[key] = obj
							count = count + 1
							fmt.Println("there")
							fmt.Println(key, " ", obj["id"], obj["fk_parent_event"])
							if obj["fk_parent_event"] != nil {
								id = obj["fk_parent_event"].(string)
							} else {
								id = ""
								//break
							}
						}
					}
				} else {
					break
				}

			}*/

		}
	}

	//fmt.Println(result)

	return result

}

func ClaimEventRequest(user_id string, event_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var og []orm.Params
	var exists []orm.Params

	var organizer_array []string

	new_organizer_id := user_id

	num, _ := o.Raw("SELECT fk_organizer_id FROM event WHERE id = ? AND fk_organizer_id is not null", event_id).Values(&og)

	if num > 0 {
		organizer_id := og[0]["fk_organizer_id"].(string)
		organizer_array = append(organizer_array, organizer_id)
	} else {
		num, _ := o.Raw("SELECT eo.fk_organizer_id FROM event e join event_organizers eo on e.id = eo.fk_event_id WHERE e.id = ? AND eo.request_status = ? ", event_id, 2).Values(&og)

		if num > 0 {
			for _, v := range og {
				organizer_array = append(organizer_array, v["fk_organizer_id"].(string))
			}
		}

	}

	if len(organizer_array) > 0 {

		num, _ := o.Raw("SELECT status FROM claim_event WHERE fk_event_id = ? AND fk_organizer_id = ? ", event_id, new_organizer_id).Values(&exists)

		if num == 0 {
			for _, v := range organizer_array {
				organizer_id := v
				_, err := o.Raw("INSERT INTO claim_event (requested_date, status, fk_event_id, fk_organizer_id) VALUES (?,?,?,?) RETURNING id", time.Now(), "1", event_id, new_organizer_id).Values(&maps)

				if err == nil {

					_, _ = o.Raw("SELECT event_name FROM event WHERE id = ? ", event_id).Values(&maps)
					event_name := maps[0]["event_name"].(string)

					new_organizer_details := GetUserDetails(new_organizer_id)
					new_organizer_name := new_organizer_details["user"]["name"].(string) + " " + new_organizer_details["user"]["last_name"].(string)
					receiver_email := new_organizer_details["user"]["email_address"].(string)
					send_email_digest := new_organizer_details["user"]["subscribe"].(string)

					desc := "This is a request from " + new_organizer_name + " for administration access to " + event_name + ". Please accept or decline the request below."

					receiver := "{" + organizer_id + "}"

					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{3,9}", "1", time.Now(), true, desc, "6", event_id, new_organizer_id, receiver).Values(&maps)

					if err == nil {

						status = "true"

						content := strings.Replace(constants.CLAIM_EVENT, "#eventOrganizer", new_organizer_name, -1)
						content = strings.Replace(content, "#eventName", event_name, -1)

						if send_email_digest == "1" {
							_, err := o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "21", content, receiver_email, time.Now(), "0", organizer_id).Values(&maps)

							if err == nil {
								status = "true"
							} else {
								fmt.Println("Email Digest Failed")
							}
						}

					} else {
						fmt.Println("Notification failed")
					}

				}
			}
		} else {

			if exists[0]["status"].(string) == "1" {
				fmt.Println("Your request is pending with event organizer")
				status = "pending"
			} else if exists[0]["status"].(string) == "3" {
				fmt.Println("Your request is declined by the event organizer")
				status = "declined"
			}

		}

	} else {
		fmt.Println("No main organizer for the event")
		status = "false"
	}

	//send mail

	return status
}

func AcceptClaimEventRequest(user_id string, event_id string, notf_id string, initiator_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	_, err := o.Raw("UPDATE claim_event SET status = ? WHERE fk_event_id = ? AND fk_organizer_id = ? ", "2", event_id, initiator_id).Values(&maps)

	if err == nil {

		_, err := o.Raw("UPDATE notification_list SET active = ? WHERE id = ? ", "9", notf_id).Values(&maps)

		if err == nil {
			status = "true"
			event_name := "undefined"
			//event_date := ""
			//event_location := ""

			_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", event_id).Values(&maps)

			if maps != nil {
				event_name = maps[0]["event_name"].(string)
				//event_date = time.Now().Format("_2 Jan 2006")
				//event_location = maps[0]["location"].(string)
			}

			user_details := GetUserDetails(user_id)
			receiver_email := user_details["user"]["email_address"].(string)
			send_email_digest := user_details["user"]["subscribe"].(string)

			desc := strings.Replace(constants.CLAIM_ACCEPTED_NOTIFICATION, "#eventName", event_name, -1)

			receiver := "{" + initiator_id + "}"

			_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", event_id, user_id, receiver).Values(&maps)

			content := strings.Replace(constants.CLAIM_ACCEPTED_DIGEST, "#eventName", event_name, -1)

			if send_email_digest == "1" {
				_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "15", content, receiver_email, time.Now(), "0", initiator_id).Values(&maps)

				if err == nil {
					status = "true"
				} else {
					fmt.Println("Email Digest Failed")
				}
			}

		} else {
			fmt.Println("Notification failed")
		}

	}

	//Rehost the event
	data := make(map[string]string)
	data["RhStartDate"] = time.Now().Format("01/02/2006")
	data["RhEndDate"] = time.Now().Format("01/02/2006")

	rehost_result := make(map[string]string)
	rehost_result = RehostEvent(data, initiator_id, event_id, "claim")

	if rehost_result["status"] == "1" {
		status = "true"
	}
	//send mail

	return status
}

func SendVolunteerRequest(msg string, user_id string, event_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params
	var organizer_array []string

	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", event_id).Values(&maps)

	event_name := maps[0]["event_name"].(string)
	event_slug := maps[0]["slug"].(string)
	eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"

	user_details := GetUserDetails(user_id)
	user_name := user_details["user"]["name"].(string)
	user_email := user_details["user"]["email_address"].(string)
	receiver_email := ""
	receiver_id := ""
	//send_email_digest := ""

	if maps[0]["fk_organizer_id"] != nil {
		organizer_id := maps[0]["fk_organizer_id"].(string)
		organizer_array = append(organizer_array, organizer_id)
	} else {
		num, _ := o.Raw("SELECT eo.fk_organizer_id FROM event e join event_organizers eo on e.id = eo.fk_event_id WHERE e.id = ? AND eo.request_status = ? ", event_id, 2).Values(&maps)

		if num > 0 {
			for _, v := range maps {
				organizer_array = append(organizer_array, v["fk_organizer_id"].(string))
			}
		}

	}

	for _, org_id := range organizer_array {
		receiver_id = org_id
		receiver_details := GetUserDetails(receiver_id)
		receiver_email = receiver_details["user"]["email_address"].(string)
		send_email_digest := receiver_details["user"]["subscribe"].(string)

		content := strings.Replace(constants.EVENT_VOLUNTEER_REQUEST, "#userName", user_name, -1)
		content = strings.Replace(content, "#userEmail", user_email, -1)
		content = strings.Replace(content, "#eventName", eventUrl, -1)
		content = strings.Replace(content, "#message", msg, -1)

		if send_email_digest == "1" {

			exists, err := o.Raw("SELECT id FROM email_digest WHERE type = ? AND message = ? AND receiver_email = ? AND fk_receiver_id = ? LIMIT 1 ", "18", content, receiver_email, receiver_id).Values(&maps)

			if exists == 0 {
				_, err = o.Raw("INSERT INTO email_digest (type, message, receiver_email, inserted_date, active, fk_receiver_id) VALUES (?,?,?,?,?,?) RETURNING id", "18", content, receiver_email, time.Now(), "0", receiver_id).Values(&maps)
			} else {
				row_id := maps[0]["id"]
				_, err = o.Raw("UPDATE email_digest SET inserted_date = ?, active = ? WHERE id = ?", time.Now(), "0", row_id).Values(&maps)
			}

			if err == nil {
				status = "true"
			} else {
				fmt.Println("Email Digest Failed")
			}
		}
	}

	desc := strings.Replace(constants.EVENT_VOLUNTEER_NOTIFICATION, "#eventName", eventUrl, -1)
	desc = strings.Replace(desc, "#message", msg, -1)

	exists, err := o.Raw("SELECT id from notification_list WHERE type = ? AND fk_event_id = ? AND notification_desc LIKE '%sent a volunteer request%' LIMIT 1 ", 9, event_id).Values(&maps)

	fmt.Println(exists, err)

	if exists == 0 {
		_, err = o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", event_id, user_id, "{"+user_id+"}").Values(&maps)
	} else {

		row_id := maps[0]["id"]
		_, err = o.Raw("UPDATE notification_list  SET active = ?, date = ?, notification_desc = ?, fk_initiator_id = ?, fk_receiver = ? WHERE id = ?", "1", time.Now(), desc, user_id, "{"+user_id+"}", row_id).Values(&maps)
	}

	if err == nil {
		status = "true"
	} else {
		fmt.Println("Notification Failed")
	}

	return status
}

func DeclineEventOrg(event_id string, member_id string, notf_id string) string {
	o := orm.NewOrm()
	status := "false"
	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM event_organizers WHERE request_status = 1 AND fk_event_id = ? AND fk_organizer_id = ? ", event_id, member_id).Values(&maps)

	if num > 0 {
		_, err := o.Raw("UPDATE event_organizers SET request_status = ? WHERE id = ? ", "3", maps[0]["id"]).Values(&maps)

		_, err = o.Raw("UPDATE notification_list SET active = ? WHERE id = ? ", "9", notf_id).Values(&maps)

		if err == nil {
			status = "true"
		} else {
			fmt.Println("Notification failed")
		}
	} else {

		fmt.Println("Invalid invitation")
		status = "invalid"
	}

	return status
}

func DeclineClaimRequest(org_id string, event_id string, notf_id string) string {

	result := "false"

	o := orm.NewOrm()

	var values []orm.Params

	_, err := o.Raw("UPDATE claim_event SET status = ? WHERE fk_event_id = ? AND fk_organizer_id = ?", "3", event_id, org_id).Values(&values)

	_, err = o.Raw("UPDATE notification_list SET active = ? WHERE id = ? ", "9", notf_id).Values(&values)

	if err == nil {
		result = "true"
	}

	return result

}

func CanRehost(event_id string) bool {
	data := GetEventVersion(event_id)
	result := true

	for _, obj := range data {
		//for _,v := range obj {
		layout := "2006-01-02T15:04:05Z07:00"
		str := ""
		if obj["end_date"] != nil {
			str = obj["end_date"].(string)
		} else {
			str = obj["start_date"].(string)
		}

		t, err := time.Parse(layout, str)
		if err != nil {
			fmt.Println(err)
		} else {
			if t.After(time.Now()) {
				fmt.Println("Cannot rehost")
				result = false
				break
			}

		}
		//}
	}
	return result
}

func RegisterTeamEvent(data map[string]string, eventId string, SelectedDivisionArray []string, SelectedClassArray []string, SelectedRosterIdArray []string, OnlySelectedClassArray []string, OnlySelectedDivisionArray []string, SelectedBoatTypeArray []string, OnlySelectedDistanceArray []string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	result := "false"

	registrationStatus := "Pending"

	_, err := o.Raw("INSERT INTO event_register (fk_team_id, fk_event_id, created_at, updated_at, active, registration_status, roster_submit_status) VALUES (?,?,?,?,?,?,?) RETURNING id", data["EventRegisterTeamid"], eventId, time.Now(), time.Now(), 1, registrationStatus, data["SubmitRosterLater"]).Values(&maps)
	fmt.Println(err)
	if maps[0]["id"] != nil {
		result = "true"

		if result == "true" {
			for index, RosterIdValue := range SelectedRosterIdArray {
				if RosterIdValue != "" {
					tempSelectedDivisionArray := strings.Split(SelectedDivisionArray[index], "_")
					if len(tempSelectedDivisionArray) > 1 {
						_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", maps[0]["id"], RosterIdValue, tempSelectedDivisionArray[0], SelectedClassArray[index], tempSelectedDivisionArray[1], SelectedBoatTypeArray[index]).Values(&maps1)
						fmt.Println(err)
					} else {
						_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", maps[0]["id"], RosterIdValue, tempSelectedDivisionArray[0], SelectedClassArray[index], "0", "0").Values(&maps1)
						fmt.Println(err)
					}
					if err != nil {
						result = "false"
					}
				}
			}

			for index, SelectedClassValue := range OnlySelectedClassArray {
				if SelectedClassValue != "" {
					tempOnlySelectedDivisionArray := strings.Split(OnlySelectedDivisionArray[index], "_")
					if len(tempOnlySelectedDivisionArray) > 1 {
						_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class, boat_type, distance ) VALUES (?,?,?,?,?)", maps[0]["id"], tempOnlySelectedDivisionArray[0], SelectedClassValue, tempOnlySelectedDivisionArray[1], OnlySelectedDistanceArray[index]).Values(&maps1)
						fmt.Println(err)
					} else {
						_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class) VALUES (?,?,?)", maps[0]["id"], OnlySelectedDivisionArray[index], SelectedClassValue).Values(&maps1)
						fmt.Println(err)
					}
					if err != nil {
						result = "false"
					}
				}
			}
		}
	}
	return result
}

func UpdateRegisterTeamEvent(data map[string]string, eventId string, SelectedDivisionArray []string, SelectedClassArray []string, SelectedRosterIdArray []string, OnlySelectedClassArray []string, OnlySelectedDivisionArray []string, updatedEventId string, SelectedBoatTypeArray []string, OnlySelectedDistanceArray []string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	result := "false"
	registrationStatus := "Pending"

	_, err := o.Raw("UPDATE event_register SET fk_team_id = ?, fk_event_id = ?, updated_at = ?, active = ?, registration_status = ?, roster_submit_status = ? WHERE id = ? RETURNING id", data["EventRegisterTeamid"], eventId, time.Now(), 1, registrationStatus, data["SubmitRosterLater"], updatedEventId).Values(&maps)
	fmt.Println(err)
	if err == nil {
		_, err := o.Raw("DELETE FROM event_register_roster WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps1)
		fmt.Println(err)
		_, err = o.Raw("DELETE FROM event_register_class_divisions WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps1)
		fmt.Println(err)
		result = "true"
		for index, RosterIdValue := range SelectedRosterIdArray {
			if RosterIdValue != "" {
				tempSelectedDivisionArray := strings.Split(SelectedDivisionArray[index], "_")
				if len(tempSelectedDivisionArray) > 1 {
					_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", maps[0]["id"], RosterIdValue, tempSelectedDivisionArray[0], SelectedClassArray[index], tempSelectedDivisionArray[1], SelectedBoatTypeArray[index]).Values(&maps1)
					fmt.Println(err)
				} else {
					_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", maps[0]["id"], RosterIdValue, tempSelectedDivisionArray[0], SelectedClassArray[index], "0", "0").Values(&maps1)
					fmt.Println(err)
				}
				if err != nil {
					result = "false"
				}
			}
		}

		// for index, SelectedClassValue := range OnlySelectedClassArray {
		// 	if SelectedClassValue != "" {
		// 		tempOnlySelectedDivisionArray := strings.Split(OnlySelectedDivisionArray[index], "_")
		// 		if len(tempOnlySelectedDivisionArray) > 1 {
		// 			_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class) VALUES (?,?,?)", maps[0]["id"], tempOnlySelectedDivisionArray[0], SelectedClassValue).Values(&maps1)
		// 			fmt.Println(err)
		// 		} else {
		// 			_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class) VALUES (?,?,?)", maps[0]["id"], OnlySelectedDivisionArray[index], SelectedClassValue).Values(&maps1)
		// 			fmt.Println(err)
		// 		}
		// 		if err != nil {
		// 			result = "false"
		// 		}
		// 	}
		// }

		for index, SelectedClassValue := range OnlySelectedClassArray {
			if SelectedClassValue != "" {
				tempOnlySelectedDivisionArray := strings.Split(OnlySelectedDivisionArray[index], "_")
				if len(tempOnlySelectedDivisionArray) > 1 {
					_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class, boat_type, distance ) VALUES (?,?,?,?,?)", maps[0]["id"], tempOnlySelectedDivisionArray[0], SelectedClassValue, tempOnlySelectedDivisionArray[1], OnlySelectedDistanceArray[index]).Values(&maps1)
					fmt.Println(err)
				} else {
					_, err := o.Raw("INSERT INTO event_register_class_divisions (fk_event_reg_id, div, class) VALUES (?,?,?)", maps[0]["id"], OnlySelectedDivisionArray[index], SelectedClassValue).Values(&maps1)
					fmt.Println(err)
				}
				if err != nil {
					result = "false"
				}
			}
		}
	}
	return result
}

func UpdateRegisterTeamEventStep2(data map[string]string, IteamId []string, IteamIdQuantity []string, PerPersonIteamId []string, PerPersonQuantity []string, price_Based string, updatedEventId string, IteamName []string, PerPersonIteamName []string, tempuserId string, isSuperAdmin bool) (string, string) {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params

	result := "true"
	totalPayAmount := "0"
	var additional_practice_bool *bool
	additional_practice_bool = nil
	if data["AdditionalPracticeBool"] == "Yes" {
		t := true
		additional_practice_bool = &t
	} else {
		t := false
		additional_practice_bool = &t
	}

	if data["ParticipantsCount"] == "" {
		data["ParticipantsCount"] = "0"
	}

	if data["AdditionalPracticeQuantity"] == "" {
		data["AdditionalPracticeQuantity"] = "0"
	}

	_, err := o.Raw("UPDATE event_register SET additional_practice_bool = ?, additional_practice_quantity = ?, updated_at = ?, participant_member_count=? WHERE id = ? RETURNING id, fk_event_id, fk_team_id", additional_practice_bool, data["AdditionalPracticeQuantity"], time.Now(), data["ParticipantsCount"], updatedEventId).Values(&maps)
	fmt.Println(err)
	if err == nil {
		// eventOrganizer, CoeventOrganizer := CheckUserIsEventOrCoEventOrganizer(maps[0]["fk_event_id"].(string), tempuserId)

		// captainCoCaptainStatus := CheckUserIsCaptainOrCoCaptain(maps[0]["fk_team_id"].(string), tempuserId)

		// changeBy := ""
		// if isSuperAdmin {
		// 	changeBy = "SuperAdmin"
		// } else if eventOrganizer || CoeventOrganizer {
		// 	changeBy = "EventOrganizer"
		// } else if captainCoCaptainStatus {
		// 	changeBy = "Team Captain"
		// }
		additionalQuantityTotal := new(big.Rat)
		additionalQuantityTotal.SetString("0")
		if data["AdditionalPracticeBool"] == "Yes" {
			result, additionalQuantityTotal = GetAdditionalPracticeTotal(data["EventRegisterEventid"], additionalQuantityTotal, result, data["AdditionalPracticeQuantity"])
		}

		if result == "true" {
			_, err = o.Raw("DELETE FROM event_register_pricing_other WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps1)

			if err != nil {
				result = "false"
			}
		}

		additionalPricingOtherTotal := new(big.Rat)
		additionalPricingOtherTotal.SetString("0")

		if result == "true" {

			teampTotalPricingOtherOrderCount := 0.00
			for index, IteamIdValue := range IteamId {
				iteamIdQuantity := "0"
				if index < len(IteamIdQuantity) {
					if IteamIdQuantity[index] != "" {
						iteamIdQuantity = IteamIdQuantity[index]
					}
				}
				_, err := o.Raw("INSERT INTO event_register_pricing_other (fk_event_reg_id, iteam_id, iteam_id_quantity) VALUES (?,?,?)", maps[0]["id"], IteamIdValue, iteamIdQuantity).Values(&maps1)

				if err != nil {
					result = "false"
					break
				} else {
					result, teampTotalPricingOtherOrderCount = GetOtherIteamTotal(data["EventRegisterEventid"], IteamIdValue, iteamIdQuantity, teampTotalPricingOtherOrderCount, result)
					if result == "false" {
						break
					}
				}
			}

			additionalPricingOtherTotal.SetFloat64(teampTotalPricingOtherOrderCount)
		}

		if result == "true" {
			_, err = o.Raw("DELETE FROM event_register_role_pricing_quantity WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps2)
			if err != nil {
				result = "false"
			}
		}

		personPricingTotal := new(big.Rat)
		personPricingTotal.SetString("0")
		teampTotalPersonMemberCount := 0
		if result == "true" {

			teampTotalPersonPricingOrderCount := 0.00

			if price_Based == "PP" {
				for index, PerPersonIteamIdValue := range PerPersonIteamId {
					if PerPersonIteamIdValue != "" {
						_, err := o.Raw("INSERT INTO event_register_role_pricing_quantity (fk_event_reg_id, role_iteam_id, role_iteam_id_quantity) VALUES (?,?,?)", maps[0]["id"], PerPersonIteamIdValue, PerPersonQuantity[index]).Values(&maps2)
						fmt.Println(err)
						if err != nil {
							result = "false"
							break
						} else {
							result, teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount = GetPerPetrsonTotalCount(data["EventRegisterEventid"], PerPersonIteamIdValue, PerPersonQuantity[index], teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount, result)
							if result == "false" {
								break
							}
						}
					}
				}
			} else {

				i, err2 := strconv.Atoi(data["ParticipantsCount"])
				if err2 == nil {
					teampTotalPersonMemberCount = i
				} else {
					result = "false"
				}
			}

			personPricingTotal.SetFloat64(teampTotalPersonPricingOrderCount)
		}

		if result == "true" {
			result, totalPayAmount = Insert_UpdateTodalEventRegisterPaymentDetail(updatedEventId, data["EventRegisterTeamid"], data["EventRegisterEventid"], additionalQuantityTotal, additionalPricingOtherTotal, personPricingTotal, teampTotalPersonMemberCount)
		}

	} else {
		result = "false"
	}
	return result, totalPayAmount
}

func UpdateRegisterTeamEventStep3(data map[string]string, AdditionalQuestion []string, AdditionalAnswer []string, updatedEventId string, eventId string, tempeventregisterteamID string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	result := "false"

	_, err := o.Raw("UPDATE event_register SET message = ?, updated_at = ? WHERE id = ? RETURNING id", data["Event_RegisterMessage"], time.Now(), updatedEventId).Values(&maps)
	fmt.Println(err)
	if err == nil {
		_, err := o.Raw("DELETE FROM reg_event_questionary WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps1)
		fmt.Println(err)
		result = "true"

		for index, additionalAnswerValue := range AdditionalAnswer {
			if additionalAnswerValue != "" {
				_, err := o.Raw("INSERT INTO reg_event_questionary (fk_event_reg_id, questions, answer) VALUES (?,?,?)", maps[0]["id"], AdditionalQuestion[index], additionalAnswerValue).Values(&maps1)
				fmt.Println(err)
				if err != nil {
					result = "false"
				}
			}
		}

	}
	if result == "true" {
		var teammaps []orm.Params
		var eventmaps []orm.Params
		var eventregmaps []orm.Params
		var eventregroster []orm.Params
		var eventregOrderDetail []orm.Params
		var eventpricingotherDetail []orm.Params
		var event_questionary []orm.Params
		var event_alldivision []orm.Params

		site_url := ""
		if len(os.Getenv("SITE_URL")) > 0 {
			site_url = os.Getenv("SITE_URL")
		} else {
			site_url = "http://localhost:5000/"
		}

		num, _ := o.Raw("SELECT * FROM team WHERE id = ? ", tempeventregisterteamID).Values(&teammaps)
		if num > 0 {
			num, _ = o.Raw("SELECT * FROM event WHERE id = ? ", eventId).Values(&eventmaps)

			num1, _ := o.Raw("SELECT * FROM event_register WHERE id = ? ", updatedEventId).Values(&eventregmaps)

			if num > 0 && num1 > 0 {
				if eventmaps[0]["fk_organizer_id"] != nil {

					teammember_details := GetUserDetails(teammaps[0]["fk_captain_id"].(string))
					eventmember_details := GetUserDetails(eventmaps[0]["fk_organizer_id"].(string))

					team_name := teammaps[0]["team_name"].(string)
					teammember_name := teammember_details["user"]["name"].(string)
					teammember_lastname := teammember_details["user"]["last_name"].(string)
					teamemail_address := teammember_details["user"]["email_address"].(string)
					teamemail_PhoneNumber := ""

					if eventmember_details["user"]["instant_emails"].(string) == "1" {
						eventmember_name := eventmember_details["user"]["name"].(string)

						regeventmsg := ""
						if eventregmaps[0]["message"] != nil {
							regeventmsg = eventregmaps[0]["message"].(string)
						}

						if teammember_details["user"]["country_dial_code"] != nil && teammember_details["user"]["phone"] != nil && teammember_details["user"]["country_dial_code"] != "" && teammember_details["user"]["phone"] != "" {
							teamemail_PhoneNumber = teammember_details["user"]["country_dial_code"].(string) + " " + teammember_details["user"]["phone"].(string)
						}

						footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)

						newcontent1 := "<p>New Event Registration for EventName: " + eventmaps[0]["event_name"].(string) + "</p><p>Team: " + team_name + "</p><p>Registration By: " + teammember_name + " " + teammember_lastname + "| " + teamemail_address + "| " + teamemail_PhoneNumber + "</p>"
						newcontent2 := "<p><h4>Message from Captain:</h4></p> <p style='font-style: italic;'>" + regeventmsg + "</p>"
						newcontent4 := ""
						num11, _ := o.Raw("SELECT * FROM event_register_roster WHERE fk_event_reg_id = ? ", updatedEventId).Values(&eventregroster)

						if num11 > 0 {
							newcontent4 += "<h4>Categories: </h4>"
							newcontent4 += "<ul>"
							for _, v := range eventregroster {
								tempclass := v["clases"].(string)
								if tempclass == "M" {
									tempclass = "Men"
								} else if tempclass == "W" {
									tempclass = "Women"
								} else if tempclass == "MX" {
									tempclass = "Mixed"
								}
								boat_type := ""
								if v["boat_type"] != nil {
									boat_type = v["boat_type"].(string)
								}
								distance := ""
								if v["distance"] != nil {
									distance = v["distance"].(string)
								}

								tempdivision := v["division"].(string)
								tempdivision1 := ""
								num111, _ := o.Raw("SELECT div_name FROM all_divisions as ad WHERE ad.value = ? ", tempdivision).Values(&event_alldivision)
								if num111 > 0 {
									tempdivision1 = event_alldivision[0]["div_name"].(string)
								}
								newcontent4 += "<li>" + tempdivision1 + " " + tempclass + " " + boat_type + "-Person " + distance + "M</li>"
							}
							newcontent4 += "</ul>"
						}
						newcontent5 := ""
						num12, _ := o.Raw("SELECT * FROM pricing_other WHERE fk_event_id = ? ", eventId).Values(&eventpricingotherDetail)
						num13, _ := o.Raw("SELECT * FROM event_register_pricing_other WHERE fk_event_reg_id = ? ", updatedEventId).Values(&eventregOrderDetail)
						if num12 > 0 && num13 > 0 {
							newcontent5 += "<h4>Order Items: </h4>"
							newcontent5 += "<table>"
							newcontent5 += "<thead><tr><td style='font-weight: 600;'>Item name</td> <td style='font-weight: 600;'>Quantity</td></tr>"
							for _, v := range eventpricingotherDetail {
								tempitem := v["item"].(string)
								tempitemId := v["id"].(string)
								ordertempitem := ""
								ordertempitemId := ""
								for _, v1 := range eventregOrderDetail {
									ordertempitem = v1["iteam_id"].(string)
									if tempitemId == ordertempitem {
										ordertempitemId = v1["iteam_id_quantity"].(string)
										newcontent5 += "<tr><td>" + tempitem + "</td><td>" + ordertempitemId + "</td> </tr>"
										break
									}
								}

							}
							newcontent5 += "</thead>"
							newcontent5 += "</table>"
						}

						newcontent6 := ""

						num14, _ := o.Raw("SELECT * FROM reg_event_questionary WHERE fk_event_reg_id = ? ", updatedEventId).Values(&event_questionary)

						if num14 > 0 {
							newcontent6 += "<h4>Questions & Answers:</h4>"
							newcontent6 += "<ul>"
							for _, v := range event_questionary {
								newcontent6 += "<li> <p>Question: " + v["questions"].(string) + "</p>  <p> Answer: " + v["answer"].(string) + "</p></li>"
							}
							newcontent6 += "</ul>"
						}
						newcontent7 := "<p>Sincerely, The Gushou Team </p>"
						//email_html_message := "<html>Hello " + eventmember_name + ", <br/><br/>" + content + content1 + content2 + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + "</html>"
						unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
						email_html_message := "<html>Hi " + eventmember_name + ", <br/><br/>" + newcontent1 + newcontent2 + newcontent4 + newcontent5 + newcontent6 + newcontent7 + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

						common.SendEmail(eventmember_details["user"]["email_address"].(string), "Event Registration On Gushou!", email_html_message)
					}
					if teammember_details["user"]["country_dial_code"] != nil && teammember_details["user"]["phone"] != nil && teammember_details["user"]["country_dial_code"] != "" && teammember_details["user"]["phone"] != "" {
						teamemail_PhoneNumber = teammember_details["user"]["country_dial_code"].(string) + " " + teammember_details["user"]["phone"].(string)
					}
					tempName := teammember_name + " " + teammember_lastname
					tempTeamPhoto := ""
					if teammember_details["user"]["files"] != nil {
						tempTeamPhoto = teammember_details["user"]["files"].(string)
					}
					_, err := o.Raw("UPDATE event_register SET registration_status = ?,reg_email_id = ?,reg_captain_name = ?,reg_phone_number = ?, updated_at = ?, register_user_file =? WHERE id = ? RETURNING id", "Success", teamemail_address, tempName, teamemail_PhoneNumber, time.Now(), tempTeamPhoto, updatedEventId).Values(&maps)
					fmt.Println(err)

				}
			}
		}
	}

	return result
}

func DeleteRegisterTeamEventFromDB(regTeameventId string) {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("DELETE FROM reg_event_questionary WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register_pricing_other WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register_roster WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register_class_divisions WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register WHERE id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register_role_pricing_quantity WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register_payments WHERE fk_event_reg_id = ? ", regTeameventId).Values(&maps)
	_, err = o.Raw("DELETE FROM event_register WHERE id = ? ", regTeameventId).Values(&maps)
	fmt.Println(err)

}

func GetEventRegistrationDetail(eventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT er.message as registermessage, t.team_name as team_name, er.registration_status as registration_status   FROM event_register er  inner join team t on er.fk_team_id = t.id   WHERE  er.fk_event_id = ?", eventId).Values(&maps)
	fmt.Println(err)
	return maps

}

func GetEventRegistrationDetail_Roster(teamId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT er.id as eventregisterid,er.fk_event_id as eventfk_event_id, er.registration_status as registration_status, err.roster_id as eventregisterrosterid  FROM event_register er inner join event ev on ev.id = er.fk_event_id  left join event_register_roster err on er.id = err.fk_event_reg_id WHERE er.fk_team_id = ?", teamId).Values(&maps)
	fmt.Println(err)
	return maps

}

func GetEventClassDivisionsDetail(eventId string, teamId string) ([]orm.Params, []orm.Params) {

	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params

	num, err := o.Raw("SELECT * FROM event_register WHERE fk_event_id = ? and fk_team_id =?", eventId, teamId).Values(&maps)
	fmt.Println(err)
	if err == nil && num > 0 {
		_, err := o.Raw("SELECT * FROM event_register_class_divisions WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps)
		fmt.Println(err)
	}
	_, err = o.Raw("SELECT * FROM all_divisions").Values(&maps1)
	fmt.Println(err)

	return maps, maps1

}

func EventRegisterRosterToEvent(eventid string, registerrosterid string, registerclass string, registerdivision string, teamId string, distance string) string {

	o := orm.NewOrm()
	result := "false"
	var maps []orm.Params
	var maps1 []orm.Params

	num, err := o.Raw("SELECT * FROM event_register WHERE active =1 and fk_event_id = ? and fk_team_id =?", eventid, teamId).Values(&maps)
	fmt.Println(err)
	div_arr := strings.Split(registerdivision, "_")
	if num > 0 {

		if len(distance) > 1 {
			num, err = o.Raw("SELECT * FROM event_register_roster WHERE  division =? and clases =? and roster_id =? and boat_type =? and distance =? and fk_event_reg_id =?", div_arr[0], registerclass, registerrosterid, div_arr[1], distance, maps[0]["id"]).Values(&maps1)
			fmt.Println(err)
		} else {
			num, _ = o.Raw("SELECT * FROM event_register_roster WHERE  division =? and clases =? and roster_id =? and fk_event_reg_id =?", registerdivision, registerclass, registerrosterid, maps[0]["id"]).Values(&maps1)
		}
		if num > 0 {
			result = "alreadyexist"
		} else {
			if len(distance) > 1 {
				_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", maps[0]["id"], registerrosterid, div_arr[0], registerclass, div_arr[1], distance).Values(&maps1)
				fmt.Println(err)
				if err == nil {
					result = "true"
				} else {
					result = "false"
				}
			} else {
				_, err := o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases) VALUES (?,?,?,?)", maps[0]["id"], registerrosterid, registerdivision, registerclass).Values(&maps1)
				fmt.Println(err)
				if err == nil {
					result = "true"
				} else {
					result = "false"
				}
			}
		}
	}

	return result
}

func CheckRosterRegisterWithEvent(rosterId string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"

	num, err := o.Raw("SELECT * FROM event_register_roster WHERE roster_id = ? ", rosterId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		result = "true"
	}
	return result
}

func CheckTeamAlreadyRegisterInEvent(team_id string, event_id string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"

	num, err := o.Raw("SELECT * FROM event_register WHERE fk_team_id = ? and fk_event_id = ? and active =1 ", team_id, event_id).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		result = "true"
	}
	return result
}

func GetEventQuestionary(id string) map[int]orm.Params {

	result := make(map[int]orm.Params)

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM event_questionary WHERE fk_event_id = ? and active='1'", id).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			result[k+1] = v
		}

	} else {
		fmt.Println("No events questionary")
		result[1] = nil
	}

	//fmt.Println(result)

	return result

}

func GetRegisterEventCount(teamId string) string {
	result := "0"

	o := orm.NewOrm()

	var maps []orm.Params

	dte := time.Now()
	tempCurrentFormatDate := dte.Format("2006-01-02")
	num, _ := o.Raw("SELECT  count(er.fk_team_id) FROM event_register as er left join event ev on ev.id = er.fk_event_id WHERE fk_team_id = ? and er.active=1 and ev.start_date >=? and er.registration_status =? and er.reg_cancel_status =?", teamId, tempCurrentFormatDate, "Success", false).Values(&maps)

	if num > 0 {
		result = maps[0]["count"].(string)
	}
	//fmt.Println(result)

	return result

}

type rostersubmitstatus struct {
	isPendingRoster bool
}

func GetRegisterEventTeamDetail(teamId string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params

	resultList := []map[string]orm.Params{}

	dte := time.Now()
	tempCurrentFormatDate := dte.Format("2006-01-02")

	num, err := o.Raw("SELECT *,ev.event_name,er.id as eventregisterid, ev.id as eventid,fk_organizer_id as eventcaptain, to_char(er.created_at, 'Mon DD- YY') as registerdate, CASE WHEN erp.total_paid = 0 THEN 'unpaid' WHEN erp.total_amount > erp.total_paid THEN 'partial' WHEN erp.total_amount = erp.total_paid THEN 'paid'  end as paymentstatus  FROM event_register as er left join event ev on ev.id = er.fk_event_id left join event_register_payments as erp on er.id = erp.fk_event_reg_id WHERE er.fk_team_id = ? and er.active= 1 and ev.start_date >= ? and er.registration_status =? and er.reg_cancel_status =? order by ev.id desc", teamId, tempCurrentFormatDate, "Success", false).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, v := range maps {

			result["registerEvent"] = v

			num1, err1 := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps1)
			fmt.Println(err1)
			_, err2 := o.Raw("SELECT CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END FROM event_register_roster  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps3)
			fmt.Println(err2)
			if num1 > 0 && maps3[0]["case"].(string) == "true" {
				if maps1[0]["div"] != nil && maps1[0]["class"] != nil && maps1[0]["boat_type"] != nil && maps1[0]["distance"] != nil {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.boat_type= tr.boat_type and err.distance= tr.distance and err.fk_event_reg_id= tr.fk_event_reg_id ) and tr.fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]

				} else {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.fk_event_reg_id= tr.fk_event_reg_id ) and tr.fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]
				}
			} else {
				result["roster_partial"] = maps3[0]
			}

			if v["eventid"] != nil {
				tempDetail := CheckStripeOptionEnableOrNotByEventOrgFullDetail(v["eventid"].(string))
				if tempDetail != nil {
					result["paymentoption"] = tempDetail[0]
				}
			}

			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}

	} else {
		fmt.Println("No Register Event")
		result["status"] = nil
	}

	//fmt.Println(result)

	return resultList

}

func GetTeamEventRegistrationDetail(teamid string, eventId string) (orm.Params, []orm.Params, []orm.Params, []orm.Params, []orm.Params, []orm.Params) {

	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params
	var maps5 []orm.Params
	var maps6 []orm.Params

	num, _ := o.Raw("SELECT * FROM event_register WHERE fk_event_id = ? and fk_team_id = ?", eventId, teamid).Values(&maps)
	if num > 0 {
		maps2 = maps[0]
		fmt.Println("maps[0](string)", maps[0]["id"].(string))
		_, _ = o.Raw("SELECT * FROM event_register_roster WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps1)
		_, _ = o.Raw("SELECT * FROM event_register_class_divisions WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps3)
		_, _ = o.Raw("SELECT * FROM event_register_pricing_other WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps4)
		_, _ = o.Raw("SELECT * FROM reg_event_questionary WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps5)
		_, _ = o.Raw("SELECT * FROM event_register_role_pricing_quantity WHERE fk_event_reg_id = ? ", maps[0]["id"].(string)).Values(&maps6)
	}
	return maps2, maps1, maps3, maps4, maps5, maps6
}

func CheckTeamRegisterOrNot(eventId string, teamId string) string {

	o := orm.NewOrm()

	var maps []orm.Params
	result := "false"
	num, err := o.Raw("SELECT  * FROM event_register WHERE fk_event_id = ? and  fk_team_id = ? and registration_status ='Success' and active = 1", eventId, teamId).Values(&maps)
	if num > 0 {
		result = "true"
	}
	fmt.Println(err)
	return result

}

func GetEventClassDivisions(eventId string) []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT  * FROM event_class_divisions  WHERE fk_event_id = ?", eventId).Values(&maps)
	fmt.Println(err)
	return maps
}

func GetEventRegisterCount(eventId string) string {
	result := "0"

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT count(fk_event_id) FROM event_register WHERE fk_event_id = ? and active= 1 and registration_status ='Success'", eventId).Values(&maps)
	if num > 0 {
		result = maps[0]["count"].(string)
	}
	//fmt.Println(result)

	return result

}

func GetEventRegisterTeamDetail(eventId string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params

	resultList := []map[string]orm.Params{}

	num, err := o.Raw("select *,CASE WHEN eve_pay.total_paid = 0 THEN 'unpaid' WHEN eve_pay.total_amount > eve_pay.total_paid THEN 'partial' WHEN eve_pay.total_amount = eve_pay.total_paid THEN 'paid' end as paymentstatus,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') as registerdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id left join event_register_payments as eve_pay on eve_pay.fk_event_reg_id=er.id WHERE er.fk_event_id = ? and er.active=1 and er.registration_status ='Success' order by er.id desc", eventId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, v := range maps {

			result["registerEvent"] = v
			num1, err1 := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps1)
			fmt.Println(err1)
			_, err2 := o.Raw("SELECT CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END FROM event_register_roster  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps3)
			fmt.Println(err2)
			if num1 > 0 && maps3[0]["case"].(string) == "true" {
				if maps1[0]["div"] != nil && maps1[0]["class"] != nil && maps1[0]["boat_type"] != nil && maps1[0]["distance"] != nil {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.boat_type= tr.boat_type and err.distance= tr.distance) and fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]

				} else {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class ) and fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]
				}
			} else {
				result["roster_partial"] = maps3[0]
			}

			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}

	} else {
		fmt.Println("No Team Register Event")
		result["status"] = nil
	}

	//fmt.Println(result)

	return resultList

}

func GetEventRegisterTeamDetailOrderByTeam(eventId string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params

	resultList := []map[string]orm.Params{}

	num, err := o.Raw("select *,CASE WHEN eve_pay.total_paid = 0 THEN 'unpaid' WHEN eve_pay.total_amount > eve_pay.total_paid THEN 'partial' WHEN eve_pay.total_amount = eve_pay.total_paid THEN 'paid' end as paymentstatus,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') as registerdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id left join event_register_payments as eve_pay on eve_pay.fk_event_reg_id=er.id WHERE er.fk_event_id = ? and er.active=1 and er.registration_status ='Success' order by te.team_name asc", eventId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, v := range maps {

			result["registerEvent"] = v
			num1, err1 := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps1)
			fmt.Println(err1)
			_, err2 := o.Raw("SELECT CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END FROM event_register_roster  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps3)
			fmt.Println(err2)
			if num1 > 0 && maps3[0]["case"].(string) == "true" {
				if maps1[0]["div"] != nil && maps1[0]["class"] != nil && maps1[0]["boat_type"] != nil && maps1[0]["distance"] != nil {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.boat_type= tr.boat_type and err.distance= tr.distance) and fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]

				} else {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class ) and fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]
				}
			} else {
				result["roster_partial"] = maps3[0]
			}

			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}

	} else {
		fmt.Println("No Team Register Event")
		result["status"] = nil
	}

	//fmt.Println(result)

	return resultList

}

func GetEventRegisterDetailById(eventRegisterId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT * FROM event_register WHERE id = ? and active= 1", eventRegisterId).Values(&maps)
	fmt.Println(err)
	return maps
}

func EditEventRegisterDetail(reg_event_captain_name string, reg_event_captain_email string, reg_event_captain_phone string, reg_event_captain_note string, reg_event_id string) string {
	result := "true"
	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("UPDATE event_register SET reg_captain_name = ?, reg_email_id = ?,reg_phone_number = ?, message = ?, updated_at = ? WHERE id = ? RETURNING id", reg_event_captain_name, reg_event_captain_email, reg_event_captain_phone, reg_event_captain_note, time.Now(), reg_event_id).Values(&maps)
	fmt.Println(err)
	if err != nil {
		result = "false"
	}
	return result
}

func SendEventTeamEmail(reg_event_ids string, email_to string, message string, login_userId string, send_announcement string, event_id string) string {
	result := "true"
	o := orm.NewOrm()
	var email_array []string
	var co_captain_email_array []string
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params
	var maps5 []orm.Params
	var maps6 []orm.Params
	var notif_insert []orm.Params
	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	if email_to == "team members" {
		emailArray := strings.Split(reg_event_ids, ",")
		for _, email_id := range emailArray {

			num, err := o.Raw("select  te.fk_captain_id,eve.event_name as event_name,fk_event_id,reg_captain_name,reg_email_id,email_address, us.id  from event_register as eve_reg left join event as eve on eve.id = eve_reg.fk_event_id left join team as te on eve_reg.fk_team_id = te.id left join teamjoin as te_join on te_join.fk_team_id = te.id left join users as us on te_join.fk_member_id = us.id left join user_profile as us_pro on us_pro.fk_user_id = us.id where eve_reg.id =? and te_join.active= 1 and request_status =2 and us_pro.instant_emails =1", email_id).Values(&maps)

			if err != nil {
				result = "false"
			}
			if num > 0 {

				num1, err1 := o.Raw("select name as login_fname from user_profile where id = ?", login_userId).Values(&maps4)
				fmt.Println(err1)

				if err1 != nil {
					result = "false"
				}
				var sendersFName string
				if num1 > 0 {
					if maps4[0]["login_fname"] != nil {
						sendersFName = maps4[0]["login_fname"].(string)
					}

				}
				var captain_email, fk_captain_id, team_mem_email, fk_event_id, fk_receiver_id, event_name string

				for _, v := range maps {

					team_mem_email = v["email_address"].(string)
					event_name = v["event_name"].(string)
					fk_captain_id = v["fk_captain_id"].(string)

					num2, err2 := o.Raw(" select email_address as captain_email from users where id  = ?", fk_captain_id).Values(&maps5)
					fmt.Println(err2)

					if err2 != nil {
						result = "false"
					}

					if num2 > 0 {

						if maps5[0]["captain_email"] != nil {
							captain_email = maps5[0]["captain_email"].(string)

						}

					}
					if captain_email != team_mem_email {

						email_array = append(email_array, v["email_address"].(string)+",")
					}

				}
				if captain_email != "" && email_array != nil {

					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
					justString := strings.Join(email_array, " ")
					html_email_message := "<html>Hi There, <br/><br/>" + "Message sent by " + sendersFName + ", " + event_name + ":<br><br>" + "<div style='text-indent: 30px;'>" + message + "</div>" + "<br><br>Sent via Gushou" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
					common.SendEmailWithBCC(captain_email+"-"+justString, event_name+", Message via Gushou", html_email_message)

					for _, v2 := range maps {

						fk_event_id = v2["fk_event_id"].(string)
						fk_receiver_id = v2["id"].(string)

						_, _ = o.Raw("select event_name, slug from event where id = ?", fk_event_id).Values(&maps3)

						event_name := maps3[0]["event_name"].(string)
						event_slug := maps3[0]["slug"].(string)
						eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
						desc := "New Message from " + eventUrl + " send via email."

						_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) RETURNING id", "{9}", "1", time.Now(), true, desc, "9", fk_event_id, login_userId, "{"+fk_receiver_id+"}").Values(&notif_insert)

						if err != nil {
							result = "false"
							fmt.Println("Notification failed")
						}

					}

				} else {
					result = "No Team Members Available for Team. Can't send mail"
				}

			}
		}
		//send_announcement
		if send_announcement == "yes" {
			AddEventAnnouncement(message, login_userId, event_id)
		}

	} else if email_to == "captains" {

		num, err := o.Raw("select get_event_organ_det.email_address as event_email,eve.fk_organizer_id,fk_captain_id,fk_co_captains,eve.event_name as event_name,fk_event_id,reg_email_id , reg_captain_name from event_register as eve_reg left join event as eve on eve.id = eve_reg.fk_event_id left join team as te on eve_reg.fk_team_id = te.id left join users as us on us.id = te.id left join user_profile as us_pro on us_pro.id = us.id left join users as get_event_organ_det on get_event_organ_det.id = eve.fk_organizer_id  where eve_reg.id in (" + reg_event_ids + ")").Values(&maps1)
		fmt.Println(err)
		if err != nil {
			result = "false"
		}
		if num > 0 {

			num1, err1 := o.Raw("select name as login_fname from user_profile where id = ?", login_userId).Values(&maps4)
			fmt.Println(err1)

			if err1 != nil {
				result = "false"
			}
			var sendersFName string
			if num1 > 0 {
				if maps4[0]["login_fname"] != nil {
					sendersFName = maps4[0]["login_fname"].(string)
				}

			}

			//var captain_email, captain_name, fk_event_id, fk_captain_id, event_name, ReceiversFName string
			var captain_email, fk_captain_id, co_captain_email, fk_co_captain_id, contact_person_email, event_organizer_email string
			var fk_event_id, event_name, ReceiversFName, event_organizer_email_toSend, contact_person_email_toSend string

			for _, v := range maps1 {

				contact_person_email = v["reg_email_id"].(string)
				fk_event_id = v["fk_event_id"].(string)
				event_name = v["event_name"].(string)
				fk_captain_id = v["fk_captain_id"].(string)
				event_organizer_email = v["event_email"].(string)

				fk_co_captain_id = v["fk_co_captains"].(string)
				fk_co_captain_id = strings.Replace(fk_co_captain_id, `}`, "", -1)
				fk_co_captain_id = strings.Replace(fk_co_captain_id, `{`, "", -1)

				num2, err2 := o.Raw(" select usp.name as login_fname , us.email_address as captain_email from user_profile as usp left join users as us on  usp.id = us.id  where usp.id = ?", fk_captain_id).Values(&maps5)
				fmt.Println(err2)

				if err2 != nil {
					result = "false"
				}

				if num2 > 0 {
					if maps5[0]["login_fname"] != nil {
						ReceiversFName = maps5[0]["login_fname"].(string)

					}
					if maps5[0]["captain_email"] != nil {
						captain_email = maps5[0]["captain_email"].(string)

					}

				}
				num3, _ := o.Raw("select email_address as co_captains_email  from users where id in (" + fk_co_captain_id + ") AND id NOT IN (" + fk_captain_id + ")").Values(&maps6)

				if num3 > 0 {
					for _, v := range maps6 {

						co_captain_email = v["co_captains_email"].(string)

						if captain_email != co_captain_email {

							co_captain_email_array = append(co_captain_email_array, v["co_captains_email"].(string)+",")
						}

					}
				}

				if event_organizer_email != captain_email {
					event_organizer_email_toSend = event_organizer_email
				}
				if contact_person_email != event_organizer_email {
					contact_person_email_toSend = contact_person_email
				}
				if contact_person_email != captain_email {
					contact_person_email_toSend = contact_person_email
				} else {
					contact_person_email_toSend = ""
				}

				if captain_email != "" {

					footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
					unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
					html_email_message := "<html>Hi " + ReceiversFName + ", <br/><br/>" + "Message sent by " + sendersFName + ", " + event_name + ":<br><br>" + "<div style='text-indent: 30px;'>" + message + "</div>" + "<br><br>Sent via Gushou" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"

					//common.SendEmail(captain_email, event_name+", Message via Gushou", html_email_message)
					justString_CoCaptainEmail := strings.Join(co_captain_email_array, " ")
					common.SendEmailWithCC(captain_email+"-"+event_organizer_email_toSend+","+justString_CoCaptainEmail+","+contact_person_email_toSend, event_name+", Message via Gushou", html_email_message)

					_, _ = o.Raw("select event_name, slug from event where id = ?", fk_event_id).Values(&maps2)

					event_name := maps2[0]["event_name"].(string)
					event_slug := maps2[0]["slug"].(string)
					eventUrl := "<a href='" + site_url + "event-view/" + event_slug + "'>" + event_name + "</a>"
					desc := "New Message from " + eventUrl + " send via email."

					_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?)", "{9}", "1", time.Now(), true, desc, "9", fk_event_id, login_userId, "{"+fk_captain_id+"}").Values(&notif_insert)

					if err != nil {
						result = "false"
						fmt.Println("Notification failed")
					}

				}
			}
		}
		//send_announcement
		if send_announcement == "yes" {
			AddEventAnnouncement(message, login_userId, event_id)
		}
	} else if send_announcement == "yes" {

		AddEventAnnouncement(message, login_userId, event_id)

	}

	return result
}

func CancelEventRegisterDetailById(reg_event_id string) string {
	result := "true"
	o := orm.NewOrm()

	var maps []orm.Params

	status := GetEventRegisterTeamPaymentToalPaidDetail(reg_event_id)

	if status {
		_, err := o.Raw("UPDATE event_register SET active = ?, updated_at = ? WHERE id = ? ", 0, time.Now(), reg_event_id).Values(&maps)
		fmt.Println(err)
		if err != nil {
			result = "false"
		}
	} else {
		_, err := o.Raw("UPDATE event_register SET reg_cancel_status = ?, updated_at = ? WHERE id = ? RETURNING id", true, time.Now(), reg_event_id).Values(&maps)

		fmt.Println(err)
		if err != nil {
			result = "false"
		}
	}
	return result
}

// func GetAllDivisionDetail() []orm.Params {
// 	o := orm.NewOrm()

// 	var maps []orm.Params

// 	_, _ = o.Raw("SELECT * FROM all_divisions as ad oCancelEventRegisterDetailByIdrder by ad.order asc").Values(&maps)

// 	return maps
// }

func GetEventSelectedClassDivision(eventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event_class_divisions WHERE fk_event_id = ? ", eventId).Values(&maps)

	return maps
}

func CheckValidRegisterEvent(register_eventId string, eventId string) map[string]orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params
	result := make(map[string]orm.Params)

	num, _ := o.Raw("SELECT * FROM event_register WHERE id = ? and fk_event_id=? ", register_eventId, eventId).Values(&maps)

	if num > 0 {
		for k, v := range maps {
			key := "event_reg" + strconv.Itoa(k)
			//fmt.Println("key ", key)
			//fmt.Println("valus", v)
			result[key] = v
		}
	}
	return result
}

func GetEventPracticePricingByEventId(eventid string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event_practice_pricing WHERE fk_event_id = ? order by name desc", eventid).Values(&maps)

	return maps
}

func GetPricingOtherIteamByEventId(eventid string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM pricing_other WHERE fk_event_id = ? order by id asc ", eventid).Values(&maps)

	return maps
}

func GetEventRegisterPricingOtherByEventRegId(register_eventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event_register_pricing_other WHERE fk_event_reg_id = ? ", register_eventId).Values(&maps)

	return maps
}

func GetRegEventQuestionaryByEventRegId(register_eventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM reg_event_questionary WHERE fk_event_reg_id = ? ", register_eventId).Values(&maps)

	return maps
}

func GetRegisterRostersListByEventRegId(register_eventId string) []map[string]string {
	result := make(map[string]string)
	o := orm.NewOrm()
	var maps []orm.Params
	//var maps1 []orm.Params
	var maps2 []orm.Params
	resultList := []map[string]string{}
	//var checkClsDivArray []string

	_, _ = o.Raw("select * from event_register_class_divisions where fk_event_reg_id = ? order by boat_type, div, class asc ", register_eventId).Values(&maps)
	for _, v := range maps {

		div_type := v["div"].(string)
		class_type := v["class"].(string)
		boat_type := ""
		//boat_typeValue := ""
		distancestring := ""
		if v["boat_type"] != nil {
			boat_type = v["boat_type"].(string)
			//boat_typeValue = "_" + v["boat_type"].(string)
		}

		if v["distance"] != nil {
			distancestring = v["distance"].(string)
		}

		//teampClsDiv := div_type + "_" + class_type + boat_typeValue

		//statusClassDiv := true
		// for _, vv := range checkClsDivArray {
		// 	if vv == teampClsDiv {
		// 		statusClassDiv = false
		// 		break
		// 	}
		// }

		// if boat_type != "" {
		// 	_, _ = o.Raw("select * from event_register_class_divisions where fk_event_reg_id = ? and class=? and div=? and boat_type=? order by div, boat_type asc ", register_eventId, class_type, div_type, boat_type).Values(&maps1)

		// } else {
		// 	_, _ = o.Raw("select * from event_register_class_divisions where fk_event_reg_id = ? and class=? and div=? order by div, boat_type asc ", register_eventId, class_type, div_type).Values(&maps1)
		// }

		//distancestring := ""
		//	rosternamestring := ""
		//var roosterStringArray []string

		// for index, vv1 := range maps1 {

		// 	if vv1["boat_type"] != nil {

		// 		tempDistance := vv1["distance"].(string)

		// 		if tempDistance == "1000" {
		// 			tempDistance = "1km"
		// 		} else if tempDistance == "2000" {
		// 			tempDistance = "2km"
		// 		} else {
		// 			tempDistance = tempDistance + "m"
		// 		}
		// 		distancestring += tempDistance
		// 		if (len(maps1) - 1) != index {
		// 			distancestring = distancestring + ", "
		// 		}
		// 		_, _ = o.Raw("select tr.name as teamrostername, * from event_register_roster err left join team_roster tr on tr.id = err.roster_id where err.fk_event_reg_id = ? and err.division=? and err.clases=? and err.boat_type=? and err.distance=? order by err.division, err.boat_type asc", register_eventId, vv1["div"].(string), vv1["class"].(string), vv1["boat_type"].(string), vv1["distance"].(string)).Values(&maps2)
		// 	} else {
		// 		_, _ = o.Raw("select tr.name as teamrostername, * from event_register_roster err left join team_roster tr on tr.id = err.roster_id where fk_event_reg_id = ? and err.division=? and err.clases=? order by err.division , err.boat_type asc", register_eventId, vv1["div"].(string), vv1["class"].(string)).Values(&maps2)
		// 	}
		// 	teamprosterid := ""

		if boat_type != "" {
			_, _ = o.Raw("select tr.name as teamrostername, * from event_register_roster err left join team_roster tr on tr.id = err.roster_id where err.fk_event_reg_id = ? and err.division=? and err.clases=? and err.boat_type=? and err.distance=? order by err.division, err.boat_type asc", register_eventId, div_type, class_type, boat_type, distancestring).Values(&maps2)
		} else {
			_, _ = o.Raw("select tr.name as teamrostername, * from event_register_roster err left join team_roster tr on tr.id = err.roster_id where fk_event_reg_id = ? and err.division=? and err.clases=? order by err.division , err.boat_type asc", register_eventId, div_type, class_type).Values(&maps2)
		}

		rosternamestring := ""
		if maps2 != nil {
			rosternamestring = maps2[0]["teamrostername"].(string)
		}

		tempDistance := ""
		if distancestring == "1000" {
			tempDistance = "1km"
		} else if distancestring == "2000" {
			tempDistance = "2km"
		} else {
			tempDistance = distancestring + "m"
		}

		result["div"] = div_type
		result["class"] = class_type
		result["boat"] = boat_type
		result["distance"] = tempDistance
		result["roster"] = rosternamestring
		//	checkClsDivArray = append(checkClsDivArray, teampClsDiv)
		resultList = append(resultList, result)
		result = make(map[string]string)

	}

	return resultList
}

func GetRegisterTeamDetailByTeamId(team_Id string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("SELECT *, te.id as teamvalue FROM team as te left join team_logo_url as tlu on tlu.fk_team_id = te.id  WHERE te.id = ? ", team_Id).Values(&maps)

	return maps
}

func GetRegisterEventDetailByRegisterEventId(register_eventId string) []map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params

	resultList := []map[string]orm.Params{}

	num, err := o.Raw("SELECT *,te.slug as teamslug,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') as registerdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id WHERE er.id= ? and  er.registration_status ='Success' ", register_eventId).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		for _, v := range maps {

			result["registerEvent"] = v

			num1, err1 := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps1)
			fmt.Println(err1)
			_, err2 := o.Raw("SELECT CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END FROM event_register_roster  WHERE fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps3)
			fmt.Println(err2)
			if num1 > 0 && maps3[0]["case"].(string) == "true" {
				if maps1[0]["div"] != nil && maps1[0]["class"] != nil && maps1[0]["boat_type"] != nil && maps1[0]["distance"] != nil {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.boat_type= tr.boat_type and err.distance= tr.distance and err.fk_event_reg_id= tr.fk_event_reg_id) and tr.fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]

				} else {
					_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.fk_event_reg_id= tr.fk_event_reg_id ) and tr.fk_event_reg_id = ? ", v["eventregisterid"].(string)).Values(&maps2)
					fmt.Println(err1)
					result["roster_partial"] = maps2[0]
				}
			} else {
				result["roster_partial"] = maps3[0]
			}

			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}

	} else {
		fmt.Println("No Team Register Event")
		result["status"] = nil
	}

	//fmt.Println(result)

	return resultList

}

func GetTotalOrderItemList(eventpricingother []orm.Params, eventregpricingother []orm.Params) ([]map[string]string, float64) {
	resultList := []map[string]string{}
	totalOrderCost := 0.00
	result := make(map[string]string)
	for _, v := range eventpricingother {
		itemIdQuantity := "0"
		statusflag := false
		for _, v1 := range eventregpricingother {
			if v["id"].(string) == v1["iteam_id"].(string) {
				itemIdQuantity = v1["iteam_id_quantity"].(string)
				result["iteam_id"] = v1["iteam_id"].(string)
				statusflag = true
				break
			}
		}

		if !statusflag {
			result["iteam_id"] = v["id"].(string)
		}

		result["iteam_name"] = v["item"].(string)
		result["iteam_cost"] = v["cost"].(string)
		result["itemId_Quantity"] = itemIdQuantity
		result["itemId_Total"] = "0.00"
		f, err := strconv.ParseFloat(v["cost"].(string), 64)
		i, err2 := strconv.Atoi(itemIdQuantity)
		if err == nil && err2 == nil {
			tempf1 := float64(i)
			totalCost := tempf1 * f
			totalOrderCost = totalOrderCost + totalCost
			result["itemId_Total"] = strconv.FormatFloat(totalCost, 'f', -1, 64)
		}
		resultList = append(resultList, result)
		result = make(map[string]string)
	}
	return resultList, totalOrderCost
}

func CheckAnyEventRegistrationHappen(eventId string) bool {

	result := false

	o := orm.NewOrm()

	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM event_register WHERE fk_event_id = ? order by id desc", eventId).Values(&maps)

	if num > 0 {
		result = true
	}
	return result
}

func GetEventRegisterClassDivisionByRegisterEventId(register_eventId string) []map[string]string {

	resultList := []map[string]string{}
	result := make(map[string]string)

	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params

	num, _ := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? order by boat_type, div, class asc", register_eventId).Values(&maps)

	if num > 0 {
		for _, v := range maps {
			//statusClassDiv := true
			tempClass := v["class"].(string)
			tempDiv := v["div"].(string)
			tempBoatType := ""

			if v["boat_type"] != nil && v["distance"] != nil {

				tempBoatType = v["boat_type"].(string)
			}

			// for _, vv := range resultList {
			// 	if vv["class"] == tempClass && vv["div"] == tempDiv && vv["boat_type"] == tempBoatType {
			// 		statusClassDiv = false
			// 		break
			// 	}
			// }

			//if statusClassDiv {
			if v["boat_type"] != nil && v["distance"] != nil {
				result["class"] = tempClass
				result["div"] = tempDiv
				result["boat_type"] = tempBoatType

				tempDistance := v["distance"].(string)
				if tempDistance == "1000" {
					tempDistance = "1km"
				} else if tempDistance == "2000" {
					tempDistance = "2km"
				} else {
					tempDistance = tempDistance + "m"
				}

				result["distance"] = tempDistance
				result["distanceValue"] = v["distance"].(string)

				// num, _ := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id =? and class = ? and div=? and boat_type =? order by id desc", register_eventId, tempClass, tempDiv, tempBoatType).Values(&maps1)
				// if num > 1 {
				// 	distanceString := ""
				// 	distanceValueString := ""

				// 	for index, v1 := range maps1 {
				// 		tempDistance := v1["distance"].(string)
				// 		if tempDistance == "1000" {
				// 			tempDistance = "1km"
				// 		} else if tempDistance == "2000" {
				// 			tempDistance = "2km"
				// 		} else {
				// 			tempDistance = tempDistance + "m"
				// 		}

				// 		distanceString += tempDistance
				// 		distanceValueString += v1["distance"].(string)
				// 		if (len(maps1) - 1) != index {
				// 			distanceString = distanceString + ", "
				// 			distanceValueString = distanceValueString + "_"
				// 		}
				// 	}
				// 	result["distance"] = distanceString
				// 	result["distanceValue"] = distanceValueString
				// } else {
				// 	tempDistance := v["distance"].(string)
				// 	if tempDistance == "1000" {
				// 		tempDistance = "1km"
				// 	} else if tempDistance == "2000" {
				// 		tempDistance = "2km"
				// 	} else {
				// 		tempDistance = tempDistance + "m"
				// 	}

				// 	result["distance"] = tempDistance
				// 	result["distanceValue"] = v["distance"].(string)
				// }
			} else {
				result["class"] = v["class"].(string)
				result["div"] = v["div"].(string)
				result["boat_type"] = ""
				result["distance"] = ""
				result["distanceValue"] = ""
			}

			result["lastrosterid"] = GetRosterSubmittedIdByClass_Division_EventId(register_eventId, tempClass, tempDiv, tempBoatType, result["distanceValue"])
			num, _ := o.Raw("SELECT div_name FROM all_divisions ad  WHERE ad.value =? ", tempDiv).Values(&maps1)
			if num > 0 {
				result["div_name"] = maps1[0]["div_name"].(string)
			} else {
				result["div_name"] = ""
			}
			resultList = append(resultList, result)
			result = make(map[string]string)
			//}
		}
	}

	return resultList
}

func SubitRosterLaterToEventByRegisterEventId(data map[string]string, selectedDivisionArray []string, selectedClassArray []string, tempSelectedRosterIdArray []string, selectedDistanceArray []string) bool {
	result := true
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	for index, RosterIdValue := range tempSelectedRosterIdArray {
		if RosterIdValue != "" {
			tempSelectedDivisionArray := strings.Split(selectedDivisionArray[index], "_")
			if len(tempSelectedDivisionArray) > 1 {
				tempSelectedDistanceArray := strings.Split(selectedDistanceArray[index], "_")
				for _, distanceValue := range tempSelectedDistanceArray {

					num, err := o.Raw("SELECT * FROM event_register_roster   WHERE fk_event_reg_id =? and division =? and clases =? and boat_type=? and distance=? ", data["RegisterEventId"], tempSelectedDivisionArray[0], selectedClassArray[index], tempSelectedDivisionArray[1], distanceValue).Values(&maps1)
					if num > 0 {
						_, err = o.Raw("UPDATE event_register_roster SET roster_id = ? where id=?", RosterIdValue, maps1[0]["id"].(string)).Values(&maps)

					} else {
						_, err = o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases, boat_type, distance) VALUES (?,?,?,?,?,?)", data["RegisterEventId"], RosterIdValue, tempSelectedDivisionArray[0], selectedClassArray[index], tempSelectedDivisionArray[1], distanceValue).Values(&maps)
					}

					if err == nil {
						_, _ = o.Raw("UPDATE team_roster SET type = ?, fk_event_id = ?, fk_practice_id = ? where id=?", "Event", data["EventId"], 0, RosterIdValue).Values(&maps)
						result = true
					} else {
						result = false
					}
				}
			} else {

				num, err := o.Raw("SELECT * FROM event_register_roster   WHERE fk_event_reg_id =? and division =? and clases =? ", data["RegisterEventId"], tempSelectedDivisionArray[0], selectedClassArray[index]).Values(&maps1)
				if num > 0 {
					_, err = o.Raw("UPDATE event_register_roster SET roster_id = ? where id=?", RosterIdValue, maps1[0]["id"].(string)).Values(&maps)
				} else {
					_, err = o.Raw("INSERT INTO event_register_roster (fk_event_reg_id, roster_id, division, clases) VALUES (?,?,?,?)", data["RegisterEventId"], RosterIdValue, tempSelectedDivisionArray[0], selectedClassArray[index]).Values(&maps)
				}
				if err == nil {
					_, err = o.Raw("UPDATE team_roster SET type = ?, fk_event_id = ?, fk_practice_id = ? where id=?", "Event", data["EventId"], 0, RosterIdValue).Values(&maps)
					result = true
				} else {
					result = false
				}
			}
		}
	}

	return result
}

func GetRegisterEventDetailByRegEventId(regeventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("SELECT * FROM event_register WHERE id = ? ", regeventId).Values(&maps)

	return maps
}

func GetRegisterRosterDetails(regeventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params
	_, _ = o.Raw("SELECT * FROM event_register_roster WHERE fk_event_reg_id = ? ", regeventId).Values(&maps)

	return maps
}

func CheckUserIsEventOrCoEventOrganizer(eventId string, userId string) (bool, bool) {
	o := orm.NewOrm()

	result := false
	result1 := false

	var maps []orm.Params
	var maps1 []orm.Params

	num, _ := o.Raw("SELECT * FROM event WHERE fk_organizer_id =? and id=? ", userId, eventId).Values(&maps)
	if num > 0 {
		result = true
	}

	num, err := o.Raw("SELECT * FROM event_organizers WHERE request_status =? and active =? and fk_organizer_id =? and fk_event_id=? ", 2, 1, userId, eventId).Values(&maps1)
	fmt.Println(err)
	if num > 0 {
		result1 = true
	}

	return result, result1
}

func GetEventPricingFee(eventId string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM pricing_fee WHERE fk_event_id =? order by id asc", eventId).Values(&maps)

	return maps
}

func GetRegisterEventPricingFee(regEventId string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event_register_role_pricing_quantity WHERE fk_event_reg_id = ? order by id asc ", regEventId).Values(&maps)

	return maps
}

func GetTotalRoleQuantityItemList(eventRolePricingDetail []orm.Params, registeEventRolePricingDetail []orm.Params) ([]map[string]string, float64, int64) {
	resultList := []map[string]string{}
	totalOrderCost := 0.00
	totalQuantityCost := 0
	result := make(map[string]string)
	for _, v := range eventRolePricingDetail {
		itemIdQuantity := "0"
		for _, v1 := range registeEventRolePricingDetail {
			if v["id"].(string) == v1["role_iteam_id"].(string) {
				itemIdQuantity = v1["role_iteam_id_quantity"].(string)
				result["role_iteam_id"] = v1["role_iteam_id"].(string)
				break
			}
		}
		result["role_name"] = v["price_fee_role"].(string)
		result["iteam_cost"] = v["regular_cost"].(string)
		result["itemId_Quantity"] = itemIdQuantity
		result["itemId_Total"] = "0.00"

		f, err := strconv.ParseFloat(v["regular_cost"].(string), 64)
		i, err2 := strconv.Atoi(itemIdQuantity)
		totalQuantityCost = totalQuantityCost + i
		if err == nil && err2 == nil {
			tempf1 := float64(i)
			totalCost := tempf1 * f
			totalOrderCost = totalOrderCost + totalCost
			result["itemId_Total"] = strconv.FormatFloat(totalCost, 'f', -1, 64)
		}
		resultList = append(resultList, result)
		result = make(map[string]string)
	}
	return resultList, totalOrderCost, int64(totalQuantityCost)
}
func GetUserName(userId string) string {
	o := orm.NewOrm()
	var userName = ""
	var maps []orm.Params
	num, _ := o.Raw("select name,last_name from user_profile where fk_user_id=?", userId).Values(&maps)
	if num > 0 {
		userName = maps[0]["name"].(string) + " " + maps[0]["last_name"].(string)
	}

	return userName

}

func UpdateEventOrderEditDetail(PerPersonIteamId []string, PerPersonQuantity []string, AdditionalQuantity string, IteamIdQuantity []string, IteamId []string, registerEventId string, Ordernote string, notificationStatus bool, eventSlug string, tempuserId string, TeamId string, changeBy string) bool {

	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	result := true
	result1 := ""
	changestatus := false

	personPricingTotal := new(big.Rat)
	personPricingTotal.SetString("0")
	teampTotalPersonMemberCount := 0
	teampTotalPersonPricingOrderCount := 0.00

	eventDetail := GetEvent_Slug_Event_Name_EventId_BySlug(eventSlug)

	price_based := ""

	tempeventId := ""

	if eventDetail != nil {

		if eventDetail[0]["price_based"] != nil {
			price_based = eventDetail[0]["price_based"].(string)
		}

		if eventDetail[0]["id"] != nil {
			tempeventId = eventDetail[0]["id"].(string)
		}
	}

	registerEventdetail := GetRegisterEventDetailByRegEventId(registerEventId)

	if price_based == "PP" {
		for index, perPersonIteamIdValue := range PerPersonIteamId {

			if perPersonIteamIdValue != "" && result {
				num, err := o.Raw("SELECT role_iteam_id_quantity FROM event_register_role_pricing_quantity WHERE role_iteam_id=? and fk_event_reg_id = ? order by id desc limit 1 ", perPersonIteamIdValue, registerEventId).Values(&maps1)
				if num > 0 {
					if maps1[0]["role_iteam_id_quantity"].(string) != PerPersonQuantity[index] {
						_, err = o.Raw("UPDATE event_register_role_pricing_quantity SET role_iteam_id_quantity = ? where role_iteam_id=? and fk_event_reg_id=? RETURNING role_iteam_id_quantity", PerPersonQuantity[index], perPersonIteamIdValue, registerEventId).Values(&maps)
						changestatus = true
						if err == nil {
							_, err = o.Raw("SELECT price_fee_role FROM pricing_fee WHERE id=? limit 1 ", perPersonIteamIdValue).Values(&maps2)
							if maps != nil && maps2 != nil {
								nameText := ""
								if maps2[0]["price_fee_role"] != nil {
									nameText = maps2[0]["price_fee_role"].(string)
									nameText = strings.Replace(nameText, "{", "", -1)
									nameText = strings.Replace(nameText, "}", "", -1)
									nameText = strings.Replace(nameText, `"`, "", -1)
								}

								result1, teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount = GetPerPetrsonTotalCount(tempeventId, perPersonIteamIdValue, PerPersonQuantity[index], teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount, "true")
								if result1 == "false" {
									result = false
									break
								}
								_, err = o.Raw("INSERT INTO event_register_order_change_logs (fk_event_reg_id, item_id, item_quantity, notes, updated_at, item_old_quantity, change_by, fk_user_id, name) VALUES (?,?,?,?,?,?,?,?,?)", registerEventId, perPersonIteamIdValue, maps[0]["role_iteam_id_quantity"].(string), Ordernote, time.Now(), maps1[0]["role_iteam_id_quantity"].(string), changeBy, tempuserId, nameText).Values(&maps)
							}
						} else {
							result = false
						}
					} else {
						result1, teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount = GetPerPetrsonTotalCount(tempeventId, perPersonIteamIdValue, PerPersonQuantity[index], teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount, "true")
						if result1 == "false" {
							result = false
							break
						}
					}
				} else if num == 0 && err == nil {
					_, err := o.Raw("INSERT INTO event_register_role_pricing_quantity (fk_event_reg_id, role_iteam_id, role_iteam_id_quantity) VALUES (?,?,?)", registerEventId, perPersonIteamIdValue, PerPersonQuantity[index]).Values(&maps2)
					if err != nil {
						result = false
						break
					} else {
						result1, teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount = GetPerPetrsonTotalCount(tempeventId, perPersonIteamIdValue, PerPersonQuantity[index], teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount, "true")
						if result1 == "false" {
							result = false
							break
						}
					}
				}
			}
		}
	} else {

		partisipantcount := "0"

		if registerEventdetail != nil {
			if registerEventdetail[0]["participant_member_count"] != nil {
				partisipantcount = registerEventdetail[0]["participant_member_count"].(string)
			}
		}
		i, err2 := strconv.Atoi(partisipantcount)
		if err2 == nil {
			teampTotalPersonMemberCount = i
		} else {
			result = false
		}
	}
	personPricingTotal.SetFloat64(teampTotalPersonPricingOrderCount)

	additionalPricingOtherTotal := new(big.Rat)
	additionalPricingOtherTotal.SetString("0")
	teampTotalPricingOtherOrderCount := 0.00

	for index, iteamIdValue := range IteamId {

		if iteamIdValue != "" && result {

			num, err := o.Raw("SELECT iteam_id_quantity FROM event_register_pricing_other WHERE iteam_id=? and fk_event_reg_id = ? order by id desc limit 1", iteamIdValue, registerEventId).Values(&maps1)

			if num > 0 {
				if maps1[0]["iteam_id_quantity"].(string) != IteamIdQuantity[index] {
					_, err = o.Raw("UPDATE event_register_pricing_other SET iteam_id_quantity =  ? where iteam_id=? and fk_event_reg_id=? RETURNING iteam_id_quantity", IteamIdQuantity[index], iteamIdValue, registerEventId).Values(&maps)
					changestatus = true
					if err == nil {
						_, err = o.Raw("SELECT item FROM pricing_other WHERE id=? limit 1 ", iteamIdValue).Values(&maps2)

						result1, teampTotalPricingOtherOrderCount = GetOtherIteamTotal(tempeventId, iteamIdValue, IteamIdQuantity[index], teampTotalPricingOtherOrderCount, "true")
						if result1 == "false" {
							result = false
							break
						}
						if maps != nil && maps2 != nil {
							nameText := ""
							if maps2[0]["item"] != nil {
								nameText = maps2[0]["item"].(string)
							}
							_, err = o.Raw("INSERT INTO event_register_order_change_logs (fk_event_reg_id, item_id, item_quantity, notes, updated_at, item_old_quantity, change_by, fk_user_id, name) VALUES (?,?,?,?,?,?,?,?,?)", registerEventId, iteamIdValue, maps[0]["iteam_id_quantity"].(string), Ordernote, time.Now(), maps1[0]["iteam_id_quantity"].(string), changeBy, tempuserId, nameText).Values(&maps)
						}
					} else {
						result = false
					}
				} else {
					result1, teampTotalPricingOtherOrderCount = GetOtherIteamTotal(tempeventId, iteamIdValue, IteamIdQuantity[index], teampTotalPricingOtherOrderCount, "true")
					if result1 == "false" {
						result = false
						break
					}
				}
			} else {
				_, _ = o.Raw("INSERT INTO event_register_pricing_other (fk_event_reg_id, iteam_id, iteam_id_quantity) VALUES (?,?,?)", registerEventId, iteamIdValue, IteamIdQuantity[index]).Values(&maps)
				result1, teampTotalPricingOtherOrderCount = GetOtherIteamTotal(tempeventId, iteamIdValue, IteamIdQuantity[index], teampTotalPricingOtherOrderCount, "true")
				if result1 == "false" {
					result = false
					break
				}
			}

		}
	}

	additionalPricingOtherTotal.SetFloat64(teampTotalPricingOtherOrderCount)

	additionalQuantityTotal := new(big.Rat)
	additionalQuantityTotal.SetString("0")

	if result && AdditionalQuantity != "0" && AdditionalQuantity != "" {

		num, err := o.Raw("SELECT additional_practice_quantity FROM event_register WHERE id = ? ", registerEventId).Values(&maps1)
		if num > 0 {
			if maps1[0]["additional_practice_quantity"].(string) != AdditionalQuantity {
				_, err = o.Raw("UPDATE event_register SET additional_practice_bool = ? , additional_practice_quantity = ? where id=? RETURNING additional_practice_quantity ", true, AdditionalQuantity, registerEventId).Values(&maps)
				changestatus = true
				if err == nil {
					if maps != nil {
						result1, additionalQuantityTotal = GetAdditionalPracticeTotal(tempeventId, additionalQuantityTotal, "true", AdditionalQuantity)
						if result1 == "false" {
							result = false
						}
						_, err = o.Raw("INSERT INTO event_register_order_change_logs (fk_event_reg_id, item_quantity, notes, name, updated_at, change_by, fk_user_id, item_old_quantity) VALUES (?,?,?,?,?,?,?,?)", registerEventId, maps[0]["additional_practice_quantity"].(string), Ordernote, "Additional Practice", time.Now(), changeBy, tempuserId, maps1[0]["additional_practice_quantity"].(string)).Values(&maps)
					}
				} else {
					result = false
				}
			} else {
				result1, additionalQuantityTotal = GetAdditionalPracticeTotal(tempeventId, additionalQuantityTotal, "true", AdditionalQuantity)
				if result1 == "false" {
					result = false
				}
			}
		}
	}

	if result {
		result1, _ = Insert_UpdateTodalEventRegisterPaymentDetail(registerEventId, TeamId, tempeventId, additionalQuantityTotal, additionalPricingOtherTotal, personPricingTotal, teampTotalPersonMemberCount)
		if result1 == "false" {
			result = false
		} else {
			result = true
		}

	}

	if notificationStatus && result && changestatus {
		event_view := GetEvent_Slug_Event_Name_EventId_BySlug(eventSlug)
		teamDetail := GetTeamDetailByTeamId(TeamId)

		if event_view != nil && teamDetail != nil {
			if teamDetail[0]["fk_captain_id"].(string) != tempuserId {
				site_url := ""
				if len(os.Getenv("SITE_URL")) > 0 {
					site_url = os.Getenv("SITE_URL")
				} else {
					site_url = "http://localhost:5000/"
				}

				eventUrl := "<a href='" + site_url + "/event/team-view/" + event_view[0]["slug"].(string) + "/" + registerEventId + "'>" + event_view[0]["event_name"].(string) + "</a>"
				desc := "Your registration details have been updated for " + eventUrl + "."

				_, err := o.Raw("INSERT INTO notification_list (actions, active, date, new_build_in_msg, notification_desc, type, fk_event_id, fk_initiator_id, fk_receiver ) VALUES (?,?,?,?,?,?,?,?,?) ", "{9}", "1", time.Now(), true, desc, "9", event_view[0]["id"].(string), tempuserId, "{"+teamDetail[0]["fk_captain_id"].(string)+"}").Values(&maps)
				fmt.Println(err)
			}
		}
	}

	return result
}

func GetEvent_Slug_Event_Name_EventId_BySlug(slug string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event WHERE slug = ? ", slug).Values(&maps)

	return maps
}

func GetRegEventOrderLogByEventRegId(register_eventId string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT to_char(updated_at, 'MM/DD/YYYY at HH:MI am') as datetimestamp, * FROM event_register_order_change_logs WHERE fk_event_reg_id = ? order by id desc ", register_eventId).Values(&maps)

	return maps
}
func GetEventAdditionalPraticePriceByEventId(eventId string) ([]orm.Params, error) {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT * FROM event_practice_pricing WHERE fk_event_id = ? and type= ?", eventId, "additional").Values(&maps)

	return maps, err
}

func GetEventPricingOtherDetailByEventId(eventId string, iteamId string) ([]orm.Params, error) {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT cost FROM pricing_other WHERE fk_event_id = ? and id= ?", eventId, iteamId).Values(&maps)

	return maps, err
}

func GetEventPersonPriceByEventId(eventId string, iteamId string) ([]orm.Params, error) {

	o := orm.NewOrm()

	var maps []orm.Params

	_, err := o.Raw("SELECT regular_cost FROM pricing_fee WHERE fk_event_id = ? and id= ?", eventId, iteamId).Values(&maps)

	return maps, err
}

func Insert_UpdateTodalEventRegisterPaymentDetail(fk_event_reg_id string, teamId string, eventId string, total_additionalQuantityTotal *big.Rat, total_additionalPricingOtherTotal *big.Rat, total_personPricingTotal *big.Rat, teampTotalPersonMemberCount int) (string, string) {

	o := orm.NewOrm()

	var maps3 []orm.Params
	var maps4 []orm.Params

	result := "true"
	tempcurrentTotalAmount := "0"
	totalAmount := new(big.Rat).Add(total_additionalQuantityTotal, total_additionalPricingOtherTotal)
	totalAmount.Add(totalAmount, total_personPricingTotal)

	total_addit_practice_other_item := new(big.Rat).Add(total_additionalPricingOtherTotal, total_additionalQuantityTotal)

	teamMemberCount := new(big.Rat).SetInt64(int64(teampTotalPersonMemberCount))

	total_gushou_fee := new(big.Rat).Mul(teamMemberCount, new(big.Rat).SetInt64(int64(constants.GUSHOUFEE)))

	mode := big.ToNearestEven

	tempInsertTotalAmount := new(big.Rat).Add(totalAmount, total_gushou_fee)

	temptotalAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(tempInsertTotalAmount)

	temptotal_eventreceive := new(big.Float).SetPrec(0).SetMode(mode).SetRat(totalAmount)

	temptotal_addit_practice_other_item := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_addit_practice_other_item)

	temptotal_gushou_fee := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_gushou_fee)

	num, err := o.Raw("SELECT * FROM event_register_payments WHERE fk_event_reg_id = ? and fk_event_id= ? and fk_team_id= ?", fk_event_reg_id, eventId, teamId).Values(&maps3)

	if err == nil {
		if num > 0 {
			currentTotalAmount, currentPaidAmount, currentEventrefundAmount, currentgushoutotalamount, currentGushoureceiveAmount, currentGushourefundAmount, currentEventReceivedAmount := GetTotalPaymentAmount(totalAmount, total_gushou_fee, maps3)
			tempcurrentTotalAmount = currentTotalAmount
			//changefee
			total_changee_amount := new(big.Rat)
			total_changee_amount.SetString("0")

			valuezero := new(big.Rat)
			valuezero.SetString("0")

			if maps3[0]["changefee"] != nil {
				total_changee_amount.SetString(maps3[0]["changefee"].(string))
			}

			if total_changee_amount.Cmp(valuezero) == 1 {
				_, err = o.Raw("UPDATE event_register_payments SET total_amount = ? + changefee  ,total_paid = ?, total_refund = ?, total_event_fee = ? + changefee, total_addit_practice_other_item = ?,total_gushou_fee = ?, total_gushou_refund = ?, total_gushou_recevie = ?, last_updated_at = ?,total_event_receive=?  where fk_event_reg_id=? ", currentTotalAmount, currentPaidAmount, currentEventrefundAmount, temptotal_eventreceive.String(), temptotal_addit_practice_other_item.String(), currentgushoutotalamount, currentGushourefundAmount, currentGushoureceiveAmount, time.Now(), currentEventReceivedAmount, fk_event_reg_id).Values(&maps4)
				if err != nil {
					result = "false"
				}
			} else {
				_, err = o.Raw("UPDATE event_register_payments SET total_amount = ?,total_paid = ?, total_refund = ?, total_event_fee = ?, total_addit_practice_other_item = ?,total_gushou_fee = ?, total_gushou_refund = ?, total_gushou_recevie = ?, last_updated_at = ?,total_event_receive=?  where fk_event_reg_id=? ", currentTotalAmount, currentPaidAmount, currentEventrefundAmount, temptotal_eventreceive.String(), temptotal_addit_practice_other_item.String(), currentgushoutotalamount, currentGushourefundAmount, currentGushoureceiveAmount, time.Now(), currentEventReceivedAmount, fk_event_reg_id).Values(&maps4)
				if err != nil {
					result = "false"
				}
			}

		} else {

			tempcurrentTotalAmount = temptotalAmount.String()
			_, err1 := o.Raw("INSERT INTO event_register_payments (fk_event_reg_id, fk_event_id, fk_team_id, total_amount, total_paid, total_refund, total_event_fee, total_addit_practice_other_item, total_gushou_fee, total_gushou_recevie, last_updated_at, total_gushou_refund, total_event_receive,discount, changefee) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", fk_event_reg_id, eventId, teamId, temptotalAmount.String(), 0, 0, temptotal_eventreceive.String(), temptotal_addit_practice_other_item.String(), temptotal_gushou_fee.String(), 0, time.Now(), 0, 0, "", 0).Values(&maps4)
			fmt.Println(err)
			if err1 != nil {
				result = "false"
			}
		}
	} else {
		result = "false"
	}
	return result, tempcurrentTotalAmount
}

func GetTotalPaymentAmount(eventtotalAmount *big.Rat, total_gushou_fee *big.Rat, register_payments []orm.Params) (string, string, string, string, string, string, string) {

	totalPaidAmount, totalRefundAmount, totalGushouAmount, totalGushouReceivedAmount, temptotal_gushou_refund, totalGushouComissionFee := "0", "0", "0", "0", "0", "0"

	currentTotalAmount, currentEventrefundAmount, currentgushoutotalamount, currentGushoureceiveAmount, currentGushourefundAmount, currentEventReceivedAmount := new(big.Rat), new(big.Rat), new(big.Rat), new(big.Rat), new(big.Rat), new(big.Rat)

	if register_payments != nil {
		if register_payments[0]["total_paid"] != nil {
			totalPaidAmount = register_payments[0]["total_paid"].(string)
		}

		if register_payments[0]["total_refund"] != nil {
			totalRefundAmount = register_payments[0]["total_refund"].(string)
		}

		if register_payments[0]["total_gushou_fee"] != nil {
			totalGushouAmount = register_payments[0]["total_gushou_fee"].(string)
		}

		if register_payments[0]["total_gushou_recevie"] != nil {
			totalGushouReceivedAmount = register_payments[0]["total_gushou_recevie"].(string)
		}

		if register_payments[0]["total_gushou_refund"] != nil {
			temptotal_gushou_refund = register_payments[0]["total_gushou_refund"].(string)
		}

		if register_payments[0]["gushou_commision_fee"] != nil {
			totalGushouComissionFee = register_payments[0]["gushou_commision_fee"].(string)
		}
	}

	totalValue := new(big.Rat)
	totalValue.SetString("0")

	totalPaidAmountRat := new(big.Rat)
	totalPaidAmountRat.SetString(totalPaidAmount)

	totalRefundAmountRat := new(big.Rat)
	totalRefundAmountRat.SetString(totalRefundAmount)

	totalGushouAmountRat := new(big.Rat)
	totalGushouAmountRat.SetString(totalGushouAmount)

	temptotal_gushou_refundRat := new(big.Rat)
	temptotal_gushou_refundRat.SetString(temptotal_gushou_refund)

	totalGushouReceivedAmountRat := new(big.Rat)
	totalGushouReceivedAmountRat.SetString(totalGushouReceivedAmount)

	tempEventReceived := new(big.Rat).Sub(totalPaidAmountRat, totalGushouReceivedAmountRat)

	tempGushouComissionFee := new(big.Rat)
	tempGushouComissionFee.SetString(totalGushouComissionFee)

	if eventtotalAmount.Cmp(tempEventReceived) == 1 {

		if totalRefundAmountRat.Cmp(totalValue) == 1 {
			totalRefundAmountRat.SetString("0")
		}

	} else if eventtotalAmount.Cmp(tempEventReceived) == -1 {
		temprefundReceived := new(big.Rat).Sub(tempEventReceived, eventtotalAmount)
		totalRefundAmountRat.SetString("0")
		totalRefundAmountRat.Add(totalRefundAmountRat, temprefundReceived)
	}

	if total_gushou_fee.Cmp(totalGushouReceivedAmountRat) == 1 {

		if temptotal_gushou_refundRat.Cmp(totalValue) == 1 {
			temptotal_gushou_refundRat.SetString("0")
		}
	} else if total_gushou_fee.Cmp(totalGushouReceivedAmountRat) == -1 {
		tempgushourefundReceived := new(big.Rat).Sub(totalGushouReceivedAmountRat, total_gushou_fee)
		temptotal_gushou_refundRat.SetString("0")
		temptotal_gushou_refundRat.Add(temptotal_gushou_refundRat, tempgushourefundReceived)
	}

	currentTotalAmount.Add(eventtotalAmount, total_gushou_fee)

	currentEventrefundAmount = totalRefundAmountRat

	currentgushoutotalamount = total_gushou_fee

	currentGushoureceiveAmount = totalGushouReceivedAmountRat

	currentGushourefundAmount = temptotal_gushou_refundRat

	currentEventReceivedAmount = tempEventReceived

	currentEventReceivedAmount.Sub(currentEventReceivedAmount, tempGushouComissionFee)

	mode := big.ToNearestEven
	tempcurrentTotalAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentTotalAmount)
	tempcurrentPaidAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(totalPaidAmountRat)
	tempcurrentEventrefundAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentEventrefundAmount)
	tempcurrentgushoutotalamount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentgushoutotalamount)
	tempcurrentGushoureceiveAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentGushoureceiveAmount)
	tempcurrentGushourefundAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentGushourefundAmount)
	tempcurrentEventReceivedAmount := new(big.Float).SetPrec(0).SetMode(mode).SetRat(currentEventReceivedAmount)

	return tempcurrentTotalAmount.String(), tempcurrentPaidAmount.String(), tempcurrentEventrefundAmount.String(), tempcurrentgushoutotalamount.String(), tempcurrentGushoureceiveAmount.String(), tempcurrentGushourefundAmount.String(), tempcurrentEventReceivedAmount.String()
}

func GetTotalRemaningPaidAmount(regeventId string) (string, string, string) {

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT total_amount - total_paid as remaamount, te.team_name FROM event_register_payments as erp left join team te on te.id = erp.fk_team_id  WHERE fk_event_reg_id = ? ", regeventId).Values(&maps)

	remaamount := "0.00"
	team_name := ""
	remaamount1 := "0.00"
	if num > 0 {
		if maps[0]["remaamount"] != nil {
			remaamount = maps[0]["remaamount"].(string)
			remaamount1 = maps[0]["remaamount"].(string)
			remaamount = common.ConvertAmountIntoFormat(remaamount)
		}

		if maps[0]["team_name"] != nil {
			team_name = maps[0]["team_name"].(string)
		}
	}

	return remaamount, team_name, remaamount1
}

func GetEvent_By_EventId(eventid string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event WHERE id = ? ", eventid).Values(&maps)

	return maps
}

func GetEventRegisterTeamPaymentId(registerEventId string) string {
	o := orm.NewOrm()
	result := ""
	var maps []orm.Params

	num, _ := o.Raw("SELECT * FROM event_register_payments WHERE fk_event_reg_id = ? ", registerEventId).Values(&maps)

	if num > 0 {
		if maps[0]["id"] != nil {
			result = maps[0]["id"].(string)
		}
	}

	return result

}
func GetAdditionalPracticeTotal(EventRegisterEventid string, additionalQuantityTotal *big.Rat, result string, additionalPracticeQuantity string) (string, *big.Rat) {
	addtionalPrice, addtionalPriceerror := GetEventAdditionalPraticePriceByEventId(EventRegisterEventid)
	if addtionalPriceerror == nil {
		if addtionalPrice != nil {
			if addtionalPrice[0]["price"] != nil {
				tempaddtionalPrice := addtionalPrice[0]["price"].(string)

				addtionalPriceTotalRat, additionalQuantityTotalRat := new(big.Rat), new(big.Rat)

				addtionalPriceTotalRat.SetString(tempaddtionalPrice)
				additionalQuantityTotalRat.SetString(additionalPracticeQuantity)
				additionalQuantityTotal.Mul(addtionalPriceTotalRat, additionalQuantityTotalRat)

			}
		}
	} else {
		result = "false"
	}

	return result, additionalQuantityTotal
}

func GetOtherIteamTotal(eventRegisterEventid string, iteamIdValue string, iteamIdQuantity string, teampTotalPricingOtherOrderCount float64, result string) (string, float64) {
	pricingOtherCost, pricingOtherCostError := GetEventPricingOtherDetailByEventId(eventRegisterEventid, iteamIdValue)

	if pricingOtherCostError == nil {
		teamppricingOtherCost := "0"
		if pricingOtherCost != nil {
			if pricingOtherCost[0]["cost"] != nil {
				teamppricingOtherCost = pricingOtherCost[0]["cost"].(string)
			}
		}
		f, err := strconv.ParseFloat(teamppricingOtherCost, 64)
		f1, err1 := strconv.ParseFloat(iteamIdQuantity, 64)

		if err == nil && err1 == nil {
			totalCost := f * f1
			teampTotalPricingOtherOrderCount = teampTotalPricingOtherOrderCount + totalCost
		} else {
			result = "false"
		}
	} else {
		result = "false"
	}

	return result, teampTotalPricingOtherOrderCount
}

func GetPerPetrsonTotalCount(eventRegisterEventid string, PerPersonIteamIdValue string, PerPersonQuantity string, teampTotalPersonPricingOrderCount float64, teampTotalPersonMemberCount int, result string) (string, float64, int) {
	pricingPerPersonCost, pricingPerPersonCostErr := GetEventPersonPriceByEventId(eventRegisterEventid, PerPersonIteamIdValue)
	if pricingPerPersonCostErr == nil {

		teamppricingPerPerson := "0"
		if pricingPerPersonCost != nil {
			if pricingPerPersonCost[0]["regular_cost"] != nil {
				teamppricingPerPerson = pricingPerPersonCost[0]["regular_cost"].(string)
			}
		}
		f, err := strconv.ParseFloat(teamppricingPerPerson, 64)
		f1, err1 := strconv.ParseFloat(PerPersonQuantity, 64)

		if err == nil && err1 == nil {
			totalCost := f * f1
			teampTotalPersonPricingOrderCount = teampTotalPersonPricingOrderCount + totalCost
			i, err2 := strconv.Atoi(PerPersonQuantity)
			if err2 == nil {
				teampTotalPersonMemberCount = teampTotalPersonMemberCount + i
			} else {
				result = "false"
			}
		} else {
			result = "false"
		}

	} else {
		result = "false"
	}

	return result, teampTotalPersonPricingOrderCount, teampTotalPersonMemberCount
}

func GetRegisterEventTeamPaymentDetail(teamId string) []map[string]string {

	result := make(map[string]string)

	resultList := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params
	dte := time.Now()
	tempCurrentFormatDate := dte.Format("2006-01-02")
	//and ev.end_date >=?
	num, err := o.Raw("select CASE WHEN ev.end_date IS NULL THEN '"+tempCurrentFormatDate+"' <= ev.start_date WHEN '"+tempCurrentFormatDate+"' <= ev.end_date THEN 't' ELSE 'f' end as eventenddatefalse,ev.end_date,ev.start_date,te.slug as teamslug,(erp.total_amount - erp.total_paid) ::money::numeric::float8 as balance, ev.fk_organizer_id as eventcaptain, ev.id as eventid, ev.event_name, ev.slug as eventslug ,ev.currency as eventcurrency, erp.fk_event_reg_id as evenrregid, erp.id as orderid, erp.total_paid::money::numeric::float8, (erp.total_amount - erp.total_paid) ::money::numeric::float8 as balance  , CASE WHEN erp.total_paid = 0 THEN 'unpaid' WHEN erp.total_amount > erp.total_paid THEN 'partial' WHEN erp.total_amount = erp.total_paid THEN 'paid'  end as paymentstatus, (total_amount - total_paid) ::money::numeric::float8 as zerobalance, discount as discountvalue  from event_register_payments  as erp left join event_register as er on er.id=erp.fk_event_reg_id   left join event as ev on ev.id = erp.fk_event_id left join team as te on te.id = erp.fk_team_id where erp.fk_team_id=?  and er.active= 1 and er.registration_status =? and er.reg_cancel_status =? order by ev.id desc", teamId, "Success", false).Values(&maps)

	fmt.Println(err)
	if num > 0 {

		for _, v := range maps {
			result["total_paid"] = "-"
			if v["total_paid"] != nil {
				result["total_paid"] = common.ConvertAmountIntoFormat(v["total_paid"].(string))
			}

			result["balance"] = "-"
			if v["balance"] != nil {
				result["balance"] = common.ConvertAmountIntoFormat(v["balance"].(string))
			}

			result["eventenddatefalse"] = "-"
			if v["eventenddatefalse"] != nil {
				result["eventenddatefalse"] = v["eventenddatefalse"].(string)
			}

			result["event_name"] = v["event_name"].(string)
			result["orderid"] = v["orderid"].(string)
			if v["paymentstatus"] != nil {
				result["paymentstatus"] = v["paymentstatus"].(string)
			}
			result["transactionhistory"] = GetLastTransactionHistoryByRegPayId(v["orderid"].(string))
			result["eventslug"] = v["eventslug"].(string)
			result["evenrregid"] = v["evenrregid"].(string)
			result["eventcurrency"] = v["eventcurrency"].(string)
			result["eventcurrency"] = common.ConvertUnitToCurrencySynbol(v["eventcurrency"].(string))

			result["paymentoption"] = "false"

			if v["eventid"] != nil {
				result["paymentoption"] = CheckStripeOptionEnableOrNotByEventOrg(v["eventid"].(string))
			}

			result["zerobalancecheck"] = "false"
			if v["zerobalance"].(string) == "0" {
				result["zerobalancecheck"] = "true"
			}

			if v["orderid"] != nil {

				order_id := v["orderid"].(string)
				num1, err1 := o.Raw("select id as invoice_no, invoicepath from event_reg_payment_log where fk_event_reg_pay = ? order by id desc limit 1", order_id).Values(&maps1)
				fmt.Println(err1)
				if num1 > 0 {
					if maps1[0]["invoice_no"] != nil {
						result["invoice_no"] = maps1[0]["invoice_no"].(string)
					}

					if maps1[0]["invoicepath"] != nil {
						result["invoicepath"] = maps1[0]["invoicepath"].(string)
					}
				}
			}

			if v["discountvalue"] != nil {
				result["discountvalue"] = v["discountvalue"].(string)
			}
			if v["eventid"] != nil {

				result["eventid"] = v["eventid"].(string)
			}
			if v["evenrregid"] != nil {
				num1, err1 := o.Raw("SELECT * FROM event_register_class_divisions  WHERE fk_event_reg_id = ? ", v["evenrregid"].(string)).Values(&maps2)
				fmt.Println(err1)
				_, err2 := o.Raw("SELECT CASE WHEN count(id)=0 THEN 'false' WHEN count(id) >=1  THEN 'true' END FROM event_register_roster  WHERE fk_event_reg_id = ? ", v["evenrregid"].(string)).Values(&maps4)
				fmt.Println(err2)
				if num1 > 0 && maps4[0]["case"].(string) == "true" {
					if maps2[0]["div"] != nil && maps2[0]["class"] != nil && maps2[0]["boat_type"] != nil && maps2[0]["distance"] != nil {
						_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.boat_type= tr.boat_type and err.distance= tr.distance and err.fk_event_reg_id= tr.fk_event_reg_id ) and tr.fk_event_reg_id = ? ", v["evenrregid"].(string)).Values(&maps3)
						fmt.Println(err1)
						result["roster_partial"] = maps3[0]["case"].(string)

					} else {
						_, err1 := o.Raw("select CASE WHEN count(id)=0 THEN 'true' WHEN count(id) >=1  THEN 'false' END from  event_register_class_divisions tr where not exists (select 1 from event_register_roster  err where err.division= tr.div and err.clases= tr.class and err.fk_event_reg_id= tr.fk_event_reg_id ) and tr.fk_event_reg_id = ? ", v["evenrregid"].(string)).Values(&maps3)
						fmt.Println(err1)
						result["roster_partial"] = maps3[0]["case"].(string)
					}
				} else {
					result["roster_partial"] = maps4[0]["case"].(string)
				}
			}
			if v["teamslug"] != nil {
				result["teamslug"] = v["teamslug"].(string)
			}

			resultList = append(resultList, result)
			result = make(map[string]string)
		}
	}
	return resultList
}

func GetRegisterEventTeamPaymentCount(teamId string) string {
	result := "0"

	o := orm.NewOrm()

	var maps []orm.Params

	dte := time.Now()
	tempCurrentFormatDate := dte.Format("2006-01-02")

	num, _ := o.Raw("SELECT  count(erp.fk_team_id) FROM event_register_payments as erp left join event_register as er on er.id=erp.fk_event_reg_id left join event ev on ev.id = erp.fk_event_id WHERE erp.fk_team_id = ? and ev.start_date >= ? and er.active= 1 and er.registration_status =? and er.reg_cancel_status =?", teamId, tempCurrentFormatDate, "Success", false).Values(&maps)

	if num > 0 {
		result = maps[0]["count"].(string)
	}
	//fmt.Println(result)

	return result
}
func GetEventPaymentList(eventId string, filterbyTeamName string, filter_paid string, filter_unpaid string, filter_partial string) ([]map[string]orm.Params, []map[string]orm.Params, []map[string]orm.Params) {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps1 []orm.Params
	var maps2 []orm.Params
	resultList := []map[string]orm.Params{}
	resultList_sortByPayStatus := []map[string]orm.Params{}
	resultList_sortByBalance := []map[string]orm.Params{}
	flag := ""
	sql := "SELECT *,te.slug as teamslug,to_char((total_amount - total_paid), 'FM999,999,999,990D00') as balance_formatted,CASE WHEN eve_pay.changefee < 0 THEN 'true' else 'false' end as changeefeedetail, CASE WHEN eve_pay.changefee < 0 THEN  to_char(eve_pay.total_event_receive + eve_pay.changefee , 'FM999,999,999,990D00') else to_char(eve_pay.total_event_receive , 'FM999,999,999,990D00') end as formatted_total_event_receive, to_char(eve_pay.total_amount, 'FM999,999,999,990D00') as formatted_total_amount,CASE WHEN eve_pay.total_paid = 0 THEN 'unpaid' WHEN eve_pay.total_amount > eve_pay.total_paid THEN 'partial' WHEN eve_pay.total_amount = eve_pay.total_paid THEN 'paid' end as paymentstatus,eve_pay.fk_event_id as pay_event_id,eve_pay.fk_team_id as pay_team_id,eve_pay.id as event_reg_pay_id,eve_pay.total_amount-eve_pay.total_paid as balance_amount,currency,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') asregisterdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id left JOIN event_register_payments eve_pay on er.id = eve_pay.fk_event_reg_id left JOIN event eve on er.fk_event_id = eve.id WHERE er.fk_event_id = ? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =?"
	fmt.Println("sql--", sql)

	if filterbyTeamName != "" {

		sql = sql + " and te.team_name ILIKE '%" + filterbyTeamName + "%'"
	}
	if filter_paid != "" || filter_unpaid != "" || filter_partial != "" {
		sql = sql + " and ("
	}
	if filter_paid != "" {
		flag = "1"
		sql = sql + "eve_pay.total_amount = eve_pay.total_paid"

	}
	if filter_unpaid != "" {

		if flag == "1" {
			sql = sql + " or eve_pay.total_paid = 0"
		} else {
			sql = sql + " eve_pay.total_paid = 0"
		}
		flag = "2"

	}
	if filter_partial != "" {

		if flag == "2" || flag == "1" {
			sql = sql + " or eve_pay.total_paid >= 1 and eve_pay.total_amount != eve_pay.total_paid"
		} else {
			sql = sql + " eve_pay.total_paid >= 1 and eve_pay.total_amount != eve_pay.total_paid"
		}
	}
	if filter_paid != "" || filter_unpaid != "" || filter_partial != "" {
		sql = sql + ")"
	}

	sql = sql + " order by er.id desc"
	fmt.Println(sql)
	num, err := o.Raw(sql, eventId, false).Values(&maps1)

	fmt.Println(err)

	if num > 0 {
		for _, v1 := range maps1 {
			result["registerEventPaymentData"] = v1
			if v1["event_reg_pay_id"] != nil {
				event_reg_pay_id := v1["event_reg_pay_id"].(string)
				num1, err1 := o.Raw("SELECT id as invoice_no, invoicepath  FROM event_reg_payment_log where fk_event_reg_pay = ? order by id desc  limit  1", event_reg_pay_id).Values(&maps2)
				fmt.Println(err1)
				if num1 > 0 {
					result["getinvoice_no"] = maps2[0]
				}
			}

			resultList = append(resultList, result)
			resultList_sortByPayStatus = append(resultList_sortByPayStatus, result)
			resultList_sortByBalance = append(resultList_sortByBalance, result)

			result = make(map[string]orm.Params)
		}

	}

	return resultList, resultList_sortByPayStatus, resultList_sortByBalance
}
func GetEventRegisterTeamDetail_sortByTeamName(eventId string, filterbyTeamName string, filter_paid string, filter_unpaid string, filter_partial string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps1 []orm.Params
	var maps2 []orm.Params
	resultList := []map[string]orm.Params{}

	flag := ""
	sql := "SELECT *,te.slug as teamslug ,to_char((total_amount - total_paid), 'FM999,999,999,990D00') as balance_formatted, CASE WHEN eve_pay.changefee < 0 THEN 'true' else 'false' end as changeefeedetail, CASE WHEN eve_pay.changefee < 0 THEN  to_char(eve_pay.total_event_receive + eve_pay.changefee , 'FM999,999,999,990D00') else to_char(eve_pay.total_event_receive , 'FM999,999,999,990D00') end as formatted_total_event_receive,to_char(eve_pay.total_amount, 'FM999,999,999,990D00') as formatted_total_amount,CASE WHEN eve_pay.total_paid = 0 THEN 'unpaid' WHEN eve_pay.total_amount > eve_pay.total_paid THEN 'partial' WHEN eve_pay.total_amount = eve_pay.total_paid THEN 'paid' end as paymentstatus,eve_pay.fk_event_id as pay_event_id,eve_pay.fk_team_id as pay_team_id,eve_pay.id as event_reg_pay_id,eve_pay.total_amount-eve_pay.total_paid as balance_amount,currency,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') asregisterdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id left JOIN event_register_payments eve_pay on er.id = eve_pay.fk_event_reg_id left JOIN event eve on er.fk_event_id = eve.id WHERE er.fk_event_id = ? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =?"
	fmt.Println("sql--", sql)

	if filterbyTeamName != "" {

		sql = sql + " and te.team_name ILIKE '%" + filterbyTeamName + "%'"
	}
	if filter_paid != "" || filter_unpaid != "" || filter_partial != "" {
		sql = sql + " and ("
	}
	if filter_paid != "" {
		flag = "1"
		sql = sql + "eve_pay.total_amount = eve_pay.total_paid"

	}
	if filter_unpaid != "" {

		if flag == "1" {
			sql = sql + " or eve_pay.total_paid = 0"
		} else {
			sql = sql + " eve_pay.total_paid = 0"
		}
		flag = "2"

	}
	if filter_partial != "" {

		if flag == "2" || flag == "1" {
			sql = sql + " or eve_pay.total_paid >= 1 and eve_pay.total_amount != eve_pay.total_paid"
		} else {
			sql = sql + " eve_pay.total_paid >= 1 and eve_pay.total_amount != eve_pay.total_paid"
		}
	}
	if filter_paid != "" || filter_unpaid != "" || filter_partial != "" {
		sql = sql + ")"
	}

	sql = sql + " order by te.team_name asc"
	fmt.Println(sql)
	num, err := o.Raw(sql, eventId, false).Values(&maps1)

	fmt.Println(err)

	if num > 0 {
		for _, v1 := range maps1 {
			result["registerEventPaymentData"] = v1
			if v1["event_reg_pay_id"] != nil {
				event_reg_pay_id := v1["event_reg_pay_id"].(string)
				num1, err1 := o.Raw("SELECT id as invoice_no FROM event_reg_payment_log where fk_event_reg_pay = ? order by id desc  limit  1", event_reg_pay_id).Values(&maps2)
				fmt.Println(err1)
				if num1 > 0 {
					result["getinvoice_no"] = maps2[0]
				}
			}

			resultList = append(resultList, result)

			result = make(map[string]orm.Params)
		}

	}

	return resultList
}
func EventOverallAmountCalculation(eventId string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params

	var maps4 []orm.Params

	resultList := []map[string]orm.Params{}

	num, err := o.Raw("select to_char(SUM (total_event_receive), 'FM999,999,999,990D00') as overall_eventreceive,to_char(SUM (total_amount), 'FM999,999,999,990D00') as overall_event_total , to_char(SUM (total_amount - total_paid), 'FM999,999,999,990D00') as overall_owed ,to_char(SUM (total_paid), 'FM999,999,999,990D00') as overall_event_paid,to_char(SUM (total_refund), 'FM999,999,999,990D00') as overall_event_refund,to_char(SUM (total_gushou_recevie), 'FM999,999,999,990D00')  as offline_gushou_received from event_register_payments as erp left join event_register as er on er.id =  erp.fk_event_reg_id where erp.fk_event_id = ? and erp.discount NOT IN ('N/A') and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =false", eventId).Values(&maps1)
	fmt.Println(err)
	if num > 0 {
		for _, v1 := range maps1 {
			result["eventOverallCalculation"] = v1

		}

	}
	num1, err1 := o.Raw("select SUM (amount) as online_pay_amount from event_register_payments eve_reg_pay left join event_reg_payment_log  as eve_pay_log on eve_reg_pay.id = eve_pay_log.fk_event_reg_pay left join event_register as er on er.id =  eve_reg_pay.fk_event_reg_id where eve_reg_pay.fk_event_id = ? and eve_reg_pay.discount NOT IN ('N/A') and eve_pay_log.transaction_type ='stripe' and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =false ", eventId).Values(&maps2)
	fmt.Println(err1)
	if num1 > 0 {
		result["totalonlineamount"] = maps2[0]
	}

	num2, err2 := o.Raw("select SUM (amount) as offline_pay_amount from event_register_payments eve_reg_pay left join event_reg_payment_log  as eve_pay_log on eve_reg_pay.id = eve_pay_log.fk_event_reg_pay left join event_register as er on er.id =  eve_reg_pay.fk_event_reg_id where eve_reg_pay.fk_event_id = ? and eve_reg_pay.discount NOT IN ('N/A') and eve_pay_log.transaction_action ='offlinepay' and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =false  ", eventId).Values(&maps3)
	fmt.Println(err2)
	if num2 > 0 {
		result["totalofflineamount"] = maps3[0]
	}

	num3, err3 := o.Raw("select SUM (amount) as offline_refund from event_register_payments eve_reg_pay left join event_reg_payment_log  as eve_pay_log on eve_reg_pay.id = eve_pay_log.fk_event_reg_pay left join event_register as er on er.id =  eve_reg_pay.fk_event_reg_id where eve_reg_pay.fk_event_id = ? and eve_reg_pay.discount NOT IN ('N/A') and eve_pay_log.transaction_action ='offlinerefund' and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =false ", eventId).Values(&maps4)
	fmt.Println(err3)
	if num3 > 0 {
		result["totalrefundamount"] = maps4[0]
	}

	resultList = append(resultList, result)
	result = make(map[string]orm.Params)

	return resultList
}
func EventPaymentManual(event_pay_reg_id string, userID string, amountPaid string, discount string, transaction_note string, currency string, choosen_type string) (string, string, string) {
	result := "true"
	paymentlogId := "0"
	o := orm.NewOrm()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params
	var maps5 []orm.Params
	var maps6 []orm.Params
	amount_recived_by := ""
	paid_username := ""
	mode := big.ToNearestEven

	num, _ := o.Raw("select * from user_profile where fk_user_id = ?", userID).Values(&maps3)

	if num > 0 {
		paid_username = maps3[0]["name"].(string) + " " + maps3[0]["last_name"].(string)
		fmt.Println("paid_username--", paid_username)
	}
	if choosen_type == "owing" {

		total_amount := new(big.Rat)
		total_amount.SetString("0")

		total_paid := new(big.Rat)
		total_paid.SetString("0")

		total_event_receive := new(big.Rat)

		amount_paid := new(big.Rat)
		amount_paid.SetString(amountPaid)

		total_event_fee := new(big.Rat)
		total_event_fee.SetString("0")

		total_change_fee := new(big.Rat)
		total_change_fee.SetString("0")

		total_change_fee1 := new(big.Rat)
		total_change_fee1.SetString("0")

		balance_total_fee := new(big.Rat)
		balance_total_fee.SetString("0")

		num, _ := o.Raw("select *, total_amount - total_paid as balance_amount from event_register_payments where id = ?", event_pay_reg_id).Values(&maps)

		if num > 0 {

			if maps[0]["total_paid"] != nil {
				total_paid.SetString(maps[0]["total_paid"].(string))
			}

			if maps[0]["total_amount"] != nil {
				total_amount.SetString(maps[0]["total_amount"].(string))
			}

			if maps[0]["total_event_fee"] != nil {
				total_event_fee.SetString(maps[0]["total_event_fee"].(string))
			}

			if maps[0]["changefee"] != nil {
				total_change_fee.SetString(maps[0]["changefee"].(string))
				total_change_fee1.SetString(maps[0]["changefee"].(string))
			}

			if maps[0]["balance_amount"] != nil {
				balance_total_fee.SetString(maps[0]["balance_amount"].(string))
			}

			if maps[0]["total_event_receive"] != nil {
				total_event_receive.SetString(maps[0]["total_event_receive"].(string))
			} else {
				total_event_receive.SetString("0")

			}

			total_balance_to_pay := new(big.Rat).Sub(total_event_fee, total_event_receive)

			eventowingamount := new(big.Rat)
			eventowingamount.SetString("0")

			zeroamount := new(big.Rat)
			zeroamount.SetString("0")

			eventowingamount.Sub(balance_total_fee, total_balance_to_pay)

			if amount_paid.Cmp(eventowingamount) == 1 || amount_paid.Cmp(eventowingamount) == 0 {

				lastchangefee := total_change_fee1

				amount_recived_by = "eventorganizer"

				diffowingamount := new(big.Rat)
				diffowingamount.SetString("0")

				value0 := new(big.Rat)
				value0.SetString("0")

				if amount_paid.Cmp(balance_total_fee) == 1 {

					diffowingamount.Sub(amount_paid, balance_total_fee)
					total_change_fee.Add(total_change_fee, diffowingamount)

					if diffowingamount.Cmp(value0) == 1 {

						diffowingamount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_change_fee)
						var err1 error

						if lastchangefee.Cmp(value0) == -1 {
							if total_change_fee.Cmp(value0) == -1 {
								total_event_receive.Sub(total_event_receive, diffowingamount)
								total_paid.Sub(total_paid, diffowingamount)
							} else {
								total_event_receive.Add(total_event_receive, lastchangefee)
								total_paid.Add(total_paid, lastchangefee)
							}

							total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
							total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)

							_, err1 = o.Raw("UPDATE event_register_payments SET  total_paid=?, total_event_receive=?, changefee =?, last_updated_at = ? where id=? ", total_paid_float.String(), total_event_receive_float.String(), diffowingamount_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

						}

						if err1 != nil {
							result = "false"
						} else if (total_change_fee.Cmp(value0) == 1 || total_change_fee.Cmp(value0) == 0) && result != "false" {

							if lastchangefee.Cmp(value0) == -1 {
								total_amount.Add(total_amount, total_change_fee)
								total_event_fee.Add(total_event_fee, total_change_fee)
							} else {
								total_amount.Add(total_amount, diffowingamount)
								total_event_fee.Add(total_event_fee, diffowingamount)
							}

							total_amount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_amount)
							total_event_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_fee)

							_, err1 = o.Raw("UPDATE event_register_payments SET total_amount =?, total_event_fee=?,  changefee =?, last_updated_at = ? where id=? ", total_amount_float.String(), total_event_float.String(), diffowingamount_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

						}

						if err1 != nil {
							result = "false"
						} else {
							num1, err := o.Raw("select *, total_amount - total_paid  as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
							fmt.Println(err)
							balance_amount_to_pay := "0"
							if num1 > 0 {
								balance_amount_to_pay = maps6[0]["balance_amount"].(string)
							}

							_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id ", "", "", time.Now(), currency, diffowingamount_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, "owing", balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
							fmt.Println(err2)
							if err2 != nil {
								result = "false"
							} else {
								if maps4[0] != nil {
									paymentlogId = maps4[0]["id"].(string)
								}
							}
						}

					}

				} else if amount_paid.Cmp(balance_total_fee) == -1 {

					diffowingamount.Sub(amount_paid, balance_total_fee)
					total_change_fee.Add(total_change_fee, diffowingamount)

					if diffowingamount.Cmp(value0) == -1 {

						diffowingamount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_change_fee)
						var err1 error

						if lastchangefee.Cmp(value0) == 1 {

							if total_change_fee.Cmp(value0) == 1 {
								total_amount.Add(total_amount, diffowingamount)
								total_event_fee.Add(total_event_fee, diffowingamount)
							} else {
								total_amount.Sub(total_amount, lastchangefee)
								total_event_fee.Sub(total_event_fee, lastchangefee)
							}

							total_amount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_amount)
							total_event_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_fee)

							_, err1 = o.Raw("UPDATE event_register_payments SET total_event_fee =?, total_amount=?, changefee =?, last_updated_at = ? where id=? ", total_event_float.String(), total_amount_float.String(), diffowingamount_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

						}

						if err1 != nil {
							result = "false"
						} else if (total_change_fee.Cmp(value0) == -1 || total_change_fee.Cmp(value0) == 0) && result != "false" {

							if lastchangefee.Cmp(value0) == 1 {
								total_event_receive.Sub(total_event_receive, total_change_fee)
								total_paid.Sub(total_paid, total_change_fee)
							} else {
								total_event_receive.Sub(total_event_receive, diffowingamount)
								total_paid.Sub(total_paid, diffowingamount)
							}

							total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
							total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)

							_, err1 = o.Raw("UPDATE event_register_payments SET total_paid =?, total_event_receive=?,  changefee =?, last_updated_at = ? where id=? ", total_paid_float.String(), total_event_receive_float.String(), diffowingamount_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

						}

						if err1 != nil {
							result = "false"
						} else {
							num1, err := o.Raw("select *, total_amount - total_paid  as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
							fmt.Println(err)
							balance_amount_to_pay := "0"
							if num1 > 0 {
								balance_amount_to_pay = maps6[0]["balance_amount"].(string)
							}

							_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, diffowingamount_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, "owing", balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
							fmt.Println(err2)
							if err2 != nil {
								result = "false"
							} else {
								if maps4[0] != nil {
									paymentlogId = maps4[0]["id"].(string)
								}
							}
						}

					}
				} else if amount_paid.Cmp(balance_total_fee) == 0 {

					diffowingamount.Sub(amount_paid, balance_total_fee)
					total_change_fee.Add(total_change_fee, diffowingamount)

					if diffowingamount.Cmp(value0) == 0 {

						total_event_receive.Add(total_event_receive, diffowingamount)
						total_paid.Add(total_paid, diffowingamount)

						total_amount.Sub(total_amount, diffowingamount)
						total_event_fee.Sub(total_event_fee, diffowingamount)

						total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
						total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)

						diffowingamount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_change_fee)

						total_amount_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_amount)
						total_event_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_fee)

						_, err1 := o.Raw("UPDATE event_register_payments SET total_event_fee =?, total_paid=?, total_amount=?, total_event_receive=?, changefee =?, last_updated_at = ? where id=? ", total_event_float.String(), total_paid_float.String(), total_amount_float.String(), total_event_receive_float.String(), diffowingamount_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

						if err1 != nil {
							result = "false"
						} else {
							num1, err := o.Raw("select *, total_amount - total_paid  as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
							fmt.Println(err)
							balance_amount_to_pay := "0"
							if num1 > 0 {
								balance_amount_to_pay = maps6[0]["balance_amount"].(string)
							}

							_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, diffowingamount_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, "owing", balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
							fmt.Println(err2)
							if err2 != nil {
								result = "false"
							} else {
								if maps4[0] != nil {
									paymentlogId = maps4[0]["id"].(string)
								}
							}
						}

					}
				}

			} else {

				maxmumeventowingamount := new(big.Float).SetPrec(0).SetMode(big.ToNearestEven).SetRat(eventowingamount)
				result = "Owing amount not below this amount " + maxmumeventowingamount.String()
			}
		}

	} else if choosen_type == "offlinepay" {

		num, _ := o.Raw("select * from event_register_payments where id = ?", event_pay_reg_id).Values(&maps)

		total_gushou := new(big.Rat)
		gushou_receive := new(big.Rat)
		total_paid := new(big.Rat)
		total_event_receive := new(big.Rat)
		total_event_receive_old := new(big.Rat)
		total_event_receive_without_commission := new(big.Rat)
		total_amount := new(big.Rat)

		amount_paid := new(big.Rat)
		amount_paid.SetString(amountPaid)

		if num > 0 {

			total_gushou.SetString(maps[0]["total_gushou_fee"].(string))
			gushou_receive.SetString(maps[0]["total_gushou_recevie"].(string))
			total_paid.SetString(maps[0]["total_paid"].(string))
			total_amount.SetString(maps[0]["total_amount"].(string))
			if maps[0]["total_event_receive"] != nil {
				total_event_receive.SetString(maps[0]["total_event_receive"].(string))
			} else {
				total_event_receive.SetString("0")

			}

			remaining_gushou := new(big.Rat).Sub(total_gushou, gushou_receive)
			fmt.Println(remaining_gushou)

			if amount_paid.Cmp(remaining_gushou) == -1 {

				gushou_receive.Add(gushou_receive, amount_paid)
				fmt.Println("gushou_receive--", gushou_receive)
				total_paid.Add(gushou_receive, total_event_receive)
				fmt.Println("total_paid--", total_paid)
				amount_recived_by = "gushou"

				gushou_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(gushou_receive)
				total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
				total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)
				amount_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(amount_paid)
				fmt.Println("amount_paid_float-", amount_paid_float.String())

				_, err1 := o.Raw("UPDATE event_register_payments SET total_paid = ?,total_gushou_recevie = ?, total_event_receive = ?, last_updated_at = ? where id=? ", total_paid_float.String(), gushou_receive_float.String(), total_event_receive_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)
				fmt.Println(err1)
				if err1 != nil {
					result = "false"
				} else {
					num1, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
					balance_amount_to_pay := "0"
					if num1 > 0 {
						balance_amount_to_pay = maps6[0]["balance_amount"].(string)
					}
					_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype, balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, amount_paid_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, amount_paid_float.String(), 0, 0, 0, "", "", "", "").Values(&maps4)

					fmt.Println(err2)
					if err2 != nil {
						result = "false"
					} else {
						if maps4[0] != nil {
							paymentlogId = maps4[0]["id"].(string)
						}
					}
				}

			} else {
				gushou_receive.Add(gushou_receive, remaining_gushou)
				fmt.Println("gushou_receive--", gushou_receive)

				total_event_receive_old.SetString(maps[0]["total_event_receive"].(string))
				fmt.Println("total_event_receive_old--", total_event_receive_old)

				// if maps[0]["gushou_commision_fee"] != nil {
				// 	total_gushou_commision_fee.SetString(maps[0]["gushou_commision_fee"].(string))
				// } else {
				// 	total_gushou_commision_fee.SetString("0")
				// }
				// fmt.Println("total_gushou_commision_fee--", total_gushou_commision_fee)

				total_event_receive_without_commission.Sub(amount_paid, remaining_gushou)

				// current_gushou_commision_fee.Mul(total_event_receive_without_commission, gushouCommisionFee)
				// current_gushou_commision_fee.Quo(current_gushou_commision_fee, intial100Value)

				// fmt.Println("current_gushou_commision_fee--", current_gushou_commision_fee)

				// current_gushou_commision_fee_string := new(big.Float).SetRat(current_gushou_commision_fee).Text('f', 2)
				// current_gushou_commision_fee_Rat := new(big.Rat)
				// current_gushou_commision_fee_Rat.SetString(current_gushou_commision_fee_string)

				//temp_total_event_receive.Sub(total_event_receive_without_commission, current_gushou_commision_fee_Rat)

				//total_gushou_commision_fee.Add(total_gushou_commision_fee, current_gushou_commision_fee_Rat)
				//fmt.Println("total_gushou_commision_fee--", total_gushou_commision_fee)

				total_event_receive.Add(total_event_receive_old, total_event_receive_without_commission)
				fmt.Println("total_event_receive--", total_event_receive)
				total_paid.Add(total_paid, total_event_receive_without_commission)
				total_paid.Add(total_paid, remaining_gushou)
				fmt.Println("total_paid--", total_paid)
				amount_recived_by = "eventorganizer"

				gushou_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(gushou_receive)
				fmt.Println(gushou_receive_float)
				total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
				fmt.Println(total_event_receive_float)
				total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)
				fmt.Println(total_paid_float)
				amount_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(amount_paid)
				//total_gushou_commision_fee_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_gushou_commision_fee)
				//fmt.Println(total_gushou_commision_fee_float)

				//current_gushou_commision_fee_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(current_gushou_commision_fee_Rat)
				//fmt.Println(current_gushou_commision_fee_float)

				// temp_total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(temp_total_event_receive)
				// fmt.Println(temp_total_event_receive_float)

				total_event_receive_without_commission_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive_without_commission)
				fmt.Println(total_event_receive_without_commission_float)

				remaining_gushou_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(remaining_gushou)
				fmt.Println(remaining_gushou_float)

				//	_, err1 := o.Raw("UPDATE event_register_payments SET total_paid = ?,total_gushou_recevie = ?, total_event_receive = ?,gushou_commision_fee = ?, last_updated_at = ? where id=? ", total_paid_float.String(), gushou_receive_float.String(), total_event_receive_float.String(), total_gushou_commision_fee_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)
				_, err1 := o.Raw("UPDATE event_register_payments SET total_paid = ?,total_gushou_recevie = ?, total_event_receive = ?,last_updated_at = ? where id=? ", total_paid_float.String(), gushou_receive_float.String(), total_event_receive_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)

				fmt.Println(err1)
				if err1 != nil {
					result = "false"
				} else {

					num1, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
					balance_amount_to_pay := "0"
					if num1 > 0 {
						balance_amount_to_pay = maps6[0]["balance_amount"].(string)
					}
					//	_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype, balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", "", "", time.Now(), currency, amount_paid_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, remaining_gushou_float.String(), temp_total_event_receive_float.String(), current_gushou_commision_fee_float.String(), 0, "", "", "", "").Values(&maps4)
					_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype, balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, amount_paid_float.String(), "", true, "", event_pay_reg_id, "offline", amount_recived_by, paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, remaining_gushou_float.String(), total_event_receive_without_commission_float.String(), 0, 0, "", "", "", "").Values(&maps4)

					fmt.Println(err2)
					if err2 != nil {
						result = "false"
					} else {
						if maps4[0] != nil {
							paymentlogId = maps4[0]["id"].(string)
						}
					}
				}

			}

		}

	} else if choosen_type == "fee" {
		num0, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
		balance_amount_to_pay := "0"
		if num0 > 0 {
			balance_amount_to_pay = maps6[0]["balance_amount"].(string)
			fmt.Println(balance_amount_to_pay)
		}

		numn, errn := o.Raw("select * from event_register_payments where id=?", event_pay_reg_id).Values(&maps5)
		fmt.Println(errn)
		if numn > 0 {
			fmt.Println("discount--", maps5[0]["discount"])
			if maps5[0]["discount"] != "" {
				check_discount := maps5[0]["discount"].(string)
				fmt.Println(check_discount)
				choosen_type = "apply fee yes"

				_, err1 := o.Raw("UPDATE event_register_payments SET discount = ? where id=? ", "", event_pay_reg_id).Values(&maps1)
				fmt.Println(err1)
				_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, 0, "", true, "", event_pay_reg_id, "offline", "", paid_username, "succeeded", "", transaction_note, choosen_type, balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
				fmt.Println(err2)
				if err2 != nil {
					result = "false"
				} else {
					if maps4[0] != nil {
						paymentlogId = maps4[0]["id"].(string)
					}
				}
			} else {
				balance_amount_to_pay = "0"
				_, err1 := o.Raw("UPDATE event_register_payments SET discount = ? where id=? ", discount, event_pay_reg_id).Values(&maps1)
				fmt.Println(err1)
				_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, 0, "", true, "", event_pay_reg_id, "offline", "", paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
				fmt.Println(err2)
				if err2 != nil {
					result = "false"
				} else {
					if maps4[0] != nil {
						paymentlogId = maps4[0]["id"].(string)
					}
				}
			}

		}

	} else if choosen_type == "offlinerefund" {

		num, _ := o.Raw("select * from event_register_payments where id = ?", event_pay_reg_id).Values(&maps)

		if num > 0 {
			amount_paid := new(big.Rat)
			amount_paid.SetString(amountPaid)
			total_event_refund := new(big.Rat)
			total_event_refund.SetString(maps[0]["total_refund"].(string))
			total_paid := new(big.Rat)
			total_paid.SetString(maps[0]["total_paid"].(string))

			total_event_receive := new(big.Rat)
			total_event_receive.SetString(maps[0]["total_event_receive"].(string))

			ZeroVal := new(big.Rat)
			ZeroVal.SetString("0")

			if amount_paid.Cmp(total_event_receive) == -1 || amount_paid.Cmp(total_event_receive) == 0 {

				total_event_receive.Sub(total_event_receive, amount_paid)
				fmt.Println("total_event_receive--", total_event_receive)
				total_paid.Sub(total_paid, amount_paid)
				fmt.Println("total_paid--", total_paid)

				if total_event_refund.Cmp(ZeroVal) == 1 {

					if amount_paid.Cmp(total_event_refund) == 1 {

						total_event_refund.SetString("0")

					} else {
						total_event_refund.Sub(total_event_refund, amount_paid)
					}

				}

				amount_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(amount_paid)
				total_event_receive_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_receive)
				total_paid_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_paid)
				total_event_refund_float := new(big.Float).SetPrec(0).SetMode(mode).SetRat(total_event_refund)

				fmt.Println(total_event_receive_float)
				fmt.Println(total_paid_float)
				amount_recived_by = "eventorganizer"

				_, err1 := o.Raw("UPDATE event_register_payments SET total_refund = ?,total_event_receive=?,total_paid =?, last_updated_at = ? where id=? ", total_event_refund_float.String(), total_event_receive_float.String(), total_paid_float.String(), time.Now(), event_pay_reg_id).Values(&maps1)
				fmt.Println(err1)
				if err1 != nil {
					result = "false"
				} else {

					num1, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
					balance_amount_to_pay := "0"
					if num1 > 0 {
						balance_amount_to_pay = maps6[0]["balance_amount"].(string)
					}
					_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), currency, amount_paid_float.String(), "", true, "", event_pay_reg_id, "offline", "eventorganizer", paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, 0, amount_paid_float.String(), 0, 0, "", "", "", "").Values(&maps4)
					fmt.Println(err2)
					if err2 != nil {
						result = "false"
					} else {
						if maps4[0] != nil {
							paymentlogId = maps4[0]["id"].(string)
						}
					}
				}

			} else {
				result = "false"
				choosen_type = "offline_refund_higher"
			}

		}
	} else if choosen_type == "recordnote" {

		num1, _ := o.Raw("select *, total_amount-total_paid as balance_amount from event_register_payments where id= ?", event_pay_reg_id).Values(&maps6)
		balance_amount_to_pay := "0"
		if num1 > 0 {
			balance_amount_to_pay = maps6[0]["balance_amount"].(string)
		}

		_, err2 := o.Raw("INSERT INTO event_reg_payment_log (chargeId,balance_trasaction_id,created_date,currency_unit,amount,customerId,paidstatus,invoiceid,fk_event_reg_pay,transaction_type,amount_recived,paidby,status,discount,transaction_note,transaction_action,balance_amount,event_transaction_fee,gushou_fee_net_fee,event_fee_net_fee,gushoucommisionfee,gushou_transaction_fee,stripe_event_conversion_currency,stripe_gushou_conversion_currency,paymenttype,balance_trasaction_id1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id", "", "", time.Now(), "", 0, "", true, "", event_pay_reg_id, "offline", "", paid_username, "succeeded", discount, transaction_note, choosen_type, balance_amount_to_pay, 0, 0, 0, 0, 0, "", "", "", "").Values(&maps4)
		fmt.Println(err2)
		if err2 != nil {
			result = "false"
		} else {
			if maps4[0] != nil {
				paymentlogId = maps4[0]["id"].(string)
			}
		}

	}

	return result, choosen_type, paymentlogId
}

func EventPaymentTrackingList(event_pay_reg_id string, eventID string, teamID string, userID string) ([]map[string]orm.Params, string, string, string) {
	result := make(map[string]orm.Params)
	resultList := []map[string]orm.Params{}
	o := orm.NewOrm()
	var maps1 []orm.Params
	currency_unit := ""
	stripe_event_conversion_currency := ""
	stripe_gushou_conversion_currency := ""

	num, _ := o.Raw("select *,to_char(pay_log.balance_amount, 'FM999,999,999,990D00') as format_balance,pay_log.id as log_id,to_char(pay_log.gushoucommisionfee, 'FM999,999,999,990D00') as formatted_gushoucommisionfee,to_char(pay_log.amount, 'FM999,999,999,990D00') as formatted_amount,to_char((pay_log.event_fee_net_fee), 'FM999,999,999,990D00') as final_event_received,to_char((pay_log.gushou_fee_net_fee + pay_log.gushou_transaction_fee), 'FM999,999,999,990D00')  as final_gushou_received,to_char(pay_log.created_date, 'DD/MM/YY at HH12:MI AM') as formatted_date from event_register_payments eve_pay left join event_reg_payment_log pay_log on eve_pay.id=pay_log.fk_event_reg_pay left join event eve on eve.id = eve_pay.fk_event_id left join team te on te.id = eve_pay.fk_team_id where pay_log.fk_event_reg_pay = ? order by pay_log.id desc", event_pay_reg_id).Values(&maps1)
	if num > 0 {
		for _, v3 := range maps1 {
			result["eventPaymentTrackData"] = v3
			currency_unit = v3["currency"].(string)
			if v3["stripe_event_conversion_currency"] != nil {
				stripe_event_conversion_currency = v3["stripe_event_conversion_currency"].(string)
			}
			if v3["stripe_gushou_conversion_currency"] != nil {
				stripe_gushou_conversion_currency = v3["stripe_gushou_conversion_currency"].(string)
			}

			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}
	}

	return resultList, currency_unit, stripe_event_conversion_currency, stripe_gushou_conversion_currency
}

func GetTeamEventDetails(event_pay_reg_id string, eventID string, teamID string, userID string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	resultList := []map[string]orm.Params{}
	o := orm.NewOrm()
	var maps1 []orm.Params
	var maps2 []orm.Params

	num1, _ := o.Raw("select * from event as ev left join event_logo_url as elu on ev.id=elu.fk_event_id where ev.id = ?", eventID).Values(&maps1)
	if num1 > 0 {
		for _, v1 := range maps1 {
			result["EventDetails"] = v1

		}
	}
	num2, _ := o.Raw("select * from team where id= ?", teamID).Values(&maps2)

	if num2 > 0 {

		for _, v2 := range maps2 {
			result["TeamDetails"] = v2

		}
	}

	resultList = append(resultList, result)
	result = make(map[string]orm.Params)
	return resultList

}

func GetEventSlugNameByEventId(eventid string) string {
	o := orm.NewOrm()
	result := ""
	var maps []orm.Params
	num, _ := o.Raw("SELECT slug FROM event WHERE id=? ", eventid).Values(&maps)
	if num > 0 {
		result = maps[0]["slug"].(string)
	}
	return result
}

func GetEventDEtailByEventId(eventid string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event WHERE id = ? order by id asc ", eventid).Values(&maps)

	return maps
}

func GetEventCreatedDateByEventId(eventid string, teamid string) []orm.Params {

	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT to_char(created_at, 'Mon DD,YYYY') as registerdate FROM event_register WHERE fk_event_id = ? and fk_team_id =? order by id asc ", eventid, teamid).Values(&maps)

	return maps
}

func GetEventRegisterTeamPaymentDetail(regpaymnetid string) []orm.Params {
	o := orm.NewOrm()

	var maps []orm.Params

	_, _ = o.Raw("SELECT * FROM event_register_payments WHERE id = ? ", regpaymnetid).Values(&maps)

	return maps
}
func SendEventPaymentEmailForTeams(reg_event_ids string) string {
	result := "true"
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps5 []orm.Params
	var co_captain_email_array []string
	site_url := ""
	if len(os.Getenv("SITE_URL")) > 0 {
		site_url = os.Getenv("SITE_URL")
	} else {
		site_url = "http://localhost:5000/"
	}

	num, err := o.Raw("select usr.email_address as event_organizer_email,eve.fk_organizer_id as event_organizer_id,te.fk_co_captains as co_captains_id,te.slug as team_slug,(eve_pay.total_amount - eve_pay.total_paid) as amount_owe,te.team_name,eve_reg.fk_event_id,reg_email_id , reg_captain_name , fk_captain_id from event_register as eve_reg left join event as eve on eve_reg.fk_event_id=eve.id left join users as usr on eve.fk_organizer_id = usr.id left join team as te on eve_reg.fk_team_id = te.id left join users as us on us.id = te.id left join user_profile as us_pro on us_pro.id = us.id left join event_register_payments as eve_pay on eve_pay.fk_event_reg_id=eve_reg.id where eve_reg.id in (" + reg_event_ids + ")").Values(&maps1)
	fmt.Println(err)
	if err != nil {
		result = "false"
	}
	if num > 0 {
		var captain_email, captain_name, amount_owe, team_name, team_slug, event_organizer_email, co_captains_id, captain_id, co_captains_emailId string

		for _, v := range maps1 {

			//captain_email = v["reg_email_id"].(string)
			captain_name = v["reg_captain_name"].(string)
			team_name = v["team_name"].(string)
			team_slug = v["team_slug"].(string)
			amount_owe = v["amount_owe"].(string)
			event_organizer_email = v["event_organizer_email"].(string)
			captain_id = v["fk_captain_id"].(string)
			co_captains_id = v["co_captains_id"].(string)
			co_captains_id = strings.Replace(co_captains_id, `}`, "", -1)
			co_captains_id = strings.Replace(co_captains_id, `{`, "", -1)

			num2, err2 := o.Raw(" select email_address as captain_email from users where id  = ?", captain_id).Values(&maps5)
			fmt.Println(err2)

			if err2 != nil {
				result = "false"
			}

			if num2 > 0 {

				if maps5[0]["captain_email"] != nil {
					captain_email = maps5[0]["captain_email"].(string)

				}

			}

			num1, _ := o.Raw("select email_address as co_captains_email  from users where id in (" + co_captains_id + ") AND id NOT IN (" + captain_id + ")").Values(&maps)

			// if num1 > 0 {
			// 	co_captains_emailId = maps[0]["co_captains_email"].(string)
			// }
			if num1 > 0 {
				for _, v := range maps {

					co_captains_emailId = v["co_captains_email"].(string)

					if captain_email != co_captains_emailId {

						co_captain_email_array = append(co_captain_email_array, v["co_captains_email"].(string)+",")
					}

				}
			}

			if captain_email != "" {
				login_link := "<a href='" + site_url + "team-view/" + team_slug + "/team_event_payment'>here</a>"
				fmt.Println(login_link)
				footer_msg := strings.Replace(constants.EMAILS_SOCIAL_FOOTER, "#siteUrl", site_url, -1)
				unsubscribe_link := strings.Replace(constants.EMAILS_UNSUBSCRIBE_FOOTER, "#siteUrl", site_url, -1)
				html_email_message := "<html>Hi " + captain_name + ", <br/><br/>" + "This is a friendly reminder that you have a balance owing of <b>" + amount_owe + "</b> " + " for <b>" + team_name + ". </b> <br><br> You can log in to your account  " + login_link + " to pay online.<br><br> Message sent on behalf of event organizer via Gushou" + footer_msg + constants.EMAILS_NO_REPLY_FOOTER + unsubscribe_link + "</html>"
				justString_CoCaptainEmail := strings.Join(co_captain_email_array, " ")
				common.SendEmailWithCC(captain_email+"-"+event_organizer_email+","+justString_CoCaptainEmail, "Remainder for Payment from your Event Organizer", html_email_message)

			}
		}
	}
	return result
}

func GetOrderOtherIteamDetailForPdfGeneration(eventid string, regeventid string) ([][]string, *big.Rat) {
	o := orm.NewOrm()
	rows := [][]string{}
	var maps []orm.Params
	totalOrderCost := new(big.Rat)
	totalOrderCost.SetString("0")

	num, err := o.Raw("select po.item as iteamname , po.cost as iteamcost, erpo.iteam_id_quantity as iteamquantity from pricing_other as po  left join event_register_pricing_other as erpo on po.id =erpo.iteam_Id where po.fk_event_id = ? and erpo.fk_event_reg_id = ?", eventid, regeventid).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		iteamname := "-"
		iteamcost := "0"
		iteamcost1 := "0"
		iteam_quantity := "0"
		iteam_quantity_price := "0"

		for _, v := range maps {

			iteamname = "-"
			iteamcost = "0"
			iteamcost1 = "0"
			iteam_quantity = "0"
			iteam_quantity_price = "0"

			if v["iteamname"] != nil {
				iteamname = v["iteamname"].(string)
			}

			if v["iteamcost"] != nil {
				iteamcost = v["iteamcost"].(string)
				iteamcost1 = common.ConvertAmountIntoFormat(iteamcost)
			}

			if v["iteamquantity"] != nil {
				iteam_quantity = v["iteamquantity"].(string)
			}

			f, err := strconv.ParseFloat(iteamcost, 64)
			i, err2 := strconv.Atoi(iteam_quantity)

			if err == nil && err2 == nil {
				tempf1 := float64(i)
				totalCost := tempf1 * f

				otherIteamTotalRat := new(big.Rat)
				otherIteamTotalRat.SetFloat64(totalCost)
				totalOrderCost.Add(totalOrderCost, otherIteamTotalRat)

				iteam_quantity_price = strconv.FormatFloat(totalCost, 'f', -1, 64)
				iteam_quantity_price = common.ConvertAmountIntoFormat(iteam_quantity_price)
			}

			rows = append(rows, []string{iteamname, iteam_quantity, iteamcost1, iteam_quantity_price})
		}
	}
	return rows, totalOrderCost
}

func GetOrderAdditionalPracticeIteamDetailForPdfGeneration(additionalPracticeQuantity string, regeventid string) ([][]string, *big.Rat) {

	rows := [][]string{}

	name := "-"
	tempaddtionalPrice := "0"

	additionalQuantityTotal := new(big.Rat)
	additionalQuantityTotal.SetString("0")

	addtionalPrice, addtionalPriceerror := GetEventAdditionalPraticePriceByEventId(regeventid)

	if addtionalPriceerror == nil {
		if addtionalPrice != nil {

			if addtionalPrice[0]["price"] != nil {
				tempaddtionalPrice = addtionalPrice[0]["price"].(string)

				addtionalPriceTotalRat, additionalQuantityTotalRat := new(big.Rat), new(big.Rat)

				addtionalPriceTotalRat.SetString(tempaddtionalPrice)
				additionalQuantityTotalRat.SetString(additionalPracticeQuantity)
				additionalQuantityTotal.Mul(addtionalPriceTotalRat, additionalQuantityTotalRat)

			}

			if addtionalPrice[0]["name"] != nil {
				name = addtionalPrice[0]["name"].(string)
			}
			tempadditionalQuantityTotal := new(big.Float).SetPrec(0).SetMode(big.ToNearestEven).SetRat(additionalQuantityTotal)
			tempadditionalQuantityTotal1 := common.ConvertAmountIntoFormat(tempadditionalQuantityTotal.String())
			if tempaddtionalPrice != "0.00" {
				rows = append(rows, []string{name, additionalPracticeQuantity, tempaddtionalPrice, tempadditionalQuantityTotal1})
			}
		}
	}

	return rows, additionalQuantityTotal
}

func GetPerpersonOrderDetailForPdfGeneration(eventid string, regeventid string) ([][]string, *big.Rat, *big.Rat) {
	rows := [][]string{}

	totalOrderCostTotal := new(big.Rat)
	totalOrderCostTotal.SetString("0")

	tempRoleTotalCount := new(big.Rat)
	tempRoleTotalCount.SetString("0")

	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("select pf.early_bird_cost as earlybirdcost ,pf.regular_cost as regularcost, pf.super_early_bird_cost as superearlybirdcost, pf.price_fee_role as pricefeerole, erro.role_iteam_id_quantity as roleitemidquantity  from pricing_fee as pf  left join event_register_role_pricing_quantity as erro on pf.id =erro.role_iteam_id  where pf.fk_event_id = ? and erro.fk_event_reg_id = ? order by pf.id asc", eventid, regeventid).Values(&maps)
	fmt.Println(err)
	if num > 0 {
		pricefeerole := "-"
		regularcost := "0"
		roleitemidquantity := "0"
		role_quantity_price := "0"
		regularcost1 := "0"

		for _, v := range maps {

			pricefeerole = "-"
			regularcost = "0"
			regularcost1 = "0"
			roleitemidquantity = "0"
			role_quantity_price = "0"

			if v["pricefeerole"] != nil {
				pricefeerole = v["pricefeerole"].(string)
				pricefeerole = strings.Replace(pricefeerole, "{", "", -1)
				pricefeerole = strings.Replace(pricefeerole, "}", "", -1)
				pricefeerole = strings.Replace(pricefeerole, `"`, "", -1)
			}

			if v["regularcost"] != nil {
				regularcost = v["regularcost"].(string)
				regularcost1 = common.ConvertAmountIntoFormat(regularcost)
			}

			if v["roleitemidquantity"] != nil {
				roleitemidquantity = v["roleitemidquantity"].(string)
			}

			f, err := strconv.ParseFloat(regularcost, 64)
			i, err2 := strconv.Atoi(roleitemidquantity)

			if err == nil && err2 == nil {
				tempf1 := float64(i)
				totalCost := tempf1 * f

				otherIteamTotalRat := new(big.Rat)
				otherIteamTotalRat.SetFloat64(totalCost)
				totalOrderCostTotal.Add(totalOrderCostTotal, otherIteamTotalRat)

				otherRoleTotalCount := new(big.Rat)
				otherRoleTotalCount.SetFloat64(float64(i))
				tempRoleTotalCount.Add(tempRoleTotalCount, otherRoleTotalCount)

				role_quantity_price = strconv.FormatFloat(totalCost, 'f', -1, 64)
				role_quantity_price = common.ConvertAmountIntoFormat(role_quantity_price)
			}

			rows = append(rows, []string{"Per person(" + pricefeerole + ")", roleitemidquantity, regularcost1, role_quantity_price})

		}
	}

	return rows, totalOrderCostTotal, tempRoleTotalCount
}

func GetTotalRemaningPaidAmountEventRegPdfGeneration(paymentid string) string {

	o := orm.NewOrm()

	var maps []orm.Params

	num, _ := o.Raw("SELECT total_amount - total_paid as remaamount FROM event_register_payments as erp  WHERE id = ? ", paymentid).Values(&maps)

	remaamount := "0.00"
	if num > 0 {
		if maps[0]["remaamount"] != nil {
			remaamount = maps[0]["remaamount"].(string)
			remaamount = common.ConvertAmountIntoFormat(remaamount)
		}
	}

	return remaamount
}

func GetSelectedTeamPaymentDetails(regeventid string) []map[string]string {

	result := make(map[string]string)

	resultList := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select CASE WHEN total_paid = 0 THEN 'unpaid' WHEN total_amount > total_paid THEN 'partial' WHEN total_amount =total_paid THEN 'paid' end as paymentstatus,(total_amount-total_paid) as balance_amount , to_char((total_amount - total_paid), 'FM999,999,999,990D00') as balance_formatted, id as eventpayId , total_event_fee ,total_event_receive, discount from event_register_payments where fk_event_reg_id = ?", regeventid).Values(&maps)
	fmt.Println(err)
	if num > 0 {

		result["balance_amount"] = maps[0]["balance_amount"].(string)
		result["balance_formatted"] = maps[0]["balance_formatted"].(string)
		result["eventpayid"] = maps[0]["eventpayid"].(string)
		result["total_event_fee"] = maps[0]["total_event_fee"].(string)
		result["total_event_receive"] = maps[0]["total_event_receive"].(string)
		if maps[0]["paymentstatus"] != nil {
			result["paymentstatus"] = maps[0]["paymentstatus"].(string)
		}
		if maps[0]["discount"] != nil {
			result["discount"] = maps[0]["discount"].(string)
		} else {
			result["discount"] = ""
		}
		resultList = append(resultList, result)
		result = make(map[string]string)
	}
	return resultList
}

func GetEventNameDetailByEventId(eventid string) string {

	o := orm.NewOrm()
	result := ""
	var maps []orm.Params

	num, _ := o.Raw("SELECT event_name FROM event WHERE id = ? order by id asc ", eventid).Values(&maps)

	if num > 0 {
		if maps[0]["event_name"] != nil {
			result = maps[0]["event_name"].(string)
		}
	}

	return result
}
func GetFilterPaymentList(eventId string, paystatusfilterValue string, getTeamNameFilter string) []map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps1 []orm.Params
	var maps2 []orm.Params
	resultList := []map[string]orm.Params{}

	sql := "SELECT *,to_char((total_amount - total_paid), 'FM999,999,999,990D00') as balance_formatted,to_char(eve_pay.total_event_receive, 'FM999,999,999,990D00') as formatted_total_event_receive,to_char(eve_pay.total_amount, 'FM999,999,999,990D00') as formatted_total_amount,CASE WHEN eve_pay.total_paid = 0 THEN 'unpaid' WHEN eve_pay.total_amount > eve_pay.total_paid THEN 'partial' WHEN eve_pay.total_amount = eve_pay.total_paid THEN 'paid' end as paymentstatus,eve_pay.fk_event_id as pay_event_id,eve_pay.fk_team_id as pay_team_id,eve_pay.id as event_reg_pay_id,eve_pay.total_amount-eve_pay.total_paid as balance_amount,currency,er.id as eventregisterid,er.fk_team_id as eventregisterteamid, to_char(er.created_at, 'Mon DD- YY') asregisterdate, tlu.path as teamlogo FROM event_register as er left join team te on te.id = er.fk_team_id left JOIN team_logo_url tlu on tlu.fk_team_id = te.id left JOIN event_register_payments eve_pay on er.id = eve_pay.fk_event_reg_id left JOIN event eve on er.fk_event_id = eve.id WHERE er.fk_event_id = ? and er.active=1 and er.registration_status ='Success'"

	if paystatusfilterValue != "" || getTeamNameFilter != "" {

		if paystatusfilterValue == "unpaid" {

			sql = sql + " " + "and eve_pay.total_paid = 0"

		} else if paystatusfilterValue == "partial" {

			sql = sql + " " + "and eve_pay.total_amount > eve_pay.total_paid"

		} else if paystatusfilterValue == "paid" {

			sql = sql + " " + "and eve_pay.total_amount = eve_pay.total_paid"

		} else if paystatusfilterValue == "all" {

			sql = sql

		} else {
			sql = sql + " " + "and te.team_name ILIKE '%" + getTeamNameFilter + "%' "
		}

	}
	sql = sql + " " + "order by er.id desc"

	fmt.Println(sql)
	num, err := o.Raw(sql, eventId).Values(&maps1)
	fmt.Println(err)
	if num > 0 {
		for _, v1 := range maps1 {
			result["registerEventPaymentData"] = v1
			event_reg_pay_id := v1["event_reg_pay_id"].(string)
			num1, err1 := o.Raw("SELECT id as invoice_no FROM event_reg_payment_log where fk_event_reg_pay = ? order by id desc  limit  1", event_reg_pay_id).Values(&maps2)
			fmt.Println(err1)
			if num1 > 0 {
				for _, v2 := range maps2 {
					result["getinvoice_no"] = v2
				}

			}
			resultList = append(resultList, result)
			result = make(map[string]orm.Params)
		}

	} else {
		fmt.Println("No Team")
		result["status"] = nil
	}

	return resultList
}

func GetEventRegisterTeamPaymentToalPaidDetail(regeventteamid string) bool {
	o := orm.NewOrm()
	totalPaid := "0.00"
	var maps []orm.Params
	result := false
	num, _ := o.Raw("SELECT total_paid FROM event_register_payments WHERE fk_event_reg_id = ? ", regeventteamid).Values(&maps)

	if num > 0 {
		if maps[0]["total_paid"] != nil {
			totalPaid = maps[0]["total_paid"].(string)

			f, _ := strconv.ParseFloat(totalPaid, 64)
			if f > 0 {
				result = false
			} else {
				result = true
			}
		}
	}
	return result
}
func GetSelectedTeamEventPaymentDetails(regeventid string) []map[string]string {

	result := make(map[string]string)

	resultList := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select te.team_name,CASE WHEN erp.changefee < 0 THEN 'true' else 'false' end as changeefeedetail, CASE WHEN erp.total_paid = 0 THEN 'unpaid' WHEN erp.total_amount > erp.total_paid THEN 'partial' WHEN erp.total_amount =erp.total_paid THEN 'paid' end as paymentstatus,(erp.total_amount-erp.total_paid) as balance_amount , to_char((erp.total_amount - erp.total_paid), 'FM999,999,999,990D00') as balance_formatted, erp.id as eventpayId ,erp.total_event_fee ,erp.total_event_receive, erp.discount from event_register_payments as erp left join team as te on te.id = erp.fk_team_id where erp.id= ?", regeventid).Values(&maps)
	fmt.Println(err)
	if num > 0 {

		result["balance_amount"] = maps[0]["balance_amount"].(string)
		result["balance_formatted"] = maps[0]["balance_formatted"].(string)
		result["eventpayid"] = maps[0]["eventpayid"].(string)
		result["total_event_fee"] = maps[0]["total_event_fee"].(string)
		result["total_event_receive"] = maps[0]["total_event_receive"].(string)
		result["paymentstatus"] = maps[0]["paymentstatus"].(string)
		result["team_name"] = maps[0]["team_name"].(string)
		result["changeefeedetail"] = maps[0]["changeefeedetail"].(string)
		if maps[0]["discount"] != nil {
			result["discount"] = maps[0]["discount"].(string)
		} else {
			result["discount"] = ""
		}
		resultList = append(resultList, result)
		result = make(map[string]string)
	}
	return resultList
}

func GetExtralOrDiscountFeesAmount(regeventpayid string) string {
	o := orm.NewOrm()
	changefee := "0.00"
	var maps []orm.Params

	num, _ := o.Raw("SELECT changefee FROM event_register_payments WHERE id = ? ", regeventpayid).Values(&maps)

	if num > 0 {
		if maps[0]["changefee"] != nil {
			changefee = maps[0]["changefee"].(string)
			changefee = common.ConvertAmountIntoFormat(changefee)
		}
	}
	return changefee
}

func GetTotalFeeChange(eventid string) string {

	o := orm.NewOrm()
	result := "0.00"
	var maps []orm.Params

	num, err := o.Raw("select sum(changefee) as totalfeechange from event_register_payments as erp left join event_register as er on er.id =  erp.fk_event_reg_id where erp.fk_event_id = ? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status =false", eventid).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		if maps[0]["totalfeechange"] != nil {
			result = maps[0]["totalfeechange"].(string)
			result = common.ConvertAmountIntoFormat(result)
		}
	}

	return result
}

func GetEventWaiverList(eventid string) ([]map[string]string, string) {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}
	flag := "false"

	num, err := o.Raw("select * from event_register where fk_event_id = ? and active=1 and registration_status =? and reg_cancel_status = ? order by id desc", eventid, "Success", "false").Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v1 := range maps {
			result["fk_team_id"] = v1["fk_team_id"].(string)
			result["fk_event_reg_id"] = v1["id"].(string)

			team_name := GetTeamNameByTeamId(v1["fk_team_id"].(string))
			result["fk_team_name"] = team_name

			captain_id, co_captain_id := GetCaptainCoCaptainByTeamId(v1["fk_team_id"].(string))
			result["captain_id"] = captain_id
			result["co_captain_id"] = co_captain_id

			roster_submitted := GetEventRosterStatusForTeam(v1["id"].(string))
			result["roster_submitted"] = roster_submitted
			if roster_submitted == "yes" {
				flag = "true"
			}
			var waiver_status1, waiver_submitted1 string
			flag1 := "true"
			fmt.Println(v1["id"].(string))
			num := GetSubmittedRosterForEventByEventRegId(v1["id"].(string))
			fmt.Println("num", len(num))
			if len(num) != 1 {

				for i := 0; i < len(num)-1; i++ {

					if num[i]["formatted_waiver_send_date"] == "" {
						flag1 = "false"
					}

					if num[i]["formatted_waiver_signed_date"] == "" && num[i]["signed_upload_date"] == "" {
						flag1 = "false"
					}

					if flag1 == "true" {
						waiver_status1 = "complete"
						waiver_submitted1 = "submitted"
					} else {
						waiver_status1 = "incomplete"
						waiver_submitted1 = "submitted"
					}
					result["waiver_status"] = waiver_status1
					result["waiver_submitted"] = waiver_submitted1
				}

			} else {
				waiver_status1 = "incomplete"
				waiver_submitted1 = "submitted"
				result["waiver_status"] = waiver_status1
				result["waiver_submitted"] = waiver_submitted1
			}

			resultList = append(resultList, result)
			result = make(map[string]string)
		}
	}

	return resultList, flag
}
func GetEventRosterStatusForTeam(event_reg_id string) string {
	roster_submitted := ""
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select * from event_register_roster where fk_event_reg_id = ?", event_reg_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		roster_submitted = "yes"
	} else {
		roster_submitted = "no"
	}
	return roster_submitted
}
func GetTeamNameByTeamId(teamid string) string {

	teamname := ""
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select * from team where id = ?", teamid).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		teamname = maps[0]["team_name"].(string)
	}
	return teamname
}
func GetWaiverStatusByTeamEventId(teamid string, eventid string) (string, string) {
	waiver_status := ""
	waiver_submitted := ""
	flag := "true"
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select * from waivers where fk_team_id = ? and fk_event_id =? order by id desc", teamid, eventid).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v1 := range maps {

			if v1["waiver_signed_date"] == nil && v1["signed_upload_date"] == nil {
				flag = "false"
			}

			if flag == "true" {
				waiver_status = "complete"
				waiver_submitted = "submitted"
			} else {
				waiver_status = "incomplete"
				waiver_submitted = "submitted"
			}
		}

	} else {
		waiver_status = "incomplete"
		waiver_submitted = "not submitted"
	}
	return waiver_status, waiver_submitted
}
func GetSubmittedRosterForEventByEventRegId(event_reg_id string) []map[string]string {

	result := make(map[string]string)
	resultList := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params

	var team_member_without_duplicate []string
	//var empty_array []string
	var team_array []string
	var string_array []string
	var fk_team_id, fk_event_id string
	num, err := o.Raw("select *,te_roster.fk_team_id,te_roster.fk_event_id,te_roster.members as team_members from event_register_roster as er_roster left join team_roster as te_roster on te_roster.id = er_roster.roster_id where er_roster.fk_event_reg_id = ?", event_reg_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {

		for _, v1 := range maps {

			var team_members string

			team_members = v1["team_members"].(string)
			fk_team_id = v1["fk_team_id"].(string)
			fk_event_id = v1["fk_event_id"].(string)
			team_members = strings.Replace(team_members, `}`, "", -1)
			team_members = strings.Replace(team_members, `{`, "", -1)
			team_array = append(team_array, team_members+",")

		}
		JustDtring := strings.Join(team_array, "")
		string_array = strings.Split(JustDtring, ",")
		team_member_without_duplicate = common.RemoveDuplicates(string_array)
		fmt.Println(fk_team_id)
		fmt.Println(fk_event_id)
		fmt.Println(team_member_without_duplicate)
		for _, user_id := range team_member_without_duplicate {

			fmt.Println(user_id)
			if user_id != "" {
				num1, err1 := o.Raw("SELECT name, last_name, classes, files, date_part('year',age(dob)) as age FROM users join user_profile on users.id= user_profile.fk_user_id WHERE users.id = ? AND name is NOT NULL AND last_name is NOT NULL", user_id).Values(&maps1)
				fmt.Print(err1)
				if num1 > 0 {
					var username, user_class, user_age string

					username = maps1[0]["name"].(string) + " " + maps1[0]["last_name"].(string)
					user_class = maps1[0]["classes"].(string)
					user_age = maps1[0]["age"].(string)
					result["username"] = username
					result["user_class"] = user_class
					if user_age < "18" {
						result["isUnder_18"] = "true"
					} else {
						result["isUnder_18"] = "false"
					}
					if maps1[0]["files"] != nil {
						result["files"] = maps1[0]["files"].(string)
					} else {
						result["files"] = ""
					}
					result["user_age"] = user_age
					result["fk_team_id"] = fk_team_id

					result["fk_event_id"] = fk_event_id

					result["fk_member_id"] = user_id

				}
				num2, err2 := o.Raw("select wai.validate,wai.signed_upload_waiver_path,to_char(wai.signed_upload_date, 'DD Mon YYYY') as signed_upload_date,wai.fk_team_waiver_id,to_char(wai.waiver_send_date, 'DD Mon YYYY') as formatted_waiver_send_date ,to_char(wai.waiver_signed_date, 'DD Mon YYYY') as formatted_waiver_signed_date,wai.fk_member_id,wai.contact_name,wai.contact_number,wai.i_agree,wai.medical_condition_desc,wai.waiver_send_date,wai.waiver_signed_date from waivers as wai where wai.fk_team_id= ? and wai.fk_event_id = ? and wai.fk_member_id = ?", fk_team_id, fk_event_id, user_id).Values(&maps2)
				fmt.Print(err2)
				if num2 > 0 {

					if maps2[0]["contact_name"] != nil {
						result["contact_name"] = maps2[0]["contact_name"].(string)
					} else {
						result["contact_name"] = ""
					}

					if maps2[0]["validate"] != nil {
						result["validate"] = maps2[0]["validate"].(string)
					} else {
						result["validate"] = ""
					}

					if maps2[0]["signed_upload_waiver_path"] != nil {
						result["signed_upload_waiver_path"] = maps2[0]["signed_upload_waiver_path"].(string)
					} else {
						result["signed_upload_waiver_path"] = ""
					}

					if maps2[0]["signed_upload_date"] != nil {
						result["signed_upload_date"] = maps2[0]["signed_upload_date"].(string)
					} else {
						result["signed_upload_date"] = ""
					}

					if maps2[0]["fk_team_waiver_id"] != nil {
						result["fk_team_waiver_id"] = maps2[0]["fk_team_waiver_id"].(string)
					} else {
						result["fk_team_waiver_id"] = ""
					}

					// result["fk_team_id"] = fk_team_id

					// result["fk_event_id"] = fk_event_id

					// if maps2[0]["fk_member_id"] != nil {
					// 	result["fk_member_id"] = maps2[0]["fk_member_id"].(string)
					// } else {
					// 	result["fk_member_id"] = ""
					// }

					if maps2[0]["contact_number"] != nil {
						result["contact_number"] = maps2[0]["contact_number"].(string)
					} else {
						result["contact_number"] = ""
					}

					if maps2[0]["i_agree"] != nil {
						result["i_agree"] = maps2[0]["i_agree"].(string)
					} else {
						result["i_agree"] = ""
					}
					if maps2[0]["medical_condition_desc"] != nil {
						result["medical_condition_desc"] = maps2[0]["medical_condition_desc"].(string)
					} else {
						result["medical_condition_desc"] = ""
					}
					if maps2[0]["formatted_waiver_send_date"] != nil {
						result["formatted_waiver_send_date"] = maps2[0]["formatted_waiver_send_date"].(string)
					} else {
						result["formatted_waiver_send_date"] = ""
					}
					if maps2[0]["formatted_waiver_signed_date"] != nil {
						result["formatted_waiver_signed_date"] = maps2[0]["formatted_waiver_signed_date"].(string)
					} else {
						result["formatted_waiver_signed_date"] = ""
					}

				}
			}
			resultList = append(resultList, result)
			result = make(map[string]string)
		}

	}
	return resultList
}

func GetAccreditationGeneralDetail(eventId string, accsearchtext string, accbyteamname string, accbyname string, accbyrole string, accbyaccess string) ([]map[string]string, []map[string]string) {

	result := make(map[string]string)
	resultList := []map[string]string{}
	resultList1 := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var userIdList []string
	sql := "SELECT DISTINCT err.roster_id as rosterid, up.fk_user_id as userid, te.team_name , te.id as teamid , up.name, up.last_name, trm.member_role, trm.accessrolearray  FROM  event_register as er left join team as te on te.id = er.fk_team_id left join event_register_roster as err on err.fk_event_reg_id = er.id left join team_roster_member_role as trm on err.roster_id = trm.fk_roster_id left join user_profile as up on trm.roster_member_id = up.fk_user_id where er.fk_event_id =? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status = false and err.roster_id notnull "

	if accsearchtext != "" {
		accsearchtext = strings.ToLower(accsearchtext)
		accsearchtext = strings.TrimSpace(accsearchtext)
		sql = sql + " " + "and "
	}

	flag1 := false
	flag2 := false
	flag3 := false
	flag4 := false

	if accsearchtext != "" && accbyteamname != "" {
		if accbyteamname != "" || accbyname != "" || accbyrole != "" || accbyaccess != "" {
			sql = sql + " " + "("
		}

		sql = sql + " " + "lower(te.team_name) ILIKE '%" + accsearchtext + "%' "
		flag1 = true
	}

	if accsearchtext != "" && accbyname != "" {
		if !flag1 {
			sql = sql + " " + "("
		} else {
			sql = sql + " " + " or "
		}
		sql = sql + " " + "lower(up.name) ILIKE '%" + accsearchtext + "%' or "
		sql = sql + " " + "lower(up.last_name) ILIKE '%" + accsearchtext + "%' "
		flag2 = true
	}

	if accsearchtext != "" && accbyrole != "" {
		if flag2 || flag1 {
			sql = sql + " " + " or "
		} else {
			sql = sql + " " + "("
		}
		sql = sql + " " + "lower(trm.member_role) ILIKE '%" + accsearchtext + "%' "
		flag3 = true
	}

	if accsearchtext != "" && accbyaccess != "" {
		if flag2 || flag1 || flag3 {
			sql = sql + " " + " or "
		} else {
			sql = sql + " " + "("
		}
		tempaccsearchtext := strings.Replace(accsearchtext, " ", "", -1)

		sql = sql + " '" + tempaccsearchtext + "' = ANY (trm.accessrolearray)"

		flag4 = true
	}

	if accsearchtext != "" && accbyteamname == "" && accbyname == "" && accbyrole == "" && accbyaccess == "" {
		sql = sql + " " + "(lower(te.team_name) ILIKE '%" + accsearchtext + "%' "
		sql = sql + " " + "or lower(up.name) ILIKE '%" + accsearchtext + "%' "
		sql = sql + " " + "or lower(up.last_name) ILIKE '%" + accsearchtext + "%' "
		tempaccsearchtext := strings.Replace(accsearchtext, " ", "", -1)
		sql = sql + " " + "or '" + tempaccsearchtext + "' = ANY (trm.accessrolearray)"
		sql = sql + " " + "or lower(trm.member_role) ILIKE '%" + accsearchtext + "%' "
		sql = sql + " " + ")"
	}

	if flag2 || flag1 || flag3 || flag4 {
		sql = sql + " " + ")"
	}

	fmt.Println(sql)
	num, err := o.Raw(sql, eventId).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, k := range maps {
			if k["userid"] != nil && k["teamid"] != nil {
				userid := k["userid"].(string)
				teamid := k["teamid"].(string)
				if !common.StringInArray(teamid+"_"+userid, userIdList) {

					num1, err1 := o.Raw("select tm.id as teamid, team_name as teamname, up.name as firstname, up.last_name as lastname, up.files as filepath, trm.member_role,trm.accessrolearray from team_roster_member_role as trm left join event_register_roster as err on err.roster_id = trm.fk_roster_id left join event_register as er on er.id = err.fk_event_reg_id left join team as tm on tm.id = er.fk_team_id left join user_profile as up  on up.fk_user_id = trm.roster_member_id  where er.fk_event_id = ? and trm.roster_member_id = ? and tm.id= ?", eventId, userid, teamid).Values(&maps1)
					fmt.Print(err1)
					if num1 > 0 {

						var roleaccess []string
						var accessrole []string

						roleaccessstring := ""
						accessrolestring := ""

						for _, v := range maps1 {

							if v["filepath"] != nil {
								result["filepath"] = v["filepath"].(string)
							} else {
								result["filepath"] = constants.STATICUSERIMAGE
							}

							if v["teamid"] != nil {
								result["teamid"] = v["teamid"].(string)
							} else {
								result["teamid"] = "0"
							}

							if v["teamname"] != nil {
								result["teamname"] = v["teamname"].(string)
							} else {
								result["teamname"] = ""
							}

							if v["firstname"] != nil {
								result["firstname"] = v["firstname"].(string)
							} else {
								result["firstname"] = ""
							}

							if v["lastname"] != nil {
								result["lastname"] = v["lastname"].(string)
							} else {
								result["lastname"] = ""
							}

							if v["member_role"] != nil {
								roleaccess = GetRoleNameByRolesvalues(v["member_role"].(string), roleaccess)
							}

							if v["accessrolearray"] != nil {
								accessrole = GetAccessNameByAccessvalues(v["accessrolearray"].(string), accessrole)
							}

						}

						if len(roleaccess) > 0 {
							roleaccessstring = strings.Join(roleaccess, ", ")
						}

						if len(accessrole) > 0 {
							accessrolestring = strings.Join(accessrole, ", ")
						}

						result["member_role"] = roleaccessstring
						result["accessrolearray"] = accessrolestring
						result["userid"] = userid
						result["eventId"] = eventId

						resultList = append(resultList, result)
						resultList1 = append(resultList1, result)
						userIdList = append(userIdList, result["teamid"]+"_"+userid)
						result = make(map[string]string)
					}
				}
			}
		}
	}

	return resultList, resultList1

}
func GetCaptainCoCaptainByTeamId(team_id string) (string, string) {
	captain_id := ""
	co_captain_id := ""
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select * from team where id = ?", team_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		captain_id = maps[0]["fk_captain_id"].(string)
		co_captain_id = maps[0]["fk_co_captains"].(string)
		co_captain_id = strings.Replace(co_captain_id, `}`, "", -1)
		co_captain_id = strings.Replace(co_captain_id, `{`, "", -1)
		fmt.Println(captain_id)
		fmt.Println(co_captain_id)
	}
	return captain_id, co_captain_id

}
func GetUserAgeByUserId(userid string) string {

	user_age := ""
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select fk_user_id,date_part('year',age(dob)) as age from user_profile where fk_user_id = ?", userid).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		if maps[0]["age"] != nil {
			user_age = maps[0]["age"].(string)
		}
	}
	return user_age
}

func GetAccGeneralUpdateDetailByTeamEventUserId(team_id string, event_id string, user_id string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}
	var roleaccess []string
	var accessrolearray []string
	var roleaccessstring string
	var accessrolestring string

	num, err := o.Raw("select up.fk_user_id as userid, up.files as filepath, up.name as firstname, up.last_name as lastname, trm.member_role,trm.accessrolearray from team_roster_member_role as trm left join event_register_roster as err on err.roster_id = trm.fk_roster_id left join event_register as er on er.id = err.fk_event_reg_id left join team as tm on tm.id = er.fk_team_id left join user_profile as up  on up.fk_user_id = trm.roster_member_id  where er.fk_event_id = ? and trm.roster_member_id = ? and tm.id= ?", event_id, user_id, team_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v := range maps {

			if v["filepath"] != nil {
				result["filepath"] = v["filepath"].(string)
			} else {
				result["filepath"] = "/static/images/user-default.png"
			}

			if v["userid"] != nil {
				result["userid"] = v["userid"].(string)
			} else {
				result["userid"] = ""
			}

			if v["firstname"] != nil {
				result["firstname"] = v["firstname"].(string)
			} else {
				result["firstname"] = ""
			}

			if v["lastname"] != nil {
				result["lastname"] = v["lastname"].(string)
			} else {
				result["lastname"] = ""
			}

			if v["member_role"] != nil {
				roleaccess = GetRoleValueArrayByRolesvalues(v["member_role"].(string), roleaccess)
			}

			if v["accessrolearray"] != nil {
				accessrolearray = GetAccessNameByAccessNameValue(v["accessrolearray"].(string), accessrolearray)
			}
		}

		if len(roleaccess) > 0 {
			roleaccessstring = strings.Join(roleaccess, ",")
		}

		if len(accessrolearray) > 0 {
			accessrolestring = strings.Join(accessrolearray, ",")
		}

		result["member_role"] = roleaccessstring

		result["access_role"] = accessrolestring

		resultList = append(resultList, result)
		result = make(map[string]string)
	}

	return resultList
}

func GetAllAccPrintDetailByTeamEventUserId(team_event_userId []string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}

	for _, k := range team_event_userId {
		sliptArray := strings.Split(k, "_")
		team_id := sliptArray[1]
		event_id := sliptArray[2]
		user_id := sliptArray[3]
		num, err := o.Raw("select ev.event_name as eventname, up.files as filepath, up.name as firstname, up.last_name as lastname, trm.member_role,trm.accessrolearray from team_roster_member_role as trm left join event_register_roster as err on err.roster_id = trm.fk_roster_id left join event_register as er on er.id = err.fk_event_reg_id left join team as tm on tm.id = er.fk_team_id left join user_profile as up  on up.fk_user_id = trm.roster_member_id left join event as ev on ev.id = er.fk_event_id  where er.fk_event_id = ? and trm.roster_member_id = ? and tm.id= ?", event_id, user_id, team_id).Values(&maps)
		fmt.Print(err)
		if num > 0 {
			var roleaccess []string
			var accessrole []string
			var roleaccessstring string
			var accessrolestring string

			for _, v := range maps {

				if v["eventname"] != nil {
					result["eventname"] = v["eventname"].(string)
				} else {
					result["eventname"] = ""
				}

				if v["filepath"] != nil {
					result["filepath"] = v["filepath"].(string)

					result["fileexist"] = common.CheckImageExistOrNotS3Bucket(v["filepath"].(string))
				} else {
					result["filepath"] = ""
					result["fileexist"] = "false"
				}

				if v["firstname"] != nil {
					result["firstname"] = v["firstname"].(string)
				} else {
					result["firstname"] = ""
				}

				if v["lastname"] != nil {
					result["lastname"] = v["lastname"].(string)
				} else {
					result["lastname"] = ""
				}

				if v["member_role"] != nil {
					roleaccess = GetRoleNameByRolesvalues(v["member_role"].(string), roleaccess)
				}

				if v["accessrolearray"] != nil {
					accessrole = GetAccessNameByAccessCode(v["accessrolearray"].(string), accessrole)
				}
			}

			if len(roleaccess) > 0 {
				sort.Slice(roleaccess, func(i, j int) bool {
					return roleaccess[i] < roleaccess[j]
				})
				roleaccessstring = strings.Join(roleaccess, " / ")
			}

			if len(accessrole) > 0 {
				sort.Slice(accessrole, func(i, j int) bool {
					return accessrole[i] < accessrole[j]
				})
				accessrolestring = strings.Join(accessrole, ",")
			}

			result["member_role"] = roleaccessstring
			result["accessrolearray"] = accessrolestring

			resultList = append(resultList, result)
			result = make(map[string]string)
		}

	}

	return resultList
}
func GetEventReportList(eventid string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}

	num, err := o.Raw("select *,te.team_name as team_name,er.id as event_reg_id,er.fk_event_id as event_id,er.fk_team_id as fk_team_id from event_register as er left join team as te on te.id = er.fk_team_id where er.fk_event_id = ? and er.active = 1 and er.registration_status = ? and er.reg_cancel_status = ? order by er.id desc", eventid, "Success", "false").Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v1 := range maps {
			result["team_name"] = v1["team_name"].(string)
			result["event_reg_id"] = v1["event_reg_id"].(string)
			result["fk_event_id"] = v1["fk_event_id"].(string)
			result["fk_team_id"] = v1["fk_team_id"].(string)

			resultList = append(resultList, result)
			result = make(map[string]string)
		}
	}
	return resultList
}

func GetRoleAccessDetail() []map[string]string {

	result := make(map[string]string)
	resultList := []map[string]string{}
	accesscodearray := []string{}
	getAllRoleDetail := GetAllRoleDetail()
	for _, grdvalue := range getAllRoleDetail {

		result["role"] = grdvalue["rolename_value"].(string)

		getrole_access_Detail := Getrole_access_Detail(grdvalue["id"].(string))

		accesscodearray = []string{}
		var accesscodearrayString string
		accesscodearray := GetAccessNameByAccessId(getrole_access_Detail, accesscodearray)

		if len(accesscodearray) > 0 {
			accesscodearrayString = strings.Join(accesscodearray, ",")
		}

		result["accesscodearray"] = accesscodearrayString
		accesscodearray = []string{}
		resultList = append(resultList, result)
		result = make(map[string]string)
	}

	return resultList
}

func GetAllRoleDetail() []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("select * from all_role").Values(&maps)
	fmt.Println(err)

	return maps

}

func GetAllAccessDetail() []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("select * from all_access").Values(&maps)
	fmt.Println(err)

	return maps

}

func Getrole_access_Detail(fk_role_id string) string {

	o := orm.NewOrm()
	var maps []orm.Params
	var teampfk_access_id string
	num, err := o.Raw("select fk_access_id from role_access_jct where fk_role_id=?", fk_role_id).Values(&maps)
	if num > 0 {
		if maps[0]["fk_access_id"] != nil {
			teampfk_access_id = maps[0]["fk_access_id"].(string)
		}
	}
	fmt.Println(err)

	return teampfk_access_id

}

func SaveManualAccDetail(accusername string, accroleselectmultiple string, accaccessrole string, acc_even_id string, accmanualuserid string, path string, currentuserID string) bool {
	status := false
	o := orm.NewOrm()

	var maps []orm.Params
	var url string
	if path != "/static/images/user-default.png" && path != "" && !strings.HasPrefix(path, "https://") {
		url = "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path
	} else {
		url = path
	}

	var err error
	var num int64
	action := ""
	if accmanualuserid != "0" {
		num, err = o.Raw("select * from manualaccdetail where userid=?", accmanualuserid).Values(&maps)
		if num > 0 {
			_, err = o.Raw("UPDATE manualaccdetail SET name = ?, role = ?, access = ?, fk_event_id = ?, files = ?, userid =?  WHERE id = ? RETURNING id", accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", acc_even_id, url, accmanualuserid, maps[0]["id"].(string)).Values(&maps)
			action = "Update"
		} else {
			_, err = o.Raw("INSERT INTO manualaccdetail (name, role, access, fk_event_id, userid, files) VALUES (?,?,?,?,?,?) RETURNING id", accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", acc_even_id, accmanualuserid, url).Values(&maps)
			action = "Create"
		}
	} else {
		_, err = o.Raw("INSERT INTO manualaccdetail (name, role, access, fk_event_id, userid, files) VALUES (?,?,?,?,?,?) RETURNING id", accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", acc_even_id, accmanualuserid, url).Values(&maps)
		action = "Create"
	}

	if err == nil {
		if maps[0]["id"] != nil {
			_, err = o.Raw("INSERT INTO manual_acc_detaillog (fk_manual_acc_Id, changename, changerole, changeaccess, changefiles, actions, updatedby, updatedat) VALUES (?,?,?,?,?,?,?,?) ", maps[0]["id"].(string), accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", url, action, currentuserID, time.Now()).Values(&maps)
		}

		if err != nil {
			status = false
		} else {
			status = true
		}
		fmt.Println(err)
	} else {
		status = false
		fmt.Println(err)
	}

	return status
}

func GetAccreditationManualDetail(eventId string) ([]map[string]string, []map[string]string) {

	result := make(map[string]string)
	resultList := []map[string]string{}
	resultList1 := []map[string]string{}
	o := orm.NewOrm()
	var maps []orm.Params
	sql := "SELECT * from manualaccdetail where fk_event_id =? order by id desc "
	fmt.Println(sql)
	num, err := o.Raw(sql, eventId).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v := range maps {

			var roleaccess []string
			var accessrole []string

			roleaccessstring := ""
			accessrolestring := ""

			if v["name"] != nil {
				result["name"] = v["name"].(string)
			} else {
				result["name"] = ""
			}

			if v["id"] != nil {
				result["id"] = v["id"].(string)
			} else {
				result["id"] = ""
			}

			if v["files"] != nil {
				result["files"] = v["files"].(string)
			} else {
				result["files"] = ""
			}

			if v["role"] != nil {
				roleaccess = GetRoleNameByRolesvalues(v["role"].(string), roleaccess)
			}

			if v["access"] != nil {
				accessrole = GetAccessNameByAccessvalues(v["access"].(string), accessrole)
			}

			if len(roleaccess) > 0 {
				roleaccessstring = strings.Join(roleaccess, ", ")
			}

			if len(accessrole) > 0 {
				accessrolestring = strings.Join(accessrole, ", ")
			}

			result["member_role"] = roleaccessstring
			result["accessrolearray"] = accessrolestring

			resultList = append(resultList, result)
			resultList1 = append(resultList1, result)
			result = make(map[string]string)

		}
	}

	return resultList, resultList1
}

func GetRosterDetailForTeam(eventregid string) ([]map[string]string, []string) {
	var team_name []string
	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}
	num, err := o.Raw("select er_roster.fk_event_reg_id as event_reg_id,er_roster.roster_id as roster_id,er.fk_team_id as team_id,er.fk_event_id as event_id, te.team_name as team_name,er_roster.division,er_roster.clases,er_roster.boat_type,er_roster.distance from event_register_roster as er_roster left join event_register as er on er_roster.fk_event_reg_id = er.id left join team as te on te.id = er.fk_team_id where er_roster.fk_event_reg_id in (" + eventregid + ") and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status ='false' order by er_roster.id desc").Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v1 := range maps {
			team_name = append(team_name, v1["team_name"].(string))
			result["event_reg_id"] = v1["event_reg_id"].(string)
			result["division"] = v1["division"].(string)
			result["distance"] = v1["distance"].(string)
			result["class"] = v1["clases"].(string)
			result["boat_type"] = v1["boat_type"].(string)
			result["roster_id"] = v1["roster_id"].(string)
			result["team_name"] = v1["team_name"].(string)
			result["team_id"] = v1["team_id"].(string)
			result["event_id"] = v1["event_id"].(string)
			resultList = append(resultList, result)
			result = make(map[string]string)
		}
	}

	return resultList, team_name
}

func GetAllAccManualPrintDetailById(printrecorddetail []string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}

	for _, k := range printrecorddetail {

		num, err := o.Raw("select *, ev.event_name as eventname from manualaccdetail as md left join event as ev on ev.id = md.fk_event_id where md.id = ? ", k).Values(&maps)
		fmt.Print(err)
		if num > 0 {
			var roleaccess []string
			var accessrole []string
			var roleaccessstring string
			var accessrolestring string

			for _, v := range maps {

				if v["eventname"] != nil {
					result["eventname"] = v["eventname"].(string)
				} else {
					result["eventname"] = ""
				}

				if v["files"] != nil {
					result["filepath"] = v["files"].(string)

					result["fileexist"] = common.CheckImageExistOrNotS3Bucket(v["files"].(string))
				} else {
					result["filepath"] = ""
					result["fileexist"] = "false"
				}

				if v["name"] != nil {
					result["firstname"] = v["name"].(string)
				} else {
					result["firstname"] = ""
				}

				result["lastname"] = ""

				if v["role"] != nil {
					roleaccess = GetRoleNameByRolesvalues(v["role"].(string), roleaccess)
				}

				if v["access"] != nil {
					accessrole = GetAccessNameByAccessCode(v["access"].(string), accessrole)
				}
			}

			if len(roleaccess) > 0 {
				sort.Slice(roleaccess, func(i, j int) bool {
					return roleaccess[i] < roleaccess[j]
				})
				roleaccessstring = strings.Join(roleaccess, " / ")
			}

			if len(accessrole) > 0 {
				sort.Slice(accessrole, func(i, j int) bool {
					return accessrole[i] < accessrole[j]
				})
				accessrolestring = strings.Join(accessrole, ",")
			}

			result["member_role"] = roleaccessstring
			result["accessrolearray"] = accessrolestring

			resultList = append(resultList, result)
			result = make(map[string]string)
		}

	}

	return resultList
}
func GetEventAllRace(event_id string) []string {

	o := orm.NewOrm()
	var maps []orm.Params
	//result := []string{}
	//resultList := []string{}
	var final_array []string
	num, err := o.Raw("select * from event where id =?", event_id).Values(&maps)
	if num > 0 {
		var boattype, division, class, distance string
		var boattype_arr, division_arr, class_arr, distance_arr []string

		if maps[0]["boattype"] != nil {
			boattype = maps[0]["boattype"].(string)
			boattype = strings.Replace(boattype, `}`, "", -1)
			boattype = strings.Replace(boattype, `{`, "", -1)
			boattype_arr = strings.Split(boattype, ",")
		}
		if maps[0]["divisions"] != nil {
			division = maps[0]["divisions"].(string)
			division = strings.Replace(division, `}`, "", -1)
			division = strings.Replace(division, `{`, "", -1)
			division_arr = strings.Split(division, ",")

		}
		if maps[0]["classes"] != nil {
			class = maps[0]["classes"].(string)
			class = strings.Replace(class, `}`, "", -1)
			class = strings.Replace(class, `{`, "", -1)
			class_arr = strings.Split(class, ",")

		}
		if maps[0]["eventdistance"] != nil {
			distance = maps[0]["eventdistance"].(string)
			distance = strings.Replace(distance, `}`, "", -1)
			distance = strings.Replace(distance, `{`, "", -1)
			distance_arr = strings.Split(distance, ",")

		}

		for _, divArr := range division_arr {

			var div = divArr

			if common.StringInArray("10", boattype_arr) {

				var val2 = "10_" + div

				if common.StringInArray("M", class_arr) {

					var val3 = val2 + "_M"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}
				if common.StringInArray("W", class_arr) {

					var val3 = val2 + "_W"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}
				if common.StringInArray("MX", class_arr) {

					var val3 = val2 + "_MX"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}

			}
			if common.StringInArray("20", boattype_arr) {

				var val2 = "20_" + div

				if common.StringInArray("M", class_arr) {

					var val3 = val2 + "_M"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}
				if common.StringInArray("W", class_arr) {

					var val3 = val2 + "_W"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}
				if common.StringInArray("MX", class_arr) {

					var val3 = val2 + "_MX"

					if common.StringInArray("200", distance_arr) {

						var val4 = val3 + "_200"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("500", distance_arr) {

						var val4 = val3 + "_500"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("1000", distance_arr) {

						var val4 = val3 + "_1000"
						final_array = append(final_array, val4)

					}
					if common.StringInArray("2000", distance_arr) {

						var val4 = val3 + "_2000"
						final_array = append(final_array, val4)

					}

				}

			}

		}
	}
	fmt.Println(err)
	fmt.Println(final_array)

	return final_array

}
func GetRosterDetailForRace(race string, event_id string) ([]map[string]string, []string) {
	var race_name []string
	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}
	racearr := strings.Split(race, ",")
	for _, racedata := range racearr {

		values := strings.Split(racedata, "_")
		fmt.Println(values)
		var boat_type = values[0]
		var division = values[1]
		var class = values[2]
		var distance = values[3]

		num, err := o.Raw("select er_roster.fk_event_reg_id as event_reg_id,er_roster.roster_id as roster_id,er.fk_team_id as team_id,er.fk_event_id as event_id, te.team_name as team_name,er_roster.division,er_roster.clases,er_roster.boat_type,er_roster.distance from event_register_roster as er_roster left join event_register as er on er_roster.fk_event_reg_id = er.id left join team as te on te.id = er.fk_team_id where er_roster.boat_type=? and  er_roster.division= ? and er_roster.clases=? and er_roster.distance=? and er.fk_event_id =? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status ='false' order by er_roster.id desc", boat_type, division, class, distance, event_id).Values(&maps)
		fmt.Print(err)
		if num > 0 {
			for _, v1 := range maps {
				result["event_reg_id"] = v1["event_reg_id"].(string)
				result["division"] = v1["division"].(string)
				result["distance"] = v1["distance"].(string)
				result["class"] = v1["clases"].(string)
				result["boat_type"] = v1["boat_type"].(string)
				result["roster_id"] = v1["roster_id"].(string)
				result["team_name"] = v1["team_name"].(string)
				result["team_id"] = v1["team_id"].(string)
				result["event_id"] = v1["event_id"].(string)
				race_name = append(race_name, v1["boat_type"].(string)+"_"+v1["division"].(string)+"_"+v1["clases"].(string)+"_"+v1["distance"].(string))
				fmt.Println(race_name)
				resultList = append(resultList, result)
				result = make(map[string]string)
			}
		}
	}

	return resultList, race_name
}
func GetRosterDetailForIndividualTeam(event_reg_id string, boat string, div string, class string, dist string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}

	num, err := o.Raw("select er_roster.fk_event_reg_id as event_reg_id,er_roster.roster_id as roster_id,er.fk_team_id as team_id,er.fk_event_id as event_id, te.team_name as team_name,er_roster.division,er_roster.clases,er_roster.boat_type,er_roster.distance from event_register_roster as er_roster left join event_register as er on er_roster.fk_event_reg_id = er.id left join team as te on te.id = er.fk_team_id where er_roster.boat_type=? and  er_roster.division= ? and er_roster.clases=? and er_roster.distance=? and er_roster.fk_event_reg_id = ? and er.active=1 and er.registration_status ='Success' and er.reg_cancel_status ='false' order by er_roster.id desc", boat, div, class, dist, event_reg_id).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v1 := range maps {
			result["event_reg_id"] = v1["event_reg_id"].(string)
			result["division"] = v1["division"].(string)
			result["distance"] = v1["distance"].(string)
			result["class"] = v1["clases"].(string)
			result["boat_type"] = v1["boat_type"].(string)
			result["roster_id"] = v1["roster_id"].(string)
			result["team_name"] = v1["team_name"].(string)
			result["team_id"] = v1["team_id"].(string)
			result["event_id"] = v1["event_id"].(string)
			resultList = append(resultList, result)
			result = make(map[string]string)
		}
	}

	return resultList
}

func GetAccManualUpdateDetailByManualAutoId(manualAutoId string) []map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	resultList := []map[string]string{}
	var roleaccess []string
	var accessrolearray []string
	var roleaccessstring string
	var accessrolestring string

	num, err := o.Raw("select files as filepath, name as firstname , role as member_role, access as accessrolearray, id as manualid from manualaccdetail where id= ?", manualAutoId).Values(&maps)
	fmt.Print(err)
	if num > 0 {
		for _, v := range maps {

			if v["filepath"] != nil {
				result["filepath"] = v["filepath"].(string)
			} else {
				result["filepath"] = "/static/images/user-default.png"
			}

			if v["manualid"] != nil {
				result["manualid"] = v["manualid"].(string)
			} else {
				result["manualid"] = ""
			}

			if v["firstname"] != nil {
				result["firstname"] = v["firstname"].(string)
			} else {
				result["firstname"] = ""
			}

			result["lastname"] = ""

			if v["member_role"] != nil {
				roleaccess = GetRoleValueArrayByRolesvalues(v["member_role"].(string), roleaccess)
			}

			if v["accessrolearray"] != nil {
				accessrolearray = GetAccessNameByAccessNameValue(v["accessrolearray"].(string), accessrolearray)
			}
		}

		if len(roleaccess) > 0 {
			roleaccessstring = strings.Join(roleaccess, ",")
		}

		if len(accessrolearray) > 0 {
			accessrolestring = strings.Join(accessrolearray, ",")
		}

		result["member_role"] = roleaccessstring

		result["access_role"] = accessrolestring

		resultList = append(resultList, result)
		result = make(map[string]string)
	}

	return resultList
}

func UpdateManualAccDetail(accusername string, accroleselectmultiple string, accaccessrole string, acc_even_id string, path string, updatedmanualid string, currentuserID string) bool {
	status := false
	o := orm.NewOrm()

	var maps []orm.Params
	var url string
	if path != "/static/images/user-default.png" && path != "" && !strings.HasPrefix(path, "https://") {
		url = "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path
	} else {
		url = path
	}

	var err error
	action := ""

	_, err = o.Raw("UPDATE manualaccdetail SET name = ?, role = ?, access = ?, fk_event_id = ?, files = ? WHERE id = ? ", accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", acc_even_id, url, updatedmanualid).Values(&maps)
	action = "Update"

	if err == nil {
		_, err = o.Raw("INSERT INTO manual_acc_detaillog (fk_manual_acc_Id, changename, changerole, changeaccess, changefiles, actions, updatedby, updatedat) VALUES (?,?,?,?,?,?,?,?) ", updatedmanualid, accusername, "{"+accroleselectmultiple+"}", "{"+accaccessrole+"}", url, action, currentuserID, time.Now()).Values(&maps)

		if err != nil {
			status = false
		} else {
			status = true
		}
		fmt.Println(err)
	} else {
		status = false
		fmt.Println(err)
	}

	return status
}

func GetPricingOtherIteamId(eventid string) []string {

	var iteamid []string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT id FROM pricing_other WHERE fk_event_id = ? ", eventid).Values(&maps)
	fmt.Println(err)
	if num > 0 && err == nil {
		for _, v := range maps {
			iteamid = append(iteamid, v["id"].(string))
		}

	}
	return iteamid
}

func SaveEventPaymentLogUrl(path string, paymentlogId string) string {
	status := "false"
	o := orm.NewOrm()

	var maps []orm.Params
	url := "https://" + os.Getenv("BUCKET") + ".s3-us-west-2.amazonaws.com" + path

	_, err := o.Raw("UPDATE event_reg_payment_log SET invoicepath = ? where id = ? ", url, paymentlogId).Values(&maps)

	if err == nil {
		status = "true"
	} else {
		status = "false"
		fmt.Println(err)
	}

	return status
}
