package models

import (
	"fmt"
	"strconv"
	"strings"
	"github.com/astaxie/beego/orm"
	_"golang.org/x/crypto/bcrypt"
)

func SearchPaddler(data map[string]string) (map[string]orm.Params, int64) {

	result := make(map[string]orm.Params)
	//result["status"] = "0"

	fmt.Println("Search by ", data)
	// AND activities @> ARRAY [?]::varchar[]

	sql := "SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE active != 9 AND"
	sqlChanged := false

	
	if len(data["PaddlerName"]) > 0 {
		sql = sql + " lower(name) LIKE '%" + strings.ToLower(data["PaddlerName"]) + "%'"
		sqlChanged = true
	}

	if len(data["Email"]) > 0 {
		if !sqlChanged {
			sql = sql + " lower(email_address) LIKE '%" + strings.ToLower(data["Email"]) + "%'"
			sqlChanged = true
		} else {
			sql = sql + " AND lower(email_address) LIKE '%" + strings.ToLower(data["Email"]) + "%'"
		}

	}

	if len(data["Location"]) > 0 {
		if !sqlChanged {
			sql =  sql + " lower(location) LIKE '%" + strings.ToLower(data["Location"]) + "%'"
			sqlChanged = true
		} else {
			sql =  sql + " AND lower(location) LIKE '%" + strings.ToLower(data["Location"]) + "%'"
		}
	}

	if len(data["Classes"]) > 0 {
		if !sqlChanged {
			sql =  sql + " classes = '" + data["Classes"] + "'"
			sqlChanged = true
		} else {
			sql =  sql + " AND classes = '" + data["Classes"] + "'"
		}
	}

	if len(data["PaddlingSide"]) > 0 {
		if !sqlChanged {
			sql =  sql + " paddling_side = '" + data["PaddlingSide"] + "'"
			sqlChanged = true
		} else {
			sql =  sql + " AND paddling_side = '" + data["PaddlingSide"] + "'"
		}
	}

	if len(data["Skill"]) > 0 {
		if !sqlChanged {
			sql =  sql + " skill_level = '" + data["Skill"] + "'"
			sqlChanged = true
		} else {
			sql =  sql + " AND skill_level = '" + data["Skill"] + "'"
		}
	}

	if len(data["ForTeam"]) > 0 {
		if !sqlChanged {
			sql =  sql + " status = '" + data["ForTeam"] + "'"
			sqlChanged = true
		} else {
			sql =  sql + " AND status = '" + data["ForTeam"] + "'"
		}
	}

	sql = sql + " order by users.id ASC LIMIT " + data["Limit"] + " OFFSET " + data["Offset"]
	
	if !sqlChanged {

		if data["Limit"] != "" || data["Offset"] != "" {
			sql = "SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE active = 1 order by users.id ASC LIMIT " + data["Limit"] + " OFFSET " + data["Offset"]
		} else {
			sql = "SELECT * FROM users join user_profile on users.id = user_profile.fk_user_id WHERE active = 1 order by users.id ASC LIMIT 10 OFFSET 0"
		}
		
	}

	if data["Type"] == "last" {
		sql = strings.Replace(sql, "ASC", "DESC", -1)
		sql = strings.TrimSuffix(sql, "OFFSET " + data["Offset"])
		sql = strings.TrimSuffix(sql, "OFFSET 0")
	}

	fmt.Println("SQL = " + sql)
	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, _ := o.Raw(sql).Values(&maps)
	if num > 0 {
		for k, v := range maps {
			
			key := "paddler_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		fmt.Println("No paddlers")
		result["status"] = nil
	}
	
	sql = strings.TrimSuffix(sql, "OFFSET " + data["Offset"])
	//fmt.Println(sql)

	if len(data["Limit"]) > 0 {
		sql = strings.Replace(sql, "LIMIT " + data["Limit"], "", -1)
	} else {
		sql = strings.Replace(sql, "LIMIT 10", "", -1)
	}
	
	
	
	//fmt.Println(sql)
	num, _ = o.Raw(sql).Values(&maps)
	
	return result, num

}