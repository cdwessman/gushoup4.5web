package smscommon

import (
	"encoding/json"
	"fmt"

	"gushou/constants"
	"os"
	"os/exec"
	"reflect"
	"unsafe"
)

type messageServiceData struct {
	FriendlyName string `json:"friendly_name"`
	AccountSid   string `json:"account_sid"`
	Sid          string `json:"sid"`
}

type msMetaData struct {
	FirstPageUrl string `json:"first_page_url"`
	ServiceUrl   string `json:"url"`
}

type messageService struct {
	MessageServiceList []messageServiceData `json:"services"`
	MsgServiceMeta     msMetaData           `json:"meta"`
}

func SendSms(toSms string, smsMessage string, uniqueId string, fromPhoneNumber string) string {

	fmt.Printf("\n In common send sms function")
	isSmsSent := "false"

	hostName := os.Getenv("HOST_NAME")
	var sendSmsTo string
	if hostName == "gogushoutesting" {
		sendSmsTo = toSms
	} else if hostName == "gogushoulive" {
		sendSmsTo = toSms
	} else {
		sendSmsTo = toSms
	}

	fromPhoneNumber = constants.FROM
	fmt.Println(sendSmsTo)

	createMsgcurl := exec.Command("curl", constants.CREATE_MESSAGE_URL, "-X", "POST", "--data-urlencode", "To="+sendSmsTo, "--data-urlencode", "From="+constants.FROM, "--data-urlencode", "Body="+smsMessage, "-u", constants.ACCOUNT_SID+":"+constants.AUTH_TOKEN)
	msgOut, err := createMsgcurl.Output()
	if err != nil {
		fmt.Println("error while sending the sms", err)
		isSmsSent = "false"
	} else {
		msgStr := BytesToString(msgOut)
		fmt.Println("Create a Message: ", msgStr)
		isSmsSent = "true"
	}

	return isSmsSent
}

func IsMessageServiceCreatedPractice(messageServiceID string) string {

	fmt.Printf("\n In isMessageServiceCreatedPractice")

	isMsCreated := "false"

	// check whether the message service is created or not
	msCheckCurl := exec.Command("curl", constants.MESSAGE_SERVICE_LIST_URL, "-u", constants.ACCOUNT_SID+":"+constants.AUTH_TOKEN)
	outMS, err := msCheckCurl.Output()

	if err != nil {
		fmt.Println("error creating for : ", err)
		isMsCreated = "false"
	} else {
		pingData := BytesToString(outMS)
		messageServiceData := messageService{}
		err := json.Unmarshal([]byte(pingData), &messageServiceData)
		if err != nil {
			fmt.Println(err)
			isMsCreated = "false"
		} else {
			if len(messageServiceData.MessageServiceList) > 0 {

				msFriendlyName := ""
				msSid := ""
				msList := messageServiceData.MessageServiceList
				for k, v := range msList {
					fmt.Println(k, v)
					fmt.Println(msList[k].FriendlyName)
					fmt.Println(msList[k].Sid)

					if msList[k].Sid == messageServiceID {
						msFriendlyName = msList[k].FriendlyName
						msSid = msList[k].Sid
						isMsCreated = "true"
						break
					}
				}

				fmt.Println(msFriendlyName)
				fmt.Println(msSid)
			}
		}

	}

	return isMsCreated
}

func BytesToString(b []byte) string {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}
	return *(*string)(unsafe.Pointer(&sh))
}
