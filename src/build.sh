go get github.com/astaxie/beego
go get github.com/astaxie/beego/orm
go get github.com/astaxie/beego/context
go get github.com/lib/pq
go get github.com/astaxie/beego/session
go get github.com/beego/i18n
go get golang.org/x/crypto/bcrypt  
# go get gopkg.in/mailgun/mailgun-go.v1
go get github.com/mailgun/mailgun-go
go get github.com/go-ini/ini
go get github.com/jmespath/go-jmespath
go get -u github.com/aws/aws-sdk-go
go get github.com/mattbaird/gochimp
go get googlemaps.github.io/maps
go get golang.org/x/net/context
go get github.com/metakeule/fmtdate
go get github.com/asaskevich/govalidator
go get github.com/subosito/twilio
go get -u github.com/elgs/gostrgen
go get github.com/stripe/stripe-go
go get github.com/bamzi/jobrunner
go get github.com/leekchan/accounting
go get golang.org/x/text/currency
go get github.com/jung-kurt/gofpdf
go get -u github.com/SebastiaanKlippert/go-wkhtmltopdf

echo "Fetched External Dependencies. Starting Build."
go build -o src/gushou/gogushou src/gushou/main.go
